﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;

using System.ServiceModel;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmAdministracion : DevExpress.XtraEditors.XtraForm
    {
        public frmAdministracion()
        {
            InitializeComponent();
        }

        public frmAdministracion(string nombre_usuario)
        {
            InitializeComponent();
            lblUsuario.Text = nombre_usuario ?? "USUARIO";
            lblCargo.Text = Globales.Evaluador.Rol.Nombre;
        }

        private void tstCandidato_Click(object sender, EventArgs e)
        {
            XtraTabPage nueva_pestania_candidato = new XtraTabPage();
            nueva_pestania_candidato.Text = "Nuevo Candidato";
            nueva_pestania_candidato.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

            // Crear vista de la pestania que mostraremos
            Vistas.VistaCandidato vista_candidato = new Vistas.VistaCandidato(ControlPestania);
            nueva_pestania_candidato.Controls.Add(vista_candidato);

            // agregamos la nueva pestania al control de pestania
            ControlPestania.TabPages.Add(nueva_pestania_candidato);
            ControlPestania.SelectedTabPage = nueva_pestania_candidato;
        }

        /// <summary>
        /// Implementa el funcionamiento para el cerrado de la pestania
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlPestania_CloseButtonClick(object sender, EventArgs e)
        {
            ((XtraTabControl)sender).TabPages.Remove(((XtraTabControl)sender).SelectedTabPage);
        }

        private void tstEscolaridad_Click(object sender, EventArgs e)
        {
            new frmEscolaridad().ShowDialog();
        }

        private void tstOcupacion_Click(object sender, EventArgs e)
        {
            frmOcupaciones frm_ocupaciones = new frmOcupaciones();
            frm_ocupaciones.ShowDialog();
        }

        private void tstAccesoTemporal_Click(object sender, EventArgs e)
        {
            frmAccesosTemporales vista_acceso_temporal = new frmAccesosTemporales();
            vista_acceso_temporal.ShowDialog();
        }
        
        private void tstExpediente_Click(object sender, EventArgs e)
        {
            XtraTabPage nueva_pestania_expediente = new XtraTabPage();
            nueva_pestania_expediente.Text = "Expediente";
            nueva_pestania_expediente.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

            // Crear vista de la pestania que mostraremos
            Vistas.VistaExpediente vista_expediente = new Vistas.VistaExpediente(ControlPestania);
            nueva_pestania_expediente.Controls.Add(vista_expediente);

            // agregamos la nueva pestania al control de pestania
            ControlPestania.TabPages.Add(nueva_pestania_expediente);
            ControlPestania.SelectedTabPage = nueva_pestania_expediente;
        }

        private void txtConfiguraciones_Click(object sender, EventArgs e)
        {
            frmConfiguracion frm_configuracion = new frmConfiguracion();
            frm_configuracion.ShowDialog();
        }

        /// <summary>
        /// Muestra el formulario de cambio de contraseña
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frmCambiarPassword().ShowDialog();
        }

        /// <summary>
        /// Popup menu item 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("¿Esta seguro de cerrar su sesión?", "Sesión", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Application.Restart();
        }

        /// <summary>
        /// Muestra el formulario para generar el reporte de la oficina
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstReporteSucursal_Click(object sender, EventArgs e)
        {
            new frmReportes().ShowDialog();
        }

        private void tstReporteAdministrativo_Click(object sender, EventArgs e)
        {
            new frmReportesAdministrativo().ShowDialog();}

        private void tstDashboard_Click(object sender, EventArgs e)
        {
            new frmDashBoard().Show();
        }

        private void tstAcercade_Click(object sender, EventArgs e)
        {
            new frmAcerca().ShowDialog();
        }

        private void frmAdministracion_Load(object sender, EventArgs e)
        {
            ConfiguracionComportamientoRoles();
        }

        /// <summary>
        /// Modifica las opciones y comportamiento de la pantalla administrativa deacuerdo al rol del usuario.
        /// </summary>
        private void ConfiguracionComportamientoRoles()
        {
            if (Globales.Evaluador.Rol.Nombre == "ADMINISTRADOR")
            {
                tstReporteAdministrativo.Visible = true;
                return;
            }
            frmDashBoard vista_dashboard = new frmDashBoard();
            vista_dashboard.Show(this);
            tstReporteAdministrativo.Visible = false;
        }
    }
}