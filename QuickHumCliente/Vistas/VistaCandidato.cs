﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Views.Grid;
using System.ServiceModel;
using DevExpress.Data.Linq;
using DevExpress.Utils;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class VistaCandidato : DevExpress.XtraEditors.XtraUserControl
    {
        
        public XtraTabControl TabControlParent { get; set; }
        public ServicioQuickHum.Candidato Candidato { get; set; }
        private ServicioQuickHum.Expediente[] Expedientes { get; set; }
        private readonly frmCargando frm_cargando = new frmCargando();

        /// <summary>
        /// Constructor
        /// </summary>
        public VistaCandidato()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        /// <summary>
        /// Constructor usado cuando se desea definir una tabControl como padre
        /// </summary>
        /// <param name="padre"></param>
        public VistaCandidato(XtraTabControl padre)
        {
            InitializeComponent();
            this.TabControlParent = padre;
            this.Dock = DockStyle.Fill;

            // Rellenar Controles
            RellenarEdad();
            try
            {
                RellenarEscolaridad();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar las escolaridades: " + ex.Message, "Escolaridad",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                padre.TabPages.Remove(padre.SelectedTabPage);
            }
        }

        /// <summary>
        /// Rellena el combobox de la edad
        /// </summary>
        private void RellenarEdad()
        {
            const int MAX_EDAD = 99;
            for (int i = 1; i <= MAX_EDAD; ++i)
            {
                cmbEdad.Properties.Items.Add(i);
            }
            cmbEdad.SelectedIndex = 0;
        }

        /// <summary>
        /// Rellena el combobox de escolaridad desde el servicio
        /// </summary>
        private void RellenarEscolaridad()
        {
            BOLEscolaridad bol_escolaridad = new BOLEscolaridad();
            ServicioQuickHum.Escolaridad[] escolaridades = bol_escolaridad.ListarEscolaridades();
            foreach (ServicioQuickHum.Escolaridad escolaridad in escolaridades)
            {
                cmbEscolaridad.Properties.Items.Add( escolaridad );
            }
            if (cmbEscolaridad.Properties.Items.Count > 0)
                cmbEscolaridad.SelectedIndex = 0;
        }

        /// <summary>
        /// Almacena un nuevo candidato en la base de datos
        /// </summary>
        private void RegistrarCandidato()
        {
            ActualizarObjetoCandidato();
            BOLCandidato negocio_candidato = new BOLCandidato();
            if (negocio_candidato.RegistrarCandidato(Candidato))
            {
                XtraMessageBox.Show("Candidato registrado con exito.", "Registro exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ActualizarDatosPestania();
                ModoActualizacion();
                return;
            }
            return;
        }

        /// <summary>
        /// Actualiza un candidato
        /// </summary>
        private void ActualizarCandidato()
        {
            this.Candidato.Escolaridad = (ServicioQuickHum.Escolaridad)cmbEscolaridad.SelectedItem;
            this.Candidato.EstadoCivil = cmbEstadoCivil.Text;
            BOLCandidato negocio_candidato = new BOLCandidato();
            try
            {
                if (negocio_candidato.ActualizarCandidato(Candidato))
                {
                    XtraMessageBox.Show("Candidato actualizado con exito", "Actualizacion exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ActualizarDatosPestania();
                }
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("No se pudo actualizar el candidato: " + ex, "Error en la actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       
        /// <summary>
        /// Actualiza aquellos controles de la pestania que representan datos del candidato en la visualizacion.
        /// </summary>
        private void ActualizarDatosPestania()
        {
            if (this.Candidato == null)
                return;
            this.CabezeraCandidato.Text = this.Candidato.Nombre + " " + this.Candidato.Apellido;
            this.TabControlParent.SelectedTabPage.Text = this.Candidato.Dui;
            this.TabControlParent.SelectedTabPage.Name = this.Candidato.Dui;
        }


        /********************* EVENTOS CONTROLES ******************/

        /// <summary>
        /// Muestra una ventana con un expediente nuevo con el candidato predefinido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAgregarExpediente_Click(object sender, EventArgs e)
        {
            XtraTabPage nueva_pestania_expediente = new XtraTabPage();
            nueva_pestania_expediente.Text = "Nuevo Expediente";
            nueva_pestania_expediente.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

            // Crear vista de la pestania que mostraremos
            ServicioQuickHum.Expediente expediente = new ServicioQuickHum.Expediente()
            {
                Candidato = this.Candidato,
                Evaluador = Globales.Evaluador,
                Ubicacion = Globales.Oficina,
            };
            Vistas.VistaExpediente vista_expediente = new Vistas.VistaExpediente(this.TabControlParent, expediente);
            nueva_pestania_expediente.Controls.Add(vista_expediente);

            // agregamos la nueva pestania al control de pestania
            this.TabControlParent.TabPages.Add(nueva_pestania_expediente);
            this.TabControlParent.SelectedTabPage = nueva_pestania_expediente;
        }
        
        /// <summary>
        /// Controla que operacion se realizara sobre el candida, sea insertar uno nuevo o actualizar uno ya existente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwitchOpcionGuardado(object sender, EventArgs e)
        {
            if (this.Candidato == null)
            {
                XtraMessageBox.Show("No ha cargado ninguna candidato", "Actualizar candidato", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            ActualizarCandidato();
        }
        
        /// <summary>
        /// Elimina un candidato
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EliminarCandidato(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Esta seguro que desea eliminar el candidato?", "Eliminacion de candidato", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            BOLCandidato negocio_candidato = new BOLCandidato();
            try
            {
                if (negocio_candidato.EliminarCandidato(this.Candidato.Dui))
                {
                    XtraMessageBox.Show("El candidato fue eliminado con exito.", "Eliminacion de candidato", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.TabControlParent.TabPages.Remove(this.TabControlParent.SelectedTabPage);
                    return;
                }
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un error en la eliminacion del candidato: " + ex.Message, "Eliminacion de candidato", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        /********************* FUNCIONES SOPORTE ******************/

        /// <summary>
        /// Configura la ventana para pasar a modificacion de registros.
        /// </summary>
        private void ModoActualizacion()
        {
            txtDui.Enabled = false;
            btnGuardar.Text = "Actualizar";
            btnEliminar.Enabled = true;
            btnAgregarExpediente.Enabled = true;
        }

        /// <summary>
        /// Muestra una ventana para introducir el dui del candidato a buscar
        /// </summary>
        private string BuscarCandidato()
        {
            frmBuscarCandidato formulario_busqueda_candidato = new frmBuscarCandidato();
            formulario_busqueda_candidato.ShowDialog();
            if (formulario_busqueda_candidato.DialogResult == DialogResult.OK)
            {
                //if (TabControlParent.TabPages.Any(tab => tab.Name == formulario_busqueda_candidato.Dui))
                //{
                //    XtraMessageBox.Show("Ya se encuentra abierta una pestaña con el candidato solicitado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                return formulario_busqueda_candidato.Dui;
            }
            return string.Empty;
        }

        /// <summary>
        /// Actualiza el miembroa candidato de la vista
        /// </summary>
        private void ActualizarObjetoCandidato()
        {
            this.Candidato = new ServicioQuickHum.Candidato()
            {
                Nombre = txtNombre.Text,
                Apellido = txtApellido.Text,
                Dui = txtDui.Text,
                Edad = byte.Parse(cmbEdad.Text),
                Genero = (ServicioQuickHum.CandidatoGenero)cmbGenero.SelectedIndex,
                EstadoCivil = cmbEstadoCivil.Text,
                Telefono = txtTelefono.Text,
                Escolaridad = (ServicioQuickHum.Escolaridad)cmbEscolaridad.SelectedItem
            };
        }

        /// <summary>
        /// Apertura un expediente seleccionado cuando se le da dos veces click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridExpedientes_DoubleClick(object sender, EventArgs e)
        {
            GridView vista = (GridView)GridExpedientes.MainView;
            QuickHumCliente.ServicioQuickHum.Expediente expediente = (Expediente)vista.GetFocusedRow();
            XtraTabPage expediente_tab = new XtraTabPage()
            {
                Text = "Expediente #" + expediente.Numero.ToString(),
                ShowCloseButton = DefaultBoolean.True,
            };
            VistaExpediente vista_expediente = new VistaExpediente(this.TabControlParent, expediente)
            {
                Dock = DockStyle.Fill
            };

            expediente_tab.Controls.Add(vista_expediente);
            TabControlParent.TabPages.Add(expediente_tab);
            TabControlParent.SelectedTabPage = expediente_tab;
        }

        /// <summary>
        /// Se ejecuta cuando se presiona el boton buscar en el menu toolstrip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string dui = BuscarCandidato();
            if (dui == string.Empty)
                return;
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += RecuperarInformacionCandidato;
            worker.RunWorkerCompleted += RecupararInformacionCandidatoFinalizada;
            worker.RunWorkerAsync(dui);
            frm_cargando.ShowDialog();
        }

        /// <summary>
        /// Actualiza los campos de la vista cuando los datos del candidato hayan sido cargados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void RecupararInformacionCandidatoFinalizada(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((bool) e.Result == false)
            {
                frm_cargando.AllowClose = true;
                frm_cargando.Close();
                return;
            }
            btnAgregarExpediente.Enabled = true;
            txtDui.Text = this.Candidato.Dui;
            picCandidato.LoadAsync(Properties.Settings.Default.DireccionImagenes + this.Candidato.Codigo + ".jpg");
            txtNombre.Text = this.Candidato.Nombre;
            txtApellido.Text = this.Candidato.Apellido;
            txtTelefono.Text = this.Candidato.Telefono;
            txtDepartamento.Text = this.Candidato.Departamento.Nombre;
            txtMunicipio.Text = this.Candidato.Municipio.Nombre;
            cmbGenero.SelectedIndex = (int)this.Candidato.Genero;
            cmbEdad.SelectedIndex = (int)this.Candidato.Edad - 1;
            ServicioQuickHum.Escolaridad escolaridad = (from ServicioQuickHum.Escolaridad a in cmbEscolaridad.Properties.Items.Cast<ServicioQuickHum.Escolaridad>()
                                                        where a.Id == this.Candidato.Escolaridad.Id &&
                                                            a.Nombre == this.Candidato.Escolaridad.Nombre
                                                        select a).SingleOrDefault();
            cmbEscolaridad.SelectedItem = escolaridad;
            cmbEstadoCivil.SelectedItem = (from string estado_civil in cmbEstadoCivil.Properties.Items
                where estado_civil == this.Candidato.EstadoCivil
                select estado_civil).SingleOrDefault();
            GridExpedientes.DataSource = this.Expedientes;
            ((GridView)GridExpedientes.MainView).Columns["Estado"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            ((GridView)GridExpedientes.MainView).Columns["Fecha"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            ((GridView)GridExpedientes.MainView).Columns["Fecha"].DisplayFormat.FormatString = "g";
            ActualizarDatosPestania();
            frm_cargando.AllowClose = true;
            frm_cargando.Close();
        }

        /// <summary>
        /// Recupera la informacion de un candidato
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void RecuperarInformacionCandidato(object sender, DoWorkEventArgs e)
        {
            BOLCandidato negocio_candidato = new BOLCandidato();
            BOLExpediente negocio_expediente = new BOLExpediente();
            try
            {
                this.Candidato = negocio_candidato.CargarCandidato((string) e.Argument);
                this.Expedientes = negocio_expediente.ListarExpedientes(this.Candidato.Dui);
                e.Result = true;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un error en el proceso: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Result = false;
                return;
            }
        }
    }
}
