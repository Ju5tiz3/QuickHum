﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmIngresoCandidato : DevExpress.XtraEditors.XtraForm
    {
        public string Dui { get; set; }
        public string Password { get; set; }

        public frmIngresoCandidato()
        {
            InitializeComponent();
            this.DataBindings.Add(new Binding("Dui", this.txtDui, "Text"));
            this.DataBindings.Add(new Binding("Password", this.txtPassword, "Text"));
        }

    }
}