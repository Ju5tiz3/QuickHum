﻿namespace QuickHumCliente.Vistas
{
    partial class frmDashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 50D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 50D);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDashBoard));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupFrame1 = new GroupFrameControl.GroupFrame();
            this.lblExpedientesFinalizados = new System.Windows.Forms.Label();
            this.lblExpedientesAnulados = new System.Windows.Forms.Label();
            this.lblExpedientesActivos = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupFrame2 = new GroupFrameControl.GroupFrame();
            this.chartGenero = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblPorcentajeFemenino = new DevExpress.XtraEditors.LabelControl();
            this.lblPorcentajeMasculino = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.circuloTotal = new CircleInfoControl.CircleInfo();
            this.circuloPersonalidad = new CircleInfoControl.CircleInfo();
            this.circuloInteligencia = new CircleInfoControl.CircleInfo();
            this.circuloComportamiento = new CircleInfoControl.CircleInfo();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.groupFrame1.SuspendLayout();
            this.groupFrame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(53)))), ((int)(((byte)(54)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(792, 54);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(792, 54);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "DashBoard";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Roboto Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(72)))), ((int)(((byte)(74)))));
            this.labelControl2.Location = new System.Drawing.Point(15, 86);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(131, 21);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Resumen mensual";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(13, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(767, 2);
            this.label1.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.labelControl3.Location = new System.Drawing.Point(55, 281);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(101, 15);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "COMPORTAMIENTO";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.labelControl4.Location = new System.Drawing.Point(262, 281);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(71, 15);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "INTELIGENCIA";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.labelControl5.Location = new System.Drawing.Point(450, 281);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(80, 15);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "PERSONALIDAD";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.labelControl6.Location = new System.Drawing.Point(666, 281);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(33, 15);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "TOTAL";
            // 
            // groupFrame1
            // 
            this.groupFrame1.BackColor = System.Drawing.Color.White;
            this.groupFrame1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.groupFrame1.Controls.Add(this.lblExpedientesFinalizados);
            this.groupFrame1.Controls.Add(this.lblExpedientesAnulados);
            this.groupFrame1.Controls.Add(this.lblExpedientesActivos);
            this.groupFrame1.Controls.Add(this.label4);
            this.groupFrame1.Controls.Add(this.label3);
            this.groupFrame1.Controls.Add(this.label2);
            this.groupFrame1.Location = new System.Drawing.Point(16, 335);
            this.groupFrame1.Name = "groupFrame1";
            this.groupFrame1.Padding = new System.Windows.Forms.Padding(1);
            this.groupFrame1.Size = new System.Drawing.Size(355, 226);
            this.groupFrame1.TabIndex = 11;
            this.groupFrame1.TitleFrameColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.groupFrame1.TitleText = "Actividad Reciente";
            // 
            // lblExpedientesFinalizados
            // 
            this.lblExpedientesFinalizados.AutoSize = true;
            this.lblExpedientesFinalizados.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpedientesFinalizados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.lblExpedientesFinalizados.Location = new System.Drawing.Point(169, 129);
            this.lblExpedientesFinalizados.Name = "lblExpedientesFinalizados";
            this.lblExpedientesFinalizados.Size = new System.Drawing.Size(15, 16);
            this.lblExpedientesFinalizados.TabIndex = 6;
            this.lblExpedientesFinalizados.Text = "0";
            // 
            // lblExpedientesAnulados
            // 
            this.lblExpedientesAnulados.AutoSize = true;
            this.lblExpedientesAnulados.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpedientesAnulados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.lblExpedientesAnulados.Location = new System.Drawing.Point(169, 99);
            this.lblExpedientesAnulados.Name = "lblExpedientesAnulados";
            this.lblExpedientesAnulados.Size = new System.Drawing.Size(15, 16);
            this.lblExpedientesAnulados.TabIndex = 5;
            this.lblExpedientesAnulados.Text = "0";
            // 
            // lblExpedientesActivos
            // 
            this.lblExpedientesActivos.AutoSize = true;
            this.lblExpedientesActivos.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpedientesActivos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.lblExpedientesActivos.Location = new System.Drawing.Point(169, 69);
            this.lblExpedientesActivos.Name = "lblExpedientesActivos";
            this.lblExpedientesActivos.Size = new System.Drawing.Size(15, 16);
            this.lblExpedientesActivos.TabIndex = 4;
            this.lblExpedientesActivos.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.label4.Location = new System.Drawing.Point(12, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Expedientes Finalizados";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.label3.Location = new System.Drawing.Point(12, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Expedientes Anulados";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(95)))), ((int)(((byte)(99)))));
            this.label2.Location = new System.Drawing.Point(12, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Expedientes Activos";
            // 
            // groupFrame2
            // 
            this.groupFrame2.BackColor = System.Drawing.Color.White;
            this.groupFrame2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.groupFrame2.Controls.Add(this.chartGenero);
            this.groupFrame2.Controls.Add(this.lblPorcentajeFemenino);
            this.groupFrame2.Controls.Add(this.lblPorcentajeMasculino);
            this.groupFrame2.Controls.Add(this.pictureBox2);
            this.groupFrame2.Controls.Add(this.pictureBox1);
            this.groupFrame2.Location = new System.Drawing.Point(414, 335);
            this.groupFrame2.Name = "groupFrame2";
            this.groupFrame2.Padding = new System.Windows.Forms.Padding(1);
            this.groupFrame2.Size = new System.Drawing.Size(355, 226);
            this.groupFrame2.TabIndex = 12;
            this.groupFrame2.TitleFrameColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.groupFrame2.TitleText = "Estadisticas Genero";
            // 
            // chartGenero
            // 
            chartArea1.Name = "ChartArea1";
            this.chartGenero.ChartAreas.Add(chartArea1);
            legend1.IsDockedInsideChartArea = false;
            legend1.Name = "Legend1";
            this.chartGenero.Legends.Add(legend1);
            this.chartGenero.Location = new System.Drawing.Point(101, 55);
            this.chartGenero.Name = "chartGenero";
            this.chartGenero.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chartGenero.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(185)))), ((int)(((byte)(250))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(98)))), ((int)(((byte)(142)))))};
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            dataPoint1.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint1.LegendText = "Masculino";
            dataPoint2.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint2.LegendText = "Femenino";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            this.chartGenero.Series.Add(series1);
            this.chartGenero.Size = new System.Drawing.Size(250, 167);
            this.chartGenero.TabIndex = 21;
            this.chartGenero.Text = "chart1";
            // 
            // lblPorcentajeFemenino
            // 
            this.lblPorcentajeFemenino.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPorcentajeFemenino.Appearance.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentajeFemenino.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.lblPorcentajeFemenino.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPorcentajeFemenino.Location = new System.Drawing.Point(48, 98);
            this.lblPorcentajeFemenino.Name = "lblPorcentajeFemenino";
            this.lblPorcentajeFemenino.Size = new System.Drawing.Size(48, 25);
            this.lblPorcentajeFemenino.TabIndex = 20;
            this.lblPorcentajeFemenino.Text = "50%";
            // 
            // lblPorcentajeMasculino
            // 
            this.lblPorcentajeMasculino.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPorcentajeMasculino.Appearance.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentajeMasculino.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.lblPorcentajeMasculino.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPorcentajeMasculino.Location = new System.Drawing.Point(48, 69);
            this.lblPorcentajeMasculino.Name = "lblPorcentajeMasculino";
            this.lblPorcentajeMasculino.Size = new System.Drawing.Size(48, 25);
            this.lblPorcentajeMasculino.TabIndex = 19;
            this.lblPorcentajeMasculino.Text = "50%";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = global::QuickHumCliente.Properties.Resources.female_icon;
            this.pictureBox2.Location = new System.Drawing.Point(11, 100);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(23, 23);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::QuickHumCliente.Properties.Resources.male_icon;
            this.pictureBox1.Location = new System.Drawing.Point(11, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // circuloTotal
            // 
            this.circuloTotal.BackColor = System.Drawing.Color.Transparent;
            this.circuloTotal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("circuloTotal.BackgroundImage")));
            this.circuloTotal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.circuloTotal.CircleColor = CircleInfoControl.CircleInfo.Color.Orange;
            this.circuloTotal.Counter = ((short)(0));
            this.circuloTotal.CounterColorText = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(254)))));
            this.circuloTotal.Location = new System.Drawing.Point(607, 125);
            this.circuloTotal.Name = "circuloTotal";
            this.circuloTotal.ShowAnnotation = false;
            this.circuloTotal.Size = new System.Drawing.Size(150, 150);
            this.circuloTotal.TabIndex = 6;
            // 
            // circuloPersonalidad
            // 
            this.circuloPersonalidad.BackColor = System.Drawing.Color.Transparent;
            this.circuloPersonalidad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("circuloPersonalidad.BackgroundImage")));
            this.circuloPersonalidad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.circuloPersonalidad.CircleColor = CircleInfoControl.CircleInfo.Color.Gray;
            this.circuloPersonalidad.Counter = ((short)(0));
            this.circuloPersonalidad.CounterColorText = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.circuloPersonalidad.Location = new System.Drawing.Point(414, 125);
            this.circuloPersonalidad.Name = "circuloPersonalidad";
            this.circuloPersonalidad.ShowAnnotation = true;
            this.circuloPersonalidad.Size = new System.Drawing.Size(150, 150);
            this.circuloPersonalidad.TabIndex = 5;
            // 
            // circuloInteligencia
            // 
            this.circuloInteligencia.BackColor = System.Drawing.Color.Transparent;
            this.circuloInteligencia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("circuloInteligencia.BackgroundImage")));
            this.circuloInteligencia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.circuloInteligencia.CircleColor = CircleInfoControl.CircleInfo.Color.Gray;
            this.circuloInteligencia.Counter = ((short)(0));
            this.circuloInteligencia.CounterColorText = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.circuloInteligencia.Location = new System.Drawing.Point(221, 125);
            this.circuloInteligencia.Name = "circuloInteligencia";
            this.circuloInteligencia.ShowAnnotation = true;
            this.circuloInteligencia.Size = new System.Drawing.Size(150, 150);
            this.circuloInteligencia.TabIndex = 4;
            // 
            // circuloComportamiento
            // 
            this.circuloComportamiento.BackColor = System.Drawing.Color.Transparent;
            this.circuloComportamiento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("circuloComportamiento.BackgroundImage")));
            this.circuloComportamiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.circuloComportamiento.CircleColor = CircleInfoControl.CircleInfo.Color.Gray;
            this.circuloComportamiento.Counter = ((short)(0));
            this.circuloComportamiento.CounterColorText = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.circuloComportamiento.Location = new System.Drawing.Point(28, 125);
            this.circuloComportamiento.Name = "circuloComportamiento";
            this.circuloComportamiento.ShowAnnotation = true;
            this.circuloComportamiento.Size = new System.Drawing.Size(150, 150);
            this.circuloComportamiento.TabIndex = 3;
            // 
            // frmDashBoard
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.groupFrame2);
            this.Controls.Add(this.groupFrame1);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.circuloTotal);
            this.Controls.Add(this.circuloPersonalidad);
            this.Controls.Add(this.circuloInteligencia);
            this.Controls.Add(this.circuloComportamiento);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDashBoard";
            this.Text = "DashBoard";
            this.Load += new System.EventHandler(this.frmDashBoard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.groupFrame1.ResumeLayout(false);
            this.groupFrame1.PerformLayout();
            this.groupFrame2.ResumeLayout(false);
            this.groupFrame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartGenero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Label label1;
        private CircleInfoControl.CircleInfo circuloComportamiento;
        private CircleInfoControl.CircleInfo circuloInteligencia;
        private CircleInfoControl.CircleInfo circuloPersonalidad;
        private CircleInfoControl.CircleInfo circuloTotal;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private GroupFrameControl.GroupFrame groupFrame1;
        private GroupFrameControl.GroupFrame groupFrame2;
        private DevExpress.XtraEditors.LabelControl lblPorcentajeFemenino;
        private DevExpress.XtraEditors.LabelControl lblPorcentajeMasculino;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartGenero;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblExpedientesFinalizados;
        private System.Windows.Forms.Label lblExpedientesAnulados;
        private System.Windows.Forms.Label lblExpedientesActivos;
    }
}