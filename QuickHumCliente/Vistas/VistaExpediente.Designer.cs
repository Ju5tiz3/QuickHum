﻿namespace QuickHumCliente.Vistas
{
    partial class VistaExpediente
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule5 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule6 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule7 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule8 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.tstripMenu = new System.Windows.Forms.ToolStrip();
            this.tstNuevo = new System.Windows.Forms.ToolStripButton();
            this.tstGuardar = new System.Windows.Forms.ToolStripButton();
            this.tstBuscar = new System.Windows.Forms.ToolStripButton();
            this.tstAgregarPruebas = new System.Windows.Forms.ToolStripButton();
            this.tstAnotacion = new System.Windows.Forms.ToolStripButton();
            this.tstCambiarEstadoExpediente = new System.Windows.Forms.ToolStripDropDownButton();
            this.tstbtnAnulado = new System.Windows.Forms.ToolStripMenuItem();
            this.tstbtnCerrado = new System.Windows.Forms.ToolStripMenuItem();
            this.tstImprimirReporte = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnMostrarOcupaciones = new DevExpress.XtraEditors.SimpleButton();
            this.btnMostrarCandidato = new DevExpress.XtraEditors.SimpleButton();
            this.GridPruebasAsignadas = new DevExpress.XtraGrid.GridControl();
            this.VistaPrincipal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Prueba = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EscolaridadPrueba = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Resultado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.botones_resultado = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtApellido = new DevExpress.XtraEditors.TextEdit();
            this.txtNombre = new DevExpress.XtraEditors.TextEdit();
            this.txtDui = new DevExpress.XtraEditors.TextEdit();
            this.txtPuesto = new DevExpress.XtraEditors.TextEdit();
            this.txtOcupacion = new DevExpress.XtraEditors.TextEdit();
            this.txtEstado = new DevExpress.XtraEditors.TextEdit();
            this.txtOficina = new DevExpress.XtraEditors.TextEdit();
            this.txtEvaluador = new DevExpress.XtraEditors.TextEdit();
            this.txtNumero = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem3 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.dxValidador = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            this.tstripMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPruebasAsignadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.botones_resultado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDui.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuesto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOcupacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOficina.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEvaluador.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidador)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(1024, 48);
            this.ContentedorTituloPestana.TabIndex = 2;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(92, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "Expediente";
            // 
            // tstripMenu
            // 
            this.tstripMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.tstripMenu.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tstripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tstripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstNuevo,
            this.tstGuardar,
            this.tstBuscar,
            this.tstAgregarPruebas,
            this.tstAnotacion,
            this.tstCambiarEstadoExpediente,
            this.tstImprimirReporte,
            this.btnEliminar});
            this.tstripMenu.Location = new System.Drawing.Point(0, 48);
            this.tstripMenu.Name = "tstripMenu";
            this.tstripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tstripMenu.Size = new System.Drawing.Size(1024, 39);
            this.tstripMenu.TabIndex = 3;
            this.tstripMenu.Text = "toolStrip2";
            // 
            // tstNuevo
            // 
            this.tstNuevo.ForeColor = System.Drawing.Color.White;
            this.tstNuevo.Image = global::QuickHumCliente.Properties.Resources.new_page;
            this.tstNuevo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstNuevo.Name = "tstNuevo";
            this.tstNuevo.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tstNuevo.Size = new System.Drawing.Size(103, 36);
            this.tstNuevo.Text = "  Nuevo";
            this.tstNuevo.Click += new System.EventHandler(this.tstNuevo_Click);
            // 
            // tstGuardar
            // 
            this.tstGuardar.ForeColor = System.Drawing.Color.White;
            this.tstGuardar.Image = global::QuickHumCliente.Properties.Resources.save;
            this.tstGuardar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstGuardar.Name = "tstGuardar";
            this.tstGuardar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tstGuardar.Size = new System.Drawing.Size(111, 36);
            this.tstGuardar.Text = "  Guardar";
            this.tstGuardar.Click += new System.EventHandler(this.tstGuardar_Click);
            // 
            // tstBuscar
            // 
            this.tstBuscar.ForeColor = System.Drawing.Color.White;
            this.tstBuscar.Image = global::QuickHumCliente.Properties.Resources.buscar2;
            this.tstBuscar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstBuscar.Name = "tstBuscar";
            this.tstBuscar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tstBuscar.Size = new System.Drawing.Size(103, 36);
            this.tstBuscar.Text = "  Buscar";
            this.tstBuscar.Click += new System.EventHandler(this.tstBuscar_Click);
            // 
            // tstAgregarPruebas
            // 
            this.tstAgregarPruebas.ForeColor = System.Drawing.Color.White;
            this.tstAgregarPruebas.Image = global::QuickHumCliente.Properties.Resources.prueba2;
            this.tstAgregarPruebas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstAgregarPruebas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstAgregarPruebas.Name = "tstAgregarPruebas";
            this.tstAgregarPruebas.Size = new System.Drawing.Size(132, 36);
            this.tstAgregarPruebas.Text = "  Agregar Pruebas";
            this.tstAgregarPruebas.Click += new System.EventHandler(this.btnAgregarPruebas_Click);
            // 
            // tstAnotacion
            // 
            this.tstAnotacion.ForeColor = System.Drawing.Color.White;
            this.tstAnotacion.Image = global::QuickHumCliente.Properties.Resources.anotacion;
            this.tstAnotacion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstAnotacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstAnotacion.Name = "tstAnotacion";
            this.tstAnotacion.Size = new System.Drawing.Size(113, 36);
            this.tstAnotacion.Text = "  Anotaciones";
            this.tstAnotacion.Click += new System.EventHandler(this.tstAnotacion_Click);
            // 
            // tstCambiarEstadoExpediente
            // 
            this.tstCambiarEstadoExpediente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstbtnAnulado,
            this.tstbtnCerrado});
            this.tstCambiarEstadoExpediente.Enabled = false;
            this.tstCambiarEstadoExpediente.ForeColor = System.Drawing.Color.White;
            this.tstCambiarEstadoExpediente.Image = global::QuickHumCliente.Properties.Resources.editar1;
            this.tstCambiarEstadoExpediente.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstCambiarEstadoExpediente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstCambiarEstadoExpediente.Name = "tstCambiarEstadoExpediente";
            this.tstCambiarEstadoExpediente.Size = new System.Drawing.Size(139, 36);
            this.tstCambiarEstadoExpediente.Text = "  Cambiar Estado";
            // 
            // tstbtnAnulado
            // 
            this.tstbtnAnulado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.tstbtnAnulado.ForeColor = System.Drawing.Color.White;
            this.tstbtnAnulado.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstbtnAnulado.Name = "tstbtnAnulado";
            this.tstbtnAnulado.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.tstbtnAnulado.Size = new System.Drawing.Size(118, 30);
            this.tstbtnAnulado.Text = "Anulado";
            this.tstbtnAnulado.Click += new System.EventHandler(this.CambiarEstadoExpediente);
            // 
            // tstbtnCerrado
            // 
            this.tstbtnCerrado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.tstbtnCerrado.ForeColor = System.Drawing.Color.White;
            this.tstbtnCerrado.Name = "tstbtnCerrado";
            this.tstbtnCerrado.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.tstbtnCerrado.Size = new System.Drawing.Size(118, 30);
            this.tstbtnCerrado.Text = "Cerrado";
            this.tstbtnCerrado.Click += new System.EventHandler(this.CambiarEstadoExpediente);
            // 
            // tstImprimirReporte
            // 
            this.tstImprimirReporte.ForeColor = System.Drawing.Color.White;
            this.tstImprimirReporte.Image = global::QuickHumCliente.Properties.Resources.print;
            this.tstImprimirReporte.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstImprimirReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstImprimirReporte.Name = "tstImprimirReporte";
            this.tstImprimirReporte.Size = new System.Drawing.Size(87, 36);
            this.tstImprimirReporte.Text = "Imprimir";
            this.tstImprimirReporte.Click += new System.EventHandler(this.tstImprimirReporte_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Enabled = false;
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Image = global::QuickHumCliente.Properties.Resources.delete;
            this.btnEliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnEliminar.Size = new System.Drawing.Size(111, 36);
            this.btnEliminar.Text = "  Eliminar";
            this.btnEliminar.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnMostrarOcupaciones);
            this.layoutControl1.Controls.Add(this.btnMostrarCandidato);
            this.layoutControl1.Controls.Add(this.GridPruebasAsignadas);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.txtApellido);
            this.layoutControl1.Controls.Add(this.txtNombre);
            this.layoutControl1.Controls.Add(this.txtDui);
            this.layoutControl1.Controls.Add(this.txtPuesto);
            this.layoutControl1.Controls.Add(this.txtOcupacion);
            this.layoutControl1.Controls.Add(this.txtEstado);
            this.layoutControl1.Controls.Add(this.txtOficina);
            this.layoutControl1.Controls.Add(this.txtEvaluador);
            this.layoutControl1.Controls.Add(this.txtNumero);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 87);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(941, 210, 820, 335);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1024, 513);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnMostrarOcupaciones
            // 
            this.btnMostrarOcupaciones.Image = global::QuickHumCliente.Properties.Resources.buscar_24;
            this.btnMostrarOcupaciones.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMostrarOcupaciones.Location = new System.Drawing.Point(446, 114);
            this.btnMostrarOcupaciones.MinimumSize = new System.Drawing.Size(0, 32);
            this.btnMostrarOcupaciones.Name = "btnMostrarOcupaciones";
            this.btnMostrarOcupaciones.Size = new System.Drawing.Size(54, 32);
            this.btnMostrarOcupaciones.StyleController = this.layoutControl1;
            this.btnMostrarOcupaciones.TabIndex = 35;
            this.btnMostrarOcupaciones.Click += new System.EventHandler(this.btnMostrarOcupaciones_Click);
            // 
            // btnMostrarCandidato
            // 
            this.btnMostrarCandidato.Image = global::QuickHumCliente.Properties.Resources.buscar_24;
            this.btnMostrarCandidato.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMostrarCandidato.Location = new System.Drawing.Point(525, 186);
            this.btnMostrarCandidato.MinimumSize = new System.Drawing.Size(0, 32);
            this.btnMostrarCandidato.Name = "btnMostrarCandidato";
            this.btnMostrarCandidato.Size = new System.Drawing.Size(43, 32);
            this.btnMostrarCandidato.StyleController = this.layoutControl1;
            this.btnMostrarCandidato.TabIndex = 34;
            this.btnMostrarCandidato.Click += new System.EventHandler(this.btnMostrarCandidato_Click);
            // 
            // GridPruebasAsignadas
            // 
            this.GridPruebasAsignadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridPruebasAsignadas.Location = new System.Drawing.Point(12, 290);
            this.GridPruebasAsignadas.MainView = this.VistaPrincipal;
            this.GridPruebasAsignadas.Name = "GridPruebasAsignadas";
            this.GridPruebasAsignadas.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.botones_resultado});
            this.GridPruebasAsignadas.Size = new System.Drawing.Size(1000, 211);
            this.GridPruebasAsignadas.TabIndex = 33;
            this.GridPruebasAsignadas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VistaPrincipal});
            // 
            // VistaPrincipal
            // 
            this.VistaPrincipal.Appearance.HeaderPanel.Font = new System.Drawing.Font("Open Sans", 9.75F);
            this.VistaPrincipal.Appearance.HeaderPanel.Options.UseFont = true;
            this.VistaPrincipal.Appearance.Row.Font = new System.Drawing.Font("Open Sans", 8.25F);
            this.VistaPrincipal.Appearance.Row.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.VistaPrincipal.Appearance.Row.Options.UseFont = true;
            this.VistaPrincipal.Appearance.Row.Options.UseForeColor = true;
            this.VistaPrincipal.ColumnPanelRowHeight = 36;
            this.VistaPrincipal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Prueba,
            this.EscolaridadPrueba,
            this.Resultado});
            this.VistaPrincipal.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.VistaPrincipal.GridControl = this.GridPruebasAsignadas;
            this.VistaPrincipal.Name = "VistaPrincipal";
            this.VistaPrincipal.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsCustomization.AllowColumnMoving = false;
            this.VistaPrincipal.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.VistaPrincipal.OptionsView.AllowHtmlDrawGroups = false;
            this.VistaPrincipal.OptionsView.EnableAppearanceEvenRow = true;
            this.VistaPrincipal.OptionsView.ShowDetailButtons = false;
            this.VistaPrincipal.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.VistaPrincipal.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VistaPrincipal.OptionsView.ShowGroupPanel = false;
            this.VistaPrincipal.RowHeight = 32;
            // 
            // Prueba
            // 
            this.Prueba.Caption = "Prueba";
            this.Prueba.FieldName = "TipoPrueba";
            this.Prueba.Image = global::QuickHumCliente.Properties.Resources.pruebaicon;
            this.Prueba.Name = "Prueba";
            this.Prueba.OptionsColumn.AllowEdit = false;
            this.Prueba.Visible = true;
            this.Prueba.VisibleIndex = 0;
            this.Prueba.Width = 327;
            // 
            // EscolaridadPrueba
            // 
            this.EscolaridadPrueba.Caption = "Escolaridad";
            this.EscolaridadPrueba.FieldName = "Escolaridad";
            this.EscolaridadPrueba.Image = global::QuickHumCliente.Properties.Resources.escolaridad;
            this.EscolaridadPrueba.Name = "EscolaridadPrueba";
            this.EscolaridadPrueba.OptionsColumn.AllowEdit = false;
            this.EscolaridadPrueba.Visible = true;
            this.EscolaridadPrueba.VisibleIndex = 1;
            this.EscolaridadPrueba.Width = 286;
            // 
            // Resultado
            // 
            this.Resultado.Caption = "  Resultado";
            this.Resultado.ColumnEdit = this.botones_resultado;
            this.Resultado.FieldName = "Resultado";
            this.Resultado.Image = global::QuickHumCliente.Properties.Resources.abaco;
            this.Resultado.Name = "Resultado";
            this.Resultado.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Resultado.Visible = true;
            this.Resultado.VisibleIndex = 2;
            this.Resultado.Width = 83;
            // 
            // botones_resultado
            // 
            this.botones_resultado.AutoHeight = false;
            this.botones_resultado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::QuickHumCliente.Properties.Resources.prueba1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.botones_resultado.Name = "botones_resultado";
            this.botones_resultado.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.botones_resultado.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.botones_resultado_ButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.labelControl1.Location = new System.Drawing.Point(12, 260);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(153, 22);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Pruebas Asignadas";
            // 
            // txtApellido
            // 
            this.txtApellido.Enabled = false;
            this.txtApellido.Location = new System.Drawing.Point(694, 224);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtApellido.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtApellido.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtApellido.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtApellido.Properties.Appearance.Options.UseBorderColor = true;
            this.txtApellido.Properties.Appearance.Options.UseFont = true;
            this.txtApellido.Properties.Appearance.Options.UseForeColor = true;
            this.txtApellido.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtApellido.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtApellido.Properties.AutoHeight = false;
            this.txtApellido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtApellido.Size = new System.Drawing.Size(318, 28);
            this.txtApellido.StyleController = this.layoutControl1;
            this.txtApellido.TabIndex = 7;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtApellido, conditionValidationRule1);
            // 
            // txtNombre
            // 
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(181, 224);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtNombre.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNombre.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtNombre.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtNombre.Properties.Appearance.Options.UseBorderColor = true;
            this.txtNombre.Properties.Appearance.Options.UseFont = true;
            this.txtNombre.Properties.Appearance.Options.UseForeColor = true;
            this.txtNombre.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtNombre.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtNombre.Properties.AutoHeight = false;
            this.txtNombre.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNombre.Size = new System.Drawing.Size(319, 28);
            this.txtNombre.StyleController = this.layoutControl1;
            this.txtNombre.TabIndex = 6;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtNombre, conditionValidationRule2);
            // 
            // txtDui
            // 
            this.txtDui.Enabled = false;
            this.txtDui.Location = new System.Drawing.Point(181, 188);
            this.txtDui.Name = "txtDui";
            this.txtDui.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtDui.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtDui.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtDui.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtDui.Properties.Appearance.Options.UseBackColor = true;
            this.txtDui.Properties.Appearance.Options.UseBorderColor = true;
            this.txtDui.Properties.Appearance.Options.UseFont = true;
            this.txtDui.Properties.Appearance.Options.UseForeColor = true;
            this.txtDui.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtDui.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtDui.Properties.AutoHeight = false;
            this.txtDui.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtDui.Size = new System.Drawing.Size(319, 28);
            this.txtDui.StyleController = this.layoutControl1;
            this.txtDui.TabIndex = 5;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtDui, conditionValidationRule3);
            // 
            // txtPuesto
            // 
            this.txtPuesto.Location = new System.Drawing.Point(694, 116);
            this.txtPuesto.Name = "txtPuesto";
            this.txtPuesto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtPuesto.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtPuesto.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtPuesto.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtPuesto.Properties.Appearance.Options.UseBackColor = true;
            this.txtPuesto.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPuesto.Properties.Appearance.Options.UseFont = true;
            this.txtPuesto.Properties.Appearance.Options.UseForeColor = true;
            this.txtPuesto.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtPuesto.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtPuesto.Properties.AutoHeight = false;
            this.txtPuesto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtPuesto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPuesto.Properties.Mask.EditMask = "[A-Za-z ]+";
            this.txtPuesto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPuesto.Properties.Mask.ShowPlaceHolders = false;
            this.txtPuesto.Size = new System.Drawing.Size(318, 28);
            this.txtPuesto.StyleController = this.layoutControl1;
            this.txtPuesto.TabIndex = 6;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtPuesto, conditionValidationRule4);
            // 
            // txtOcupacion
            // 
            this.txtOcupacion.Enabled = false;
            this.txtOcupacion.Location = new System.Drawing.Point(181, 116);
            this.txtOcupacion.Name = "txtOcupacion";
            this.txtOcupacion.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtOcupacion.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtOcupacion.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtOcupacion.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtOcupacion.Properties.Appearance.Options.UseBackColor = true;
            this.txtOcupacion.Properties.Appearance.Options.UseBorderColor = true;
            this.txtOcupacion.Properties.Appearance.Options.UseFont = true;
            this.txtOcupacion.Properties.Appearance.Options.UseForeColor = true;
            this.txtOcupacion.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtOcupacion.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtOcupacion.Properties.AutoHeight = false;
            this.txtOcupacion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtOcupacion.Size = new System.Drawing.Size(261, 28);
            this.txtOcupacion.StyleController = this.layoutControl1;
            this.txtOcupacion.TabIndex = 6;
            conditionValidationRule5.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule5.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtOcupacion, conditionValidationRule5);
            // 
            // txtEstado
            // 
            this.txtEstado.Enabled = false;
            this.txtEstado.Location = new System.Drawing.Point(694, 80);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtEstado.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtEstado.Properties.Appearance.Options.UseBackColor = true;
            this.txtEstado.Properties.Appearance.Options.UseBorderColor = true;
            this.txtEstado.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtEstado.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtEstado.Properties.AutoHeight = false;
            this.txtEstado.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtEstado.Size = new System.Drawing.Size(318, 28);
            this.txtEstado.StyleController = this.layoutControl1;
            this.txtEstado.TabIndex = 6;
            conditionValidationRule6.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule6.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtEstado, conditionValidationRule6);
            // 
            // txtOficina
            // 
            this.txtOficina.Enabled = false;
            this.txtOficina.Location = new System.Drawing.Point(181, 80);
            this.txtOficina.Name = "txtOficina";
            this.txtOficina.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtOficina.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtOficina.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtOficina.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtOficina.Properties.Appearance.Options.UseBackColor = true;
            this.txtOficina.Properties.Appearance.Options.UseBorderColor = true;
            this.txtOficina.Properties.Appearance.Options.UseFont = true;
            this.txtOficina.Properties.Appearance.Options.UseForeColor = true;
            this.txtOficina.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtOficina.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtOficina.Properties.AutoHeight = false;
            this.txtOficina.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtOficina.Size = new System.Drawing.Size(319, 28);
            this.txtOficina.StyleController = this.layoutControl1;
            this.txtOficina.TabIndex = 5;
            conditionValidationRule7.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule7.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtOficina, conditionValidationRule7);
            // 
            // txtEvaluador
            // 
            this.txtEvaluador.Enabled = false;
            this.txtEvaluador.Location = new System.Drawing.Point(694, 44);
            this.txtEvaluador.Name = "txtEvaluador";
            this.txtEvaluador.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtEvaluador.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtEvaluador.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtEvaluador.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtEvaluador.Properties.Appearance.Options.UseBackColor = true;
            this.txtEvaluador.Properties.Appearance.Options.UseBorderColor = true;
            this.txtEvaluador.Properties.Appearance.Options.UseFont = true;
            this.txtEvaluador.Properties.Appearance.Options.UseForeColor = true;
            this.txtEvaluador.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtEvaluador.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtEvaluador.Properties.AutoHeight = false;
            this.txtEvaluador.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtEvaluador.Size = new System.Drawing.Size(318, 28);
            this.txtEvaluador.StyleController = this.layoutControl1;
            this.txtEvaluador.TabIndex = 5;
            conditionValidationRule8.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule8.ErrorText = "El campo no puede estar vacio";
            this.dxValidador.SetValidationRule(this.txtEvaluador, conditionValidationRule8);
            // 
            // txtNumero
            // 
            this.txtNumero.Enabled = false;
            this.txtNumero.Location = new System.Drawing.Point(181, 44);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtNumero.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNumero.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtNumero.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtNumero.Properties.Appearance.Options.UseBackColor = true;
            this.txtNumero.Properties.Appearance.Options.UseBorderColor = true;
            this.txtNumero.Properties.Appearance.Options.UseFont = true;
            this.txtNumero.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumero.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtNumero.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtNumero.Properties.AutoHeight = false;
            this.txtNumero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNumero.Size = new System.Drawing.Size(319, 28);
            this.txtNumero.StyleController = this.layoutControl1;
            this.txtNumero.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem1,
            this.simpleSeparator1,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.simpleLabelItem3,
            this.simpleSeparator3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem13,
            this.layoutControlItem10,
            this.emptySpaceItem14,
            this.emptySpaceItem11,
            this.layoutControlItem12,
            this.layoutControlItem2,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.simpleSeparator4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1024, 513);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem1.CustomizationFormText = "Informacion General";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 2, 2);
            this.simpleLabelItem1.Size = new System.Drawing.Size(1004, 26);
            this.simpleLabelItem1.Text = "Informacion General";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(166, 22);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 26);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1004, 4);
            this.simpleSeparator1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.txtNumero;
            this.layoutControlItem1.CustomizationFormText = "NUMERO";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(492, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem1.Text = "NUMERO";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(166, 18);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.txtEvaluador;
            this.layoutControlItem3.CustomizationFormText = "EVALUADOR";
            this.layoutControlItem3.Location = new System.Drawing.Point(513, 30);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(491, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem3.Text = "EVALUADOR";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(166, 18);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(492, 30);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(21, 108);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(21, 108);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(21, 108);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.Control = this.txtOficina;
            this.layoutControlItem4.CustomizationFormText = "OFICINA";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(492, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem4.Text = "OFICINA";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(166, 18);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.Control = this.txtEstado;
            this.layoutControlItem5.CustomizationFormText = "ESTADO";
            this.layoutControlItem5.Location = new System.Drawing.Point(513, 66);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(491, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem5.Text = "ESTADO";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(166, 18);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.Control = this.txtOcupacion;
            this.layoutControlItem6.CustomizationFormText = "OCUPACION";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(434, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem6.Text = "OCUPACION";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(166, 18);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.txtPuesto;
            this.layoutControlItem7.CustomizationFormText = "PUESTO";
            this.layoutControlItem7.Location = new System.Drawing.Point(513, 102);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(491, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem7.Text = "PUESTO A APLICAR";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(166, 18);
            // 
            // simpleLabelItem3
            // 
            this.simpleLabelItem3.AllowHotTrack = false;
            this.simpleLabelItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem3.CustomizationFormText = "Candidato";
            this.simpleLabelItem3.Location = new System.Drawing.Point(0, 138);
            this.simpleLabelItem3.MaxSize = new System.Drawing.Size(0, 32);
            this.simpleLabelItem3.MinSize = new System.Drawing.Size(173, 32);
            this.simpleLabelItem3.Name = "simpleLabelItem3";
            this.simpleLabelItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 2, 2);
            this.simpleLabelItem3.Size = new System.Drawing.Size(1004, 32);
            this.simpleLabelItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 0);
            this.simpleLabelItem3.Text = "Candidato";
            this.simpleLabelItem3.TextSize = new System.Drawing.Size(166, 22);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 170);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(1004, 4);
            this.simpleSeparator3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
            this.simpleSeparator3.Text = "simpleSeparator3";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.Control = this.txtDui;
            this.layoutControlItem8.CustomizationFormText = "DUI";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(492, 36);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem8.Text = "DUI";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(166, 18);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.Control = this.txtNombre;
            this.layoutControlItem9.CustomizationFormText = "NOMBRE";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(492, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem9.Text = "NOMBRE";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(166, 18);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(492, 210);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(21, 36);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(21, 36);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(21, 36);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.Text = "emptySpaceItem13";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.Control = this.txtApellido;
            this.layoutControlItem10.CustomizationFormText = "APELLIDO";
            this.layoutControlItem10.Location = new System.Drawing.Point(513, 210);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(223, 36);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(491, 36);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem10.Text = "APELLIDO";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(166, 18);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(492, 174);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(21, 36);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(21, 36);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(21, 36);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.Text = "emptySpaceItem14";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(560, 174);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 36);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 36);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(444, 36);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.labelControl1;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 246);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(155, 30);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(155, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 0, 2, 2);
            this.layoutControlItem12.Size = new System.Drawing.Size(1004, 30);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnMostrarCandidato;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(513, 174);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(47, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(47, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(47, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnMostrarOcupaciones;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(434, 102);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(58, 36);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.GridPruebasAsignadas;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 278);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(104, 150);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(1004, 215);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.CustomizationFormText = "simpleSeparator4";
            this.simpleSeparator4.Location = new System.Drawing.Point(0, 276);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(1004, 2);
            this.simpleSeparator4.Text = "simpleSeparator4";
            // 
            // VistaExpediente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.tstripMenu);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.Name = "VistaExpediente";
            this.Size = new System.Drawing.Size(1024, 600);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            this.tstripMenu.ResumeLayout(false);
            this.tstripMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPruebasAsignadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.botones_resultado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDui.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuesto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOcupacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOficina.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEvaluador.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private System.Windows.Forms.ToolStrip tstripMenu;
        private System.Windows.Forms.ToolStripButton tstGuardar;
        private System.Windows.Forms.ToolStripButton tstBuscar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton tstAgregarPruebas;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl GridPruebasAsignadas;
        private DevExpress.XtraGrid.Views.Grid.GridView VistaPrincipal;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtApellido;
        private DevExpress.XtraEditors.TextEdit txtNombre;
        private DevExpress.XtraEditors.TextEdit txtDui;
        private DevExpress.XtraEditors.TextEdit txtPuesto;
        private DevExpress.XtraEditors.TextEdit txtOcupacion;
        private DevExpress.XtraEditors.TextEdit txtEstado;
        private DevExpress.XtraEditors.TextEdit txtOficina;
        private DevExpress.XtraEditors.TextEdit txtEvaluador;
        private DevExpress.XtraEditors.TextEdit txtNumero;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton btnMostrarCandidato;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnMostrarOcupaciones;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private System.Windows.Forms.ToolStripButton tstNuevo;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidador;
        private DevExpress.XtraGrid.Columns.GridColumn Prueba;
        private DevExpress.XtraGrid.Columns.GridColumn EscolaridadPrueba;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit botones_resultado;
        private DevExpress.XtraGrid.Columns.GridColumn Resultado;
        private System.Windows.Forms.ToolStripButton tstAnotacion;
        private System.Windows.Forms.ToolStripDropDownButton tstCambiarEstadoExpediente;
        private System.Windows.Forms.ToolStripMenuItem tstbtnAnulado;
        private System.Windows.Forms.ToolStripMenuItem tstbtnCerrado;
        private System.Windows.Forms.ToolStripButton tstImprimirReporte;

    }
}
