﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.XtraGrid.Views.Grid;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;


namespace QuickHumCliente.Vistas
{
    public partial class frmEscolaridad : XtraForm
    {
        public Escolaridad Escolaridad { get; set; }
        private Escolaridad[] _escolaridades = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public frmEscolaridad()
        {
            InitializeComponent();
            InicializarEscolaridades();
            this.Escolaridad = new Escolaridad();
        }

        /// <summary>
        /// Lista las escolaridades y las muestra en el grid
        /// </summary>
        private void InicializarEscolaridades()
        {
            BOLEscolaridad bol_escolaridad = new BOLEscolaridad();
            try
            {
                this._escolaridades = bol_escolaridad.ListarEscolaridades();
                gridEscolaridad.DataSource = this._escolaridades;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Hubo un problema al cargar las escolaridades: " + ex.Message,
                    "Problemas al cargar escolaridades", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }


        /********************** EVENTOS *************************/

        /// <summary>
        /// Almacena una nueva escolaridad </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstNuevaEscolaridad_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Esta seguro de crear una nueva escolaridad: " + this.txtEscolaridad.Text,
                    "Registrar Escolaridad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            this.txtEscolaridad.Enabled = true;
            BOLEscolaridad bol_escolaridad = new BOLEscolaridad();
            this.Escolaridad.Nombre = txtEscolaridad.Text;
            try
            {
                bol_escolaridad.NuevaEscolaridad(this.Escolaridad);
                XtraMessageBox.Show("La escolaridad se registro con exito.", "Escolaridad ingresada",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();}
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en el guardado de la escolaridad: " + ex.Message,
                    "Problema en guardado escolaridad", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en el guardado de la escolaridad: " + ex.Message,
                    "Problema en guardado escolaridad", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Selecciona una escolaridad del grid y guarda su valor en la propiedad escolaridad de la forma
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridEscolaridad_DoubleClick(object sender, EventArgs e)
        {
            GridView vista = (GridView) gridEscolaridad.MainView;
            if (vista.GetSelectedRows().Length == 0)
                return;
            this.Escolaridad = (Escolaridad)vista.GetRow(vista.GetSelectedRows()[0]);
            this.txtEscolaridad.Text = this.Escolaridad.Nombre;
        }

        /// <summary>
        /// Actualiza una escolaridad </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtActualizarEscolaridad_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Esta seguro de actualizar la escolaridad: " + this.txtEscolaridad.Text,
                "Actualizar Escolaridad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            this.Escolaridad.Nombre = this.txtEscolaridad.Text;
            BOLEscolaridad bol_escolaridad = new BOLEscolaridad();
            try
            {
                bol_escolaridad.ActualizarEscolaridad(this.Escolaridad);
                XtraMessageBox.Show("La escolaridad se actualizo con exito.", "Escolaridad actualizada",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la actualizacion de la escolaridad: " + ex.Message,
                    "Problema en actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la actualizacion de la escolaridad" + ex.Message,
                    "Error en actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Elimina una escolaridad seleccionada en el grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEliminar_Click(object sender, EventArgs e){
            GridView vista = (GridView)gridEscolaridad.MainView;
            if (this.Escolaridad.Nombre == null || vista.SelectedRowsCount == 0){
                XtraMessageBox.Show("No has seleccionado ninguna escolaridad", "Escolaridad vacia", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
            if (XtraMessageBox.Show("Esta seguro de eliminar la escolaridad: " + this.txtEscolaridad.Text,
                "Eliminar Escolaridad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            BOLEscolaridad bol_ecolaridad = new BOLEscolaridad();
            try
            {
                bol_ecolaridad.EliminarEscolaridad(this.Escolaridad);
                XtraMessageBox.Show("Escolaridad eliminada con exito.", "Eliminacion de escolaridad",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la eliminacion de la escolaridad: " + ex.Message,
                    "Problema en eliminacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la eliminacion de la escolaridad" + ex.Message,
                    "Problema en eliminacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }
    }
}
