﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.ServiceModel;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Datos;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmAccesosTemporales : DevExpress.XtraEditors.XtraForm
    {

        private Candidato _candidato = null;

        public frmAccesosTemporales()
        {
            InitializeComponent();
        }

        /*************** EVENTOS **************/

        private void btnBuscarCandidato_Click(object sender, EventArgs e)
        {
            Vistas.frmBuscarCandidato vista_buscar_candidato = new frmBuscarCandidato();
            if (vista_buscar_candidato.ShowDialog() != DialogResult.OK)
                return;
            if (vista_buscar_candidato.Dui == string.Empty)
                throw new ArgumentException("Dui invalido");
            BOLCandidato bol_candidato = new BOLCandidato();
            try
            {
                this._candidato = bol_candidato.CargarCandidato(vista_buscar_candidato.Dui);
                txtDui.Text = this._candidato.Dui;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar el candidato: " + ex.Message, "Problema al cargar candidato" ,MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Genera y almacena un acceso temporal para el candidato
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstGenerarAcceso_Click(object sender, EventArgs e)
        {
            BOLAutenticacion bol_autenticacion = new BOLAutenticacion();
            try{
                string password = bol_autenticacion.RegistrarAccesoTemporal(this._candidato, cmbDuracion.Text);
                this.txtPassword.Text = password;
                XtraMessageBox.Show("Se genero el acceso temporal con exito.", "Acceso temporal generado",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al generar el acceso temporal: " + ex.Message,
                    "Problema generacion acceso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al generar el acceso temporal: " + ex.Message,
                    "Problema generacion acceso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (FormatException ex){
                XtraMessageBox.Show("Ocurrio un problema al generar el acceso temporal: " + ex.Message,
                    "Problema generacion acceso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Depura los accesos inutiles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstDepurarAcceso_Click(object sender, EventArgs e)
        {
            BOLAutenticacion bol_autenticacion = new BOLAutenticacion();
            try
            {
                bol_autenticacion.DepurarAccesosTemporales();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al depurar los accesos temporales: " + ex.Message,
                    "Depuracion de accesos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void tstImprimirAcceso_Click(object sender, EventArgs e)
        {
            PrintDocument print_document = new PrintDocument();
            PrinterSettings print_settings = new PrinterSettings();
            PrintDialog print_dialog = new PrintDialog();
            print_dialog.Document = print_document;
            if (print_dialog.ShowDialog() == DialogResult.Cancel)
                return;
            print_document.PrintPage += print_document_PrintPage;
            print_document.Print();
        }

        void print_document_PrintPage(object sender, PrintPageEventArgs e)
        {
            Font font_documento = new Font("Arial", 24.0f);
            e.Graphics.DrawString(this.txtPassword.Text, font_documento, Brushes.Black, e.MarginBounds.Left, 0, new StringFormat());
        }
    }}