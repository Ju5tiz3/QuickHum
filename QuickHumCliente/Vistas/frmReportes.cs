﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;

namespace QuickHumCliente.Vistas
{
    public partial class frmReportes : DevExpress.XtraEditors.XtraForm
    {
        public frmReportes()
        {
            InitializeComponent();
            dateInicio.DateTime = DateTime.Now;
            dateFin.DateTime = DateTime.Now;
        }

        private void btnGenerarReporte_Click(object sender, EventArgs e)
        {
            try
            {
                new Reportes.frmReporteLocal(dateInicio.DateTime, dateFin.DateTime).ShowDialog();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al generar el reporte: " + ex.Message, "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}