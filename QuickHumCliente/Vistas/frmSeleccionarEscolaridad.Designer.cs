﻿namespace QuickHumCliente.Vistas
{
    partial class frmSeleccionarEscolaridad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbEscolaridades = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEscolaridades.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbEscolaridades
            // 
            this.cmbEscolaridades.Location = new System.Drawing.Point(12, 12);
            this.cmbEscolaridades.Name = "cmbEscolaridades";
            this.cmbEscolaridades.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.cmbEscolaridades.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbEscolaridades.Properties.AutoHeight = false;
            this.cmbEscolaridades.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbEscolaridades.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEscolaridades.Properties.DropDownItemHeight = 32;
            this.cmbEscolaridades.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEscolaridades.Size = new System.Drawing.Size(323, 32);
            this.cmbEscolaridades.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(249, 53);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(86, 27);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Aceptar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // frmSeleccionarEscolaridad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 87);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cmbEscolaridades);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSeleccionarEscolaridad";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Escolaridad";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSeleccionarEscolaridad_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.cmbEscolaridades.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit cmbEscolaridades;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}