﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmSeleccionarNumeroExpediente : DevExpress.XtraEditors.XtraForm
    {
        public int NumeroExpediente { get; set; }

        public frmSeleccionarNumeroExpediente()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (dxValidationProvider1.Validate() == false)
                return;
            this.DialogResult = DialogResult.OK;
            this.NumeroExpediente = int.Parse(txtNumeroExpediente.Text);
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}