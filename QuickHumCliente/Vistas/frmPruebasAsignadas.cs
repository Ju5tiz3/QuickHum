﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.ServiceModel;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;

using DevExpress.XtraGrid.Views.Grid;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;
using QuickHumCliente.Vistas.Pruebas.Allport;
using QuickHumCliente.Vistas.Pruebas.Barsit;
using QuickHumCliente.Vistas.Pruebas.Bfq;
using QuickHumCliente.Vistas.Pruebas.Cleaver;
using QuickHumCliente.Vistas.Pruebas.CPI;
using QuickHumCliente.Vistas.Pruebas.Dominos;
using QuickHumCliente.Vistas.Pruebas.Intelsoft;
using QuickHumCliente.Vistas.Pruebas.IPV;
using QuickHumCliente.Vistas.Pruebas.Kostick;
using QuickHumCliente.Vistas.Pruebas.Lifo;
using QuickHumCliente.Vistas.Pruebas.Luscher;
using QuickHumCliente.Vistas.Pruebas.MMPIA;
using QuickHumCliente.Vistas.Pruebas.MMPIA2;
using QuickHumCliente.Vistas.Pruebas.Moss;
using QuickHumCliente.Vistas.Pruebas.PF16;
using QuickHumCliente.Vistas.Pruebas.PIPG;
using QuickHumCliente.Vistas.Pruebas.Raven;
using QuickHumCliente.Vistas.Pruebas.Terman;
using QuickHumCliente.Vistas.Pruebas.Wonderlic;
using QuickHumCliente.Vistas.Pruebas.Zavic;

namespace QuickHumCliente.Vistas
{
    public partial class frmPruebasAsignadas : DevExpress.XtraEditors.XtraForm
    {
        private bool _pruebas_finalizadas = false;
        public Expediente Expediente { get; set; }
        private readonly Dictionary<Prueba, EscolaridadPrueba> _pruebas_asignadas = new Dictionary<Prueba, EscolaridadPrueba>();
        private DataTable tabla_pruebas = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public frmPruebasAsignadas()
        {
            InitializeComponent();
            try
            {
                this.CabezeraCandidato.Text = Globales.Candidato.Nombre + " " + Globales.Candidato.Apellido;
                this.Expediente = Globales.cliente.CargarExpedienteActivo(Globales.Candidato.Dui);
                //gridPruebas.DataSource = this.Expediente.PruebasAsignadas;
                BOLPrueba bol_prueba = new BOLPrueba();
                //_pruebas = bol_prueba.ListaPruebas();
                ListarPruebasAsignadas();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar el expediente activo del candidato: " + ex.Message, "Carga expediente activo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                return;
            }
        }
       
        /// <summary>
        /// Lista las pruebas y las muestra en un datagrid
        /// </summary>
        private bool ListarPruebasAsignadas()
        {
            tabla_pruebas = new DataTable("PruebasAsignadas");
            tabla_pruebas.Columns.Add("Prueba");
            tabla_pruebas.Columns.Add("Tiempo");
            tabla_pruebas.Columns.Add("Estado");
            //tabla_pruebas.Columns.Add("Estado");
            BOLPrueba bol_prueba = new BOLPrueba();
            foreach (var prueba in this.Expediente.PruebasAsignadas)
            {
                PruebaEstado estado = PruebaEstado.INACTIVA;
                try
                {
                    estado = bol_prueba.ObtenerEstadoPrueba(prueba.TipoPrueba.Nombre, this.Expediente.Id);
                }
                catch (FaultException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Estado Prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                tabla_pruebas.Rows.Add(prueba.TipoPrueba, prueba.TipoPrueba.Tiempo, estado);
                _pruebas_asignadas.Add(prueba.TipoPrueba, prueba.Escolaridad);
            }
            gridPruebas.DataSource = tabla_pruebas;
            return true;}

        /// <summary>
        /// Cierra el expediente activo
        /// </summary>
        /// <returns></returns>
        private bool CerrarExpediente()
        {
            BOLExpediente bol_expediente = new BOLExpediente();
            try
            {
                bol_expediente.CambiarEstadoExpediente(this.Expediente.Numero, Expediente.EstadoExpediente.CERRADO);
                XtraMessageBox.Show("Las pruebas han finalizado. El expediente procedera a cerrarse.", "Cerrar Expediente", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return true;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cerrar el expediente: " + ex.Message, "Cerrar Expediente", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            return false;
        }

        /***************** EVENTOS ****************/


        /// <summary>
        /// Comenzar las pruebas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnComenzarPruebas_Click(object sender, EventArgs e)
        {
            BOLPrueba bol_prueba = new BOLPrueba();
            foreach (KeyValuePair<Prueba, EscolaridadPrueba> prueba in _pruebas_asignadas)
            {
                switch (prueba.Key.Nombre)
                {
                    case "ALLPORT":
                        new frmAllport(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "BARSIT":
                        new frmBarsit(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "BFQ":
                        new frmBFQ(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "DOMINOS":
                        new frmDominos(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "CPI":
                        new frmCPI(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "IPV":
                        new frmIPV(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "TERMAN":
                        new frmTerman(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "ZAVIC":
                        new frmZavic(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "MOSS":
                        new frmMoss(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "RAVEN":
                        new frmRaven(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "MMPI-A":
                        new frmMMPIA(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "MMPI-2":
                        new frmMMPIA_2(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "KOSTICK":
                        new frmKostick(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "LIFO":
                        new frmLifo(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "PF16":
                        new frmPF16(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "PIPG":
                        new frmPIPG(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "WONDERLIC":
                        new frmWonderlic(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "CLEAVER":
                        new frmCleaver(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "INTELSOFT":
                        new frmIntelsoft(this.Expediente, prueba.Value).ShowDialog();
                        break;
                    case "LUSCHER":
                        new frmLuscher(this.Expediente, prueba.Value).ShowDialog();
                        break;
                }
            }
            CerrarExpediente();
            _pruebas_finalizadas = true;
            this.Close();
        }

        /// <summary> Verifica si se desea cerrar el expediente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPruebasAsignadas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_pruebas_finalizadas == true)
                return;
            if (
                XtraMessageBox.Show("Esta seguro que desea salir de la evaluacion?", "Salir", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}