﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmConfirmarSeleccionPrueba : DevExpress.XtraEditors.XtraForm
    {
        public Prueba Prueba { get; set; }
        public EscolaridadPrueba Escolaridad { get; set; }

        public frmConfirmarSeleccionPrueba(Prueba prueba)
        {
            InitializeComponent();
            this.Prueba = prueba;
        }

        /// <summary>
        /// Evento ejecutado al cargar el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmConfirmarSeleccionPrueba_Load(object sender, EventArgs e)
        {
            lblNombrePrueba.Text = this.Prueba.Nombre;
            lblTiempo.Text = this.Prueba.Tiempo.ToString();
            lblDescripcionPrueba.Text = this.Prueba.Descripcion;
            RellenarEscolaridad();
        }

        /// <summary>
        /// Rellena el lookupedit con los valores de las escolaridades de la prueba
        /// </summary>
        private void RellenarEscolaridad()
        {
            BOLPrueba bol_prueba = new BOLPrueba();
            EscolaridadPrueba[] escolaridades = null;
            try
            {
                escolaridades = bol_prueba.ObtenerEscolaridadesPrueba(this.Prueba);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrió un problema al cargar las escolaridades de la prueba: " + ex.Message, "Escolaridades " + this.Prueba.Nombre,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.DialogResult = DialogResult.No;
                return;
            }
            lookEscolaridades.Properties.DataSource = escolaridades;
            lookEscolaridades.ItemIndex = 0;
            this.Escolaridad = (EscolaridadPrueba)lookEscolaridades.GetSelectedDataRow();
        }

        /// <summary>
        /// Actualiza la propiedad escolaridad a la escolaridad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lookEscolaridades_EditValueChanged(object sender, EventArgs e)
        {
            this.Escolaridad = (EscolaridadPrueba)lookEscolaridades.GetSelectedDataRow();
        }

        /// <summary>
        /// Evento que se dispara cuando se da click en el boton aceptar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
            return;
        }

        /// <summary>
        /// Evento que se dispara cuando se da click en el boton cancelar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
            return;
        }
    }
}