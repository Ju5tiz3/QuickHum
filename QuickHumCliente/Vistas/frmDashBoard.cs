﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmDashBoard : DevExpress.XtraEditors.XtraForm
    {
        public frmDashBoard()
        {
            InitializeComponent();
        }

        private void CargarInformacion()
        {
            try
            {
                ResumenPruebasCategoria();
                ResumenPruebasGenero();
                ResumenEstadoExpedientes();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar los datos de la dashboard: " + ex.Message,
                    "Datos Dashboard", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
    }

        private void ResumenPruebasCategoria()
        {
            DateTime fecha_inicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime fecha_final = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1);
            DataTable pruebas_sucursal =
                BOLReportes.GenerarPruebasRealizadasSucursales(Globales.Oficina.Id, fecha_inicial, fecha_final);
            circuloComportamiento.Counter = short.Parse(pruebas_sucursal.Rows[0].Field<string>("TOTALCOMPORTAMIENTO"));
            circuloInteligencia.Counter = short.Parse(pruebas_sucursal.Rows[0].Field<string>("TOTALINTELIGENCIA"));
            circuloPersonalidad.Counter = short.Parse(pruebas_sucursal.Rows[0].Field<string>("TOTALPERSONALIDAD"));
            circuloTotal.Counter = (short)(circuloComportamiento.Counter + circuloInteligencia.Counter + circuloPersonalidad.Counter);
        }

        private void ResumenPruebasGenero()
        {
            DateTime fecha_inicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime fecha_final = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1);
            PruebasRealizadasGenero[] pruebas_genero = BOLReportes.ObtenerPruebasRealizadasGenero(
                Globales.Evaluador.Id, Globales.Oficina.Id, fecha_inicial, fecha_final);
            int total_masculino = pruebas_genero.Sum(l_pruebas => l_pruebas.masculino);
            int total_femenino = pruebas_genero.Sum(l_pruebas => l_pruebas.femenino);
            float valor_unidad_porcentaje = 100f/(total_masculino + total_femenino);
            string[] nombres_series = {"Masculino", "Femenino"};
            int[] valores_series = {total_masculino, total_femenino};
            chartGenero.Series[0].Points[0].SetValueY(total_masculino);
            chartGenero.Series[0].Points[1].SetValueY(total_femenino);
            lblPorcentajeMasculino.Text =
                Math.Round(total_masculino*valor_unidad_porcentaje).ToString(CultureInfo.InvariantCulture) + "%";
            lblPorcentajeFemenino.Text =
                Math.Round(total_femenino*valor_unidad_porcentaje).ToString(CultureInfo.InvariantCulture) + "%";
        }

        private void ResumenEstadoExpedientes()
        {
            DateTime fecha_inicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime fecha_final = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1);
            EstadisticasExpedientes estadisticas_expedientes =
                BOLReportes.ObtenerEstadisticasExpedientes
                (Globales.Evaluador.Id, Globales.Oficina.Id, fecha_inicial, fecha_final);
            lblExpedientesActivos.Text = estadisticas_expedientes.Activos.ToString();
            lblExpedientesAnulados.Text = estadisticas_expedientes.Anulados.ToString();
            lblExpedientesFinalizados.Text = estadisticas_expedientes.Cerrados.ToString();
        }

        private void frmDashBoard_Load(object sender, EventArgs e)
        {
            CargarInformacion();
        }
    }
}