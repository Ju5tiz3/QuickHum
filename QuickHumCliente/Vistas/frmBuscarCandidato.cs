﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmBuscarCandidato : DevExpress.XtraEditors.XtraForm
    {

        public string Dui { get; set; }

        public frmBuscarCandidato()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (txtDui.Text == string.Empty)
            {
                XtraMessageBox.Show("El campo del dui a buscar esta vacio.", "Campos invalidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Dui = txtDui.Text;
            this.Close();
        }
    }
}