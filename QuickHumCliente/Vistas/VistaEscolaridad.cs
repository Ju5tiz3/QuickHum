﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class VistaEscolaridad : XtraUserControl
    {

        private Escolaridad[] _escolaridades = null;

        public VistaEscolaridad()
        {
            InitializeComponent();
        }

        private void ListarEscolaridades()
        {
            BOLEscolaridad bol_escolaridad = new BOLEscolaridad();
            try
            {
                this._escolaridades = bol_escolaridad.ListarEscolaridades();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Hubo un problema al cargar las escolaridades: " + ex.Message,
                    "Problemas al cargar escolaridades", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
