﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.ServiceModel;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Vistas.Reportes;

namespace QuickHumCliente.Vistas
{
    public partial class frmReportesAdministrativo : DevExpress.XtraEditors.XtraForm
    {
        public frmReportesAdministrativo()
        {
            InitializeComponent();
            dateInicio.DateTime = DateTime.Now;
            dateFin.DateTime = DateTime.Now;
        }

        private void opcionFiltroPruebas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (opcionFiltroPruebas.SelectedIndex == 1)
            {
                cmbOficina.Enabled = false;
                return;
            }
            cmbOficina.Enabled = true;
        }

        private void btnGenerarReporte_Click(object sender, EventArgs e)
        {
            if (opcionFiltroPruebas.SelectedIndex == 0)
            {
                XtraMessageBox.Show("No Implementado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                new frmReporteSucursales(dateInicio.DateTime, dateFin.DateTime).ShowDialog();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al generar el reporte: " + ex.Message, "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}