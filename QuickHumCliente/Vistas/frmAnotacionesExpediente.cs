﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmAnotacionesExpediente : DevExpress.XtraEditors.XtraForm
    {
        public string Anotaciones { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="anotaciones"></param>
        public frmAnotacionesExpediente(string anotaciones = "")
        {
            InitializeComponent();
            Anotaciones = anotaciones;
            memoAnotaciones.Text = Anotaciones;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            this.Anotaciones = memoAnotaciones.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}