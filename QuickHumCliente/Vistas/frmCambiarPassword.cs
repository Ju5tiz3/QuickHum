﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;

namespace QuickHumCliente.Vistas
{
    public partial class frmCambiarPassword : DevExpress.XtraEditors.XtraForm
    {
        public frmCambiarPassword()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ACtualiza la contraseña
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            BOLUsuario bol_usuario = new BOLUsuario();
            try
            {
                if (bol_usuario.ActualizarPassword(Globales.Evaluador, txtPassA.Text, txtPassB.Text))
                    XtraMessageBox.Show("La contraseña ha sido actualizada con exito.", "Cambio de contraseña",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al actualizar la contraseña: " + ex.Message, "Cambio de contraseña",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al actualizar la contraseña: " + ex.Message, "Cambio de contraseña",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this.Close();
        }
    }
}