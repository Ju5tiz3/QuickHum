﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using TogglepyControl;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System.ServiceModel;
using DevExpress.XtraGrid.Columns;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmSeleccionPruebas : DevExpress.XtraEditors.XtraForm
    {
        public Dictionary<ServicioQuickHum.Prueba, EscolaridadPrueba> pruebas_activas = new Dictionary<Prueba, EscolaridadPrueba>();
        readonly Dictionary<Togglepy, ServicioQuickHum.Prueba> mapeo_pruebas = new Dictionary<Togglepy, ServicioQuickHum.Prueba>();
        
        /// <summary>
        /// Constructor
        /// </summary>
        public frmSeleccionPruebas()
        {
            InitializeComponent();
            CrearBotonesPruebas();
        }
        
        
        /********************* EVENTOS CONTROLES ******************/

        /// <summary>
        /// Cierra la ventana
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        /// <summary>
        /// Confirma el resultado de la ventana asi como las pruebas seleccionadas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSeleccionarPruebas_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }


        /****************** FUNCIONES AYUDA ***********************/

        /// <summary>
        /// Activa o desactiva la prueba seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwitchPrueba(object sender, EventArgs e)
        {
            Togglepy boton = (Togglepy) sender;
            if (boton.Checked == false)
            {
                if (pruebas_activas.ContainsKey(mapeo_pruebas[boton]))
                {
                    pruebas_activas.Remove(mapeo_pruebas[boton]);
                }
                return;
            }
            frmConfirmarSeleccionPrueba frm_prueba_confirmacion = new frmConfirmarSeleccionPrueba(mapeo_pruebas[boton]);
            if (frm_prueba_confirmacion.ShowDialog() != DialogResult.OK)
            {
                boton.Checked = false;
                return;
            }
            pruebas_activas.Add(frm_prueba_confirmacion.Prueba, frm_prueba_confirmacion.Escolaridad);
        }

        /// <summary>
        /// Crea dinamicamente los botones en base a la lista de pruebas recuperadas
        /// </summary>
        private void CrearBotonesPruebas()
        {
            Prueba[] pruebas = new BOLPrueba().ListaPruebas();
            foreach (Prueba prueba in pruebas)
            {
                if (prueba.Nombre == Properties.Settings.Default.HUMANSOFT)
                    continue;
                Togglepy boton_togglepy = new Togglepy()
                {
                    Name = prueba.Nombre,
                    Text = prueba.Nombre,
                    TestName = prueba.Nombre,
                    Education = "Ninguna",
                    BackColor = Color.FromArgb(214, 216, 223),
                    UncheckedColor = Color.FromArgb(214, 216, 223),
                    CheckedColor = Color.FromArgb(58, 190, 6),
                    TitleCheckedColor = Color.White,
                    TitleUncheckedColor = Color.FromArgb(132, 134, 134),
                    EducationCheckedColor = Color.White,
                    EducationUncheckedColor = Color.FromArgb(136, 137, 140),
                    Size = new Size(118, 96)
                };
                // Agregar Manejador de eventos al boton creado
                boton_togglepy.Click += SwitchPrueba;

                // Mapeo el boton a una prueba especifica
                mapeo_pruebas.Add(boton_togglepy, prueba);
                
                // Ubico el boton en la pestania respectiva
                switch (prueba.Categoria.Nombre)
                {
                    case "COMPORTAMIENTO":
                        panelComportamiento.Controls.Add(boton_togglepy);
                        break;
                    case "PERSONALIDAD":
                        panelPersonalidad.Controls.Add(boton_togglepy);
                        break;
                    case "INTELIGENCIA":
                        panelInteligencia.Controls.Add(boton_togglepy);
                        break;
                }
            }
        }
    }
}