﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmCargando : DevExpress.XtraEditors.XtraForm
    {
        public bool AllowClose { get; set; }

        public frmCargando()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Cancelar Alt F4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCargando_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4 && e.Alt)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void frmCargando_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AllowClose == false)
                e.Cancel = true;
            AllowClose = false;
        }
    }
}