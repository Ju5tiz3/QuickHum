﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Reportes
{
    public partial class frmReporteSucursales : DevExpress.XtraEditors.XtraForm
    {
        private DateTime _fecha_inicio, _fecha_fin;

        public frmReporteSucursales(DateTime fecha_inicio, DateTime fecha_fin)
        {
            InitializeComponent();
            _fecha_inicio = fecha_inicio;
            _fecha_fin = fecha_fin;
            CargarResumenPruebas();
        }

        private void frmReportePruebasGlobal_Load(object sender, EventArgs e)
        {
            rptResumenPruebasCategoriaGlobal.LocalReport.SetParameters(new ReportParameter("ADMINISTRADOR", Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido));
            rptResumenPruebasCategoriaGlobal.LocalReport.SetParameters(new ReportParameter("RANGO_FECHA", _fecha_inicio.ToString("d") + " - " + _fecha_fin.ToString("d")));
            this.rptResumenPruebasCategoriaGlobal.RefreshReport();
        }

        private void CargarResumenPruebas()
        {
            DataTable dt_resumen_pruebas_categoria = BOLReportes.GenerarPruebasRealizadasSucursales(-1, _fecha_inicio,
                _fecha_fin);
            ReportDataSource rds_categoria = new ReportDataSource("dsResumenPruebasCategoriaGlobal", dt_resumen_pruebas_categoria);
            rptResumenPruebasCategoriaGlobal.LocalReport.DataSources.Add(rds_categoria);
        }

    }
}