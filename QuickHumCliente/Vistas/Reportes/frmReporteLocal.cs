﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Reportes
{
    public partial class frmReporteLocal : DevExpress.XtraEditors.XtraForm
    {
        private DateTime _fecha_inicio, _fecha_fin;
        
        public frmReporteLocal(DateTime fecha_inicio, DateTime fecha_fin)
        {
            InitializeComponent();
            _fecha_inicio = fecha_inicio;
            _fecha_fin = fecha_fin;
            CargarResumenPruebas();
        }

        private void frmReportePruebas_Load(object sender, EventArgs e)
        {
            rptResumenPruebas.LocalReport.SetParameters(new ReportParameter("EVALUADOR", Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido));
            rptResumenPruebas.LocalReport.SetParameters(new ReportParameter("OFICINA", Globales.Oficina.Nombre));
            rptResumenPruebas.LocalReport.SetParameters(new ReportParameter("RANGO_FECHA", _fecha_inicio.ToString("d") + " - " + _fecha_fin.ToString("d")));
            this.rptResumenPruebas.RefreshReport();
        }

        private void CargarResumenPruebas()
        {
            DataTable dt_resumen_pruebas = BOLReportes.GenerarPruebasRealizadasPorGenero(Globales.Evaluador.Id,
                Globales.Oficina.Id, _fecha_inicio, _fecha_fin);
            DataTable dt_resumen_pruebas_categoria =
                BOLReportes.GenerarPruebasRealizadasPorCategoriaYGenero(Globales.Evaluador.Id,
                    Globales.Oficina.Id, _fecha_inicio, _fecha_fin);
            ReportDataSource rds = new ReportDataSource("dsResumenPruebasGenero", dt_resumen_pruebas);
            ReportDataSource rds_categoria = new ReportDataSource("dsResumenPruebasCategoria",
                dt_resumen_pruebas_categoria);

            rptResumenPruebas.LocalReport.DataSources.Add(rds);
            rptResumenPruebas.LocalReport.DataSources.Add(rds_categoria);
        }
    }
}