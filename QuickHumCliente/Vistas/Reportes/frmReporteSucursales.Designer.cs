﻿namespace QuickHumCliente.Vistas.Reportes
{
    partial class frmReporteSucursales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rptResumenPruebasCategoriaGlobal = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rptResumenPruebasCategoriaGlobal
            // 
            this.rptResumenPruebasCategoriaGlobal.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsResumenPruebasCategoriaGlobal";
            this.rptResumenPruebasCategoriaGlobal.LocalReport.DataSources.Add(reportDataSource1);
            this.rptResumenPruebasCategoriaGlobal.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Reportes.ReporteSucursales.rdlc";
            this.rptResumenPruebasCategoriaGlobal.Location = new System.Drawing.Point(0, 0);
            this.rptResumenPruebasCategoriaGlobal.Name = "rptResumenPruebasCategoriaGlobal";
            this.rptResumenPruebasCategoriaGlobal.Size = new System.Drawing.Size(792, 573);
            this.rptResumenPruebasCategoriaGlobal.TabIndex = 0;
            // 
            // frmReporteSucursales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.rptResumenPruebasCategoriaGlobal);
            this.Name = "frmReporteSucursales";
            this.Text = "Reporte Global";
            this.Load += new System.EventHandler(this.frmReportePruebasGlobal_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptResumenPruebasCategoriaGlobal;
    }
}