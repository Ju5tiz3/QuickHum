﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.XtraGrid.Views.Grid;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmOcupaciones : XtraForm
    {
        private Ocupacion[] _ocupaciones = null;
        private Ocupacion Ocupacion { get; set; }

        public frmOcupaciones()
        {
            InitializeComponent();
            InicializarOcupaciones();
            txtOcupacion.MaskBox.Mask.ShowPlaceHolders = false;
            txtDescripcion.MaskBox.Mask.ShowPlaceHolders = false;
            this.Ocupacion = new Ocupacion()
            {
                Id =  -1
            };
        }

        /// <summary>
        /// Inicializa las ocupaciones y loas carga en un grid
        /// </summary>
        private void InicializarOcupaciones()
        {
            BOLOcupacion bol_ocupacion = new BOLOcupacion();
            try
            {
                this._ocupaciones = bol_ocupacion.ListarOcupaciones();
                gridOcupacion.DataSource = this._ocupaciones;
                if (this._ocupaciones.Length == 0)
                    return;
                ((GridView) gridOcupacion.MainView).Columns[0].Width = 25;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al listar las ocupaciones: " + ex.Message,
                    "Carga de ocupaciones", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /************************** EVENTOS ***************************/

        /// <summary>
        /// Almacena una nueva ocupacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNuevaOcupacion_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Esta seguro de guardar la ocupacion: " + this.txtOcupacion.Text,
                "Guardar Ocupacion",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }this.Ocupacion.Nombre = txtOcupacion.Text;
            this.Ocupacion.Descripcion = txtDescripcion.Text;
            BOLOcupacion bol_ocupacion = new BOLOcupacion();
            try
            {
                bol_ocupacion.NuevaOcupacion(this.Ocupacion);
                XtraMessageBox.Show("La ocupacion se guardo con exito.", "Ocupacion guardada", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Close();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al guardar la ocupacion: " + ex.Message,
                    "Problema al guardar ocupacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al guardar la ocupacion: " + ex.Message,
                    "Problema al guardar ocupacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Selecciona ocupacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridOcupacion_DoubleClick(object sender, EventArgs e)
        {
            GridView vista = (GridView)gridOcupacion.MainView;
            if (vista.SelectedRowsCount == 0)
                return;
            this.Ocupacion = (Ocupacion)vista.GetRow(vista.GetSelectedRows()[0]);
            this.txtOcupacion.Text = this.Ocupacion.Nombre;
            this.txtDescripcion.Text = this.Ocupacion.Descripcion;
        }

        /// <summary>
        /// Actualiza una ocupacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstActualizarOcupacion_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show(
                    "Esta seguro de actualizar la ocupacion: " + this.Ocupacion.Nombre,
                    "Actualizar ocupacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            BOLOcupacion bol_ocupacion = new BOLOcupacion();
            try
            {
                bol_ocupacion.ActualizarOcupacion(this.Ocupacion, txtOcupacion.Text, txtDescripcion.Text);
                XtraMessageBox.Show("La ocupacion se actualizo con exito.", "Ocupacion actualizada", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Close();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al actualizar la ocupacion: " + ex.Message,
                    "Problema en actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al actualizar la ocupacion: " + ex.Message,
                    "Problema en actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Elimina una ocupacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstEliminarOcupacion_Click(object sender, EventArgs e)
        {
            if (
                XtraMessageBox.Show("Esta seguro de eliminar la ocupacion?", "Eliminar ocupacion",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            BOLOcupacion bol_ocupacion = new BOLOcupacion();
            try
            {
                bol_ocupacion.EliminarOcupacion(this.Ocupacion);
                XtraMessageBox.Show("La ocupacion fue eliminada con exito", "Eliminar ocupacion", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Close();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al eliminar la ocupacion: " + ex.Message,
                    "Problema en eliminacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al eliminar la ocupacion: " + ex.Message,
                    "Problema en eliminacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }
    }
}
