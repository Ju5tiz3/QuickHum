﻿namespace QuickHumCliente.Vistas
{
    partial class frmOcupaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.txtNuevaOcupacion = new System.Windows.Forms.ToolStripButton();
            this.tstActualizarOcupacion = new System.Windows.Forms.ToolStripButton();
            this.tstEliminarOcupacion = new System.Windows.Forms.ToolStripButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDescripcion = new DevExpress.XtraEditors.MemoEdit();
            this.gridOcupacion = new DevExpress.XtraGrid.GridControl();
            this.VistaPrincipal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtOcupacion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOcupacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOcupacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(794, 48);
            this.ContentedorTituloPestana.TabIndex = 4;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(103, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "Ocupaciones";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.toolStrip2.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtNuevaOcupacion,
            this.tstActualizarOcupacion,
            this.tstEliminarOcupacion});
            this.toolStrip2.Location = new System.Drawing.Point(0, 48);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip2.Size = new System.Drawing.Size(794, 39);
            this.toolStrip2.TabIndex = 5;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // txtNuevaOcupacion
            // 
            this.txtNuevaOcupacion.ForeColor = System.Drawing.Color.White;
            this.txtNuevaOcupacion.Image = global::QuickHumCliente.Properties.Resources.save;
            this.txtNuevaOcupacion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.txtNuevaOcupacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.txtNuevaOcupacion.Name = "txtNuevaOcupacion";
            this.txtNuevaOcupacion.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.txtNuevaOcupacion.Size = new System.Drawing.Size(105, 36);
            this.txtNuevaOcupacion.Text = "Guardar";
            this.txtNuevaOcupacion.Click += new System.EventHandler(this.txtNuevaOcupacion_Click);
            // 
            // tstActualizarOcupacion
            // 
            this.tstActualizarOcupacion.ForeColor = System.Drawing.Color.White;
            this.tstActualizarOcupacion.Image = global::QuickHumCliente.Properties.Resources.editar1;
            this.tstActualizarOcupacion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstActualizarOcupacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstActualizarOcupacion.Name = "tstActualizarOcupacion";
            this.tstActualizarOcupacion.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tstActualizarOcupacion.Size = new System.Drawing.Size(113, 36);
            this.tstActualizarOcupacion.Text = "Actualizar";
            this.tstActualizarOcupacion.Click += new System.EventHandler(this.tstActualizarOcupacion_Click);
            // 
            // tstEliminarOcupacion
            // 
            this.tstEliminarOcupacion.ForeColor = System.Drawing.Color.White;
            this.tstEliminarOcupacion.Image = global::QuickHumCliente.Properties.Resources.delete;
            this.tstEliminarOcupacion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstEliminarOcupacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tstEliminarOcupacion.Name = "tstEliminarOcupacion";
            this.tstEliminarOcupacion.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tstEliminarOcupacion.Size = new System.Drawing.Size(105, 36);
            this.tstEliminarOcupacion.Text = "Eliminar";
            this.tstEliminarOcupacion.Click += new System.EventHandler(this.tstEliminarOcupacion_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtDescripcion);
            this.layoutControl1.Controls.Add(this.gridOcupacion);
            this.layoutControl1.Controls.Add(this.txtOcupacion);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 87);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1033, 241, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(794, 488);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(99, 98);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Properties.AcceptsReturn = false;
            this.txtDescripcion.Properties.Appearance.Font = new System.Drawing.Font("Open Sans", 9.75F);
            this.txtDescripcion.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtDescripcion.Properties.Appearance.Options.UseFont = true;
            this.txtDescripcion.Properties.Appearance.Options.UseForeColor = true;
            this.txtDescripcion.Properties.MaxLength = 250;
            this.txtDescripcion.Properties.NullValuePrompt = "Escribe la descripcion de la ocupacion...";
            this.txtDescripcion.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtDescripcion.Size = new System.Drawing.Size(683, 92);
            this.txtDescripcion.StyleController = this.layoutControl1;
            this.txtDescripcion.TabIndex = 8;
            // 
            // gridOcupacion
            // 
            this.gridOcupacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridOcupacion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridOcupacion.Location = new System.Drawing.Point(12, 204);
            this.gridOcupacion.MainView = this.VistaPrincipal;
            this.gridOcupacion.Name = "gridOcupacion";
            this.gridOcupacion.Size = new System.Drawing.Size(770, 272);
            this.gridOcupacion.TabIndex = 7;
            this.gridOcupacion.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VistaPrincipal});
            this.gridOcupacion.DoubleClick += new System.EventHandler(this.gridOcupacion_DoubleClick);
            // 
            // VistaPrincipal
            // 
            this.VistaPrincipal.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.VistaPrincipal.GridControl = this.gridOcupacion;
            this.VistaPrincipal.Name = "VistaPrincipal";
            this.VistaPrincipal.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.Editable = false;
            this.VistaPrincipal.OptionsCustomization.AllowColumnMoving = false;
            this.VistaPrincipal.OptionsCustomization.AllowFilter = false;
            this.VistaPrincipal.OptionsCustomization.AllowGroup = false;
            this.VistaPrincipal.OptionsCustomization.AllowQuickHideColumns = false;
            this.VistaPrincipal.OptionsCustomization.AllowSort = false;
            this.VistaPrincipal.OptionsFind.AlwaysVisible = true;
            this.VistaPrincipal.OptionsFind.FindDelay = 500;
            this.VistaPrincipal.OptionsFind.FindNullPrompt = "Ingrese la escolaridad a buscar...";
            this.VistaPrincipal.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.VistaPrincipal.OptionsView.AllowHtmlDrawGroups = false;
            this.VistaPrincipal.OptionsView.ShowGroupPanel = false;
            this.VistaPrincipal.RowHeight = 32;
            // 
            // txtOcupacion
            // 
            this.txtOcupacion.Location = new System.Drawing.Point(99, 56);
            this.txtOcupacion.Name = "txtOcupacion";
            this.txtOcupacion.Properties.Appearance.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOcupacion.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtOcupacion.Properties.Appearance.Options.UseFont = true;
            this.txtOcupacion.Properties.Appearance.Options.UseForeColor = true;
            this.txtOcupacion.Properties.AutoHeight = false;
            this.txtOcupacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOcupacion.Properties.Mask.EditMask = "[\\w ]{1,100}";
            this.txtOcupacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtOcupacion.Properties.Mask.PlaceHolder = ' ';
            this.txtOcupacion.Properties.Mask.SaveLiteral = false;
            this.txtOcupacion.Properties.Mask.ShowPlaceHolders = false;
            this.txtOcupacion.Properties.MaxLength = 100;
            this.txtOcupacion.Properties.NullValuePrompt = "Escribe el nombre de la ocupacion...";
            this.txtOcupacion.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtOcupacion.Size = new System.Drawing.Size(296, 28);
            this.txtOcupacion.StyleController = this.layoutControl1;
            this.txtOcupacion.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.labelControl1.Location = new System.Drawing.Point(10, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.labelControl1.Size = new System.Drawing.Size(183, 32);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Informacion Generales";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.simpleSeparator1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(794, 488);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(183, 32);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(183, 32);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(774, 32);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.txtOcupacion;
            this.layoutControlItem2.CustomizationFormText = "PUESTO";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(141, 32);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(387, 32);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "PUESTO";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(84, 18);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 32);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(774, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 76);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(774, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 34);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(774, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridOcupacion;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(774, 276);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(387, 44);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 32);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 32);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(387, 32);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(774, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.txtDescripcion;
            this.layoutControlItem3.CustomizationFormText = "DESCRIPCION";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 96);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(101, 96);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(774, 96);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "DESCRIPCION";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(84, 18);
            // 
            // frmOcupaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 575);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOcupaciones";
            this.Text = "Ocupaciones";
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridOcupacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOcupacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton txtNuevaOcupacion;
        private System.Windows.Forms.ToolStripButton tstActualizarOcupacion;
        private System.Windows.Forms.ToolStripButton tstEliminarOcupacion;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtOcupacion;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.GridControl gridOcupacion;
        private DevExpress.XtraGrid.Views.Grid.GridView VistaPrincipal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit txtDescripcion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}