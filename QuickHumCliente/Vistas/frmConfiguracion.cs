﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Net.NetworkInformation;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Drawing.Helpers;
using DevExpress.XtraEditors;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmConfiguracion : DevExpress.XtraEditors.XtraForm
    {
        frmCargando frm_cargando = new frmCargando();
        public frmConfiguracion()
        {
            InitializeComponent();
        }

        private void btnVerificarServidor_Click(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
            frm_cargando.ShowDialog();
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((bool) e.Result == true)
            {
                panelServidorEstado.BackColor = Color.FromArgb(140, 230, 0);
                lblEstadoServidor.Text = "EN LINEA";
            }
            else
            {
                panelServidorEstado.BackColor = Color.FromArgb(222, 40, 0);
                lblEstadoServidor.Text = "FUERA DE LINEA";
            }
            frm_cargando.AllowClose = true;frm_cargando.Close();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = VerificarEstadoServicio();
        }


        private bool VerificarEstadoServicio()
        {
            QuickHumCliente.ServicioQuickHum.QuickHumClient cliente = new QuickHumClient();
            try
            {
                cliente.Endpoint.Address = new EndpointAddress(new Uri(this.txtIp.Text));
                cliente.Open();
                cliente.Close();
                return true;
            }
            catch (CommunicationException)
            {
                cliente.Abort();
                return false;
            }
        }

        /// <summary>
        /// Carga las configuraciones en los campos respectivos 
        /// </summary>
        private void CargarConfiguraciones()
        {
            txtIp.Text = Properties.Settings.Default.DireccionServicio;
            txtHumansoft.Text = Properties.Settings.Default.HUMANSOFT;
            txtIntelsoft.Text = Properties.Settings.Default.INTELSOFT;
            txtIpv.Text = Properties.Settings.Default.IPV;
            txtKostick.Text =  Properties.Settings.Default.KOSTICK;
            txtLifo.Text = Properties.Settings.Default.LIFO;
            txtLuscher.Text = Properties.Settings.Default.LUSCHER;
            txtMMPI2.Text = Properties.Settings.Default.MMPI2;
            txtMMPIA.Text = Properties.Settings.Default.MMPIA;
            txtMoss.Text = Properties.Settings.Default.MOSS;
            txt16pf.Text = Properties.Settings.Default.PF16;
            txtPipg.Text = Properties.Settings.Default.PIPG;
            txtRaven.Text = Properties.Settings.Default.RAVEN;
            txtTerman.Text = Properties.Settings.Default.TERMAN;
            txtWonderlic.Text = Properties.Settings.Default.WONDERLIC;
            txtZavic.Text = Properties.Settings.Default.ZAVIC;
            txtAllport.Text = Properties.Settings.Default.ALLPORT;
            txtBarsit.Text = Properties.Settings.Default.BARSIT;
            txtBeta.Text = Properties.Settings.Default.BETAIII;
            txtBfq.Text = Properties.Settings.Default.BFQ;
            txtCleaver.Text = Properties.Settings.Default.CLEAVER;
            txtCpi.Text = Properties.Settings.Default.CPI;
            txtDominos.Text = Properties.Settings.Default.DOMINOS;
        }
        
        /// <summary>
        /// Guarda las configuraciones
        /// </summary>
        private void GuardarConfiguraciones()
        {
            Properties.Settings.Default.DireccionServicio = txtIp.Text;
            Properties.Settings.Default.HUMANSOFT = txtHumansoft.Text;
            Properties.Settings.Default.INTELSOFT = txtIntelsoft.Text;
            Properties.Settings.Default.IPV = txtIpv.Text;
            Properties.Settings.Default.KOSTICK = txtKostick.Text;
            Properties.Settings.Default.LIFO = txtLifo.Text;
            Properties.Settings.Default.LUSCHER = txtLuscher.Text;
            Properties.Settings.Default.MMPI2 = txtMMPI2.Text;
            Properties.Settings.Default.MMPIA = txtMMPIA.Text;
            Properties.Settings.Default.MOSS = txtMoss.Text;
            Properties.Settings.Default.PF16 = txt16pf.Text;
            Properties.Settings.Default.PIPG = txtPipg.Text;
            Properties.Settings.Default.RAVEN = txtRaven.Text;
            Properties.Settings.Default.TERMAN = txtTerman.Text;
            Properties.Settings.Default.WONDERLIC = txtWonderlic.Text;
            Properties.Settings.Default.ZAVIC = txtZavic.Text;
            Properties.Settings.Default.ALLPORT = txtAllport.Text;
            Properties.Settings.Default.BARSIT = txtBarsit.Text;
            Properties.Settings.Default.BETAIII = txtBeta.Text;
            Properties.Settings.Default.BFQ = txtBfq.Text;
            Properties.Settings.Default.CLEAVER = txtCleaver.Text;
            Properties.Settings.Default.CPI = txtCpi.Text;
            Properties.Settings.Default.DOMINOS = txtDominos.Text;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Guarda las configuraciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarConfiguraciones();
            XtraMessageBox.Show("La configuracion ha sido guardada.", "Configuracion", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            this.Close();
        }

        /// <summary>
        /// Funcion ejecutada al momento que se carga el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmConfiguracion_Load(object sender, EventArgs e)
        {
            CargarConfiguraciones();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}