﻿namespace QuickHumCliente.Vistas
{
    partial class VistaCandidato
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnGuardar = new System.Windows.Forms.ToolStripButton();
            this.btnBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnAgregarExpediente = new System.Windows.Forms.ToolStripButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtMunicipio = new DevExpress.XtraEditors.TextEdit();
            this.txtDepartamento = new DevExpress.XtraEditors.TextEdit();
            this.picCandidato = new DevExpress.XtraEditors.PictureEdit();
            this.cmbEdad = new DevExpress.XtraEditors.ComboBoxEdit();
            this.GridExpedientes = new DevExpress.XtraGrid.GridControl();
            this.VistaPrincipal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEvaluador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPuestoAplicado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFecha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbEscolaridad = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTelefono = new DevExpress.XtraEditors.TextEdit();
            this.txtDui = new DevExpress.XtraEditors.TextEdit();
            this.cmbEstadoCivil = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtApellido = new DevExpress.XtraEditors.TextEdit();
            this.cmbGenero = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtNombre = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ValidadorCandidato = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartamento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCandidato.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEdad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExpedientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEscolaridad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDui.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEstadoCivil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGenero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidadorCandidato)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(1000, 48);
            this.ContentedorTituloPestana.TabIndex = 1;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(85, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "Candidato";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.toolStrip2.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnGuardar,
            this.btnBuscar,
            this.btnEliminar,
            this.btnAgregarExpediente});
            this.toolStrip2.Location = new System.Drawing.Point(0, 48);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip2.Size = new System.Drawing.Size(1000, 39);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btnGuardar
            // 
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Image = global::QuickHumCliente.Properties.Resources.save;
            this.btnGuardar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnGuardar.Size = new System.Drawing.Size(113, 36);
            this.btnGuardar.Text = "Actualizar";
            this.btnGuardar.Click += new System.EventHandler(this.SwitchOpcionGuardado);
            // 
            // btnBuscar
            // 
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Image = global::QuickHumCliente.Properties.Resources.buscar2;
            this.btnBuscar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnBuscar.Size = new System.Drawing.Size(97, 36);
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Enabled = false;
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Image = global::QuickHumCliente.Properties.Resources.delete;
            this.btnEliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnEliminar.Size = new System.Drawing.Size(105, 36);
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.EliminarCandidato);
            // 
            // btnAgregarExpediente
            // 
            this.btnAgregarExpediente.Enabled = false;
            this.btnAgregarExpediente.ForeColor = System.Drawing.Color.White;
            this.btnAgregarExpediente.Image = global::QuickHumCliente.Properties.Resources.editar1;
            this.btnAgregarExpediente.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAgregarExpediente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAgregarExpediente.Name = "btnAgregarExpediente";
            this.btnAgregarExpediente.Size = new System.Drawing.Size(143, 36);
            this.btnAgregarExpediente.Text = "Agregar Expediente";
            this.btnAgregarExpediente.Click += new System.EventHandler(this.btnAgregarExpediente_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtMunicipio);
            this.layoutControl1.Controls.Add(this.txtDepartamento);
            this.layoutControl1.Controls.Add(this.picCandidato);
            this.layoutControl1.Controls.Add(this.cmbEdad);
            this.layoutControl1.Controls.Add(this.GridExpedientes);
            this.layoutControl1.Controls.Add(this.cmbEscolaridad);
            this.layoutControl1.Controls.Add(this.txtTelefono);
            this.layoutControl1.Controls.Add(this.txtDui);
            this.layoutControl1.Controls.Add(this.cmbEstadoCivil);
            this.layoutControl1.Controls.Add(this.txtApellido);
            this.layoutControl1.Controls.Add(this.cmbGenero);
            this.layoutControl1.Controls.Add(this.txtNombre);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 87);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(917, 88, 778, 340);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.ShowTemplates = true;
            this.layoutControl1.Size = new System.Drawing.Size(1000, 613);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.EditValue = "";
            this.txtMunicipio.Enabled = false;
            this.txtMunicipio.Location = new System.Drawing.Point(732, 217);
            this.txtMunicipio.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtMunicipio.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtMunicipio.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtMunicipio.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtMunicipio.Properties.Appearance.Options.UseBackColor = true;
            this.txtMunicipio.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMunicipio.Properties.Appearance.Options.UseFont = true;
            this.txtMunicipio.Properties.Appearance.Options.UseForeColor = true;
            this.txtMunicipio.Properties.AutoHeight = false;
            this.txtMunicipio.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMunicipio.Properties.Mask.EditMask = "\\d{1,9}";
            this.txtMunicipio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMunicipio.Properties.Mask.PlaceHolder = '0';
            this.txtMunicipio.Properties.Mask.SaveLiteral = false;
            this.txtMunicipio.Properties.NullValuePrompt = "DUI";
            this.txtMunicipio.Size = new System.Drawing.Size(256, 32);
            this.txtMunicipio.StyleController = this.layoutControl1;
            this.txtMunicipio.TabIndex = 28;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Campo Requerido";
            this.ValidadorCandidato.SetValidationRule(this.txtMunicipio, conditionValidationRule1);
            // 
            // txtDepartamento
            // 
            this.txtDepartamento.EditValue = "";
            this.txtDepartamento.Enabled = false;
            this.txtDepartamento.Location = new System.Drawing.Point(318, 217);
            this.txtDepartamento.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtDepartamento.Name = "txtDepartamento";
            this.txtDepartamento.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtDepartamento.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtDepartamento.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtDepartamento.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtDepartamento.Properties.Appearance.Options.UseBackColor = true;
            this.txtDepartamento.Properties.Appearance.Options.UseBorderColor = true;
            this.txtDepartamento.Properties.Appearance.Options.UseFont = true;
            this.txtDepartamento.Properties.Appearance.Options.UseForeColor = true;
            this.txtDepartamento.Properties.AutoHeight = false;
            this.txtDepartamento.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtDepartamento.Properties.Mask.EditMask = "\\d{1,9}";
            this.txtDepartamento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtDepartamento.Properties.Mask.PlaceHolder = '0';
            this.txtDepartamento.Properties.Mask.SaveLiteral = false;
            this.txtDepartamento.Properties.NullValuePrompt = "DUI";
            this.txtDepartamento.Size = new System.Drawing.Size(256, 32);
            this.txtDepartamento.StyleController = this.layoutControl1;
            this.txtDepartamento.TabIndex = 28;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Campo Requerido";
            this.ValidadorCandidato.SetValidationRule(this.txtDepartamento, conditionValidationRule2);
            // 
            // picCandidato
            // 
            this.picCandidato.EditValue = global::QuickHumCliente.Properties.Resources.usuario_foto_vacia3;
            this.picCandidato.Location = new System.Drawing.Point(14, 58);
            this.picCandidato.MaximumSize = new System.Drawing.Size(150, 190);
            this.picCandidato.Name = "picCandidato";
            this.picCandidato.Properties.AllowFocused = false;
            this.picCandidato.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picCandidato.Properties.Appearance.Options.UseBackColor = true;
            this.picCandidato.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picCandidato.Properties.ErrorImage = global::QuickHumCliente.Properties.Resources.usuario_foto_vacia2;
            this.picCandidato.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picCandidato.Size = new System.Drawing.Size(150, 190);
            this.picCandidato.StyleController = this.layoutControl1;
            this.picCandidato.TabIndex = 23;
            // 
            // cmbEdad
            // 
            this.cmbEdad.Enabled = false;
            this.cmbEdad.Location = new System.Drawing.Point(732, 57);
            this.cmbEdad.Name = "cmbEdad";
            this.cmbEdad.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbEdad.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEdad.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cmbEdad.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbEdad.Properties.Appearance.Options.UseFont = true;
            this.cmbEdad.Properties.Appearance.Options.UseForeColor = true;
            this.cmbEdad.Properties.AutoHeight = false;
            this.cmbEdad.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbEdad.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEdad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEdad.Size = new System.Drawing.Size(256, 32);
            this.cmbEdad.StyleController = this.layoutControl1;
            this.cmbEdad.TabIndex = 33;
            // 
            // GridExpedientes
            // 
            this.GridExpedientes.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridExpedientes.Location = new System.Drawing.Point(12, 296);
            this.GridExpedientes.MainView = this.VistaPrincipal;
            this.GridExpedientes.Name = "GridExpedientes";
            this.GridExpedientes.Size = new System.Drawing.Size(976, 305);
            this.GridExpedientes.TabIndex = 32;
            this.GridExpedientes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VistaPrincipal});
            this.GridExpedientes.DoubleClick += new System.EventHandler(this.GridExpedientes_DoubleClick);
            // 
            // VistaPrincipal
            // 
            this.VistaPrincipal.Appearance.Row.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VistaPrincipal.Appearance.Row.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.VistaPrincipal.Appearance.Row.Options.UseFont = true;
            this.VistaPrincipal.Appearance.Row.Options.UseForeColor = true;
            this.VistaPrincipal.ColumnPanelRowHeight = 32;
            this.VistaPrincipal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNumero,
            this.colEvaluador,
            this.colUbicacion,
            this.colEstado,
            this.colPuestoAplicado,
            this.colFecha});
            this.VistaPrincipal.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.VistaPrincipal.GridControl = this.GridExpedientes;
            this.VistaPrincipal.Name = "VistaPrincipal";
            this.VistaPrincipal.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.Editable = false;
            this.VistaPrincipal.OptionsCustomization.AllowColumnMoving = false;
            this.VistaPrincipal.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.VistaPrincipal.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VistaPrincipal.OptionsView.ShowGroupPanel = false;
            this.VistaPrincipal.RowHeight = 32;
            // 
            // colNumero
            // 
            this.colNumero.AppearanceCell.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colNumero.AppearanceCell.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colNumero.AppearanceCell.Options.UseFont = true;
            this.colNumero.AppearanceCell.Options.UseTextOptions = true;
            this.colNumero.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNumero.Caption = "Numero";
            this.colNumero.FieldName = "Numero";
            this.colNumero.Name = "colNumero";
            this.colNumero.Visible = true;
            this.colNumero.VisibleIndex = 0;
            this.colNumero.Width = 113;
            // 
            // colEvaluador
            // 
            this.colEvaluador.Caption = "Evaluador";
            this.colEvaluador.FieldName = "Evaluador";
            this.colEvaluador.Name = "colEvaluador";
            this.colEvaluador.Visible = true;
            this.colEvaluador.VisibleIndex = 1;
            // 
            // colUbicacion
            // 
            this.colUbicacion.Caption = "Ubicacion";
            this.colUbicacion.FieldName = "Ubicacion";
            this.colUbicacion.Name = "colUbicacion";
            this.colUbicacion.Visible = true;
            this.colUbicacion.VisibleIndex = 2;
            // 
            // colEstado
            // 
            this.colEstado.Caption = "Estado";
            this.colEstado.FieldName = "Estado";
            this.colEstado.Name = "colEstado";
            this.colEstado.Visible = true;
            this.colEstado.VisibleIndex = 3;
            // 
            // colPuestoAplicado
            // 
            this.colPuestoAplicado.Caption = "Puesto Aplicado";
            this.colPuestoAplicado.FieldName = "PuestoAplicado";
            this.colPuestoAplicado.Name = "colPuestoAplicado";
            this.colPuestoAplicado.Visible = true;
            this.colPuestoAplicado.VisibleIndex = 4;
            // 
            // colFecha
            // 
            this.colFecha.Caption = "Fecha";
            this.colFecha.FieldName = "Fecha";
            this.colFecha.Name = "colFecha";
            this.colFecha.Visible = true;
            this.colFecha.VisibleIndex = 5;
            // 
            // cmbEscolaridad
            // 
            this.cmbEscolaridad.EditValue = "";
            this.cmbEscolaridad.Location = new System.Drawing.Point(732, 177);
            this.cmbEscolaridad.Name = "cmbEscolaridad";
            this.cmbEscolaridad.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cmbEscolaridad.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbEscolaridad.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEscolaridad.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cmbEscolaridad.Properties.Appearance.Options.UseBackColor = true;
            this.cmbEscolaridad.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbEscolaridad.Properties.Appearance.Options.UseFont = true;
            this.cmbEscolaridad.Properties.Appearance.Options.UseForeColor = true;
            this.cmbEscolaridad.Properties.AutoHeight = false;
            this.cmbEscolaridad.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbEscolaridad.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEscolaridad.Properties.DropDownItemHeight = 32;
            this.cmbEscolaridad.Properties.NullValuePrompt = "ESCOLARIDAD";
            this.cmbEscolaridad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEscolaridad.Size = new System.Drawing.Size(256, 32);
            this.cmbEscolaridad.StyleController = this.layoutControl1;
            this.cmbEscolaridad.TabIndex = 31;
            // 
            // txtTelefono
            // 
            this.txtTelefono.EditValue = "";
            this.txtTelefono.Enabled = false;
            this.txtTelefono.Location = new System.Drawing.Point(318, 177);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtTelefono.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtTelefono.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtTelefono.Properties.Appearance.Options.UseBackColor = true;
            this.txtTelefono.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTelefono.Properties.Appearance.Options.UseFont = true;
            this.txtTelefono.Properties.Appearance.Options.UseForeColor = true;
            this.txtTelefono.Properties.AutoHeight = false;
            this.txtTelefono.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtTelefono.Properties.Mask.EditMask = "\\d{4}-\\d{4}";
            this.txtTelefono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtTelefono.Properties.Mask.PlaceHolder = '0';
            this.txtTelefono.Properties.NullValuePrompt = "NOMBRE";
            this.txtTelefono.Size = new System.Drawing.Size(256, 32);
            this.txtTelefono.StyleController = this.layoutControl1;
            this.txtTelefono.TabIndex = 24;
            // 
            // txtDui
            // 
            this.txtDui.EditValue = "";
            this.txtDui.Enabled = false;
            this.txtDui.Location = new System.Drawing.Point(318, 57);
            this.txtDui.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtDui.Name = "txtDui";
            this.txtDui.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtDui.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtDui.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtDui.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtDui.Properties.Appearance.Options.UseBackColor = true;
            this.txtDui.Properties.Appearance.Options.UseBorderColor = true;
            this.txtDui.Properties.Appearance.Options.UseFont = true;
            this.txtDui.Properties.Appearance.Options.UseForeColor = true;
            this.txtDui.Properties.AutoHeight = false;
            this.txtDui.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtDui.Properties.Mask.EditMask = "\\d{1,9}";
            this.txtDui.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtDui.Properties.Mask.PlaceHolder = '0';
            this.txtDui.Properties.Mask.SaveLiteral = false;
            this.txtDui.Properties.NullValuePrompt = "DUI";
            this.txtDui.Size = new System.Drawing.Size(256, 32);
            this.txtDui.StyleController = this.layoutControl1;
            this.txtDui.TabIndex = 27;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Campo Requerido";
            this.ValidadorCandidato.SetValidationRule(this.txtDui, conditionValidationRule3);
            // 
            // cmbEstadoCivil
            // 
            this.cmbEstadoCivil.EditValue = "SOLTERO";
            this.cmbEstadoCivil.Location = new System.Drawing.Point(732, 137);
            this.cmbEstadoCivil.Name = "cmbEstadoCivil";
            this.cmbEstadoCivil.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cmbEstadoCivil.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbEstadoCivil.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEstadoCivil.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cmbEstadoCivil.Properties.Appearance.Options.UseBackColor = true;
            this.cmbEstadoCivil.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbEstadoCivil.Properties.Appearance.Options.UseFont = true;
            this.cmbEstadoCivil.Properties.Appearance.Options.UseForeColor = true;
            this.cmbEstadoCivil.Properties.AutoHeight = false;
            this.cmbEstadoCivil.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbEstadoCivil.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEstadoCivil.Properties.DropDownItemHeight = 32;
            this.cmbEstadoCivil.Properties.Items.AddRange(new object[] {
            "ACOMPAÑADO",
            "CASADO",
            "SOLTERO",
            "VIUDO"});
            this.cmbEstadoCivil.Properties.NullValuePrompt = "ESCOLARIDAD";
            this.cmbEstadoCivil.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEstadoCivil.Size = new System.Drawing.Size(256, 32);
            this.cmbEstadoCivil.StyleController = this.layoutControl1;
            this.cmbEstadoCivil.TabIndex = 30;
            // 
            // txtApellido
            // 
            this.txtApellido.EditValue = "";
            this.txtApellido.Enabled = false;
            this.txtApellido.Location = new System.Drawing.Point(732, 97);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtApellido.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtApellido.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.txtApellido.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtApellido.Properties.Appearance.Options.UseBorderColor = true;
            this.txtApellido.Properties.Appearance.Options.UseFont = true;
            this.txtApellido.Properties.Appearance.Options.UseForeColor = true;
            this.txtApellido.Properties.AutoHeight = false;
            this.txtApellido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtApellido.Properties.Mask.EditMask = "\\w+";
            this.txtApellido.Properties.Mask.ShowPlaceHolders = false;
            this.txtApellido.Properties.NullValuePrompt = "APELLIDO";
            this.txtApellido.Size = new System.Drawing.Size(256, 32);
            this.txtApellido.StyleController = this.layoutControl1;
            this.txtApellido.TabIndex = 24;
            // 
            // cmbGenero
            // 
            this.cmbGenero.EditValue = "MASCULINO";
            this.cmbGenero.Enabled = false;
            this.cmbGenero.Location = new System.Drawing.Point(318, 137);
            this.cmbGenero.Name = "cmbGenero";
            this.cmbGenero.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cmbGenero.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbGenero.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGenero.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cmbGenero.Properties.Appearance.Options.UseBackColor = true;
            this.cmbGenero.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbGenero.Properties.Appearance.Options.UseFont = true;
            this.cmbGenero.Properties.Appearance.Options.UseForeColor = true;
            this.cmbGenero.Properties.AutoHeight = false;
            this.cmbGenero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbGenero.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGenero.Properties.DropDownItemHeight = 32;
            this.cmbGenero.Properties.Items.AddRange(new object[] {
            "MASCULINO",
            "FEMENINO"});
            this.cmbGenero.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbGenero.Size = new System.Drawing.Size(256, 32);
            this.cmbGenero.StyleController = this.layoutControl1;
            this.cmbGenero.TabIndex = 27;
            // 
            // txtNombre
            // 
            this.txtNombre.EditValue = "";
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(318, 97);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(5, 15, 0, 15);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtNombre.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNombre.Properties.Appearance.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtNombre.Properties.Appearance.Options.UseBorderColor = true;
            this.txtNombre.Properties.Appearance.Options.UseFont = true;
            this.txtNombre.Properties.Appearance.Options.UseForeColor = true;
            this.txtNombre.Properties.AutoHeight = false;
            this.txtNombre.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNombre.Properties.Mask.EditMask = "\\w+";
            this.txtNombre.Properties.Mask.ShowPlaceHolders = false;
            this.txtNombre.Properties.NullValuePrompt = "NOMBRE";
            this.txtNombre.Size = new System.Drawing.Size(256, 32);
            this.txtNombre.StyleController = this.layoutControl1;
            this.txtNombre.TabIndex = 23;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem1,
            this.simpleSeparator1,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.emptySpaceItem5,
            this.layoutControlItem12,
            this.simpleLabelItem2,
            this.simpleSeparator2,
            this.layoutControlItem13,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1000, 613);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleLabelItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem1.CustomizationFormText = "Datos Generales";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.simpleLabelItem1.MinSize = new System.Drawing.Size(138, 29);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 2, 5);
            this.simpleLabelItem1.Size = new System.Drawing.Size(980, 29);
            this.simpleLabelItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem1.Text = "Datos Generales";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(131, 22);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 29);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(980, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.Control = this.txtNombre;
            this.layoutControlItem4.CustomizationFormText = "NOMBRE:";
            this.layoutControlItem4.Location = new System.Drawing.Point(172, 83);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem4.Text = "NOMBRE:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.Control = this.cmbGenero;
            this.layoutControlItem6.CustomizationFormText = "GENERO:";
            this.layoutControlItem6.Location = new System.Drawing.Point(172, 123);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem6.Text = "GENERO:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(131, 18);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 31);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(980, 12);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(160, 43);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(12, 200);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(12, 200);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(12, 200);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(566, 43);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(20, 200);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(20, 200);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(20, 200);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.Control = this.txtApellido;
            this.layoutControlItem5.CustomizationFormText = "APELLIDO";
            this.layoutControlItem5.Location = new System.Drawing.Point(586, 83);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem5.Text = "APELLIDO:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.cmbEstadoCivil;
            this.layoutControlItem7.CustomizationFormText = "ESTADO CIVIL:";
            this.layoutControlItem7.FillControlToClientArea = false;
            this.layoutControlItem7.Location = new System.Drawing.Point(586, 123);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem7.Text = "ESTADO CIVIL:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.txtDui;
            this.layoutControlItem2.CustomizationFormText = "DUI:";
            this.layoutControlItem2.Location = new System.Drawing.Point(172, 43);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem2.Text = "DUI:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.Control = this.txtTelefono;
            this.layoutControlItem8.CustomizationFormText = "TELEFONO:";
            this.layoutControlItem8.Location = new System.Drawing.Point(172, 163);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem8.Text = "TELEFONO:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(131, 18);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 243);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(980, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.Control = this.cmbEscolaridad;
            this.layoutControlItem12.CustomizationFormText = "ESCOLARIDAD:";
            this.layoutControlItem12.Location = new System.Drawing.Point(586, 163);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem12.Text = "ESCOLARIDAD:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(131, 18);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem2.CustomizationFormText = "Expedientes";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 253);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 2, 5);
            this.simpleLabelItem2.Size = new System.Drawing.Size(980, 29);
            this.simpleLabelItem2.Text = "Expedientes";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(131, 22);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 282);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(980, 2);
            this.simpleSeparator2.Text = "simpleSeparator2";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.GridExpedientes;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 284);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(980, 309);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.cmbEdad;
            this.layoutControlItem3.CustomizationFormText = "EDAD:";
            this.layoutControlItem3.Location = new System.Drawing.Point(586, 43);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem3.Text = "EDAD:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.picCandidato;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 43);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(160, 200);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(160, 200);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(160, 200);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 3, 3);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.Control = this.txtDepartamento;
            this.layoutControlItem9.CustomizationFormText = "DEPARTAMENTO";
            this.layoutControlItem9.Location = new System.Drawing.Point(172, 203);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem9.Text = "DEPARTAMENTO";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(131, 18);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.Control = this.txtMunicipio;
            this.layoutControlItem10.CustomizationFormText = "MUNICPIO";
            this.layoutControlItem10.Location = new System.Drawing.Point(586, 203);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(394, 40);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 2, 2);
            this.layoutControlItem10.Text = "MUNICIPIO";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(131, 18);
            // 
            // VistaCandidato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.Name = "VistaCandidato";
            this.Size = new System.Drawing.Size(1000, 700);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartamento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCandidato.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEdad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExpedientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEscolaridad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDui.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEstadoCivil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGenero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidadorCandidato)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnGuardar;
        private System.Windows.Forms.ToolStripButton btnBuscar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnAgregarExpediente;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEdad;
        private DevExpress.XtraGrid.GridControl GridExpedientes;
        private DevExpress.XtraGrid.Views.Grid.GridView VistaPrincipal;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEscolaridad;
        private DevExpress.XtraEditors.TextEdit txtTelefono;
        private DevExpress.XtraEditors.TextEdit txtDui;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEstadoCivil;
        private DevExpress.XtraEditors.TextEdit txtApellido;
        private DevExpress.XtraEditors.ComboBoxEdit cmbGenero;
        private DevExpress.XtraEditors.TextEdit txtNombre;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider ValidadorCandidato;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colEvaluador;
        private DevExpress.XtraGrid.Columns.GridColumn colUbicacion;
        private DevExpress.XtraGrid.Columns.GridColumn colEstado;
        private DevExpress.XtraGrid.Columns.GridColumn colPuestoAplicado;
        private DevExpress.XtraGrid.Columns.GridColumn colFecha;
        private DevExpress.XtraEditors.PictureEdit picCandidato;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtMunicipio;
        private DevExpress.XtraEditors.TextEdit txtDepartamento;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
