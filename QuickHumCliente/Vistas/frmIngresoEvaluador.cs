﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuickHumCliente.Vistas
{
    public partial class frmIngresoEvaluador : DevExpress.XtraEditors.XtraForm
    {

        public string Usuario { get; set; }
        public string Password { get; set; }

        public frmIngresoEvaluador()
        {
            InitializeComponent();
            this.DataBindings.Add(new Binding("Usuario", this.txtUsuario, "Text"));
            this.DataBindings.Add(new Binding("Password", this.txtPassword, "Text"));
        }
    }
}