﻿namespace QuickHumCliente.Vistas
{
    partial class frmReportesAdministrativo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnGenerarReporte = new DevExpress.XtraEditors.SimpleButton();
            this.grupoFiltros = new DevExpress.XtraEditors.GroupControl();
            this.cmbOficina = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dateFin = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateInicio = new DevExpress.XtraEditors.DateEdit();
            this.opcionFiltroPruebas = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grupoFiltros)).BeginInit();
            this.grupoFiltros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOficina.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opcionFiltroPruebas.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(405, 48);
            this.ContentedorTituloPestana.TabIndex = 16;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(197, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "Reportes Administrativos";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(382, 2);
            this.label1.TabIndex = 18;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(12, 54);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 22);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Pruebas";
            // 
            // btnGenerarReporte
            // 
            this.btnGenerarReporte.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerarReporte.Appearance.Options.UseFont = true;
            this.btnGenerarReporte.Image = global::QuickHumCliente.Properties.Resources.prueba1;
            this.btnGenerarReporte.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnGenerarReporte.Location = new System.Drawing.Point(293, 293);
            this.btnGenerarReporte.Name = "btnGenerarReporte";
            this.btnGenerarReporte.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnGenerarReporte.Size = new System.Drawing.Size(100, 60);
            this.btnGenerarReporte.TabIndex = 23;
            this.btnGenerarReporte.Text = "Generar";
            this.btnGenerarReporte.Click += new System.EventHandler(this.btnGenerarReporte_Click);
            // 
            // grupoFiltros
            // 
            this.grupoFiltros.AppearanceCaption.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupoFiltros.AppearanceCaption.Options.UseFont = true;
            this.grupoFiltros.Controls.Add(this.cmbOficina);
            this.grupoFiltros.Controls.Add(this.labelControl4);
            this.grupoFiltros.Controls.Add(this.dateFin);
            this.grupoFiltros.Controls.Add(this.labelControl3);
            this.grupoFiltros.Controls.Add(this.labelControl2);
            this.grupoFiltros.Controls.Add(this.dateInicio);
            this.grupoFiltros.Location = new System.Drawing.Point(12, 143);
            this.grupoFiltros.Name = "grupoFiltros";
            this.grupoFiltros.Size = new System.Drawing.Size(381, 144);
            this.grupoFiltros.TabIndex = 25;
            this.grupoFiltros.Text = "Filtros";
            // 
            // cmbOficina
            // 
            this.cmbOficina.Location = new System.Drawing.Point(96, 32);
            this.cmbOficina.Name = "cmbOficina";
            this.cmbOficina.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cmbOficina.Properties.Appearance.Options.UseBorderColor = true;
            this.cmbOficina.Properties.AutoHeight = false;
            this.cmbOficina.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbOficina.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbOficina.Size = new System.Drawing.Size(280, 28);
            this.cmbOficina.TabIndex = 28;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.labelControl4.Location = new System.Drawing.Point(37, 36);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 18);
            this.labelControl4.TabIndex = 27;
            this.labelControl4.Text = "Oficina";
            // 
            // dateFin
            // 
            this.dateFin.EditValue = null;
            this.dateFin.Location = new System.Drawing.Point(96, 100);
            this.dateFin.Name = "dateFin";
            this.dateFin.Properties.AllowFocused = false;
            this.dateFin.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(223)))), ((int)(((byte)(223)))));
            this.dateFin.Properties.Appearance.Options.UseBorderColor = true;
            this.dateFin.Properties.AutoHeight = false;
            this.dateFin.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dateFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.dateFin.Size = new System.Drawing.Size(280, 28);
            this.dateFin.TabIndex = 26;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.labelControl3.Location = new System.Drawing.Point(15, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(65, 18);
            this.labelControl3.TabIndex = 25;
            this.labelControl3.Text = "Fecha final";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Roboto Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.labelControl2.Location = new System.Drawing.Point(5, 70);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 18);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Fecha inicial";
            // 
            // dateInicio
            // 
            this.dateInicio.EditValue = null;
            this.dateInicio.Location = new System.Drawing.Point(96, 66);
            this.dateInicio.Name = "dateInicio";
            this.dateInicio.Properties.AllowFocused = false;
            this.dateInicio.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(223)))), ((int)(((byte)(223)))));
            this.dateInicio.Properties.Appearance.Options.UseBorderColor = true;
            this.dateInicio.Properties.AutoHeight = false;
            this.dateInicio.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dateInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.dateInicio.Size = new System.Drawing.Size(280, 28);
            this.dateInicio.TabIndex = 23;
            // 
            // opcionFiltroPruebas
            // 
            this.opcionFiltroPruebas.EditValue = ((byte)(0));
            this.opcionFiltroPruebas.Location = new System.Drawing.Point(13, 88);
            this.opcionFiltroPruebas.Name = "opcionFiltroPruebas";
            this.opcionFiltroPruebas.Properties.Appearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.opcionFiltroPruebas.Properties.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opcionFiltroPruebas.Properties.Appearance.Options.UseBorderColor = true;
            this.opcionFiltroPruebas.Properties.Appearance.Options.UseFont = true;
            this.opcionFiltroPruebas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.opcionFiltroPruebas.Properties.Columns = 2;
            this.opcionFiltroPruebas.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(0)), "Sucursal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Global")});
            this.opcionFiltroPruebas.Size = new System.Drawing.Size(381, 43);
            this.opcionFiltroPruebas.TabIndex = 28;
            this.opcionFiltroPruebas.SelectedIndexChanged += new System.EventHandler(this.opcionFiltroPruebas_SelectedIndexChanged);
            // 
            // frmReportesAdministrativo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 357);
            this.Controls.Add(this.opcionFiltroPruebas);
            this.Controls.Add(this.grupoFiltros);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.Controls.Add(this.btnGenerarReporte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReportesAdministrativo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reporte";
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grupoFiltros)).EndInit();
            this.grupoFiltros.ResumeLayout(false);
            this.grupoFiltros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOficina.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opcionFiltroPruebas.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private DevExpress.XtraEditors.SimpleButton btnGenerarReporte;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl grupoFiltros;
        private DevExpress.XtraEditors.DateEdit dateFin;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateInicio;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.RadioGroup opcionFiltroPruebas;
        private DevExpress.XtraEditors.ComboBoxEdit cmbOficina;

    }
}