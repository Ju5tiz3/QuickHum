﻿namespace QuickHumCliente.Vistas
{
    partial class frmConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguracion));
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPipg = new DevExpress.XtraEditors.TextEdit();
            this.txtZavic = new DevExpress.XtraEditors.TextEdit();
            this.txtMoss = new DevExpress.XtraEditors.TextEdit();
            this.txtLifo = new DevExpress.XtraEditors.TextEdit();
            this.txtKostick = new DevExpress.XtraEditors.TextEdit();
            this.txtHumansoft = new DevExpress.XtraEditors.TextEdit();
            this.txtCleaver = new DevExpress.XtraEditors.TextEdit();
            this.txtAllport = new DevExpress.XtraEditors.TextEdit();
            this.txtWonderlic = new DevExpress.XtraEditors.TextEdit();
            this.txtTerman = new DevExpress.XtraEditors.TextEdit();
            this.txtRaven = new DevExpress.XtraEditors.TextEdit();
            this.txtIntelsoft = new DevExpress.XtraEditors.TextEdit();
            this.txtDominos = new DevExpress.XtraEditors.TextEdit();
            this.txtBeta = new DevExpress.XtraEditors.TextEdit();
            this.txtBarsit = new DevExpress.XtraEditors.TextEdit();
            this.txtMMPIA = new DevExpress.XtraEditors.TextEdit();
            this.txtMMPI2 = new DevExpress.XtraEditors.TextEdit();
            this.txtIpv = new DevExpress.XtraEditors.TextEdit();
            this.txtCpi = new DevExpress.XtraEditors.TextEdit();
            this.txtLuscher = new DevExpress.XtraEditors.TextEdit();
            this.txtBfq = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt16pf = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoServidor = new DevExpress.XtraEditors.LabelControl();
            this.panelServidorEstado = new DevExpress.XtraEditors.PanelControl();
            this.btnVerificarServidor = new DevExpress.XtraEditors.SimpleButton();
            this.txtIp = new DevExpress.XtraEditors.TextEdit();
            this.tooltips = new DevExpress.Utils.ToolTipController(this.components);
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPipg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZavic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoss.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLifo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKostick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHumansoft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCleaver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWonderlic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRaven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntelsoft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDominos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarsit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMPIA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMPI2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIpv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuscher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBfq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt16pf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelServidorEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIp.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(632, 48);
            this.ContentedorTituloPestana.TabIndex = 4;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(114, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "Configuracion";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Open Sans", 9.75F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 48);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(632, 646);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.btnCancelar);
            this.xtraTabPage1.Controls.Add(this.btnGuardar);
            this.xtraTabPage1.Controls.Add(this.labelControl26);
            this.xtraTabPage1.Controls.Add(this.tableLayoutPanel1);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.lblEstadoServidor);
            this.xtraTabPage1.Controls.Add(this.panelServidorEstado);
            this.xtraTabPage1.Controls.Add(this.btnVerificarServidor);
            this.xtraTabPage1.Controls.Add(this.txtIp);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(626, 613);
            this.xtraTabPage1.Text = "General";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(539, 583);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(458, 583);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 11;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.labelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl26.Location = new System.Drawing.Point(12, 531);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl26.Size = new System.Drawing.Size(602, 32);
            this.labelControl26.TabIndex = 10;
            this.labelControl26.Text = "La modificacion de los nombres de las pruebas puede alterar el comportamiento cor" +
    "recto de la aplicacion";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.Controls.Add(this.txtPipg, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtZavic, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtMoss, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtLifo, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtKostick, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtHumansoft, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtCleaver, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtAllport, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtWonderlic, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtTerman, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtRaven, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtIntelsoft, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtDominos, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtBeta, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtBarsit, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtMMPIA, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtMMPI2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtIpv, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCpi, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtLuscher, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtBfq, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl25, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.labelControl24, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.labelControl23, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.labelControl22, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelControl21, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.labelControl20, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl19, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl18, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl17, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl16, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl15, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl14, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.labelControl13, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelControl12, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.labelControl11, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl10, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl8, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt16pf, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 182);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(602, 343);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // txtPipg
            // 
            this.txtPipg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPipg.Location = new System.Drawing.Point(393, 313);
            this.txtPipg.Name = "txtPipg";
            this.txtPipg.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtPipg.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPipg.Properties.AutoHeight = false;
            this.txtPipg.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtPipg.Size = new System.Drawing.Size(206, 27);
            this.txtPipg.TabIndex = 43;
            // 
            // txtZavic
            // 
            this.txtZavic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtZavic.Location = new System.Drawing.Point(93, 313);
            this.txtZavic.Name = "txtZavic";
            this.txtZavic.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtZavic.Properties.Appearance.Options.UseBorderColor = true;
            this.txtZavic.Properties.AutoHeight = false;
            this.txtZavic.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtZavic.Size = new System.Drawing.Size(204, 27);
            this.txtZavic.TabIndex = 42;
            // 
            // txtMoss
            // 
            this.txtMoss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMoss.Location = new System.Drawing.Point(393, 282);
            this.txtMoss.Name = "txtMoss";
            this.txtMoss.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtMoss.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMoss.Properties.AutoHeight = false;
            this.txtMoss.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMoss.Size = new System.Drawing.Size(206, 25);
            this.txtMoss.TabIndex = 41;
            // 
            // txtLifo
            // 
            this.txtLifo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLifo.Location = new System.Drawing.Point(93, 282);
            this.txtLifo.Name = "txtLifo";
            this.txtLifo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtLifo.Properties.Appearance.Options.UseBorderColor = true;
            this.txtLifo.Properties.AutoHeight = false;
            this.txtLifo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtLifo.Size = new System.Drawing.Size(204, 25);
            this.txtLifo.TabIndex = 40;
            // 
            // txtKostick
            // 
            this.txtKostick.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKostick.Location = new System.Drawing.Point(393, 251);
            this.txtKostick.Name = "txtKostick";
            this.txtKostick.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtKostick.Properties.Appearance.Options.UseBorderColor = true;
            this.txtKostick.Properties.AutoHeight = false;
            this.txtKostick.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtKostick.Size = new System.Drawing.Size(206, 25);
            this.txtKostick.TabIndex = 39;
            // 
            // txtHumansoft
            // 
            this.txtHumansoft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtHumansoft.Location = new System.Drawing.Point(93, 251);
            this.txtHumansoft.Name = "txtHumansoft";
            this.txtHumansoft.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtHumansoft.Properties.Appearance.Options.UseBorderColor = true;
            this.txtHumansoft.Properties.AutoHeight = false;
            this.txtHumansoft.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtHumansoft.Size = new System.Drawing.Size(204, 25);
            this.txtHumansoft.TabIndex = 38;
            // 
            // txtCleaver
            // 
            this.txtCleaver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCleaver.Location = new System.Drawing.Point(393, 220);
            this.txtCleaver.Name = "txtCleaver";
            this.txtCleaver.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtCleaver.Properties.Appearance.Options.UseBorderColor = true;
            this.txtCleaver.Properties.AutoHeight = false;
            this.txtCleaver.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtCleaver.Size = new System.Drawing.Size(206, 25);
            this.txtCleaver.TabIndex = 37;
            // 
            // txtAllport
            // 
            this.txtAllport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAllport.Location = new System.Drawing.Point(93, 220);
            this.txtAllport.Name = "txtAllport";
            this.txtAllport.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtAllport.Properties.Appearance.Options.UseBorderColor = true;
            this.txtAllport.Properties.AutoHeight = false;
            this.txtAllport.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtAllport.Size = new System.Drawing.Size(204, 25);
            this.txtAllport.TabIndex = 36;
            // 
            // txtWonderlic
            // 
            this.txtWonderlic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWonderlic.Location = new System.Drawing.Point(393, 189);
            this.txtWonderlic.Name = "txtWonderlic";
            this.txtWonderlic.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtWonderlic.Properties.Appearance.Options.UseBorderColor = true;
            this.txtWonderlic.Properties.AutoHeight = false;
            this.txtWonderlic.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtWonderlic.Size = new System.Drawing.Size(206, 25);
            this.txtWonderlic.TabIndex = 35;
            // 
            // txtTerman
            // 
            this.txtTerman.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTerman.Location = new System.Drawing.Point(93, 189);
            this.txtTerman.Name = "txtTerman";
            this.txtTerman.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtTerman.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTerman.Properties.AutoHeight = false;
            this.txtTerman.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtTerman.Size = new System.Drawing.Size(204, 25);
            this.txtTerman.TabIndex = 34;
            // 
            // txtRaven
            // 
            this.txtRaven.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRaven.Location = new System.Drawing.Point(393, 158);
            this.txtRaven.Name = "txtRaven";
            this.txtRaven.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtRaven.Properties.Appearance.Options.UseBorderColor = true;
            this.txtRaven.Properties.AutoHeight = false;
            this.txtRaven.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtRaven.Size = new System.Drawing.Size(206, 25);
            this.txtRaven.TabIndex = 33;
            // 
            // txtIntelsoft
            // 
            this.txtIntelsoft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntelsoft.Location = new System.Drawing.Point(93, 158);
            this.txtIntelsoft.Name = "txtIntelsoft";
            this.txtIntelsoft.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtIntelsoft.Properties.Appearance.Options.UseBorderColor = true;
            this.txtIntelsoft.Properties.AutoHeight = false;
            this.txtIntelsoft.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtIntelsoft.Size = new System.Drawing.Size(204, 25);
            this.txtIntelsoft.TabIndex = 32;
            // 
            // txtDominos
            // 
            this.txtDominos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDominos.Location = new System.Drawing.Point(393, 127);
            this.txtDominos.Name = "txtDominos";
            this.txtDominos.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtDominos.Properties.Appearance.Options.UseBorderColor = true;
            this.txtDominos.Properties.AutoHeight = false;
            this.txtDominos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtDominos.Size = new System.Drawing.Size(206, 25);
            this.txtDominos.TabIndex = 31;
            // 
            // txtBeta
            // 
            this.txtBeta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBeta.Location = new System.Drawing.Point(93, 127);
            this.txtBeta.Name = "txtBeta";
            this.txtBeta.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtBeta.Properties.Appearance.Options.UseBorderColor = true;
            this.txtBeta.Properties.AutoHeight = false;
            this.txtBeta.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtBeta.Size = new System.Drawing.Size(204, 25);
            this.txtBeta.TabIndex = 30;
            // 
            // txtBarsit
            // 
            this.txtBarsit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBarsit.Location = new System.Drawing.Point(393, 96);
            this.txtBarsit.Name = "txtBarsit";
            this.txtBarsit.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtBarsit.Properties.Appearance.Options.UseBorderColor = true;
            this.txtBarsit.Properties.AutoHeight = false;
            this.txtBarsit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtBarsit.Size = new System.Drawing.Size(206, 25);
            this.txtBarsit.TabIndex = 29;
            // 
            // txtMMPIA
            // 
            this.txtMMPIA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMMPIA.Location = new System.Drawing.Point(93, 96);
            this.txtMMPIA.Name = "txtMMPIA";
            this.txtMMPIA.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtMMPIA.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMMPIA.Properties.AutoHeight = false;
            this.txtMMPIA.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMMPIA.Size = new System.Drawing.Size(204, 25);
            this.txtMMPIA.TabIndex = 28;
            // 
            // txtMMPI2
            // 
            this.txtMMPI2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMMPI2.Location = new System.Drawing.Point(393, 65);
            this.txtMMPI2.Name = "txtMMPI2";
            this.txtMMPI2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtMMPI2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMMPI2.Properties.AutoHeight = false;
            this.txtMMPI2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMMPI2.Size = new System.Drawing.Size(206, 25);
            this.txtMMPI2.TabIndex = 27;
            // 
            // txtIpv
            // 
            this.txtIpv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIpv.Location = new System.Drawing.Point(93, 65);
            this.txtIpv.Name = "txtIpv";
            this.txtIpv.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtIpv.Properties.Appearance.Options.UseBorderColor = true;
            this.txtIpv.Properties.AutoHeight = false;
            this.txtIpv.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtIpv.Size = new System.Drawing.Size(204, 25);
            this.txtIpv.TabIndex = 26;
            // 
            // txtCpi
            // 
            this.txtCpi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCpi.Location = new System.Drawing.Point(393, 34);
            this.txtCpi.Name = "txtCpi";
            this.txtCpi.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtCpi.Properties.Appearance.Options.UseBorderColor = true;
            this.txtCpi.Properties.AutoHeight = false;
            this.txtCpi.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtCpi.Size = new System.Drawing.Size(206, 25);
            this.txtCpi.TabIndex = 25;
            // 
            // txtLuscher
            // 
            this.txtLuscher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLuscher.Location = new System.Drawing.Point(93, 34);
            this.txtLuscher.Name = "txtLuscher";
            this.txtLuscher.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtLuscher.Properties.Appearance.Options.UseBorderColor = true;
            this.txtLuscher.Properties.AutoHeight = false;
            this.txtLuscher.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtLuscher.Size = new System.Drawing.Size(204, 25);
            this.txtLuscher.TabIndex = 24;
            // 
            // txtBfq
            // 
            this.txtBfq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBfq.Location = new System.Drawing.Point(393, 3);
            this.txtBfq.Name = "txtBfq";
            this.txtBfq.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtBfq.Properties.Appearance.Options.UseBorderColor = true;
            this.txtBfq.Properties.AutoHeight = false;
            this.txtBfq.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtBfq.Size = new System.Drawing.Size(206, 25);
            this.txtBfq.TabIndex = 23;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl25.Location = new System.Drawing.Point(303, 313);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(84, 27);
            this.labelControl25.TabIndex = 21;
            this.labelControl25.Text = "PIPG";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl24.Location = new System.Drawing.Point(3, 313);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(84, 27);
            this.labelControl24.TabIndex = 20;
            this.labelControl24.Text = "ZAVIC";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl23.Location = new System.Drawing.Point(303, 282);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(84, 25);
            this.labelControl23.TabIndex = 19;
            this.labelControl23.Text = "MOSS";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl22.Location = new System.Drawing.Point(303, 251);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(84, 25);
            this.labelControl22.TabIndex = 18;
            this.labelControl22.Text = "KOSTICK";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl21.Location = new System.Drawing.Point(303, 220);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(84, 25);
            this.labelControl21.TabIndex = 17;
            this.labelControl21.Text = "CLEAVER";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl20.Location = new System.Drawing.Point(303, 189);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(84, 25);
            this.labelControl20.TabIndex = 16;
            this.labelControl20.Text = "WONDERLIC";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl19.Location = new System.Drawing.Point(303, 158);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(84, 25);
            this.labelControl19.TabIndex = 15;
            this.labelControl19.Text = "RAVEN";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl18.Location = new System.Drawing.Point(303, 127);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(84, 25);
            this.labelControl18.TabIndex = 14;
            this.labelControl18.Text = "DOMINOS";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(303, 96);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(84, 25);
            this.labelControl17.TabIndex = 13;
            this.labelControl17.Text = "BARSIT";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl16.Location = new System.Drawing.Point(303, 65);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(84, 25);
            this.labelControl16.TabIndex = 12;
            this.labelControl16.Text = "MMPI-2";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl15.Location = new System.Drawing.Point(303, 34);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(84, 25);
            this.labelControl15.TabIndex = 11;
            this.labelControl15.Text = "CPI";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl14.Location = new System.Drawing.Point(3, 282);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(84, 25);
            this.labelControl14.TabIndex = 10;
            this.labelControl14.Text = "LIFO";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl13.Location = new System.Drawing.Point(3, 251);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(84, 25);
            this.labelControl13.TabIndex = 9;
            this.labelControl13.Text = "HUMANSOFT";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl12.Location = new System.Drawing.Point(3, 220);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(84, 25);
            this.labelControl12.TabIndex = 8;
            this.labelControl12.Text = "ALLPORT";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl11.Location = new System.Drawing.Point(3, 189);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(84, 25);
            this.labelControl11.TabIndex = 7;
            this.labelControl11.Text = "TERMAN";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl10.Location = new System.Drawing.Point(3, 158);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(84, 25);
            this.labelControl10.TabIndex = 6;
            this.labelControl10.Text = "INTELSOFT";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(3, 127);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(84, 25);
            this.labelControl9.TabIndex = 5;
            this.labelControl9.Text = "BETA III";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(3, 96);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(84, 25);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "MMPI-A";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(3, 65);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(84, 25);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "IPV";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl6.Location = new System.Drawing.Point(3, 34);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(84, 25);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "LUSCHER";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(3, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(84, 25);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "16PF";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(303, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(84, 25);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "BFQ";
            // 
            // txt16pf
            // 
            this.txt16pf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt16pf.Location = new System.Drawing.Point(93, 3);
            this.txt16pf.Name = "txt16pf";
            this.txt16pf.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txt16pf.Properties.Appearance.Options.UseBorderColor = true;
            this.txt16pf.Properties.AutoHeight = false;
            this.txt16pf.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txt16pf.Size = new System.Drawing.Size(204, 25);
            this.txt16pf.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(11, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(603, 2);
            this.label2.TabIndex = 8;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Roboto Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(11, 140);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(224, 21);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Configuracion Nombre Pruebas";
            // 
            // lblEstadoServidor
            // 
            this.lblEstadoServidor.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoServidor.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblEstadoServidor.Location = new System.Drawing.Point(526, 101);
            this.lblEstadoServidor.Name = "lblEstadoServidor";
            this.lblEstadoServidor.Size = new System.Drawing.Size(88, 15);
            this.lblEstadoServidor.TabIndex = 6;
            this.lblEstadoServidor.Text = "FUERA DE LINEA";
            // 
            // panelServidorEstado
            // 
            this.panelServidorEstado.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(40)))), ((int)(((byte)(0)))));
            this.panelServidorEstado.Appearance.Options.UseBackColor = true;
            this.panelServidorEstado.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelServidorEstado.Location = new System.Drawing.Point(496, 98);
            this.panelServidorEstado.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelServidorEstado.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelServidorEstado.Name = "panelServidorEstado";
            this.panelServidorEstado.Size = new System.Drawing.Size(20, 20);
            this.panelServidorEstado.TabIndex = 5;
            // 
            // btnVerificarServidor
            // 
            this.btnVerificarServidor.Location = new System.Drawing.Point(380, 96);
            this.btnVerificarServidor.Name = "btnVerificarServidor";
            this.btnVerificarServidor.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnVerificarServidor.Size = new System.Drawing.Size(108, 24);
            this.btnVerificarServidor.TabIndex = 4;
            this.btnVerificarServidor.Text = "Verificar Servicio";
            this.btnVerificarServidor.Click += new System.EventHandler(this.btnVerificarServidor_Click);
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(105, 60);
            this.txtIp.Name = "txtIp";
            this.txtIp.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtIp.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.txtIp.Properties.Appearance.Options.UseBackColor = true;
            this.txtIp.Properties.Appearance.Options.UseBorderColor = true;
            this.txtIp.Properties.AutoHeight = false;
            this.txtIp.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtIp.Properties.Mask.SaveLiteral = false;
            this.txtIp.Properties.Mask.ShowPlaceHolders = false;
            this.txtIp.Size = new System.Drawing.Size(509, 24);
            this.txtIp.TabIndex = 3;
            this.txtIp.ToolTip = resources.GetString("txtIp.ToolTip");
            this.txtIp.ToolTipController = this.tooltips;
            this.txtIp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.txtIp.ToolTipTitle = "Configuración Dirección";
            // 
            // tooltips
            // 
            this.tooltips.AllowHtmlText = true;
            this.tooltips.AutoPopDelay = 2000;
            this.tooltips.IconSize = DevExpress.Utils.ToolTipIconSize.Large;
            this.tooltips.ToolTipStyle = DevExpress.Utils.ToolTipStyle.Windows7;
            this.tooltips.ToolTipType = DevExpress.Utils.ToolTipType.Standard;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(11, 64);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(61, 15);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Direccion IP";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(11, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(603, 2);
            this.label1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Roboto Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(11, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(161, 21);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Configuracion Servicio";
            // 
            // frmConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 694);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmConfiguracion";
            this.Text = "Configuraciones";
            this.Load += new System.EventHandler(this.frmConfiguracion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPipg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZavic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoss.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLifo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKostick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHumansoft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCleaver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWonderlic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRaven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntelsoft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDominos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarsit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMPIA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMPI2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIpv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuscher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBfq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt16pf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelServidorEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIp.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtIp;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl panelServidorEstado;
        private DevExpress.XtraEditors.SimpleButton btnVerificarServidor;
        private DevExpress.XtraEditors.LabelControl lblEstadoServidor;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txtPipg;
        private DevExpress.XtraEditors.TextEdit txtZavic;
        private DevExpress.XtraEditors.TextEdit txtMoss;
        private DevExpress.XtraEditors.TextEdit txtLifo;
        private DevExpress.XtraEditors.TextEdit txtKostick;
        private DevExpress.XtraEditors.TextEdit txtHumansoft;
        private DevExpress.XtraEditors.TextEdit txtCleaver;
        private DevExpress.XtraEditors.TextEdit txtAllport;
        private DevExpress.XtraEditors.TextEdit txtWonderlic;
        private DevExpress.XtraEditors.TextEdit txtTerman;
        private DevExpress.XtraEditors.TextEdit txtRaven;
        private DevExpress.XtraEditors.TextEdit txtIntelsoft;
        private DevExpress.XtraEditors.TextEdit txtDominos;
        private DevExpress.XtraEditors.TextEdit txtBeta;
        private DevExpress.XtraEditors.TextEdit txtBarsit;
        private DevExpress.XtraEditors.TextEdit txtMMPIA;
        private DevExpress.XtraEditors.TextEdit txtMMPI2;
        private DevExpress.XtraEditors.TextEdit txtIpv;
        private DevExpress.XtraEditors.TextEdit txtCpi;
        private DevExpress.XtraEditors.TextEdit txtLuscher;
        private DevExpress.XtraEditors.TextEdit txtBfq;
        private DevExpress.XtraEditors.TextEdit txt16pf;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.Utils.ToolTipController tooltips;
    }
}