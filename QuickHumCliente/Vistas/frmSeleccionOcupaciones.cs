﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;
using DevExpress.XtraGrid.Views.Grid;
using System.ServiceModel;

namespace QuickHumCliente.Vistas
{
    public partial class frmSeleccionOcupaciones : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Ocupacion seleccionada
        /// </summary>
        public ServicioQuickHum.Ocupacion Ocupacion { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public frmSeleccionOcupaciones()
        {
            InitializeComponent();
            CargarOcupaciones();
        }

        /// <summary>
        /// Carga en un grid las ocupaciones
        /// </summary>
        private void CargarOcupaciones()
        {
            BOLOcupacion negocio_ocupacion = new BOLOcupacion();
            try
            {
                GridOcupaciones.DataSource = negocio_ocupacion.ListarOcupaciones();
                GridView vista_principal = (GridView)GridOcupaciones.MainView;
                vista_principal.Columns[0].Width = 100;
                vista_principal.Columns[0].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                vista_principal.Columns[1].Width = 200;
                vista_principal.Columns[2].Width = 500;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un error al cargar las ocupaciones: " + ex, "Error carga de ocupaciones", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            GridView vista_actual = (GridView)GridOcupaciones.MainView;
            if (vista_actual.SelectedRowsCount == 0)
            {
                XtraMessageBox.Show("No ha seleccionado ninguna ocupacion", "Seleccion nula", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Ocupacion = (ServicioQuickHum.Ocupacion)vista_actual.GetRow(vista_actual.GetSelectedRows()[0]);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void GridOcupaciones_DoubleClick(object sender, EventArgs e)
        {
            GridView vista_actual = (GridView)GridOcupaciones.MainView;
            if (vista_actual.SelectedRowsCount != 0)
            {
                InvokeOnClick(btnSeleccionar, null);
            }
        }
    }
}