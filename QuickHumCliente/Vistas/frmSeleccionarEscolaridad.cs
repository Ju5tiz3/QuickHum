﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmSeleccionarEscolaridad : DevExpress.XtraEditors.XtraForm
    {
        public EscolaridadPrueba Escolaridad { get; set; }
        public frmSeleccionarEscolaridad(Prueba prueba)
        {
            InitializeComponent();
            CargarEscolaridades(prueba);
        }

        private void CargarEscolaridades(Prueba prueba)
        {
            BOLPrueba bol_prueba = new BOLPrueba();
            try
            {
                EscolaridadPrueba[] escolaridades = bol_prueba.ObtenerEscolaridadesPrueba(prueba);
                if (escolaridades == null)
                {
                    XtraMessageBox.Show("No existen escolaridades asignadas a esta prueba", "Escolaridades",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
                foreach (EscolaridadPrueba escolaridad in escolaridades)
                {
                    cmbEscolaridades.Properties.Items.Add(escolaridad);
                }
                cmbEscolaridades.SelectedIndex = 0;
                return;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al recuperar las escolaridades de la prueba: " + ex.Message,
                    "Escolaridades", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al recuperar las escolaridades de la prueba: " + ex.Message,
                    "Escolaridades", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this.Close();

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Escolaridad = (EscolaridadPrueba)cmbEscolaridades.SelectedItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmSeleccionarEscolaridad_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Escolaridad == null)
                e.Cancel = true;}
    }
}