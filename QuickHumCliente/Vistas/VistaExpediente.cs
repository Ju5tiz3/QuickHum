﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;
using QuickHumCliente.Vistas.Pruebas.Allport;
using QuickHumCliente.Vistas.Pruebas.Barsit;
using QuickHumCliente.Vistas.Pruebas.BETA_IIR;
using QuickHumCliente.Vistas.Pruebas.Bfq;
using QuickHumCliente.Vistas.Pruebas.Cleaver;
using QuickHumCliente.Vistas.Pruebas.CPI;
using QuickHumCliente.Vistas.Pruebas.Dominos;
using QuickHumCliente.Vistas.Pruebas.Intelsoft;
using QuickHumCliente.Vistas.Pruebas.Kostick;
using QuickHumCliente.Vistas.Pruebas.Lifo;
using QuickHumCliente.Vistas.Pruebas.Luscher;
using QuickHumCliente.Vistas.Pruebas.MMPIA;
using QuickHumCliente.Vistas.Pruebas.MMPIA2;
using QuickHumCliente.Vistas.Pruebas.Moss;
using QuickHumCliente.Vistas.Pruebas.PF16;
using QuickHumCliente.Vistas.Pruebas.PIPG;
using QuickHumCliente.Vistas.Pruebas.Raven;
using QuickHumCliente.Vistas.Pruebas.TERMAN;
using QuickHumCliente.Vistas.Pruebas.Wonderlic;
using QuickHumCliente.Vistas.Pruebas.Zavic;

namespace QuickHumCliente.Vistas
{
    public partial class VistaExpediente : DevExpress.XtraEditors.XtraUserControl
    {
        private bool _modo_actualizacion = false;
        XtraTabControl TabControlParent { get; set; }
        public ServicioQuickHum.Expediente Expediente { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public VistaExpediente()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Consturctor
        /// </summary>
        /// <param name="padre"></param>
        public VistaExpediente(XtraTabControl padre, Expediente expediente = null)
        {
            InitializeComponent();
            this.TabControlParent = padre;
            this.Dock = DockStyle.Fill;
            // Cojemos los datos del expediente pasado, y si este es nulo, entonces creamos un expediente vacion con ciertos valores por defecto
            this.Expediente = expediente ?? ( expediente = new Expediente()
            {
                Evaluador = Globales.Evaluador,
                Ubicacion = Globales.Oficina,
            });
            // Evaluamos si el expediente posee ya un numero asignado, es decir, existe.
            if (this.Expediente.Numero != 0)
            {
                SwitchActualizacionUI(true);
            }
            tstripMenu.Renderer = new MyRenderer();
            ActualizarCampos();
        }

        /// <summary>
        /// Actualiza los campos con los valores del objeto expediente
        /// </summary>
        private void ActualizarCampos()
        {
            txtNumero.Text = this.Expediente.Numero.ToString() ?? "0";
            txtDui.Text = this.Expediente.Candidato.Dui;
            txtNombre.Text = this.Expediente.Candidato.Nombre;
            txtApellido.Text = this.Expediente.Candidato.Apellido;
            txtPuesto.Text = this.Expediente.PuestoAplicado;
            txtEvaluador.Text = this.Expediente.Evaluador.Nombre;
            txtOficina.Text = this.Expediente.Ubicacion.Nombre;
            txtOcupacion.Text = this.Expediente.Ocupacion.Nombre;
            txtEstado.Text = Enum.GetName(typeof(ServicioQuickHum.Expediente.EstadoExpediente), ServicioQuickHum.Expediente.EstadoExpediente.ABIERTO);
            GridPruebasAsignadas.DataSource = this.Expediente.PruebasAsignadas;
        }

        /// <summary>
        /// Actualiza los valores de las propiedades del expediente con los valores de los campos
        /// </summary>
        private void ActualizarObjetoExpediente()
        {
            this.Expediente.PuestoAplicado = txtPuesto.Text.ToUpper();
        }

        /********************* EVENTOS CONTROLES ******************/

        private void btnMostrarOcupaciones_Click(object sender, EventArgs e)
        {
            frmSeleccionOcupaciones frm_ocupaciones = new frmSeleccionOcupaciones();
            DialogResult resultado = frm_ocupaciones.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                this.Expediente.Ocupacion = frm_ocupaciones.Ocupacion;
                this.txtOcupacion.Text = this.Expediente.Ocupacion.Nombre;
            }
        }
        
        private void tstNuevo_Click(object sender, EventArgs e)
        {
            LimpiarExpediente();
            SwitchActualizacionUI(false);
        }

        private void tstGuardar_Click(object sender, EventArgs e)
        {
            if (dxValidador.Validate() == false)
            {
                XtraMessageBox.Show("Existen errores en los campos, favor corregirlos.", "Error en validacion de campos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Expediente.PruebasAsignadas == null ||  Expediente.PruebasAsignadas.Length == 0)
            {
                XtraMessageBox.Show("No ha asignado ninguna prueba al expediente", "Error en validacion de campos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ActualizarObjetoExpediente();
            BOLExpediente bol_expediente = new BOLExpediente();
            try
            {
                if (this._modo_actualizacion)
                {
                    if (bol_expediente.ActualizarAnotacionesExpediente(this.Expediente.Numero,
                        this.Expediente.Anotaciones))
                        XtraMessageBox.Show("Expediente actualizado", "Actualización Expediente", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    return;
                }

                if (!bol_expediente.NuevoExpediente(this.Expediente)) return;
                XtraMessageBox.Show("Expediente registrado con exito", "Registro expediente", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.TabControlParent.TabPages.Remove(this.TabControlParent.SelectedTabPage);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Hubo un error al guardar el expediente: " + ex.Message, "Error en registro de expediente",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAgregarPruebas_Click(object sender, EventArgs e){
            frmSeleccionPruebas vista_seleccion_prueba = new frmSeleccionPruebas();
            if (vista_seleccion_prueba.IsDisposed)
                return;
            vista_seleccion_prueba.ShowDialog();
            if (vista_seleccion_prueba.DialogResult != DialogResult.OK)
            {
                return;
            }
            List<ServicioQuickHum.PruebaAsignada> pruebas_asignadas = new List<PruebaAsignada>();
            foreach (var prueba in vista_seleccion_prueba.pruebas_activas)
            {
                pruebas_asignadas.Add(new PruebaAsignada()
                {
                    TipoPrueba = prueba.Key,
                    Escolaridad = prueba.Value
                });
            }
            this.Expediente.PruebasAsignadas = pruebas_asignadas.ToArray();
            GridPruebasAsignadas.DataSource = this.Expediente.PruebasAsignadas;
        }
        
        private void btnMostrarCandidato_Click(object sender, EventArgs e)
        {
            frmBuscarCandidato frm_buscar_candidato = new frmBuscarCandidato();
            frm_buscar_candidato.ShowDialog();
            if (frm_buscar_candidato.DialogResult == DialogResult.OK)
            {
                BOLCandidato bol_candidato = new BOLCandidato();
                try
                {
                    this.Expediente.Candidato = bol_candidato.CargarCandidato(frm_buscar_candidato.Dui);
                    txtDui.Text = this.Expediente.Candidato.Dui;
                    txtNombre.Text = this.Expediente.Candidato.Nombre;
                    txtApellido.Text = this.Expediente.Candidato.Apellido;
                }
                catch (FaultException ex)
                {
                    XtraMessageBox.Show("Hubo un error al cargar el candidato: " + ex.Message,
                        "Error en busqueda de candidato", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void botones_resultado_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            PruebaAsignada prueba_asignada = (PruebaAsignada)((GridView) GridPruebasAsignadas.MainView).GetFocusedRow();
            XtraMessageBox.Show(prueba_asignada.TipoPrueba.Nombre);
            BOLPrueba bol_prueba = new BOLPrueba();
            try
            {
                int numero = bol_prueba.ObtenerNumeroPrueba(prueba_asignada.TipoPrueba.Nombre, this.Expediente.Numero);
                CargarFormularioReporte(prueba_asignada.TipoPrueba.Nombre, numero, this.Expediente.Numero);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la busqueda de la prueba: " + ex.Message, "Busqueda prueba",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema en la busqueda de la prueba: " + ex.Message, "Busqueda prueba",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void CargarFormularioReporte(string prueba_nombre, int numero_prueba, int numero_expediente)
        {
            BOLPrueba bol_prueba = new BOLPrueba();
            try
            {
                Test test = bol_prueba.ObtenerEnumeracionPrueba(prueba_nombre);
                switch (test)
                {
                    case Test.IPV:
                        XtraMessageBox.Show("Carga reporte de prueba IPV numero: " + numero_prueba.ToString(),
                            "Carga reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        new Pruebas.IPV.frmIPVreporte(this.Expediente).ShowDialog();
                        break;
                    case Test.ALLPORT:
                        new frmReporteAllport(this.Expediente).ShowDialog();
                        break;
                    case Test.BARSIT:
                        new frmReporteBarsit(this.Expediente).ShowDialog();
                        break;
                    case Test.BFQ:
                        new frmBFQReporte(this.Expediente).ShowDialog();
                        break;
                    case Test.DOMINOS:
                        new frmReporteDominos(this.Expediente).ShowDialog();
                        break;
                    case Test.KOSTICK:
                        new frmReporteKostick(this.Expediente).ShowDialog();
                        break;
                    case Test.MMPIA:
                        new frmReporteMMPI(this.Expediente).ShowDialog();
                        break;
                    case Test.MMPI2:
                        new frmRerporteMMPIA2(this.Expediente).ShowDialog();
                        break;
                    case Test.MOSS:
                        new frmReporteMoss(this.Expediente).ShowDialog();
                        break;
                    case Test.PIPG:
                        new frmPIPGReporte(this.Expediente).ShowDialog();
                        break;
                    case Test.RAVEN:
                        new frmReporteRaven(this.Expediente).ShowDialog();
                        break;
                    case Test.TERMAN:
                        new frmTerman_reporte(this.Expediente).ShowDialog();
                        break;
                    case Test.ZAVIC:
                        new frmZavicreporte(this.Expediente).ShowDialog();
                        break;
                    case Test.BETAIII:
                        new frmReporteBetaIIR(this.Expediente).ShowDialog();
			            break;
                    case Test.CPI:
                        new frmReporteCPI(this.Expediente).ShowDialog();
                        break;
                    case Test.LIFO:
                        new frmReporteLifo(this.Expediente).ShowDialog();
                        break;
                    case Test.WONDERLIC:
                        new frmWonderlicReporte(this.Expediente).ShowDialog();
                        break;
                    case Test.CLEAVER:
                        new frmCleaverreporte(this.Expediente).ShowDialog();
                        break;
                    case Test.INTELSOFT:
                        new frmIntelsoftReporte(this.Expediente).ShowDialog();
                        break;
                    case Test.PF16:
                        new frmReportePF16(this.Expediente).ShowDialog();
                        break;
                    case Test.LUSCHER:
                        new frmLuscherReporte(this.Expediente).ShowDialog();
                        break;
                }
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void tstBuscar_Click(object sender, EventArgs e)
        {
            frmSeleccionarNumeroExpediente frm_seleccionar_expediente = new frmSeleccionarNumeroExpediente();
            if (frm_seleccionar_expediente.ShowDialog() != DialogResult.OK)
                return;
            BOLExpediente bol_expediente = new BOLExpediente();
            try
            {
                Expediente expediente = bol_expediente.CargarExpediente(frm_seleccionar_expediente.NumeroExpediente);
                this.Expediente = expediente;
                XtraMessageBox.Show("El expediente se cargo con exito.", "Carga expediente", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                ActualizarCampos();
                SwitchActualizacionUI(true);
                return;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar el expediente: " + ex.Message, "Carga expediente",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al cargar el expediente: " + ex.Message, "Carga expediente",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /******************** HELPER *****************************/

        private void SwitchActualizacionUI(bool modo_actualizacion)
        {
            if (modo_actualizacion)
            {
                tstGuardar.Text = "Actualizar";
                tstAgregarPruebas.Enabled = false;
                tstCambiarEstadoExpediente.Enabled = true;
                this._modo_actualizacion = true;
                return;
            }
            tstGuardar.Text = "Guardar";
            tstAgregarPruebas.Enabled = true;
            tstCambiarEstadoExpediente.Enabled = false;
            _modo_actualizacion = false;
        }

        private void LimpiarExpediente()
        {
            Expediente = new Expediente();
            txtNumero.Text = string.Empty;
            txtDui.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtApellido.Text = string.Empty;txtOcupacion.Text = string.Empty;
            txtEstado.Text = Enum.GetName(typeof(ServicioQuickHum.Expediente.EstadoExpediente), ServicioQuickHum.Expediente.EstadoExpediente.ABIERTO);
            this.Expediente.Estado = Expediente.EstadoExpediente.ABIERTO;
            btnMostrarCandidato.Enabled = true;
            GridPruebasAsignadas.DataSource = null;
        }

        /// <summary>
        /// Presenta una pantalla para ingresar o visualizar las anotaciones de un expediente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstAnotacion_Click(object sender, EventArgs e)
        {
            frmAnotacionesExpediente frm_anotaciones = new frmAnotacionesExpediente(this.Expediente.Anotaciones);
            if (frm_anotaciones.ShowDialog() == DialogResult.No)
                return;
            this.Expediente.Anotaciones = frm_anotaciones.Anotaciones;
        }

        /// <summary>
        /// Cambia el estado del expediente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarEstadoExpediente(object sender, EventArgs e)
        {
            if (
                XtraMessageBox.Show("¿Esta seguro de cambiar el estado del expediente?", "Actualización estado",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            ToolStripItem boton = (ToolStripItem) sender;
            BOLExpediente bol_expediente = new BOLExpediente();
            try
            {
                switch (boton.Name)
                {
                    case "tstbtnAnulado":
                        bol_expediente.CambiarEstadoExpediente(this.Expediente.Numero, Expediente.EstadoExpediente.NULO);
                        break;
                    case "tstbtnCerrado":
                        bol_expediente.CambiarEstadoExpediente(this.Expediente.Numero,
                            Expediente.EstadoExpediente.CERRADO);
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message, "Actualización estado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            XtraMessageBox.Show("El estado del expediente ha sido cambiado satisfactoriamente.", "Actualización estado", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Muestra el formulario que contiene el reporte del expediente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tstImprimirReporte_Click(object sender, EventArgs e)
        {
            if (this.Expediente.PruebasAsignadas == null)
            {
                XtraMessageBox.Show("El expediente esta vacío.", "Imprimir", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            new frmReporteExpediente(this.Expediente).ShowDialog();
        }
    }

    public class MyRenderer : ToolStripProfessionalRenderer
    {
        protected override void OnRenderDropDownButtonBackground(ToolStripItemRenderEventArgs e)
        {
            var btn = e.Item as ToolStripDropDownButton;
            if (btn != null && btn.Pressed)
            {
                Rectangle bounds = new Rectangle(Point.Empty, e.Item.Size);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(60, 60, 60)), bounds);
            }
            else base.OnRenderDropDownButtonBackground(e);
        }
    }
}
