﻿namespace QuickHumCliente.Vistas
{
    partial class frmPruebasAsignadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraCandidato = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnComenzarPruebas = new DevExpress.XtraEditors.SimpleButton();
            this.gridPruebas = new DevExpress.XtraGrid.GridControl();
            this.VistaPrincipal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Prueba = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Tiempo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Estado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPruebas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraCandidato);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(792, 48);
            this.ContentedorTituloPestana.TabIndex = 4;
            // 
            // CabezeraCandidato
            // 
            this.CabezeraCandidato.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraCandidato.Location = new System.Drawing.Point(0, 0);
            this.CabezeraCandidato.Name = "CabezeraCandidato";
            this.CabezeraCandidato.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraCandidato.Size = new System.Drawing.Size(181, 36);
            this.CabezeraCandidato.TabIndex = 1;
            this.CabezeraCandidato.Text = "[NOMBRE CANDIDATO]";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnComenzarPruebas);
            this.layoutControl1.Controls.Add(this.gridPruebas);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 48);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(792, 525);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnComenzarPruebas
            // 
            this.btnComenzarPruebas.Image = global::QuickHumCliente.Properties.Resources.pruebainiciar;
            this.btnComenzarPruebas.Location = new System.Drawing.Point(660, 475);
            this.btnComenzarPruebas.MinimumSize = new System.Drawing.Size(0, 36);
            this.btnComenzarPruebas.Name = "btnComenzarPruebas";
            this.btnComenzarPruebas.Size = new System.Drawing.Size(120, 38);
            this.btnComenzarPruebas.StyleController = this.layoutControl1;
            this.btnComenzarPruebas.TabIndex = 7;
            this.btnComenzarPruebas.Text = "Comenzar";
            this.btnComenzarPruebas.Click += new System.EventHandler(this.btnComenzarPruebas_Click);
            // 
            // gridPruebas
            // 
            this.gridPruebas.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridPruebas.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridPruebas.Location = new System.Drawing.Point(12, 60);
            this.gridPruebas.MainView = this.VistaPrincipal;
            this.gridPruebas.Name = "gridPruebas";
            this.gridPruebas.Size = new System.Drawing.Size(768, 401);
            this.gridPruebas.TabIndex = 6;
            this.gridPruebas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VistaPrincipal});
            // 
            // VistaPrincipal
            // 
            this.VistaPrincipal.Appearance.Row.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VistaPrincipal.Appearance.Row.Options.UseFont = true;
            this.VistaPrincipal.ColumnPanelRowHeight = 32;
            this.VistaPrincipal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Prueba,
            this.Tiempo,
            this.Estado});
            this.VistaPrincipal.GridControl = this.gridPruebas;
            this.VistaPrincipal.Name = "VistaPrincipal";
            this.VistaPrincipal.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VistaPrincipal.OptionsBehavior.Editable = false;
            this.VistaPrincipal.OptionsCustomization.AllowColumnMoving = false;
            this.VistaPrincipal.OptionsCustomization.AllowGroup = false;
            this.VistaPrincipal.OptionsCustomization.AllowQuickHideColumns = false;
            this.VistaPrincipal.OptionsFind.AllowFindPanel = false;
            this.VistaPrincipal.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.VistaPrincipal.OptionsView.ShowGroupPanel = false;
            this.VistaPrincipal.RowHeight = 48;
            // 
            // Prueba
            // 
            this.Prueba.AppearanceHeader.Font = new System.Drawing.Font("Open Sans Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prueba.AppearanceHeader.Options.UseFont = true;
            this.Prueba.Caption = "  Prueba";
            this.Prueba.FieldName = "Prueba";
            this.Prueba.Image = global::QuickHumCliente.Properties.Resources.pruebaicon;
            this.Prueba.Name = "Prueba";
            this.Prueba.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Prueba.Visible = true;
            this.Prueba.VisibleIndex = 0;
            // 
            // Tiempo
            // 
            this.Tiempo.AppearanceHeader.Font = new System.Drawing.Font("Open Sans Condensed", 12F, System.Drawing.FontStyle.Bold);
            this.Tiempo.AppearanceHeader.Options.UseFont = true;
            this.Tiempo.Caption = "  Tiempo";
            this.Tiempo.FieldName = "Tiempo";
            this.Tiempo.Image = global::QuickHumCliente.Properties.Resources.pruebatiempo;
            this.Tiempo.Name = "Tiempo";
            this.Tiempo.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Tiempo.Visible = true;
            this.Tiempo.VisibleIndex = 1;
            // 
            // Estado
            // 
            this.Estado.AppearanceHeader.Font = new System.Drawing.Font("Open Sans Condensed", 12F, System.Drawing.FontStyle.Bold);
            this.Estado.AppearanceHeader.Options.UseFont = true;
            this.Estado.Caption = "   Estado";
            this.Estado.FieldName = "Estado";
            this.Estado.Image = global::QuickHumCliente.Properties.Resources.pruebaarchivos;
            this.Estado.Name = "Estado";
            this.Estado.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Estado.Visible = true;
            this.Estado.VisibleIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.labelControl1.Size = new System.Drawing.Size(151, 32);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Pruebas Asignadas";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.simpleSeparator1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(792, 525);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(772, 36);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 36);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(772, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 38);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(772, 10);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 463);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(648, 42);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridPruebas;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(772, 405);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnComenzarPruebas;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(648, 463);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(124, 42);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 453);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(772, 10);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frmPruebasAsignadas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPruebasAsignadas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pruebas Asignadas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPruebasAsignadas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPruebas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VistaPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraCandidato;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.GridControl gridPruebas;
        private DevExpress.XtraGrid.Views.Grid.GridView VistaPrincipal;
        private DevExpress.XtraGrid.Columns.GridColumn Prueba;
        private DevExpress.XtraGrid.Columns.GridColumn Tiempo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnComenzarPruebas;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Columns.GridColumn Estado;
    }
}