﻿namespace QuickHumCliente.Vistas.Pruebas.IPV
{
    partial class frmIPV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIPV));
            this.label3 = new System.Windows.Forms.Label();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.lblIdPregunta = new System.Windows.Forms.Label();
            this.lblMsj = new System.Windows.Forms.Label();
            this.rbtC = new System.Windows.Forms.RadioButton();
            this.rbtB = new System.Windows.Forms.RadioButton();
            this.rbtA = new System.Windows.Forms.RadioButton();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.btnCancelarAyuda = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(705, 36);
            this.label3.TabIndex = 63;
            this.label3.Text = "PRUEBA IPV";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPreguntas.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtPreguntas.Location = new System.Drawing.Point(13, 11);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtPreguntas.Size = new System.Drawing.Size(659, 68);
            this.txtPreguntas.TabIndex = 64;
            this.txtPreguntas.Text = "";
            // 
            // lblIdPregunta
            // 
            this.lblIdPregunta.AutoSize = true;
            this.lblIdPregunta.Location = new System.Drawing.Point(593, 272);
            this.lblIdPregunta.Name = "lblIdPregunta";
            this.lblIdPregunta.Size = new System.Drawing.Size(88, 13);
            this.lblIdPregunta.TabIndex = 73;
            this.lblIdPregunta.Text = "Pregunta N de M";
            this.lblIdPregunta.Visible = false;
            // 
            // lblMsj
            // 
            this.lblMsj.AutoSize = true;
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(9, 275);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(135, 18);
            this.lblMsj.TabIndex = 72;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // rbtC
            // 
            this.rbtC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtC.Font = new System.Drawing.Font("Calibri", 12F);
            this.rbtC.Location = new System.Drawing.Point(33, 202);
            this.rbtC.Name = "rbtC";
            this.rbtC.Size = new System.Drawing.Size(592, 36);
            this.rbtC.TabIndex = 71;
            this.rbtC.TabStop = true;
            this.rbtC.Text = "radioButton3";
            this.rbtC.UseVisualStyleBackColor = false;
            this.rbtC.Click += new System.EventHandler(this.rbtC_Click);
            // 
            // rbtB
            // 
            this.rbtB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtB.Font = new System.Drawing.Font("Calibri", 12F);
            this.rbtB.Location = new System.Drawing.Point(33, 150);
            this.rbtB.Name = "rbtB";
            this.rbtB.Size = new System.Drawing.Size(592, 36);
            this.rbtB.TabIndex = 70;
            this.rbtB.TabStop = true;
            this.rbtB.Text = "radioButton2";
            this.rbtB.UseVisualStyleBackColor = false;
            this.rbtB.Click += new System.EventHandler(this.rbtB_Click);
            // 
            // rbtA
            // 
            this.rbtA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtA.Font = new System.Drawing.Font("Calibri", 12F);
            this.rbtA.Location = new System.Drawing.Point(33, 97);
            this.rbtA.Name = "rbtA";
            this.rbtA.Size = new System.Drawing.Size(592, 36);
            this.rbtA.TabIndex = 69;
            this.rbtA.TabStop = true;
            this.rbtA.Text = "radioButton1";
            this.rbtA.UseVisualStyleBackColor = false;
            this.rbtA.Click += new System.EventHandler(this.rbtA_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(234, 15);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 72;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(10, 16);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 71;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNumPreg.Location = new System.Drawing.Point(277, 3);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(118, 19);
            this.lblNumPreg.TabIndex = 69;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(571, 15);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 70;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(350, 15);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 68;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo1;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(436, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 37);
            this.panel1.TabIndex = 73;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(46, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lbltime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltime.Location = new System.Drawing.Point(170, 10);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(46, 18);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel2);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.MinimumSize = new System.Drawing.Size(700, 582);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(705, 622);
            this.panelAyuda.TabIndex = 74;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.btnCancelarAyuda);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.btnIniciarAyuda);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(47, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(621, 570);
            this.panel2.TabIndex = 75;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(36, 36);
            this.panel3.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(17, 486);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(411, 17);
            this.label10.TabIndex = 78;
            this.label10.Text = "Como ha visto, es muy sencillo ahora continúe con los siguientes 86.";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panel6.Controls.Add(this.radioButton7);
            this.panel6.Controls.Add(this.radioButton9);
            this.panel6.Controls.Add(this.radioButton8);
            this.panel6.Location = new System.Drawing.Point(14, 363);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(595, 115);
            this.panel6.TabIndex = 73;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.BackColor = System.Drawing.Color.Transparent;
            this.radioButton7.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton7.Location = new System.Drawing.Point(7, 15);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(348, 21);
            this.radioButton7.TabIndex = 71;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "A) Avisar del hecho al primer vendedor que encuentre.";
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.BackColor = System.Drawing.Color.Transparent;
            this.radioButton9.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton9.Location = new System.Drawing.Point(7, 45);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(342, 21);
            this.radioButton9.TabIndex = 72;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "B) No hacer nada porque piensa que no es cosa suya.";
            this.radioButton9.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.BackColor = System.Drawing.Color.Transparent;
            this.radioButton8.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton8.Location = new System.Drawing.Point(7, 80);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(466, 21);
            this.radioButton8.TabIndex = 73;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "C) Indicar al infractor con el gesto de palabra que ha visto lo que ha hecho.\r\n";
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // btnCancelarAyuda
            // 
            this.btnCancelarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCancelarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarAyuda.Location = new System.Drawing.Point(17, 520);
            this.btnCancelarAyuda.Name = "btnCancelarAyuda";
            this.btnCancelarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnCancelarAyuda.TabIndex = 69;
            this.btnCancelarAyuda.Text = "Cancelar";
            this.btnCancelarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarAyuda.UseVisualStyleBackColor = false;
            this.btnCancelarAyuda.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panel5.Controls.Add(this.radioButton4);
            this.panel5.Controls.Add(this.radioButton6);
            this.panel5.Controls.Add(this.radioButton5);
            this.panel5.Location = new System.Drawing.Point(14, 172);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(595, 115);
            this.panel5.TabIndex = 73;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton4.Location = new System.Drawing.Point(8, 16);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(284, 21);
            this.radioButton4.TabIndex = 71;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "A) Una casa en el campo con un gran jardin.";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.BackColor = System.Drawing.Color.Transparent;
            this.radioButton6.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton6.Location = new System.Drawing.Point(8, 81);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(417, 21);
            this.radioButton6.TabIndex = 73;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "C) Una casa situada en una calle tranquila de una pequeña ciudad.\r\n";
            this.radioButton6.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.BackColor = System.Drawing.Color.Transparent;
            this.radioButton5.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton5.Location = new System.Drawing.Point(8, 47);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(300, 21);
            this.radioButton5.TabIndex = 72;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "B) Un lujoso apartamento en una gran ciudad.";
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.btnCerrarAyuda);
            this.panel4.Location = new System.Drawing.Point(584, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 72;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(-2, 1);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 69;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(169, 520);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(15, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(469, 34);
            this.label8.TabIndex = 76;
            this.label8.Text = "J... esta comprando en un gran almacen cuando advierte que alguien esconde\r\n disc" +
    "retamente un disco bajo su sueter. ¿Que hara J...?\r\n";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 11F);
            this.richTextBox1.Location = new System.Drawing.Point(14, 43);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(595, 91);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F);
            this.label5.Location = new System.Drawing.Point(11, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 18);
            this.label5.TabIndex = 75;
            this.label5.Text = "Otras se refieren a personas ajenas como :\r\n";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(11, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(449, 34);
            this.label4.TabIndex = 62;
            this.label4.Text = "-Si usted pudiera elegir el tipo de vivienda de manera completamente libre.\r\n ¿Qu" +
    "e preferiria?\r\n";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(25, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(559, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.button1.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(660, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 35);
            this.button1.TabIndex = 75;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel7
            // 
            this.panel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.gbPreguntas);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.lblIdPregunta);
            this.panel7.Controls.Add(this.lblMsj);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Location = new System.Drawing.Point(2, 81);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(690, 410);
            this.panel7.TabIndex = 76;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.rbtC);
            this.gbPreguntas.Controls.Add(this.rbtB);
            this.gbPreguntas.Controls.Add(this.txtPreguntas);
            this.gbPreguntas.Controls.Add(this.rbtA);
            this.gbPreguntas.Location = new System.Drawing.Point(3, 3);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(683, 260);
            this.gbPreguntas.TabIndex = 75;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel9.Controls.Add(this.lblNumPreg);
            this.panel9.Location = new System.Drawing.Point(1, 323);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(689, 25);
            this.panel9.TabIndex = 74;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.btnAnterior);
            this.panel8.Controls.Add(this.btnSiguiente);
            this.panel8.Controls.Add(this.btnCancelar);
            this.panel8.Controls.Add(this.btnAyuda);
            this.panel8.Location = new System.Drawing.Point(1, 348);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(689, 60);
            this.panel8.TabIndex = 0;
            // 
            // frmIPV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 658);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(713, 665);
            this.Name = "frmIPV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRUEBA IPV";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIPV_FormClosing);
            this.Load += new System.EventHandler(this.frmIPV_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelAyuda.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.RadioButton rbtC;
        private System.Windows.Forms.RadioButton rbtB;
        private System.Windows.Forms.RadioButton rbtA;
        private System.Windows.Forms.Label lblIdPregunta;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Button btnCancelarAyuda;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel gbPreguntas;
    }
}