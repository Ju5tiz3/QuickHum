﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.IPV
{
    public partial class frmIPVreporte : Form
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;

        DataTable dtresultados = new DataTable();
        DataTable dt = new DataTable();
        DataTable dtar = new DataTable();
        DataTable dtCandidato = new DataTable();
        string ia, ip, ib, iia, iip, iib, iiia, iiip, iiib, iva, ivp, ivb, va, vp, vb, via, vip, vib, viia, viip, viib, viiia, viiip, viiib, ixa, ixp, ixb, dgva, dgvp, dgvb, ra, rp, rb, aa, ap, ab;
        string I, II, III, IV, V, VI, VII, VIII, IX, DGV, R, A, expresion;
        int idI = 0, idII = 0, idIII = 0, IDIV = 0, idV = 0, idVI = 0, idVII = 0, idVIII = 0, idIX = 0, idR = 0, idA = 0, idD = 0;
        int conteo = 0, contador = 0;
        int per = 0;
        DataRow[] filtro;
        public frmIPVreporte(Expediente expediente)
        {
            InitializeComponent();
            if (expediente == null)
            {
                XtraMessageBox.Show("El expediente trasladado al reporte esta vacio", "Reporte", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            this._expediente = expediente;
        }

        private void frmIPVreporte_Load(object sender, EventArgs e)
        {
            dtresultados.Columns.Add("NumeroOrden");
            dtresultados.Columns.Add("Factor");
            dtresultados.Columns.Add("FactorAR");
            dtresultados.Columns.Add("Suma");
            dtresultados.Columns.Add("Grafica");

            dt.TableName = "Factores";
            dt.Columns.Add("Idfactor");
            dt.Columns.Add("Punto");
            dt.Columns.Add("Grafico");

            dtar.Columns.Add("id_factor");
            dtar.Columns.Add("Punto");
            dtar.Columns.Add("Grafico");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            interpretaciones();
            GenerarReporte();

        }

        private void GenerarReporte()
        {
            dtresultados.Clear();
            dtar.Clear();
            dt.Clear();
            dto_ipv_resultados[] reporte = cliente.ListaIpvResultados(this._expediente.Id);
            dto_ipv_grafica_factores[] grafica = cliente.ListarGraficaFactores();
            dto_IPVGraficaFactorAR[] graficaAR = cliente.ListaIPVGraficaFactoresAR();

            for (int j = 0; j < grafica.ToList().Count; j++)
            {
                DataRow fila = dt.NewRow();
                fila["Idfactor"] = Convert.ToInt32(grafica[j].Idfactor);
                fila["Punto"] = Convert.ToInt32(grafica[j].PuntoGraficaFactores);
                fila["Grafico"] = Convert.ToInt32(grafica[j].GraficoGraficaFactores);
                dt.Rows.Add(fila);
            }
            conteo = dt.Rows.Count;
            for (int y = 0; y < graficaAR.ToList().Count; y++)
            {
                DataRow filas = dtar.NewRow();
                filas["id_factor"] = Convert.ToInt32(graficaAR[y].IdfactorAR);
                filas["Punto"] = Convert.ToInt32(graficaAR[y].Punto);
                filas["Grafico"] = Convert.ToInt32(graficaAR[y].Grafico);
                dtar.Rows.Add(filas);
            }
            contador = dtar.Rows.Count;
            int[] sumaA = new int[5];
            int[] sumaR = new int[4];
            for (int i = 0; i < reporte.ToList().Count; i++)
            {
                DataRow row = dtresultados.NewRow();
                switch (reporte[i].Factor)
                {

                    case "I":
                        row["NumeroOrden"] = Convert.ToInt32(1);
                        sumaR[0] = sumaR[0] + reporte[i].Suma;
                        idI = 1;

                        expresion = "Idfactor = '1' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { I = ia; }
                        else if (per >= 5 && per <= 7)
                        { I = ip; }
                        else
                        { I = ib; }

                        break;
                    case "II":
                        row["NumeroOrden"] = Convert.ToInt32(2);
                        sumaR[1] = sumaR[1] + reporte[i].Suma;
                        idII = 2;

                        expresion = "Idfactor = '2' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { II = iia; }
                        else if (per >= 5 && per <= 7)
                        { II = iip; }
                        else
                        { II = iib; }

                        break;
                    case "III":
                        row["NumeroOrden"] = Convert.ToInt32(3);
                        sumaR[2] = sumaR[2] + reporte[i].Suma;
                        idIII = 3;
                        expresion = "Idfactor = '3' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { III = iiia; }
                        else if (per >= 5 && per <= 7)
                        { III = iiip; }
                        else
                        { III = iiib; }

                        break;
                    case "IV":
                        row["NumeroOrden"] = Convert.ToInt32(4);
                        sumaR[3] = sumaR[3] + reporte[i].Suma;
                        IDIV = 4;
                        expresion = "Idfactor = '4' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { IV = iva; }
                        else if (per >= 5 && per <= 7)
                        { IV = ivp; }
                        else
                        { IV = ivb; }

                        break;
                    case "V":
                        row["NumeroOrden"] = Convert.ToInt32(5);
                        sumaA[0] = sumaA[0] + reporte[i].Suma;
                        idV = 5;
                        expresion = "Idfactor = '5' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { V = va; }
                        else if (per >= 5 && per <= 7)
                        { V = vp; }
                        else
                        { V = vb; }

                        break;
                    case "VI":
                        row["NumeroOrden"] = Convert.ToInt32(6);
                        sumaA[1] = sumaA[1] + reporte[i].Suma;
                        idVI = 6;
                        expresion = "Idfactor = '6' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { VI = via; }
                        else if (per >= 5 && per <= 7)
                        { VI = vip; }
                        else
                        { VI = vib; }

                        break;
                    case "VII":
                        row["NumeroOrden"] = Convert.ToInt32(7);
                        sumaA[2] = sumaA[2] + reporte[i].Suma;
                        idVII = 7;
                        expresion = "Idfactor = '7' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { VII = viia; }
                        else if (per >= 5 && per <= 7)
                        { VII = viip; }
                        else
                        { VII = viib; }

                        break;
                    case "VIII":
                        row["NumeroOrden"] = Convert.ToInt32(8);
                        sumaA[3] = sumaA[3] + reporte[i].Suma;
                        idVIII = 8;
                        expresion = "Idfactor = '8' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { VIII = viiia; }
                        else if (per >= 5 && per <= 7)
                        { VIII = viiip; }
                        else
                        { VIII = viiib; }
                        break;
                    case "IX":
                        row["NumeroOrden"] = Convert.ToInt32(9);
                        sumaA[4] = sumaA[4] + reporte[i].Suma;
                        idIX = 9;
                        expresion = "Idfactor = '9' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                        if (per >= 8)
                        { IX = ixa; }
                        else if (per >= 5 && per <= 7)
                        { IX = ixp; }
                        else
                        { IX = ixb; }

                        break;
                    case "DGV":
                        idD = 10;
                        expresion = "Idfactor = '10' and Punto= '" + reporte[i].Suma + "'";
                        filtro = dt.Select(expresion);
                        per = Convert.ToInt32(filtro[0]["Grafico"].ToString());
                        for (int w = 0; w < conteo; w++)
                        {
                            if (idD == Convert.ToInt32(dt.Rows[w]["Idfactor"].ToString()))
                            {
                                if (Convert.ToInt32(reporte[i].Suma) == Convert.ToInt32(dt.Rows[w]["Punto"].ToString()))
                                {
                                    per = Convert.ToInt32(dt.Rows[w]["Grafico"].ToString());
                                    if (per >= 8)
                                    { DGV = dgva; }
                                    else if (per >= 5 && per <= 7)
                                    { DGV = dgvp; }
                                    else
                                    { DGV = dgvb; }
                                }
                            }
                        }
                        break;

                }

                row["Factor"] = Convert.ToString(reporte[i].Factor);
                row["FactorAR"] = Convert.ToString(reporte[i].FactorAR);
                row["Suma"] = Convert.ToInt32(reporte[i].Suma);
                row["Grafica"] = per;

                dtresultados.Rows.Add(row);
                if (i == 9)
                {
                    idR = 1;
                    sumaR[0] += sumaR[1] + sumaR[2] + sumaR[3];

                    expresion = "id_factor = '1' and Punto= '" + sumaR[0] + "'";
                    filtro = dtar.Select(expresion);
                    per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                    if (per >= 8)
                    { R = ra; }
                    else if (per >= 5 && per <= 7)
                    { R = rp; }
                    else
                    { R = rb; }


                    row = dtresultados.NewRow();
                    row["Factor"] = "Receptividad";
                    row["Suma"] = Convert.ToInt32(sumaR[0]);
                    row["Grafica"] = per;
                    dtresultados.Rows.Add(row);


                    idA = 2;
                    sumaA[0] += sumaA[1] + sumaA[2] + sumaA[3] + sumaA[4];

                    expresion = "id_factor = '2' and Punto= '" + sumaA[0] + "'";
                    filtro = dtar.Select(expresion);
                    per = Convert.ToInt32(filtro[0]["Grafico"].ToString());

                    if (per >= 8)
                    { A = aa; }
                    else if (per >= 5 && per <= 7)
                    { A = ap; }
                    else
                    { A = ab; }

                    row = dtresultados.NewRow();
                    row["Factor"] = "Agresividad";
                    row["Suma"] = Convert.ToInt32(sumaA[0]);
                    row["Grafica"] = per;
                    dtresultados.Rows.Add(row);
                }



            }

            //DataView vista = new DataView();
            //vista = new DataView(dtresultados, "", "Factor ASC", DataViewRowState.CurrentRows);
            //dt = vista.Table;


            //string expresio = "Factor ASC";
            //string filtrp = "Suma < 1000";
            //DataRow[] rows;
            //rows = dtresultados.Select(filtrp, expresio);
            ReportParameter[] parametro = new ReportParameter[14];
            parametro[0] = new ReportParameter("DUI", this._expediente.Candidato.Dui);
            parametro[1] = new ReportParameter("I", I);
            parametro[2] = new ReportParameter("II", II);
            parametro[3] = new ReportParameter("III", III);
            parametro[4] = new ReportParameter("IV", IV);
            parametro[5] = new ReportParameter("V", V);
            parametro[6] = new ReportParameter("VI", VI);
            parametro[7] = new ReportParameter("VII", VII);
            parametro[8] = new ReportParameter("VIII", VIII);
            parametro[9] = new ReportParameter("IX", IX);
            parametro[10] = new ReportParameter("DGV", DGV);
            parametro[11] = new ReportParameter("A", A);
            parametro[12] = new ReportParameter("R", R);
            parametro[13] = new ReportParameter("filtro", string.Empty);

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            ReportDataSource rds = new ReportDataSource("DataSetIPV", dtresultados);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.IPV.reporteipv.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.LocalReport.SetParameters(parametro);
            reportViewer1.RefreshReport();
        }

        private void interpretaciones()
        {
            ia = "ALTO Muestra a una persona por demás empático y objetivo en sus relaciones humanas, con gran capacidad intuitiva y capaz de integrar en su contexto un suceso cualquiera.";
            dgva = "ALTO Señala a una persona que posee una gran capacidad para establecer relaciones con los demás, muestra combatividad y bastante control de sí mismo que le permiten persuadir al cliente. En sí, cuenta con rasgos de personalidad idóneos para actividades comerciales.";
            ixa = "ALTO Muestra a una persona extrovertido, que busca y crea nuevos contactos, gusta de convivir con los demás, es sensible a las relaciones humanas y prefiere estar acompañado que solo.";
            ra = "ALTO Muestra a una persona con gran capacidad de empatía, que sabe escuchar y comprender, que se adapta fácilmente a personas y circunstancias y que posee control de si mismo y resistencia a la frustración.";
            aa = "ALTO Señala a una persona sumamente activo y dinámico, que cuenta con gran capacidad para soportar y provocar situaciones conflictivas, orientado a ganar, a dominar, a persuadir, es seguro de sí y gusta de riesgos.";
            iia = "ALTO  Muestra a una persona con una gran habilidad para adaptarse a situaciones y personas de manera fácil y rápida, es muy flexible en sus actividades tanto intelectuales como de relación.";
            iiia = "ALTO Muestra a una persona con gran capacidad de autocontrol, dueño de sí mismo, el cual, administra eficientemente su potencial psicológico y físico, muy organizado y perseverante y muy hábil para ocultar sus sentimientos.";
            iva = "ALTO Muestra a una persona que posee una gran capacidad para soportar acciones frustrantes, que comprende los fracasos, e incluso puede llegar a aprender de ellos, así bien, cuando llega a tener un tropiezo no personalizar las situaciones en que se ve implicado.";
            va = "ALTO Muestra a una persona capaz de provocar conflictos con la finalidad de persuadir, es capaz de soportar los desacuerdos, se caracteriza por ser polémico y porfiado. Posee una excelente agresividad comercial.";
            via = "ALTO Señala a una persona con una gran voluntad de dominio, con una actitud ganadora, manipulador, persuasivo y cautivador, es dominante y con actitud ascendente propia de personas con jerarquía.";
            viia = "ALTO Muestra a un individuo por demás seguro de sí mismo, que gusta de situaciones novedosas e inesperadas, capaz de enfrentarse a riesgos e incluso atreverse a buscarlos, siempre y cuando considere que éstos le traerán ganancias.";
            viiia = "ALTO Muestra a una persona que gusta de actividades deportivas, no soporta la pasividad física.";

            dgvp = "PROMEDIO Posee facilidad para establecer relaciones con los demás, muestra combatividad que le permite persuadir al cliente, pero al mismo tiempo, control de sí mismo. En sí, cuenta con rasgos de personalidad acordes a actividades comerciales";
            ixp = "PROMEDIO Es extrovertido, capaz de crear nuevos contactos y convivir con los demás, es sensible a las relaciones humanas y prefiere estar acompañado que solo.";
            rp = "PROMEDIO Es una persona que sabe ponerse en lugar de los demás, sabe escuchar y comprender, cuenta con capacidad de adaptación a personas y circunstancias, posee control de si mismo y resistencia a la frustración";
            ap = "PROMEDIO Es una persona activa y dinámico, cuenta con capacidad para soportar y provocar situaciones conflictivas con el deseo de ganar, posee una actitud dominante por poder o ascendencia, seguro de sí, gusta de riesgos.";
            ip = "PROMEDIO Es empático y objetivo en sus relaciones humanas, intuitivo y capaz de integrar en su contexto un suceso cualquiera.";
            iip = "PROMEDIO Cuenta con una habilidad para adaptarse fácil y rápidamente a situaciones y personas diferentes, es flexible en sus actividades tanto intelectuales como de relación..- Adaptabilidad: Cuenta con una habilidad para adaptarse fácil y rápidamente a situaciones y personas diferentes, es flexible en sus actividades tanto intelectuales como de relación.";
            iiip = "PROMEDIO Es un individuo controlado, dueño de sí mismo, capaz de una buena administración de su potencial psicológico o físico, es una persona organizada y perseverante, hábil para ocultar sus sentimientos.";
            ivp = "PROMEDIO Es una persona que soporta adecuadamente las acciones frustrantes, capaz de comprender los fracasos y de no personalizar las situaciones en que se ve implicado.";
            vp = "PROMEDIO Es una persona capaz de entrar conflictos y soportar los desacuerdos, posee agresividad comercial.";
            vip = "PROMEDIO Señala a una persona con voluntad de dominio, con deseos de ganar, de manipular, persuasivo y cautivador, es dominante y con actitud ascendente propia de personas con jerarquía.";
            viip = "PROMEDIO Es un individuo seguro de sí mismo, que le gustan las situaciones novedosas e inesperadas, capaz de enfrentarse a riesgos.";
            viiip = "PROMEDIO Es una persona activa que no le gusta la pasividad.";


            dgvb = "BAJO No muestra una disposición general para la venta, es decir, le cuesta trabajo establecer relaciones con los demás, no cuenta con una actitud combativa ni control de sí mismo. Características indispensables para realizar actividades comerciales.";
            ixb = "BAJO Es introvertido, le cuesta trabajo crear nuevos contactos y convivir con los demás, puede llegar a preferir estar solo que acompañado.";
            rb = "BAJO Es una persona que no es empática con los demás, se limita a oír, no escucha ni comprende, le cuesta mucho trabajo adaptarse a personas y circunstancias nuevas, tiene poco control de si mismo y poca resistencia a la frustración.";
            ab = "BAJO No se caracteriza por ser un persona activa y dinámica, no le gusta participar en actividades deportivas, no soporta situaciones conflictivas, no demuestra una actitud de poder o ascendencia suficiente para dominar, es inseguro e incapaz de enfrentar riesgos.";
            ib = "BAJO Es una persona que no lo caracteriza la empatía y objetividad en sus relaciones humanas, no es intuitivo ni integrador.";
            iib = "BAJO No se adapta a situaciones y personas nuevas, es inflexible en sus actividades intelectuales y de relación.";
            iiib = "BAJO No posee control en sí mismo, no logra administrar su potencial psicológico y/o físico, regularmente no es organizado ni perseverante.";
            ivb = "BAJO Es muy poco tolerante y aveces no logra dejar de personalizar las situaciones en que se ve implicado.";
            vb = "BAJO Le cuesta mucho trabajo soportar desacuerdos y no cuenta con una agresividad comercial.";
            vib = "BAJO Señala a una persona con poca voluntad de dominio, de manipular, persuadir y/o cautivar.";
            viib = "BAJO Es inseguro, no goza de situaciones novedosas e inesperadas, difícilmente se enfrentaría a riesgos.";
            viiib = "BAJO Es una persona que no gusta de actividades deportivas, suele ser pasivo.";

        }
    }

}
