﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.IPV
{
    public partial class frmIPV : Form
    {
        private QuickHumClient cliente = Globales.cliente;
        private bool prueba_inicada = false;
        dto_ipv_preguntas preguntas = new dto_ipv_preguntas();
        dto_ipv_respuestas respuestas = new dto_ipv_respuestas();
        dto_ipv_respuestas_candidato respcandidato = new dto_ipv_respuestas_candidato();

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        int x = 0;
        int punto = 0;
        int idresp = 0;
        int recorrido = 0;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;
        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtactualizar = new DataTable();
        DataRow[] row;
        ServicioQuickHum.IPV modelo_IPV;
        int NumeroAsignacion;

        public frmIPV(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }

        private void frmIPV_Load(object sender, EventArgs e)
        {

            dto_ipv_preguntas[] ListadoPreguntas = cliente.ListadoIPVPreguntas();

            //estructura Datatable de las respuestas del candidato
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("numero_prueba");
            dtactualizar.Columns.Add("id_respuesta"); dtactualizar.Columns.Add("punto");
            //this.Size = new Size(710, 569);


            dtpreguntas.Columns.Add("idpregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtrespuestas.Columns.Add("id_respuesta");
            dtrespuestas.Columns.Add("respuesta");
            dtrespuestas.Columns.Add("respuesta_correcta");
            dtrespuestas.Columns.Add("id_pregunta");
            for (int i = 0; i < ListadoPreguntas.ToList().Count; i++)
            {
                DataRow fila = dtpreguntas.NewRow();

                fila["idpregunta"] = Convert.ToInt32(ListadoPreguntas[i].IdPregunta);
                fila["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);

                for (int j = 0; j < 3; j++)
                {
                    DataRow row = dtrespuestas.NewRow();

                    row["id_respuesta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].IdRespuesta);
                    row["respuesta"] = Convert.ToString(ListadoPreguntas[i].Respuesta[j].Respuesta);
                    row["respuesta_correcta"] = Convert.ToString(ListadoPreguntas[i].Respuesta[j].RespuestaCorrecta);
                    row["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].IdPregunta);

                    dtrespuestas.Rows.Add(row);

                }
                dtpreguntas.Rows.Add(fila);
            }

            txtPreguntas.Text = "1 " + dtpreguntas.Rows[0][1].ToString();

            lblIdPregunta.Text = dtpreguntas.Rows[0]["idpregunta"].ToString();
            rbtA.Text = "A) " + dtrespuestas.Rows[0]["respuesta"].ToString();
            rbtB.Text = "B) " + dtrespuestas.Rows[1]["respuesta"].ToString();
            rbtC.Text = "C) " + dtrespuestas.Rows[2]["respuesta"].ToString();

            rbtA.Name = dtrespuestas.Rows[0]["id_respuesta"].ToString();
            rbtB.Name = dtrespuestas.Rows[1]["id_respuesta"].ToString();
            rbtC.Name = dtrespuestas.Rows[2]["id_respuesta"].ToString();




            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            DateTime hora;
            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void mostrarPreguntas(int i)
        {
            txtPreguntas.Text = i + 1 + ") " + dtpreguntas.Rows[i][1].ToString();
            row = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[i][0].ToString() + " ' ");

            if (row.Length > 0)
            {
                lblIdPregunta.Text = row[0]["id_pregunta"].ToString();
                rbtA.Name = row[0]["id_respuesta"].ToString();
                rbtB.Name = row[1]["id_respuesta"].ToString();
                rbtC.Name = row[2]["id_respuesta"].ToString();
                rbtA.Text = "A) " + row[0]["respuesta"].ToString();
                rbtB.Text = "B) " + row[1]["respuesta"].ToString();
                rbtC.Text = "C) " + row[2]["respuesta"].ToString();

            }

        }

        private void CheckarRadiosatras()
        {
            if (Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3)]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x - 1)]["id_respuesta"].ToString()))
            {
                rbtA.Checked = true;

            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3) + 1]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta"].ToString()))
            {
                rbtB.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3) + 2]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta"].ToString()))
            {
                rbtC.Checked = true;
            }
        }

        private void CheckarRadiosadelante()
        {
            if (Convert.ToInt32(dtrespuestas.Rows[((3 * x))]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x)]["id_respuesta"].ToString()))
            {
                rbtA.Checked = true;

            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(3 * x) + 1]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x]["id_respuesta"].ToString()))
            {
                rbtB.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(3 * x) + 2]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x]["id_respuesta"].ToString()))
            {
                rbtC.Checked = true;
            }
        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {

            if (btnSiguiente.Text == "Siguiente")
            {
                Validar();
                x++;
                guardarRespuestas();


                mostrarPreguntas(x);
                lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtpreguntas.Rows.Count;
                limpiarcampos();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtpreguntas.Rows.Count;
                mostrarPreguntas(x);

                if (recorrido > x)
                {
                    CheckarRadiosadelante();
                }


                if (recorrido <= x)
                {
                    this.btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    limpiarcampos();
                }
            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba IPV", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Dispose();
                }

            }


            if (x == dtpreguntas.Rows.Count - 1)
            {
                btnSiguiente.Text = "Finalizar";
            }
            btnAnterior.Enabled = true;
        }
        private void btnAnterior_Click(object sender, EventArgs e)
        {

            dto_ipv_respuestas_candidato[] listaresuestapcandidato = cliente.ListaPreguntasContestadasIPV(NumeroAsignacion);


            dtactualizar.Clear();
            for (int w = 0; w < listaresuestapcandidato.ToList().Count; w++)
            {
                DataRow filas = dtactualizar.NewRow();

                filas["id_respuesta_candidato"] = Convert.ToInt32(listaresuestapcandidato[w].IdRespuestaCandidato);
                filas["numero_prueba"] = Convert.ToInt32(listaresuestapcandidato[w].NumeroPrueba);
                filas["id_respuesta"] = Convert.ToInt32(listaresuestapcandidato[w].IdRespuesta);
                filas["punto"] = Convert.ToInt32(listaresuestapcandidato[w].PuntoRespuestaCandidato);
                dtactualizar.Rows.Add(filas);
            }
            recorrido = dtactualizar.Rows.Count;

            btnSiguiente.Text = "Siguiente.";
            btnSiguiente.Enabled = true;
            CheckarRadiosatras();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtpreguntas.Rows.Count;
            x--;

            mostrarPreguntas(x);
            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";

            }
        }

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if ((rbtA.Checked == false) && (rbtB.Checked == false) && (rbtC.Checked == false))
                {

                }
                else
                {
                    x++;
                    guardarRespuestas();


                }
            }
            respcandidato.NumeroPrueba = NumeroAsignacion;
            cliente.FinalizaPruebaIPV(respcandidato);
            this.Dispose();
        }

        private void guardarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuesta = Convert.ToInt32(idresp);
            respcandidato.PuntoRespuestaCandidato = Convert.ToInt32(punto);
            respcandidato.NumeroPrueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertarRespuestaIPV(respcandidato);



        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta_candidato"]);
            respcandidato.IdRespuesta = Convert.ToInt32(idresp);
            respcandidato.PuntoRespuestaCandidato = Convert.ToInt32(punto);
            cliente.ActualizarRespuestaIPV(respcandidato);

        }
        private void Evaluar()
        {
            if (rbtA.Checked == true)
            {
                if (Convert.ToString(dtrespuestas.Rows[((3 * x) - 3)]["respuesta_correcta"]) == "si")
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                idresp = Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3)]["id_respuesta"].ToString());

            }
            else if (rbtB.Checked == true)
            {
                if (Convert.ToString(dtrespuestas.Rows[((3 * x) - 3) + 1]["respuesta_correcta"]) == "si")
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                idresp = Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3) + 1]["id_respuesta"].ToString());
            }
            else if (rbtC.Checked == true)
            {
                if (Convert.ToString(dtrespuestas.Rows[((3 * x) - 3) + 2]["respuesta_correcta"]) == "si")
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                idresp = Convert.ToInt32(dtrespuestas.Rows[((3 * x) - 3) + 2]["id_respuesta"].ToString());

            }
        }

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private bool Validar()
        {
            if ((rbtA.Checked == false) && (rbtB.Checked == false) && (rbtC.Checked == false))
            {
                lblMsj.Text = "Debe Seleccionar una opcion";
                btnSiguiente.Enabled = false;
                return false;
            }
            btnSiguiente.Enabled = true;
            this.btnSiguiente.Focus();
            return true;
        }

        private void limpiarcampos()
        {
            foreach (Control radio in this.gbPreguntas.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                modelo_IPV = cliente.NuevoIPV(_expediente.Numero, _escolaridad_prueba);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message,
                    "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            this.prueba_inicada = true;
            NumeroAsignacion = modelo_IPV.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 87";
            this.panelAyuda.Hide();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
        }



        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void rbtA_Click(object sender, EventArgs e)
        {
            Validar();
        }

        private void rbtB_Click(object sender, EventArgs e)
        {
            Validar();
        }

        private void rbtC_Click(object sender, EventArgs e)
        {
            Validar();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela el proceso de la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba IPV", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_inicada)
                cliente.CambiarEstadoPrueba(Test.IPV, NumeroAsignacion, PruebaEstado.CANCELADA);
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void frmIPV_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
