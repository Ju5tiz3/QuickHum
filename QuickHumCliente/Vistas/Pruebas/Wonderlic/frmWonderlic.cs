﻿using System;
using System.Data;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Wonderlic
{
    public partial class frmWonderlic : Form
    {
        public frmWonderlic(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        private readonly QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        readonly dto_wonderlic_respuestas_candidato respcandidato = new dto_wonderlic_respuestas_candidato();
        WONDERLIC wonderlic = new WONDERLIC();
        dto_wonderlic_respuestas_candidato[] listaresp;

        readonly DataTable dtrespcorrectas = new DataTable();
        readonly DataTable dtpreguntas = new DataTable();
        readonly DataTable dtactualizar = new DataTable();
        DataRow[] row;
        private int conthoras = 0, contminutos = 0,contsegundos = 0;
        int x = 0, recorrido = 0, punto = 0, numero_prueba;
        string respuesta;
        readonly Bitmap[] Imagenes = new Bitmap[50];

        private void Form2_Load(object sender, EventArgs e)
        {
            //this.Size = new Size(548, 545);
            this.panelAyuda.Location = new Point(1, 1);
            CargarImagenes();
            listardatos();
            validar();
            movercontroles();
            DateTime hora;
            hora = Convert.ToDateTime("0:12:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void CargarImagenes()
        {
            Imagenes[0] = Properties.RecursosPruebas.wonderlic1;
            Imagenes[1] = Properties.RecursosPruebas.wonderlic2;
            Imagenes[2] = Properties.RecursosPruebas.wonderlic3;
            Imagenes[3] = Properties.RecursosPruebas.wonderlic4;
            Imagenes[4] = Properties.RecursosPruebas.wonderlic5;
            Imagenes[5] = Properties.RecursosPruebas.wonderlic6;
            Imagenes[6] = Properties.RecursosPruebas.wonderlic7;
            Imagenes[7] = Properties.RecursosPruebas.wonderlic8;
            Imagenes[8] = Properties.RecursosPruebas.wonderlic9;
            Imagenes[9] = Properties.RecursosPruebas.wonderlic10;
            Imagenes[10] = Properties.RecursosPruebas.wonderlic11;
            Imagenes[11] = Properties.RecursosPruebas.wonderlic12;
            Imagenes[12] = Properties.RecursosPruebas.wonderlic13;
            Imagenes[13] = Properties.RecursosPruebas.wonderlic14;
            Imagenes[14] = Properties.RecursosPruebas.wonderlic15;
            Imagenes[15] = Properties.RecursosPruebas.wonderlic16;
            Imagenes[16] = Properties.RecursosPruebas.wonderlic17;
            Imagenes[17] = Properties.RecursosPruebas.wonderlic18;
            Imagenes[18] = Properties.RecursosPruebas.wonderlic19;
            Imagenes[19] = Properties.RecursosPruebas.wonderlic20;
            Imagenes[20] = Properties.RecursosPruebas.wonderlic21;
            Imagenes[21] = Properties.RecursosPruebas.wonderlic22;
            Imagenes[22] = Properties.RecursosPruebas.wonderlic23;
            Imagenes[23] = Properties.RecursosPruebas.wonderlic24;
            Imagenes[24] = Properties.RecursosPruebas.wonderlic25;
            Imagenes[25] = Properties.RecursosPruebas.wonderlic26;
            Imagenes[26] = Properties.RecursosPruebas.wonderlic27;
            Imagenes[27] = Properties.RecursosPruebas.wonderlic28;
            Imagenes[28] = Properties.RecursosPruebas.wonderlic29;
            Imagenes[29] = Properties.RecursosPruebas.wonderlic30;
            Imagenes[30] = Properties.RecursosPruebas.wonderlic31;
            Imagenes[31] = Properties.RecursosPruebas.wonderlic32;
            Imagenes[32] = Properties.RecursosPruebas.wonderlic33;
            Imagenes[33] = Properties.RecursosPruebas.wonderlic34;
            Imagenes[34] = Properties.RecursosPruebas.wonderlic35;
            Imagenes[35] = Properties.RecursosPruebas.wonderlic36;
            Imagenes[36] = Properties.RecursosPruebas.wonderlic37;
            Imagenes[37] = Properties.RecursosPruebas.wonderlic38;
            Imagenes[38] = Properties.RecursosPruebas.wonderlic39;
            Imagenes[39] = Properties.RecursosPruebas.wonderlic40;
            Imagenes[40] = Properties.RecursosPruebas.wonderlic41;
            Imagenes[41] = Properties.RecursosPruebas.wonderlic42;
            Imagenes[42] = Properties.RecursosPruebas.wonderlic43;
            Imagenes[43] = Properties.RecursosPruebas.wonderlic44;
            Imagenes[44] = Properties.RecursosPruebas.wonderlic45;
            Imagenes[45] = Properties.RecursosPruebas.wonderlic46;
            Imagenes[46] = Properties.RecursosPruebas.wonderlic47;
            Imagenes[47] = Properties.RecursosPruebas.wonderlic48;
            Imagenes[48] = Properties.RecursosPruebas.wonderlic49;
            Imagenes[49] = Properties.RecursosPruebas.wonderlic50;
        }

        private void Limpiar()
        {
            txtA.Clear();
            txtB.Clear();
            txtC.Clear();
            txtD.Clear();
        }
        private void btnsiguiente_Click(object sender, EventArgs e)
        {
           
            if(btnsiguiente.Text=="Siguiente")
            {
                x++;
                validar();
                GuardarRespuestas();
                MostrarPreguntas(x);
                Limpiar();
                btnsiguiente.Enabled = false;
                movercontroles();
                txtA.Focus();
            }

            else if (btnsiguiente.Text=="Siguiente.")
            {
                x++;
                validar();
                ActualizarRespuestas();
                if (recorrido > x)
                {
                    LlenarTextbox();
                }
                if (recorrido <= x)
                {
                    btnsiguiente.Enabled = false;
                    btnsiguiente.Text = "Siguiente";
                    Limpiar();
                }
                MostrarPreguntas(x);
                txtA.Focus();
                movercontroles();
            }

            else if (btnsiguiente.Text=="Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Wonderlic", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Close();
                }
            }

            if (x == dtpreguntas.Rows.Count - 1)
            {
                btnsiguiente.Text = "Finalizar";
            }
            btnanterior.Enabled = true;
        }


        private void btnanterior_Click(object sender, EventArgs e)
        {
            listaresp = cliente.ListaPreguntasContestadasWonderlic(numero_prueba);
            dtactualizar.Clear();
            for(int w = 0; w <listaresp.Length; w++)
            {
                DataRow row = dtactualizar.NewRow();
                row["idrespuestacandidato"] = Convert.ToInt32(listaresp[w].Idrespuestacandidato);
                row["id_pregunta"] = Convert.ToInt32(listaresp[w].Idpregunta);
                 row["respuesta"]= Convert.ToString(listaresp[w].Respuesta);
                row["punto"] = Convert.ToInt32(listaresp[w].Punto);
                row["numero_prueba"] = Convert.ToInt32(listaresp[w].numero_prueba);
                dtactualizar.Rows.Add(row);
            }
            recorrido = dtactualizar.Rows.Count;

            x--;
            btnsiguiente.Text = "Siguiente.";
           
            validar();
            lblNumPreg.Text= "Pregunta: "+(x)+ " de 50";
            MostrarPreguntas(x);
            
           
            if (recorrido > x)
            {
                LlenarTextbox();
            }

            if (x == 0)
            {
                btnanterior.Enabled = false;
            }
            if (this.btnsiguiente.Text == "Finalizar")
            {
                this.btnsiguiente.Text = "Siguiente";

            }
            btnsiguiente.Enabled = true;
            movercontroles();
        }

        private void Evaluar()
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x-1]["tiporespuesta"].ToString()) == 4)
            {
                if (Convert.ToString(dtrespcorrectas.Rows[x - 1]["respuesta"].ToString()) == txtA.Text.Trim().ToLower() + "," + txtB.Text.Trim().ToLower())
                {punto = 1;}
                else
                {punto = 0;}
                respuesta = txtA.Text.Trim().ToLower() + "," + txtB.Text.Trim().ToLower();
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x - 1]["tiporespuesta"].ToString()) == 5)
            {
                if (Convert.ToString(dtrespcorrectas.Rows[x - 1]["respuesta"].ToString()) == txtA.Text.Trim().ToLower() + "," + txtB.Text.Trim().ToLower() + "," + txtC.Text.Trim().ToLower() + "," + txtD.Text.Trim().ToLower())
                {punto = 1;}
                else
                { punto = 0; }
                respuesta = txtA.Text.Trim().ToLower() + "," + txtB.Text.Trim().ToLower() + "," + txtC.Text.Trim().ToLower() + "," + txtD.Text.Trim().ToLower();
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x - 1]["tiporespuesta"].ToString()) == 1 || Convert.ToInt32(dtpreguntas.Rows[x - 1]["tiporespuesta"].ToString()) == 2 || Convert.ToInt32(dtpreguntas.Rows[x - 1]["tiporespuesta"].ToString()) == 3)
            {
                if (dtrespcorrectas.Rows[x - 1]["respuesta"].ToString() == txtA.Text.Trim().ToLower())
                { punto = 1; }
                else
                { punto = 0; }
       
            respuesta = txtA.Text.Trim().ToLower();
           }
            pictureBox1.Name = dtpreguntas.Rows[x - 1]["idpregunta"].ToString();
        }

        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idpregunta = Convert.ToInt32( pictureBox1.Name);
            respcandidato.Respuesta =Convert.ToString( respuesta);
            respcandidato.Punto = punto;
            respcandidato.numero_prueba= numero_prueba;
            cliente.InsertarRespuestaWonderlic(respcandidato);
        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtactualizar.Rows[x - 1]["idrespuestacandidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(pictureBox1.Name);
            respcandidato.Respuesta = Convert.ToString(respuesta);
            respcandidato.Punto = Convert.ToInt32(punto);
            cliente.ActualizaRespuestaWonderlic(respcandidato);
        }

        private void LlenarTextbox()
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
            {
                string[] numero = listaresp[x].Respuesta.Split(',');

                txtA.Text = numero[0];
                txtB.Text = numero[1]; ;
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
                string[] separar = listaresp[x ].Respuesta.Split(',');

                txtA.Text = separar[0];
                txtB.Text = separar[1];
                txtC.Text = separar[2];
                txtD.Text = separar[3];
            }
            else 
            {
                txtA.Text=dtactualizar.Rows[x ]["respuesta"].ToString();
            }
        }

        /// <summary>
        /// Funcion para Validar Numeros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ex"></param>
        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }
        }

        /// <summary>
        /// Funcion para valida los TextBox
        /// Significado de Tipo de Respuesta 
        /// 1.Valida Solo Texto
        /// 2.Valida Solo Numeros.
        /// 3.Valida Texto y numeros.
        /// 4.Habilita 2 TextBox
        /// 5.Habilita 4 TextBox.
        /// </summary>
        private void validar()
        {
            //valida solo numeros
            if (Convert.ToInt32( dtpreguntas.Rows[x]["tiporespuesta"].ToString())==2)
            {
               if (txtA.Text.Length==0)
               {
                   btnsiguiente.Enabled = false;
               }
               else
               {
                   btnsiguiente.Enabled = true;
                 
               }
               
                txtB.Visible = false;
                txtC.Visible = false;
                txtD.Visible = false;
            }
                //valida solo texto
            else if (Convert.ToInt32( dtpreguntas.Rows[x]["tiporespuesta"].ToString())==1)
            {
               
                if (txtA.Text.Length ==0)
                {
                    btnsiguiente.Enabled = false;
                }
                else
                {
                    btnsiguiente.Enabled = true;
                    
                }
                
                txtB.Visible = false;
                txtC.Visible = false;
                txtD.Visible = false;
            }
                //valida texto y numero

            else if (Convert.ToInt32( dtpreguntas.Rows[x]["tiporespuesta"].ToString())==3)
            {
              
                if (txtA.Text.Length ==0)
                {
                    btnsiguiente.Enabled = false;
                }
                else
                {
                    btnsiguiente.Enabled = true;
                  
                }
                txtB.Visible = false;
                txtC.Visible = false;
                txtD.Visible = false;
            }
                //habilita 2 textbox
            else if (Convert.ToInt32( dtpreguntas.Rows[x]["tiporespuesta"].ToString())==4)
            {
               
                if (txtA.Text.Length == 0 || txtB.Text.Length==0)
                {
                    btnsiguiente.Enabled = false;
                }
                else
                {
                    btnsiguiente.Enabled = true;
                   
                }
                txtB.Visible = true;
                txtC.Visible = false;
                txtD.Visible = false;
            }
                //habilita 4 texbox
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
              
                if (txtA.Text.Length == 0 || txtB.Text.Length == 0 || txtC.Text.Length==0 || txtD.Text.Length==0)
                {
                    btnsiguiente.Enabled = false;
                }
                else
                {
                    btnsiguiente.Enabled = true;
                   
                }
                txtB.Visible = true;
                txtC.Visible = true;
                txtD.Visible = true;
            }
        }
        private void listardatos()
        {
            dto_wonderlic_preguntas[] preguntas = cliente.ListarPreguntasWonderlic();
            dto_wonderlic_respuestas_correctas[] respuestas = cliente.ListarRespuetasCorrectasWonderlic();

            dtpreguntas.Columns.Add("idpregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("tiporespuesta");

            dtrespcorrectas.Columns.Add("idrespuestacorrecta");
            dtrespcorrectas.Columns.Add("respuesta");

            dtactualizar.Columns.Add("idrespuestacandidato");
            dtactualizar.Columns.Add("id_pregunta");
            dtactualizar.Columns.Add("respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("numero_prueba");

            for(int i=0;i<preguntas.Length; i++)
            {
                DataRow row = dtpreguntas.NewRow();

                row["idpregunta"] = Convert.ToInt32(preguntas[i].Idpregunta);
                 row["pregunta"] = Convert.ToInt32(preguntas[i].Pregunta);
                 row["tiporespuesta"] = Convert.ToString(preguntas[i].Tipo_respuesta);
                 dtpreguntas.Rows.Add(row);
            }

            for (int i = 0; i < respuestas.Length; i++)
            {
                DataRow row = dtrespcorrectas.NewRow();

                row["idrespuestacorrecta"] = Convert.ToInt32(respuestas[i].Idrespuestacorrecta);
                row["respuesta"] = Convert.ToString(respuestas[i].Respuesta);
                
                dtrespcorrectas.Rows.Add(row);
            }


            pictureBox1.Image = Imagenes[0];

          

            DateTime hora;
            hora = Convert.ToDateTime("0:12:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }
        private void MostrarPreguntas(int i)
        {
            lblNumPreg.Text = "Pregunta: " + (i + 1) + " de 50.";
            pictureBox1.Image = Imagenes[x];

        }
        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                wonderlic = cliente.NuevoWonderlic(this._expediente.Numero, _escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            numero_prueba = wonderlic.IdPrueba;

            this.lblNumPreg.Text = "Pregunta: 1 de 50";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            MessageBox.Show("Dispone de 12 Minutos para realizar la prueba.", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Information);
            this.tiempo.Enabled = true;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
          
        }

        #region EventoKeyPress de los TExbox
        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 2)
            {
                numeros(sender, e);

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 1)
            {
                if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
                {

                    e.Handled = true;
                    return;
                }

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
            {
                numeros(sender, e);
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
                numeros(sender, e);
            }

        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 2)
            {
                numeros(sender, e);

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 1)
            {
                if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
                {

                    e.Handled = true;
                    return;
                }

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
            {
                numeros(sender, e);
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
                numeros(sender, e);
            }

        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 2)
            {
                numeros(sender, e);

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 1)
            {
                if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
                {

                    e.Handled = true;
                    return;
                }

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
            {
                numeros(sender, e);
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
                numeros(sender, e);
            }

        }

        private void txtD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 2)
            {
                numeros(sender, e);

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 1)
            {
                if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
                {

                    e.Handled = true;
                    return;
                }

            }

            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
            {
                numeros(sender, e);
            }
            else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
            {
                numeros(sender, e);
            }

        }
        #endregion

        #region Eventos Keyup y textchanged de los Textbox
        private void txtA_KeyUp(object sender, KeyEventArgs e)
        {
            validar();
        }

        private void txtB_KeyUp(object sender, KeyEventArgs e)
        {
            validar();
        }

        private void txtC_KeyUp(object sender, KeyEventArgs e)
        {
            validar();
        }

        private void txtD_KeyUp(object sender, KeyEventArgs e)
        {
            validar();
        }

        private void txtA_TextChanged(object sender, EventArgs e)
        {
            if (x == 48 || x == 22 || x == 37)
            {
                txtB.Focus();
            }

        }

        private void txtB_TextChanged(object sender, EventArgs e)
        {
            if (x == 48)
            {
                txtC.Focus();
            }
            else if (x == 22)
            { btnsiguiente.Focus(); }
        }

        private void txtC_TextChanged(object sender, EventArgs e)
        {

            txtD.Focus();

        }

        private void txtD_TextChanged(object sender, EventArgs e)
        {
            btnsiguiente.Focus();
        }
        #endregion
       

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void FinalizarPrueba()
       {
           if (btnsiguiente.Text=="Siguiente" || btnsiguiente.Text=="Finalizar")
           {
               if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
               {
                   if (txtA.Text.Length == 0 || txtB.Text.Length == 0 || txtC.Text.Length == 0 || txtD.Text.Length == 0)
                   {

                   }
                   else
                   {
                       x++;
                       GuardarRespuestas();
                   }
               }

               else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
               {
                   if (txtA.Text.Length == 0 || txtB.Text.Length == 0 )
                   {

                   }
                   else
                   {
                       x++;
                       GuardarRespuestas();
                   }
               }
               else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 1 || Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 2 || Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 3)
               {
                   if (txtA.Text.Length == 0)
                   {

                   }
                   else
                   {
                       x++;
                       GuardarRespuestas();
                   }
               } 
           }
           respcandidato.numero_prueba = numero_prueba;
           cliente.FinalizaPruebaWonderlic(respcandidato);
           this.Close();
       }
        private void movercontroles()
        {
             if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 5)
             {
                 lblresp.Location = new Point(186, 221);
                 txtA.Location = new Point(272, 218);
                 txtB.Location = new Point(338, 218);
                 txtC.Location = new Point(401, 218);
                 txtD.Location = new Point(464, 218);
             }

              else if (Convert.ToInt32(dtpreguntas.Rows[x]["tiporespuesta"].ToString()) == 4)
             {
                 lblresp.Location = new Point(318, 221);
                 txtA.Location = new Point(401, 218);
                 txtB.Location = new Point(464, 218);
             }

             else
             {
                 lblresp.Location = new Point(381, 218);
                 txtA.Location = new Point(464, 218);
             }
        }

        private void frmWonderlic_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }

        private void CancelarPrueba()
        {
            
        }

        private void btnCloseAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
        }

        private void btnCerrarVentanaPrueba_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Wonderlic", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            this.Close();
        }

        
    }
}
