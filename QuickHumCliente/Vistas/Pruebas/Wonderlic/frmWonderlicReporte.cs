﻿using System;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.ServicioQuickHum;
using QuickHumCliente.Motor.Negocio;

namespace QuickHumCliente.Vistas.Pruebas.Wonderlic
{
    public partial class frmWonderlicReporte : Form
    {
        public frmWonderlicReporte(Expediente expediente)
        {
            InitializeComponent();
            this._expediente = expediente;
        }

        private readonly QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        readonly DataTable dtresultado = new DataTable();
        readonly DataTable dtinterpretacion = new DataTable();
        DataTable dtCandidato = new DataTable();

        int punto = 0, ajuste = 0;
        string  rango = "", interpretacion = "";

        private void frmWonderlicReporte_Load(object sender, EventArgs e)
        {
            dto_wonderlic_rangos[] interpretacion = cliente.ListaWonderlicinterpretacion();
            
          
            
            dtinterpretacion.Columns.Add("puntajemenor");
            dtinterpretacion.Columns.Add("puntajemayor");
            dtinterpretacion.Columns.Add("rango");
            dtinterpretacion.Columns.Add("interpretacion");
            dtinterpretacion.Columns.Add("idrango");

            for (int x = 0; x < interpretacion.Length;x++ )
            {
                DataRow rows = dtinterpretacion.NewRow();
                rows["idrango"]=Convert.ToInt32(interpretacion[x].Idrango);
                rows["puntajemenor"]=Convert.ToInt32(interpretacion[x].Puntajemenor);
                rows["puntajemayor"]=Convert.ToInt32(interpretacion[x].Puntajemayor);
                rows["rango"]=Convert.ToString(interpretacion[x].Rango);
                rows["interpretacion"]=Convert.ToString(interpretacion[x].Interpretacion);
                dtinterpretacion.Rows.Add(rows);
            }

            dtresultado.Columns.Add("Puntos");
            dtresultado.Columns.Add("Ajuste");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            CargarReporte();
           
        }
        
        private void AjusteEdad()
        {
            if (this._expediente.Candidato.Edad <= 39 && this._expediente.Candidato.Edad >= 30)
            {
                ajuste = punto + 1;
            }
            else if (this._expediente.Candidato.Edad <= 49 && this._expediente.Candidato.Edad >= 40)
            {
                ajuste = punto + 2;
            }
            else if (this._expediente.Candidato.Edad <= 54 && this._expediente.Candidato.Edad >= 50)
            {
                ajuste = punto + 3;
            }
            else if (this._expediente.Candidato.Edad <= 59 && this._expediente.Candidato.Edad >= 55)
            {
                ajuste = punto + 4;
            }
            else if (this._expediente.Candidato.Edad >= 60)
            {
                ajuste = punto + 5;
            }
            else if (this._expediente.Candidato.Edad <= 29)
            {
                ajuste = punto;
            }
        }

        private void CargarReporte()
        {
            dtresultado.Clear();
            punto = cliente.ListaWonderlicResultados(this._expediente.Id);
            AjusteEdad();
            if (ajuste >= 35)
            {
                rango = dtinterpretacion.Rows[5]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[5]["interpretacion"].ToString();
            }
            else if (ajuste <= 34 && ajuste >= 25)
            {
                rango = dtinterpretacion.Rows[4]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[54]["interpretacion"].ToString();
            }
            else if (ajuste <= 24 && ajuste >= 20)
            {
                rango = dtinterpretacion.Rows[3]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[3]["interpretacion"].ToString();
            }
            else if (ajuste <= 19 && ajuste >= 16)
            {
                rango = dtinterpretacion.Rows[2]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[2]["interpretacion"].ToString();
            }

            else if (ajuste <= 15 && ajuste >= 13)
            {
                rango = dtinterpretacion.Rows[1]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[1]["interpretacion"].ToString();
            }
            else if (ajuste <= 12)
            {
                rango = dtinterpretacion.Rows[0]["rango"].ToString();
                interpretacion = dtinterpretacion.Rows[0]["interpretacion"].ToString();
            }

            DataRow row = dtresultado.NewRow();
            row["Puntos"] = punto;
            row["Ajuste"] = ajuste;
            dtresultado.Rows.Add(row);

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportParameter parametro = new ReportParameter();
            parametro = new ReportParameter("Rango", rango + ": " + interpretacion);

            ReportDataSource rds = new ReportDataSource("DataSetWonderlic", dtresultado);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Wonderlic.reporteWonderlic.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.SetParameters(parametro);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.RefreshReport();
        }
    }
}
