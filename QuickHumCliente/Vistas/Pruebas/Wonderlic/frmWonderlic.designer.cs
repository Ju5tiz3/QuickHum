﻿namespace QuickHumCliente.Vistas.Pruebas.Wonderlic
{
    partial class frmWonderlic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWonderlic));
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtD = new System.Windows.Forms.TextBox();
            this.txtC = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.txtA = new System.Windows.Forms.TextBox();
            this.lblresp = new System.Windows.Forms.Label();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.btnCerrarVentanaPrueba = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnanterior = new System.Windows.Forms.Button();
            this.btnsiguiente = new System.Windows.Forms.Button();
            this.panelAyuda.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelAyuda
            // 
            this.panelAyuda.AutoScroll = true;
            this.panelAyuda.Controls.Add(this.panel1);
            this.panelAyuda.Controls.Add(this.label1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(591, 511);
            this.panelAyuda.TabIndex = 64;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 11F);
            this.richTextBox1.Location = new System.Drawing.Point(10, 45);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox1.Size = new System.Drawing.Size(523, 362);
            this.richTextBox1.TabIndex = 72;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtD
            // 
            this.txtD.Font = new System.Drawing.Font("Calibri", 16F);
            this.txtD.Location = new System.Drawing.Point(470, 203);
            this.txtD.Name = "txtD";
            this.txtD.Size = new System.Drawing.Size(47, 34);
            this.txtD.TabIndex = 6;
            this.txtD.TextChanged += new System.EventHandler(this.txtD_TextChanged);
            this.txtD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtD_KeyPress);
            this.txtD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtD_KeyUp);
            // 
            // txtC
            // 
            this.txtC.Font = new System.Drawing.Font("Calibri", 16F);
            this.txtC.Location = new System.Drawing.Point(407, 203);
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(47, 34);
            this.txtC.TabIndex = 5;
            this.txtC.TextChanged += new System.EventHandler(this.txtC_TextChanged);
            this.txtC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtC_KeyPress);
            this.txtC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtC_KeyUp);
            // 
            // txtB
            // 
            this.txtB.Font = new System.Drawing.Font("Calibri", 16F);
            this.txtB.Location = new System.Drawing.Point(344, 203);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(47, 34);
            this.txtB.TabIndex = 4;
            this.txtB.TextChanged += new System.EventHandler(this.txtB_TextChanged);
            this.txtB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_KeyPress);
            this.txtB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtB_KeyUp);
            // 
            // txtA
            // 
            this.txtA.Font = new System.Drawing.Font("Calibri", 16F);
            this.txtA.Location = new System.Drawing.Point(278, 203);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(47, 34);
            this.txtA.TabIndex = 3;
            this.txtA.TextChanged += new System.EventHandler(this.txtA_TextChanged);
            this.txtA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_KeyPress);
            this.txtA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtA_KeyUp);
            // 
            // lblresp
            // 
            this.lblresp.AutoSize = true;
            this.lblresp.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblresp.Location = new System.Drawing.Point(177, 212);
            this.lblresp.Name = "lblresp";
            this.lblresp.Size = new System.Drawing.Size(76, 18);
            this.lblresp.TabIndex = 2;
            this.lblresp.Text = "Respuesta:";
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblNumPreg.Location = new System.Drawing.Point(208, 2);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(111, 18);
            this.lblNumPreg.TabIndex = 66;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnCerrarAyuda);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.btnIniciarAyuda);
            this.panel1.Location = new System.Drawing.Point(24, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(545, 467);
            this.panel1.TabIndex = 73;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.txtD);
            this.groupBox1.Controls.Add(this.btnAyuda);
            this.groupBox1.Controls.Add(this.txtC);
            this.groupBox1.Controls.Add(this.btnanterior);
            this.groupBox1.Controls.Add(this.lblresp);
            this.groupBox1.Controls.Add(this.btnsiguiente);
            this.groupBox1.Controls.Add(this.txtB);
            this.groupBox1.Controls.Add(this.txtA);
            this.groupBox1.Location = new System.Drawing.Point(32, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 389);
            this.groupBox1.TabIndex = 77;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(591, 36);
            this.label3.TabIndex = 78;
            this.label3.Text = "PRUEBA WONDERLIC";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(545, 36);
            this.label2.TabIndex = 73;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.lblNumPreg);
            this.panel2.Location = new System.Drawing.Point(0, 260);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(531, 23);
            this.panel2.TabIndex = 68;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(510, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 74;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCloseAyuda_Click);
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.Resources.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(211, 418);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(132, 37);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.lbltime);
            this.panel3.Location = new System.Drawing.Point(348, 45);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 37);
            this.panel3.TabIndex = 80;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(42, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 76;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbltime.Location = new System.Drawing.Point(161, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 75;
            this.lbltime.Text = "label9";
            // 
            // btnCerrarVentanaPrueba
            // 
            this.btnCerrarVentanaPrueba.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarVentanaPrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarVentanaPrueba.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCerrarVentanaPrueba.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarVentanaPrueba.FlatAppearance.BorderSize = 0;
            this.btnCerrarVentanaPrueba.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarVentanaPrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarVentanaPrueba.Location = new System.Drawing.Point(556, 1);
            this.btnCerrarVentanaPrueba.Name = "btnCerrarVentanaPrueba";
            this.btnCerrarVentanaPrueba.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarVentanaPrueba.TabIndex = 79;
            this.btnCerrarVentanaPrueba.UseVisualStyleBackColor = false;
            this.btnCerrarVentanaPrueba.Click += new System.EventHandler(this.btnCerrarVentanaPrueba_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(14, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(503, 171);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.Resources.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(26, 323);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(121, 36);
            this.btnAyuda.TabIndex = 67;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnanterior
            // 
            this.btnanterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnanterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnanterior.Enabled = false;
            this.btnanterior.FlatAppearance.BorderSize = 0;
            this.btnanterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnanterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnanterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnanterior.Image = global::QuickHumCliente.Properties.Resources.previousBtn;
            this.btnanterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnanterior.Location = new System.Drawing.Point(204, 323);
            this.btnanterior.Name = "btnanterior";
            this.btnanterior.Size = new System.Drawing.Size(121, 36);
            this.btnanterior.TabIndex = 2;
            this.btnanterior.Text = "Anterior";
            this.btnanterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnanterior.UseVisualStyleBackColor = false;
            this.btnanterior.Click += new System.EventHandler(this.btnanterior_Click);
            // 
            // btnsiguiente
            // 
            this.btnsiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnsiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsiguiente.Enabled = false;
            this.btnsiguiente.FlatAppearance.BorderSize = 0;
            this.btnsiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnsiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnsiguiente.Image = global::QuickHumCliente.Properties.Resources.nextBtn;
            this.btnsiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsiguiente.Location = new System.Drawing.Point(386, 323);
            this.btnsiguiente.Name = "btnsiguiente";
            this.btnsiguiente.Size = new System.Drawing.Size(121, 36);
            this.btnsiguiente.TabIndex = 0;
            this.btnsiguiente.Text = "Siguiente";
            this.btnsiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsiguiente.UseVisualStyleBackColor = false;
            this.btnsiguiente.Click += new System.EventHandler(this.btnsiguiente_Click);
            // 
            // frmWonderlic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(591, 547);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnCerrarVentanaPrueba);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "frmWonderlic";
            this.Text = "Prueba Wonderlic";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWonderlic_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panelAyuda.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnsiguiente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnanterior;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.TextBox txtD;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Label lblresp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Button btnCerrarVentanaPrueba;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;

    }
}