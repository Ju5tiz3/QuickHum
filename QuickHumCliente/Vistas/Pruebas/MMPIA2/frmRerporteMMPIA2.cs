﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.MMPIA2
{
    public partial class frmRerporteMMPIA2 : DevExpress.XtraEditors.XtraForm
    {
        private Expediente _expediente = null;

        DataTable data = new DataTable();
        DataTable dtCandidato = new DataTable();
        byte Genero, Edad;

        public frmRerporteMMPIA2(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void xfrmRerporteMMPIA_2_Load(object sender, EventArgs e)
        {

            QuickHumClient cliente = Globales.cliente;

            labName.Text = _expediente.Candidato.Nombre + " " + _expediente.Candidato.Apellido;
            labEdad.Text = _expediente.Candidato.Edad.ToString();
            labSexo.Text = Convert.ToString((CandidatoGenero)_expediente.Candidato.Genero);
            Genero = (byte)(CandidatoGenero)_expediente.Candidato.Genero;
            Edad = _expediente.Candidato.Edad;

            data.Columns.Add("Escala");
            data.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            dto_MMPIA2Factor[] modelo_factor = cliente.ColecionPuntosMMPIA2(_expediente.Id,Genero);

            for (int x = 0; x < modelo_factor.Length; x++)
            {
                if (modelo_factor[x].RangoGenero != 0)
                {
                    DataRow rows = data.NewRow();
                    rows["Escala"] = modelo_factor[x].NombreEscala;
                    rows["Rango"] = Convert.ToInt32(modelo_factor[x].RangoGenero);
                    data.Rows.Add(rows);
                }
            }

            ReportDataSource ReportData = new ReportDataSource("dtMMPIA2", data);
            ReportParameter ReportParameter = new ReportParameter("sexo","1".ToString());
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            rpvMMPIA2.LocalReport.DataSources.Clear();
            rpvMMPIA2.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.MMPIA2.ReporteMMPIA-2.rdlc";
            rpvMMPIA2.LocalReport.DataSources.Add(ReportData);
            rpvMMPIA2.LocalReport.SetParameters(ReportParameter);
            rpvMMPIA2.LocalReport.DataSources.Add(rds_candidato);
            rpvMMPIA2.RefreshReport();
        }
    }
}