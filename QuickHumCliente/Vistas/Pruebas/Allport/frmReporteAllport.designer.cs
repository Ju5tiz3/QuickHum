﻿namespace QuickHumCliente.Vistas.Pruebas.Allport
{
    partial class frmReporteAllport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReporteAllport));
            this.dtAllportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptPruebaAllport = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.dtAllportBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtAllportBindingSource
            // 
            this.dtAllportBindingSource.DataMember = "dtAllport";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptPruebaAllport);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(904, 496);
            this.panel2.TabIndex = 0;
            // 
            // rptPruebaAllport
            // 
            this.rptPruebaAllport.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.dtAllportBindingSource;
            this.rptPruebaAllport.LocalReport.DataSources.Add(reportDataSource1);
            this.rptPruebaAllport.LocalReport.ReportEmbeddedResource = "capaPresentacion.rptPruebaAllport.rdlc";
            this.rptPruebaAllport.Location = new System.Drawing.Point(0, 0);
            this.rptPruebaAllport.Name = "rptPruebaAllport";
            this.rptPruebaAllport.Size = new System.Drawing.Size(904, 496);
            this.rptPruebaAllport.TabIndex = 0;
            // 
            // frmReporteAllport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 496);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReporteAllport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resultados Allport";
            this.Load += new System.EventHandler(this.frmReporteAllport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtAllportBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer rptPruebaAllport;
        private System.Windows.Forms.BindingSource dtAllportBindingSource;
        //private dsReportes dsReportes;
    }
}