﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Allport
{
    public partial class frmReporteAllport : Form
    {
        QuickHumClient Cliente = Globales.cliente;
        DataTable dtResultados = new DataTable();
        DataTable dtCandidato = new DataTable();

        private Expediente _expediente = null;


        public frmReporteAllport(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        

        private void frmReporteAllport_Load(object sender, EventArgs e)
        {
            dtResultados.Columns.Add("Factor");
            dtResultados.Columns.Add("Valor");
            dtResultados.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");
            GenerarReporte();
        }

        private void GenerarReporte()
        {
            dtResultados.Clear();

            dto_allport_resultados[] Resultados;
            try
            {
                Resultados = Cliente.ListaAllportResultados(this._expediente.Id);
                for (int i = 0; i < Resultados.ToList().Count; i++)
                {
                    //Validamos que no sea el valor NULO el cual no debe de tomarse en cuenta dentro de los resultados del reporte
                    if (Resultados[i].Factor != "NULO")
                    {
                        DataRow row = dtResultados.NewRow();
                        row["Factor"] = Convert.ToString(Resultados[i].Factor);
                        row["Valor"] = Convert.ToInt32(Resultados[i].Suma);
                        dtResultados.Rows.Add(row);
                    }
                }

                // COnstruimos el del candidato
                DataRow row_candidato = dtCandidato.NewRow();
                row_candidato["Dui"] = this._expediente.Candidato.Dui;
                row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
                row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
                row_candidato["Edad"] = this._expediente.Candidato.Edad;
                row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
                row_candidato["Fecha"] = this._expediente.Fecha;
                dtCandidato.Rows.Add(row_candidato);

                ReportParameter Parametros = new ReportParameter();
                Parametros = new ReportParameter("DUI", this._expediente.Candidato.Dui);

                ReportDataSource rds = new ReportDataSource("DataSet1", dtResultados);
                ReportDataSource rds_candidato = new ReportDataSource("dsetCandidato", dtCandidato);
                rptPruebaAllport.LocalReport.DataSources.Clear();
                //rptPruebaMoss.LocalReport.ReportPath = "rptPruebaMoss.rdlc";
                rptPruebaAllport.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Allport.rptPruebaAllport.rdlc";
                rptPruebaAllport.LocalReport.DataSources.Add(rds);
                rptPruebaAllport.LocalReport.DataSources.Add(rds_candidato);
                //rptPruebaAllport.LocalReport.SetParameters(Parametros);
                rptPruebaAllport.RefreshReport();
            }
            catch (Exception)
            {
                MessageBox.Show("Candidato no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }          
        }
 
    }
}
