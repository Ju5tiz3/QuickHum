﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Allport
{
    public partial class frmAllport : Form
    {

        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;
        dto_allport_respuestas_candidato RespuestasCandidato = new dto_allport_respuestas_candidato();


        //Variable que funcionara como parametro.
        int Numero_prueba;

        DataTable dtPreguntas = new DataTable();
        DataTable dtRespuestas = new DataTable();
        DataTable dtResCandidato = new DataTable();
        DataRow[] row;
        int x = 0;
        int Recorrido = 0;
        int Etapa = 0;

        public frmAllport(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        private void frmAllport_Load(object sender, EventArgs e)
        {
            //Estructurando el DataTable ResCandidato
            dtResCandidato.Columns.Add("id_respuesta_candidato");
            dtResCandidato.Columns.Add("id_respuesta");
            dtResCandidato.Columns.Add("puntaje");


            DateTime hora;

            CargarPreguntasRespuestas();

            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void CargarPreguntasRespuestas()
        {

            dto_allport_preguntas[] ListaPreguntas = Cliente.ListaAllportPreguntas();

            dtPreguntas.Columns.Add("id_pregunta");
            dtPreguntas.Columns.Add("Descripcion");
            dtPreguntas.Columns.Add("Etapa");

            dtRespuestas.Columns.Add("id_respuesta");
            dtRespuestas.Columns.Add("id_pregunta");
            dtRespuestas.Columns.Add("Descripcion");
            dtRespuestas.Columns.Add("id_factor");

            for (int i = 0; i < ListaPreguntas.ToList().Count; i++)
            {
                DataRow row = dtPreguntas.NewRow();

                row["id_pregunta"] = Convert.ToInt32(ListaPreguntas[i].IdPregunta);
                row["Descripcion"] = Convert.ToString(ListaPreguntas[i].Pregunta);
                row["Etapa"] = Convert.ToInt32(ListaPreguntas[i].Etapa);

                dtPreguntas.Rows.Add(row);


                for (int j = 0; j < ListaPreguntas[i].Respuesta.Count(); j++)
                {
                    DataRow row2 = dtRespuestas.NewRow();

                    row2["id_respuesta"] = Convert.ToInt32(ListaPreguntas[i].Respuesta[j].IdRespuesta);
                    row2["id_pregunta"] = Convert.ToInt32(ListaPreguntas[i].Respuesta[j].IdPregunta);
                    row2["Descripcion"] = Convert.ToString(ListaPreguntas[i].Respuesta[j].Respuesta);
                    row2["id_factor"] = Convert.ToInt32(ListaPreguntas[i].Respuesta[j].Factor);

                    dtRespuestas.Rows.Add(row2);
                }


            }

            txtPreguntas.Text = "1 " + ") " + dtPreguntas.Rows[0][1].ToString();

            rtbA.Name = dtRespuestas.Rows[0]["id_respuesta"].ToString();
            rtbB.Name = dtRespuestas.Rows[1]["id_respuesta"].ToString();
            rtbC.Name = dtRespuestas.Rows[2]["id_respuesta"].ToString();
            rtbD.Name = dtRespuestas.Rows[3]["id_respuesta"].ToString();

            rtbA.Text = "A) " + dtRespuestas.Rows[0]["Descripcion"].ToString();
            rtbB.Text = "B) " + dtRespuestas.Rows[1]["Descripcion"].ToString();
            rtbC.Text = "C) " + dtRespuestas.Rows[2]["Descripcion"].ToString();
            rtbD.Text = "D) " + dtRespuestas.Rows[3]["Descripcion"].ToString();
            Etapa = Convert.ToInt32(dtPreguntas.Rows[0]["Etapa"]);

        }


        ALLPORT model_Allport;

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                model_Allport = Cliente.NuevoAllPort(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message,
                    "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            this.prueba_iniciada = true;
            Numero_prueba = model_Allport.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 60";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.txtA.Focus();
            this.tiempo.Enabled = true;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void FinalizarPrueba()
        {
            //Verificamos si hay una respuesta por partte del candidato antes de que acabara el tiempo
            if (Etapa == 1)
            {
                if (txtA.Text.Trim().Length > 0 && txtB.Text.Trim().Length > 0 && btnSiguiente.Name == "Siguiente")
                {
                    guardarRespuestas(x);
                }
            }
            else
            {
                if (txtA.Text.Trim().Length > 0 && txtB.Text.Trim().Length > 0 && txtC.Text.Trim().Length > 0 && txtD.Text.Trim().Length > 0 && btnSiguiente.Name == "Siguiente")
                {
                    guardarRespuestas(x);
                }
            }


            RespuestasCandidato.NumeroPrueba = Numero_prueba;
            Cliente.FinalizaAllportPrueba(RespuestasCandidato);
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            //Limpiamos las respuestas anteriores.
            dtResCandidato.Clear();

            //Listamos las respuestas del candidato.
            dto_allport_respuestas_candidato[] ListaRespuestas = Cliente.ListaPreguntasContestadasAllport(Numero_prueba);


            for (int i = 0; i < ListaRespuestas.ToList().Count; i++)
            {
                DataRow row = dtResCandidato.NewRow();

                row["id_respuesta_candidato"] = Convert.ToInt32(ListaRespuestas[i].IdRespuestaTabla);
                row["id_respuesta"] = Convert.ToString(ListaRespuestas[i].IdRespuesta);
                row["puntaje"] = Convert.ToString(ListaRespuestas[i].Puntaje);

                dtResCandidato.Rows.Add(row);
            }
            Recorrido = dtResCandidato.Rows.Count;



            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtPreguntas.Rows.Count;
            x--;

            mostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";
                desctivarCajas(true);
            }

            btnSiguiente.Text = "Actualizar";
            btnSiguiente.Enabled = true;


        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                guardarRespuestas(x);
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    desctivarCajas(false);
                    return;
                }
                mostrarPreguntas(x);
          
                LimpiarCampos();
                txtA.Focus();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Text == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Etapa == 1)
                {
                    if ((Recorrido / 2) <= x)
                    {
                        btnSiguiente.Text = "Siguiente";
                        LimpiarCampos();
                        btnSiguiente.Enabled = false;
                    }
                }
                else if (Etapa == 2)
                {
                    if (((Recorrido - 60) / 4) + 30 <= x)
                    {
                        btnSiguiente.Text = "Siguiente";
                        LimpiarCampos();
                        btnSiguiente.Enabled = false;
                    }
                }

                mostrarPreguntas(x);


            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    guardarRespuestas(x);
                    FinalizarPrueba();
                    this.Close();

                }

            }


            if (x == dtPreguntas.Rows.Count - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
            }


            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;



            btnAnterior.Enabled = true;
            //btnSiguiente.Enabled = false;

        }


        ArrayList ArrayIdTabla = new ArrayList();
        private void ActualizarRespuestas()
        {

            //Limpiamor los valores anteriores
            ArrayRespuestas.Clear();


            //Cargamos las respuestas del candidato dependiendo de la etapa.   
            ArrayRespuestas.Add(Convert.ToInt32(txtA.Text));
            ArrayRespuestas.Add(Convert.ToInt32(txtB.Text));


            if (Etapa == 2)
            {
                ArrayRespuestas.Add(Convert.ToInt32(txtC.Text));
                ArrayRespuestas.Add(Convert.ToInt32(txtD.Text));

            }

            RespuestasCandidato.NumeroPrueba = Numero_prueba;

            for (int i = 0; i < ArrayRespuestas.Count; i++)
            {
                RespuestasCandidato.IdRespuestaTabla = Convert.ToInt32(ArrayIdTabla[i]);
                RespuestasCandidato.IdRespuesta = Convert.ToInt32(ArrayIdPregunta[i]);
                RespuestasCandidato.Puntaje = Convert.ToInt32(ArrayRespuestas[i]);
                Cliente.ActualizaAllportRespuesta(RespuestasCandidato);
            }

            //Borramos de memoria los registros modificados para que queden libres las posiciones del ArrayList
            ArrayIdTabla.Clear();
            ArrayIdPregunta.Clear();

        }

        ArrayList ArrayRespuestas = new ArrayList();
        ArrayList ArrayIdPregunta = new ArrayList();

        private void guardarRespuestas(int parmPregunta)
        {
            //Limpiamor los valores anteriores
            ArrayRespuestas.Clear();
            ArrayIdPregunta.Clear();

            //Cargamos las respuestas del candidato dependiendo de la etapa.     

            ArrayRespuestas.Add(Convert.ToInt32(txtA.Text));
            ArrayRespuestas.Add(Convert.ToInt32(txtB.Text));

            ArrayIdPregunta.Add(Convert.ToInt32(rtbA.Name));
            ArrayIdPregunta.Add(Convert.ToInt32(rtbB.Name));

            if (Etapa == 2)
            {
                ArrayRespuestas.Add(Convert.ToInt32(txtC.Text));
                ArrayRespuestas.Add(Convert.ToInt32(txtD.Text));

                ArrayIdPregunta.Add(Convert.ToInt32(rtbC.Name));
                ArrayIdPregunta.Add(Convert.ToInt32(rtbD.Name));
            }


            RespuestasCandidato.NumeroPrueba = Numero_prueba;

            for (int i = 0; i < ArrayRespuestas.Count; i++)
            {
                RespuestasCandidato.IdRespuesta = Convert.ToInt32(ArrayIdPregunta[i]);
                RespuestasCandidato.Puntaje = Convert.ToInt32(ArrayRespuestas[i]);
                Cliente.InsertaAllportRespuesta(RespuestasCandidato);
            }

        }

        private void mostrarPreguntas(int i)
        {
            ArrayIdTabla.Clear();
            ArrayIdPregunta.Clear();

            Etapa = Convert.ToInt32(dtPreguntas.Rows[i]["Etapa"]);
            ValidarEtapas(Etapa);

            this.txtPreguntas.Text = i + 1 + " ) " + dtPreguntas.Rows[i][1].ToString();

            //se buscan las respuestas correspondientes a la pregunta en el datatable que ya se tiene en memoria
            row = dtRespuestas.Select("id_pregunta = '" + dtPreguntas.Rows[i][0].ToString() + "'");

            if (row.Length > 0)
            {
                //this.lblIdPregunta.Text = row[0]["id_pregunta"].ToString();

                rtbA.Name = row[0]["id_respuesta"].ToString();
                rtbB.Name = row[1]["id_respuesta"].ToString();
                ArrayIdPregunta.Add(Convert.ToInt32(rtbA.Name));
                ArrayIdPregunta.Add(Convert.ToInt32(rtbB.Name));

                rtbA.Text = "A) " + row[0]["Descripcion"].ToString();
                rtbB.Text = "B) " + row[1]["Descripcion"].ToString();

                if (Etapa == 2)
                {
                    rtbC.Text = "C) " + row[2]["Descripcion"].ToString();
                    rtbD.Text = "D) " + row[3]["Descripcion"].ToString();

                    rtbC.Name = row[2]["id_respuesta"].ToString();
                    rtbD.Name = row[3]["id_respuesta"].ToString();

                    ArrayIdPregunta.Add(Convert.ToInt32(rtbC.Name));
                    ArrayIdPregunta.Add(Convert.ToInt32(rtbD.Name));
                }

            }


            //Retornamos las respuestas del candidato por si desea corregir
            if (Etapa == 1)
            {

                if (Recorrido / 2 > i)
                {

                    if (i == 0)
                    {
                        //Mostramos las primeras respuestas
                        txtA.Text = dtResCandidato.Rows[0]["puntaje"].ToString();
                        txtB.Text = dtResCandidato.Rows[1]["puntaje"].ToString();

                        //Igualamos las llaves primarias para lograr la modificacion
                        ArrayIdTabla.Add(dtResCandidato.Rows[0]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[1]["id_respuesta_candidato"]);

                    }
                    else
                    {
                        //Mostramos el resto de respuestas del candidato.
                        txtA.Text = dtResCandidato.Rows[(i * 2)]["puntaje"].ToString();
                        txtB.Text = dtResCandidato.Rows[(i * 2) + 1]["puntaje"].ToString();

                        //Igualamos las llaves primarias para lograr la modificacion
                        ArrayIdTabla.Add(dtResCandidato.Rows[i * 2]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[(i * 2) + 1]["id_respuesta_candidato"]);

                    }

                }

            }
            else if (Etapa == 2)
            {
                if ((((Recorrido - 60)) / 4) + 30 > i)
                {
                    if (i == 30)
                    {
                        txtA.Text = dtResCandidato.Rows[60]["puntaje"].ToString();
                        txtB.Text = dtResCandidato.Rows[61]["puntaje"].ToString();
                        txtC.Text = dtResCandidato.Rows[62]["puntaje"].ToString();
                        txtD.Text = dtResCandidato.Rows[63]["puntaje"].ToString();

                        //Igualamos las llaves primarias para lograr la modificacion
                        ArrayIdTabla.Add(dtResCandidato.Rows[60]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[61]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[62]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[63]["id_respuesta_candidato"]);

                    }
                    else
                    {
                        txtA.Text = dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 4)]["puntaje"].ToString();
                        txtB.Text = dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 3)]["puntaje"].ToString();
                        txtC.Text = dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 2)]["puntaje"].ToString();
                        txtD.Text = dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 1)]["puntaje"].ToString();

                        //Igualamos las llaves primarias para lograr la modificacion
                        ArrayIdTabla.Add(dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 4)]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 3)]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 2)]["id_respuesta_candidato"]);
                        ArrayIdTabla.Add(dtResCandidato.Rows[(((Recorrido / 4) * 3) + (Recorrido / 4) - 1)]["id_respuesta_candidato"]);

                    }

                }

            }


        }

        private void LimpiarCampos()
        {
            txtA.Clear();
            txtB.Clear();
            txtC.Clear();
            txtD.Clear();

        }

        private void desctivarCajas(bool valor)
        {
            this.txtA.Enabled = valor;
            this.txtB.Enabled = valor;
            this.txtC.Enabled = valor;
            this.txtD.Enabled = valor;
        }

        private void validarCamposLlenos()
        {
            int[] datos;
            int i = 0;

            if (Etapa == 1)
            {

                if (txtA.Text.Trim().Length > 0 && txtB.Text.Trim().Length > 0)
                {
                    btnSiguiente.Enabled = true;
                }
                else
                {
                    btnSiguiente.Enabled = false;
                }

            }
            if (Convert.ToInt32(Etapa) == 2)
            {

                if (this.txtA.Text.Length == 0 || this.txtB.Text.Length == 0 || this.txtC.Text.Length == 0 || this.txtD.Text.Length == 0)
                {
                    this.btnSiguiente.Enabled = false;
                    return;
                }
                else
                {
                    this.btnSiguiente.Enabled = true;
                    this.btnSiguiente.Focus();
                }


                // validar campos con el mismo valor

                datos = new int[4];
                foreach (Control text in this.gbPreguntas.Controls)
                {
                    if (text is TextBox)
                    {
                        datos[i] = Convert.ToInt32(text.Text);
                        i++;
                    }
                }
                Array.Sort(datos);
                for (i = 1; i < 4; i++)
                {
                    if (datos[i] == datos[i - 1])
                    {
                        this.lblMsj.Text = "El número: " + datos[i] + " está repetido. \nRecuerde que no se pueden repetir los números y solo puede usar del 1 al 4.";
                        this.btnSiguiente.Enabled = false;
                        return;
                    }
                    else
                    {
                        this.lblMsj.Text = "";
                    }
                }
                // fin de validación de campos con el mismo valor.  

            }


        }

        private void ValidarEtapas(int parmEtapa)
        {

            if (parmEtapa == 1)
            {
                rtbC.Visible = false;
                rtbD.Visible = false;

                txtC.Visible = false;
                txtD.Visible = false;

                lblEtapa.Text = "Etapa: 1";

            }
            else if (parmEtapa == 2)
            {
                rtbC.Visible = true;
                rtbD.Visible = true;

                txtC.Visible = true;
                txtD.Visible = true;

                lblEtapa.Text = "Etapa: 2";
            }

        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.ALLPORT, Numero_prueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        #region Validar Solo numeros en los TextBox Mediante el evento KeyPress

        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }


        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtD_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }


        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }

            if (Etapa == 1)
            {
                // bloquear las teclas 0 y del 4 al 9
                if ((ex.KeyChar == (char)Keys.D4) || (ex.KeyChar == (char)Keys.D5) || (ex.KeyChar == (char)Keys.D6) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
                {
                    ex.Handled = true;
                    return;
                }
            }
            else if (Etapa == 2)
            {
                // bloquear las teclas 0 y del 5 al 9
                if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D5) || (ex.KeyChar == (char)Keys.D6) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
                {
                    ex.Handled = true;
                    return;
                }
            }


        }

        #endregion

        #region "Verificar que todos los campos estén llenos con el evento KeyUp"


        private void txtA_KeyUp(object sender, KeyEventArgs e)
        {

            if (Etapa == 1)
            {

                if (txtA.Text.Trim().Length > 0)
                {

                    if (Convert.ToInt32(txtA.Text) >= 0)
                    {
                        txtB.Text = Convert.ToString(3 - Convert.ToInt32(txtA.Text));

                    }
                    else
                    {
                        txtA.Text = Convert.ToString(3 - Convert.ToInt32(txtB.Text));
                        return;
                    }
                }

            }

            txtB.Focus();
            validarCamposLlenos();

        }

        private void txtB_KeyUp(object sender, KeyEventArgs e)
        {
            if (Etapa == 1)
            {
                if (txtB.TextLength > 0)
                {
                    if (Convert.ToInt32(txtB.Text) >= 0)
                    {
                        txtA.Text = Convert.ToString(3 - Convert.ToInt32(txtB.Text));
                    }
                    else
                    {
                        txtB.Text = Convert.ToString(3 - Convert.ToInt32(txtA.Text));
                    }
                }
            }


            if (Etapa == 2)
            {
                txtC.Focus();
            }

            validarCamposLlenos();

        }

        private void txtC_KeyUp(object sender, KeyEventArgs e)
        {

            validarCamposLlenos();

            if (Etapa == 2)
            {
                txtD.Focus();
            }
        }

        private void txtD_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            if (Etapa == 2)
            {
                btnSiguiente.Focus();
            }
        }

        #endregion

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void frmAllport_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
