﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie6 : Form
    {
        QuickHumClient cliente = Globales.cliente;
        dto_terman_respuestas_candidato respcandidato = new dto_terman_respuestas_candidato();
        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtactualizar = new DataTable();
        private int number_prueba = frmTerman.numero_pruebaTerman;
        int x = 0, punto = 0, punto1 = 0, punto2 = 0, punto3 = 0, punto4 = 0, recorrido = 0;
        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        string Respuesta, Respuesta2, Respuesta3, Respuesta4, Respuesta5;

        public frmTermanSerie6()
        {
            InitializeComponent();
        }

        private void frmTermanSerie6_Load(object sender, EventArgs e)
        {
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_pregunta");
            dtactualizar.Columns.Add("respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("serie");
            dtactualizar.Columns.Add("numero_prueba");


            //this.Size = new Size(510, 442);
            this.panelAyuda.Location = new Point(1, 1);
            ListarDatos();
        }

        private void ListarDatos()
        {
            dto_terman_preguntas[] ListadoPreguntas = cliente.ListadoTermanPreguntas();

            dtpreguntas.Columns.Add("id_pregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("respuesta_correcta");
            dtpreguntas.Columns.Add("serie");

            dtrespuestas.Columns.Add("idpregunta");
            dtrespuestas.Columns.Add("id_respuesta");
            dtrespuestas.Columns.Add("respuesta");

            for (int i = 0; i < ListadoPreguntas.Length; i++)
            {
                if (ListadoPreguntas[i].Serie == 6)
                {
                    DataRow row = dtpreguntas.NewRow();

                    row["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].Idpregunta);
                    row["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);
                    row["respuesta_correcta"] = Convert.ToString(ListadoPreguntas[i].Respuesta_Correcta);
                    row["serie"] = Convert.ToInt32(ListadoPreguntas[i].Serie);

                    dtpreguntas.Rows.Add(row);
                }

            }

            lbl1.Text = ((x * 4) + 1) + "- " + dtpreguntas.Rows[0]["pregunta"].ToString();
            lbl2.Text = ((x * 4) + 2) + "- " + dtpreguntas.Rows[1]["pregunta"].ToString();
            lbl3.Text = ((x * 4) + 3) + "- " + dtpreguntas.Rows[2]["pregunta"].ToString();
            lbl4.Text = ((x * 4) + 4) + "- " + dtpreguntas.Rows[3]["pregunta"].ToString();


            lbl1.Name = dtpreguntas.Rows[0]["id_pregunta"].ToString();
            lbl2.Name = dtpreguntas.Rows[1]["id_pregunta"].ToString();
            lbl3.Name = dtpreguntas.Rows[2]["id_pregunta"].ToString();
            lbl4.Name = dtpreguntas.Rows[3]["id_pregunta"].ToString();




            DateTime hora;
            hora = Convert.ToDateTime("0:02:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void MostrarPreguntas(int i)
        {

            lbl1.Text = ((i * 4) + 1) + "- " + dtpreguntas.Rows[(i * 4)]["pregunta"].ToString();
            lbl2.Text = ((i * 4) + 2) + "- " + dtpreguntas.Rows[(i * 4) + 1]["pregunta"].ToString();
            lbl3.Text = ((i * 4) + 3) + "- " + dtpreguntas.Rows[(i * 4) + 2]["pregunta"].ToString();
            lbl4.Text = ((i * 4) + 4) + "- " + dtpreguntas.Rows[(i * 4) + 3]["pregunta"].ToString();


            lbl1.Name = dtpreguntas.Rows[(i * 4)]["id_pregunta"].ToString();
            lbl2.Name = dtpreguntas.Rows[(i * 4) + 1]["id_pregunta"].ToString();
            lbl3.Name = dtpreguntas.Rows[(i * 4) + 2]["id_pregunta"].ToString();
            lbl4.Name = dtpreguntas.Rows[(i * 4) + 3]["id_pregunta"].ToString();
        }

        private void Evaluar()
        {
            if (rbtA.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 4]["respuesta_correcta"].ToString().Trim() == rbtA.Text.Trim())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                Respuesta = rbtA.Text;
            }

            else if (rbtB.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 4]["respuesta_correcta"].ToString().Trim() == rbtB.Text.Trim())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                Respuesta = rbtB.Text;
            }

            if (rbtC.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 3]["respuesta_correcta"].ToString().Trim() == rbtC.Text.Trim())
                {
                    punto1 = 1;
                }
                else
                {
                    punto1 = 0;
                }
                Respuesta2 = rbtC.Text;
            }

            else if (rbtD.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 3]["respuesta_correcta"].ToString().Trim() == rbtD.Text.Trim())
                {
                    punto1 = 1;
                }
                else
                {
                    punto1 = 0;
                }
                Respuesta2 = rbtD.Text;
            }
            if (rbtE.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 2]["respuesta_correcta"].ToString().Trim() == rbtE.Text.Trim())
                {
                    punto2 = 1;
                }
                else
                {
                    punto2 = 0;
                }
                Respuesta3 = rbtE.Text;
            }
            else if (rbtF.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 2]["respuesta_correcta"].ToString().Trim() == rbtF.Text.Trim())
                {
                    punto2 = 1;
                }
                else
                {
                    punto2 = 0;
                }
                Respuesta3 = rbtF.Text;
            }
            if (rbtG.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 1]["respuesta_correcta"].ToString().Trim() == rbtG.Text.Trim())
                {
                    punto3 = 1;
                }
                else
                {
                    punto3 = 0;
                }
                Respuesta4 = rbtG.Text;
            }
            else if (rbtH.Checked == true)
            {
                if (dtpreguntas.Rows[(x * 4) - 1]["respuesta_correcta"].ToString().Trim() == rbtH.Text.Trim())
                {
                    punto3 = 1;
                }
                else
                {
                    punto3 = 0;
                }
                Respuesta4 = rbtH.Text;
            }


        }

        private void Retroceder()
        {
            if (dtactualizar.Rows[(x * 4)]["respuesta"].ToString().Trim() == rbtA.Text.Trim())
            {
                rbtA.Checked = true;
            }
            else if (dtactualizar.Rows[(x * 4)]["respuesta"].ToString().Trim() == rbtB.Text.Trim())
            {
                rbtB.Checked = true;
            }
            if (dtactualizar.Rows[(x * 4) + 1]["respuesta"].ToString().Trim() == rbtC.Text.Trim())
            {
                rbtC.Checked = true;
            }
            else if (dtactualizar.Rows[(x * 4) + 1]["respuesta"].ToString().Trim() == rbtD.Text.Trim())
            {
                rbtD.Checked = true;
            }
            if (dtactualizar.Rows[(x * 4) + 2]["respuesta"].ToString().Trim() == rbtE.Text.Trim())
            {
                rbtE.Checked = true;
            }
            else if (dtactualizar.Rows[(x * 4) + 2]["respuesta"].ToString().Trim() == rbtF.Text.Trim())
            {
                rbtF.Checked = true;
            }
            if (dtactualizar.Rows[(x * 4) + 3]["respuesta"].ToString().Trim() == rbtG.Text.Trim())
            {
                rbtG.Checked = true;
            }
            else if (dtactualizar.Rows[(x * 4) + 3]["respuesta"].ToString().Trim() == rbtH.Text.Trim())
            {
                rbtH.Checked = true;
            }
        }

        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Respuesta = Respuesta;
            respcandidato.Punto = punto;
            respcandidato.Serie = 6;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);

            respcandidato.Idpregunta = Convert.ToInt32(lbl2.Name);
            respcandidato.Respuesta = Respuesta2;
            respcandidato.Punto = punto1;
            respcandidato.Serie = 6;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);

            respcandidato.Idpregunta = Convert.ToInt32(lbl3.Name);
            respcandidato.Respuesta = Respuesta3;
            respcandidato.Punto = punto2;
            respcandidato.Serie = 6;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);

            respcandidato.Idpregunta = Convert.ToInt32(lbl4.Name);
            respcandidato.Respuesta = Respuesta4;
            respcandidato.Punto = punto3;
            respcandidato.Serie = 6;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);
        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 4) - 4]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Respuesta = Respuesta;
            respcandidato.Punto = punto;
            cliente.ActualizaTermanRespuesta(respcandidato);

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 4) - 3]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl2.Name);
            respcandidato.Respuesta = Respuesta2;
            respcandidato.Punto = punto1;
            cliente.ActualizaTermanRespuesta(respcandidato);

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 4) - 2]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl3.Name);
            respcandidato.Respuesta = Respuesta3;
            respcandidato.Punto = punto2;
            cliente.ActualizaTermanRespuesta(respcandidato);

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 4) - 1]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl4.Name);
            respcandidato.Respuesta = Respuesta4;
            respcandidato.Punto = punto3;
            cliente.ActualizaTermanRespuesta(respcandidato);
        }

        private void LimpiarRadios()
        {
            rbtA.Checked = false;
            rbtB.Checked = false;
            rbtC.Checked = false;
            rbtD.Checked = false;
            rbtE.Checked = false;
            rbtF.Checked = false;
            rbtG.Checked = false;
            rbtH.Checked = false;

        }

        private void ValidarRadios()
        {
            if ((rbtA.Checked == true || rbtB.Checked == true) && (rbtC.Checked == true || rbtD.Checked == true) && (rbtE.Checked == true || rbtF.Checked == true) && (rbtG.Checked == true || rbtH.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        #region Validacion de Radios
        private void rbtA_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtB_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtC_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtD_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtE_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtF_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtG_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }

        private void rbtH_Click(object sender, EventArgs e)
        {
            ValidarRadios();
        }
        #endregion

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if ((rbtA.Checked == true || rbtB.Checked == true) && (rbtC.Checked == true || rbtD.Checked == true) && (rbtE.Checked == true || rbtF.Checked == true) && (rbtG.Checked == true || rbtH.Checked == true))
                {
                    x++; GuardarRespuestas();
                }
                else
                {

                }
                this.Dispose();
            }

        }
        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrar_ayuda.Show();
            this.btnIniciarAyuda.Hide();
            btnCerrar_ayuda.Visible = true;
            //this.Size = new Size(510, 442);
        }

        private void btnCerrar_ayuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
            //this.Size = new Size(630, 442);
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            //this.Size = new Size(630, 442);
            LimpiarRadios();
            this.tiempo.Enabled = true;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                MostrarPreguntas(x);
                btnSiguiente.Enabled = false;
                LimpiarRadios();
            }
            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                MostrarPreguntas(x);
                if (recorrido > (x * 4))
                {
                    Retroceder();
                }
                if (recorrido <= (x * 4))
                {
                    btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    LimpiarRadios();
                }
            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }
            }

            if ((x * 4) == dtpreguntas.Rows.Count - 4)
            {
                btnSiguiente.Text = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }
            btnAnterior.Enabled = true;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_terman_respuestas_candidato[] listarrespuesta = cliente.ListaTermanRespuestasCandidatos(number_prueba);

            dtactualizar.Clear();
            for (int w = 0; w < listarrespuesta.Length; w++)
            {
                if (listarrespuesta[w].Serie == 6)
                {
                    DataRow row = dtactualizar.NewRow();
                    row["id_respuesta_candidato"] = Convert.ToInt32(listarrespuesta[w].IdRespuestaCandidato);
                    row["id_pregunta"] = Convert.ToInt32(listarrespuesta[w].Idpregunta);
                    row["respuesta"] = Convert.ToString(listarrespuesta[w].Respuesta);
                    row["punto"] = Convert.ToInt32(listarrespuesta[w].Punto);
                    row["serie"] = Convert.ToInt32(listarrespuesta[w].Serie);
                    row["numero_prueba"] = Convert.ToInt32(listarrespuesta[w].Numero_prueba);
                    dtactualizar.Rows.Add(row);
                }
            }
            recorrido = dtactualizar.Rows.Count;

            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }

            x--;
            btnSiguiente.Text = "Siguiente.";

            if (recorrido > x)
            {
                Retroceder();
            }

            MostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";

            }
            btnSiguiente.Enabled = true;
        }

        private void tiempo_Tick_1(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = (contminutos - 1);
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void frmTermanSerie6_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void btnCerrar_ayuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
            //this.Size = new Size(630, 442);
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
