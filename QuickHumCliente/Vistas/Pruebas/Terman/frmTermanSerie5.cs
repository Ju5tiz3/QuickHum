﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie5 : Form
    {
        QuickHumClient cliente = Globales.cliente;
        dto_terman_respuestas_candidato respcandidato = new dto_terman_respuestas_candidato();

        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtactualizar = new DataTable();
        DataRow[] row;
        private int number_prueba = frmTerman.numero_pruebaTerman;
        int x = 0, punto = 0, recorrido = 0;
        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        string Respuesta;

        public frmTermanSerie5()
        {
            InitializeComponent();
        }

        private void frmTermanSerie5_Load(object sender, EventArgs e)
        {

            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_pregunta");
            dtactualizar.Columns.Add("respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("serie");
            dtactualizar.Columns.Add("numero_prueba");
            //this.Size = new Size(605, 426);
            this.panelAyuda.Location = new Point(1, 1);
            listarDatos();
            CambiarTextos();
        }

        private void listarDatos()
        {
            dto_terman_preguntas[] ListadoPreguntas = cliente.ListadoTermanPreguntas();

            dtpreguntas.Columns.Add("id_pregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("respuesta_correcta");
            dtpreguntas.Columns.Add("serie");

            dtrespuestas.Columns.Add("idpregunta");
            dtrespuestas.Columns.Add("id_respuesta");
            dtrespuestas.Columns.Add("respuesta");

            for (int i = 0; i < ListadoPreguntas.Length; i++)
            {
                if (ListadoPreguntas[i].Serie == 5)
                {
                    DataRow row = dtpreguntas.NewRow();

                    row["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].Idpregunta);
                    row["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);
                    row["respuesta_correcta"] = Convert.ToString(ListadoPreguntas[i].Respuesta_Correcta);
                    row["serie"] = Convert.ToInt32(ListadoPreguntas[i].Serie);

                    dtpreguntas.Rows.Add(row);
                }

            }

            lbl1.Text = "1) " + dtpreguntas.Rows[0]["pregunta"].ToString() + ":";
            lbl1.Name = dtpreguntas.Rows[0]["id_pregunta"].ToString();


            DateTime hora;
            hora = Convert.ToDateTime("0:05:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void MostrarPreguntas(int i)
        {
            lbl1.Text = i + 1 + ") " + dtpreguntas.Rows[i]["pregunta"].ToString() + ":";
            lbl1.Name = dtpreguntas.Rows[i]["id_pregunta"].ToString();

        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            txtrespuesta.Focus();
            tiempo.Enabled = true;
        }

        private void CambiarTextos()
        {
            if (x == 0)
            { lbltexto.Text = "Dolares."; }

            else if (x == 1)
            { lbltexto.Text = "Kilometros."; }

            else if (x == 2)
            { lbltexto.Text = "Dias."; }

            else if (x == 3)
            { lbltexto.Text = "Dolares."; }

            else if (x == 4)
            { lbltexto.Text = "Veces."; }

            else if (x == 5)
            { lbltexto.Text = "Dolares."; }

            else if (x == 6)
            { lbltexto.Text = "Dolares."; }

            else if (x == 7)
            { lbltexto.Text = "Metros."; }

            else if (x == 8)
            { lbltexto.Text = "Hombres."; }

            else if (x == 9)
            { lbltexto.Text = "Dolares."; }

            else if (x == 10)
            { lbltexto.Text = "Metros."; }

            else if (x == 11)
            { lbltexto.Text = "%."; }
        }

        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }
        }

        private void txtrespuesta_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void Evaluar()
        {
            if (dtpreguntas.Rows[x - 1]["respuesta_correcta"].ToString().Trim() == txtrespuesta.Text.Trim())
            {
                punto = 1;
            }
            else
            {
                punto = 0;
            }
            Respuesta = txtrespuesta.Text;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                MostrarPreguntas(x);
                btnSiguiente.Enabled = false;
                CambiarTextos();
                txtrespuesta.Clear();
                txtrespuesta.Focus();
            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                MostrarPreguntas(x);
                CambiarTextos();
                if (recorrido > x)
                { Retroceder(); }
                if (recorrido <= x)
                {
                    btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    txtrespuesta.Clear();
                    txtrespuesta.Focus();
                }
            }


            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }
            }
            if (x == dtpreguntas.Rows.Count - 1)
            {
                btnSiguiente.Text = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }
            btnAnterior.Enabled = true;
        }

        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Respuesta = txtrespuesta.Text;
            respcandidato.Punto = punto;
            respcandidato.Numero_prueba = number_prueba;
            respcandidato.Serie = 5;
            cliente.InsertaTermanRespuesta(respcandidato);

        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Respuesta = txtrespuesta.Text;
            respcandidato.Punto = punto;
            cliente.ActualizaTermanRespuesta(respcandidato);
        }

        private void Retroceder()
        {
            txtrespuesta.Text = dtactualizar.Rows[x]["respuesta"].ToString();
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_terman_respuestas_candidato[] listarrespuesta = cliente.ListaTermanRespuestasCandidatos(number_prueba);

            dtactualizar.Clear();
            for (int w = 0; w < listarrespuesta.Length; w++)
            {
                if (listarrespuesta[w].Serie == 5)
                {
                    DataRow row = dtactualizar.NewRow();
                    row["id_respuesta_candidato"] = Convert.ToInt32(listarrespuesta[w].IdRespuestaCandidato);
                    row["id_pregunta"] = Convert.ToInt32(listarrespuesta[w].Idpregunta);
                    row["respuesta"] = Convert.ToString(listarrespuesta[w].Respuesta);
                    row["punto"] = Convert.ToInt32(listarrespuesta[w].Punto);
                    row["serie"] = Convert.ToInt32(listarrespuesta[w].Serie);
                    row["numero_prueba"] = Convert.ToInt32(listarrespuesta[w].Numero_prueba);
                    dtactualizar.Rows.Add(row);
                }
            }
            recorrido = dtactualizar.Rows.Count;

            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }

            x--;
            btnSiguiente.Text = "Siguiente.";
            CambiarTextos();
            if (recorrido > x)
            {
                Retroceder();
            }

            MostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";

            }
            btnSiguiente.Enabled = true;
            txtrespuesta.Focus();
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {

            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;

        }
        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if (txtrespuesta.Text.Trim().Length == 0)
                {

                }
                else
                {
                    x++; GuardarRespuestas();
                }
            }

        }

        private void txtrespuesta_TextChanged(object sender, EventArgs e)
        {
            if (txtrespuesta.Text.Trim().Length == 0)
            {
                btnSiguiente.Enabled = false;
            }
            else
            {
                btnSiguiente.Enabled = true;

            }
        }

        private void frmTermanSerie5_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
