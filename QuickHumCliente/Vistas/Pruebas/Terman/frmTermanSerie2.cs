﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie2 : Form
    {
        QuickHumClient cliente = Globales.cliente;
        dto_terman_respuestas_candidato respcandidato = new dto_terman_respuestas_candidato();
        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtactualizar = new DataTable();
        DataRow[] row, preguntas;
        private int number_prueba = frmTerman.numero_pruebaTerman;
        int x = 0, punto = 0, recorrido = 0;
        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        string Respuesta;

        public frmTermanSerie2()
        {
            InitializeComponent();
        }


        private void frmTermanSerie2_Load(object sender, EventArgs e)
        {
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_pregunta");
            dtactualizar.Columns.Add("respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("serie");
            dtactualizar.Columns.Add("numero_prueba");

            this.Size = new Size(458, 396);
            this.panelAyuda.Location = new Point(1, 1);
            ListarDatos();
        }

        private void ListarDatos()
        {
            dto_terman_preguntas[] ListadoPreguntas = cliente.ListadoTermanPreguntas();
            dtpreguntas.Columns.Add("id_pregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("respuesta_correcta");
            dtpreguntas.Columns.Add("serie");

            dtrespuestas.Columns.Add("idpregunta");
            dtrespuestas.Columns.Add("id_respuesta");
            dtrespuestas.Columns.Add("respuesta");


            for (int i = 0; i < ListadoPreguntas.Length; i++)
            {
                if (ListadoPreguntas[i].Serie == 2)
                {
                    DataRow row = dtpreguntas.NewRow();

                    row["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].Idpregunta);
                    row["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);
                    row["respuesta_correcta"] = Convert.ToString(ListadoPreguntas[i].Respuesta_Correcta);
                    row["serie"] = Convert.ToInt32(ListadoPreguntas[i].Serie);

                    for (int j = 0; j < 3; j++)
                    {
                        DataRow fila = dtrespuestas.NewRow();
                        fila["id_respuesta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].Idrespuesta);
                        fila["idpregunta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].Idpregunta);
                        fila["respuesta"] = Convert.ToString(ListadoPreguntas[i].Respuesta[j].Respuesta);

                        dtrespuestas.Rows.Add(fila);
                    }
                    dtpreguntas.Rows.Add(row);
                }

            }

            txtPreguntas.Text = "1) " + dtpreguntas.Rows[0]["pregunta"].ToString();
            txtPreguntas.Name = dtpreguntas.Rows[0]["id_pregunta"].ToString();
            rbtA.Text = dtrespuestas.Rows[0]["respuesta"].ToString();
            rbtB.Text = dtrespuestas.Rows[1]["respuesta"].ToString();
            rbtC.Text = dtrespuestas.Rows[2]["respuesta"].ToString();


            DateTime hora;
            hora = Convert.ToDateTime("0:02:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void MostrarPreguntas(int i)
        {
            txtPreguntas.Text = i + 1 + ") " + dtpreguntas.Rows[i]["pregunta"].ToString();
            row = dtrespuestas.Select("idpregunta ='" + dtpreguntas.Rows[i]["id_pregunta"].ToString() + "'");

            if (row.Length > 0)
            {

                txtPreguntas.Name = row[0]["idpregunta"].ToString();
                rbtA.Text = row[0]["respuesta"].ToString();
                rbtB.Text = row[1]["respuesta"].ToString();
                rbtC.Text = row[2]["respuesta"].ToString();


            }

        }

        private void Evaluar()
        {
            if (rbtA.Checked == true)
            {
                if (dtpreguntas.Rows[x - 1]["respuesta_correcta"].ToString().Trim() == rbtA.Text.Trim())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                Respuesta = rbtA.Text;
            }

            else if (rbtB.Checked == true)
            {
                if (dtpreguntas.Rows[x - 1]["respuesta_correcta"].ToString().Trim() == rbtB.Text.Trim())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                Respuesta = rbtB.Text;
            }

            else if (rbtC.Checked == true)
            {
                if (dtpreguntas.Rows[x - 1]["respuesta_correcta"].ToString().Trim() == rbtC.Text.Trim())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                Respuesta = rbtC.Text;
            }


        }

        private void Retroceder()
        {
            if (dtrespuestas.Rows[(x * 3)]["respuesta"].ToString() == dtactualizar.Rows[x]["respuesta"].ToString())
            {
                rbtA.Checked = true;
            }

            else if (dtrespuestas.Rows[(x * 3) + 1]["respuesta"].ToString() == dtactualizar.Rows[x]["respuesta"].ToString())
            {
                rbtB.Checked = true;
            }

            else if (dtrespuestas.Rows[(x * 3) + 2]["respuesta"].ToString() == dtactualizar.Rows[x]["respuesta"].ToString())
            {
                rbtC.Checked = true;
            }


        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                MostrarPreguntas(x);
                btnSiguiente.Enabled = false;
                LimpiarRadios();
            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                MostrarPreguntas(x);
                if (recorrido > x)
                {
                    Retroceder();
                }
                if (recorrido <= x)
                {
                    btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    LimpiarRadios();
                }

            }

            else if (this.btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {

                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }
            }
            if (x == dtpreguntas.Rows.Count - 1)
            {
                btnSiguiente.Text = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }
            btnAnterior.Enabled = true;
        }

        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idpregunta = Convert.ToInt32(txtPreguntas.Name);
            respcandidato.Punto = Convert.ToInt32(punto);
            respcandidato.Respuesta = Convert.ToString(Respuesta);
            respcandidato.Serie = 2;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);

        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(txtPreguntas.Name);
            respcandidato.Punto = punto;
            respcandidato.Respuesta = Convert.ToString(Respuesta);
            cliente.ActualizaTermanRespuesta(respcandidato);

        }

        private void LimpiarRadios()
        {
            foreach (Control radio in this.gbPreguntas.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
        }

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if (rbtA.Checked == true || rbtB.Checked == true || rbtC.Checked == true)
                {
                    x++; GuardarRespuestas();
                }
                else
                {

                }
                this.Dispose();
            }

        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            tiempo.Enabled = true;
        }


        private void Validacion()
        {
            if (rbtA.Checked == true || rbtB.Checked == true || rbtC.Checked == true)
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        #region Validacion de los RAdioButtons
        private void rbtA_Click(object sender, EventArgs e)
        {
            Validacion();
        }

        private void rbtB_Click(object sender, EventArgs e)
        {
            Validacion();
        }

        private void rbtC_Click(object sender, EventArgs e)
        {
            Validacion();
        }
        #endregion

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_terman_respuestas_candidato[] listarrespuesta = cliente.ListaTermanRespuestasCandidatos(number_prueba);

            dtactualizar.Clear();
            for (int w = 0; w < listarrespuesta.Length; w++)
            {
                if (listarrespuesta[w].Serie == 2)
                {
                    DataRow row = dtactualizar.NewRow();
                    row["id_respuesta_candidato"] = Convert.ToInt32(listarrespuesta[w].IdRespuestaCandidato);
                    row["id_pregunta"] = Convert.ToInt32(listarrespuesta[w].Idpregunta);
                    row["respuesta"] = Convert.ToString(listarrespuesta[w].Respuesta);
                    row["punto"] = Convert.ToInt32(listarrespuesta[w].Punto);
                    row["serie"] = Convert.ToInt32(listarrespuesta[w].Serie);
                    row["numero_prueba"] = Convert.ToInt32(listarrespuesta[w].Numero_prueba);
                    dtactualizar.Rows.Add(row);
                }
            }
            recorrido = dtactualizar.Rows.Count;

            x--;

            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }

            btnSiguiente.Text = "Siguiente.";

            if (recorrido > x)
            {
                Retroceder();
            }

            MostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";
            }
            btnSiguiente.Enabled = true;

        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrar_ayuda.Show();
            this.btnIniciarAyuda.Hide();

        }

        private void btnCerrar_ayuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void frmTermanSerie2_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnCerrar_ayuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindows_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}
