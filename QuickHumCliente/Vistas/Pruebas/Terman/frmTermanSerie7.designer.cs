﻿namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    partial class frmTermanSerie7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTermanSerie7));
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rbtL = new System.Windows.Forms.RadioButton();
            this.rbtK = new System.Windows.Forms.RadioButton();
            this.rbtJ = new System.Windows.Forms.RadioButton();
            this.rbtI = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rbtM = new System.Windows.Forms.RadioButton();
            this.rbtN = new System.Windows.Forms.RadioButton();
            this.rbtP = new System.Windows.Forms.RadioButton();
            this.rbtO = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbtH = new System.Windows.Forms.RadioButton();
            this.rbtG = new System.Windows.Forms.RadioButton();
            this.rbtF = new System.Windows.Forms.RadioButton();
            this.rbtE = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtD = new System.Windows.Forms.RadioButton();
            this.rbtC = new System.Windows.Forms.RadioButton();
            this.rbtA = new System.Windows.Forms.RadioButton();
            this.rbtB = new System.Windows.Forms.RadioButton();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCerrar_ayuda = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(644, 36);
            this.label3.TabIndex = 84;
            this.label3.Text = "Serie VII Terman";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.Controls.Add(this.rbtL);
            this.panel5.Controls.Add(this.rbtK);
            this.panel5.Controls.Add(this.rbtJ);
            this.panel5.Controls.Add(this.rbtI);
            this.panel5.Font = new System.Drawing.Font("Calibri", 12F);
            this.panel5.Location = new System.Drawing.Point(24, 191);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(547, 43);
            this.panel5.TabIndex = 86;
            // 
            // rbtL
            // 
            this.rbtL.BackColor = System.Drawing.Color.Transparent;
            this.rbtL.Location = new System.Drawing.Point(417, 8);
            this.rbtL.Name = "rbtL";
            this.rbtL.Size = new System.Drawing.Size(97, 22);
            this.rbtL.TabIndex = 83;
            this.rbtL.TabStop = true;
            this.rbtL.Text = "NO";
            this.rbtL.UseVisualStyleBackColor = false;
            this.rbtL.Click += new System.EventHandler(this.rbtL_Click);
            // 
            // rbtK
            // 
            this.rbtK.BackColor = System.Drawing.Color.Transparent;
            this.rbtK.Location = new System.Drawing.Point(290, 8);
            this.rbtK.Name = "rbtK";
            this.rbtK.Size = new System.Drawing.Size(97, 22);
            this.rbtK.TabIndex = 84;
            this.rbtK.TabStop = true;
            this.rbtK.Text = "NO";
            this.rbtK.UseVisualStyleBackColor = false;
            this.rbtK.Click += new System.EventHandler(this.rbtK_Click);
            // 
            // rbtJ
            // 
            this.rbtJ.BackColor = System.Drawing.Color.Transparent;
            this.rbtJ.Location = new System.Drawing.Point(152, 8);
            this.rbtJ.Name = "rbtJ";
            this.rbtJ.Size = new System.Drawing.Size(97, 22);
            this.rbtJ.TabIndex = 85;
            this.rbtJ.TabStop = true;
            this.rbtJ.Text = "NO";
            this.rbtJ.UseVisualStyleBackColor = false;
            this.rbtJ.Click += new System.EventHandler(this.rbtJ_Click);
            // 
            // rbtI
            // 
            this.rbtI.BackColor = System.Drawing.Color.Transparent;
            this.rbtI.Location = new System.Drawing.Point(7, 8);
            this.rbtI.Name = "rbtI";
            this.rbtI.Size = new System.Drawing.Size(96, 22);
            this.rbtI.TabIndex = 86;
            this.rbtI.TabStop = true;
            this.rbtI.Text = "NO";
            this.rbtI.UseVisualStyleBackColor = false;
            this.rbtI.Click += new System.EventHandler(this.rbtI_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.Controls.Add(this.rbtM);
            this.panel6.Controls.Add(this.rbtN);
            this.panel6.Controls.Add(this.rbtP);
            this.panel6.Controls.Add(this.rbtO);
            this.panel6.Font = new System.Drawing.Font("Calibri", 12F);
            this.panel6.Location = new System.Drawing.Point(24, 270);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(547, 43);
            this.panel6.TabIndex = 86;
            // 
            // rbtM
            // 
            this.rbtM.BackColor = System.Drawing.Color.Transparent;
            this.rbtM.Location = new System.Drawing.Point(7, 10);
            this.rbtM.Name = "rbtM";
            this.rbtM.Size = new System.Drawing.Size(96, 22);
            this.rbtM.TabIndex = 79;
            this.rbtM.TabStop = true;
            this.rbtM.Text = "NO";
            this.rbtM.UseVisualStyleBackColor = false;
            this.rbtM.Click += new System.EventHandler(this.rbtM_Click);
            // 
            // rbtN
            // 
            this.rbtN.BackColor = System.Drawing.Color.Transparent;
            this.rbtN.Location = new System.Drawing.Point(152, 10);
            this.rbtN.Name = "rbtN";
            this.rbtN.Size = new System.Drawing.Size(97, 22);
            this.rbtN.TabIndex = 82;
            this.rbtN.TabStop = true;
            this.rbtN.Text = "NO";
            this.rbtN.UseVisualStyleBackColor = false;
            this.rbtN.Click += new System.EventHandler(this.rbtN_Click);
            // 
            // rbtP
            // 
            this.rbtP.BackColor = System.Drawing.Color.Transparent;
            this.rbtP.Location = new System.Drawing.Point(417, 10);
            this.rbtP.Name = "rbtP";
            this.rbtP.Size = new System.Drawing.Size(97, 22);
            this.rbtP.TabIndex = 81;
            this.rbtP.TabStop = true;
            this.rbtP.Text = "NO";
            this.rbtP.UseVisualStyleBackColor = false;
            this.rbtP.Click += new System.EventHandler(this.rbtP_Click);
            // 
            // rbtO
            // 
            this.rbtO.BackColor = System.Drawing.Color.Transparent;
            this.rbtO.Location = new System.Drawing.Point(290, 10);
            this.rbtO.Name = "rbtO";
            this.rbtO.Size = new System.Drawing.Size(97, 22);
            this.rbtO.TabIndex = 80;
            this.rbtO.TabStop = true;
            this.rbtO.Text = "NO";
            this.rbtO.UseVisualStyleBackColor = false;
            this.rbtO.Click += new System.EventHandler(this.rbtO_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Controls.Add(this.rbtH);
            this.panel3.Controls.Add(this.rbtG);
            this.panel3.Controls.Add(this.rbtF);
            this.panel3.Controls.Add(this.rbtE);
            this.panel3.Font = new System.Drawing.Font("Calibri", 12F);
            this.panel3.Location = new System.Drawing.Point(24, 112);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(547, 43);
            this.panel3.TabIndex = 85;
            // 
            // rbtH
            // 
            this.rbtH.BackColor = System.Drawing.Color.Transparent;
            this.rbtH.Location = new System.Drawing.Point(417, 9);
            this.rbtH.Name = "rbtH";
            this.rbtH.Size = new System.Drawing.Size(97, 22);
            this.rbtH.TabIndex = 78;
            this.rbtH.TabStop = true;
            this.rbtH.Text = "NO";
            this.rbtH.UseVisualStyleBackColor = false;
            this.rbtH.Click += new System.EventHandler(this.rbtH_Click);
            // 
            // rbtG
            // 
            this.rbtG.BackColor = System.Drawing.Color.Transparent;
            this.rbtG.Location = new System.Drawing.Point(290, 9);
            this.rbtG.Name = "rbtG";
            this.rbtG.Size = new System.Drawing.Size(97, 22);
            this.rbtG.TabIndex = 77;
            this.rbtG.TabStop = true;
            this.rbtG.Text = "SI";
            this.rbtG.UseVisualStyleBackColor = false;
            this.rbtG.Click += new System.EventHandler(this.rbtG_Click);
            // 
            // rbtF
            // 
            this.rbtF.BackColor = System.Drawing.Color.Transparent;
            this.rbtF.Location = new System.Drawing.Point(152, 9);
            this.rbtF.Name = "rbtF";
            this.rbtF.Size = new System.Drawing.Size(97, 22);
            this.rbtF.TabIndex = 75;
            this.rbtF.TabStop = true;
            this.rbtF.Text = "NO";
            this.rbtF.UseVisualStyleBackColor = false;
            this.rbtF.Click += new System.EventHandler(this.rbtF_Click);
            // 
            // rbtE
            // 
            this.rbtE.BackColor = System.Drawing.Color.Transparent;
            this.rbtE.Location = new System.Drawing.Point(7, 8);
            this.rbtE.Name = "rbtE";
            this.rbtE.Size = new System.Drawing.Size(96, 22);
            this.rbtE.TabIndex = 76;
            this.rbtE.TabStop = true;
            this.rbtE.Text = "SI";
            this.rbtE.UseVisualStyleBackColor = false;
            this.rbtE.Click += new System.EventHandler(this.rbtE_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.rbtD);
            this.panel2.Controls.Add(this.rbtC);
            this.panel2.Controls.Add(this.rbtA);
            this.panel2.Controls.Add(this.rbtB);
            this.panel2.Font = new System.Drawing.Font("Calibri", 12F);
            this.panel2.Location = new System.Drawing.Point(24, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(547, 43);
            this.panel2.TabIndex = 84;
            // 
            // rbtD
            // 
            this.rbtD.BackColor = System.Drawing.Color.Transparent;
            this.rbtD.Location = new System.Drawing.Point(417, 10);
            this.rbtD.Name = "rbtD";
            this.rbtD.Size = new System.Drawing.Size(97, 22);
            this.rbtD.TabIndex = 71;
            this.rbtD.TabStop = true;
            this.rbtD.Text = "NO";
            this.rbtD.UseVisualStyleBackColor = false;
            this.rbtD.Click += new System.EventHandler(this.rbtD_Click);
            // 
            // rbtC
            // 
            this.rbtC.BackColor = System.Drawing.Color.Transparent;
            this.rbtC.Location = new System.Drawing.Point(290, 10);
            this.rbtC.Name = "rbtC";
            this.rbtC.Size = new System.Drawing.Size(97, 22);
            this.rbtC.TabIndex = 74;
            this.rbtC.TabStop = true;
            this.rbtC.Text = "SI";
            this.rbtC.UseVisualStyleBackColor = false;
            this.rbtC.Click += new System.EventHandler(this.rbtC_Click);
            // 
            // rbtA
            // 
            this.rbtA.BackColor = System.Drawing.Color.Transparent;
            this.rbtA.Location = new System.Drawing.Point(6, 10);
            this.rbtA.Name = "rbtA";
            this.rbtA.Size = new System.Drawing.Size(97, 22);
            this.rbtA.TabIndex = 71;
            this.rbtA.TabStop = true;
            this.rbtA.Text = "SI";
            this.rbtA.UseVisualStyleBackColor = false;
            this.rbtA.Click += new System.EventHandler(this.rbtA_Click);
            // 
            // rbtB
            // 
            this.rbtB.BackColor = System.Drawing.Color.Transparent;
            this.rbtB.Location = new System.Drawing.Point(152, 10);
            this.rbtB.Name = "rbtB";
            this.rbtB.Size = new System.Drawing.Size(97, 22);
            this.rbtB.TabIndex = 70;
            this.rbtB.Text = "NO";
            this.rbtB.UseVisualStyleBackColor = false;
            this.rbtB.Click += new System.EventHandler(this.rbtB_Click);
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl4.Location = new System.Drawing.Point(21, 248);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(49, 19);
            this.lbl4.TabIndex = 82;
            this.lbl4.Text = "label8";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl3.Location = new System.Drawing.Point(21, 166);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(49, 19);
            this.lbl3.TabIndex = 81;
            this.lbl3.Text = "label6";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl2.Location = new System.Drawing.Point(21, 91);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(49, 19);
            this.lbl2.TabIndex = 80;
            this.lbl2.Text = "label5";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl1.Location = new System.Drawing.Point(21, 9);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(49, 19);
            this.lbl1.TabIndex = 79;
            this.lbl1.Text = "label4";
            // 
            // panelAyuda
            // 
            this.panelAyuda.Controls.Add(this.panel7);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(644, 526);
            this.panelAyuda.TabIndex = 93;
            // 
            // panel7
            // 
            this.panel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Controls.Add(this.btnIniciarAyuda);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.btnCerrar_ayuda);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Location = new System.Drawing.Point(64, 17);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(527, 457);
            this.panel7.TabIndex = 78;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Calibri", 12F);
            this.label6.Location = new System.Drawing.Point(25, 284);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(463, 35);
            this.label6.TabIndex = 84;
            this.label6.Text = "2.-  EL SOMBRERO es a LA CABEZA como el zapato es a:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.radioButton2);
            this.panel4.Controls.Add(this.radioButton3);
            this.panel4.Controls.Add(this.radioButton7);
            this.panel4.Controls.Add(this.radioButton8);
            this.panel4.Enabled = false;
            this.panel4.Location = new System.Drawing.Point(29, 322);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(459, 40);
            this.panel4.TabIndex = 77;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton2.Location = new System.Drawing.Point(18, 8);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(83, 20);
            this.radioButton2.TabIndex = 74;
            this.radioButton2.Text = "a) Brazo";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton3.Location = new System.Drawing.Point(129, 8);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(89, 20);
            this.radioButton3.TabIndex = 71;
            this.radioButton3.Text = "b) Abrigo";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.BackColor = System.Drawing.Color.Transparent;
            this.radioButton7.Checked = true;
            this.radioButton7.Enabled = false;
            this.radioButton7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton7.Location = new System.Drawing.Point(255, 8);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(65, 20);
            this.radioButton7.TabIndex = 73;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "c) Pie";
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.BackColor = System.Drawing.Color.Transparent;
            this.radioButton8.Enabled = false;
            this.radioButton8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton8.Location = new System.Drawing.Point(362, 8);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(89, 20);
            this.radioButton8.TabIndex = 72;
            this.radioButton8.Text = "d) Pierna";
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(205, 388);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 36);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Calibri", 12F);
            this.label5.Location = new System.Drawing.Point(25, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(463, 65);
            this.label5.TabIndex = 84;
            this.label5.Text = "Ejemplos:\r\n\r\n1.-  EL OIDO es a OIR como el ojo es a:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.radioButton4);
            this.panel1.Controls.Add(this.radioButton6);
            this.panel1.Controls.Add(this.radioButton5);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(29, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(459, 40);
            this.panel1.TabIndex = 76;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton1.Location = new System.Drawing.Point(18, 8);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 20);
            this.radioButton1.TabIndex = 74;
            this.radioButton1.Text = "a) Mesa";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Checked = true;
            this.radioButton4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton4.Location = new System.Drawing.Point(129, 8);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(67, 20);
            this.radioButton4.TabIndex = 71;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "b) Ver";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.BackColor = System.Drawing.Color.Transparent;
            this.radioButton6.Enabled = false;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton6.Location = new System.Drawing.Point(255, 8);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(82, 20);
            this.radioButton6.TabIndex = 73;
            this.radioButton6.Text = "c) Mano";
            this.radioButton6.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.BackColor = System.Drawing.Color.Transparent;
            this.radioButton5.Enabled = false;
            this.radioButton5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton5.Location = new System.Drawing.Point(362, 8);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(84, 20);
            this.radioButton5.TabIndex = 72;
            this.radioButton5.Text = "d) Jugar";
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Calibri", 12F);
            this.label4.Location = new System.Drawing.Point(25, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(463, 74);
            this.label4.TabIndex = 84;
            this.label4.Text = "En cada oración que se plantea a continuación existe una relación entre dos palab" +
    "ras. Se le solicita identificar la palabra relacionada con tercer palabra y seña" +
    "lar el \r\nliteral correspondiente.";
            // 
            // btnCerrar_ayuda
            // 
            this.btnCerrar_ayuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar_ayuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrar_ayuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrar_ayuda.FlatAppearance.BorderSize = 0;
            this.btnCerrar_ayuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrar_ayuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar_ayuda.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar_ayuda.Image")));
            this.btnCerrar_ayuda.Location = new System.Drawing.Point(489, 1);
            this.btnCerrar_ayuda.Name = "btnCerrar_ayuda";
            this.btnCerrar_ayuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrar_ayuda.TabIndex = 83;
            this.btnCerrar_ayuda.UseVisualStyleBackColor = false;
            this.btnCerrar_ayuda.Visible = false;
            this.btnCerrar_ayuda.Click += new System.EventHandler(this.btnCerrar_ayuda_Click_1);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(527, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbPreguntas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gbPreguntas.Controls.Add(this.panel5);
            this.gbPreguntas.Controls.Add(this.panel2);
            this.gbPreguntas.Controls.Add(this.panel6);
            this.gbPreguntas.Controls.Add(this.lbl1);
            this.gbPreguntas.Controls.Add(this.btnAnterior);
            this.gbPreguntas.Controls.Add(this.panel3);
            this.gbPreguntas.Controls.Add(this.btnAyuda);
            this.gbPreguntas.Controls.Add(this.btnSiguiente);
            this.gbPreguntas.Controls.Add(this.lbl2);
            this.gbPreguntas.Controls.Add(this.lbl3);
            this.gbPreguntas.Controls.Add(this.lbl4);
            this.gbPreguntas.Location = new System.Drawing.Point(18, 92);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(597, 412);
            this.gbPreguntas.TabIndex = 100;
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(325, 353);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 98;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(24, 353);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 97;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(461, 353);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 96;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.lbltime);
            this.panel8.Location = new System.Drawing.Point(381, 36);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(249, 50);
            this.panel8.TabIndex = 99;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(49, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 94;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbltime.Location = new System.Drawing.Point(170, 15);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 95;
            this.lbltime.Text = "label9";
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.Image")));
            this.btnCloseWindow.Location = new System.Drawing.Point(595, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 84;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // frmTermanSerie7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(644, 562);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.gbPreguntas);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(646, 564);
            this.Name = "frmTermanSerie7";
            this.Text = "Serie VII Terman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTermanSerie7_FormClosing);
            this.Load += new System.EventHandler(this.frmTermanSerie7_Load);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelAyuda.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rbtE;
        private System.Windows.Forms.RadioButton rbtF;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton rbtG;
        private System.Windows.Forms.RadioButton rbtH;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbtC;
        private System.Windows.Forms.RadioButton rbtD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtA;
        private System.Windows.Forms.RadioButton rbtB;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbtL;
        private System.Windows.Forms.RadioButton rbtK;
        private System.Windows.Forms.RadioButton rbtJ;
        private System.Windows.Forms.RadioButton rbtI;
        private System.Windows.Forms.RadioButton rbtM;
        private System.Windows.Forms.RadioButton rbtN;
        private System.Windows.Forms.RadioButton rbtP;
        private System.Windows.Forms.RadioButton rbtO;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnCerrar_ayuda;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel gbPreguntas;
    }
}