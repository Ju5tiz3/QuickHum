﻿namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    partial class frmTermanSerie4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTermanSerie4));
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMsj = new System.Windows.Forms.Label();
            this.pnlOpciones = new System.Windows.Forms.Panel();
            this.chkOpcion5 = new System.Windows.Forms.CheckBox();
            this.chkOpcion4 = new System.Windows.Forms.CheckBox();
            this.chkOpcion3 = new System.Windows.Forms.CheckBox();
            this.chkOpcion2 = new System.Windows.Forms.CheckBox();
            this.chkOpcion1 = new System.Windows.Forms.CheckBox();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelAyuda.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlOpciones.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAyuda
            // 
            this.panelAyuda.Controls.Add(this.panel3);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(670, 575);
            this.panelAyuda.TabIndex = 80;
            this.panelAyuda.Tag = "";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.btnCerrarAyuda);
            this.panel3.Controls.Add(this.btnIniciarAyuda);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(79, 43);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(513, 326);
            this.panel3.TabIndex = 77;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarAyuda.Image")));
            this.btnCerrarAyuda.Location = new System.Drawing.Point(478, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 80;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(206, 264);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 36);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 48);
            this.label4.TabIndex = 62;
            this.label4.Text = "Ejemplo:\r\n\r\nUn hombre tiene siempre:\r\n";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.checkBox4);
            this.panel1.Controls.Add(this.checkBox5);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(24, 182);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 56);
            this.panel1.TabIndex = 76;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Font = new System.Drawing.Font("Calibri", 12F);
            this.checkBox4.Location = new System.Drawing.Point(300, 15);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(60, 23);
            this.checkBox4.TabIndex = 71;
            this.checkBox4.Text = "Boca";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Calibri", 12F);
            this.checkBox5.Location = new System.Drawing.Point(382, 15);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(71, 23);
            this.checkBox5.TabIndex = 71;
            this.checkBox5.Text = "Dinero";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 12F);
            this.checkBox1.Location = new System.Drawing.Point(10, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(74, 23);
            this.checkBox1.TabIndex = 71;
            this.checkBox1.Text = "Cuerpo";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Calibri", 12F);
            this.checkBox2.Location = new System.Drawing.Point(107, 15);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(64, 23);
            this.checkBox2.TabIndex = 71;
            this.checkBox2.Text = "Gorra";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Calibri", 12F);
            this.checkBox3.Location = new System.Drawing.Point(193, 15);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(82, 23);
            this.checkBox3.TabIndex = 71;
            this.checkBox3.Text = "Guantes";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(513, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Calibri", 12F);
            this.label1.Location = new System.Drawing.Point(19, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(474, 65);
            this.label1.TabIndex = 63;
            this.label1.Text = "Seleccione en pantalla las opciones correspondientes a las dos palabras que indic" +
    "an  algo que siempre tiene el sujeto. Anote solamente dos para cada renglón.\r\n";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(670, 36);
            this.label3.TabIndex = 81;
            this.label3.Text = " Serie IV Terman";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMsj
            // 
            this.lblMsj.AutoSize = true;
            this.lblMsj.BackColor = System.Drawing.Color.Transparent;
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(13, 331);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(152, 19);
            this.lblMsj.TabIndex = 74;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // pnlOpciones
            // 
            this.pnlOpciones.Controls.Add(this.chkOpcion5);
            this.pnlOpciones.Controls.Add(this.chkOpcion4);
            this.pnlOpciones.Controls.Add(this.chkOpcion3);
            this.pnlOpciones.Controls.Add(this.chkOpcion2);
            this.pnlOpciones.Controls.Add(this.chkOpcion1);
            this.pnlOpciones.Location = new System.Drawing.Point(14, 94);
            this.pnlOpciones.Name = "pnlOpciones";
            this.pnlOpciones.Size = new System.Drawing.Size(599, 226);
            this.pnlOpciones.TabIndex = 73;
            // 
            // chkOpcion5
            // 
            this.chkOpcion5.BackColor = System.Drawing.Color.Transparent;
            this.chkOpcion5.Font = new System.Drawing.Font("Calibri", 12F);
            this.chkOpcion5.Location = new System.Drawing.Point(21, 177);
            this.chkOpcion5.Name = "chkOpcion5";
            this.chkOpcion5.Size = new System.Drawing.Size(217, 35);
            this.chkOpcion5.TabIndex = 72;
            this.chkOpcion5.Text = "chkOpcion5";
            this.chkOpcion5.UseVisualStyleBackColor = false;
            this.chkOpcion5.CheckedChanged += new System.EventHandler(this.chkOpcion5_CheckedChanged);
            // 
            // chkOpcion4
            // 
            this.chkOpcion4.BackColor = System.Drawing.Color.Transparent;
            this.chkOpcion4.Font = new System.Drawing.Font("Calibri", 12F);
            this.chkOpcion4.Location = new System.Drawing.Point(21, 136);
            this.chkOpcion4.Name = "chkOpcion4";
            this.chkOpcion4.Size = new System.Drawing.Size(217, 35);
            this.chkOpcion4.TabIndex = 72;
            this.chkOpcion4.Text = "chkOpcion4";
            this.chkOpcion4.UseVisualStyleBackColor = false;
            this.chkOpcion4.CheckedChanged += new System.EventHandler(this.chkOpcion4_CheckedChanged);
            // 
            // chkOpcion3
            // 
            this.chkOpcion3.BackColor = System.Drawing.Color.Transparent;
            this.chkOpcion3.Font = new System.Drawing.Font("Calibri", 12F);
            this.chkOpcion3.Location = new System.Drawing.Point(21, 95);
            this.chkOpcion3.Name = "chkOpcion3";
            this.chkOpcion3.Size = new System.Drawing.Size(217, 35);
            this.chkOpcion3.TabIndex = 72;
            this.chkOpcion3.Text = "chkOpcion3";
            this.chkOpcion3.UseVisualStyleBackColor = false;
            this.chkOpcion3.CheckedChanged += new System.EventHandler(this.chkOpcion3_CheckedChanged);
            // 
            // chkOpcion2
            // 
            this.chkOpcion2.BackColor = System.Drawing.Color.Transparent;
            this.chkOpcion2.Font = new System.Drawing.Font("Calibri", 12F);
            this.chkOpcion2.Location = new System.Drawing.Point(21, 54);
            this.chkOpcion2.Name = "chkOpcion2";
            this.chkOpcion2.Size = new System.Drawing.Size(217, 35);
            this.chkOpcion2.TabIndex = 72;
            this.chkOpcion2.Text = "chkOpcion2";
            this.chkOpcion2.UseVisualStyleBackColor = false;
            this.chkOpcion2.CheckedChanged += new System.EventHandler(this.chkOpcion2_CheckedChanged);
            // 
            // chkOpcion1
            // 
            this.chkOpcion1.BackColor = System.Drawing.Color.Transparent;
            this.chkOpcion1.Font = new System.Drawing.Font("Calibri", 12F);
            this.chkOpcion1.Location = new System.Drawing.Point(21, 13);
            this.chkOpcion1.Name = "chkOpcion1";
            this.chkOpcion1.Size = new System.Drawing.Size(217, 35);
            this.chkOpcion1.TabIndex = 72;
            this.chkOpcion1.Text = "chkOpcion1";
            this.chkOpcion1.UseVisualStyleBackColor = false;
            this.chkOpcion1.CheckedChanged += new System.EventHandler(this.chkOpcion1_CheckedChanged);
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreguntas.Location = new System.Drawing.Point(14, 13);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.Size = new System.Drawing.Size(599, 75);
            this.txtPreguntas.TabIndex = 12;
            this.txtPreguntas.Text = "";
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.Color.White;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(367, 422);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 87;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.Color.White;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(17, 422);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 86;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Location = new System.Drawing.Point(244, 3);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(118, 19);
            this.lblNumPreg.TabIndex = 84;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(498, 422);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 83;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.lbltime);
            this.panel2.Location = new System.Drawing.Point(406, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(249, 39);
            this.panel2.TabIndex = 88;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(47, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Location = new System.Drawing.Point(169, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.Image")));
            this.btnCloseWindow.Location = new System.Drawing.Point(634, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 89;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbPreguntas.BackColor = System.Drawing.Color.White;
            this.gbPreguntas.Controls.Add(this.panel5);
            this.gbPreguntas.Controls.Add(this.lblMsj);
            this.gbPreguntas.Controls.Add(this.txtPreguntas);
            this.gbPreguntas.Controls.Add(this.pnlOpciones);
            this.gbPreguntas.Controls.Add(this.btnAnterior);
            this.gbPreguntas.Controls.Add(this.btnAyuda);
            this.gbPreguntas.Controls.Add(this.btnSiguiente);
            this.gbPreguntas.Font = new System.Drawing.Font("Calibri", 12F);
            this.gbPreguntas.Location = new System.Drawing.Point(15, 94);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(628, 474);
            this.gbPreguntas.TabIndex = 90;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.LightGray;
            this.panel5.Controls.Add(this.lblNumPreg);
            this.panel5.Location = new System.Drawing.Point(0, 364);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(628, 24);
            this.panel5.TabIndex = 85;
            // 
            // frmTermanSerie4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(670, 611);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.gbPreguntas);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(672, 613);
            this.Name = "frmTermanSerie4";
            this.Text = "Serie IV Terman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTermanSerieIV_FormClosing);
            this.Load += new System.EventHandler(this.frmTermanSerieIV_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlOpciones.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlOpciones;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.CheckBox chkOpcion5;
        private System.Windows.Forms.CheckBox chkOpcion4;
        private System.Windows.Forms.CheckBox chkOpcion3;
        private System.Windows.Forms.CheckBox chkOpcion2;
        private System.Windows.Forms.CheckBox chkOpcion1;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Panel panel5;
    }
}