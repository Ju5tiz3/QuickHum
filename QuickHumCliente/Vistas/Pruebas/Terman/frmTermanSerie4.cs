﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie4 : Form
    {

        QuickHumClient Cliente = Globales.cliente;
        dto_terman_respuestas_candidato Respuestas = new dto_terman_respuestas_candidato();
        dto_terman_preguntas[] ListaPreguntas;
        dto_terman_respuestas_candidato[] ListaRespuestasCandidato;

        DataTable dtPreguntas = new DataTable();
        DataTable dtRespuestas = new DataTable();
        DataTable dtRespuestasCandidato = new DataTable();
        private int number_prueba = frmTerman.numero_pruebaTerman;
        int x = 0;
        int Recorrido = 0;
        int NumeroPrueba = 1;
        string[] RespuestaCorrecta;
        string[] Respuesta = new string[4];

        public frmTermanSerie4()
        {
            InitializeComponent();
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {

            this.lblNumPreg.Text = "Pregunta: 1 de 18";
            this.panelAyuda.Hide();
            //this.Size = new Size(687, 564);
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;

            MostrarPreguntas();

        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {

            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
            this.Size = new Size(687, 564);

        }


        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {

            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void frmTermanSerieIV_Load(object sender, EventArgs e)
        {
            btnSiguiente.Name = "Siguiente";

            //Estructuramos los campos de los DataTables
            dtPreguntas.Columns.Add("IdPregunta");
            dtPreguntas.Columns.Add("Pregunta");
            dtPreguntas.Columns.Add("RespuestaCorrecta");

            dtRespuestas.Columns.Add("IdRespuesta");
            dtRespuestas.Columns.Add("IdPregunta");
            dtRespuestas.Columns.Add("Respuesta");

            dtRespuestasCandidato.Columns.Add("IdRespuesta");
            dtRespuestasCandidato.Columns.Add("IdPregunta");
            dtRespuestasCandidato.Columns.Add("punto");
            dtRespuestasCandidato.Columns.Add("Respuesta");

            DateTime hora;

            CargarPreguntasRespuestas();
            //this.Size=new Size(686, 381);
            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:03:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void CargarPreguntasRespuestas()
        {

            ListaPreguntas = Cliente.ListadoTermanPreguntas();


            for (int i = 0; i < ListaPreguntas.ToArray().Count(); i++)
            {

                if (ListaPreguntas[i].Serie == 4)
                {
                    DataRow RowPregunta = dtPreguntas.NewRow();

                    RowPregunta["IdPregunta"] = ListaPreguntas[i].Idpregunta;
                    RowPregunta["Pregunta"] = ListaPreguntas[i].Pregunta;
                    RowPregunta["RespuestaCorrecta"] = ListaPreguntas[i].Respuesta_Correcta;

                    dtPreguntas.Rows.Add(RowPregunta);

                    for (int j = 0; j < 5; j++)
                    {

                        DataRow RowRespuestas = dtRespuestas.NewRow();

                        RowRespuestas["IdRespuesta"] = ListaPreguntas[i].Respuesta[j].Idrespuesta;
                        RowRespuestas["IdPregunta"] = ListaPreguntas[i].Respuesta[j].Idpregunta;
                        RowRespuestas["Respuesta"] = ListaPreguntas[i].Respuesta[j].Respuesta;

                        dtRespuestas.Rows.Add(RowRespuestas);

                    }

                }
            }

        }


        private void MostrarPreguntas()
        {
            limpiarCampos();

            txtPreguntas.Text = dtPreguntas.Rows[x][1].ToString();

            chkOpcion1.Text = dtRespuestas.Rows[(x * 5)][2].ToString();
            chkOpcion2.Text = dtRespuestas.Rows[(x * 5) + 1][2].ToString();
            chkOpcion3.Text = dtRespuestas.Rows[(x * 5) + 2][2].ToString();
            chkOpcion4.Text = dtRespuestas.Rows[(x * 5) + 3][2].ToString();
            chkOpcion5.Text = dtRespuestas.Rows[(x * 5) + 4][2].ToString();

            RespuestaCorrecta = dtPreguntas.Rows[x][2].ToString().Split(',');


            if (Recorrido > x)
            {
                string[] ResCandidato = dtRespuestasCandidato.Rows[x][3].ToString().Split(',');


                if (ResCandidato[0] == chkOpcion1.Text || ResCandidato[1] == chkOpcion1.Text)
                {
                    chkOpcion1.Checked = true;
                }

                if (ResCandidato[0] == chkOpcion2.Text || ResCandidato[1] == chkOpcion2.Text)
                {
                    chkOpcion2.Checked = true;
                }

                if (ResCandidato[0] == chkOpcion3.Text || ResCandidato[1] == chkOpcion3.Text)
                {
                    chkOpcion3.Checked = true;
                }

                if (ResCandidato[0] == chkOpcion4.Text || ResCandidato[1] == chkOpcion4.Text)
                {
                    chkOpcion4.Checked = true;
                }

                if (ResCandidato[0] == chkOpcion5.Text || ResCandidato[1] == chkOpcion5.Text)
                {
                    chkOpcion5.Checked = true;
                }

            }

        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Name == "Siguiente")
            {
                x++;
                guardarRespuestas();


                if (this.btnSiguiente.Name == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    btnSiguiente.Name = "Finalizar";
                    return;
                }




                MostrarPreguntas();

                limpiarCampos();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Name == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {

                    btnSiguiente.Name = "Siguiente";
                    limpiarCampos();
                    btnSiguiente.Enabled = false;
                }

                MostrarPreguntas();


            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {

                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;

            if (x == dtPreguntas.Rows.Count - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }

            btnAnterior.Enabled = true;


        }

        private void FinalizarPrueba()
        {

            if (chkOpcion1.Checked == false || chkOpcion2.Checked == false || chkOpcion3.Checked == false || chkOpcion4.Checked == false || chkOpcion5.Checked == false)
            {

            }
            else
            {
                x++; guardarRespuestas();
            }

            this.Dispose();
        }

        private void limpiarCampos()
        {
            chkOpcion1.Checked = false;
            chkOpcion2.Checked = false;
            chkOpcion3.Checked = false;
            chkOpcion4.Checked = false;
            chkOpcion5.Checked = false;

        }

        private void guardarRespuestas()
        {

            Respuestas.Numero_prueba = number_prueba;
            Respuestas.Idpregunta = Convert.ToInt32(dtPreguntas.Rows[x][0]);
            Respuestas.Punto = ValidarRespuesta();
            Respuestas.Respuesta = Respuesta[0] + "," + Respuesta[1];
            Respuestas.Serie = 4;

            Cliente.InsertaTermanRespuesta(Respuestas);

        }

        private void ActualizarRespuestas()
        {

            Respuestas.IdRespuestaCandidato = Convert.ToInt32(dtRespuestasCandidato.Rows[x - 1][0]);
            Respuestas.Idpregunta = Convert.ToInt32(dtRespuestasCandidato.Rows[x - 1][1]);
            Respuestas.Numero_prueba = number_prueba;
            Respuestas.Punto = ValidarRespuesta();
            Respuestas.Respuesta = Respuesta[0] + "," + Respuesta[1];
            Respuestas.Serie = 4;

            Cliente.ActualizaTermanRespuesta(Respuestas);


        }

        private int ValidarRespuesta()
        {
            int Validacion = 0;
            int ContRespuestas = 0;
            Respuesta[0] = "";
            Respuesta[1] = "";
            int Punto = 0;

            for (int i = 0; i < 2; i++)
            {
                if (chkOpcion1.Checked == true)
                {
                    if (chkOpcion1.Text == RespuestaCorrecta[i])
                    {
                        Validacion = Validacion + 1;

                    }
                    Respuesta[ContRespuestas] = chkOpcion1.Text;
                    ContRespuestas++;
                }

                if (chkOpcion2.Checked == true)
                {
                    if (chkOpcion2.Text == RespuestaCorrecta[i])
                    {
                        Validacion = Validacion + 1;

                    }
                    Respuesta[ContRespuestas] = chkOpcion2.Text;
                    ContRespuestas++;
                }

                if (chkOpcion3.Checked == true)
                {
                    if (chkOpcion3.Text == RespuestaCorrecta[i])
                    {
                        Validacion = Validacion + 1;

                    }
                    Respuesta[ContRespuestas] = chkOpcion3.Text;
                    ContRespuestas++;
                }

                if (chkOpcion4.Checked == true)
                {
                    if (chkOpcion4.Text == RespuestaCorrecta[i])
                    {
                        Validacion = Validacion + 1;

                    }
                    Respuesta[ContRespuestas] = chkOpcion4.Text;
                    ContRespuestas++;

                }

                if (chkOpcion5.Checked == true)
                {
                    if (chkOpcion5.Text == RespuestaCorrecta[i])
                    {
                        Validacion = Validacion + 1;

                    }

                    Respuesta[ContRespuestas] = chkOpcion5.Text;
                    ContRespuestas++;

                }
            }


            if (Validacion == 2)
            {
                Punto = 1;
            }
            else
            {
                Punto = 0;
            }


            return Punto;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            //Limpiamos las respuestas anteriores.
            dtRespuestasCandidato.Clear();

            //Listamos las re`spuestas del candidato.
            dto_terman_respuestas_candidato[] ListaRespuestas = Cliente.ListaTermanRespuestasCandidatos(number_prueba);


            for (int i = 0; i < ListaRespuestas.ToList().Count; i++)
            {

                if (ListaRespuestas[i].Serie == 4)
                {

                    DataRow row = dtRespuestasCandidato.NewRow();

                    row["IdRespuesta"] = Convert.ToInt32(ListaRespuestas[i].IdRespuestaCandidato);
                    row["IdPregunta"] = Convert.ToString(ListaRespuestas[i].Idpregunta);
                    row["punto"] = Convert.ToString(ListaRespuestas[i].Punto);
                    row["Respuesta"] = Convert.ToString(ListaRespuestas[i].Respuesta);

                    dtRespuestasCandidato.Rows.Add(row);

                }

            }


            Recorrido = dtRespuestasCandidato.Rows.Count;

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtPreguntas.Rows.Count;

            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }
            x--;

            MostrarPreguntas();

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Name == "Finalizar")
            {
                btnSiguiente.Text = "Siguiente";
                this.btnSiguiente.Name = "Siguiente";

            }

            btnSiguiente.Name = "Actualizar";
            btnSiguiente.Enabled = true;

        }


        int contarCheck = 0;
        private void ValidarCheckBox()
        {


            if (contarCheck == 2)
            {
                btnSiguiente.Enabled = true;
                lblMsj.Text = "";
            }
            else if (contarCheck < 2)
            {
                btnSiguiente.Enabled = false;
                lblMsj.Text = "";

            }
            else if (contarCheck > 2)
            {

                lblMsj.Text = "Seleccione solamente 2 opciones";
                btnSiguiente.Enabled = false;

            }


        }


        #region Validacion de CheckBox


        private void chkOpcion1_CheckedChanged(object sender, EventArgs e)
        {

            if (chkOpcion1.Checked == true)
            {

                contarCheck = contarCheck + 1;
            }
            else
            {
                contarCheck = contarCheck - 1;
            }

            ValidarCheckBox();
        }

        private void chkOpcion2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOpcion2.Checked == true)
            {

                contarCheck = contarCheck + 1;
            }
            else
            {
                contarCheck = contarCheck - 1;
            }

            ValidarCheckBox();
        }

        private void chkOpcion3_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOpcion3.Checked == true)
            {

                contarCheck = contarCheck + 1;
            }
            else
            {
                contarCheck = contarCheck - 1;
            }

            ValidarCheckBox();
        }

        private void chkOpcion4_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOpcion4.Checked == true)
            {

                contarCheck = contarCheck + 1;
            }
            else
            {
                contarCheck = contarCheck - 1;
            }
            ValidarCheckBox();

        }

        private void chkOpcion5_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOpcion5.Checked == true)
            {

                contarCheck = contarCheck + 1;
            }
            else
            {
                contarCheck = contarCheck - 1;
            }
            ValidarCheckBox();
        }




        #endregion

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            //this.Size = new Size(686, 381);
            this.btnIniciarAyuda.Hide();
        }

        private void frmTermanSerieIV_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }




    }
}
