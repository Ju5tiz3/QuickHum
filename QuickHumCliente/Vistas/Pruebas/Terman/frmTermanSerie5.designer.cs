﻿namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    partial class frmTermanSerie5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTermanSerie5));
            this.label3 = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lbltexto = new System.Windows.Forms.Label();
            this.txtrespuesta = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelAyuda.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(594, 36);
            this.label3.TabIndex = 78;
            this.label3.Text = "Serie V Terman";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.MinimumSize = new System.Drawing.Size(594, 422);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(594, 422);
            this.panelAyuda.TabIndex = 79;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnCerrarAyuda);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnIniciarAyuda);
            this.panel1.Location = new System.Drawing.Point(40, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 366);
            this.panel1.TabIndex = 75;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarAyuda.Image")));
            this.btnCerrarAyuda.Location = new System.Drawing.Point(479, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 81;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Calibri", 12F);
            this.label1.Location = new System.Drawing.Point(12, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(485, 187);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(514, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 12F);
            this.groupBox2.Location = new System.Drawing.Point(17, 231);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(480, 61);
            this.groupBox2.TabIndex = 74;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F);
            this.label4.Location = new System.Drawing.Point(258, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Manzanas";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 14F);
            this.textBox1.Location = new System.Drawing.Point(152, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 30);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "18";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.Color.White;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(200, 311);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 36);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 15F);
            this.label5.Location = new System.Drawing.Point(127, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 24);
            this.label5.TabIndex = 81;
            this.label5.Text = "Respuesta:";
            // 
            // lbltexto
            // 
            this.lbltexto.AutoSize = true;
            this.lbltexto.Font = new System.Drawing.Font("Calibri", 15F);
            this.lbltexto.Location = new System.Drawing.Point(357, 143);
            this.lbltexto.Name = "lbltexto";
            this.lbltexto.Size = new System.Drawing.Size(95, 24);
            this.lbltexto.TabIndex = 2;
            this.lbltexto.Text = "Manzanas";
            // 
            // txtrespuesta
            // 
            this.txtrespuesta.Font = new System.Drawing.Font("Calibri", 16F);
            this.txtrespuesta.Location = new System.Drawing.Point(244, 139);
            this.txtrespuesta.Name = "txtrespuesta";
            this.txtrespuesta.Size = new System.Drawing.Size(100, 34);
            this.txtrespuesta.TabIndex = 80;
            this.txtrespuesta.TextChanged += new System.EventHandler(this.txtrespuesta_TextChanged);
            this.txtrespuesta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrespuesta_KeyPress);
            // 
            // lbl1
            // 
            this.lbl1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl1.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl1.Location = new System.Drawing.Point(16, 19);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(549, 83);
            this.lbl1.TabIndex = 79;
            this.lbl1.Text = "label4";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Location = new System.Drawing.Point(170, 13);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 86;
            this.lbltime.Text = "label9";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(48, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 85;
            this.label7.Text = "Tiempo restante:";
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.Color.White;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(306, 257);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 91;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.Color.White;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(33, 257);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 90;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(434, 257);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 89;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.Image")));
            this.btnCloseWindow.Location = new System.Drawing.Point(560, 1);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 92;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbPreguntas.BackColor = System.Drawing.Color.White;
            this.gbPreguntas.Controls.Add(this.label5);
            this.gbPreguntas.Controls.Add(this.lbl1);
            this.gbPreguntas.Controls.Add(this.lbltexto);
            this.gbPreguntas.Controls.Add(this.txtrespuesta);
            this.gbPreguntas.Controls.Add(this.btnAnterior);
            this.gbPreguntas.Controls.Add(this.btnAyuda);
            this.gbPreguntas.Controls.Add(this.btnSiguiente);
            this.gbPreguntas.Location = new System.Drawing.Point(8, 95);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(577, 313);
            this.gbPreguntas.TabIndex = 93;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.lbltime);
            this.panel3.Location = new System.Drawing.Point(346, 42);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(248, 44);
            this.panel3.TabIndex = 0;
            // 
            // frmTermanSerie5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(594, 458);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.gbPreguntas);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "frmTermanSerie5";
            this.Text = "Serie V Terman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTermanSerie5_FormClosing);
            this.Load += new System.EventHandler(this.frmTermanSerie5_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbltexto;
        private System.Windows.Forms.TextBox txtrespuesta;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Panel panel3;
    }
}