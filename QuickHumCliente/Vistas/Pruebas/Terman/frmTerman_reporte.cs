﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.TERMAN
{
    public partial class frmTerman_reporte : Form
    {

        QuickHumClient cliente = Globales.cliente;

        DataTable dtresultados = new DataTable();
        DataTable dtedadmental = new DataTable();
        DataTable dtcoeficiente = new DataTable();
        DataTable dtCandidato = new DataTable();
        int punto = 0, Suma = 0; string interpretacion = ""; string serie = "", orden;
        int edadmental = 0; double CI = 0; int coeficiente = 0;
        DataRow[] filtro; string expresion; string aprendizaje = ""; string rango = "";
        private Expediente _expediente = null;
        public frmTerman_reporte(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }
        private void frmTerman_reporte_Load(object sender, EventArgs e)
        {
               dto_terman_edadmental[] edadmental  = cliente.ListarEdadMentalTerman();
               dto_terman_coeficiente[] coeficiente = cliente.ListarCoeficienteTerman();
            dtresultados.Columns.Add("Punto");
            dtresultados.Columns.Add("Serie");
            dtresultados.Columns.Add("Orden");
            dtresultados.Columns.Add("Factor");

            dtedadmental.Columns.Add("idedad");
            dtedadmental.Columns.Add("puntos");
            dtedadmental.Columns.Add("edad");

            dtcoeficiente.Columns.Add("idcoeficiente");
            dtcoeficiente.Columns.Add("puntuacion");
            dtcoeficiente.Columns.Add("coeficiente");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            for (int z = 0; z < edadmental.Length; z++)
            {
                DataRow row = dtedadmental.NewRow();
                row ["idedad"] = Convert.ToInt32(edadmental[z].Idedadmental);
                row["puntos"] = Convert.ToInt32(edadmental[z].Puntos);
                row["edad"] = Convert.ToInt32(edadmental[z].EdadMental);
                dtedadmental.Rows.Add(row);
            }

            for (int w = 0; w < coeficiente.Length; w++)
            {
                DataRow row = dtcoeficiente.NewRow();
                row["idcoeficiente"] = Convert.ToInt32(coeficiente[w].IdCoeficiente);
                row["puntuacion"] = Convert.ToInt32(coeficiente[w].Puntaje);
                row["coeficiente"] = Convert.ToInt32(coeficiente[w].Coeficiente);
                dtcoeficiente.Rows.Add(row);
            }

            CargarReporte();

        }

        private void CargarReporte()
        {
            Suma = 0;
            dtresultados.Clear();
            dto_terman_resultados[] resultado = cliente.ListarTermanResultados(_expediente.Id);

            for (int x = 0; x < resultado.Length; x++)
            {
                DataRow row = dtresultados.NewRow();

                switch (resultado[x].Serie)
                {
                    case 1:
                        orden = "A";
                        serie = "Información";
                        punto = resultado[x].Total;
                        break;

                    case 2:
                        orden = "B";
                        serie = "Juicio";
                        punto = resultado[x].Total * 2;
                        break;

                    case 3:
                        orden = "C";
                        serie = "Vocabulario";
                        punto = resultado[x].Total;
                        break;

                    case 4:
                        orden = "D";
                        serie = "Síntesis";
                        punto = resultado[x].Total;
                        break;

                    case 5:
                        orden = "E";
                        serie = "Concentración";
                        punto = resultado[x].Total * 2;
                        break;

                    case 6:
                        orden = "F";
                        serie = "Análisis";
                        punto = resultado[x].Total;
                        break;

                    case 7:
                        orden = "G";
                        serie = "Abstracción";
                        punto = resultado[x].Total;
                        break;

                    case 8:
                        orden = "H";
                        serie = "Planeación";
                        punto = resultado[x].Total;
                        break;

                    case 9:
                        orden = "I";
                        serie = "Organización";
                        punto = resultado[x].Total;
                        break;

                    case 10:
                        orden = "J";
                        serie = "Atención";
                        punto = resultado[x].Total * 2;
                        break;

                }
                Suma = Suma + punto;
                row["Punto"] = punto;
                row["Orden"] = orden;
                row["Factor"] = serie;
                row["Serie"] = Convert.ToInt32(resultado[x].Serie);
                dtresultados.Rows.Add(row);
            }



            if (Suma <= 4)
            { Suma = 5; }

            expresion = ("puntos= '" + Suma + "'");
            filtro = dtedadmental.Select(expresion);
            edadmental = Convert.ToInt32(filtro[0]["edad"].ToString());


            CI = edadmental;
            CI /= (192); CI = Convert.ToInt32(CI *= 100);


            expresion = ("puntuacion = '" + CI + "'");
            filtro = dtcoeficiente.Select(expresion);
            coeficiente = Convert.ToInt32(filtro[0]["coeficiente"].ToString());




            if (Suma > 0 && Suma <= 69)
            { rango = "Inferior"; }
            else if (Suma >= 70 && Suma <= 79)
            { rango = "Inferior termino medio"; }
            else if (Suma >= 80 && Suma <= 89)
            { rango = "Termino medio bajo"; }
            else if (Suma >= 90 && Suma <= 109)
            { rango = "Termino medio"; }
            else if (Suma >= 110 && Suma <= 119)
            { rango = "Termino medio alto"; }
            else if (Suma >= 120 && Suma <= 139)
            { rango = "Superior "; }
            else if (Suma >= 140)
            { rango = "Sobresaliente"; }




            if (Suma <= 94)
            { aprendizaje = "Deficiente"; }

            else if (Suma <= 101 && Suma >= 95)
            { aprendizaje = "Inferior"; }

            else if (Suma <= 122 && Suma >= 102)
            { aprendizaje = "Termino medio bajo"; }

            else if (Suma <= 136 && Suma >= 123)
            { aprendizaje = "Normal"; }

            else if (Suma <= 150 && Suma >= 137)
            { aprendizaje = "Termino medio alto"; }

            else if (Suma <= 171 && Suma >= 151)
            { aprendizaje = "Superior"; }

            else if (Suma >= 172)
            { aprendizaje = "Sobresaliente"; }

            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportParameter[] parametro = new ReportParameter[5];
            parametro[0] = new ReportParameter("Edad", Convert.ToString(edadmental));
            parametro[1] = new ReportParameter("Coeficiente", Convert.ToString(coeficiente));
            parametro[2] = new ReportParameter("Aprendizaje", aprendizaje);
            parametro[3] = new ReportParameter("Rango", rango);
            parametro[4] = new ReportParameter("Suma", Convert.ToString(Suma));
            ReportDataSource rds = new ReportDataSource("DataSetTerman", dtresultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Terman.reporteTerman.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.LocalReport.SetParameters(parametro);
            reportViewer1.RefreshReport();

        }

       
    }
}
