﻿namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    partial class frmTermanSerie8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTermanSerie8));
            this.label3 = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.btnCerrar_ayuda = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtA = new System.Windows.Forms.RadioButton();
            this.rbtB = new System.Windows.Forms.RadioButton();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.panelAyuda.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(579, 36);
            this.label3.TabIndex = 85;
            this.label3.Text = "Serie VIII Terman";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelAyuda
            // 
            this.panelAyuda.Controls.Add(this.panel1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(579, 423);
            this.panelAyuda.TabIndex = 94;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btnCerrar_ayuda);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnIniciarAyuda);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(20, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(537, 369);
            this.panel1.TabIndex = 78;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Controls.Add(this.radioButton2);
            this.panel3.Controls.Add(this.radioButton3);
            this.panel3.Enabled = false;
            this.panel3.Location = new System.Drawing.Point(413, 168);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(107, 40);
            this.panel3.TabIndex = 78;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.Checked = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton2.Location = new System.Drawing.Point(9, 11);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(35, 20);
            this.radioButton2.TabIndex = 74;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "V";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.Enabled = false;
            this.radioButton3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton3.Location = new System.Drawing.Point(63, 11);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(35, 20);
            this.radioButton3.TabIndex = 71;
            this.radioButton3.Text = "F";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // btnCerrar_ayuda
            // 
            this.btnCerrar_ayuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar_ayuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrar_ayuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrar_ayuda.FlatAppearance.BorderSize = 0;
            this.btnCerrar_ayuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrar_ayuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar_ayuda.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar_ayuda.Image")));
            this.btnCerrar_ayuda.Location = new System.Drawing.Point(502, 0);
            this.btnCerrar_ayuda.Name = "btnCerrar_ayuda";
            this.btnCerrar_ayuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrar_ayuda.TabIndex = 85;
            this.btnCerrar_ayuda.UseVisualStyleBackColor = false;
            this.btnCerrar_ayuda.Visible = false;
            this.btnCerrar_ayuda.Click += new System.EventHandler(this.btnCerrar_ayuda_Click_1);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Calibri", 12F);
            this.label4.Location = new System.Drawing.Point(13, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(507, 77);
            this.label4.TabIndex = 62;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(537, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(202, 319);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 36);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.radioButton7);
            this.panel4.Controls.Add(this.radioButton8);
            this.panel4.Enabled = false;
            this.panel4.Location = new System.Drawing.Point(413, 250);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(107, 40);
            this.panel4.TabIndex = 77;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.BackColor = System.Drawing.Color.Transparent;
            this.radioButton7.Enabled = false;
            this.radioButton7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton7.Location = new System.Drawing.Point(9, 10);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(35, 20);
            this.radioButton7.TabIndex = 73;
            this.radioButton7.Text = "V";
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.BackColor = System.Drawing.Color.Transparent;
            this.radioButton8.Checked = true;
            this.radioButton8.Enabled = false;
            this.radioButton8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton8.Location = new System.Drawing.Point(62, 10);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(35, 20);
            this.radioButton8.TabIndex = 72;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "F";
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(393, 174);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.rbtA);
            this.panel2.Controls.Add(this.rbtB);
            this.panel2.Font = new System.Drawing.Font("Calibri", 12F);
            this.panel2.Location = new System.Drawing.Point(23, 92);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(436, 43);
            this.panel2.TabIndex = 84;
            // 
            // rbtA
            // 
            this.rbtA.BackColor = System.Drawing.Color.Transparent;
            this.rbtA.Location = new System.Drawing.Point(91, 11);
            this.rbtA.Name = "rbtA";
            this.rbtA.Size = new System.Drawing.Size(97, 22);
            this.rbtA.TabIndex = 71;
            this.rbtA.TabStop = true;
            this.rbtA.Text = "V";
            this.rbtA.UseVisualStyleBackColor = false;
            this.rbtA.Click += new System.EventHandler(this.rbtA_Click);
            // 
            // rbtB
            // 
            this.rbtB.BackColor = System.Drawing.Color.Transparent;
            this.rbtB.Location = new System.Drawing.Point(237, 11);
            this.rbtB.Name = "rbtB";
            this.rbtB.Size = new System.Drawing.Size(97, 22);
            this.rbtB.TabIndex = 70;
            this.rbtB.Text = "F";
            this.rbtB.UseVisualStyleBackColor = false;
            this.rbtB.Click += new System.EventHandler(this.rbtB_Click);
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbl1.Location = new System.Drawing.Point(20, 21);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(439, 55);
            this.lbl1.TabIndex = 79;
            this.lbl1.Text = "label4";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbltime.Location = new System.Drawing.Point(166, 12);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 97;
            this.lbltime.Text = "label9";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(45, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 96;
            this.label7.Text = "Tiempo restante:";
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(248, 210);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 101;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(12, 210);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 100;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(381, 210);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 99;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.Image")));
            this.btnCloseWindow.Location = new System.Drawing.Point(543, 1);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 86;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.lbltime);
            this.panel5.Location = new System.Drawing.Point(323, 36);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(242, 45);
            this.panel5.TabIndex = 102;
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.gbPreguntas);
            this.panel6.Controls.Add(this.btnSiguiente);
            this.panel6.Controls.Add(this.btnAyuda);
            this.panel6.Controls.Add(this.btnAnterior);
            this.panel6.Location = new System.Drawing.Point(29, 87);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(506, 256);
            this.panel6.TabIndex = 103;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.panel2);
            this.gbPreguntas.Controls.Add(this.lbl1);
            this.gbPreguntas.Location = new System.Drawing.Point(12, 12);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(479, 163);
            this.gbPreguntas.TabIndex = 102;
            // 
            // frmTermanSerie8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(579, 459);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(581, 461);
            this.Name = "frmTermanSerie8";
            this.Text = "Serie VIII Terman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTermanSerie8_FormClosing);
            this.Load += new System.EventHandler(this.frmTermanSerie8_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.gbPreguntas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtA;
        private System.Windows.Forms.RadioButton rbtB;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Button btnCerrar_ayuda;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel gbPreguntas;
    }
}