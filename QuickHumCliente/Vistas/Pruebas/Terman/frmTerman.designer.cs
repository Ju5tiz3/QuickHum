﻿namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    partial class frmTerman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTerman));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCloseWindows = new System.Windows.Forms.Button();
            this.btnfinalizar = new System.Windows.Forms.Button();
            this.btnPrueba10 = new System.Windows.Forms.Button();
            this.btnPrueba9 = new System.Windows.Forms.Button();
            this.btnPrueba8 = new System.Windows.Forms.Button();
            this.btnPrueba7 = new System.Windows.Forms.Button();
            this.btnPrueba5 = new System.Windows.Forms.Button();
            this.btnPrueba3 = new System.Windows.Forms.Button();
            this.btnPrueba2 = new System.Windows.Forms.Button();
            this.btnPrueba6 = new System.Windows.Forms.Button();
            this.btnPrueba4 = new System.Windows.Forms.Button();
            this.btnPrueba1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 12F);
            this.richTextBox1.Location = new System.Drawing.Point(15, 47);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(479, 330);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Location = new System.Drawing.Point(12, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(512, 396);
            this.panel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(512, 36);
            this.label2.TabIndex = 62;
            this.label2.Text = "INSTRUCCIONES GENERALES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(860, 36);
            this.label3.TabIndex = 67;
            this.label3.Text = "TEST DE INTELIGENCIA DE TERMAN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCloseWindows
            // 
            this.btnCloseWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindows.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindows.FlatAppearance.BorderSize = 0;
            this.btnCloseWindows.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindows.Image = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCloseWindows.Location = new System.Drawing.Point(825, 0);
            this.btnCloseWindows.Name = "btnCloseWindows";
            this.btnCloseWindows.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindows.TabIndex = 78;
            this.btnCloseWindows.UseVisualStyleBackColor = false;
            this.btnCloseWindows.Click += new System.EventHandler(this.btnCloseWindows_Click);
            // 
            // btnfinalizar
            // 
            this.btnfinalizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnfinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnfinalizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnfinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfinalizar.Font = new System.Drawing.Font("Calibri", 15F);
            this.btnfinalizar.ForeColor = System.Drawing.Color.White;
            this.btnfinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinalizar.Location = new System.Drawing.Point(552, 402);
            this.btnfinalizar.Name = "btnfinalizar";
            this.btnfinalizar.Size = new System.Drawing.Size(294, 45);
            this.btnfinalizar.TabIndex = 10;
            this.btnfinalizar.Text = "Finalizar Prueba";
            this.btnfinalizar.UseVisualStyleBackColor = false;
            this.btnfinalizar.Click += new System.EventHandler(this.btnfinalizar_Click);
            // 
            // btnPrueba10
            // 
            this.btnPrueba10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba10.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba10.ForeColor = System.Drawing.Color.White;
            this.btnPrueba10.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba10.Image")));
            this.btnPrueba10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba10.Location = new System.Drawing.Point(702, 286);
            this.btnPrueba10.Name = "btnPrueba10";
            this.btnPrueba10.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba10.TabIndex = 9;
            this.btnPrueba10.Text = "Prueba 10";
            this.btnPrueba10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba10.UseVisualStyleBackColor = false;
            this.btnPrueba10.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba9
            // 
            this.btnPrueba9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba9.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba9.ForeColor = System.Drawing.Color.White;
            this.btnPrueba9.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba9.Image")));
            this.btnPrueba9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba9.Location = new System.Drawing.Point(552, 286);
            this.btnPrueba9.Name = "btnPrueba9";
            this.btnPrueba9.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba9.TabIndex = 8;
            this.btnPrueba9.Text = "Prueba 9";
            this.btnPrueba9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba9.UseVisualStyleBackColor = false;
            this.btnPrueba9.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba8
            // 
            this.btnPrueba8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba8.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba8.ForeColor = System.Drawing.Color.White;
            this.btnPrueba8.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba8.Image")));
            this.btnPrueba8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba8.Location = new System.Drawing.Point(702, 225);
            this.btnPrueba8.Name = "btnPrueba8";
            this.btnPrueba8.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba8.TabIndex = 7;
            this.btnPrueba8.Text = "Prueba 8";
            this.btnPrueba8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba8.UseVisualStyleBackColor = false;
            this.btnPrueba8.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba7
            // 
            this.btnPrueba7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba7.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba7.ForeColor = System.Drawing.Color.White;
            this.btnPrueba7.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba7.Image")));
            this.btnPrueba7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba7.Location = new System.Drawing.Point(552, 225);
            this.btnPrueba7.Name = "btnPrueba7";
            this.btnPrueba7.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba7.TabIndex = 6;
            this.btnPrueba7.Text = "Prueba 7";
            this.btnPrueba7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba7.UseVisualStyleBackColor = false;
            this.btnPrueba7.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba5
            // 
            this.btnPrueba5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba5.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba5.ForeColor = System.Drawing.Color.White;
            this.btnPrueba5.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba5.Image")));
            this.btnPrueba5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba5.Location = new System.Drawing.Point(552, 168);
            this.btnPrueba5.Name = "btnPrueba5";
            this.btnPrueba5.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba5.TabIndex = 5;
            this.btnPrueba5.Text = "Prueba 5";
            this.btnPrueba5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba5.UseVisualStyleBackColor = false;
            this.btnPrueba5.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba3
            // 
            this.btnPrueba3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba3.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba3.ForeColor = System.Drawing.Color.White;
            this.btnPrueba3.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba3.Image")));
            this.btnPrueba3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba3.Location = new System.Drawing.Point(552, 109);
            this.btnPrueba3.Name = "btnPrueba3";
            this.btnPrueba3.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba3.TabIndex = 4;
            this.btnPrueba3.Text = "Prueba 3";
            this.btnPrueba3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba3.UseVisualStyleBackColor = false;
            this.btnPrueba3.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba2
            // 
            this.btnPrueba2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba2.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba2.ForeColor = System.Drawing.Color.White;
            this.btnPrueba2.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba2.Image")));
            this.btnPrueba2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba2.Location = new System.Drawing.Point(702, 54);
            this.btnPrueba2.Name = "btnPrueba2";
            this.btnPrueba2.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba2.TabIndex = 3;
            this.btnPrueba2.Text = "Prueba 2";
            this.btnPrueba2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba2.UseVisualStyleBackColor = false;
            this.btnPrueba2.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba6
            // 
            this.btnPrueba6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba6.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba6.ForeColor = System.Drawing.Color.White;
            this.btnPrueba6.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba6.Image")));
            this.btnPrueba6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba6.Location = new System.Drawing.Point(702, 168);
            this.btnPrueba6.Name = "btnPrueba6";
            this.btnPrueba6.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba6.TabIndex = 2;
            this.btnPrueba6.Text = "Prueba 6";
            this.btnPrueba6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba6.UseVisualStyleBackColor = false;
            this.btnPrueba6.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba4
            // 
            this.btnPrueba4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba4.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba4.ForeColor = System.Drawing.Color.White;
            this.btnPrueba4.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba4.Image")));
            this.btnPrueba4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba4.Location = new System.Drawing.Point(702, 109);
            this.btnPrueba4.Name = "btnPrueba4";
            this.btnPrueba4.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba4.TabIndex = 1;
            this.btnPrueba4.Text = "Prueba 4";
            this.btnPrueba4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba4.UseVisualStyleBackColor = false;
            this.btnPrueba4.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // btnPrueba1
            // 
            this.btnPrueba1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrueba1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnPrueba1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrueba1.Font = new System.Drawing.Font("Calibri", 14F);
            this.btnPrueba1.ForeColor = System.Drawing.Color.White;
            this.btnPrueba1.Image = ((System.Drawing.Image)(resources.GetObject("btnPrueba1.Image")));
            this.btnPrueba1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrueba1.Location = new System.Drawing.Point(552, 54);
            this.btnPrueba1.Name = "btnPrueba1";
            this.btnPrueba1.Size = new System.Drawing.Size(144, 48);
            this.btnPrueba1.TabIndex = 0;
            this.btnPrueba1.Text = "Prueba 1";
            this.btnPrueba1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrueba1.UseVisualStyleBackColor = false;
            this.btnPrueba1.Click += new System.EventHandler(this.SeleccionPruebaTerman);
            // 
            // frmTerman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(860, 459);
            this.ControlBox = false;
            this.Controls.Add(this.btnCloseWindows);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnfinalizar);
            this.Controls.Add(this.btnPrueba10);
            this.Controls.Add(this.btnPrueba9);
            this.Controls.Add(this.btnPrueba8);
            this.Controls.Add(this.btnPrueba7);
            this.Controls.Add(this.btnPrueba5);
            this.Controls.Add(this.btnPrueba3);
            this.Controls.Add(this.btnPrueba2);
            this.Controls.Add(this.btnPrueba6);
            this.Controls.Add(this.btnPrueba4);
            this.Controls.Add(this.btnPrueba1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(605, 432);
            this.Name = "frmTerman";
            this.Text = "Test de inteligencia Terman";
            this.Load += new System.EventHandler(this.frmTerman_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnPrueba1;
        public System.Windows.Forms.Button btnPrueba4;
        public System.Windows.Forms.Button btnPrueba6;
        public System.Windows.Forms.Button btnPrueba2;
        public System.Windows.Forms.Button btnPrueba3;
        public System.Windows.Forms.Button btnPrueba5;
        public System.Windows.Forms.Button btnPrueba7;
        public System.Windows.Forms.Button btnPrueba8;
        public System.Windows.Forms.Button btnPrueba9;
        public System.Windows.Forms.Button btnPrueba10;
        public System.Windows.Forms.Button btnfinalizar;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCloseWindows;
    }
}