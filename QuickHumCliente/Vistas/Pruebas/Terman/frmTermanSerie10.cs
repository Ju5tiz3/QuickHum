﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie10 : Form
    {
        QuickHumClient cliente = Globales.cliente;
        dto_terman_respuestas_candidato respcandidato = new dto_terman_respuestas_candidato();
        dto_terman_respuestas_candidato[] listarrespuesta;
        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtactualizar = new DataTable();
        DataRow[] row;
        int x = 0, punto = 0, recorrido = 0;
        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        string Respuesta;
        private int number_prueba = frmTerman.numero_pruebaTerman;

        public frmTermanSerie10()
        {
            InitializeComponent();
        }

        private void frmTermanSerie10_Load(object sender, EventArgs e)
        {
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_pregunta");
            dtactualizar.Columns.Add("respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("serie");
            dtactualizar.Columns.Add("numero_prueba");


            //this.Size = new Size(562, 340);
            this.panelAyuda.Location = new Point(1, 1);
            listarDatos();
        }

        private void listarDatos()
        {
            dto_terman_preguntas[] ListadoPreguntas = cliente.ListadoTermanPreguntas();

            dtpreguntas.Columns.Add("id_pregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("respuesta_correcta");
            dtpreguntas.Columns.Add("serie");



            for (int i = 0; i < ListadoPreguntas.Length; i++)
            {
                if (ListadoPreguntas[i].Serie == 10)
                {
                    DataRow row = dtpreguntas.NewRow();

                    row["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].Idpregunta);
                    row["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);
                    row["respuesta_correcta"] = Convert.ToString(ListadoPreguntas[i].Respuesta_Correcta);
                    row["serie"] = Convert.ToInt32(ListadoPreguntas[i].Serie);


                    dtpreguntas.Rows.Add(row);
                }

            }

            lbl1.Text = "1) " + dtpreguntas.Rows[0]["pregunta"].ToString() + ":";
            lbl1.Name = dtpreguntas.Rows[0]["id_pregunta"].ToString();


            DateTime hora;
            hora = Convert.ToDateTime("0:04:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void MostrarPreguntas(int i)
        {
            lbl1.Text = i + 1 + ") " + dtpreguntas.Rows[i]["pregunta"].ToString();

            lbl1.Name = dtpreguntas.Rows[i]["id_pregunta"].ToString();


        }

        private void Evaluar()
        {
            if (dtpreguntas.Rows[x - 1]["respuesta_correcta"].ToString() == txtA.Text.Trim() + "-" + txtB.Text.Trim())
            {
                punto = 1;
            }
            else
            {
                punto = 0;
            }
            Respuesta = txtA.Text.Trim() + "-" + txtB.Text.Trim();
        }

        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Punto = Convert.ToInt32(punto);
            respcandidato.Respuesta = Convert.ToString(Respuesta);
            respcandidato.Serie = 10;
            respcandidato.Numero_prueba = number_prueba;
            cliente.InsertaTermanRespuesta(respcandidato);

        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[x - 1]["id_respuesta_candidato"].ToString());
            respcandidato.Idpregunta = Convert.ToInt32(lbl1.Name);
            respcandidato.Punto = punto;
            respcandidato.Respuesta = Convert.ToString(Respuesta);
            cliente.ActualizaTermanRespuesta(respcandidato);

        }

        private void Retroceder()
        {
            string[] separar = dtactualizar.Rows[x]["respuesta"].ToString().Split('-');

            txtA.Text = separar[0];
            txtB.Text = separar[1];
        }

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if (txtA.Text.Trim().Length == 0 || txtB.Text.Trim().Length == 0)
                {

                }
                else
                {
                    x++; GuardarRespuestas();
                }
                this.Dispose();
            }

        }

        private void LimpiarTextbox()
        {
            txtA.Clear();
            txtB.Clear();
        }

        private void Validacion()
        {
            if (txtA.Text.Trim().Length == 0 || txtB.Text.Trim().Length == 0)
            {
                btnSiguiente.Enabled = false;

            }
            else
            {
                btnSiguiente.Enabled = true;
            }
        }

        public void Numeros(object sender, KeyPressEventArgs e)
        {

        }

        private void txtA_TextChanged(object sender, EventArgs e)
        {
            Validacion();

        }

        private void txtB_TextChanged(object sender, EventArgs e)
        {
            Validacion();
        }

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrar_ayuda.Show();
            this.btnIniciarAyuda.Hide();
            btnCerrar_ayuda.Visible = true;
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            tiempo.Enabled = true;
        }

        private void btnCerrar_ayuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                MostrarPreguntas(x);
                btnSiguiente.Enabled = false;
                LimpiarTextbox();
                txtA.Focus();

            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                MostrarPreguntas(x);
                if (recorrido > x)
                {
                    Retroceder();
                }
                if (recorrido <= x)
                {
                    btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    LimpiarTextbox();
                }
                txtA.Focus();
            }

            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }
            }
            if (x == dtpreguntas.Rows.Count - 1)
            {
                btnSiguiente.Text = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }
            btnAnterior.Enabled = true;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            listarrespuesta = cliente.ListaTermanRespuestasCandidatos(number_prueba);

            dtactualizar.Clear();
            for (int w = 0; w < listarrespuesta.Length; w++)
            {
                if (listarrespuesta[w].Serie == 10)
                {
                    DataRow row = dtactualizar.NewRow();
                    row["id_respuesta_candidato"] = Convert.ToInt32(listarrespuesta[w].IdRespuestaCandidato);
                    row["id_pregunta"] = Convert.ToInt32(listarrespuesta[w].Idpregunta);
                    row["respuesta"] = Convert.ToString(listarrespuesta[w].Respuesta);
                    row["punto"] = Convert.ToInt32(listarrespuesta[w].Punto);
                    row["serie"] = Convert.ToInt32(listarrespuesta[w].Serie);
                    row["numero_prueba"] = Convert.ToInt32(listarrespuesta[w].Numero_prueba);
                    dtactualizar.Rows.Add(row);
                }
            }
            recorrido = dtactualizar.Rows.Count;
            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }
            x--;
            btnSiguiente.Text = "Siguiente.";
            MostrarPreguntas(x);
            if (recorrido > x)
            {
                Retroceder();
            }



            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";

            }
            btnSiguiente.Enabled = true;
        }

        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            string caracter = " 1234567890/.";

            if (!(caracter.IndexOf(e.KeyChar) >= 0) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            string caracter = " 1234567890/.";

            if (!(caracter.IndexOf(e.KeyChar) >= 0) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void frmTermanSerie10_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void btnCerrar_ayuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtB_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            string caracter = " 1234567890/.";

            if (!(caracter.IndexOf(e.KeyChar) >= 0) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void txtB_TextChanged_1(object sender, EventArgs e)
        {
            Validacion();
        }

        private void txtA_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            string caracter = " 1234567890/.";

            if (!(caracter.IndexOf(e.KeyChar) >= 0) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void txtA_TextChanged_1(object sender, EventArgs e)
        {
            Validacion();
        }
    }
}
