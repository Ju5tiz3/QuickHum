﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTermanSerie9 : Form
    {

        int Numero_prueba = 1;

        DataTable dtPreguntas = new DataTable();
        DataTable dtRespuestas = new DataTable();
        DataTable dtRespuestasCandidato = new DataTable();
        private int number_prueba = frmTerman.numero_pruebaTerman;
        int x = 0;
        string RespuestaCorrecta = "";
        int Recorrido = 0;
        DataRow[] row;
        string Respuesta = "";

        QuickHumClient Cliente = Globales.cliente;
        dto_terman_respuestas_candidato Respuestas = new dto_terman_respuestas_candidato();
        dto_terman_preguntas[] ListaPreguntas;

        public frmTermanSerie9()
        {
            InitializeComponent();
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.lblNumPreg.Text = "Pregunta: 1 de 18";
            this.panelAyuda.Hide();
            this.Size = new Size(544, 487);
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
            MostrarPreguntas();

        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
            //this.Size = new Size(710, 515);
        }

        private void frmTermanSerieIX_Load(object sender, EventArgs e)
        {
            btnSiguiente.Name = "Siguiente";

            //Estructuramos los campos de los DataTables
            dtPreguntas.Columns.Add("IdPregunta");
            dtPreguntas.Columns.Add("Pregunta");
            dtPreguntas.Columns.Add("RespuestaCorrecta");

            dtRespuestas.Columns.Add("IdRespuesta");
            dtRespuestas.Columns.Add("IdPregunta");
            dtRespuestas.Columns.Add("Respuesta");

            dtRespuestasCandidato.Columns.Add("IdRespuesta");
            dtRespuestasCandidato.Columns.Add("IdPregunta");
            dtRespuestasCandidato.Columns.Add("punto");
            dtRespuestasCandidato.Columns.Add("Respuesta");

            DateTime hora;

            CargarPreguntasRespuestas();

            this.panelAyuda.Location = new Point(1, 1);
            //  this.lblMsj.Text = "";
            //this.Size = new Size(636, 489);
            hora = Convert.ToDateTime("0:02:00");
            // hora = Convert.ToDateTime("0:02:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void CargarPreguntasRespuestas()
        {
            ListaPreguntas = Cliente.ListadoTermanPreguntas();


            for (int i = 0; i < ListaPreguntas.ToArray().Count(); i++)
            {

                if (ListaPreguntas[i].Serie == 9)
                {
                    DataRow RowPregunta = dtPreguntas.NewRow();

                    RowPregunta["IdPregunta"] = ListaPreguntas[i].Idpregunta;
                    RowPregunta["Pregunta"] = ListaPreguntas[i].Pregunta;
                    RowPregunta["RespuestaCorrecta"] = ListaPreguntas[i].Respuesta_Correcta;

                    dtPreguntas.Rows.Add(RowPregunta);

                    for (int j = 0; j < 5; j++)
                    {

                        DataRow RowRespuestas = dtRespuestas.NewRow();

                        RowRespuestas["IdRespuesta"] = ListaPreguntas[i].Respuesta[j].Idrespuesta;
                        RowRespuestas["IdPregunta"] = ListaPreguntas[i].Respuesta[j].Idpregunta;
                        RowRespuestas["Respuesta"] = ListaPreguntas[i].Respuesta[j].Respuesta;

                        dtRespuestas.Rows.Add(RowRespuestas);

                    }

                }
            }
        }


        private void MostrarPreguntas()
        {
            limpiarCampos();

            //   txtPreguntas.Text = dtPreguntas.Rows[x][1].ToString();

            rbtOpcionA.Text = dtRespuestas.Rows[(x * 5)][2].ToString();
            rbtOpcionB.Text = dtRespuestas.Rows[(x * 5) + 1][2].ToString();
            rbtOpcionC.Text = dtRespuestas.Rows[(x * 5) + 2][2].ToString();
            rbtOpcionD.Text = dtRespuestas.Rows[(x * 5) + 3][2].ToString();
            rbtOpcionE.Text = dtRespuestas.Rows[(x * 5) + 4][2].ToString();



            RespuestaCorrecta = dtPreguntas.Rows[x][2].ToString();


            if (Recorrido > x)
            {


                if (rbtOpcionA.Text == dtRespuestasCandidato.Rows[x][3].ToString())
                {
                    rbtOpcionA.Checked = true;
                }
                else if (rbtOpcionB.Text == dtRespuestasCandidato.Rows[x][3].ToString())
                {
                    rbtOpcionB.Checked = true;
                }
                else if (rbtOpcionC.Text == dtRespuestasCandidato.Rows[x][3].ToString())
                {
                    rbtOpcionC.Checked = true;
                }
                else if (rbtOpcionD.Text == dtRespuestasCandidato.Rows[x][3].ToString())
                {
                    rbtOpcionD.Checked = true;
                }
                else if (rbtOpcionE.Text == dtRespuestasCandidato.Rows[x][3].ToString())
                {
                    rbtOpcionE.Checked = true;
                }



            }

        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Name == "Siguiente")
            {
                x++;
                guardarRespuestas();


                if (this.btnSiguiente.Name == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    return;
                }


                MostrarPreguntas();

                limpiarCampos();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Name == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {

                    btnSiguiente.Name = "Siguiente";
                    limpiarCampos();
                    btnSiguiente.Enabled = false;
                }

                MostrarPreguntas();


            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {

                    FinalizarPrueba();

                    tiempo.Enabled = false;
                    this.Dispose();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;


            if (x == dtPreguntas.Rows.Count - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
                btnSiguiente.BackColor = Color.FromArgb(203, 68, 58);
                btnSiguiente.Image = Properties.RecursosPruebas.timerStop;
            }


            btnAnterior.Enabled = true;
        }

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Name == "Siguiente" || btnSiguiente.Name == "Finalizar")
            {
                if (rbtOpcionA.Checked == true || rbtOpcionB.Checked == true || rbtOpcionC.Checked == true ||
                    rbtOpcionD.Checked == true || rbtOpcionE.Checked == true)
                {
                    x++; guardarRespuestas();
                }
                else
                {
                    this.Dispose();
                }
            }


        }


        private void limpiarCampos()
        {
            rbtOpcionA.Checked = false;
            rbtOpcionB.Checked = false;
            rbtOpcionC.Checked = false;
            rbtOpcionD.Checked = false;
            rbtOpcionE.Checked = false;

        }

        private void guardarRespuestas()
        {

            Respuestas.Numero_prueba = number_prueba;
            Respuestas.Idpregunta = Convert.ToInt32(dtPreguntas.Rows[x - 1][0]);
            Respuestas.Punto = ValidarRespuesta();
            Respuestas.Respuesta = Respuesta;
            Respuestas.Serie = 9;

            Cliente.InsertaTermanRespuesta(Respuestas);

        }

        private void ActualizarRespuestas()
        {

            Respuestas.IdRespuestaCandidato = Convert.ToInt32(dtRespuestasCandidato.Rows[x - 1][0]);
            Respuestas.Idpregunta = Convert.ToInt32(dtRespuestasCandidato.Rows[x - 1][1]);
            Respuestas.Numero_prueba = number_prueba;
            Respuestas.Punto = ValidarRespuesta();
            Respuestas.Respuesta = Respuesta;
            Respuestas.Serie = 9;

            Cliente.ActualizaTermanRespuesta(Respuestas);


        }

        private int ValidarRespuesta()
        {

            int Punto = 0;
            Respuesta = "";

            if (rbtOpcionA.Checked == true)
            {
                if (rbtOpcionA.Text == RespuestaCorrecta)
                {
                    Punto = 1;
                }
                Respuesta = rbtOpcionA.Text;
            }
            else if (rbtOpcionB.Checked == true)
            {
                if (rbtOpcionB.Text == RespuestaCorrecta)
                {
                    Punto = 1;
                }
                Respuesta = rbtOpcionB.Text;
            }
            else if (rbtOpcionC.Checked == true)
            {
                if (rbtOpcionC.Text == RespuestaCorrecta)
                {
                    Punto = 1;
                }
                Respuesta = rbtOpcionC.Text;
            }
            else if (rbtOpcionD.Checked == true)
            {
                if (rbtOpcionD.Text == RespuestaCorrecta)
                {
                    Punto = 1;
                }
                Respuesta = rbtOpcionD.Text;
            }
            else if (rbtOpcionE.Checked == true)
            {
                if (rbtOpcionE.Text == RespuestaCorrecta)
                {
                    Punto = 1;
                }
                Respuesta = rbtOpcionE.Text;
            }



            return Punto;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            //Limpiamos las respuestas anteriores.
            dtRespuestasCandidato.Clear();

            //Listamos las re`spuestas del candidato.
            dto_terman_respuestas_candidato[] ListaRespuestas = Cliente.ListaTermanRespuestasCandidatos(number_prueba);


            for (int i = 0; i < ListaRespuestas.ToList().Count; i++)
            {

                if (ListaRespuestas[i].Serie == 9)
                {

                    DataRow row = dtRespuestasCandidato.NewRow();

                    row["IdRespuesta"] = Convert.ToInt32(ListaRespuestas[i].IdRespuestaCandidato);
                    row["IdPregunta"] = Convert.ToString(ListaRespuestas[i].Idpregunta);
                    row["punto"] = Convert.ToString(ListaRespuestas[i].Punto);
                    row["Respuesta"] = Convert.ToString(ListaRespuestas[i].Respuesta);

                    dtRespuestasCandidato.Rows.Add(row);

                }

            }

            Recorrido = dtRespuestasCandidato.Rows.Count;

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtPreguntas.Rows.Count;

            ///////
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            }
            x--;

            MostrarPreguntas();

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Name == "Finalizar")
            {
                btnSiguiente.Text = "Siguiente";
                this.btnSiguiente.Name = "Siguiente";

            }

            btnSiguiente.Name = "Actualizar";
            btnSiguiente.Enabled = true;

        }


        #region Validar si hay un RadioButton seleccionado

        private void rbtOpcionA_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        private void rbtOpcionB_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        private void rbtOpcionC_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        private void rbtOpcionD_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        private void rbtOpcionE_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        #endregion

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Dispose();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
            this.Size = new Size(544, 565);

        }

        private void gbPreguntas_Enter(object sender, EventArgs e)
        {

        }

        private void frmTermanSerieIX_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
            this.Dispose();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
            this.Size = new Size(544, 487);
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }

}

