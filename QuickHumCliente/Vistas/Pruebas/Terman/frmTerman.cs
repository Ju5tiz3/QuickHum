﻿using System;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Terman
{
    public partial class frmTerman : Form
    {
        public static int numero_pruebaTerman;
        QuickHumClient cliente = Globales.cliente;
        ServicioQuickHum.TERMAN terman = new ServicioQuickHum.TERMAN();
        dto_terman_respuestas_candidato respcandidato = new dto_terman_respuestas_candidato();

        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;
        public frmTerman(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }
       
        private void SeleccionPruebaTerman(object sender, EventArgs e)
        {
            Button boton_remitente = (Button) sender;
            boton_remitente.Enabled = false;
            switch (boton_remitente.Name)
            {
                case "btnPrueba1":
                    new frmTermanSerie1().ShowDialog();
                    break;
                case "btnPrueba2":
                    new frmTermanSerie2().ShowDialog();
                    break;
                case "btnPrueba3":
                    new frmTermanSerie3().ShowDialog();
                    break;
                case "btnPrueba4":
                    new frmTermanSerie4().ShowDialog();
                    break;
                case "btnPrueba5":
                    new frmTermanSerie5().ShowDialog();
                    break;
                case "btnPrueba6":
                    new frmTermanSerie6().ShowDialog();
                    break;
                case "btnPrueba7":
                    new frmTermanSerie7().ShowDialog();
                    break;
                case "btnPrueba8":
                    new frmTermanSerie8().ShowDialog();
                    break;
                case "btnPrueba9":
                    new frmTermanSerie9().ShowDialog();
                    break;
                case "btnPrueba10":
                    new frmTermanSerie10().ShowDialog();
                    break;
            }
        }

        private void frmTerman_Load(object sender, EventArgs e)
        {
            try
            {
                terman = cliente.NuevoTerman(_expediente.Numero, _escolaridad_prueba);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Visible = false;
                this.Close();
                return;
            }
            numero_pruebaTerman = terman.IdPrueba;
           
            
        }

        private void btnfinalizar_Click(object sender, EventArgs e)
        {
            
            if (btnPrueba1.Enabled==false && btnPrueba2.Enabled==false && btnPrueba3.Enabled==false && btnPrueba4.Enabled== false && btnPrueba5.Enabled==false &&
                btnPrueba6.Enabled==false && btnPrueba7.Enabled==false && btnPrueba8.Enabled == false && btnPrueba9.Enabled==false && btnPrueba10.Enabled==false)
            {
               XtraMessageBox.Show("La Prueba se ha calificado","Prueba Terman",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                respcandidato.Numero_prueba = numero_pruebaTerman;
                cliente.FinalizaTermanPrueba(respcandidato);
                this.Close();
            }
            else
            {
                XtraMessageBox.Show("Debe Terminar todas las pruebas", "Prueba Terman", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseWindows_Click(object sender, EventArgs e)
        {
            if (
                XtraMessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Terman",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            respcandidato.Numero_prueba = numero_pruebaTerman;
            cliente.FinalizaTermanPrueba(respcandidato);
            this.Close();
        }
    }
}
