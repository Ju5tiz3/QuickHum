﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.PIPG
{
    public partial class frmPIPG : Form
    {

        ServicioQuickHum.PIPG model_PIPG;

        private QuickHumClient cliente = Globales.cliente;
        private bool prueba_inicada = false;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;

        dto_pipg_respuestas_candidato respcandidato = new dto_pipg_respuestas_candidato();

        readonly DataTable dtpreguntas = new DataTable();
        readonly DataTable dtrespuestas = new DataTable();
        readonly DataTable dtactualizar = new DataTable();
        int x = 0;
        int idmas = 0; int idmenos = 0; int punto = 0;
        int recorrido = 0;
        DataRow[] row, row1, row2, row3;
        int NumeroAsignacion;

        public frmPIPG(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad = escolaridad;
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            model_PIPG = new ServicioQuickHum.PIPG();
            try
            {
                model_PIPG = cliente.NuevoPipg(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_inicada = true;
            NumeroAsignacion = model_PIPG.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 38";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
        }

        private void frmPIPG_Load(object sender, EventArgs e)
        {
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_respuesta");
            dtactualizar.Columns.Add("punto");
            dtactualizar.Columns.Add("numero_prueba");

            //this.Size = new Size(679, 545);
            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";
            listardatos();

        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                ValidacionRAdioButtons();
                x++;
                guardarRespuestas();
                mostrarPreguntas(x);
                lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + Convert.ToInt32(dtpreguntas.Rows.Count - 114);
                limpiarcampos();
                btnSiguiente.Enabled = false;
            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + Convert.ToInt32(dtpreguntas.Rows.Count - 114);
                mostrarPreguntas(x);

                if (recorrido > (x * 2))
                {
                    CheckarRadiosadelante();
                }

                if (recorrido <= (x * 2))
                {
                    this.btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    limpiarcampos();
                }
            }

            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba P-IPG", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                {
                    x++;
                    guardarRespuestas();
                    respcandidato.NumeroPrueba = NumeroAsignacion;
                    cliente.FinalizaPipgPrueba(respcandidato);
                    this.Dispose();
                }
            }

            if (x == Convert.ToInt32(dtpreguntas.Rows.Count - 114) - 1)
            {
                btnSiguiente.Text = "Finalizar";
            }
            btnAnterior.Enabled = true;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_pipg_respuestas_candidato[] listarespcandidato = cliente.ListaPipgRespuestasCandidatos(NumeroAsignacion);

            dtactualizar.Clear();
            for (int w = 0; w < listarespcandidato.ToList().Count; w++)
            {
                DataRow filitas = dtactualizar.NewRow();

                filitas["id_respuesta_candidato"] = Convert.ToInt32(listarespcandidato[w].IdRespuestaCandidato);
                filitas["id_respuesta"] = Convert.ToInt32(listarespcandidato[w].IdRespuesta);
                filitas["punto"] = Convert.ToInt32(listarespcandidato[w].Punto);
                filitas["numero_prueba"] = Convert.ToInt32(listarespcandidato[w].NumeroPrueba);
                dtactualizar.Rows.Add(filitas);
            }

            recorrido = dtactualizar.Rows.Count;

            btnSiguiente.Text = "Siguiente.";
            btnSiguiente.Enabled = true;
            CheckarRadiosatras();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + Convert.ToInt32(dtpreguntas.Rows.Count - 114);
            x--;

            mostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";

            }

        }


        private void CheckarRadiosadelante()
        {
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x)]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtAmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 1]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtAmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 2]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtBmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 3]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtBmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 4]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtCmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 5]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtCmenos.Checked = true;
            }

            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 6]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtDmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 7]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtDmenos.Checked = true;
            }
        }

        private void CheckarRadiosatras()
        {
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) - 8]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta"].ToString()))
            {
                rbtAmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 1]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta"].ToString()))
            {
                rbtAmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 2]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta"].ToString()))
            {
                rbtBmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 3]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta"].ToString()))
            {
                rbtBmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 4]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta"].ToString()))
            {
                rbtCmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 5]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta"].ToString()))
            {
                rbtCmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 6]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta"].ToString()))
            {
                rbtDmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 7]["id_respuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta"].ToString()))
            {
                rbtDmenos.Checked = true;
            }
        }

        private void Evaluar()
        {
            if (rbtAmas.Checked == true)
            {
                idmas = Convert.ToInt32(dtrespuestas.Rows[(8 * x) - 8]["id_respuesta"].ToString());

            }
            else if (rbtAmenos.Checked == true)
            {
                idmenos = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 1]["id_respuesta"].ToString());
            }
            if (rbtBmas.Checked == true)
            {
                idmas = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 2]["id_respuesta"].ToString());

            }
            else if (rbtBmenos.Checked == true)
            {
                idmenos = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 3]["id_respuesta"].ToString());
            }
            if (rbtCmas.Checked == true)
            {
                idmas = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 4]["id_respuesta"].ToString());

            }
            else if (rbtCmenos.Checked == true)
            {
                idmenos = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 5]["id_respuesta"].ToString());
            }
            if (rbtDmas.Checked == true)
            {
                idmas = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 6]["id_respuesta"].ToString());

            }
            else if (rbtDmenos.Checked == true)
            {
                idmenos = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 7]["id_respuesta"].ToString());
            }

            punto = 1;
        }

        private void guardarRespuestas()
        {
            Evaluar();
            respcandidato.IdRespuesta = Convert.ToInt32(idmas);
            respcandidato.Punto = Convert.ToInt32(punto);
            respcandidato.NumeroPrueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaPipgRespuesta(respcandidato);

            respcandidato.IdRespuesta = Convert.ToInt32(idmenos);
            respcandidato.Punto = Convert.ToInt32(punto);
            respcandidato.NumeroPrueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaPipgRespuesta(respcandidato);
        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta_candidato"]);
            respcandidato.IdRespuesta = Convert.ToInt32(idmas);
            respcandidato.Punto = Convert.ToInt32(punto);
            cliente.ActualizaPipgRespuesta(respcandidato);

            respcandidato.IdRespuestaCandidato = Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta_candidato"]);
            respcandidato.IdRespuesta = Convert.ToInt32(idmenos);
            respcandidato.Punto = Convert.ToInt32(punto);
            cliente.ActualizaPipgRespuesta(respcandidato);
        }

        private void listardatos()
        {
            dto_pipg_preguntas[] ListadoPreguntas = cliente.ListadoPipgPreguntas();

            dtpreguntas.Columns.Add("id_pregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("id_identificador");

            dtrespuestas.Columns.Add("idpregunta");
            dtrespuestas.Columns.Add("id_respuesta");
            dtrespuestas.Columns.Add("respuesta");

            for (int i = 0; i < ListadoPreguntas.Length; i++)
            {
                DataRow fila = dtpreguntas.NewRow();
                fila["id_pregunta"] = Convert.ToInt32(ListadoPreguntas[i].IdPregunta);
                fila["pregunta"] = Convert.ToString(ListadoPreguntas[i].Pregunta);
                fila["id_identificador"] = Convert.ToInt32(ListadoPreguntas[i].Identificador);

                for (int j = 0; j < 2; j++)
                {
                    DataRow row = dtrespuestas.NewRow();
                    row["idpregunta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].IdPregunta);
                    row["id_respuesta"] = Convert.ToInt32(ListadoPreguntas[i].Respuesta[j].IdRespuesta);
                    row["respuesta"] = Convert.ToString(ListadoPreguntas[i].Respuesta[j].Respuesta);

                    dtrespuestas.Rows.Add(row);
                }
                dtpreguntas.Rows.Add(fila);
            }

            txtPreguntas.Text = "1) ";

            lblIdPregunta.Text = dtpreguntas.Rows[0]["id_pregunta"].ToString();
            rtbA.Text = "A) " + dtpreguntas.Rows[0]["pregunta"].ToString();
            rtbB.Text = "B) " + dtpreguntas.Rows[1]["pregunta"].ToString();
            rtbC.Text = "C) " + dtpreguntas.Rows[2]["pregunta"].ToString();
            rtbD.Text = "D) " + dtpreguntas.Rows[3]["pregunta"].ToString();

            rbtAmas.Name = dtrespuestas.Rows[0]["id_respuesta"].ToString();
            rbtAmenos.Name = dtrespuestas.Rows[1]["id_respuesta"].ToString();
            rbtBmas.Name = dtrespuestas.Rows[2]["id_respuesta"].ToString();
            rbtBmenos.Name = dtrespuestas.Rows[3]["id_respuesta"].ToString();
            rbtCmas.Name = dtrespuestas.Rows[4]["id_respuesta"].ToString();
            rbtCmenos.Name = dtrespuestas.Rows[5]["id_respuesta"].ToString();
            rbtDmas.Name = dtrespuestas.Rows[6]["id_respuesta"].ToString();
            rbtDmenos.Name = dtrespuestas.Rows[7]["id_respuesta"].ToString();
        }

        private void mostrarPreguntas(int i)
        {
            txtPreguntas.Text = i + 1 + ") ";
            row = dtrespuestas.Select("idpregunta = '" + dtpreguntas.Rows[(4 * i)][0].ToString() + " ' ");
            row1 = dtrespuestas.Select("idpregunta = '" + dtpreguntas.Rows[(4 * i) + 1][0].ToString() + " ' ");
            row2 = dtrespuestas.Select("idpregunta = '" + dtpreguntas.Rows[(4 * i) + 2][0].ToString() + " ' ");
            row3 = dtrespuestas.Select("idpregunta = '" + dtpreguntas.Rows[(4 * i) + 3][0].ToString() + " ' ");


            if (row.Length > 0)
            {
                //lblIdPregunta.Text = dtpreguntas.Rows[(4 * x)-4]["id_pregunta"].ToString();
                rtbA.Text = "A) " + dtpreguntas.Rows[(4 * x)]["pregunta"].ToString();
                rtbB.Text = "B) " + dtpreguntas.Rows[(4 * x) + 1]["pregunta"].ToString();
                rtbC.Text = "C) " + dtpreguntas.Rows[(4 * x) + 2]["pregunta"].ToString();
                rtbD.Text = "D) " + dtpreguntas.Rows[(4 * x) + 3]["pregunta"].ToString();

                rbtAmas.Name = row[0]["id_respuesta"].ToString();
                rbtAmenos.Name = row[1]["id_respuesta"].ToString();
                rbtBmas.Name = row1[0]["id_respuesta"].ToString();
                rbtBmenos.Name = row1[1]["id_respuesta"].ToString();
                rbtCmas.Name = row2[0]["id_respuesta"].ToString();
                rbtCmenos.Name = row2[1]["id_respuesta"].ToString();
                rbtDmas.Name = row3[0]["id_respuesta"].ToString();
                rbtDmenos.Name = row3[1]["id_respuesta"].ToString();
            }
        }

        private void limpiarcampos()
        {
            foreach (Control radio in this.panel2.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }

            foreach (Control radio in this.panel3.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
        }

        private bool ValidacionRAdioButtons()
        {
            if (rbtAmas.Checked == false && rbtBmas.Checked == false && rbtCmas.Checked == false && rbtDmas.Checked == false)
            {
                if (rbtAmenos.Checked == false && rbtBmenos.Checked == false && rbtCmenos.Checked == false && rbtDmenos.Checked == false)
                {
                    MessageBox.Show("Debe ingresar un campo de la columna (-) ");
                    return false;
                }
                return true;
            }

            return true;
        }
        #region Validar RadioButtons
        private void rbtAmas_Click(object sender, EventArgs e)
        {
            if (rbtAmas.Checked == true)
            {
                rbtAmenos.Checked = false;
            }

            if ((rbtAmas.Checked == true && rbtBmenos.Checked == true) || (rbtAmas.Checked == true && rbtCmenos.Checked == true) || (rbtAmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;

            }

        }

        private void rbtAmenos_Click(object sender, EventArgs e)
        {
            if (rbtAmenos.Checked == true)
            {
                rbtAmas.Checked = false;
            }

            if ((rbtAmenos.Checked == true && rbtBmas.Checked == true) || (rbtAmenos.Checked == true && rbtCmas.Checked == true) || (rbtAmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtBmas_Click(object sender, EventArgs e)
        {
            if (rbtBmas.Checked == true)
            {
                rbtBmenos.Checked = false;
            }
            if ((rbtBmas.Checked == true && rbtAmenos.Checked == true) || (rbtBmas.Checked == true && rbtCmenos.Checked == true) || (rbtBmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtBmenos_Click(object sender, EventArgs e)
        {
            if (rbtBmenos.Checked == true)
            {
                rbtBmas.Checked = false;
            }

            if ((rbtBmenos.Checked == true && rbtAmas.Checked == true) || (rbtBmenos.Checked == true && rbtCmas.Checked == true) || (rbtBmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtCmas_Click(object sender, EventArgs e)
        {
            if (rbtCmas.Checked == true)
            {
                rbtCmenos.Checked = false;
            }

            if ((rbtCmas.Checked == true && rbtAmenos.Checked == true) || (rbtCmas.Checked == true && rbtBmenos.Checked == true) || (rbtCmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtCmenos_Click(object sender, EventArgs e)
        {
            if (rbtCmenos.Checked == true)
            {
                rbtCmas.Checked = false;
            }

            if ((rbtCmenos.Checked == true && rbtAmas.Checked == true) || (rbtCmenos.Checked == true && rbtBmas.Checked == true) || (rbtCmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtDmas_Click(object sender, EventArgs e)
        {
            if (rbtDmas.Checked == true)
            {
                rbtDmenos.Checked = false;
            }


            if ((rbtDmas.Checked == true && rbtAmenos.Checked == true) || (rbtDmas.Checked == true && rbtBmenos.Checked == true) || (rbtDmas.Checked == true && rbtCmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtDmenos_Click(object sender, EventArgs e)
        {
            if (rbtDmenos.Checked == true)
            {
                rbtDmas.Checked = false;
            }


            if ((rbtDmenos.Checked == true && rbtAmas.Checked == true) || (rbtDmenos.Checked == true && rbtBmas.Checked == true) || (rbtDmenos.Checked == true && rbtCmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }
        #endregion

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        /// <summary>
        /// Cancela el proceso de la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba IPV", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_inicada)
                cliente.CambiarEstadoPrueba(Test.IPV, NumeroAsignacion, PruebaEstado.CANCELADA);
            this.Dispose();
        }
    }
}
