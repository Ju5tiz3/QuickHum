﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.PIPG
{
    public partial class frmPIPGReporte : Form
    {
        QuickHumClient cliente = Globales.cliente;
        DataTable dtresultados = new DataTable();
        DataTable dtgrafica = new DataTable();
        DataTable dtinter = new DataTable();
        DataTable dtCandidato = new DataTable();
        ReportDataSource rds_candidato = new ReportDataSource();
        string interpretacionesA, interpretacionesR, interpretacionesE, interpretacionesS, interpretacionesC, interpretacionesO, interpretacionesRP, interpretacionesV;
        string rangoA, rangoR, rangoE, rangoS, rangoC, rangoO, rangoRP, rangoV, expresion;
        int contador = 0, percentil = 0, count = 0;
        int ida = 0, idr = 0, ide = 0, ids = 0, idc = 0, ido = 0, idrp = 0, idv = 0, idae = 0, idinterpretacion = 0;
        DataRow[] filtro;
        private Expediente _expediente = null;

        string escolar;
        public frmPIPGReporte(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;

        }
        private void frmPIPGReporte_Load(object sender, EventArgs e)
        {
            for (int x = 0; x < this._expediente.PruebasAsignadas.Length; x++)
            {
                if (Convert.ToString(this._expediente.PruebasAsignadas[x].TipoPrueba.Nombre) == "PIPG")
                {
                    escolar = Convert.ToString(this._expediente.PruebasAsignadas[x].Escolaridad);
                }
            }

            dtresultados.Columns.Add("Factor");
            dtresultados.Columns.Add("Idfactor");
            dtresultados.Columns.Add("Descripcion");
            dtresultados.Columns.Add("Suma");
            dtresultados.Columns.Add("Grafica");
            // dtresultados.Columns.Add("interpretacion");

            dtgrafica.Columns.Add("id_factor");
            dtgrafica.Columns.Add("punto");
            dtgrafica.Columns.Add("grafico");
            dtgrafica.Columns.Add("escolaridad");
            dtgrafica.Columns.Add("autoestima");
            dtgrafica.Columns.Add("puntoauto");
            dtgrafica.Columns.Add("graficoauto");
            dtgrafica.Columns.Add("id_descripcion");


            dtinter.Columns.Add("idinterpretacion");
            dtinter.Columns.Add("interpretacion");
            dtinter.Columns.Add("rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            CargarReporte();

        }

        private void CargarReporte()
        {
            dtresultados.Clear();
            dtgrafica.Clear();
            dtinter.Clear();
            dto_pipg_resultados_factor[] resultado = cliente.ListaPipgResultadosFactor(_expediente.Id);
            dto_pipg_grafica_factor[] grafica = cliente.ListaPipgGrafica();
            dto_pipg_interpretacion[] interpretacion = cliente.ListaPipginterpretacion();

            for (int p = 0; p < interpretacion.ToList().Count; p++)
            {
                DataRow fil = dtinter.NewRow();

                fil["idinterpretacion"] = Convert.ToInt32(interpretacion[p].IdDescripcion);
                fil["interpretacion"] = Convert.ToString(interpretacion[p].Interpretacion);
                fil["rango"] = Convert.ToString(interpretacion[p].Rango);
                dtinter.Rows.Add(fil);

            }
            count = dtinter.Rows.Count;
            for (int x = 0; x < grafica.ToList().Count; x++)
            {
                DataRow rows = dtgrafica.NewRow();

                rows["id_factor"] = Convert.ToInt32(grafica[x].IdFactor);
                rows["punto"] = Convert.ToInt32(grafica[x].Punto);
                rows["grafico"] = Convert.ToInt32(grafica[x].Grafico);
                rows["escolaridad"] = Convert.ToString(grafica[x].Escolaridad);
                rows["autoestima"] = Convert.ToInt32(grafica[x].Autoestima);
                rows["puntoauto"] = Convert.ToInt32(grafica[x].Puntoauto);
                rows["graficoauto"] = Convert.ToInt32(grafica[x].Graficoauto);
                rows["id_descripcion"] = Convert.ToInt32(grafica[x].IdDescripcion);
                dtgrafica.Rows.Add(rows);
            }
            contador = dtgrafica.Rows.Count;
            int[] suma = new int[4];

            for (int i = 0; i < resultado.ToList().Count; i++)
            {
                DataRow fila = dtresultados.NewRow();

                switch (resultado[i].IdFactor)
                {

                    case 1:
                        fila["Factor"] = "Ascendencia";
                        suma[0] = suma[0] + resultado[i].Suma;


                        expresion = "id_factor= '1'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesA = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoA = Convert.ToString(filtro[0]["rango"].ToString());

                        break;
                    case 2:
                        fila["Factor"] = "Responsabilidad";
                        suma[1] = suma[1] + resultado[i].Suma;


                        expresion = "id_factor= '2'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesR = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoR = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 3:
                        fila["Factor"] = "Estabilidad Emocional";
                        suma[2] = suma[2] + resultado[i].Suma;


                        expresion = "id_factor= '3'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesE = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoE = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 4:
                        fila["Factor"] = "Sociabilidad";
                        suma[3] = suma[3] + resultado[i].Suma;


                        expresion = "id_factor= '4'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesS = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoS = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 5:
                        fila["Factor"] = "Cautela";


                        expresion = "id_factor= '5'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesC = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoC = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 6:
                        fila["Factor"] = "Originalidad";


                        expresion = "id_factor= '6'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesO = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoO = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 7:
                        fila["Factor"] = "Relaciones Personales";


                        expresion = "id_factor= '7'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesRP = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoRP = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 8:
                        fila["Factor"] = "Vigor";


                        expresion = "id_factor= '8'  and punto= '" + resultado[i].Suma + "' and escolaridad = '" + escolar + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["id_descripcion"].ToString());

                        expresion = "idinterpretacion= '" + idinterpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpretacionesV = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rangoV = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                }

                fila["Idfactor"] = Convert.ToInt32(resultado[i].IdFactor);
                fila["Descripcion"] = Convert.ToString(resultado[i].DescripcionFactor);
                fila["Suma"] = Convert.ToInt32(resultado[i].Suma);
                fila["Grafica"] = percentil;

                dtresultados.Rows.Add(fila);
                if (Convert.ToString(fila["Factor"]) == "Vigor")
                {

                    suma[0] += suma[1] + suma[2] + suma[3];

                    expresion = "autoestima= '1'  and puntoauto= '" + suma[0] + "'";
                    filtro = dtgrafica.Select(expresion);
                    percentil = Convert.ToInt32(filtro[0]["graficoauto"].ToString());

                    fila = dtresultados.NewRow();
                    fila["Factor"] = "Autoestima";
                    fila["Idfactor"] = Convert.ToInt32(11);
                    fila["Suma"] = Convert.ToInt32(suma[0]);
                    fila["Grafica"] = percentil;
                    dtresultados.Rows.Add(fila);
                }

            }

            ReportParameter[] parametro = new ReportParameter[17];
            parametro[0] = new ReportParameter("DUI", this._expediente.Candidato.Dui);
            parametro[1] = new ReportParameter("Ascendencia", interpretacionesA);
            parametro[2] = new ReportParameter("Responsabilidad", interpretacionesR);
            parametro[3] = new ReportParameter("Estabilidad", interpretacionesE);
            parametro[4] = new ReportParameter("Sociabilidad", interpretacionesS);
            parametro[5] = new ReportParameter("Cautela", interpretacionesC);
            parametro[6] = new ReportParameter("Originalidad", interpretacionesO);
            parametro[7] = new ReportParameter("Relaciones", interpretacionesRP);
            parametro[8] = new ReportParameter("Vigor", interpretacionesV);
            parametro[9] = new ReportParameter("RangoA", rangoA);
            parametro[10] = new ReportParameter("RangoR", rangoR);
            parametro[11] = new ReportParameter("RangoE", rangoE);
            parametro[12] = new ReportParameter("RangoS", rangoS);
            parametro[13] = new ReportParameter("RangoC", rangoC);
            parametro[14] = new ReportParameter("RangoO", rangoO);
            parametro[15] = new ReportParameter("RangoRP", rangoRP);
            parametro[16] = new ReportParameter("RangoV", rangoV);


            ReportDataSource rds = new ReportDataSource("DataSetpipg", dtresultados);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.PIPG.reportepipg.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.LocalReport.SetParameters(parametro);

            reportViewer1.RefreshReport();
        }


    }
}
