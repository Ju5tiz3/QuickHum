﻿namespace QuickHumCliente.Vistas.Pruebas.BETA_IIR
{
    partial class frmBetaIII
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBetaIII));
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.Panel();
            this.labSexo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labNumAsignacion = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labFecha = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labEdad = new System.Windows.Forms.Label();
            this.labStado = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labCentro = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.btnMatrices = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnObjetos = new System.Windows.Forms.Button();
            this.btnFiguras = new System.Windows.Forms.Button();
            this.btnPares = new System.Windows.Forms.Button();
            this.btnClaves = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labMT = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labM = new System.Windows.Forms.Label();
            this.labS = new System.Windows.Forms.Label();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Panel666 = new System.Windows.Forms.Panel();
            this.PanelInfo = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labInfoPrueba = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.labInfoTop = new System.Windows.Forms.Label();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Panel666.SuspendLayout();
            this.PanelInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(873, 36);
            this.label3.TabIndex = 65;
            this.label3.Text = "PRUEBA BETA III";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.labSexo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.labNumAsignacion);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.labFecha);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.labEdad);
            this.groupBox3.Controls.Add(this.labStado);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.labCentro);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.labName);
            this.groupBox3.Location = new System.Drawing.Point(12, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(847, 168);
            this.groupBox3.TabIndex = 67;
            // 
            // labSexo
            // 
            this.labSexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labSexo.AutoSize = true;
            this.labSexo.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labSexo.Location = new System.Drawing.Point(593, 83);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(71, 18);
            this.labSexo.TabIndex = 65;
            this.labSexo.Text = "Masculino";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(528, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 22);
            this.label9.TabIndex = 64;
            this.label9.Text = "Sexo:";
            // 
            // labNumAsignacion
            // 
            this.labNumAsignacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labNumAsignacion.AutoSize = true;
            this.labNumAsignacion.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.labNumAsignacion.Location = new System.Drawing.Point(528, 138);
            this.labNumAsignacion.Name = "labNumAsignacion";
            this.labNumAsignacion.Size = new System.Drawing.Size(71, 22);
            this.labNumAsignacion.TabIndex = 46;
            this.labNumAsignacion.Text = "Sexo: ¿?";
            this.labNumAsignacion.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(847, 28);
            this.label15.TabIndex = 63;
            this.label15.Text = "Datos Generales";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labFecha
            // 
            this.labFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labFecha.AutoSize = true;
            this.labFecha.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labFecha.Location = new System.Drawing.Point(593, 119);
            this.labFecha.Name = "labFecha";
            this.labFecha.Size = new System.Drawing.Size(74, 18);
            this.labFecha.TabIndex = 45;
            this.labFecha.Text = "15-01-2015";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(15, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 22);
            this.label7.TabIndex = 32;
            this.label7.Text = "Nombre y Apellido:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(15, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 22);
            this.label10.TabIndex = 35;
            this.label10.Text = "Centro:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(15, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 22);
            this.label12.TabIndex = 37;
            this.label12.Text = "Estado Civil:";
            // 
            // labEdad
            // 
            this.labEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labEdad.AutoSize = true;
            this.labEdad.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labEdad.Location = new System.Drawing.Point(593, 55);
            this.labEdad.Name = "labEdad";
            this.labEdad.Size = new System.Drawing.Size(22, 18);
            this.labEdad.TabIndex = 42;
            this.labEdad.Text = "23";
            // 
            // labStado
            // 
            this.labStado.AutoSize = true;
            this.labStado.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labStado.Location = new System.Drawing.Point(128, 120);
            this.labStado.Name = "labStado";
            this.labStado.Size = new System.Drawing.Size(52, 18);
            this.labStado.TabIndex = 41;
            this.labStado.Text = "Casado";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(528, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 22);
            this.label8.TabIndex = 33;
            this.label8.Text = "Edad:";
            // 
            // labCentro
            // 
            this.labCentro.AutoSize = true;
            this.labCentro.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labCentro.Location = new System.Drawing.Point(88, 87);
            this.labCentro.Name = "labCentro";
            this.labCentro.Size = new System.Drawing.Size(133, 18);
            this.labCentro.TabIndex = 40;
            this.labCentro.Text = "Regional San Miguel";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(528, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 22);
            this.label13.TabIndex = 38;
            this.label13.Text = "Fecha:";
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labName.Location = new System.Drawing.Point(177, 54);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(116, 18);
            this.labName.TabIndex = 39;
            this.labName.Text = "Carlos Hernandez";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnFinalizar);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnIniciar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 187);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(847, 333);
            this.panel1.TabIndex = 68;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnMatrices);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnObjetos);
            this.groupBox1.Controls.Add(this.btnFiguras);
            this.groupBox1.Controls.Add(this.btnPares);
            this.groupBox1.Controls.Add(this.btnClaves);
            this.groupBox1.Location = new System.Drawing.Point(117, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(657, 182);
            this.groupBox1.TabIndex = 68;
            // 
            // btnMatrices
            // 
            this.btnMatrices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(67)))), ((int)(((byte)(97)))));
            this.btnMatrices.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMatrices.FlatAppearance.BorderSize = 0;
            this.btnMatrices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMatrices.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnMatrices.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMatrices.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnMatrices.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMatrices.Location = new System.Drawing.Point(339, 86);
            this.btnMatrices.Name = "btnMatrices";
            this.btnMatrices.Size = new System.Drawing.Size(300, 37);
            this.btnMatrices.TabIndex = 1014;
            this.btnMatrices.Text = "Matrices";
            this.btnMatrices.UseVisualStyleBackColor = false;
            this.btnMatrices.Click += new System.EventHandler(this.btnMatrices_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(657, 28);
            this.label1.TabIndex = 1016;
            this.label1.Text = "Tareas";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnObjetos
            // 
            this.btnObjetos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(67)))), ((int)(((byte)(97)))));
            this.btnObjetos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnObjetos.FlatAppearance.BorderSize = 0;
            this.btnObjetos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObjetos.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnObjetos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnObjetos.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnObjetos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnObjetos.Location = new System.Drawing.Point(339, 43);
            this.btnObjetos.Name = "btnObjetos";
            this.btnObjetos.Size = new System.Drawing.Size(300, 37);
            this.btnObjetos.TabIndex = 1013;
            this.btnObjetos.Text = "Objetos Equivocados";
            this.btnObjetos.UseVisualStyleBackColor = false;
            this.btnObjetos.Click += new System.EventHandler(this.btnObjetos_Click);
            // 
            // btnFiguras
            // 
            this.btnFiguras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(67)))), ((int)(((byte)(97)))));
            this.btnFiguras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFiguras.FlatAppearance.BorderSize = 0;
            this.btnFiguras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiguras.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnFiguras.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnFiguras.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnFiguras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiguras.Location = new System.Drawing.Point(16, 86);
            this.btnFiguras.Name = "btnFiguras";
            this.btnFiguras.Size = new System.Drawing.Size(300, 37);
            this.btnFiguras.TabIndex = 1010;
            this.btnFiguras.Text = "Figuras Incompletas";
            this.btnFiguras.UseVisualStyleBackColor = false;
            this.btnFiguras.Click += new System.EventHandler(this.btnFiguras_Click);
            // 
            // btnPares
            // 
            this.btnPares.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(67)))), ((int)(((byte)(97)))));
            this.btnPares.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPares.FlatAppearance.BorderSize = 0;
            this.btnPares.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPares.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnPares.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPares.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnPares.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPares.Location = new System.Drawing.Point(16, 129);
            this.btnPares.Name = "btnPares";
            this.btnPares.Size = new System.Drawing.Size(300, 37);
            this.btnPares.TabIndex = 1011;
            this.btnPares.Text = "Pares Iguales y Pares Desiguales";
            this.btnPares.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPares.UseVisualStyleBackColor = false;
            this.btnPares.Click += new System.EventHandler(this.btnPares_Click);
            // 
            // btnClaves
            // 
            this.btnClaves.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(67)))), ((int)(((byte)(97)))));
            this.btnClaves.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClaves.FlatAppearance.BorderSize = 0;
            this.btnClaves.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClaves.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnClaves.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClaves.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnClaves.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClaves.Location = new System.Drawing.Point(16, 43);
            this.btnClaves.Name = "btnClaves";
            this.btnClaves.Size = new System.Drawing.Size(300, 37);
            this.btnClaves.TabIndex = 1009;
            this.btnClaves.Text = "Claves";
            this.btnClaves.UseVisualStyleBackColor = false;
            this.btnClaves.Click += new System.EventHandler(this.btnClaves_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnFinalizar.FlatAppearance.BorderSize = 0;
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnFinalizar.ForeColor = System.Drawing.Color.White;
            this.btnFinalizar.Image = global::QuickHumCliente.Properties.RecursosPruebas.timerStop;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(610, 268);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(164, 33);
            this.btnFinalizar.TabIndex = 26;
            this.btnFinalizar.Text = "Finalizar Prueba";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo21;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.labMT);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labM);
            this.panel2.Controls.Add(this.labS);
            this.panel2.Location = new System.Drawing.Point(487, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 37);
            this.panel2.TabIndex = 67;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(287, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 18);
            this.label17.TabIndex = 24;
            this.label17.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(68, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 19);
            this.label5.TabIndex = 22;
            this.label5.Text = "Tiempo Transcurrido:";
            // 
            // labMT
            // 
            this.labMT.AutoSize = true;
            this.labMT.BackColor = System.Drawing.Color.Transparent;
            this.labMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMT.ForeColor = System.Drawing.Color.White;
            this.labMT.Location = new System.Drawing.Point(222, 10);
            this.labMT.Name = "labMT";
            this.labMT.Size = new System.Drawing.Size(26, 18);
            this.labMT.TabIndex = 21;
            this.labMT.Text = "00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(248, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 18);
            this.label6.TabIndex = 23;
            this.label6.Text = ":";
            // 
            // labM
            // 
            this.labM.AutoSize = true;
            this.labM.BackColor = System.Drawing.Color.Transparent;
            this.labM.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labM.ForeColor = System.Drawing.Color.White;
            this.labM.Location = new System.Drawing.Point(300, 10);
            this.labM.Name = "labM";
            this.labM.Size = new System.Drawing.Size(26, 18);
            this.labM.TabIndex = 19;
            this.labM.Text = "00";
            // 
            // labS
            // 
            this.labS.AutoSize = true;
            this.labS.BackColor = System.Drawing.Color.Transparent;
            this.labS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labS.ForeColor = System.Drawing.Color.White;
            this.labS.Location = new System.Drawing.Point(261, 10);
            this.labS.Name = "labS";
            this.labS.Size = new System.Drawing.Size(26, 18);
            this.labS.TabIndex = 20;
            this.labS.Text = "00";
            // 
            // btnIniciar
            // 
            this.btnIniciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciar.FlatAppearance.BorderSize = 0;
            this.btnIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnIniciar.ForeColor = System.Drawing.Color.White;
            this.btnIniciar.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciar.Location = new System.Drawing.Point(119, 268);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(164, 33);
            this.btnIniciar.TabIndex = 64;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = false;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(847, 28);
            this.label2.TabIndex = 63;
            this.label2.Text = "Ayuda de la Prueba";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel666
            // 
            this.Panel666.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.Panel666.Controls.Add(this.PanelInfo);
            this.Panel666.Controls.Add(this.groupBox3);
            this.Panel666.Controls.Add(this.panel1);
            this.Panel666.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel666.Location = new System.Drawing.Point(0, 36);
            this.Panel666.Name = "Panel666";
            this.Panel666.Size = new System.Drawing.Size(873, 545);
            this.Panel666.TabIndex = 78;
            // 
            // PanelInfo
            // 
            this.PanelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.PanelInfo.Controls.Add(this.panel3);
            this.PanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelInfo.Location = new System.Drawing.Point(0, 0);
            this.PanelInfo.Name = "PanelInfo";
            this.PanelInfo.Size = new System.Drawing.Size(873, 545);
            this.PanelInfo.TabIndex = 79;
            this.PanelInfo.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.labInfoPrueba);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.labInfoTop);
            this.panel3.Location = new System.Drawing.Point(169, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(523, 263);
            this.panel3.TabIndex = 73;
            // 
            // labInfoPrueba
            // 
            this.labInfoPrueba.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labInfoPrueba.Font = new System.Drawing.Font("Calibri", 12F);
            this.labInfoPrueba.Location = new System.Drawing.Point(49, 51);
            this.labInfoPrueba.Name = "labInfoPrueba";
            this.labInfoPrueba.Size = new System.Drawing.Size(423, 207);
            this.labInfoPrueba.TabIndex = 76;
            this.labInfoPrueba.Text = resources.GetString("labInfoPrueba.Text");
            this.labInfoPrueba.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(1, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 75;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.btnCerrarAyuda);
            this.panel5.Location = new System.Drawing.Point(487, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(36, 36);
            this.panel5.TabIndex = 74;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close;
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(-2, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 76;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // labInfoTop
            // 
            this.labInfoTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.labInfoTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.labInfoTop.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.labInfoTop.ForeColor = System.Drawing.Color.White;
            this.labInfoTop.Location = new System.Drawing.Point(0, 0);
            this.labInfoTop.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.labInfoTop.Name = "labInfoTop";
            this.labInfoTop.Size = new System.Drawing.Size(523, 36);
            this.labInfoTop.TabIndex = 61;
            this.labInfoTop.Text = "INDICACIONES";
            this.labInfoTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // frmBetaIII
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 581);
            this.Controls.Add(this.Panel666);
            this.Controls.Add(this.label3);
            this.Name = "frmBetaIII";
            this.Text = "Beta II-R";
            this.Load += new System.EventHandler(this.frmBetaII_R_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Panel666.ResumeLayout(false);
            this.PanelInfo.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel groupBox3;
        private System.Windows.Forms.Label labNumAsignacion;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labFecha;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labEdad;
        private System.Windows.Forms.Label labStado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labCentro;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnMatrices;
        private System.Windows.Forms.Button btnObjetos;
        private System.Windows.Forms.Button btnPares;
        private System.Windows.Forms.Button btnFiguras;
        private System.Windows.Forms.Button btnClaves;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labM;
        private System.Windows.Forms.Label labS;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Panel Panel666;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Panel PanelInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labInfoPrueba;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Label labInfoTop;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Label label1;
    }
}