﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Reporting.WinForms;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.BETA_IIR
{
    public partial class frmReporteBetaIIR : Form
    {
        private QuickHumClient cliente = Globales.cliente;

        private Expediente _expediente = null;

        DataTable data = new DataTable();
        DataTable dtCandidato = new DataTable();

        int NumeroAsignacion;
        double divicion ;

        public frmReporteBetaIIR(Expediente expediente)
        {
            _expediente = expediente;
            InitializeComponent();
        }

        public void CargarResultado()
        {
            NumeroAsignacion = cliente.RecuperarNumeroAsignacion(_expediente.Id);
            dto_BETAIIIRespuesta[] respuestas = cliente.CargarRespuestasBetaIII(NumeroAsignacion);
            if (respuestas.Length > 0)
            {
                txtPT1.Text = respuestas[0].Puntos.ToString(); txtPT1.Enabled = false;
                txtPT2.Text = respuestas[1].Puntos.ToString(); txtPT2.Enabled = false;
                txtPT3.Text = respuestas[2].Puntos.ToString(); txtPT3.Enabled = false;
                txtPT4.Text = respuestas[3].Puntos.ToString(); txtPT4.Enabled = false;
                txtPT5.Text = respuestas[4].Puntos.ToString(); txtPT5.Enabled = false;
                btnResultado.Name = "Cargar";

                dto_BETAIIIPuntos[] puntos = cliente.ListarPuntosBetaIII(NumeroAsignacion, _expediente.Candidato.Edad, _expediente.Candidato.Edad);
                txtPN1.Text = puntos[0].Puntos.ToString();
                txtPN2.Text = puntos[1].Puntos.ToString();
                txtPN3.Text = puntos[2].Puntos.ToString();
                txtPN4.Text = puntos[3].Puntos.ToString();
                txtPN5.Text = puntos[4].Puntos.ToString();

                txtCI.Text = puntos[0].PuntosCI.ToString();
                txtPercentil.Text = puntos[0].PuntosPCT.ToString();
                txtTotal.Text = puntos[0].PuntosTotal.ToString();

                if (puntos[0].PuntosCI >= 130) { txtInteligencia.Text = "Muy Superior"; }
                if (puntos[0].PuntosCI >= 120 && puntos[0].PuntosCI <= 129) { txtInteligencia.Text = "Superior"; }
                if (puntos[0].PuntosCI >= 110 && puntos[0].PuntosCI <= 119) { txtInteligencia.Text = "Promedio Alto"; }
                if (puntos[0].PuntosCI >= 90 && puntos[0].PuntosCI <= 109) { txtInteligencia.Text = "Promedio"; }
                if (puntos[0].PuntosCI >= 80 && puntos[0].PuntosCI <= 89) { txtInteligencia.Text = "Promedio Bajo"; }
                if (puntos[0].PuntosCI >= 70 && puntos[0].PuntosCI <= 79) { txtInteligencia.Text = "Limítrofe"; }
                if (puntos[0].PuntosCI <= 69) { txtInteligencia.Text = "Extremadamente Bajo"; }

                string[] Tarea = new string[] { "Claves", "Figuras Incompletas", "Pares Iguales y Pares Desiguales", "Obejetos Equivocados", "Matrices" };

                data.Columns.Add("Punto");
                data.Columns.Add("Tarea");
                for (int k = 0; k < puntos.Length; ++k)
                {
                    if (k == 0) { divicion = 0.19; } if (k == 1) { divicion = 0.18; } 
                    if (k == 2) { divicion = 0.19; } if (k == 3) { divicion = 0.14; } if (k == 4) { divicion = 0.15; }

                    if (_expediente.Candidato.Edad >= 18 && _expediente.Candidato.Edad <= 19) { if (k == 4) { divicion = 0.16; } }
                    if (_expediente.Candidato.Edad >= 20 && _expediente.Candidato.Edad <= 24) { if (k == 4) { divicion = 0.16; } }
                    if (_expediente.Candidato.Edad >= 25 && _expediente.Candidato.Edad <= 34) { if (k == 4) { divicion = 0.17; } }
                    if (_expediente.Candidato.Edad >= 35 && _expediente.Candidato.Edad <= 44) { if (k == 4) { divicion = 0.17; } }
                    if (_expediente.Candidato.Edad >= 45 && _expediente.Candidato.Edad <= 54) { if (k == 1) { divicion = 0.19; } if (k == 4) { divicion = 0.18; } }
                    if (_expediente.Candidato.Edad >= 55 && _expediente.Candidato.Edad <= 64) { if (k == 1) { divicion = 0.19; } if (k == 3) { divicion = 0.15; } if (k == 4) { divicion = 0.19; } }
                    if (_expediente.Candidato.Edad >= 65 && _expediente.Candidato.Edad <= 74) { if (k == 1) { divicion = 0.19; } if (k == 3) { divicion = 0.16; } if (k == 4) { divicion = 0.19; } }
                    if (_expediente.Candidato.Edad >= 75 && _expediente.Candidato.Edad <= 89) { if (k == 1) { divicion = 0.19; } if (k == 3) { divicion = 0.18; } if (k == 4) { divicion = 0.19; } }

                    DataRow rows = data.NewRow();
                    rows["Punto"] = Convert.ToInt16(puntos[k].Puntos / divicion);
                    rows["Tarea"] = Tarea[k];
                    //MessageBox.Show(Convert.ToInt16(puntos[k].Puntos / divicion).ToString());
                    data.Rows.Add(rows);
                }

                ReportDataSource ReportData = new ReportDataSource("dtBetaIII", data);
                ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
                rpvBetaIIR.LocalReport.DataSources.Clear();
                rpvBetaIIR.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.BETA_IIR.ReporteBetaIII.rdlc";
                rpvBetaIIR.LocalReport.DataSources.Add(ReportData);
                rpvBetaIIR.LocalReport.DataSources.Add(rds_candidato);
                rpvBetaIIR.RefreshReport();
                btnResultado.Enabled = false;
                return;
            }
            btnResultado.Name = "Guardar";
            this.rpvBetaIIR.RefreshReport();
        
        }

        private void frmBetaIIR_Load(object sender, EventArgs e)
        {
            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            CargarResultado();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (txtPT1.Text == string.Empty) { txtPT1.Focus(); return; } if (txtPT2.Text == string.Empty) { txtPT2.Focus(); return; } if (txtPT3.Text == string.Empty) { txtPT3.Focus(); return; }
            if (txtPT4.Text == string.Empty) { txtPT4.Focus(); return; } if (txtPT5.Text == string.Empty) { txtPT5.Focus(); return; }

            if (Convert.ToInt32(txtPT1.Text) <= 3) { MessageBox.Show("El Valor de los Puntos debe ser mayor a 3", "Beta IIR", MessageBoxButtons.OK, MessageBoxIcon.Error); txtPT1.Focus(); txtPT1.Clear(); return; }

            if (Convert.ToInt32(txtPT1.Text) > 140) { Error.SetError(txtPT1, "El numéro de puntos, exede el maximo de la puntuanción para la tarea"); txtPT1.Clear(); txtPT1.Focus(); if (txtPT1.Text != string.Empty) { Error.Clear(); } return; }
            if (Convert.ToInt32(txtPT2.Text) > 24) { Error.SetError(txtPT2, "El numéro de puntos, exede el maximo de la puntuanción para la tarea"); txtPT2.Clear(); txtPT2.Focus(); if (txtPT2.Text != string.Empty) { Error.Clear(); } return; }
            if (Convert.ToInt32(txtPT3.Text) > 55) { Error.SetError(txtPT3, "El numéro de puntos, exede el maximo de la puntuanción para la tarea"); txtPT3.Clear(); txtPT3.Focus(); if (txtPT3.Text != string.Empty) { Error.Clear(); } return; }
            if (Convert.ToInt32(txtPT4.Text) > 24) { Error.SetError(txtPT4, "El numéro de puntos, exede el maximo de la puntuanción para la tarea"); txtPT4.Clear(); txtPT4.Focus(); if (txtPT4.Text != string.Empty) { Error.Clear(); } return; }
            if (Convert.ToInt32(txtPT5.Text) > 25) { Error.SetError(txtPT5, "El numéro de puntos, exede el maximo de la puntuanción para la tarea"); txtPT5.Clear(); txtPT5.Focus(); if (txtPT5.Text != string.Empty) { Error.Clear(); } return; }

            byte[] PuntosCandidato = new byte[6];
            PuntosCandidato[0] = Convert.ToByte(txtPT1.Text); PuntosCandidato[1] = Convert.ToByte(txtPT2.Text);
            PuntosCandidato[2] = Convert.ToByte(txtPT3.Text); PuntosCandidato[3] = Convert.ToByte(txtPT4.Text);
            PuntosCandidato[4] = Convert.ToByte(txtPT5.Text); 
            byte num = 1;

                if (btnResultado.Name == "Guardar")
                {
                    NumeroAsignacion = cliente.RecuperarNumeroAsignacion(_expediente.Id);

                    dto_BETAIIIRespuesta insert = new dto_BETAIIIRespuesta();
                    for (int x = 0; x < 5; x++)
                    {
                        if (x == 0) { divicion = 0.19; } if (x == 1) { divicion = 0.18; }
                        if (x == 2) { divicion = 0.19; } if (x == 3) { divicion = 0.14; } if (x == 4) { divicion = 0.15; }

                        if (_expediente.Candidato.Edad >= 18 && _expediente.Candidato.Edad <= 19) { if (x == 4) { divicion = 0.16; } }
                        if (_expediente.Candidato.Edad >= 20 && _expediente.Candidato.Edad <= 24) { if (x == 4) { divicion = 0.16; } }
                        if (_expediente.Candidato.Edad >= 25 && _expediente.Candidato.Edad <= 34) { if (x == 4) { divicion = 0.17; } }
                        if (_expediente.Candidato.Edad >= 35 && _expediente.Candidato.Edad <= 44) { if (x == 4) { divicion = 0.17; } }
                        if (_expediente.Candidato.Edad >= 45 && _expediente.Candidato.Edad <= 54) { if (x == 1) { divicion = 0.19; } if (x == 4) { divicion = 0.18; } }
                        if (_expediente.Candidato.Edad >= 55 && _expediente.Candidato.Edad <= 64) { if (x == 1) { divicion = 0.19; } if (x == 3) { divicion = 0.15; } if (x == 4) { divicion = 0.19; } }
                        if (_expediente.Candidato.Edad >= 65 && _expediente.Candidato.Edad <= 74) { if (x == 1) { divicion = 0.19; } if (x == 3) { divicion = 0.16; } if (x == 4) { divicion = 0.19; } }
                        if (_expediente.Candidato.Edad >= 75 && _expediente.Candidato.Edad <= 89) { if (x == 1) { divicion = 0.19; } if (x == 3) { divicion = 0.18; } if (x == 4) { divicion = 0.19; } }

                        insert.PuntoTarea = PuntosCandidato[x];
                        insert.IdTarea = num;
                        insert.NumeroPrueba = NumeroAsignacion;
                        if (cliente.InsertarRespuestasBetaII(insert) == true)
                        { num += 1; }
                    }
                }

               dto_BETAIIIPuntos[] puntos = cliente.ListarPuntosBetaIII(NumeroAsignacion,_expediente.Candidato.Edad,_expediente.Candidato.Edad);
               txtPN1.Text = puntos[0].Puntos.ToString();
               txtPN2.Text = puntos[1].Puntos.ToString();
               txtPN3.Text = puntos[2].Puntos.ToString();
               txtPN4.Text = puntos[3].Puntos.ToString();
               txtPN5.Text = puntos[4].Puntos.ToString();

               txtCI.Text = puntos[0].PuntosCI.ToString();
               txtPercentil.Text = puntos[0].PuntosPCT.ToString();
               txtTotal.Text = puntos[0].PuntosTotal.ToString();

               if (puntos[0].PuntosCI >= 130) { txtInteligencia.Text = "Muy Superior"; }
               if (puntos[0].PuntosCI >= 120 && puntos[0].PuntosCI <= 129) { txtInteligencia.Text = "Superior"; }
               if (puntos[0].PuntosCI >= 110 && puntos[0].PuntosCI <= 119) { txtInteligencia.Text = "Promedio Alto"; }
               if (puntos[0].PuntosCI >= 90 && puntos[0].PuntosCI <= 109) { txtInteligencia.Text = "Promedio"; }
               if (puntos[0].PuntosCI >= 80 && puntos[0].PuntosCI <= 89) { txtInteligencia.Text = "Promedio Bajo"; }
               if (puntos[0].PuntosCI >= 70 && puntos[0].PuntosCI <= 79) { txtInteligencia.Text = "Limítrofe"; }
               if (puntos[0].PuntosCI <= 69) { txtInteligencia.Text = "Extremadamente Bajo"; }

                   string[] Tarea = new string[] { "Claves", "Figuras Incompletas", "Pares Iguales y Pares Desiguales", "Obejetos Equivocados", "Matrices" };

                   data.Columns.Add("Punto");
                   data.Columns.Add("Tarea");
                   for (int k = 0; k < puntos.Length; ++k)
                   {

                       if (k == 2) { divicion = 0.18; } if (k == 4) { divicion = 0.14; } if (k == 5) { divicion = 0.15; }

                       DataRow rows = data.NewRow();
                       rows["Punto"] = Convert.ToInt16(puntos[k].Puntos / divicion);
                       rows["Tarea"] = Tarea[k];
                       data.Rows.Add(rows);
                   }

                   ReportDataSource ReportData = new ReportDataSource("dtBetaIII", data);//dsCandidatos
                   rpvBetaIIR.LocalReport.DataSources.Clear();
                   rpvBetaIIR.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.BETA_IIR.ReporteBetaIII.rdlc";
                   rpvBetaIIR.LocalReport.DataSources.Add(ReportData);
                   rpvBetaIIR.RefreshReport();
        }
        
    }
}
