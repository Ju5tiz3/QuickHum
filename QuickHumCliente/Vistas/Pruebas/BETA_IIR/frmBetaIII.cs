﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ServiceModel;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.BETA_IIR
{
    public partial class frmBetaIII : Form
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;

        int mil, seg, min;

        int NumeroAsignacion = 0;

        public frmBetaIII(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                BETAIII nuevo = cliente.NuevoBetaIII(_expediente.Numero, _escolaridad_prueba);
                NumeroAsignacion = nuevo.IdPrueba;
                Timer.Start();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
        }

        private void frmBetaII_R_Load(object sender, EventArgs e)
        {
            labName.Text = _expediente.Candidato.Nombre + " " + _expediente.Candidato.Apellido;
            labSexo.Text = Convert.ToString(_expediente.Candidato.Genero);
            labStado.Text = _expediente.Candidato.EstadoCivil;
            labFecha.Text = DateTime.Now.ToString("yyyy-MM-dd");
            labEdad.Text = Convert.ToString(_expediente.Candidato.Edad);
            labCentro.Text = _expediente.Ubicacion.Departamento.Nombre;

        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            BETAIII Finalizar = new BETAIII();
            Finalizar.IdPrueba = NumeroAsignacion;
            if (cliente.FinalizarBetaIII(Finalizar))
            {
                XtraMessageBox.Show("Test Finalizado", "BETA II-R", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Timer.Stop();
                this.Close();
                return;
            }
            XtraMessageBox.Show("Error al Finalizar el Test", "BETA II-R", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.Panel666.Hide();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Timer.Interval = 10;
            mil = Convert.ToInt16(labM.Text); mil += 1; labM.Text = mil.ToString();
            if (mil == 60)
            {
                seg = Convert.ToInt16(labS.Text); seg += 1; labS.Text = seg.ToString(); labM.Text = "00";

                if (seg == 60) { min = Convert.ToInt16(labMT.Text); min += 1; labMT.Text = min.ToString(); labS.Text = "00"; }
            }
            if (min == 25)
            {
                Timer.Stop();
                XtraMessageBox.Show("Se ha terminado el tiempo estimado para realizar esta prueba", "TIEMPO AGOTADO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Ayuda Sub Prueba (Claves)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClaves_Click(object sender, EventArgs e)
        {
            labInfoTop.Text = "Claves";
            labInfoPrueba.Text = "Observe la fila de cuadritos que aparece en la parte superior de la página. Arriba de cada cuadrito hay un símbolo, y debajo de cada símbolo hay un número. La tarea es poner en los cuadritos de abajo, " +
                                " el numero correcto dentro de cada símbolo. Mire la primera fila de cuadritos de la parte de abajo. Los primeros cuatro cuadros ya están hechos. Fíjese que se ha puesto un 5 debajo del signo de la suma (+). " +
                                " Ahora siga con lo demás, poniendo el número correcto debajo  de cada símbolo";
            PanelInfo.Show();
        }

        /// <summary>
        /// Ayuda Sub Prueba (Figuras Incompletas)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFiguras_Click(object sender, EventArgs e)
        {
            labInfoTop.Text = "Figuras Incompletas";
            labInfoPrueba.Text = "Debe trazar la parte que le falta del dibujo, sin tratar de hacerlo perfecto o artístico; recuerde que debe trabajar tan rápido como les sea posible. En el ejercicio A, el tenedor le hace falta un diente, " +
            " y en el ejercicio B, le hace falta una pata a la mesa, en los cuadros de abajo se muestra como se deben trazar el diente del tenedor, ya la pata de la mesa";
            PanelInfo.Show();
        }

        /// <summary>
        /// Ayuda Sub Prueba (Objetos Equivocados)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnObjetos_Click(object sender, EventArgs e)
        {
            labInfoTop.Text = "Objetos Equivocados";
            labInfoPrueba.Text = "Mire los tres ejercicios de práctica que aparecen en la parte superior de la página. En cada ejercicio, deben marcar el dibujo que muestra algo que está mal o que no tiene sentido. Ahora, trace un tache o una X sobre los dibujos que están mal o que no tienen sentido, " +
            " en los ejercios A, B, C. Ejercicio A, se ha marcado el sombrero que tiene un agujero. Ejercicio B, se ha marcado el abrigo al que le falta una  manga. Ejercicio C, la mesa que tiene una pata menos. En cada ejercicio tracen una X sobre el dibujo que está mal o que no tiene sentido ";
            PanelInfo.Show();
        }

        /// <summary>
        /// Ayuda Sub Prueba (Pares Iguales y Pares Desiguales)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPares_Click(object sender, EventArgs e)
        {
            labInfoTop.Text = "Pares Iguales y Pares Desiguales";
            labInfoPrueba.Text = "Mire los cuatro ejercicios que aparecen  en la parte superior de la página; vea cada pareja de dibujos, símbolos o números. Si los dos dibujos, " +
           " símbolos o números de la pareja son los mismos, encierren en un círculo el signo de IGUALDAD (=). Si los dos dibujos, símbolos o números no son iguales, encierre en un círculo el singo de DESIGUALDAD (≠) " +
           " Ejercicio A, la cruz y el cuadro no son iguales, por eso se ha encerrado en un círculo el signo de DESIGUALDAD. " +
           " Ejercicio B, las latas son iguales, por eso se ha encerrado en un círculo el signo de IGUALDAD ";
            PanelInfo.Show();
        }

        /// <summary>
        /// Ayuda Sub Prueba (Matrices)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMatrices_Click(object sender, EventArgs e)
        {
            labInfoTop.Text = "Matrices";
            labInfoPrueba.Text = "Mire los dos ejercicios de práctica que aparecen en la parte superior de la página. Fíjese en los signos de pregunta o interrogación (?) que están en los cuadritos  de la izquierda. El sino de interrogación significa que allí falta un dibujo. Para el ejercicio A, escoja el grupo de 5 dibujos que aparece a la derecha, el dibujo que debe estar en el cuadrito que tiene el signo de la interrogación." +
            " Encierren en un círculo el número que aparece debajo de la respuesta correcta para el ejercicio A. Continúe con el ejercicio B ";
            PanelInfo.Show();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            PanelInfo.Hide();
        }

    }
}
