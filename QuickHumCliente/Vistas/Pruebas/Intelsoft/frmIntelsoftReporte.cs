﻿using System;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;


namespace QuickHumCliente.Vistas.Pruebas.Intelsoft
{
    public partial class frmIntelsoftReporte : Form
    {
        public frmIntelsoftReporte(Expediente expediente)
        {
            InitializeComponent();
            this._expediente = expediente;
        }

        dto_intel_resultados[] resultados;
        private readonly QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;

        readonly DataTable dtresultados = new DataTable();
        readonly DataTable dtgrafica = new DataTable();
        readonly DataTable dtinter = new DataTable();
        DataTable dtCandidato = new DataTable();

        string expresion, interpretaciones,orden;
        DataRow[] filtro;
        int percentil = 0, idinterpretacion = 0;
        private void frmIntelsoftReporte_Load(object sender, EventArgs e)
        {
            dto_intel_interpretacion[] interp = cliente.ListaIntelinterpretacion();
            dto_intel_grafica[] grafica = cliente.ListarIntelGrafico();
            dtresultados.Columns.Add("Idfactor");
            dtresultados.Columns.Add("Factor");
            dtresultados.Columns.Add("Total");
            dtresultados.Columns.Add("Percentil");
            dtresultados.Columns.Add("Orden");
            dtresultados.Columns.Add("Interpretacion");

           
            dtgrafica.Columns.Add("idgrafica");
            dtgrafica.Columns.Add("id_factor");
            dtgrafica.Columns.Add("punto");
            dtgrafica.Columns.Add("grafica");
            dtgrafica.Columns.Add("idinterpretacion");

            dtinter.Columns.Add("id_interpretacion");
            dtinter.Columns.Add("interpretacion");
            dtinter.Columns.Add("rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            for (int w = 0; w < grafica.Length;w++ )
            {
                DataRow row = dtgrafica.NewRow();
                row["idgrafica"]=Convert.ToInt32(grafica[w].Idgrafica);
                row["id_factor"] = Convert.ToInt32(grafica[w].Idfactor);
                row["punto"] = Convert.ToInt32(grafica[w].Punto);
                row["grafica"] = Convert.ToInt32(grafica[w].Grafico);
                row["idinterpretacion"] = Convert.ToInt32(grafica[w].Idinterpretacion);
                dtgrafica.Rows.Add(row);
            }

            for (int y = 0; y < interp.Length; y++ )
            {
                DataRow row = dtinter.NewRow();
                row["id_interpretacion"]= Convert.ToInt32(interp[y].Idinterpretacion);
                 row["interpretacion"]= Convert.ToString(interp[y].Interpretacion);
                 row["rango"]= Convert.ToString(interp[y].Rango);
                 dtinter.Rows.Add(row);

            }

            Resultados();

        }

        private void Resultados()
        {
            resultados = cliente.ListaResultadosIntel(this._expediente.Id);
            dtresultados.Clear();
            for (int i = 0; i < resultados.Length; i++)
            {
                DataRow row = dtresultados.NewRow();
                switch (resultados[i].Idfactor)
                {
                    case 1:
                        orden = "A";
                        expresion = "id_factor= '1' and punto='"+resultados[i].Total+"'";;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "1. Sucesos de la vida: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 2:
                        orden = "B";
                        expresion = "id_factor= '2' and punto='"+resultados[i].Total+"'";;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "2. Presiones del Trabajo: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 3:
                        orden = "C";
                        expresion = "id_factor= '3' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "3. Presiones Personales: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 4:
                        orden = "D";
                        expresion = "id_factor= '4' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "4. Conciencia Emocional de sí mismo: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 5:
                        orden = "E";
                        expresion = "id_factor= '5' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "5. Expresión Emocional: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 6:
                        orden = "F";
                        expresion = "id_factor= '6' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "6. Conciencia Emocional de otros: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 7:
                        orden = "G";
                        expresion = "id_factor= '7' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "7. Intención: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 8:
                        orden = "H";
                        expresion = "id_factor= '8' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "8. Creatividad: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 9:
                        orden = "I";
                        expresion = "id_factor= '9' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "9. Elasticidad: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 10:
                        orden = "J";
                        expresion = "id_factor= '10' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "10. Conexiones Interpersonales: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 11:
                        orden = "K";
                        expresion = "id_factor= '11' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "11. Descontento Constructivo: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 12:
                        orden = "L";
                        expresion = "id_factor= '12' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "12. Compasión: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 13:
                        orden = "M";
                        expresion = "id_factor= '13' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "13. Perspectiva: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 14:
                        orden = "N";
                        expresion = "id_factor= '14' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "14. Intuición: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 15:
                        orden = "O";
                        expresion = "id_factor= '15' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "15. Radio de Confianza: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 16:
                        orden = "P";
                        expresion = "id_factor= '16' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "16. Poder Personal: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 17:
                        orden = "Q";
                        expresion = "id_factor= '17' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "17. Integridad: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 18:
                        orden = "R";
                        expresion = "id_factor= '18' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "18. Salud General: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 19:
                        orden = "S";
                        expresion = "id_factor= '19' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "19. Calidad de vida: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 20:
                        orden = "T";
                        expresion = "id_factor= '20' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "20. Cociente de Relaciones: " + filtro[0]["interpretacion"].ToString();
                        break;

                    case 21:
                        orden = "U";
                        expresion = "id_factor= '21' and punto='" + resultados[i].Total + "'"; ;
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafica"].ToString());
                        idinterpretacion = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = "id_interpretacion = '"+idinterpretacion+"'";
                        filtro = dtinter.Select(expresion);
                        interpretaciones = "21. Óptimo Rendimiento.: " + filtro[0]["interpretacion"].ToString();
                        break;
                }
                row["Idfactor"] = Convert.ToInt32(resultados[i].Idfactor);
                row["Factor"] = Convert.ToString(resultados[i].Factor);
                row["Total"] = Convert.ToInt32(resultados[i].Total);
                row["Percentil"] = percentil;
                row["Orden"] = orden;
                row["Interpretacion"] = interpretaciones;
                dtresultados.Rows.Add(row);
            }

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportDataSource rds = new ReportDataSource("DataSetintel", dtresultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Intelsoft.reporteintel.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.RefreshReport();

        }

       
    }
}
