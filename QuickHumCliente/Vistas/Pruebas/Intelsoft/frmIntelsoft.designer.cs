﻿namespace QuickHumCliente.Vistas.Pruebas.Intelsoft
{
    partial class frmIntelsoft
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblpregunta3 = new System.Windows.Forms.Label();
            this.rbtC1 = new System.Windows.Forms.RadioButton();
            this.rbtC4 = new System.Windows.Forms.RadioButton();
            this.rbtC2 = new System.Windows.Forms.RadioButton();
            this.rbtC3 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblpregunta2 = new System.Windows.Forms.Label();
            this.rbtB1 = new System.Windows.Forms.RadioButton();
            this.rbtB2 = new System.Windows.Forms.RadioButton();
            this.rbtB3 = new System.Windows.Forms.RadioButton();
            this.rbtB4 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtA3 = new System.Windows.Forms.RadioButton();
            this.lblpregunta1 = new System.Windows.Forms.Label();
            this.rbtA1 = new System.Windows.Forms.RadioButton();
            this.rbtA2 = new System.Windows.Forms.RadioButton();
            this.rbtA4 = new System.Windows.Forms.RadioButton();
            this.lblC = new System.Windows.Forms.Label();
            this.lblD = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.lblA = new System.Windows.Forms.Label();
            this.txtescala = new System.Windows.Forms.TextBox();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblpregunta3);
            this.panel3.Controls.Add(this.rbtC1);
            this.panel3.Controls.Add(this.rbtC4);
            this.panel3.Controls.Add(this.rbtC2);
            this.panel3.Controls.Add(this.rbtC3);
            this.panel3.Location = new System.Drawing.Point(28, 250);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(658, 70);
            this.panel3.TabIndex = 22;
            // 
            // lblpregunta3
            // 
            this.lblpregunta3.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblpregunta3.Location = new System.Drawing.Point(4, 4);
            this.lblpregunta3.Name = "lblpregunta3";
            this.lblpregunta3.Size = new System.Drawing.Size(229, 60);
            this.lblpregunta3.TabIndex = 5;
            this.lblpregunta3.Text = "label8";
            // 
            // rbtC1
            // 
            this.rbtC1.AutoSize = true;
            this.rbtC1.Location = new System.Drawing.Point(256, 29);
            this.rbtC1.Name = "rbtC1";
            this.rbtC1.Size = new System.Drawing.Size(14, 13);
            this.rbtC1.TabIndex = 16;
            this.rbtC1.TabStop = true;
            this.rbtC1.UseVisualStyleBackColor = true;
            this.rbtC1.Click += new System.EventHandler(this.rbtC1_Click);
            // 
            // rbtC4
            // 
            this.rbtC4.AutoSize = true;
            this.rbtC4.Location = new System.Drawing.Point(595, 28);
            this.rbtC4.Name = "rbtC4";
            this.rbtC4.Size = new System.Drawing.Size(14, 13);
            this.rbtC4.TabIndex = 19;
            this.rbtC4.TabStop = true;
            this.rbtC4.UseVisualStyleBackColor = true;
            this.rbtC4.Click += new System.EventHandler(this.rbtC4_Click);
            // 
            // rbtC2
            // 
            this.rbtC2.AutoSize = true;
            this.rbtC2.Location = new System.Drawing.Point(369, 29);
            this.rbtC2.Name = "rbtC2";
            this.rbtC2.Size = new System.Drawing.Size(14, 13);
            this.rbtC2.TabIndex = 17;
            this.rbtC2.TabStop = true;
            this.rbtC2.UseVisualStyleBackColor = true;
            this.rbtC2.Click += new System.EventHandler(this.rbtC2_Click);
            // 
            // rbtC3
            // 
            this.rbtC3.AutoSize = true;
            this.rbtC3.Location = new System.Drawing.Point(482, 28);
            this.rbtC3.Name = "rbtC3";
            this.rbtC3.Size = new System.Drawing.Size(14, 13);
            this.rbtC3.TabIndex = 18;
            this.rbtC3.TabStop = true;
            this.rbtC3.UseVisualStyleBackColor = true;
            this.rbtC3.Click += new System.EventHandler(this.rbtC3_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblpregunta2);
            this.panel2.Controls.Add(this.rbtB1);
            this.panel2.Controls.Add(this.rbtB2);
            this.panel2.Controls.Add(this.rbtB3);
            this.panel2.Controls.Add(this.rbtB4);
            this.panel2.Location = new System.Drawing.Point(28, 172);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(658, 70);
            this.panel2.TabIndex = 21;
            // 
            // lblpregunta2
            // 
            this.lblpregunta2.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblpregunta2.Location = new System.Drawing.Point(4, 5);
            this.lblpregunta2.Name = "lblpregunta2";
            this.lblpregunta2.Size = new System.Drawing.Size(229, 58);
            this.lblpregunta2.TabIndex = 6;
            this.lblpregunta2.Text = "label7";
            // 
            // rbtB1
            // 
            this.rbtB1.AutoSize = true;
            this.rbtB1.Location = new System.Drawing.Point(256, 27);
            this.rbtB1.Name = "rbtB1";
            this.rbtB1.Size = new System.Drawing.Size(14, 13);
            this.rbtB1.TabIndex = 12;
            this.rbtB1.TabStop = true;
            this.rbtB1.UseVisualStyleBackColor = true;
            this.rbtB1.Click += new System.EventHandler(this.rbtB1_Click);
            // 
            // rbtB2
            // 
            this.rbtB2.AutoSize = true;
            this.rbtB2.Location = new System.Drawing.Point(369, 27);
            this.rbtB2.Name = "rbtB2";
            this.rbtB2.Size = new System.Drawing.Size(14, 13);
            this.rbtB2.TabIndex = 13;
            this.rbtB2.TabStop = true;
            this.rbtB2.UseVisualStyleBackColor = true;
            this.rbtB2.Click += new System.EventHandler(this.rbtB2_Click);
            // 
            // rbtB3
            // 
            this.rbtB3.AutoSize = true;
            this.rbtB3.Location = new System.Drawing.Point(482, 27);
            this.rbtB3.Name = "rbtB3";
            this.rbtB3.Size = new System.Drawing.Size(14, 13);
            this.rbtB3.TabIndex = 14;
            this.rbtB3.TabStop = true;
            this.rbtB3.UseVisualStyleBackColor = true;
            this.rbtB3.Click += new System.EventHandler(this.rbtB3_Click);
            // 
            // rbtB4
            // 
            this.rbtB4.AutoSize = true;
            this.rbtB4.Location = new System.Drawing.Point(595, 27);
            this.rbtB4.Name = "rbtB4";
            this.rbtB4.Size = new System.Drawing.Size(14, 13);
            this.rbtB4.TabIndex = 15;
            this.rbtB4.TabStop = true;
            this.rbtB4.UseVisualStyleBackColor = true;
            this.rbtB4.Click += new System.EventHandler(this.rbtB4_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbtA3);
            this.panel1.Controls.Add(this.lblpregunta1);
            this.panel1.Controls.Add(this.rbtA1);
            this.panel1.Controls.Add(this.rbtA2);
            this.panel1.Controls.Add(this.rbtA4);
            this.panel1.Location = new System.Drawing.Point(28, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(658, 70);
            this.panel1.TabIndex = 20;
            // 
            // rbtA3
            // 
            this.rbtA3.AutoSize = true;
            this.rbtA3.Location = new System.Drawing.Point(482, 27);
            this.rbtA3.Name = "rbtA3";
            this.rbtA3.Size = new System.Drawing.Size(14, 13);
            this.rbtA3.TabIndex = 10;
            this.rbtA3.TabStop = true;
            this.rbtA3.UseVisualStyleBackColor = true;
            this.rbtA3.Click += new System.EventHandler(this.rbtA3_Click);
            // 
            // lblpregunta1
            // 
            this.lblpregunta1.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblpregunta1.Location = new System.Drawing.Point(7, 5);
            this.lblpregunta1.Name = "lblpregunta1";
            this.lblpregunta1.Size = new System.Drawing.Size(225, 60);
            this.lblpregunta1.TabIndex = 7;
            this.lblpregunta1.Text = "label6";
            // 
            // rbtA1
            // 
            this.rbtA1.AutoSize = true;
            this.rbtA1.Location = new System.Drawing.Point(256, 28);
            this.rbtA1.Name = "rbtA1";
            this.rbtA1.Size = new System.Drawing.Size(14, 13);
            this.rbtA1.TabIndex = 8;
            this.rbtA1.TabStop = true;
            this.rbtA1.UseVisualStyleBackColor = true;
            this.rbtA1.Click += new System.EventHandler(this.rbtA1_Click);
            // 
            // rbtA2
            // 
            this.rbtA2.AutoSize = true;
            this.rbtA2.Location = new System.Drawing.Point(369, 28);
            this.rbtA2.Name = "rbtA2";
            this.rbtA2.Size = new System.Drawing.Size(14, 13);
            this.rbtA2.TabIndex = 9;
            this.rbtA2.TabStop = true;
            this.rbtA2.UseVisualStyleBackColor = true;
            this.rbtA2.Click += new System.EventHandler(this.rbtA2_Click);
            // 
            // rbtA4
            // 
            this.rbtA4.AutoSize = true;
            this.rbtA4.Location = new System.Drawing.Point(596, 27);
            this.rbtA4.Name = "rbtA4";
            this.rbtA4.Size = new System.Drawing.Size(14, 13);
            this.rbtA4.TabIndex = 11;
            this.rbtA4.TabStop = true;
            this.rbtA4.UseVisualStyleBackColor = true;
            this.rbtA4.Click += new System.EventHandler(this.rbtA4_Click);
            // 
            // lblC
            // 
            this.lblC.BackColor = System.Drawing.Color.Transparent;
            this.lblC.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblC.Location = new System.Drawing.Point(471, 34);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(113, 44);
            this.lblC.TabIndex = 4;
            this.lblC.Text = "label5";
            this.lblC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblD
            // 
            this.lblD.BackColor = System.Drawing.Color.Transparent;
            this.lblD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblD.Location = new System.Drawing.Point(590, 34);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(95, 44);
            this.lblD.TabIndex = 3;
            this.lblD.Text = "label4";
            this.lblD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblB
            // 
            this.lblB.BackColor = System.Drawing.Color.Transparent;
            this.lblB.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblB.Location = new System.Drawing.Point(353, 34);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(112, 44);
            this.lblB.TabIndex = 2;
            this.lblB.Text = "label2";
            this.lblB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblA
            // 
            this.lblA.BackColor = System.Drawing.Color.Transparent;
            this.lblA.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblA.Location = new System.Drawing.Point(244, 34);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(103, 44);
            this.lblA.TabIndex = 1;
            this.lblA.Text = "label1";
            this.lblA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtescala
            // 
            this.txtescala.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtescala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtescala.Font = new System.Drawing.Font("Calibri", 11F);
            this.txtescala.Location = new System.Drawing.Point(27, 34);
            this.txtescala.Multiline = true;
            this.txtescala.Name = "txtescala";
            this.txtescala.ReadOnly = true;
            this.txtescala.Size = new System.Drawing.Size(211, 44);
            this.txtescala.TabIndex = 0;
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.Resources.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(223, 341);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnAyuda.TabIndex = 76;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.Resources.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(383, 341);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(118, 33);
            this.btnSiguiente.TabIndex = 75;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(716, 36);
            this.label3.TabIndex = 77;
            this.label3.Text = "INTELSOFT - PRUEBA DE INTELIGENCIA EMOCIONAL";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoScroll = true;
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.txtescala);
            this.groupBox1.Controls.Add(this.lblA);
            this.groupBox1.Controls.Add(this.btnAyuda);
            this.groupBox1.Controls.Add(this.lblB);
            this.groupBox1.Controls.Add(this.btnSiguiente);
            this.groupBox1.Controls.Add(this.lblC);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.lblD);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(716, 400);
            this.groupBox1.TabIndex = 78;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.button1.BackgroundImage = global::QuickHumCliente.Properties.Resources.close;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(681, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 35);
            this.button1.TabIndex = 79;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmIntelsoft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(716, 436);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Name = "frmIntelsoft";
            this.Text = "Intelsoft";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIntelsoft_FormClosing);
            this.Load += new System.EventHandler(this.frmIntelsoft_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtescala;
        private System.Windows.Forms.Label lblpregunta1;
        private System.Windows.Forms.Label lblpregunta2;
        private System.Windows.Forms.Label lblpregunta3;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lblD;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbtA3;
        private System.Windows.Forms.RadioButton rbtA1;
        private System.Windows.Forms.RadioButton rbtA2;
        private System.Windows.Forms.RadioButton rbtA4;
        private System.Windows.Forms.RadioButton rbtC4;
        private System.Windows.Forms.RadioButton rbtC3;
        private System.Windows.Forms.RadioButton rbtC2;
        private System.Windows.Forms.RadioButton rbtC1;
        private System.Windows.Forms.RadioButton rbtB4;
        private System.Windows.Forms.RadioButton rbtB3;
        private System.Windows.Forms.RadioButton rbtB2;
        private System.Windows.Forms.RadioButton rbtB1;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Button button1;
    }
}