﻿using System;
using System.Data;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Intelsoft
{
    public partial class frmIntelsoft : Form
    {
        public frmIntelsoft(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        private QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        private readonly EscolaridadPrueba _escolaridad = null;
        dto_intel_respuestas_candidato respcandidato = new dto_intel_respuestas_candidato();
        INTELSOFT intel = new INTELSOFT();
        DataTable dtrespuestas = new DataTable();
        DataTable dtpreguntas = new DataTable();
        DataTable dtactualizar = new DataTable();
        DataRow[] f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18, f19, f20, f21;
        int r1 = 0, r2 = 0, r3 = 0, id1 = 0, id2 = 0, id3 = 0, num = 0, numero_prueba = 0;
        string expresion;
        int x = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, a = 0, z = 0, k = 0, l = 0, m = 0, n = 0, o = 0, p = 0, q = 0, r = 0, s = 0, t = 0, u = 0, v = 0, resta = 0;

        private void frmIntelsoft_Load(object sender, EventArgs e)
        {
            try
            {
                intel = cliente.NuevoIntel(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            numero_prueba = intel.IdPrueba;
            listardatos();
            this.btnSiguiente.Focus();
        }

        private void listardatos()
        {
            dto_intel_preguntas[] preguntas = cliente.ListadoIntelPreguntas();
            dtpreguntas.Columns.Add("idpregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("idfactor");

            dtrespuestas.Columns.Add("idrespuesta");
            dtrespuestas.Columns.Add("respuesta");
            dtrespuestas.Columns.Add("id_pregunta");
            dtrespuestas.Columns.Add("puntuacion");

            for (int i = 0; i < preguntas.Length; i++)
            {
                DataRow row = dtpreguntas.NewRow();
                row["idpregunta"] = Convert.ToInt32(preguntas[i].IdPregunta);
                row["pregunta"] = Convert.ToString(preguntas[i].Pregunta);
                row["idfactor"] = Convert.ToInt32(preguntas[i].Idfactor);

                for (int j = 0; j < 4; j++)
                {
                    DataRow fila = dtrespuestas.NewRow();
                    fila["idrespuesta"] = Convert.ToInt32(preguntas[i].Respuesta[j].Idrespuesta);
                    fila["respuesta"] = Convert.ToString(preguntas[i].Respuesta[j].Respuesta);
                    fila["id_pregunta"] = Convert.ToInt32(preguntas[i].Respuesta[j].IdPregunta);
                    fila["puntuacion"] = Convert.ToInt32(preguntas[i].Respuesta[j].Puntuacion);
                    dtrespuestas.Rows.Add(fila);
                }
                dtpreguntas.Rows.Add(row);
            }
            txtescala.Text = "Escala 1: Sucesos en la vida";

            mensajes();

            preguntass();
        }
        private void preguntass()
        {

            expresion = "idfactor = '1'";
            f1 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '2'";
            f2 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '3'";
            f3 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '4'";
            f4 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '5'";
            f5 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '6'";
            f6 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '7'";
            f7 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '8'";
            f8 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '9'";
            f9 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '10'";
            f10 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '11'";
            f11 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '12'";
            f12 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '13'";
            f13 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '14'";
            f14 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '15'";
            f15 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '16'";
            f16 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '17'";
            f17 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '18'";
            f18 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '19'";
            f19 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '20'";
            f20 = dtpreguntas.Select(expresion);

            expresion = "idfactor = '21'";
            f21 = dtpreguntas.Select(expresion);

            lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f1[0]["pregunta"].ToString();
            lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f1[1]["pregunta"].ToString();
            lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f1[2]["pregunta"].ToString();
        }
        private void ayuda()
        {
            if (x >= 0 && x < 6)
            {
                MessageBox.Show("Piense en el Año Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada uno de los sucesos de trabajo y personales que se anotan.");

            }
            else if (x >= 6 && x < 12)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada una de las presiones del trabajo que se anotan.");

            }

            else if (x >= 12 && x < 17)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada una de las presiones personales que se anotan.");

            }

            else if (x >= 17 && x < 21)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted. Esto me describe:");
            }

            else if (x >= 21 && x < 24)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe:");

            }

            else if (x >= 24 && x < 29)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");

            }

            else if (x >= 29 && x < 34)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe");

            }

            else if (x >= 34 && x < 38)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe");

            }

            else if (x >= 38 && x < 43)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");

            }

            else if (x >= 43 && x < 47)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 47 && x < 52)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 52 && x < 56)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 56 && x < 59)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");

            }

            else if (x >= 59 && x < 63)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 63 && x < 67)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");

            }

            else if (x >= 67 && x < 72)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 72 && x < 75)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");

            }

            else if (x >= 75 && x < 86)
            {
                MessageBox.Show("Piense en el Mes Pasado. Indique con qué frecuencia ha experimentado los síntomas siguientes.");

            }

            else if (x >= 86 && x < 90)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");

            }

            else if (x >= 90 && x < 93)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");

            }

            else if (x >= 93 && x < 96)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe:");

            }
        }
        private void mensajes()
        {
            if (x == 0)
            {
                MessageBox.Show("Piense en el Año Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada uno de los sucesos de trabajo y personales que se anotan.");
                lblA.Text = dtrespuestas.Rows[0]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[1]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[2]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[3]["respuesta"].ToString();
            }
            else if (x == 6)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada una de las presiones del trabajo que se anotan.");
                lblA.Text = dtrespuestas.Rows[72]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[73]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[74]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[75]["respuesta"].ToString();
            }

            else if (x == 12)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cuánta aflicción o angustia le causó cada una de las presiones personales que se anotan.");
                lblA.Text = dtrespuestas.Rows[140]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[141]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[142]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[143]["respuesta"].ToString();
            }

            else if (x == 17)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[196]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[197]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[198]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[199]["respuesta"].ToString();
            }

            else if (x == 21)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[240]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[241]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[242]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[243]["respuesta"].ToString();
            }

            else if (x == 24)
            {
                MessageBox.Show("En la siguiente indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[276]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[277]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[278]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[279]["respuesta"].ToString();
            }

            else if (x == 29)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe");
                lblA.Text = dtrespuestas.Rows[328]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[329]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[330]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[331]["respuesta"].ToString();
            }

            else if (x == 34)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe");
                lblA.Text = dtrespuestas.Rows[384]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[385]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[386]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[387]["respuesta"].ToString();
            }

            else if (x == 38)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[424]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[425]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[426]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[427]["respuesta"].ToString();
            }

            else if (x == 43)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[476]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[477]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[478]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[479]["respuesta"].ToString();
            }

            else if (x == 47)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[516]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[517]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[518]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[519]["respuesta"].ToString();
            }

            else if (x == 52)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[568]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[569]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[570]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[571]["respuesta"].ToString();
            }

            else if (x == 56)
            {
                MessageBox.Show(": Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[616]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[617]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[618]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[619]["respuesta"].ToString();
            }

            else if (x == 59)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[648]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[649]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[650]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[651]["respuesta"].ToString();
            }

            else if (x == 63)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[692]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[693]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[694]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[695]["respuesta"].ToString();
            }

            else if (x == 67)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[732]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[733]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[734]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[735]["respuesta"].ToString();
            }

            else if (x == 72)
            {
                MessageBox.Show("Piense en el Mes Pasado. En la lista siguiente indique cómo describe cada renglón su conducta o intención. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[784]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[785]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[786]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[787]["respuesta"].ToString();
            }

            else if (x == 75)
            {
                MessageBox.Show("Piense en el Mes Pasado. Indique con qué frecuencia ha experimentado los síntomas siguientes.");
                lblA.Text = dtrespuestas.Rows[820]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[821]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[822]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[823]["respuesta"].ToString();
            }

            else if (x == 86)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[948]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[949]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[950]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[951]["respuesta"].ToString();
            }

            else if (x == 90)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe: ");
                lblA.Text = dtrespuestas.Rows[992]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[993]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[994]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[995]["respuesta"].ToString();
            }

            else if (x == 93)
            {
                MessageBox.Show("En la siguiente lista indique cómo describe cada renglón la manera como usted piensa y siente sobre usted mismo actualmente. Esto me describe:");
                lblA.Text = dtrespuestas.Rows[1020]["respuesta"].ToString();
                lblB.Text = dtrespuestas.Rows[1021]["respuesta"].ToString();
                lblC.Text = dtrespuestas.Rows[1022]["respuesta"].ToString();
                lblD.Text = dtrespuestas.Rows[1023]["respuesta"].ToString();
            }
        }
        private void Restar()
        {

            if (panel2.Visible == false && panel3.Visible == false)
            {
                resta = resta + 8;
                num = num + 2;
            }
            else if (panel3.Visible == false)
            {
                resta = resta + 4;
                num = num + 1;
            }
        }
        private void Evaluar()
        {
            if (panel1.Visible == true)
            {
                if (rbtA1.Checked == true)
                {
                    r1 = Convert.ToInt32(dtrespuestas.Rows[((x * 12) - 12) - resta]["puntuacion"].ToString());
                    id1 = Convert.ToInt16(dtrespuestas.Rows[((x * 12) - 12) - resta]["idrespuesta"].ToString());
                }
                else if (rbtA2.Checked == true)
                {
                    r1 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 1) - resta]["puntuacion"].ToString());
                    id1 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 1) - resta]["idrespuesta"].ToString());
                }
                else if (rbtA3.Checked == true)
                {
                    r1 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 2) - resta]["puntuacion"].ToString());
                    id1 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 2) - resta]["idrespuesta"].ToString());
                }
                else if (rbtA4.Checked == true)
                {
                    r1 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 3) - resta]["puntuacion"].ToString());
                    id1 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 3) - resta]["idrespuesta"].ToString());
                }
            }

            if (panel2.Visible == true)
            {
                if (rbtB1.Checked == true)
                {
                    r2 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 4) - resta]["puntuacion"].ToString());
                    id2 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 4) - resta]["idrespuesta"].ToString());
                }
                else if (rbtB2.Checked == true)
                {
                    r2 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 5) - resta]["puntuacion"].ToString());
                    id2 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 5) - resta]["idrespuesta"].ToString());
                }
                else if (rbtB3.Checked == true)
                {
                    r2 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 6) - resta]["puntuacion"].ToString());
                    id2 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 6) - resta]["idrespuesta"].ToString());
                }
                else if (rbtB4.Checked == true)
                {
                    r2 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 7) - resta]["puntuacion"].ToString());
                    id2 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 7) - resta]["idrespuesta"].ToString());
                }
            }

            if (panel3.Visible == true)
            {
                if (rbtC1.Checked == true)
                {
                    r3 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 8) - resta]["puntuacion"].ToString());
                    id3 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 8) - resta]["idrespuesta"].ToString());
                }
                else if (rbtC2.Checked == true)
                {
                    r3 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 9) - resta]["puntuacion"].ToString());
                    id3 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 9) - resta]["idrespuesta"].ToString());
                }
                else if (rbtC3.Checked == true)
                {
                    r3 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 10) - resta]["puntuacion"].ToString());
                    id3 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 10) - resta]["idrespuesta"].ToString());
                }
                else if (rbtC4.Checked == true)
                {
                    r3 = Convert.ToInt32(dtrespuestas.Rows[(((x * 12) - 12) + 11) - resta]["puntuacion"].ToString());
                    id3 = Convert.ToInt16(dtrespuestas.Rows[(((x * 12) - 12) + 11) - resta]["idrespuesta"].ToString());
                }
            }
        }
        private void GuardarRespuestas()
        {
            Evaluar();

            if (panel1.Visible == true)
            {
                respcandidato.Idrespuesta = id1;
                respcandidato.Puntaje = r1;
                respcandidato.NumeroPrueba = numero_prueba;
                cliente.InsertaIntelRespuesta(respcandidato);
            }
            if (panel2.Visible == true)
            {
                respcandidato.Idrespuesta = id2;
                respcandidato.Puntaje = r2;
                respcandidato.NumeroPrueba = numero_prueba;
                cliente.InsertaIntelRespuesta(respcandidato);
            }
            if (panel3.Visible == true)
            {
                respcandidato.Idrespuesta = id3;
                respcandidato.Puntaje = r3;
                respcandidato.NumeroPrueba = numero_prueba;
                cliente.InsertaIntelRespuesta(respcandidato);
            }
        }
        private void MostrarPreguntas(int i)
        {

            if (x <= 5)
            {
                txtescala.Text = "Escala 1: Sucesos en la vida";
                lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f1[x * 3]["pregunta"].ToString();
                lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f1[(x * 3) + 1]["pregunta"].ToString();
                lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f1[(x * 3) + 2]["pregunta"].ToString();
            }
            else if (x >= 6 && x <= 11)
            {
                txtescala.Text = "Escala 2: Presiones del Trabajo";

                if (b == 5)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f2[b * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f2[(b * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f2[b * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f2[(b * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f2[(b * 3) + 2]["pregunta"].ToString();
                }
                b++;

            }
            else if (x >= 12 && x <= 16)
            {
                txtescala.Text = "Escala 3: Presiones Personales";
                panel3.Visible = true;
                if (c == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f3[c * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f3[(c * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f3[c * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f3[(c * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f3[(c * 3) + 2]["pregunta"].ToString();
                }
                c++;
            }
            else if (x >= 17 && x <= 20)
            {
                txtescala.Text = "Escala 4: Conciencia Emocional de si mismo";
                panel3.Visible = true;
                if (d == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f4[d * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f4[(d * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f4[d * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f4[(d * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f4[(d * 3) + 2]["pregunta"].ToString();
                }
                d++;
            }

            else if (x >= 21 && x <= 23)
            {
                txtescala.Text = "Escala 5: Expresion Emocional";
                panel3.Visible = true;
                lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f5[e * 3]["pregunta"].ToString();
                lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f5[(e * 3) + 1]["pregunta"].ToString();
                lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f5[(e * 3) + 2]["pregunta"].ToString();
                e++;
            }

            else if (x >= 24 && x <= 28)
            {
                txtescala.Text = "Escala 6: Conciencia Emocional de otros";
                panel3.Visible = true;
                if (f == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f6[f * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f6[f * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f6[(f * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f6[(f * 3) + 2]["pregunta"].ToString();
                }
                f++;
            }

            else if (x >= 29 && x <= 33)
            {
                txtescala.Text = "Escala 7: Intención";
                panel2.Visible = true;
                panel3.Visible = true;
                if (g == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f7[g * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f7[(g * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f7[g * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f7[(g * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f7[(g * 3) + 2]["pregunta"].ToString();
                }
                g++;
            }
            else if (x >= 34 && x <= 37)
            {
                txtescala.Text = "Escala 8: Creatividad";
                panel2.Visible = true;
                panel3.Visible = true;
                if (h == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f8[h * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f8[h * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f8[(h * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f8[(h * 3) + 2]["pregunta"].ToString();
                }
                h++;
            }

            else if (x >= 38 && x <= 42)
            {
                txtescala.Text = "Escala 9: Elasticidad";
                panel2.Visible = true;
                panel3.Visible = true;
                if (a == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f9[a * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f9[a * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f9[(a * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f9[(a * 3) + 2]["pregunta"].ToString();
                }
                a++;
            }

            else if (x >= 43 && x <= 46)
            {
                txtescala.Text = "Escala 10: Conexiones Interpersonales";
                panel2.Visible = true;
                panel3.Visible = true;
                if (z == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f10[z * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f10[z * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f10[(z * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f10[(z * 3) + 2]["pregunta"].ToString();
                }
                z++;
            }

            else if (x >= 47 && x <= 51)
            {
                txtescala.Text = "Escala 11: Descontento Constructivo";
                panel2.Visible = true;
                panel3.Visible = true;
                if (k == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f11[k * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f11[k * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f11[(k * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f11[(k * 3) + 2]["pregunta"].ToString();
                }
                k++;
            }

            else if (x >= 52 && x <= 55)
            {
                txtescala.Text = "Escala 12: Compasión";
                panel2.Visible = true;
                panel3.Visible = true;

                lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f12[l * 3]["pregunta"].ToString();
                lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f12[(l * 3) + 1]["pregunta"].ToString();
                lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f12[(l * 3) + 2]["pregunta"].ToString();
                l++;
            }

            else if (x >= 56 && x <= 58)
            {
                txtescala.Text = "Escala 13: Perspectiva";
                panel2.Visible = true;
                panel3.Visible = true;
                if (m == 2)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f13[m * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f13[(m * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f13[m * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f13[(m * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f13[(m * 3) + 2]["pregunta"].ToString();
                }
                m++;
            }

            else if (x >= 59 && x <= 62)
            {
                txtescala.Text = "Escala 14: Intuición";
                panel2.Visible = true;
                panel3.Visible = true;
                if (n == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f14[n * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f14[(n * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f14[n * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f14[(n * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f14[(n * 3) + 2]["pregunta"].ToString();
                }
                n++;
            }

            else if (x >= 63 && x <= 66)
            {
                txtescala.Text = "Escala 15: Radio de Confianza";
                panel2.Visible = true;
                panel3.Visible = true;
                if (o == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f15[o * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f15[o * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f15[(o * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f15[(o * 3) + 2]["pregunta"].ToString();
                }
                o++;
            }

            else if (x >= 67 && x <= 71)
            {
                txtescala.Text = "Escala 16: Poder Personal";
                panel2.Visible = true;
                panel3.Visible = true;
                if (p == 4)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f16[p * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f16[p * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f16[(p * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f16[(p * 3) + 2]["pregunta"].ToString();
                }
                p++;
            }

            else if (x >= 72 && x <= 74)
            {
                txtescala.Text = "Escala 17: Integridad";
                panel2.Visible = true;
                panel3.Visible = true;
                lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f17[q * 3]["pregunta"].ToString();
                lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f17[(q * 3) + 1]["pregunta"].ToString();
                lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f17[(q * 3) + 2]["pregunta"].ToString();
                q++;
            }

            else if (x >= 75 && x <= 85)
            {
                txtescala.Text = "Escala 18: Salud General";
                panel2.Visible = true;
                panel3.Visible = true;
                if (r == 10)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f18[r * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f18[(r * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f18[r * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f18[(r * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f18[(r * 3) + 2]["pregunta"].ToString();
                }
                r++;
            }

            else if (x >= 86 && x <= 89)
            {
                txtescala.Text = "Escala 19: Calidad de vida";
                panel2.Visible = true;
                panel3.Visible = true;
                if (s == 3)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f19[s * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f19[(s * 3) + 1]["pregunta"].ToString();
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f19[s * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f19[(s * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f19[(s * 3) + 2]["pregunta"].ToString();
                }
                s++;
            }

            else if (x >= 90 && x <= 92)
            {
                txtescala.Text = "Escala 20: Cociente de Relaciones";
                panel2.Visible = true;
                panel3.Visible = true;
                if (t == 2)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f20[t * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f20[t * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f20[(t * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f20[(t * 3) + 2]["pregunta"].ToString();
                }
                t++;
            }

            else if (x >= 93 && x <= 95)
            {
                txtescala.Text = "Escala 21:Óptimo Rendimiento.";
                panel2.Visible = true;
                panel3.Visible = true;
                if (u == 2)
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f21[u * 3]["pregunta"].ToString();
                    panel2.Visible = false;
                    panel3.Visible = false;
                }
                else
                {
                    lblpregunta1.Text = (((x * 3) + 1) - num) + "- " + f21[u * 3]["pregunta"].ToString();
                    lblpregunta2.Text = (((x * 3) + 2) - num) + "- " + f21[(u * 3) + 1]["pregunta"].ToString();
                    lblpregunta3.Text = (((x * 3) + 3) - num) + "- " + f21[(u * 3) + 2]["pregunta"].ToString();
                }
                u++;
            }

        }
        private void validacion()
        {
            if (panel1.Visible == true && panel2.Visible == true && panel3.Visible == true)
            {
                if ((rbtA1.Checked == true || rbtA2.Checked == true || rbtA3.Checked == true || rbtA4.Checked == true) && (rbtB1.Checked == true || rbtB2.Checked == true || rbtB3.Checked == true || rbtB4.Checked == true) && (rbtC1.Checked == true || rbtC2.Checked == true || rbtC3.Checked == true || rbtC4.Checked == true))
                {
                    btnSiguiente.Enabled = true; btnSiguiente.Focus();
                }
                else
                { btnSiguiente.Enabled = false; }
            }

            if (panel3.Visible == false)
            {
                if ((rbtA1.Checked == true || rbtA2.Checked == true || rbtA3.Checked == true || rbtA4.Checked == true) && (rbtB1.Checked == true || rbtB2.Checked == true || rbtB3.Checked == true || rbtB4.Checked == true))
                {
                    btnSiguiente.Enabled = true; btnSiguiente.Focus();
                }
                else
                { btnSiguiente.Enabled = false; }
            }

            if (panel2.Visible == false && panel3.Visible == false)
            {
                if ((rbtA1.Checked == true || rbtA2.Checked == true || rbtA3.Checked == true || rbtA4.Checked == true))
                {
                    btnSiguiente.Enabled = true; btnSiguiente.Focus();
                }
                else
                { btnSiguiente.Enabled = false; }
            }


        }
        private void limpiarcampos()
        {
            foreach (Control radio in this.panel1.Controls)
            {
                if (radio is RadioButton)
                {

                    ((RadioButton)radio).Checked = false;
                }
            }

            foreach (Control radio in this.panel2.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
            foreach (Control radio in this.panel3.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {

            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                txtescala.Clear();
                GuardarRespuestas();
                Restar();
                MostrarPreguntas(x);
                mensajes();
                limpiarcampos();
            }

            if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Intelsoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    x++;
                    txtescala.Text = "Escala 21:Óptimo Rendimiento.";
                    GuardarRespuestas();
                    respcandidato.NumeroPrueba = numero_prueba;
                    cliente.FinalizaIntelPrueba(respcandidato);
                    this.Close();
                }
            }
            if (x == 95)
            {
                btnSiguiente.Text = "Finalizar";
            }
            btnSiguiente.Enabled = false;
        }
        #region Validar los Radiobuttons
        private void rbtA1_Click(object sender, EventArgs e)
        {

            validacion();
        }

        private void rbtA2_Click(object sender, EventArgs e)
        {

            validacion();
        }

        private void rbtA3_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtA4_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtB1_Click(object sender, EventArgs e)
        {

            validacion();
        }

        private void rbtB2_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtB3_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtB4_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtC1_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtC2_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtC3_Click(object sender, EventArgs e)
        {
            validacion();
        }

        private void rbtC4_Click(object sender, EventArgs e)
        {
            validacion();
        }
        #endregion
        private void btnAyuda_Click(object sender, EventArgs e)
        {
            ayuda();
        }

        private void frmIntelsoft_FormClosing(object sender, FormClosingEventArgs e)
        {
            respcandidato.NumeroPrueba = numero_prueba;
            cliente.FinalizaIntelPrueba(respcandidato);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Esta seguro que quieres salir de la prueba? ", "Prueba Intelsoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                this.Close();
            }
        }

    }
}
