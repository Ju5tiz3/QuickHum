﻿namespace QuickHumCliente.Vistas.Pruebas.Luscher
{
    partial class frmLuscher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLuscher));
            this.gbOpcionesAzul = new System.Windows.Forms.GroupBox();
            this.imgA4 = new System.Windows.Forms.PictureBox();
            this.imgA3 = new System.Windows.Forms.PictureBox();
            this.imgA2 = new System.Windows.Forms.PictureBox();
            this.imgA1 = new System.Windows.Forms.PictureBox();
            this.gbColoresOrdenadosAzul = new System.Windows.Forms.GroupBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.gbColoresOrdenadosVerde = new System.Windows.Forms.GroupBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.gbOpcionesVerde = new System.Windows.Forms.GroupBox();
            this.imgV4 = new System.Windows.Forms.PictureBox();
            this.imgV3 = new System.Windows.Forms.PictureBox();
            this.imgV2 = new System.Windows.Forms.PictureBox();
            this.imgV1 = new System.Windows.Forms.PictureBox();
            this.gbColoresOrdenadosAmarillo = new System.Windows.Forms.GroupBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.gbColoresOrdenadosRojo = new System.Windows.Forms.GroupBox();
            this.gbOpcionesAmarillo = new System.Windows.Forms.GroupBox();
            this.imgY1 = new System.Windows.Forms.PictureBox();
            this.imgY4 = new System.Windows.Forms.PictureBox();
            this.imgY2 = new System.Windows.Forms.PictureBox();
            this.imgY3 = new System.Windows.Forms.PictureBox();
            this.gbOpcionesRojo = new System.Windows.Forms.GroupBox();
            this.imgR4 = new System.Windows.Forms.PictureBox();
            this.imgR2 = new System.Windows.Forms.PictureBox();
            this.imgR3 = new System.Windows.Forms.PictureBox();
            this.imgR1 = new System.Windows.Forms.PictureBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCloseVentana = new System.Windows.Forms.Button();
            this.gbOpcionesAzul.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgA4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA1)).BeginInit();
            this.gbColoresOrdenadosAzul.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.gbColoresOrdenadosVerde.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            this.gbOpcionesVerde.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgV4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV1)).BeginInit();
            this.gbColoresOrdenadosAmarillo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            this.gbColoresOrdenadosRojo.SuspendLayout();
            this.gbOpcionesAmarillo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgY1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY3)).BeginInit();
            this.gbOpcionesRojo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgR4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR1)).BeginInit();
            this.panelAyuda.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbOpcionesAzul
            // 
            this.gbOpcionesAzul.Controls.Add(this.imgA4);
            this.gbOpcionesAzul.Controls.Add(this.imgA3);
            this.gbOpcionesAzul.Controls.Add(this.imgA2);
            this.gbOpcionesAzul.Controls.Add(this.imgA1);
            this.gbOpcionesAzul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOpcionesAzul.Location = new System.Drawing.Point(29, 45);
            this.gbOpcionesAzul.Name = "gbOpcionesAzul";
            this.gbOpcionesAzul.Size = new System.Drawing.Size(361, 104);
            this.gbOpcionesAzul.TabIndex = 0;
            this.gbOpcionesAzul.TabStop = false;
            this.gbOpcionesAzul.Text = "Opciones de color Azul";
            // 
            // imgA4
            // 
            this.imgA4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.imgA4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgA4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgA4.Location = new System.Drawing.Point(268, 17);
            this.imgA4.Name = "imgA4";
            this.imgA4.Size = new System.Drawing.Size(80, 80);
            this.imgA4.TabIndex = 3;
            this.imgA4.TabStop = false;
            // 
            // imgA3
            // 
            this.imgA3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.imgA3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgA3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgA3.Location = new System.Drawing.Point(182, 17);
            this.imgA3.Name = "imgA3";
            this.imgA3.Size = new System.Drawing.Size(80, 80);
            this.imgA3.TabIndex = 2;
            this.imgA3.TabStop = false;
            // 
            // imgA2
            // 
            this.imgA2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(255)))));
            this.imgA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgA2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgA2.Location = new System.Drawing.Point(96, 17);
            this.imgA2.Name = "imgA2";
            this.imgA2.Size = new System.Drawing.Size(80, 80);
            this.imgA2.TabIndex = 1;
            this.imgA2.TabStop = false;
            // 
            // imgA1
            // 
            this.imgA1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(11)))), ((int)(((byte)(255)))));
            this.imgA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgA1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgA1.Location = new System.Drawing.Point(10, 17);
            this.imgA1.Name = "imgA1";
            this.imgA1.Size = new System.Drawing.Size(80, 80);
            this.imgA1.TabIndex = 0;
            this.imgA1.TabStop = false;
            this.imgA1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imgA1_MouseDown);
            // 
            // gbColoresOrdenadosAzul
            // 
            this.gbColoresOrdenadosAzul.Controls.Add(this.pictureBox17);
            this.gbColoresOrdenadosAzul.Controls.Add(this.pictureBox18);
            this.gbColoresOrdenadosAzul.Controls.Add(this.pictureBox19);
            this.gbColoresOrdenadosAzul.Controls.Add(this.pictureBox20);
            this.gbColoresOrdenadosAzul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbColoresOrdenadosAzul.Location = new System.Drawing.Point(29, 160);
            this.gbColoresOrdenadosAzul.Name = "gbColoresOrdenadosAzul";
            this.gbColoresOrdenadosAzul.Size = new System.Drawing.Size(361, 104);
            this.gbColoresOrdenadosAzul.TabIndex = 1;
            this.gbColoresOrdenadosAzul.TabStop = false;
            this.gbColoresOrdenadosAzul.Text = "Orden de Colores Azul";
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.White;
            this.pictureBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox17.Location = new System.Drawing.Point(11, 17);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(80, 80);
            this.pictureBox17.TabIndex = 7;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.White;
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Location = new System.Drawing.Point(97, 17);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(80, 80);
            this.pictureBox18.TabIndex = 6;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.White;
            this.pictureBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox19.Location = new System.Drawing.Point(183, 17);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(80, 80);
            this.pictureBox19.TabIndex = 5;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.White;
            this.pictureBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox20.Location = new System.Drawing.Point(268, 17);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(80, 80);
            this.pictureBox20.TabIndex = 4;
            this.pictureBox20.TabStop = false;
            // 
            // gbColoresOrdenadosVerde
            // 
            this.gbColoresOrdenadosVerde.Controls.Add(this.pictureBox21);
            this.gbColoresOrdenadosVerde.Controls.Add(this.pictureBox22);
            this.gbColoresOrdenadosVerde.Controls.Add(this.pictureBox23);
            this.gbColoresOrdenadosVerde.Controls.Add(this.pictureBox24);
            this.gbColoresOrdenadosVerde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbColoresOrdenadosVerde.Location = new System.Drawing.Point(29, 394);
            this.gbColoresOrdenadosVerde.Name = "gbColoresOrdenadosVerde";
            this.gbColoresOrdenadosVerde.Size = new System.Drawing.Size(361, 104);
            this.gbColoresOrdenadosVerde.TabIndex = 3;
            this.gbColoresOrdenadosVerde.TabStop = false;
            this.gbColoresOrdenadosVerde.Text = "Orden de Colores Verde";
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.White;
            this.pictureBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox21.Location = new System.Drawing.Point(11, 18);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(80, 80);
            this.pictureBox21.TabIndex = 11;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.White;
            this.pictureBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox22.Location = new System.Drawing.Point(96, 18);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(80, 80);
            this.pictureBox22.TabIndex = 10;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.White;
            this.pictureBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox23.Location = new System.Drawing.Point(182, 18);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(80, 80);
            this.pictureBox23.TabIndex = 9;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.White;
            this.pictureBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox24.Location = new System.Drawing.Point(268, 18);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(80, 80);
            this.pictureBox24.TabIndex = 8;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.White;
            this.pictureBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox25.Location = new System.Drawing.Point(12, 17);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(80, 80);
            this.pictureBox25.TabIndex = 11;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.White;
            this.pictureBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox26.Location = new System.Drawing.Point(98, 17);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(80, 80);
            this.pictureBox26.TabIndex = 10;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.White;
            this.pictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox27.Location = new System.Drawing.Point(184, 17);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(80, 80);
            this.pictureBox27.TabIndex = 9;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.White;
            this.pictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox28.Location = new System.Drawing.Point(270, 17);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(80, 80);
            this.pictureBox28.TabIndex = 8;
            this.pictureBox28.TabStop = false;
            // 
            // gbOpcionesVerde
            // 
            this.gbOpcionesVerde.Controls.Add(this.imgV4);
            this.gbOpcionesVerde.Controls.Add(this.imgV3);
            this.gbOpcionesVerde.Controls.Add(this.imgV2);
            this.gbOpcionesVerde.Controls.Add(this.imgV1);
            this.gbOpcionesVerde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOpcionesVerde.Location = new System.Drawing.Point(29, 278);
            this.gbOpcionesVerde.Name = "gbOpcionesVerde";
            this.gbOpcionesVerde.Size = new System.Drawing.Size(361, 104);
            this.gbOpcionesVerde.TabIndex = 2;
            this.gbOpcionesVerde.TabStop = false;
            this.gbOpcionesVerde.Text = "Opciones de color Verde";
            // 
            // imgV4
            // 
            this.imgV4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.imgV4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgV4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgV4.Location = new System.Drawing.Point(268, 17);
            this.imgV4.Name = "imgV4";
            this.imgV4.Size = new System.Drawing.Size(80, 80);
            this.imgV4.TabIndex = 7;
            this.imgV4.TabStop = false;
            // 
            // imgV3
            // 
            this.imgV3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.imgV3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgV3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgV3.Location = new System.Drawing.Point(182, 17);
            this.imgV3.Name = "imgV3";
            this.imgV3.Size = new System.Drawing.Size(80, 80);
            this.imgV3.TabIndex = 6;
            this.imgV3.TabStop = false;
            // 
            // imgV2
            // 
            this.imgV2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(102)))));
            this.imgV2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgV2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgV2.Location = new System.Drawing.Point(96, 17);
            this.imgV2.Name = "imgV2";
            this.imgV2.Size = new System.Drawing.Size(80, 80);
            this.imgV2.TabIndex = 5;
            this.imgV2.TabStop = false;
            // 
            // imgV1
            // 
            this.imgV1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(255)))), ((int)(((byte)(11)))));
            this.imgV1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgV1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgV1.Location = new System.Drawing.Point(10, 17);
            this.imgV1.Name = "imgV1";
            this.imgV1.Size = new System.Drawing.Size(80, 80);
            this.imgV1.TabIndex = 4;
            this.imgV1.TabStop = false;
            // 
            // gbColoresOrdenadosAmarillo
            // 
            this.gbColoresOrdenadosAmarillo.Controls.Add(this.pictureBox29);
            this.gbColoresOrdenadosAmarillo.Controls.Add(this.pictureBox32);
            this.gbColoresOrdenadosAmarillo.Controls.Add(this.pictureBox30);
            this.gbColoresOrdenadosAmarillo.Controls.Add(this.pictureBox31);
            this.gbColoresOrdenadosAmarillo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbColoresOrdenadosAmarillo.Location = new System.Drawing.Point(421, 394);
            this.gbColoresOrdenadosAmarillo.Name = "gbColoresOrdenadosAmarillo";
            this.gbColoresOrdenadosAmarillo.Size = new System.Drawing.Size(361, 104);
            this.gbColoresOrdenadosAmarillo.TabIndex = 7;
            this.gbColoresOrdenadosAmarillo.TabStop = false;
            this.gbColoresOrdenadosAmarillo.Text = "Orden de Colores Amarillo";
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.White;
            this.pictureBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox29.Location = new System.Drawing.Point(12, 18);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(80, 80);
            this.pictureBox29.TabIndex = 11;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.White;
            this.pictureBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox32.Location = new System.Drawing.Point(98, 18);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(80, 80);
            this.pictureBox32.TabIndex = 8;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.White;
            this.pictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox30.Location = new System.Drawing.Point(184, 18);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(80, 80);
            this.pictureBox30.TabIndex = 10;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.White;
            this.pictureBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox31.Location = new System.Drawing.Point(269, 18);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(80, 80);
            this.pictureBox31.TabIndex = 9;
            this.pictureBox31.TabStop = false;
            // 
            // gbColoresOrdenadosRojo
            // 
            this.gbColoresOrdenadosRojo.Controls.Add(this.pictureBox25);
            this.gbColoresOrdenadosRojo.Controls.Add(this.pictureBox26);
            this.gbColoresOrdenadosRojo.Controls.Add(this.pictureBox28);
            this.gbColoresOrdenadosRojo.Controls.Add(this.pictureBox27);
            this.gbColoresOrdenadosRojo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbColoresOrdenadosRojo.Location = new System.Drawing.Point(421, 160);
            this.gbColoresOrdenadosRojo.Name = "gbColoresOrdenadosRojo";
            this.gbColoresOrdenadosRojo.Size = new System.Drawing.Size(361, 104);
            this.gbColoresOrdenadosRojo.TabIndex = 5;
            this.gbColoresOrdenadosRojo.TabStop = false;
            this.gbColoresOrdenadosRojo.Text = "Orden de Colores Rojo";
            // 
            // gbOpcionesAmarillo
            // 
            this.gbOpcionesAmarillo.Controls.Add(this.imgY1);
            this.gbOpcionesAmarillo.Controls.Add(this.imgY4);
            this.gbOpcionesAmarillo.Controls.Add(this.imgY2);
            this.gbOpcionesAmarillo.Controls.Add(this.imgY3);
            this.gbOpcionesAmarillo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOpcionesAmarillo.Location = new System.Drawing.Point(421, 278);
            this.gbOpcionesAmarillo.Name = "gbOpcionesAmarillo";
            this.gbOpcionesAmarillo.Size = new System.Drawing.Size(361, 104);
            this.gbOpcionesAmarillo.TabIndex = 6;
            this.gbOpcionesAmarillo.TabStop = false;
            this.gbOpcionesAmarillo.Text = "Opciones de color Amarillo";
            // 
            // imgY1
            // 
            this.imgY1.BackColor = System.Drawing.Color.Yellow;
            this.imgY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgY1.Location = new System.Drawing.Point(12, 17);
            this.imgY1.Name = "imgY1";
            this.imgY1.Size = new System.Drawing.Size(80, 80);
            this.imgY1.TabIndex = 15;
            this.imgY1.TabStop = false;
            // 
            // imgY4
            // 
            this.imgY4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(200)))));
            this.imgY4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgY4.Location = new System.Drawing.Point(269, 17);
            this.imgY4.Name = "imgY4";
            this.imgY4.Size = new System.Drawing.Size(80, 80);
            this.imgY4.TabIndex = 13;
            this.imgY4.TabStop = false;
            // 
            // imgY2
            // 
            this.imgY2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(110)))));
            this.imgY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgY2.Location = new System.Drawing.Point(98, 17);
            this.imgY2.Name = "imgY2";
            this.imgY2.Size = new System.Drawing.Size(80, 80);
            this.imgY2.TabIndex = 12;
            this.imgY2.TabStop = false;
            // 
            // imgY3
            // 
            this.imgY3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(160)))));
            this.imgY3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgY3.Location = new System.Drawing.Point(183, 17);
            this.imgY3.Name = "imgY3";
            this.imgY3.Size = new System.Drawing.Size(80, 80);
            this.imgY3.TabIndex = 14;
            this.imgY3.TabStop = false;
            // 
            // gbOpcionesRojo
            // 
            this.gbOpcionesRojo.Controls.Add(this.imgR4);
            this.gbOpcionesRojo.Controls.Add(this.imgR2);
            this.gbOpcionesRojo.Controls.Add(this.imgR3);
            this.gbOpcionesRojo.Controls.Add(this.imgR1);
            this.gbOpcionesRojo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOpcionesRojo.Location = new System.Drawing.Point(421, 45);
            this.gbOpcionesRojo.Name = "gbOpcionesRojo";
            this.gbOpcionesRojo.Size = new System.Drawing.Size(361, 104);
            this.gbOpcionesRojo.TabIndex = 4;
            this.gbOpcionesRojo.TabStop = false;
            this.gbOpcionesRojo.Text = "Opciones de Color Rojo";
            // 
            // imgR4
            // 
            this.imgR4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.imgR4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgR4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgR4.Location = new System.Drawing.Point(184, 17);
            this.imgR4.Name = "imgR4";
            this.imgR4.Size = new System.Drawing.Size(80, 80);
            this.imgR4.TabIndex = 7;
            this.imgR4.TabStop = false;
            // 
            // imgR2
            // 
            this.imgR2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.imgR2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgR2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgR2.Location = new System.Drawing.Point(98, 17);
            this.imgR2.Name = "imgR2";
            this.imgR2.Size = new System.Drawing.Size(80, 80);
            this.imgR2.TabIndex = 5;
            this.imgR2.TabStop = false;
            // 
            // imgR3
            // 
            this.imgR3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.imgR3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgR3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgR3.Location = new System.Drawing.Point(269, 17);
            this.imgR3.Name = "imgR3";
            this.imgR3.Size = new System.Drawing.Size(80, 80);
            this.imgR3.TabIndex = 6;
            this.imgR3.TabStop = false;
            // 
            // imgR1
            // 
            this.imgR1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(11)))), ((int)(((byte)(3)))));
            this.imgR1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgR1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgR1.Location = new System.Drawing.Point(12, 17);
            this.imgR1.Name = "imgR1";
            this.imgR1.Size = new System.Drawing.Size(80, 80);
            this.imgR1.TabIndex = 4;
            this.imgR1.TabStop = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGuardar.Location = new System.Drawing.Point(324, 514);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(169, 33);
            this.btnGuardar.TabIndex = 8;
            this.btnGuardar.Text = "Finalizar Prueba";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 0);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(841, 572);
            this.panelAyuda.TabIndex = 72;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(505, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(476, 224);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.Resources.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(201, 296);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(111, 34);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click_1);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(841, 36);
            this.label3.TabIndex = 73;
            this.label3.Text = "PRUEBA LUSCHER";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnIniciarAyuda);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(167, 75);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 361);
            this.panel1.TabIndex = 73;
            // 
            // btnCloseVentana
            // 
            this.btnCloseVentana.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseVentana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseVentana.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCloseVentana.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseVentana.FlatAppearance.BorderSize = 0;
            this.btnCloseVentana.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseVentana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseVentana.Location = new System.Drawing.Point(806, 1);
            this.btnCloseVentana.Name = "btnCloseVentana";
            this.btnCloseVentana.Size = new System.Drawing.Size(35, 35);
            this.btnCloseVentana.TabIndex = 80;
            this.btnCloseVentana.UseVisualStyleBackColor = false;
            this.btnCloseVentana.Click += new System.EventHandler(this.btnCloseVentana_Click);
            // 
            // frmLuscher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(841, 572);
            this.ControlBox = false;
            this.Controls.Add(this.btnCloseVentana);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.gbColoresOrdenadosAmarillo);
            this.Controls.Add(this.gbColoresOrdenadosRojo);
            this.Controls.Add(this.gbColoresOrdenadosVerde);
            this.Controls.Add(this.gbOpcionesAmarillo);
            this.Controls.Add(this.gbColoresOrdenadosAzul);
            this.Controls.Add(this.gbOpcionesRojo);
            this.Controls.Add(this.gbOpcionesVerde);
            this.Controls.Add(this.gbOpcionesAzul);
            this.Name = "frmLuscher";
            this.Text = "Prueba Luscher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbOpcionesAzul.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgA4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgA1)).EndInit();
            this.gbColoresOrdenadosAzul.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.gbColoresOrdenadosVerde.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            this.gbOpcionesVerde.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgV4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgV1)).EndInit();
            this.gbColoresOrdenadosAmarillo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            this.gbColoresOrdenadosRojo.ResumeLayout(false);
            this.gbOpcionesAmarillo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgY1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgY3)).EndInit();
            this.gbOpcionesRojo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgR4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgR1)).EndInit();
            this.panelAyuda.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbOpcionesAzul;
        private System.Windows.Forms.GroupBox gbColoresOrdenadosAzul;
        private System.Windows.Forms.GroupBox gbColoresOrdenadosVerde;
        private System.Windows.Forms.GroupBox gbOpcionesVerde;
        private System.Windows.Forms.GroupBox gbColoresOrdenadosAmarillo;
        private System.Windows.Forms.GroupBox gbColoresOrdenadosRojo;
        private System.Windows.Forms.GroupBox gbOpcionesAmarillo;
        private System.Windows.Forms.GroupBox gbOpcionesRojo;
        private System.Windows.Forms.PictureBox imgA4;
        private System.Windows.Forms.PictureBox imgA3;
        private System.Windows.Forms.PictureBox imgA2;
        private System.Windows.Forms.PictureBox imgA1;
        private System.Windows.Forms.PictureBox imgV4;
        private System.Windows.Forms.PictureBox imgV3;
        private System.Windows.Forms.PictureBox imgV2;
        private System.Windows.Forms.PictureBox imgV1;
        private System.Windows.Forms.PictureBox imgR4;
        private System.Windows.Forms.PictureBox imgR2;
        private System.Windows.Forms.PictureBox imgR3;
        private System.Windows.Forms.PictureBox imgR1;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.PictureBox imgY1;
        private System.Windows.Forms.PictureBox imgY4;
        private System.Windows.Forms.PictureBox imgY2;
        private System.Windows.Forms.PictureBox imgY3;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCloseVentana;
    }
}

