﻿using System;
using System.Drawing;
using System.Windows.Forms;

using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Luscher
{
    public partial class frmLuscher : Form
    {
        private QuickHumClient cliente = new QuickHumClient();
        LUSCHER luscher = new LUSCHER();
        int numero_prueba;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;
        Color obtenerColor;
        string obtenerName;
        string obtenerNombrePanel;
        public frmLuscher(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad;
        }


        private void imgA1_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox imgSelecionada = sender as PictureBox;
            obtenerColor = imgSelecionada.BackColor;
            obtenerName = imgSelecionada.Name;
            obtenerNombrePanel = obtenerName.Substring(0, 4);

            switch (obtenerNombrePanel)
            {
                case "imgA":
                    this.gbColoresOrdenadosAzul.Enabled = true;
                    this.gbColoresOrdenadosVerde.Enabled = false;
                    this.gbColoresOrdenadosRojo.Enabled = false;
                    this.gbColoresOrdenadosAmarillo.Enabled = false;
                    break;
                case "imgV":
                    this.gbColoresOrdenadosAzul.Enabled = false;
                    this.gbColoresOrdenadosVerde.Enabled = true;
                    this.gbColoresOrdenadosRojo.Enabled = false;
                    this.gbColoresOrdenadosAmarillo.Enabled = false;
                    break;
                case "imgR":
                    this.gbColoresOrdenadosAzul.Enabled = false;
                    this.gbColoresOrdenadosVerde.Enabled = false;
                    this.gbColoresOrdenadosRojo.Enabled = true;
                    this.gbColoresOrdenadosAmarillo.Enabled = false;
                    break;
                case "imgY":
                    this.gbColoresOrdenadosAzul.Enabled = false;
                    this.gbColoresOrdenadosVerde.Enabled = false;
                    this.gbColoresOrdenadosRojo.Enabled = false;
                    this.gbColoresOrdenadosAmarillo.Enabled = true;
                    break;
            }

            imgSelecionada.DoDragDrop(imgSelecionada.BackColor, DragDropEffects.Copy);
        }

        private void pictureCartasOrdenadas_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void pictureCartasOrdenadasAzul_DragDrop(object sender, DragEventArgs e)
        {

            PictureBox imgUbicada = sender as PictureBox;

            foreach (Control img in this.gbColoresOrdenadosAzul.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == obtenerColor)
                    {
                        img.BackColor = Color.Transparent;
                    }
                }
            }
            imgUbicada.Name = obtenerName;
            imgUbicada.BackColor = obtenerColor;
        }

        private void pictureCartasOrdenadasVerde_DragDrop(object sender, DragEventArgs e)
        {
            PictureBox imgUbicada = sender as PictureBox;
            foreach (Control img in this.gbColoresOrdenadosVerde.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == obtenerColor)
                    {
                        img.BackColor = Color.Transparent;
                    }
                }
            }
            imgUbicada.Name = obtenerName;
            imgUbicada.BackColor = obtenerColor;
        }

        private void pictureCartasOrdenadasRojo_DragDrop(object sender, DragEventArgs e)
        {
            PictureBox imgUbicada = sender as PictureBox;

            foreach (Control img in this.gbColoresOrdenadosRojo.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == obtenerColor)
                    {
                        img.BackColor = Color.Transparent;
                    }
                }
            }
            imgUbicada.Name = obtenerName;
            imgUbicada.BackColor = obtenerColor;
        }

        private void pictureCartasOrdenadasAmarillo_DragDrop(object sender, DragEventArgs e)
        {
            PictureBox imgUbicada = sender as PictureBox;

            foreach (Control img in this.gbColoresOrdenadosAmarillo.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == obtenerColor)
                    {
                        img.BackColor = Color.Transparent;
                    }
                }
            }
            imgUbicada.Name = obtenerName;
            imgUbicada.BackColor = obtenerColor;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region llenado de eventos para los colores azul
            foreach (Control img in this.gbOpcionesAzul.Controls)
            {
                if (img is PictureBox)
                    img.MouseDown += imgA1_MouseDown;
            }
            foreach (Control img in gbColoresOrdenadosAzul.Controls)
            {
                if (img is PictureBox)
                {
                    img.AllowDrop = true;
                    img.DragEnter += pictureCartasOrdenadas_DragEnter;
                    img.DragDrop += pictureCartasOrdenadasAzul_DragDrop;
                }
            }

            #endregion

            #region Llenado de eventos para los colores Verde
            foreach (Control img in this.gbOpcionesVerde.Controls)
            {
                if (img is PictureBox)
                    img.MouseDown += imgA1_MouseDown;
            }
            foreach (Control img in gbColoresOrdenadosVerde.Controls)
            {
                if (img is PictureBox)
                {
                    img.AllowDrop = true;
                    img.DragEnter += pictureCartasOrdenadas_DragEnter;
                    img.DragDrop += pictureCartasOrdenadasVerde_DragDrop;
                }
            }

            #endregion

            #region Llenado de eventos para los colores Rojo
            foreach (Control img in this.gbOpcionesRojo.Controls)
            {
                if (img is PictureBox)
                    img.MouseDown += imgA1_MouseDown;
            }
            foreach (Control img in gbColoresOrdenadosRojo.Controls)
            {
                if (img is PictureBox)
                {
                    img.AllowDrop = true;
                    img.DragEnter += pictureCartasOrdenadas_DragEnter;
                    img.DragDrop += pictureCartasOrdenadasRojo_DragDrop;
                }
            }

            #endregion

            #region Llenado de eventos para los colores Amarillo
            foreach (Control img in this.gbOpcionesAmarillo.Controls)
            {
                if (img is PictureBox)
                    img.MouseDown += imgA1_MouseDown;
            }
            foreach (Control img in gbColoresOrdenadosAmarillo.Controls)
            {
                if (img is PictureBox)
                {
                    img.AllowDrop = true;
                    img.DragEnter += pictureCartasOrdenadas_DragEnter;
                    img.DragDrop += pictureCartasOrdenadasAmarillo_DragDrop;
                }
            }

            #endregion
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int[] colores = new int[16];
            int[] valores_verdes = new int[4];
            int x = 0;
            int y = 0;
            dto_luscher_respuestas_candidato[] modelo_luscher_resultado = new dto_luscher_respuestas_candidato[4];

            foreach (Control img in this.gbColoresOrdenadosAzul.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == Color.White)
                    {
                        MessageBox.Show("Falta Seleccionar Colores.");
                        return;
                    }
                    colores[x] = Convert.ToInt32(img.Name.Remove(0, 4));
                    x++;
                }
            }
            modelo_luscher_resultado[0] = new dto_luscher_respuestas_candidato();
            modelo_luscher_resultado[0].NumeroPrueba = numero_prueba;
            modelo_luscher_resultado[0].IdColor = 1;
            modelo_luscher_resultado[0].Combinacion = Convert.ToInt32(colores[0] + "" + colores[1] + "" + colores[2] + "" + colores[3]);
            y++;
            foreach (Control img in this.gbColoresOrdenadosVerde.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == Color.White)
                    {
                        MessageBox.Show("Falta Seleccionar Colores.");
                        return;
                    }
                    colores[x] = Convert.ToInt32(img.Name.Remove(0, 4));
                    x++;
                }
            }
            modelo_luscher_resultado[1] = new dto_luscher_respuestas_candidato();
            modelo_luscher_resultado[1].NumeroPrueba = numero_prueba;
            modelo_luscher_resultado[1].IdColor = 2;
            modelo_luscher_resultado[1].Combinacion = Convert.ToInt32(colores[4] + "" + colores[5] + "" + colores[6] + "" + colores[7]);
            foreach (Control img in this.gbColoresOrdenadosRojo.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == Color.White)
                    {
                        MessageBox.Show("Falta Seleccionar Colores.");
                        return;
                    }
                    colores[x] = Convert.ToInt32(img.Name.Remove(0, 4));
                    x++;
                }
            }
            modelo_luscher_resultado[2] = new dto_luscher_respuestas_candidato();
            modelo_luscher_resultado[2].NumeroPrueba = numero_prueba;
            modelo_luscher_resultado[2].IdColor = 3;
            modelo_luscher_resultado[2].Combinacion = Convert.ToInt32(colores[8] + "" + colores[9] + "" + colores[10] + "" + colores[11]);
            foreach (Control img in this.gbColoresOrdenadosAmarillo.Controls)
            {
                if (img is PictureBox)
                {
                    if (img.BackColor == Color.White)
                    {
                        MessageBox.Show("Falta Seleccionar Colores.");
                        return;
                    }
                    colores[x] = Convert.ToInt32(img.Name.Remove(0, 4));
                    x++;
                }
            }
            modelo_luscher_resultado[3] = new dto_luscher_respuestas_candidato();
            modelo_luscher_resultado[3].NumeroPrueba = numero_prueba;
            modelo_luscher_resultado[3].IdColor = 4;
            modelo_luscher_resultado[3].Combinacion = Convert.ToInt32(colores[12] + "" + colores[13] + "" + colores[14] + "" + colores[15]);

            if (cliente.GuardarLuscherResultados(modelo_luscher_resultado))
                MessageBox.Show("Prueba Finalizada.");
            this.Close();
        }

        private void btnIniciarAyuda_Click_1(object sender, EventArgs e)
        {
            this.btnIniciarAyuda.Hide();
            EscolaridadPrueba escolaridad = new EscolaridadPrueba();
            escolaridad.Id = 8;
            this.btnGuardar.Visible = true;
            luscher = cliente.NuevoLuscher(_expediente.Numero, escolaridad);
            numero_prueba = luscher.IdPrueba;
            panelAyuda.Visible = false;
        }

        private void btnCloseVentana_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
