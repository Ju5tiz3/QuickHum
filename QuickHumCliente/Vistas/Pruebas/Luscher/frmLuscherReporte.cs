﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Luscher
{
    public partial class frmLuscherReporte : Form
    {
        private QuickHumClient cliente = Globales.cliente;
        private DataTable dtresultados = new DataTable();
        DataTable dtCandidato = new DataTable();
        private Expediente _expediente = null;

        public frmLuscherReporte(Expediente expediente)
        {
            InitializeComponent();
            if (expediente == null)
            {
                XtraMessageBox.Show("El expediente trasladado al reporte esta vacio", "Reporte", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            this._expediente = expediente;
        }


        private void frmResultados_Load(object sender, EventArgs e)
        {
            dtresultados.Columns.Add("Color");
            dtresultados.Columns.Add("Combinacion");
            dtresultados.Columns.Add("Interpretacion");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            GenerarReporte();
        }

        private void GenerarReporte()
        {
            dtresultados.Clear();
            dto_cargar_resultados[] resultados = cliente.ListarLuscherResultados(this._expediente.Id);

            for (int x = 0; x < resultados.Length; x++)
            {
                DataRow row = dtresultados.NewRow();

                row["Color"] = Convert.ToString(resultados[x].Color);
                row["Combinacion"] = Convert.ToInt32(resultados[x].Combinacion);
                row["Interpretacion"] = resultados[x].Interpretacion;

                dtresultados.Rows.Add(row);
            }

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportDataSource rds = new ReportDataSource("DataSetLuscher", dtresultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Luscher.reporteLuscher.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.RefreshReport();
        }
    }
}
