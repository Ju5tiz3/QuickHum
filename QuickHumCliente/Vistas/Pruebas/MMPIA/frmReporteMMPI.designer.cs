﻿namespace QuickHumCliente.Vistas.Pruebas.MMPIA
{
    partial class frmReporteMMPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.rpvMMPIA = new Microsoft.Reporting.WinForms.ReportViewer();
            this.labName = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.labSexo = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.labEdad = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(951, 536);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 65;
            // 
            // panelContainer1
            // 
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.panelContainer1.ID = new System.Guid("536e358d-16ce-42c4-b68d-376102be4342");
            this.panelContainer1.Location = new System.Drawing.Point(1097, 84);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.OriginalSize = new System.Drawing.Size(250, 200);
            this.panelContainer1.Size = new System.Drawing.Size(250, 571);
            // 
            // rpvMMPIA
            // 
            this.rpvMMPIA.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSetMMPI";
            reportDataSource1.Value = null;
            this.rpvMMPIA.LocalReport.DataSources.Add(reportDataSource1);
            this.rpvMMPIA.LocalReport.ReportEmbeddedResource = "QuickHum.ReporteMMPI.rdlc";
            this.rpvMMPIA.Location = new System.Drawing.Point(0, 0);
            this.rpvMMPIA.Name = "rpvMMPIA";
            this.rpvMMPIA.Size = new System.Drawing.Size(951, 536);
            this.rpvMMPIA.TabIndex = 2;
            // 
            // labName
            // 
            this.labName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("Calibri", 12F);
            this.labName.Location = new System.Drawing.Point(468, 42);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(123, 19);
            this.labName.TabIndex = 39;
            this.labName.Text = "Carlos Hernandez";
            // 
            // label59
            // 
            this.label59.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(320, 85);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(45, 19);
            this.label59.TabIndex = 34;
            this.label59.Text = "Sexo:";
            // 
            // labSexo
            // 
            this.labSexo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labSexo.AutoSize = true;
            this.labSexo.Font = new System.Drawing.Font("Calibri", 12F);
            this.labSexo.Location = new System.Drawing.Point(468, 85);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(76, 19);
            this.labSexo.TabIndex = 43;
            this.labSexo.Text = "Masculino";
            // 
            // label61
            // 
            this.label61.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(320, 42);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(142, 19);
            this.label61.TabIndex = 32;
            this.label61.Text = "Nombre y Apellido:";
            // 
            // labEdad
            // 
            this.labEdad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labEdad.AutoSize = true;
            this.labEdad.Font = new System.Drawing.Font("Cambria", 11.25F);
            this.labEdad.Location = new System.Drawing.Point(469, 66);
            this.labEdad.Name = "labEdad";
            this.labEdad.Size = new System.Drawing.Size(24, 17);
            this.labEdad.TabIndex = 44;
            this.labEdad.Text = "12";
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Calibri", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(915, 22);
            this.label17.TabIndex = 45;
            this.label17.Text = "Datos Generales";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label54
            // 
            this.label54.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(320, 64);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 19);
            this.label54.TabIndex = 46;
            this.label54.Text = "Edad:";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.BackColor = System.Drawing.Color.White;
            this.groupBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.labEdad);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Controls.Add(this.labSexo);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.labName);
            this.groupBox7.Location = new System.Drawing.Point(0, 43);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(917, 125);
            this.groupBox7.TabIndex = 1012;
            // 
            // frmReporteMMPI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(951, 536);
            this.Controls.Add(this.rpvMMPIA);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox7);
            this.Name = "frmReporteMMPI";
            this.Text = "frmReporteMMPI";
            this.Load += new System.EventHandler(this.frmReporteMMPI_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
       // private DevExpress.XtraReports.UserDesigner.DesignControlContainer propertyGridDockPanel1_Container;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        //private DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel propertyGridDockPanel1;
        private System.Windows.Forms.BindingSource dt;
       // private _DsMMPIA_MMPIA_2 _DsMMPIA_MMPIA_21;
        private Microsoft.Reporting.WinForms.ReportViewer rpvMMPIA;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label labEdad;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel groupBox7;
    }
}