﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.MMPIA
{
    public partial class frmReporteMMPI : Form
    {
        DataTable dtResultado = new DataTable();
        DataTable dtCandidato = new DataTable();

        private Expediente _expediente = null;

        public frmReporteMMPI(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }
        

        private void frmReporteMMPI_Load(object sender, EventArgs e)
        {
            byte Genero, Edad;
            QuickHumClient cliente = Globales.cliente;


            //ServicioQuickHum.MMPIA modelo_MMPIA = new ServicioQuickHum.MMPIA();

            //Candidato candidato = cliente.CargarCandidato("76690331-3");
            labName.Text = _expediente.Candidato.Nombre + " " + _expediente.Candidato.Apellido;
            labEdad.Text = _expediente.Candidato.Edad.ToString();
            labSexo.Text = Convert.ToString((CandidatoGenero)_expediente.Candidato.Genero);
            Genero = (byte)(CandidatoGenero)_expediente.Candidato.Genero;
            Edad = _expediente.Candidato.Edad;

            dtResultado.Columns.Add("Escala");
            dtResultado.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            dto_MMPIAFactor[] modelo_factor = cliente.ColecionPuntosMMPIA(_expediente.Id, Genero);

            for (int x = 0; x < modelo_factor.Length; x++)
            {
                if (modelo_factor[x].RangoGenero != 0)
                {
                    //MessageBox.Show("Escala : " + modelo_factor[x].NombreEscala + " Rango : " + modelo_factor[x].RangoGenero);
                    DataRow rows = dtResultado.NewRow();
                    rows["Escala"] = modelo_factor[x].NombreEscala;
                    rows["Rango"] = Convert.ToInt32(modelo_factor[x].RangoGenero);
                    dtResultado.Rows.Add(rows);
                }
                
            }

            ReportDataSource rds = new ReportDataSource("dtMMPIA", dtResultado);
            ReportParameter parm = new ReportParameter("sexo", Genero.ToString());
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            rpvMMPIA.LocalReport.DataSources.Clear();
            rpvMMPIA.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.MMPIA.ReporteMMPI.rdlc";
            rpvMMPIA.LocalReport.DataSources.Add(rds);
            rpvMMPIA.LocalReport.SetParameters(parm);
            rpvMMPIA.LocalReport.DataSources.Add(rds_candidato);

            rpvMMPIA.RefreshReport();
        }

    }
}
