﻿namespace QuickHumCliente.Vistas.Pruebas.MMPIA
{
    partial class frmMMPIA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMMPIA));
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.Panel();
            this.chkVerdadero = new DevExpress.XtraEditors.CheckEdit();
            this.chkFalso = new DevExpress.XtraEditors.CheckEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.labNum = new System.Windows.Forms.Label();
            this.labPreg = new System.Windows.Forms.Label();
            this.labPregunta = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.Panel();
            this.labNumAsignacion = new System.Windows.Forms.Label();
            this.labFecha = new System.Windows.Forms.Label();
            this.labCurso = new System.Windows.Forms.Label();
            this.labSexo = new System.Windows.Forms.Label();
            this.labEdad = new System.Windows.Forms.Label();
            this.labStado = new System.Windows.Forms.Label();
            this.labCentro = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Time = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.labM = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labS = new System.Windows.Forms.Label();
            this.labMT = new System.Windows.Forms.Label();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerdadero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFalso.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBox1.Controls.Add(this.btnFinalizar);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnSiguiente);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.labNum);
            this.groupBox1.Controls.Add(this.labPreg);
            this.groupBox1.Controls.Add(this.labPregunta);
            this.groupBox1.Controls.Add(this.btnAtras);
            this.groupBox1.Location = new System.Drawing.Point(38, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 358);
            this.groupBox1.TabIndex = 43;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnFinalizar.FlatAppearance.BorderSize = 0;
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnFinalizar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(607, 301);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(118, 33);
            this.btnFinalizar.TabIndex = 70;
            this.btnFinalizar.Text = "Cancelar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = false;
            this.btnFinalizar.Visible = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnCancelarAyuda_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.chkVerdadero);
            this.groupBox2.Controls.Add(this.chkFalso);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Location = new System.Drawing.Point(20, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(705, 123);
            this.groupBox2.TabIndex = 68;
            // 
            // chkVerdadero
            // 
            this.chkVerdadero.Location = new System.Drawing.Point(179, 53);
            this.chkVerdadero.Name = "chkVerdadero";
            this.chkVerdadero.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.chkVerdadero.Properties.Appearance.Options.UseFont = true;
            this.chkVerdadero.Properties.Caption = "Verdadero";
            this.chkVerdadero.Size = new System.Drawing.Size(113, 28);
            this.chkVerdadero.TabIndex = 70;
            // 
            // chkFalso
            // 
            this.chkFalso.Location = new System.Drawing.Point(429, 53);
            this.chkFalso.Name = "chkFalso";
            this.chkFalso.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.chkFalso.Properties.Appearance.Options.UseFont = true;
            this.chkFalso.Properties.Caption = "Falso";
            this.chkFalso.Size = new System.Drawing.Size(81, 28);
            this.chkFalso.TabIndex = 69;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(705, 28);
            this.label16.TabIndex = 68;
            this.label16.Text = "Opciones de respuesta";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.Location = new System.Drawing.Point(20, 301);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(119, 33);
            this.btnSiguiente.TabIndex = 17;
            this.btnSiguiente.Text = "Iniciar";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(752, 28);
            this.label17.TabIndex = 67;
            this.label17.Text = "PREGUNTAS";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labNum
            // 
            this.labNum.AutoSize = true;
            this.labNum.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.labNum.Location = new System.Drawing.Point(105, 40);
            this.labNum.Name = "labNum";
            this.labNum.Size = new System.Drawing.Size(19, 22);
            this.labNum.TabIndex = 15;
            this.labNum.Text = "1";
            // 
            // labPreg
            // 
            this.labPreg.AutoSize = true;
            this.labPreg.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.labPreg.Location = new System.Drawing.Point(20, 39);
            this.labPreg.Name = "labPreg";
            this.labPreg.Size = new System.Drawing.Size(84, 22);
            this.labPreg.TabIndex = 13;
            this.labPreg.Text = "Pregunta:";
            // 
            // labPregunta
            // 
            this.labPregunta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labPregunta.Font = new System.Drawing.Font("Calibri", 12F);
            this.labPregunta.Location = new System.Drawing.Point(20, 74);
            this.labPregunta.Name = "labPregunta";
            this.labPregunta.Size = new System.Drawing.Size(705, 60);
            this.labPregunta.TabIndex = 14;
            this.labPregunta.Text = "Texto descriptivo de la pregunta";
            // 
            // btnAtras
            // 
            this.btnAtras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAtras.FlatAppearance.BorderSize = 0;
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtras.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAtras.ForeColor = System.Drawing.Color.White;
            this.btnAtras.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAtras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtras.Location = new System.Drawing.Point(21, 301);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(118, 33);
            this.btnAtras.TabIndex = 24;
            this.btnAtras.Text = "Atras";
            this.btnAtras.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtras.UseVisualStyleBackColor = false;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBox3.Controls.Add(this.labNumAsignacion);
            this.groupBox3.Controls.Add(this.labFecha);
            this.groupBox3.Controls.Add(this.labCurso);
            this.groupBox3.Controls.Add(this.labSexo);
            this.groupBox3.Controls.Add(this.labEdad);
            this.groupBox3.Controls.Add(this.labStado);
            this.groupBox3.Controls.Add(this.labCentro);
            this.groupBox3.Controls.Add(this.labName);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(56, 212);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(702, 199);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.Visible = false;
            // 
            // labNumAsignacion
            // 
            this.labNumAsignacion.AutoSize = true;
            this.labNumAsignacion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labNumAsignacion.Location = new System.Drawing.Point(446, 169);
            this.labNumAsignacion.Name = "labNumAsignacion";
            this.labNumAsignacion.Size = new System.Drawing.Size(45, 19);
            this.labNumAsignacion.TabIndex = 81;
            this.labNumAsignacion.Text = "Sexo:";
            this.labNumAsignacion.Visible = false;
            // 
            // labFecha
            // 
            this.labFecha.AutoSize = true;
            this.labFecha.Font = new System.Drawing.Font("Calibri", 12F);
            this.labFecha.Location = new System.Drawing.Point(508, 114);
            this.labFecha.Name = "labFecha";
            this.labFecha.Size = new System.Drawing.Size(38, 19);
            this.labFecha.TabIndex = 80;
            this.labFecha.Text = "date";
            // 
            // labCurso
            // 
            this.labCurso.AutoSize = true;
            this.labCurso.Font = new System.Drawing.Font("Calibri", 12F);
            this.labCurso.Location = new System.Drawing.Point(560, 80);
            this.labCurso.Name = "labCurso";
            this.labCurso.Size = new System.Drawing.Size(52, 19);
            this.labCurso.TabIndex = 79;
            this.labCurso.Text = "course";
            // 
            // labSexo
            // 
            this.labSexo.AutoSize = true;
            this.labSexo.Font = new System.Drawing.Font("Calibri", 12F);
            this.labSexo.Location = new System.Drawing.Point(500, 146);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(46, 19);
            this.labSexo.TabIndex = 78;
            this.labSexo.Text = "genre";
            // 
            // labEdad
            // 
            this.labEdad.AutoSize = true;
            this.labEdad.Font = new System.Drawing.Font("Calibri", 12F);
            this.labEdad.Location = new System.Drawing.Point(500, 41);
            this.labEdad.Name = "labEdad";
            this.labEdad.Size = new System.Drawing.Size(33, 19);
            this.labEdad.TabIndex = 77;
            this.labEdad.Text = "age";
            // 
            // labStado
            // 
            this.labStado.AutoSize = true;
            this.labStado.Font = new System.Drawing.Font("Calibri", 12F);
            this.labStado.Location = new System.Drawing.Point(168, 143);
            this.labStado.Name = "labStado";
            this.labStado.Size = new System.Drawing.Size(86, 19);
            this.labStado.TabIndex = 76;
            this.labStado.Text = "relationship";
            // 
            // labCentro
            // 
            this.labCentro.AutoSize = true;
            this.labCentro.Font = new System.Drawing.Font("Calibri", 12F);
            this.labCentro.Location = new System.Drawing.Point(168, 93);
            this.labCentro.Name = "labCentro";
            this.labCentro.Size = new System.Drawing.Size(50, 19);
            this.labCentro.TabIndex = 75;
            this.labCentro.Text = "region";
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("Calibri", 12F);
            this.labName.Location = new System.Drawing.Point(168, 43);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(45, 19);
            this.labName.TabIndex = 74;
            this.labName.Text = "name";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(444, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 19);
            this.label13.TabIndex = 73;
            this.label13.Text = "Fecha:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(20, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 19);
            this.label12.TabIndex = 72;
            this.label12.Text = "Estado Civil:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(444, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 19);
            this.label11.TabIndex = 71;
            this.label11.Text = "Curso Puesto:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(20, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 19);
            this.label10.TabIndex = 70;
            this.label10.Text = "Centro:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(446, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 19);
            this.label9.TabIndex = 69;
            this.label9.Text = "Sexo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(444, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 19);
            this.label8.TabIndex = 68;
            this.label8.Text = "Edad:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(20, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 19);
            this.label7.TabIndex = 67;
            this.label7.Text = "Nombre y Apellido:";
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(700, 28);
            this.label22.TabIndex = 66;
            this.label22.Text = "DATOS GENERALES";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(830, 36);
            this.label3.TabIndex = 66;
            this.label3.Text = "PRUEBA MMPIA";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Time
            // 
            this.Time.Tick += new System.EventHandler(this.Time_Tick);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo2;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.labM);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labS);
            this.panel2.Controls.Add(this.labMT);
            this.panel2.Location = new System.Drawing.Point(476, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(342, 43);
            this.panel2.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(58, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 19);
            this.label5.TabIndex = 22;
            this.label5.Text = "Tiempo Transcurrido:";
            // 
            // labM
            // 
            this.labM.AutoSize = true;
            this.labM.BackColor = System.Drawing.Color.Transparent;
            this.labM.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labM.ForeColor = System.Drawing.Color.White;
            this.labM.Location = new System.Drawing.Point(282, 11);
            this.labM.Name = "labM";
            this.labM.Size = new System.Drawing.Size(25, 19);
            this.labM.TabIndex = 19;
            this.labM.Text = "00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(266, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 19);
            this.label1.TabIndex = 23;
            this.label1.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(233, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 19);
            this.label6.TabIndex = 23;
            this.label6.Text = ":";
            // 
            // labS
            // 
            this.labS.AutoSize = true;
            this.labS.BackColor = System.Drawing.Color.Transparent;
            this.labS.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labS.ForeColor = System.Drawing.Color.White;
            this.labS.Location = new System.Drawing.Point(247, 11);
            this.labS.Name = "labS";
            this.labS.Size = new System.Drawing.Size(17, 19);
            this.labS.TabIndex = 20;
            this.labS.Text = "0";
            // 
            // labMT
            // 
            this.labMT.AutoSize = true;
            this.labMT.BackColor = System.Drawing.Color.Transparent;
            this.labMT.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labMT.ForeColor = System.Drawing.Color.White;
            this.labMT.Location = new System.Drawing.Point(213, 11);
            this.labMT.Name = "labMT";
            this.labMT.Size = new System.Drawing.Size(17, 19);
            this.labMT.TabIndex = 21;
            this.labMT.Text = "0";
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImage = global::QuickHumCliente.Properties.Resources.close;
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Location = new System.Drawing.Point(795, 1);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 106;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click_1);
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel3);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(830, 437);
            this.panelAyuda.TabIndex = 107;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Location = new System.Drawing.Point(117, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(597, 362);
            this.panel3.TabIndex = 74;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Location = new System.Drawing.Point(560, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(36, 36);
            this.panel7.TabIndex = 80;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(239, 314);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 33);
            this.button1.TabIndex = 18;
            this.button1.Text = "Iniciar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(1, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(36, 36);
            this.panel6.TabIndex = 79;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(34, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(529, 36);
            this.label4.TabIndex = 61;
            this.label4.Text = "INDICACIONES";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.Font = new System.Drawing.Font("Calibri", 11F);
            this.label14.Location = new System.Drawing.Point(14, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(575, 259);
            this.label14.TabIndex = 0;
            this.label14.Text = resources.GetString("label14.Text");
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmMMPIA
            // 
            this.AcceptButton = this.btnSiguiente;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(830, 473);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(750, 300);
            this.Name = "frmMMPIA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prueba MMPIA";
            this.Load += new System.EventHandler(this.xfrmMMPIAPreguntas_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkVerdadero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFalso.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelAyuda.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMT;
        private System.Windows.Forms.Label labS;
        private System.Windows.Forms.Label labM;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Panel groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labNum;
        private System.Windows.Forms.Label labPreg;
        private System.Windows.Forms.Label labPregunta;
        private System.Windows.Forms.Panel groupBox3;
        private System.Windows.Forms.Label labNumAsignacion;
        private System.Windows.Forms.Label labFecha;
        private System.Windows.Forms.Label labCurso;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label labEdad;
        private System.Windows.Forms.Label labStado;
        private System.Windows.Forms.Label labCentro;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit chkFalso;
        private DevExpress.XtraEditors.CheckEdit chkVerdadero;
        private System.Windows.Forms.Timer Time;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;

    }
}