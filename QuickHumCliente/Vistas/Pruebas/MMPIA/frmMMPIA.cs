﻿using System;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.MMPIA
{
    public partial class frmMMPIA : DevExpress.XtraEditors.XtraForm
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;

        string[] Pregunta = new string[479];

        int[] IdRespuesta = new int[479];
        int  mil, seg, min;
        int IdRespuestaSeleccionada = 0;
        int NumeroAsignacion, OpcionRespuesta;
        int z = 0;
        int NumeroRespuesta = 1;

        ServicioQuickHum.MMPIA modelo_MMPIA;

        public frmMMPIA(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }

        /// <summary>
        /// Crea la Prueba Con Sus Preguntas y Respuestas
        /// </summary>
        public void CrearPrueba()
        {
            modelo_MMPIA = new ServicioQuickHum.MMPIA();
            try
            {
                modelo_MMPIA = cliente.NuevoMMPIA(_expediente.Numero, _escolaridad_prueba);
                for (int x = 0; x < modelo_MMPIA.Pregunta.Length; ++x)
                {
                    Pregunta[x] = modelo_MMPIA.Pregunta[x].Pregunta;
                    IdRespuesta[x] = modelo_MMPIA.Pregunta[x].IdPregunta;
                }

                IdRespuestaSeleccionada = IdRespuesta[0];

                labPregunta.Text = Pregunta[0];
                labNum.Text = NumeroRespuesta++.ToString();
                NumeroAsignacion = modelo_MMPIA.IdPrueba;
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
        }

        /// <summary>
        /// Funcion que contiente un arreglo con las Preguntas para el Test
        /// </summary>
        public void ListarPreguntas(int z)
        {
            NumeroRespuesta = z + 1;
            labPregunta.Text = Pregunta[z];
            labNum.Text = NumeroRespuesta.ToString();

            IdRespuestaSeleccionada = IdRespuesta[z];
        }

        /// <summary>
        /// Accion al Cargar el Formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xfrmMMPIAPreguntas_Load(object sender, EventArgs e)
        {

            chkFalso.Hide(); chkVerdadero.Hide(); btnAtras.Enabled = false; btnFinalizar.Enabled = false;
            labName.Hide(); labStado.Hide(); labSexo.Hide(); labNum.Hide(); labCentro.Hide(); labCurso.Hide(); labEdad.Hide(); labCentro.Hide(); labFecha.Hide();
        }

        /// <summary>
        /// Inicia la Prueba...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Iniciar")
            {
                CrearPrueba();

                //Estilos del boton al cambiar de Iniciar a Siguiente 
                btnSiguiente.Text = "Siguiente";
                btnSiguiente.TextAlign = ContentAlignment.MiddleLeft;
                btnSiguiente.ImageAlign = ContentAlignment.MiddleRight;
                btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                btnSiguiente.Location = new Point(btnSiguiente.Location.X + 130, btnSiguiente.Location.Y);
                btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;

                labName.Show(); labStado.Show(); labSexo.Show(); labNum.Show(); labCentro.Show(); labCurso.Show(); labEdad.Show(); labCentro.Show(); labFecha.Show();
                this.labS.Show(); this.labM.Show(); this.labMT.Show(); this.label5.Show(); this.label6.Show(); Time.Start();
                this.chkVerdadero.Show(); this.chkFalso.Show(); btnAtras.Enabled = true; btnFinalizar.Enabled = true;
                return;
            }

            if (btnSiguiente.Text == "Siguiente")
            {
                if (chkFalso.Checked) { OpcionRespuesta = 0; }

                if (chkVerdadero.Checked) { OpcionRespuesta = 1; }

                if (chkFalso.Checked == false && chkVerdadero.Checked == false) { OpcionRespuesta = 2; }

                if (chkFalso.Checked && chkVerdadero.Checked) { OpcionRespuesta = 3; }
                btnFinalizar.Visible = true;
                dto_MMPIAPunto actualizar_respuesta = new dto_MMPIAPunto();

                if (labPreg.Name == "labPreg")
                {

                }
                else {
                    dto_MMPIAPunto[] modelo_punto = cliente.ListarRespuestasMMPIA(NumeroAsignacion);

                    if (modelo_punto.Count() > z)
                    {
                        if (modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 0 || modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 1
                             || modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 2 || modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 3)
                        {
                            chkVerdadero.Checked = false; chkFalso.Checked = false;
                            actualizar_respuesta.IdPunto = modelo_punto[z].IdPunto;
                            actualizar_respuesta.OpcionRespuesta = (byte)OpcionRespuesta;
                            if (cliente.ActualizarRespuestasMMPIA(actualizar_respuesta))
                            { }
                            z++;
                            if (Convert.ToInt32(labNum.Text) >= modelo_punto.Count()) { ListarPreguntas(z); return; }
                            if (modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 0) { chkFalso.Checked = true; chkVerdadero.Checked = false; }
                            if (modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 1) { chkVerdadero.Checked = true; chkFalso.Checked = false; }
                            if (modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 2) { chkFalso.Checked = false; chkVerdadero.Checked = false; }
                            if (modelo_punto[z].IdPunto == modelo_punto[z].IdPunto && modelo_punto[z].OpcionRespuesta == 3) { chkFalso.Checked = true; chkVerdadero.Checked = true; }
                            ListarPreguntas(z);

                            //if (Convert.ToInt32(labNum.Text) > modelo_punto.Count()) { chkFalso.Checked = false; chkVerdadero.Checked = false; }
                            btnAtras.Enabled = true; btnFinalizar.Enabled = true;
                            return;
                        }
                    }
                }

                dto_MMPIAPunto insertar_respuesta = new dto_MMPIAPunto();
                insertar_respuesta.IdPregunta = IdRespuestaSeleccionada;
                insertar_respuesta.NumeroPrueba = NumeroAsignacion;
                insertar_respuesta.OpcionRespuesta = (byte)OpcionRespuesta;
                if(cliente.InsertarRespuestaMMPIA(insertar_respuesta))
                {
                    z++; 
                    labPreg.Name = "Pregunta";             
                }

                chkFalso.Checked = false; chkVerdadero.Checked = false;

                if (Convert.ToInt32(labNum.Text) > 1) { btnAtras.Enabled = true; }

                if (Convert.ToInt32(labNum.Text) == 478)
                {
                    btnSiguiente.Text = "Finalizar";
                    ServicioQuickHum.MMPIA finalizar_prueba = new ServicioQuickHum.MMPIA();
                    finalizar_prueba.IdPrueba = modelo_MMPIA.IdPrueba;
                    if (cliente.FinalizarMMPIA(finalizar_prueba))
                    { }
                    Time.Stop();
                    MessageBox.Show("Test Finalizado", "MMPIA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return;
                }
                ListarPreguntas(z);
            }
        }

        /// <summary>
        /// Actualiza las respuestas que el candidato ha contestado
        /// </summary>
        /// <param name="sender"></param>5
        /// <param name="e"></param>
        private void btnAtras_Click(object sender, EventArgs e)
        {
            if (z == 0) { btnAtras.Enabled = false; return; }
            z--;
           
            dto_MMPIAPunto[] modelo_punto = cliente.ListarRespuestasMMPIA(NumeroAsignacion);

            for (int x = 0; x < modelo_punto.Length; ++x)
            {
                IdRespuesta[x] = modelo_punto[x].IdPregunta;
            }

            labNum.Text = z.ToString();

            if (z == 0)
            {
                labNum.Text = "1";
                labPregunta.Text = Pregunta[0];
                if (IdRespuesta[0] == modelo_MMPIA.Pregunta[0].IdPregunta && modelo_punto[0].OpcionRespuesta == 0) { chkFalso.Checked = true; chkVerdadero.Checked = false; }
                if (IdRespuesta[0] == modelo_MMPIA.Pregunta[0].IdPregunta && modelo_punto[0].OpcionRespuesta == 1) { chkVerdadero.Checked = true; chkFalso.Checked = false; }
                if (IdRespuesta[0] == modelo_MMPIA.Pregunta[0].IdPregunta && modelo_punto[0].OpcionRespuesta == 2) { chkFalso.Checked = false; chkVerdadero.Checked = false; }
                if (IdRespuesta[0] == modelo_MMPIA.Pregunta[0].IdPregunta && modelo_punto[0].OpcionRespuesta == 3) { chkFalso.Checked = true; chkVerdadero.Checked = true; }
                
                ListarPreguntas(z);
                btnAtras.Enabled = false; btnFinalizar.Enabled = false;
                return;
            }

            if (IdRespuesta[z] == modelo_MMPIA.Pregunta[z].IdPregunta && modelo_punto[z].OpcionRespuesta == 0) { chkFalso.Checked = true; chkVerdadero.Checked = false; }
            if (IdRespuesta[z] == modelo_MMPIA.Pregunta[z].IdPregunta && modelo_punto[z].OpcionRespuesta == 1) { chkVerdadero.Checked = true; chkFalso.Checked = false; }
            if (IdRespuesta[z] == modelo_MMPIA.Pregunta[z].IdPregunta && modelo_punto[z].OpcionRespuesta == 2) { chkFalso.Checked = false; chkVerdadero.Checked = false; }
            if (IdRespuesta[z] == modelo_MMPIA.Pregunta[z].IdPregunta && modelo_punto[z].OpcionRespuesta == 3) { chkFalso.Checked = true; chkVerdadero.Checked = true; }
                ListarPreguntas(z);
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Time_Tick(object sender, EventArgs e)
        {
            Time.Interval = 10;

            mil = Convert.ToInt16(labM.Text); mil += 1; labM.Text = mil.ToString();

            if (mil == 60)
            {
                seg = Convert.ToInt16(labS.Text); seg += 1; labS.Text = seg.ToString(); labM.Text = "00";

                if (seg == 60) { min = Convert.ToInt16(labMT.Text); min += 1; labMT.Text = min.ToString(); labS.Text = "00"; }
            }
            if (min == 40)
            {
                Time.Stop();
                MessageBox.Show("Se ha terminado el tiempo estimado para realizar esta prueba", "TIEMPO AGOTADO", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                this.Close();
            }
        }

        /// <summary>
        /// Finaliza la Prueba
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelarAyuda_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
            //ServicioQuickHum.MMPIA finalizar_prueba = new ServicioQuickHum.MMPIA();
            //finalizar_prueba.IdPrueba = modelo_MMPIA.IdPrueba;
            //if (cliente.FinalizarMMPIA(finalizar_prueba))
            //{ }

            //Time.Stop();
            //labName.Text = ""; labNum.Text = ""; labPregunta.Text = ""; chkFalso.Visible = false; chkFalso.Visible = false;

            //MessageBox.Show("Test Finalizado", "MMPIA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //this.Close();
        }

        private void btnCloseWindow_Click_1(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba MMPIA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            cliente.CambiarEstadoPrueba(Test.MMPIA, NumeroAsignacion, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnSiguiente_Click(sender, e);
        }

    }
}