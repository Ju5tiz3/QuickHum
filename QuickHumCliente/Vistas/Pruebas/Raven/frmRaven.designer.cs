﻿namespace QuickHumCliente.Vistas.Pruebas.Raven
{
    partial class frmRaven
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRaven));
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.panelAyuda1 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.gbRespuesta = new System.Windows.Forms.Panel();
            this.txtSeleccion = new System.Windows.Forms.TextBox();
            this.picSeleccion = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.picRes3 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.picSuperior = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.picRes1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.picRes2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.picRes5 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.picRes4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.picRes6 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.picResInf4 = new System.Windows.Forms.PictureBox();
            this.picResSup4 = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.panelAyuda1.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.gbRespuesta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccion)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRes3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSuperior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResInf4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResSup4)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(929, 36);
            this.label3.TabIndex = 67;
            this.label3.Text = "PRUEBA RAVEN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNumPreg.Location = new System.Drawing.Point(251, 4);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(115, 17);
            this.lblNumPreg.TabIndex = 75;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // panelAyuda1
            // 
            this.panelAyuda1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panelAyuda1.BackColor = System.Drawing.Color.White;
            this.panelAyuda1.Controls.Add(this.btnCerrarAyuda);
            this.panelAyuda1.Controls.Add(this.label12);
            this.panelAyuda1.Controls.Add(this.btnIniciarAyuda);
            this.panelAyuda1.Controls.Add(this.label14);
            this.panelAyuda1.Location = new System.Drawing.Point(173, 39);
            this.panelAyuda1.Name = "panelAyuda1";
            this.panelAyuda1.Size = new System.Drawing.Size(579, 323);
            this.panelAyuda1.TabIndex = 100;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarAyuda.Image")));
            this.btnCerrarAyuda.Location = new System.Drawing.Point(543, 1);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 103;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Font = new System.Drawing.Font("Calibri", 11F);
            this.label12.Location = new System.Drawing.Point(16, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(545, 143);
            this.label12.TabIndex = 73;
            this.label12.Text = resources.GetString("label12.Text");
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(228, 263);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(579, 36);
            this.label14.TabIndex = 61;
            this.label14.Text = "INDICACIONES";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panelAyuda1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(929, 639);
            this.panelAyuda.TabIndex = 102;
            // 
            // gbRespuesta
            // 
            this.gbRespuesta.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbRespuesta.BackColor = System.Drawing.Color.White;
            this.gbRespuesta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gbRespuesta.Controls.Add(this.txtSeleccion);
            this.gbRespuesta.Controls.Add(this.picSeleccion);
            this.gbRespuesta.Controls.Add(this.label10);
            this.gbRespuesta.Location = new System.Drawing.Point(645, 90);
            this.gbRespuesta.Name = "gbRespuesta";
            this.gbRespuesta.Size = new System.Drawing.Size(276, 245);
            this.gbRespuesta.TabIndex = 104;
            // 
            // txtSeleccion
            // 
            this.txtSeleccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeleccion.Location = new System.Drawing.Point(108, 155);
            this.txtSeleccion.MaxLength = 1;
            this.txtSeleccion.Multiline = true;
            this.txtSeleccion.Name = "txtSeleccion";
            this.txtSeleccion.Size = new System.Drawing.Size(70, 50);
            this.txtSeleccion.TabIndex = 15;
            this.txtSeleccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSeleccion.TextChanged += new System.EventHandler(this.txtSeleccion_TextChanged_1);
            this.txtSeleccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSeleccion_KeyPress_1);
            this.txtSeleccion.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSeleccion_KeyUp_1);
            // 
            // picSeleccion
            // 
            this.picSeleccion.Image = global::QuickHumCliente.Properties.RecursosPruebas.raven_pregunta;
            this.picSeleccion.Location = new System.Drawing.Point(55, 40);
            this.picSeleccion.Name = "picSeleccion";
            this.picSeleccion.Size = new System.Drawing.Size(166, 95);
            this.picSeleccion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSeleccion.TabIndex = 14;
            this.picSeleccion.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F);
            this.label10.Location = new System.Drawing.Point(42, 169);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 19);
            this.label10.TabIndex = 16;
            this.label10.Text = "Lamina:";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.gbPreguntas);
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Controls.Add(this.btnSiguiente);
            this.panel1.Controls.Add(this.btnAnterior);
            this.panel1.Controls.Add(this.btnAyuda);
            this.panel1.Location = new System.Drawing.Point(5, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 574);
            this.panel1.TabIndex = 105;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel9.Controls.Add(this.lblNumPreg);
            this.panel9.Location = new System.Drawing.Point(0, 467);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(631, 25);
            this.panel9.TabIndex = 79;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.label4);
            this.gbPreguntas.Controls.Add(this.picRes3);
            this.gbPreguntas.Controls.Add(this.label9);
            this.gbPreguntas.Controls.Add(this.picSuperior);
            this.gbPreguntas.Controls.Add(this.label6);
            this.gbPreguntas.Controls.Add(this.picRes1);
            this.gbPreguntas.Controls.Add(this.label8);
            this.gbPreguntas.Controls.Add(this.picRes2);
            this.gbPreguntas.Controls.Add(this.label5);
            this.gbPreguntas.Controls.Add(this.picRes5);
            this.gbPreguntas.Controls.Add(this.label1);
            this.gbPreguntas.Controls.Add(this.picRes4);
            this.gbPreguntas.Controls.Add(this.label2);
            this.gbPreguntas.Controls.Add(this.picRes6);
            this.gbPreguntas.Controls.Add(this.label7);
            this.gbPreguntas.Controls.Add(this.picResInf4);
            this.gbPreguntas.Controls.Add(this.picResSup4);
            this.gbPreguntas.Location = new System.Drawing.Point(5, 7);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(618, 460);
            this.gbPreguntas.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 366);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "5.";
            // 
            // picRes3
            // 
            this.picRes3.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRes3.Location = new System.Drawing.Point(321, 279);
            this.picRes3.Name = "picRes3";
            this.picRes3.Size = new System.Drawing.Size(133, 78);
            this.picRes3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes3.TabIndex = 5;
            this.picRes3.TabStop = false;
            this.picRes3.Click += new System.EventHandler(this.picRes3_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(455, 366);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "8.";
            this.label9.Visible = false;
            // 
            // picSuperior
            // 
            this.picSuperior.Location = new System.Drawing.Point(23, 18);
            this.picSuperior.Name = "picSuperior";
            this.picSuperior.Size = new System.Drawing.Size(485, 243);
            this.picSuperior.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSuperior.TabIndex = 4;
            this.picSuperior.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(305, 366);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "6.";
            // 
            // picRes1
            // 
            this.picRes1.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRes1.Location = new System.Drawing.Point(23, 279);
            this.picRes1.Name = "picRes1";
            this.picRes1.Size = new System.Drawing.Size(133, 78);
            this.picRes1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes1.TabIndex = 10;
            this.picRes1.TabStop = false;
            this.picRes1.Click += new System.EventHandler(this.picRes1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(455, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "4.";
            this.label8.Visible = false;
            // 
            // picRes2
            // 
            this.picRes2.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRes2.Location = new System.Drawing.Point(172, 279);
            this.picRes2.Name = "picRes2";
            this.picRes2.Size = new System.Drawing.Size(133, 78);
            this.picRes2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes2.TabIndex = 9;
            this.picRes2.TabStop = false;
            this.picRes2.Click += new System.EventHandler(this.picRes2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(305, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "3.";
            // 
            // picRes5
            // 
            this.picRes5.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRes5.Location = new System.Drawing.Point(172, 366);
            this.picRes5.Name = "picRes5";
            this.picRes5.Size = new System.Drawing.Size(133, 78);
            this.picRes5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes5.TabIndex = 8;
            this.picRes5.TabStop = false;
            this.picRes5.Click += new System.EventHandler(this.picRes5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 279);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "2.";
            // 
            // picRes4
            // 
            this.picRes4.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRes4.Location = new System.Drawing.Point(23, 367);
            this.picRes4.Name = "picRes4";
            this.picRes4.Size = new System.Drawing.Size(133, 78);
            this.picRes4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes4.TabIndex = 7;
            this.picRes4.TabStop = false;
            this.picRes4.Click += new System.EventHandler(this.picRes4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 367);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "4.";
            // 
            // picRes6
            // 
            this.picRes6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.picRes6.Location = new System.Drawing.Point(321, 366);
            this.picRes6.Name = "picRes6";
            this.picRes6.Size = new System.Drawing.Size(133, 78);
            this.picRes6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRes6.TabIndex = 6;
            this.picRes6.TabStop = false;
            this.picRes6.Click += new System.EventHandler(this.picRes6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "1.";
            // 
            // picResInf4
            // 
            this.picResInf4.Cursor = System.Windows.Forms.Cursors.Default;
            this.picResInf4.Location = new System.Drawing.Point(471, 366);
            this.picResInf4.Name = "picResInf4";
            this.picResInf4.Size = new System.Drawing.Size(133, 78);
            this.picResInf4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picResInf4.TabIndex = 6;
            this.picResInf4.TabStop = false;
            this.picResInf4.Visible = false;
            this.picResInf4.Click += new System.EventHandler(this.picResInf4_Click);
            // 
            // picResSup4
            // 
            this.picResSup4.Cursor = System.Windows.Forms.Cursors.Default;
            this.picResSup4.Location = new System.Drawing.Point(471, 279);
            this.picResSup4.Name = "picResSup4";
            this.picResSup4.Size = new System.Drawing.Size(133, 78);
            this.picResSup4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picResSup4.TabIndex = 5;
            this.picResSup4.TabStop = false;
            this.picResSup4.Visible = false;
            this.picResSup4.Click += new System.EventHandler(this.picResSup4_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(491, 512);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(118, 33);
            this.btnCancelar.TabIndex = 76;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(326, 512);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(118, 33);
            this.btnSiguiente.TabIndex = 74;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(177, 512);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(118, 33);
            this.btnAnterior.TabIndex = 78;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(12, 512);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnAyuda.TabIndex = 77;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.Image")));
            this.btnCloseWindow.Location = new System.Drawing.Point(892, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 103;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.lbltime);
            this.panel2.Location = new System.Drawing.Point(672, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 38);
            this.panel2.TabIndex = 101;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(55, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 19);
            this.label15.TabIndex = 65;
            this.label15.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbltime.Location = new System.Drawing.Point(173, 10);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // frmRaven
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(929, 675);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbRespuesta);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRaven";
            this.Text = "Prueba Raven";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRaven_FormClosing);
            this.Load += new System.EventHandler(this.frmRaven_Load);
            this.panelAyuda1.ResumeLayout(false);
            this.panelAyuda.ResumeLayout(false);
            this.gbRespuesta.ResumeLayout(false);
            this.gbRespuesta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccion)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRes3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSuperior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRes6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResInf4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResSup4)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panelAyuda1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Panel gbRespuesta;
        private System.Windows.Forms.TextBox txtSeleccion;
        private System.Windows.Forms.PictureBox picSeleccion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox picRes3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox picSuperior;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picRes1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox picRes2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picRes5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picRes4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picRes6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox picResInf4;
        private System.Windows.Forms.PictureBox picResSup4;
        private System.Windows.Forms.Panel panel9;
    }
}