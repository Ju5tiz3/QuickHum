﻿using System;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Raven
{
    public partial class frmRaven : Form
    {
        private readonly QuickHumClient Cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        private readonly EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;
        readonly dto_raven_respuestas_candidato respuestas = new dto_raven_respuestas_candidato();
        dto_raven_preguntas[] ListaPreguntas;
        dto_raven_respuestas_candidato[] RespuestasCandidato;

        //Vectores que contendran las imagenes
        Bitmap[] ImgSuperiores = new Bitmap[60];
        Bitmap[,] ImgRespuestas = new Bitmap[60, 8];

        int NumeroPrueba = 1;
        int x = 0;
        int Etapa = 1;
        int Recorrido = 0;

        public frmRaven(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad = escolaridad;
        }

        private void frmRaven_Load(object sender, EventArgs e)
        {
            DateTime hora;

            btnSiguiente.Name = "Siguiente";

            //this.panelAyuda.Location = new Point(1, 1);


            //Cargamos Las preguntas y respuestas de la prueba
            ListaPreguntas = Cliente.ListaRavenPreguntas();

            hora = Convert.ToDateTime("0:45:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);


        }

        private void MostrarImagenes(int i)
        {
            //Validamos la etapa en que nos encontramos
            if (x < 24)
            {
                Etapa = 1;
            }
            else
            {
                Etapa = 2;
            }

            //Mostramos las imagenes Superiores
            picSuperior.Image = ImgSuperiores[i];


            //Mostramos las posibles respuestas
            if (Etapa == 1)
            {
                picRes1.Image = ImgRespuestas[i, 0];
                picRes2.Image = ImgRespuestas[i, 1];
                picRes3.Image = ImgRespuestas[i, 2];
                picRes4.Image = ImgRespuestas[i, 3];
                picRes5.Image = ImgRespuestas[i, 4];
                picRes6.Image = ImgRespuestas[i, 5];

                //Ocultamos 2 Opciones
                picResSup4.Visible = false;
                picResInf4.Visible = false;

                //Corregimos los valores de los Label
                label8.Visible = false;
                label2.Text = "4";
                label4.Text = "5";
                label6.Text = "6";
                label9.Visible = false;

            }
            else if (Etapa == 2)
            {
                picRes1.Image = ImgRespuestas[i, 0];
                picRes2.Image = ImgRespuestas[i, 1];
                picRes3.Image = ImgRespuestas[i, 2];
                picResSup4.Image = ImgRespuestas[i, 3];
                picRes4.Image = ImgRespuestas[i, 4];
                picRes5.Image = ImgRespuestas[i, 5];
                picRes6.Image = ImgRespuestas[i, 6];
                picResInf4.Image = ImgRespuestas[i, 7];

                //Ponemos visibles 2 opciones mas
                picResSup4.Visible = true;
                picResInf4.Visible = true;

                //Corregimos los valores de los Label
                label8.Visible = true;
                label2.Text = "5";
                label4.Text = "6";
                label6.Text = "7";
                label9.Visible = true;

            }


            if (Recorrido > i)
            {

                txtSeleccion.Text = RespuestasCandidato[i].Respuesta.ToString();
                SeleccionarImagen();

            }


        }

        private void CargarImagenes()
        {

            //Declaramos todas las imagenes superiores
            ImgSuperiores[0] = Properties.RecursosPruebas.ravenP1;
            ImgSuperiores[1] = Properties.RecursosPruebas.ravenP2;
            ImgSuperiores[2] = Properties.RecursosPruebas.ravenP3;
            ImgSuperiores[3] = Properties.RecursosPruebas.ravenP4;
            ImgSuperiores[4] = Properties.RecursosPruebas.ravenP5;
            ImgSuperiores[5] = Properties.RecursosPruebas.ravenP6;
            ImgSuperiores[6] = Properties.RecursosPruebas.ravenP7;
            ImgSuperiores[7] = Properties.RecursosPruebas.ravenP8;
            ImgSuperiores[8] = Properties.RecursosPruebas.ravenP9;
            ImgSuperiores[9] = Properties.RecursosPruebas.ravenP10;
            ImgSuperiores[10] = Properties.RecursosPruebas.ravenP11;
            ImgSuperiores[11] = Properties.RecursosPruebas.ravenP12;
            ImgSuperiores[12] = Properties.RecursosPruebas.ravenP13;
            ImgSuperiores[13] = Properties.RecursosPruebas.ravenP14;
            ImgSuperiores[14] = Properties.RecursosPruebas.ravenP15;
            ImgSuperiores[15] = Properties.RecursosPruebas.ravenP16;
            ImgSuperiores[16] = Properties.RecursosPruebas.ravenP17;
            ImgSuperiores[17] = Properties.RecursosPruebas.ravenP18;
            ImgSuperiores[18] = Properties.RecursosPruebas.ravenP19;
            ImgSuperiores[19] = Properties.RecursosPruebas.ravenP20;
            ImgSuperiores[20] = Properties.RecursosPruebas.ravenP21;
            ImgSuperiores[21] = Properties.RecursosPruebas.ravenP22;
            ImgSuperiores[22] = Properties.RecursosPruebas.ravenP23;
            ImgSuperiores[23] = Properties.RecursosPruebas.ravenP24;
            ImgSuperiores[24] = Properties.RecursosPruebas.ravenP25;
            ImgSuperiores[25] = Properties.RecursosPruebas.ravenP26;
            ImgSuperiores[26] = Properties.RecursosPruebas.ravenP27;
            ImgSuperiores[27] = Properties.RecursosPruebas.ravenP28;
            ImgSuperiores[28] = Properties.RecursosPruebas.ravenP29;
            ImgSuperiores[29] = Properties.RecursosPruebas.ravenP30;
            ImgSuperiores[30] = Properties.RecursosPruebas.ravenP31;
            ImgSuperiores[31] = Properties.RecursosPruebas.ravenP32;
            ImgSuperiores[32] = Properties.RecursosPruebas.ravenP33;
            ImgSuperiores[33] = Properties.RecursosPruebas.ravenP34;
            ImgSuperiores[34] = Properties.RecursosPruebas.ravenP35;
            ImgSuperiores[35] = Properties.RecursosPruebas.ravenP36;
            ImgSuperiores[36] = Properties.RecursosPruebas.ravenP37;
            ImgSuperiores[37] = Properties.RecursosPruebas.ravenP38;
            ImgSuperiores[38] = Properties.RecursosPruebas.ravenP39;
            ImgSuperiores[39] = Properties.RecursosPruebas.ravenP40;
            ImgSuperiores[40] = Properties.RecursosPruebas.ravenP41;
            ImgSuperiores[41] = Properties.RecursosPruebas.ravenP42;
            ImgSuperiores[42] = Properties.RecursosPruebas.ravenP43;
            ImgSuperiores[43] = Properties.RecursosPruebas.ravenP44;
            ImgSuperiores[44] = Properties.RecursosPruebas.ravenP45;
            ImgSuperiores[45] = Properties.RecursosPruebas.ravenP46;
            ImgSuperiores[46] = Properties.RecursosPruebas.ravenP47;
            ImgSuperiores[47] = Properties.RecursosPruebas.ravenP48;
            ImgSuperiores[48] = Properties.RecursosPruebas.ravenP49;
            ImgSuperiores[49] = Properties.RecursosPruebas.ravenP50;
            ImgSuperiores[50] = Properties.RecursosPruebas.ravenP51;
            ImgSuperiores[51] = Properties.RecursosPruebas.ravenP52;
            ImgSuperiores[52] = Properties.RecursosPruebas.ravenP53;
            ImgSuperiores[53] = Properties.RecursosPruebas.ravenP54;
            ImgSuperiores[54] = Properties.RecursosPruebas.ravenP55;
            ImgSuperiores[55] = Properties.RecursosPruebas.ravenP56;
            ImgSuperiores[56] = Properties.RecursosPruebas.ravenP57;
            ImgSuperiores[57] = Properties.RecursosPruebas.ravenP58;
            ImgSuperiores[58] = Properties.RecursosPruebas.ravenP59;
            ImgSuperiores[59] = Properties.RecursosPruebas.ravenP60;


            //Declaramos las posibles respuestas
            ImgRespuestas[0, 0] = Properties.RecursosPruebas.raven1_1;
            ImgRespuestas[0, 1] = Properties.RecursosPruebas.raven1_2;
            ImgRespuestas[0, 2] = Properties.RecursosPruebas.raven1_3;
            ImgRespuestas[0, 3] = Properties.RecursosPruebas.raven1_4;
            ImgRespuestas[0, 4] = Properties.RecursosPruebas.raven1_5;
            ImgRespuestas[0, 5] = Properties.RecursosPruebas.raven1_6;

            ImgRespuestas[1, 0] = Properties.RecursosPruebas.raven2_1;
            ImgRespuestas[1, 1] = Properties.RecursosPruebas.raven2_2;
            ImgRespuestas[1, 2] = Properties.RecursosPruebas.raven2_3;
            ImgRespuestas[1, 3] = Properties.RecursosPruebas.raven2_4;
            ImgRespuestas[1, 4] = Properties.RecursosPruebas.raven2_5;
            ImgRespuestas[1, 5] = Properties.RecursosPruebas.raven2_6;

            ImgRespuestas[2, 0] = Properties.RecursosPruebas.raven3_1;
            ImgRespuestas[2, 1] = Properties.RecursosPruebas.raven3_2;
            ImgRespuestas[2, 2] = Properties.RecursosPruebas.raven3_3;
            ImgRespuestas[2, 3] = Properties.RecursosPruebas.raven3_4;
            ImgRespuestas[2, 4] = Properties.RecursosPruebas.raven3_5;
            ImgRespuestas[2, 5] = Properties.RecursosPruebas.raven3_6;

            ImgRespuestas[3, 0] = Properties.RecursosPruebas.raven4_1;
            ImgRespuestas[3, 1] = Properties.RecursosPruebas.raven4_2;
            ImgRespuestas[3, 2] = Properties.RecursosPruebas.raven4_3;
            ImgRespuestas[3, 3] = Properties.RecursosPruebas.raven4_4;
            ImgRespuestas[3, 4] = Properties.RecursosPruebas.raven4_5;
            ImgRespuestas[3, 5] = Properties.RecursosPruebas.raven4_6;

            ImgRespuestas[4, 0] = Properties.RecursosPruebas.raven5_1;
            ImgRespuestas[4, 1] = Properties.RecursosPruebas.raven5_2;
            ImgRespuestas[4, 2] = Properties.RecursosPruebas.raven5_3;
            ImgRespuestas[4, 3] = Properties.RecursosPruebas.raven5_4;
            ImgRespuestas[4, 4] = Properties.RecursosPruebas.raven5_5;
            ImgRespuestas[4, 5] = Properties.RecursosPruebas.raven5_6;

            ImgRespuestas[5, 0] = Properties.RecursosPruebas.raven6_1;
            ImgRespuestas[5, 1] = Properties.RecursosPruebas.raven6_2;
            ImgRespuestas[5, 2] = Properties.RecursosPruebas.raven6_3;
            ImgRespuestas[5, 3] = Properties.RecursosPruebas.raven6_4;
            ImgRespuestas[5, 4] = Properties.RecursosPruebas.raven6_5;
            ImgRespuestas[5, 5] = Properties.RecursosPruebas.raven6_6;

            ImgRespuestas[6, 0] = Properties.RecursosPruebas.raven7_1;
            ImgRespuestas[6, 1] = Properties.RecursosPruebas.raven7_2;
            ImgRespuestas[6, 2] = Properties.RecursosPruebas.raven7_3;
            ImgRespuestas[6, 3] = Properties.RecursosPruebas.raven7_4;
            ImgRespuestas[6, 4] = Properties.RecursosPruebas.raven7_5;
            ImgRespuestas[6, 5] = Properties.RecursosPruebas.raven7_6;

            ImgRespuestas[7, 0] = Properties.RecursosPruebas.raven8_1;
            ImgRespuestas[7, 1] = Properties.RecursosPruebas.raven8_2;
            ImgRespuestas[7, 2] = Properties.RecursosPruebas.raven8_3;
            ImgRespuestas[7, 3] = Properties.RecursosPruebas.raven8_4;
            ImgRespuestas[7, 4] = Properties.RecursosPruebas.raven8_5;
            ImgRespuestas[7, 5] = Properties.RecursosPruebas.raven8_6;

            ImgRespuestas[8, 0] = Properties.RecursosPruebas.raven9_1;
            ImgRespuestas[8, 1] = Properties.RecursosPruebas.raven9_2;
            ImgRespuestas[8, 2] = Properties.RecursosPruebas.raven9_3;
            ImgRespuestas[8, 3] = Properties.RecursosPruebas.raven9_4;
            ImgRespuestas[8, 4] = Properties.RecursosPruebas.raven9_5;
            ImgRespuestas[8, 5] = Properties.RecursosPruebas.raven9_6;

            ImgRespuestas[9, 0] = Properties.RecursosPruebas.raven10_1;
            ImgRespuestas[9, 1] = Properties.RecursosPruebas.raven10_2;
            ImgRespuestas[9, 2] = Properties.RecursosPruebas.raven10_3;
            ImgRespuestas[9, 3] = Properties.RecursosPruebas.raven10_4;
            ImgRespuestas[9, 4] = Properties.RecursosPruebas.raven10_5;
            ImgRespuestas[9, 5] = Properties.RecursosPruebas.raven10_6;

            ImgRespuestas[10, 0] = Properties.RecursosPruebas.raven11_1;
            ImgRespuestas[10, 1] = Properties.RecursosPruebas.raven11_2;
            ImgRespuestas[10, 2] = Properties.RecursosPruebas.raven11_3;
            ImgRespuestas[10, 3] = Properties.RecursosPruebas.raven11_4;
            ImgRespuestas[10, 4] = Properties.RecursosPruebas.raven11_5;
            ImgRespuestas[10, 5] = Properties.RecursosPruebas.raven11_6;

            ImgRespuestas[11, 0] = Properties.RecursosPruebas.raven12_1;
            ImgRespuestas[11, 1] = Properties.RecursosPruebas.raven12_2;
            ImgRespuestas[11, 2] = Properties.RecursosPruebas.raven12_3;
            ImgRespuestas[11, 3] = Properties.RecursosPruebas.raven12_4;
            ImgRespuestas[11, 4] = Properties.RecursosPruebas.raven12_5;
            ImgRespuestas[11, 5] = Properties.RecursosPruebas.raven12_6;

            ImgRespuestas[12, 0] = Properties.RecursosPruebas.raven13_1;
            ImgRespuestas[12, 1] = Properties.RecursosPruebas.raven13_2;
            ImgRespuestas[12, 2] = Properties.RecursosPruebas.raven13_3;
            ImgRespuestas[12, 3] = Properties.RecursosPruebas.raven13_4;
            ImgRespuestas[12, 4] = Properties.RecursosPruebas.raven13_5;
            ImgRespuestas[12, 5] = Properties.RecursosPruebas.raven13_6;

            ImgRespuestas[13, 0] = Properties.RecursosPruebas.raven14_1;
            ImgRespuestas[13, 1] = Properties.RecursosPruebas.raven14_2;
            ImgRespuestas[13, 2] = Properties.RecursosPruebas.raven14_3;
            ImgRespuestas[13, 3] = Properties.RecursosPruebas.raven14_4;
            ImgRespuestas[13, 4] = Properties.RecursosPruebas.raven14_5;
            ImgRespuestas[13, 5] = Properties.RecursosPruebas.raven14_6;

            ImgRespuestas[14, 0] = Properties.RecursosPruebas.raven15_1;
            ImgRespuestas[14, 1] = Properties.RecursosPruebas.raven15_2;
            ImgRespuestas[14, 2] = Properties.RecursosPruebas.raven15_3;
            ImgRespuestas[14, 3] = Properties.RecursosPruebas.raven15_4;
            ImgRespuestas[14, 4] = Properties.RecursosPruebas.raven15_5;
            ImgRespuestas[14, 5] = Properties.RecursosPruebas.raven15_6;

            ImgRespuestas[15, 0] = Properties.RecursosPruebas.raven16_1;
            ImgRespuestas[15, 1] = Properties.RecursosPruebas.raven16_2;
            ImgRespuestas[15, 2] = Properties.RecursosPruebas.raven16_3;
            ImgRespuestas[15, 3] = Properties.RecursosPruebas.raven16_4;
            ImgRespuestas[15, 4] = Properties.RecursosPruebas.raven16_5;
            ImgRespuestas[15, 5] = Properties.RecursosPruebas.raven16_6;

            ImgRespuestas[16, 0] = Properties.RecursosPruebas.raven17_1;
            ImgRespuestas[16, 1] = Properties.RecursosPruebas.raven17_2;
            ImgRespuestas[16, 2] = Properties.RecursosPruebas.raven17_3;
            ImgRespuestas[16, 3] = Properties.RecursosPruebas.raven17_4;
            ImgRespuestas[16, 4] = Properties.RecursosPruebas.raven17_5;
            ImgRespuestas[16, 5] = Properties.RecursosPruebas.raven17_6;

            ImgRespuestas[17, 0] = Properties.RecursosPruebas.raven18_1;
            ImgRespuestas[17, 1] = Properties.RecursosPruebas.raven18_2;
            ImgRespuestas[17, 2] = Properties.RecursosPruebas.raven18_3;
            ImgRespuestas[17, 3] = Properties.RecursosPruebas.raven18_4;
            ImgRespuestas[17, 4] = Properties.RecursosPruebas.raven18_5;
            ImgRespuestas[17, 5] = Properties.RecursosPruebas.raven18_6;

            ImgRespuestas[18, 0] = Properties.RecursosPruebas.raven19_1;
            ImgRespuestas[18, 1] = Properties.RecursosPruebas.raven19_2;
            ImgRespuestas[18, 2] = Properties.RecursosPruebas.raven19_3;
            ImgRespuestas[18, 3] = Properties.RecursosPruebas.raven19_4;
            ImgRespuestas[18, 4] = Properties.RecursosPruebas.raven19_5;
            ImgRespuestas[18, 5] = Properties.RecursosPruebas.raven19_6;

            ImgRespuestas[19, 0] = Properties.RecursosPruebas.raven20_1;
            ImgRespuestas[19, 1] = Properties.RecursosPruebas.raven20_2;
            ImgRespuestas[19, 2] = Properties.RecursosPruebas.raven20_3;
            ImgRespuestas[19, 3] = Properties.RecursosPruebas.raven20_4;
            ImgRespuestas[19, 4] = Properties.RecursosPruebas.raven20_5;
            ImgRespuestas[19, 5] = Properties.RecursosPruebas.raven20_6;

            ImgRespuestas[20, 0] = Properties.RecursosPruebas.raven21_1;
            ImgRespuestas[20, 1] = Properties.RecursosPruebas.raven21_2;
            ImgRespuestas[20, 2] = Properties.RecursosPruebas.raven21_3;
            ImgRespuestas[20, 3] = Properties.RecursosPruebas.raven21_4;
            ImgRespuestas[20, 4] = Properties.RecursosPruebas.raven21_5;
            ImgRespuestas[20, 5] = Properties.RecursosPruebas.raven21_6;

            ImgRespuestas[21, 0] = Properties.RecursosPruebas.raven22_1;
            ImgRespuestas[21, 1] = Properties.RecursosPruebas.raven22_2;
            ImgRespuestas[21, 2] = Properties.RecursosPruebas.raven22_3;
            ImgRespuestas[21, 3] = Properties.RecursosPruebas.raven22_4;
            ImgRespuestas[21, 4] = Properties.RecursosPruebas.raven22_5;
            ImgRespuestas[21, 5] = Properties.RecursosPruebas.raven22_6;

            ImgRespuestas[22, 0] = Properties.RecursosPruebas.raven23_1;
            ImgRespuestas[22, 1] = Properties.RecursosPruebas.raven23_2;
            ImgRespuestas[22, 2] = Properties.RecursosPruebas.raven23_3;
            ImgRespuestas[22, 3] = Properties.RecursosPruebas.raven23_4;
            ImgRespuestas[22, 4] = Properties.RecursosPruebas.raven23_5;
            ImgRespuestas[22, 5] = Properties.RecursosPruebas.raven23_6;

            ImgRespuestas[23, 0] = Properties.RecursosPruebas.raven24_1;
            ImgRespuestas[23, 1] = Properties.RecursosPruebas.raven24_2;
            ImgRespuestas[23, 2] = Properties.RecursosPruebas.raven24_3;
            ImgRespuestas[23, 3] = Properties.RecursosPruebas.raven24_4;
            ImgRespuestas[23, 4] = Properties.RecursosPruebas.raven24_5;
            ImgRespuestas[23, 5] = Properties.RecursosPruebas.raven24_6;

            ImgRespuestas[24, 0] = Properties.RecursosPruebas.raven25_1;
            ImgRespuestas[24, 1] = Properties.RecursosPruebas.raven25_2;
            ImgRespuestas[24, 2] = Properties.RecursosPruebas.raven25_3;
            ImgRespuestas[24, 3] = Properties.RecursosPruebas.raven25_4;
            ImgRespuestas[24, 4] = Properties.RecursosPruebas.raven25_5;
            ImgRespuestas[24, 5] = Properties.RecursosPruebas.raven25_6;
            ImgRespuestas[24, 6] = Properties.RecursosPruebas.raven25_7;
            ImgRespuestas[24, 7] = Properties.RecursosPruebas.raven25_8;

            ImgRespuestas[25, 0] = Properties.RecursosPruebas.raven26_1;
            ImgRespuestas[25, 1] = Properties.RecursosPruebas.raven26_2;
            ImgRespuestas[25, 2] = Properties.RecursosPruebas.raven26_3;
            ImgRespuestas[25, 3] = Properties.RecursosPruebas.raven26_4;
            ImgRespuestas[25, 4] = Properties.RecursosPruebas.raven26_5;
            ImgRespuestas[25, 5] = Properties.RecursosPruebas.raven26_6;
            ImgRespuestas[25, 6] = Properties.RecursosPruebas.raven26_7;
            ImgRespuestas[25, 7] = Properties.RecursosPruebas.raven26_8;

            ImgRespuestas[26, 0] = Properties.RecursosPruebas.raven27_1;
            ImgRespuestas[26, 1] = Properties.RecursosPruebas.raven27_2;
            ImgRespuestas[26, 2] = Properties.RecursosPruebas.raven27_3;
            ImgRespuestas[26, 3] = Properties.RecursosPruebas.raven27_4;
            ImgRespuestas[26, 4] = Properties.RecursosPruebas.raven27_5;
            ImgRespuestas[26, 5] = Properties.RecursosPruebas.raven27_6;
            ImgRespuestas[26, 6] = Properties.RecursosPruebas.raven27_7;
            ImgRespuestas[26, 7] = Properties.RecursosPruebas.raven27_8;

            ImgRespuestas[27, 0] = Properties.RecursosPruebas.raven28_1;
            ImgRespuestas[27, 1] = Properties.RecursosPruebas.raven28_2;
            ImgRespuestas[27, 2] = Properties.RecursosPruebas.raven28_3;
            ImgRespuestas[27, 3] = Properties.RecursosPruebas.raven28_4;
            ImgRespuestas[27, 4] = Properties.RecursosPruebas.raven28_5;
            ImgRespuestas[27, 5] = Properties.RecursosPruebas.raven28_6;
            ImgRespuestas[27, 6] = Properties.RecursosPruebas.raven28_7;
            ImgRespuestas[27, 7] = Properties.RecursosPruebas.raven28_8;

            ImgRespuestas[28, 0] = Properties.RecursosPruebas.raven29_1;
            ImgRespuestas[28, 1] = Properties.RecursosPruebas.raven29_2;
            ImgRespuestas[28, 2] = Properties.RecursosPruebas.raven29_3;
            ImgRespuestas[28, 3] = Properties.RecursosPruebas.raven29_4;
            ImgRespuestas[28, 4] = Properties.RecursosPruebas.raven29_5;
            ImgRespuestas[28, 5] = Properties.RecursosPruebas.raven29_6;
            ImgRespuestas[28, 6] = Properties.RecursosPruebas.raven29_7;
            ImgRespuestas[28, 7] = Properties.RecursosPruebas.raven29_8;

            ImgRespuestas[29, 0] = Properties.RecursosPruebas.raven30_1;
            ImgRespuestas[29, 1] = Properties.RecursosPruebas.raven30_2;
            ImgRespuestas[29, 2] = Properties.RecursosPruebas.raven30_3;
            ImgRespuestas[29, 3] = Properties.RecursosPruebas.raven30_4;
            ImgRespuestas[29, 4] = Properties.RecursosPruebas.raven30_5;
            ImgRespuestas[29, 5] = Properties.RecursosPruebas.raven30_6;
            ImgRespuestas[29, 6] = Properties.RecursosPruebas.raven30_7;
            ImgRespuestas[29, 7] = Properties.RecursosPruebas.raven30_8;

            ImgRespuestas[30, 0] = Properties.RecursosPruebas.raven31_1;
            ImgRespuestas[30, 1] = Properties.RecursosPruebas.raven31_2;
            ImgRespuestas[30, 2] = Properties.RecursosPruebas.raven31_3;
            ImgRespuestas[30, 3] = Properties.RecursosPruebas.raven31_4;
            ImgRespuestas[30, 4] = Properties.RecursosPruebas.raven31_5;
            ImgRespuestas[30, 5] = Properties.RecursosPruebas.raven31_6;
            ImgRespuestas[30, 6] = Properties.RecursosPruebas.raven31_7;
            ImgRespuestas[30, 7] = Properties.RecursosPruebas.raven31_8;

            ImgRespuestas[31, 0] = Properties.RecursosPruebas.raven32_1;
            ImgRespuestas[31, 1] = Properties.RecursosPruebas.raven32_2;
            ImgRespuestas[31, 2] = Properties.RecursosPruebas.raven32_3;
            ImgRespuestas[31, 3] = Properties.RecursosPruebas.raven32_4;
            ImgRespuestas[31, 4] = Properties.RecursosPruebas.raven32_5;
            ImgRespuestas[31, 5] = Properties.RecursosPruebas.raven32_6;
            ImgRespuestas[31, 6] = Properties.RecursosPruebas.raven32_7;
            ImgRespuestas[31, 7] = Properties.RecursosPruebas.raven32_8;

            ImgRespuestas[32, 0] = Properties.RecursosPruebas.raven33_1;
            ImgRespuestas[32, 1] = Properties.RecursosPruebas.raven33_2;
            ImgRespuestas[32, 2] = Properties.RecursosPruebas.raven33_3;
            ImgRespuestas[32, 3] = Properties.RecursosPruebas.raven33_4;
            ImgRespuestas[32, 4] = Properties.RecursosPruebas.raven33_5;
            ImgRespuestas[32, 5] = Properties.RecursosPruebas.raven33_6;
            ImgRespuestas[32, 6] = Properties.RecursosPruebas.raven33_7;
            ImgRespuestas[32, 7] = Properties.RecursosPruebas.raven33_8;

            ImgRespuestas[33, 0] = Properties.RecursosPruebas.raven34_1;
            ImgRespuestas[33, 1] = Properties.RecursosPruebas.raven34_2;
            ImgRespuestas[33, 2] = Properties.RecursosPruebas.raven34_3;
            ImgRespuestas[33, 3] = Properties.RecursosPruebas.raven34_4;
            ImgRespuestas[33, 4] = Properties.RecursosPruebas.raven34_5;
            ImgRespuestas[33, 5] = Properties.RecursosPruebas.raven34_6;
            ImgRespuestas[33, 6] = Properties.RecursosPruebas.raven34_7;
            ImgRespuestas[33, 7] = Properties.RecursosPruebas.raven34_8;

            ImgRespuestas[34, 0] = Properties.RecursosPruebas.raven35_1;
            ImgRespuestas[34, 1] = Properties.RecursosPruebas.raven35_2;
            ImgRespuestas[34, 2] = Properties.RecursosPruebas.raven35_3;
            ImgRespuestas[34, 3] = Properties.RecursosPruebas.raven35_4;
            ImgRespuestas[34, 4] = Properties.RecursosPruebas.raven35_5;
            ImgRespuestas[34, 5] = Properties.RecursosPruebas.raven35_6;
            ImgRespuestas[34, 6] = Properties.RecursosPruebas.raven35_7;
            ImgRespuestas[34, 7] = Properties.RecursosPruebas.raven35_8;

            ImgRespuestas[35, 0] = Properties.RecursosPruebas.raven36_1;
            ImgRespuestas[35, 1] = Properties.RecursosPruebas.raven36_2;
            ImgRespuestas[35, 2] = Properties.RecursosPruebas.raven36_3;
            ImgRespuestas[35, 3] = Properties.RecursosPruebas.raven36_4;
            ImgRespuestas[35, 4] = Properties.RecursosPruebas.raven36_5;
            ImgRespuestas[35, 5] = Properties.RecursosPruebas.raven36_6;
            ImgRespuestas[35, 6] = Properties.RecursosPruebas.raven36_7;
            ImgRespuestas[35, 7] = Properties.RecursosPruebas.raven36_8;

            ImgRespuestas[31, 0] = Properties.RecursosPruebas.raven32_1;
            ImgRespuestas[31, 1] = Properties.RecursosPruebas.raven32_2;
            ImgRespuestas[31, 2] = Properties.RecursosPruebas.raven32_3;
            ImgRespuestas[31, 3] = Properties.RecursosPruebas.raven32_4;
            ImgRespuestas[31, 4] = Properties.RecursosPruebas.raven32_5;
            ImgRespuestas[31, 5] = Properties.RecursosPruebas.raven32_6;
            ImgRespuestas[31, 6] = Properties.RecursosPruebas.raven32_7;
            ImgRespuestas[31, 7] = Properties.RecursosPruebas.raven32_8;

            ImgRespuestas[32, 0] = Properties.RecursosPruebas.raven33_1;
            ImgRespuestas[32, 1] = Properties.RecursosPruebas.raven33_2;
            ImgRespuestas[32, 2] = Properties.RecursosPruebas.raven33_3;
            ImgRespuestas[32, 3] = Properties.RecursosPruebas.raven33_4;
            ImgRespuestas[32, 4] = Properties.RecursosPruebas.raven33_5;
            ImgRespuestas[32, 5] = Properties.RecursosPruebas.raven33_6;
            ImgRespuestas[32, 6] = Properties.RecursosPruebas.raven33_7;
            ImgRespuestas[32, 7] = Properties.RecursosPruebas.raven33_8;

            ImgRespuestas[33, 0] = Properties.RecursosPruebas.raven34_1;
            ImgRespuestas[33, 1] = Properties.RecursosPruebas.raven34_2;
            ImgRespuestas[33, 2] = Properties.RecursosPruebas.raven34_3;
            ImgRespuestas[33, 3] = Properties.RecursosPruebas.raven34_4;
            ImgRespuestas[33, 4] = Properties.RecursosPruebas.raven34_5;
            ImgRespuestas[33, 5] = Properties.RecursosPruebas.raven34_6;
            ImgRespuestas[33, 6] = Properties.RecursosPruebas.raven34_7;
            ImgRespuestas[33, 7] = Properties.RecursosPruebas.raven34_8;

            ImgRespuestas[34, 0] = Properties.RecursosPruebas.raven35_1;
            ImgRespuestas[34, 1] = Properties.RecursosPruebas.raven35_2;
            ImgRespuestas[34, 2] = Properties.RecursosPruebas.raven35_3;
            ImgRespuestas[34, 3] = Properties.RecursosPruebas.raven35_4;
            ImgRespuestas[34, 4] = Properties.RecursosPruebas.raven35_5;
            ImgRespuestas[34, 5] = Properties.RecursosPruebas.raven35_6;
            ImgRespuestas[34, 6] = Properties.RecursosPruebas.raven35_7;
            ImgRespuestas[34, 7] = Properties.RecursosPruebas.raven35_8;

            ImgRespuestas[35, 0] = Properties.RecursosPruebas.raven36_1;
            ImgRespuestas[35, 1] = Properties.RecursosPruebas.raven36_2;
            ImgRespuestas[35, 2] = Properties.RecursosPruebas.raven36_3;
            ImgRespuestas[35, 3] = Properties.RecursosPruebas.raven36_4;
            ImgRespuestas[35, 4] = Properties.RecursosPruebas.raven36_5;
            ImgRespuestas[35, 5] = Properties.RecursosPruebas.raven36_6;
            ImgRespuestas[35, 6] = Properties.RecursosPruebas.raven36_7;
            ImgRespuestas[35, 7] = Properties.RecursosPruebas.raven36_8;

            ImgRespuestas[36, 0] = Properties.RecursosPruebas.raven37_1;
            ImgRespuestas[36, 1] = Properties.RecursosPruebas.raven37_2;
            ImgRespuestas[36, 2] = Properties.RecursosPruebas.raven37_3;
            ImgRespuestas[36, 3] = Properties.RecursosPruebas.raven37_4;
            ImgRespuestas[36, 4] = Properties.RecursosPruebas.raven37_5;
            ImgRespuestas[36, 5] = Properties.RecursosPruebas.raven37_6;
            ImgRespuestas[36, 6] = Properties.RecursosPruebas.raven37_7;
            ImgRespuestas[36, 7] = Properties.RecursosPruebas.raven37_8;

            ImgRespuestas[37, 0] = Properties.RecursosPruebas.raven38_1;
            ImgRespuestas[37, 1] = Properties.RecursosPruebas.raven38_2;
            ImgRespuestas[37, 2] = Properties.RecursosPruebas.raven38_3;
            ImgRespuestas[37, 3] = Properties.RecursosPruebas.raven38_4;
            ImgRespuestas[37, 4] = Properties.RecursosPruebas.raven38_5;
            ImgRespuestas[37, 5] = Properties.RecursosPruebas.raven38_6;
            ImgRespuestas[37, 6] = Properties.RecursosPruebas.raven38_7;
            ImgRespuestas[37, 7] = Properties.RecursosPruebas.raven38_8;

            ImgRespuestas[38, 0] = Properties.RecursosPruebas.raven39_1;
            ImgRespuestas[38, 1] = Properties.RecursosPruebas.raven39_2;
            ImgRespuestas[38, 2] = Properties.RecursosPruebas.raven39_3;
            ImgRespuestas[38, 3] = Properties.RecursosPruebas.raven39_4;
            ImgRespuestas[38, 4] = Properties.RecursosPruebas.raven39_5;
            ImgRespuestas[38, 5] = Properties.RecursosPruebas.raven39_6;
            ImgRespuestas[38, 6] = Properties.RecursosPruebas.raven39_7;
            ImgRespuestas[38, 7] = Properties.RecursosPruebas.raven39_8;

            ImgRespuestas[39, 0] = Properties.RecursosPruebas.raven40_1;
            ImgRespuestas[39, 1] = Properties.RecursosPruebas.raven40_2;
            ImgRespuestas[39, 2] = Properties.RecursosPruebas.raven40_3;
            ImgRespuestas[39, 3] = Properties.RecursosPruebas.raven40_4;
            ImgRespuestas[39, 4] = Properties.RecursosPruebas.raven40_5;
            ImgRespuestas[39, 5] = Properties.RecursosPruebas.raven40_6;
            ImgRespuestas[39, 6] = Properties.RecursosPruebas.raven40_7;
            ImgRespuestas[39, 7] = Properties.RecursosPruebas.raven40_8;

            ImgRespuestas[40, 0] = Properties.RecursosPruebas.raven41_1;
            ImgRespuestas[40, 1] = Properties.RecursosPruebas.raven41_2;
            ImgRespuestas[40, 2] = Properties.RecursosPruebas.raven41_3;
            ImgRespuestas[40, 3] = Properties.RecursosPruebas.raven41_4;
            ImgRespuestas[40, 4] = Properties.RecursosPruebas.raven41_5;
            ImgRespuestas[40, 5] = Properties.RecursosPruebas.raven41_6;
            ImgRespuestas[40, 6] = Properties.RecursosPruebas.raven41_7;
            ImgRespuestas[40, 7] = Properties.RecursosPruebas.raven41_8;

            ImgRespuestas[41, 0] = Properties.RecursosPruebas.raven42_1;
            ImgRespuestas[41, 1] = Properties.RecursosPruebas.raven42_2;
            ImgRespuestas[41, 2] = Properties.RecursosPruebas.raven42_3;
            ImgRespuestas[41, 3] = Properties.RecursosPruebas.raven42_4;
            ImgRespuestas[41, 4] = Properties.RecursosPruebas.raven42_5;
            ImgRespuestas[41, 5] = Properties.RecursosPruebas.raven42_6;
            ImgRespuestas[41, 6] = Properties.RecursosPruebas.raven42_7;
            ImgRespuestas[41, 7] = Properties.RecursosPruebas.raven42_8;

            ImgRespuestas[42, 0] = Properties.RecursosPruebas.raven43_1;
            ImgRespuestas[42, 1] = Properties.RecursosPruebas.raven43_2;
            ImgRespuestas[42, 2] = Properties.RecursosPruebas.raven43_3;
            ImgRespuestas[42, 3] = Properties.RecursosPruebas.raven43_4;
            ImgRespuestas[42, 4] = Properties.RecursosPruebas.raven43_5;
            ImgRespuestas[42, 5] = Properties.RecursosPruebas.raven43_6;
            ImgRespuestas[42, 6] = Properties.RecursosPruebas.raven43_7;
            ImgRespuestas[42, 7] = Properties.RecursosPruebas.raven43_8;

            ImgRespuestas[43, 0] = Properties.RecursosPruebas.raven44_1;
            ImgRespuestas[43, 1] = Properties.RecursosPruebas.raven44_2;
            ImgRespuestas[43, 2] = Properties.RecursosPruebas.raven44_3;
            ImgRespuestas[43, 3] = Properties.RecursosPruebas.raven44_4;
            ImgRespuestas[43, 4] = Properties.RecursosPruebas.raven44_5;
            ImgRespuestas[43, 5] = Properties.RecursosPruebas.raven44_6;
            ImgRespuestas[43, 6] = Properties.RecursosPruebas.raven44_7;
            ImgRespuestas[43, 7] = Properties.RecursosPruebas.raven44_8;

            ImgRespuestas[44, 0] = Properties.RecursosPruebas.raven45_1;
            ImgRespuestas[44, 1] = Properties.RecursosPruebas.raven45_2;
            ImgRespuestas[44, 2] = Properties.RecursosPruebas.raven45_3;
            ImgRespuestas[44, 3] = Properties.RecursosPruebas.raven45_4;
            ImgRespuestas[44, 4] = Properties.RecursosPruebas.raven45_5;
            ImgRespuestas[44, 5] = Properties.RecursosPruebas.raven45_6;
            ImgRespuestas[44, 6] = Properties.RecursosPruebas.raven45_7;
            ImgRespuestas[44, 7] = Properties.RecursosPruebas.raven45_8;

            ImgRespuestas[45, 0] = Properties.RecursosPruebas.raven46_1;
            ImgRespuestas[45, 1] = Properties.RecursosPruebas.raven46_2;
            ImgRespuestas[45, 2] = Properties.RecursosPruebas.raven46_3;
            ImgRespuestas[45, 3] = Properties.RecursosPruebas.raven46_4;
            ImgRespuestas[45, 4] = Properties.RecursosPruebas.raven46_5;
            ImgRespuestas[45, 5] = Properties.RecursosPruebas.raven46_6;
            ImgRespuestas[45, 6] = Properties.RecursosPruebas.raven46_7;
            ImgRespuestas[45, 7] = Properties.RecursosPruebas.raven46_8;

            ImgRespuestas[46, 0] = Properties.RecursosPruebas.raven47_1;
            ImgRespuestas[46, 1] = Properties.RecursosPruebas.raven47_2;
            ImgRespuestas[46, 2] = Properties.RecursosPruebas.raven47_3;
            ImgRespuestas[46, 3] = Properties.RecursosPruebas.raven47_4;
            ImgRespuestas[46, 4] = Properties.RecursosPruebas.raven47_5;
            ImgRespuestas[46, 5] = Properties.RecursosPruebas.raven47_6;
            ImgRespuestas[46, 6] = Properties.RecursosPruebas.raven47_7;
            ImgRespuestas[46, 7] = Properties.RecursosPruebas.raven47_8;

            ImgRespuestas[47, 0] = Properties.RecursosPruebas.raven48_1;
            ImgRespuestas[47, 1] = Properties.RecursosPruebas.raven48_2;
            ImgRespuestas[47, 2] = Properties.RecursosPruebas.raven48_3;
            ImgRespuestas[47, 3] = Properties.RecursosPruebas.raven48_4;
            ImgRespuestas[47, 4] = Properties.RecursosPruebas.raven48_5;
            ImgRespuestas[47, 5] = Properties.RecursosPruebas.raven48_6;
            ImgRespuestas[47, 6] = Properties.RecursosPruebas.raven48_7;
            ImgRespuestas[47, 7] = Properties.RecursosPruebas.raven48_8;

            ImgRespuestas[48, 0] = Properties.RecursosPruebas.raven49_1;
            ImgRespuestas[48, 1] = Properties.RecursosPruebas.raven49_2;
            ImgRespuestas[48, 2] = Properties.RecursosPruebas.raven49_3;
            ImgRespuestas[48, 3] = Properties.RecursosPruebas.raven49_4;
            ImgRespuestas[48, 4] = Properties.RecursosPruebas.raven49_5;
            ImgRespuestas[48, 5] = Properties.RecursosPruebas.raven49_6;
            ImgRespuestas[48, 6] = Properties.RecursosPruebas.raven49_7;
            ImgRespuestas[48, 7] = Properties.RecursosPruebas.raven49_8;

            ImgRespuestas[49, 0] = Properties.RecursosPruebas.raven50_1;
            ImgRespuestas[49, 1] = Properties.RecursosPruebas.raven50_2;
            ImgRespuestas[49, 2] = Properties.RecursosPruebas.raven50_3;
            ImgRespuestas[49, 3] = Properties.RecursosPruebas.raven50_4;
            ImgRespuestas[49, 4] = Properties.RecursosPruebas.raven50_5;
            ImgRespuestas[49, 5] = Properties.RecursosPruebas.raven50_6;
            ImgRespuestas[49, 6] = Properties.RecursosPruebas.raven50_7;
            ImgRespuestas[49, 7] = Properties.RecursosPruebas.raven50_8;

            ImgRespuestas[50, 0] = Properties.RecursosPruebas.raven51_1;
            ImgRespuestas[50, 1] = Properties.RecursosPruebas.raven51_2;
            ImgRespuestas[50, 2] = Properties.RecursosPruebas.raven51_3;
            ImgRespuestas[50, 3] = Properties.RecursosPruebas.raven51_4;
            ImgRespuestas[50, 4] = Properties.RecursosPruebas.raven51_5;
            ImgRespuestas[50, 5] = Properties.RecursosPruebas.raven51_6;
            ImgRespuestas[50, 6] = Properties.RecursosPruebas.raven51_7;
            ImgRespuestas[50, 7] = Properties.RecursosPruebas.raven51_8;

            ImgRespuestas[51, 0] = Properties.RecursosPruebas.raven52_1;
            ImgRespuestas[51, 1] = Properties.RecursosPruebas.raven52_2;
            ImgRespuestas[51, 2] = Properties.RecursosPruebas.raven52_3;
            ImgRespuestas[51, 3] = Properties.RecursosPruebas.raven52_4;
            ImgRespuestas[51, 4] = Properties.RecursosPruebas.raven52_5;
            ImgRespuestas[51, 5] = Properties.RecursosPruebas.raven52_6;
            ImgRespuestas[51, 6] = Properties.RecursosPruebas.raven52_7;
            ImgRespuestas[51, 7] = Properties.RecursosPruebas.raven52_8;

            ImgRespuestas[52, 0] = Properties.RecursosPruebas.raven53_1;
            ImgRespuestas[52, 1] = Properties.RecursosPruebas.raven53_2;
            ImgRespuestas[52, 2] = Properties.RecursosPruebas.raven53_3;
            ImgRespuestas[52, 3] = Properties.RecursosPruebas.raven53_4;
            ImgRespuestas[52, 4] = Properties.RecursosPruebas.raven53_5;
            ImgRespuestas[52, 5] = Properties.RecursosPruebas.raven53_6;
            ImgRespuestas[52, 6] = Properties.RecursosPruebas.raven53_7;
            ImgRespuestas[52, 7] = Properties.RecursosPruebas.raven53_8;

            ImgRespuestas[53, 0] = Properties.RecursosPruebas.raven54_1;
            ImgRespuestas[53, 1] = Properties.RecursosPruebas.raven54_2;
            ImgRespuestas[53, 2] = Properties.RecursosPruebas.raven54_3;
            ImgRespuestas[53, 3] = Properties.RecursosPruebas.raven54_4;
            ImgRespuestas[53, 4] = Properties.RecursosPruebas.raven54_5;
            ImgRespuestas[53, 5] = Properties.RecursosPruebas.raven54_6;
            ImgRespuestas[53, 6] = Properties.RecursosPruebas.raven54_7;
            ImgRespuestas[53, 7] = Properties.RecursosPruebas.raven54_8;

            ImgRespuestas[54, 0] = Properties.RecursosPruebas.raven55_1;
            ImgRespuestas[54, 1] = Properties.RecursosPruebas.raven55_2;
            ImgRespuestas[54, 2] = Properties.RecursosPruebas.raven55_3;
            ImgRespuestas[54, 3] = Properties.RecursosPruebas.raven55_4;
            ImgRespuestas[54, 4] = Properties.RecursosPruebas.raven55_5;
            ImgRespuestas[54, 5] = Properties.RecursosPruebas.raven55_6;
            ImgRespuestas[54, 6] = Properties.RecursosPruebas.raven55_7;
            ImgRespuestas[54, 7] = Properties.RecursosPruebas.raven55_8;

            ImgRespuestas[55, 0] = Properties.RecursosPruebas.raven56_1;
            ImgRespuestas[55, 1] = Properties.RecursosPruebas.raven56_2;
            ImgRespuestas[55, 2] = Properties.RecursosPruebas.raven56_3;
            ImgRespuestas[55, 3] = Properties.RecursosPruebas.raven56_4;
            ImgRespuestas[55, 4] = Properties.RecursosPruebas.raven56_5;
            ImgRespuestas[55, 5] = Properties.RecursosPruebas.raven56_6;
            ImgRespuestas[55, 6] = Properties.RecursosPruebas.raven56_7;
            ImgRespuestas[55, 7] = Properties.RecursosPruebas.raven56_8;

            ImgRespuestas[56, 0] = Properties.RecursosPruebas.raven57_1;
            ImgRespuestas[56, 1] = Properties.RecursosPruebas.raven57_2;
            ImgRespuestas[56, 2] = Properties.RecursosPruebas.raven57_3;
            ImgRespuestas[56, 3] = Properties.RecursosPruebas.raven57_4;
            ImgRespuestas[56, 4] = Properties.RecursosPruebas.raven57_5;
            ImgRespuestas[56, 5] = Properties.RecursosPruebas.raven57_6;
            ImgRespuestas[56, 6] = Properties.RecursosPruebas.raven57_7;
            ImgRespuestas[56, 7] = Properties.RecursosPruebas.raven57_8;

            ImgRespuestas[57, 0] = Properties.RecursosPruebas.raven58_1;
            ImgRespuestas[57, 1] = Properties.RecursosPruebas.raven58_2;
            ImgRespuestas[57, 2] = Properties.RecursosPruebas.raven58_3;
            ImgRespuestas[57, 3] = Properties.RecursosPruebas.raven58_4;
            ImgRespuestas[57, 4] = Properties.RecursosPruebas.raven58_5;
            ImgRespuestas[57, 5] = Properties.RecursosPruebas.raven58_6;
            ImgRespuestas[57, 6] = Properties.RecursosPruebas.raven58_7;
            ImgRespuestas[57, 7] = Properties.RecursosPruebas.raven58_8;

            ImgRespuestas[58, 0] = Properties.RecursosPruebas.raven59_1;
            ImgRespuestas[58, 1] = Properties.RecursosPruebas.raven59_2;
            ImgRespuestas[58, 2] = Properties.RecursosPruebas.raven59_3;
            ImgRespuestas[58, 3] = Properties.RecursosPruebas.raven59_4;
            ImgRespuestas[58, 4] = Properties.RecursosPruebas.raven59_5;
            ImgRespuestas[58, 5] = Properties.RecursosPruebas.raven59_6;
            ImgRespuestas[58, 6] = Properties.RecursosPruebas.raven59_7;
            ImgRespuestas[58, 7] = Properties.RecursosPruebas.raven59_8;

            ImgRespuestas[59, 0] = Properties.RecursosPruebas.raven60_1;
            ImgRespuestas[59, 1] = Properties.RecursosPruebas.raven60_2;
            ImgRespuestas[59, 2] = Properties.RecursosPruebas.raven60_3;
            ImgRespuestas[59, 3] = Properties.RecursosPruebas.raven60_4;
            ImgRespuestas[59, 4] = Properties.RecursosPruebas.raven60_5;
            ImgRespuestas[59, 5] = Properties.RecursosPruebas.raven60_6;
            ImgRespuestas[59, 6] = Properties.RecursosPruebas.raven60_7;
            ImgRespuestas[59, 7] = Properties.RecursosPruebas.raven60_8;

        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Name == "Siguiente")
            {

                x++;
                GuardarRespuestas();

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    return;
                }

             
                MostrarImagenes(x);

                LimpiarControles();
                // btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Name == "Actualizar")
            {

                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {
                    btnSiguiente.Name = "Siguiente";
                    LimpiarControles();
                    btnSiguiente.Enabled = false;
                }

                MostrarImagenes(x);

            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    GuardarRespuestas();
                    respuestas.Numero_Prueba = NumeroPrueba;
                    Cliente.FinalizaPruebaRaven(respuestas);
                    tiempo.Enabled = false;
                    this.Close();

                }

            }


            if (x == 59)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de 60";

            btnAnterior.Enabled = true;

        }

        private void FinalizarPrueba()
        {
            //Validamos si el candidato ha contestado todo
            if (txtSeleccion.Text.Trim().Length > 0 && btnSiguiente.Name == "Siguiente")
            {
                GuardarRespuestas();
            }

            respuestas.Numero_Prueba = NumeroPrueba;
            Cliente.FinalizaPruebaRaven(respuestas);



        }

        private void GuardarRespuestas()
        {


            respuestas.Id_pregunta = ListaPreguntas[x - 1].id_pregunta;
            respuestas.Respuesta = Convert.ToInt32(txtSeleccion.Text);
            respuestas.Numero_Prueba = NumeroPrueba;

            //Evaluamos la respuesta
            if (Convert.ToInt32(ListaPreguntas[x - 1].Respuesta) == Convert.ToInt32(txtSeleccion.Text))
            {
                respuestas.Punto = 1;
            }
            else
            {
                respuestas.Punto = 0;
            }


            Cliente.InsertaRespuestaRaven(respuestas);


        }

        private void ActualizarRespuestas()
        {

            respuestas.Id_respuesta_candidato = RespuestasCandidato[x - 1].Id_respuesta_candidato;
            respuestas.Respuesta = Convert.ToInt32(txtSeleccion.Text);
            //respuestas.Numero_Prueba = NumeroPrueba;

            //Evaluamos la respuesta
            if (Convert.ToInt32(ListaPreguntas[x - 1].Respuesta) == Convert.ToInt32(txtSeleccion.Text))
            {
                respuestas.Punto = 1;
            }
            else
            {
                respuestas.Punto = 0;
            }

            Cliente.ActualizaRespuestaRaven(respuestas);

        }

        private void LimpiarControles()
        {
            txtSeleccion.Text = "";
            picSeleccion.Image = Properties.RecursosPruebas.raven_pregunta;

        }

        #region Seleccion de Respuesta



        private void picRes1_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes1.Image;
            txtSeleccion.Text = "1";
        }

        private void picRes2_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes2.Image;
            txtSeleccion.Text = "2";
        }

        private void picRes3_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes3.Image;
            txtSeleccion.Text = "3";
        }

        private void picResSup4_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picResSup4.Image;
            txtSeleccion.Text = "4";
        }

        private void picRes4_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes4.Image;

            if (Etapa == 1)
            {
                txtSeleccion.Text = "4";
            }
            else
            {
                txtSeleccion.Text = "5";
            }

        }

        private void picRes5_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes5.Image;

            if (Etapa == 1)
            {
                txtSeleccion.Text = "5";
            }
            else
            {
                txtSeleccion.Text = "6";
            }
        }

        private void picRes6_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picRes6.Image;

            if (Etapa == 1)
            {
                txtSeleccion.Text = "6";
            }
            else
            {
                txtSeleccion.Text = "7";
            }
        }

        private void picResInf4_Click(object sender, EventArgs e)
        {
            picSeleccion.Image = picResInf4.Image;
            txtSeleccion.Text = "8";
        }


        #endregion

        #region Validar Solo Numeros

        private void txtSeleccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }

            if (Etapa == 1)
            {
                // bloquear las teclas 0 y del 7 al 9
                if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
                {
                    ex.Handled = true;
                    return;
                }
            }
            else if (Etapa == 2)
            {
                // bloquear las teclas 0 y 9
                if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D9))
                {
                    ex.Handled = true;
                    return;
                }
            }


        }


        #endregion

        private void txtSeleccion_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtSeleccion.Text.Length > 0)
            {
                SeleccionarImagen();
                btnSiguiente.Enabled = true;

            }
            else
            {
                picSeleccion.Image = Properties.RecursosPruebas.raven_pregunta;
                btnSiguiente.Enabled = false;
            }
        }

        private void SeleccionarImagen()
        {

            if (Etapa == 1)
            {
                switch (Convert.ToInt32(txtSeleccion.Text))
                {
                    case 1:
                        picSeleccion.Image = picRes1.Image;
                        break;

                    case 2:
                        picSeleccion.Image = picRes2.Image;
                        break;

                    case 3:
                        picSeleccion.Image = picRes3.Image;
                        break;

                    case 4:
                        picSeleccion.Image = picRes4.Image;
                        break;

                    case 5:
                        picSeleccion.Image = picRes5.Image;
                        break;

                    case 6:
                        picSeleccion.Image = picRes6.Image;
                        break;
                }

            }
            else
            {

                switch (Convert.ToInt32(txtSeleccion.Text))
                {
                    case 1:
                        picSeleccion.Image = picRes1.Image;
                        break;

                    case 2:
                        picSeleccion.Image = picRes2.Image;
                        break;

                    case 3:
                        picSeleccion.Image = picRes3.Image;
                        break;

                    case 4:
                        picSeleccion.Image = picResSup4.Image;
                        break;

                    case 5:
                        picSeleccion.Image = picRes4.Image;
                        break;

                    case 6:
                        picSeleccion.Image = picRes5.Image;
                        break;

                    case 7:
                        picSeleccion.Image = picRes6.Image;
                        break;

                    case 8:
                        picSeleccion.Image = picResInf4.Image;
                        break;
                }


            }


        }

        private void txtSeleccion_TextChanged(object sender, EventArgs e)
        {
            if (txtSeleccion.Text.Length > 0)
            {
                btnSiguiente.Enabled = true;

            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            RespuestasCandidato = Cliente.ListaPreguntasContestadasRaven(NumeroPrueba);
            Recorrido = RespuestasCandidato.ToArray().Count();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de 60";


            x--;
            MostrarImagenes(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }

            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Name = "Siguiente";
                this.btnSiguiente.Text = "Siguiente";
            }

            btnSiguiente.Name = "Actualizar";
            btnSiguiente.Enabled = true;


        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda1.Hide();
            this.panel1.Enabled = true;
        }

        RAVEN model_raven;
        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                model_raven = Cliente.NuevoRaven(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            NumeroPrueba = model_raven.IdPrueba;

            CargarImagenes();

            //Mostramos preguntas iniciales
            MostrarImagenes(0);

            this.lblNumPreg.Text = "Pregunta: 1 de 60";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.txtSeleccion.Focus();
            this.tiempo.Enabled = true;

        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.panel1.Enabled = true;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Show();
        }

        private void txtSeleccion_TextChanged_1(object sender, EventArgs e)
        {
            if (txtSeleccion.Text.Length > 0)
            {
                btnSiguiente.Enabled = true;

            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void txtSeleccion_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtSeleccion_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (txtSeleccion.Text.Length > 0)
            {
                SeleccionarImagen();
                btnSiguiente.Enabled = true;

            }
            else
            {
                picSeleccion.Image = Properties.RecursosPruebas.raven_pregunta;
                btnSiguiente.Enabled = false;
            }
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela el proceso de la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Raven", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.RAVEN, NumeroPrueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmRaven_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
