﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Raven
{
    public partial class frmReporteRaven : Form
    {
        QuickHumClient Cliente = Globales.cliente;
        dto_raven_discrepancia[] ListaDiscrepancia;
        dto_raven_percentiles[] ListaPercentiles;
        dto_raven_resultados[] ListaResultados;
        DataTable dtResultados = new DataTable();
        DataTable dtPercentiles = new DataTable();
        DataTable dtCandidato = new DataTable();
        private Expediente _expediente = null;

        int EdadCandidato = 0;

        public frmReporteRaven(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void CargarReporte()
        {
            dtPercentiles.Clear();
            dtResultados.Clear();

            ReportParameter Parametros = new ReportParameter();

            string ParametroError = "";

            int SumaTotal = 0;
            int Discrepancia_A = 0;
            int Discrepancia_B = 0;
            int Discrepancia_C = 0;
            int Discrepancia_D = 0;
            int Discrepancia_E = 0;


            //Temporalmente se maneja la edad introduciendola desde un Texboxt Luego sera obtenida del expediente
            EdadCandidato = _expediente.Candidato.Edad;

            ListaDiscrepancia = Cliente.ListaRavenDiscrepancia();
            ListaPercentiles = Cliente.ListaRavenPercentiles();

            ListaResultados = Cliente.ListaRavenResultados(_expediente.Id);

            //Caolculamos la puntuacion total obtenida por el candidato
            for (int i = 0; i < ListaResultados.ToArray().Count(); i++)
            {

                SumaTotal = SumaTotal + ListaResultados[i].Suma_Serie;

            }


            //Valoramos si el candidato logro el puntaje minimo establecido por las escalas
            if (SumaTotal >= 10)
            {

                //Obtenemos la discrepancia segun el puntaje obtenido por el candidato
                for (int i = 0; i < ListaDiscrepancia.ToArray().Count(); i++)
                {
                    if (ListaDiscrepancia[i].Puntos == SumaTotal)
                    {
                        Discrepancia_A = Convert.ToInt32(ListaDiscrepancia[i].Serie_A);
                        Discrepancia_B = Convert.ToInt32(ListaDiscrepancia[i].Serie_B);
                        Discrepancia_C = Convert.ToInt32(ListaDiscrepancia[i].Serie_C);
                        Discrepancia_D = Convert.ToInt32(ListaDiscrepancia[i].Serie_D);
                        Discrepancia_E = Convert.ToInt32(ListaDiscrepancia[i].Serie_E);

                    }

                }

                //Cargamos los resultados obtenidos
                for (int i = 0; i < ListaResultados.ToArray().Count(); i++)
                {
                    DataRow row = dtResultados.NewRow();

                    if (ListaResultados[i].Serie == "A")
                    {
                        row["Serie"] = "A";
                        row["Suma"] = Convert.ToInt32(ListaResultados[i].Suma_Serie);
                        row["Discrepancia"] = Discrepancia_A;
                        row["Resultado"] = Convert.ToInt32(ListaResultados[i].Suma_Serie) - Discrepancia_A;
                    }
                    else if (ListaResultados[i].Serie == "B")
                    {
                        row["Serie"] = "B";
                        row["Suma"] = Convert.ToInt32(ListaResultados[i].Suma_Serie);
                        row["Discrepancia"] = Discrepancia_B;
                        row["Resultado"] = Convert.ToInt32(ListaResultados[i].Suma_Serie) - Discrepancia_B;

                    }
                    else if (ListaResultados[i].Serie == "C")
                    {
                        row["Serie"] = "C";
                        row["Suma"] = Convert.ToInt32(ListaResultados[i].Suma_Serie);
                        row["Discrepancia"] = Discrepancia_C;
                        row["Resultado"] = Convert.ToInt32(ListaResultados[i].Suma_Serie) - Discrepancia_C;

                    }
                    else if (ListaResultados[i].Serie == "D")
                    {
                        row["Serie"] = "D";
                        row["Suma"] = Convert.ToInt32(ListaResultados[i].Suma_Serie);
                        row["Discrepancia"] = Discrepancia_D;
                        row["Resultado"] = Convert.ToInt32(ListaResultados[i].Suma_Serie) - Discrepancia_D;

                    }
                    else if (ListaResultados[i].Serie == "E")
                    {
                        row["Serie"] = "E";
                        row["Suma"] = Convert.ToInt32(ListaResultados[i].Suma_Serie);
                        row["Discrepancia"] = Discrepancia_E;
                        row["Resultado"] = Convert.ToInt32(ListaResultados[i].Suma_Serie) - Discrepancia_E;

                    }


                    dtResultados.Rows.Add(row);
                }


                //Calculamos el Percentil y el rango en que se encuentran los resultados del Candidato
                for (int i = 0; i < ListaPercentiles.ToArray().Count(); i++)
                {

                    if ((SumaTotal >= ListaPercentiles[i].Puntaje_Minimo && SumaTotal <= ListaPercentiles[i].Puntaje_Maximo) && (EdadCandidato >= ListaPercentiles[i].Edad_Minima && EdadCandidato <= ListaPercentiles[i].Edad_Maxima))
                    {
                        DataRow row = dtPercentiles.NewRow();

                        row["PuntajeTotal"] = SumaTotal;
                        row["Percentiles"] = ListaPercentiles[i].Percentil;
                        row["Rangos"] = ListaPercentiles[i].Rango;
                        row["Diagnostico"] = ListaPercentiles[i].Diagnostico;

                        dtPercentiles.Rows.Add(row);

                    }

                }
               

            }
            else
            {

                ParametroError = "La prueba no presenta puntajes debido a que el candidato obtuvo  solamente" + SumaTotal + " , siendo 10 la escala minima de evaluacion";

            }

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            Parametros = new ReportParameter("ParametroError", ParametroError);

            ReportDataSource rds = new ReportDataSource("dsDiscrepancia", dtResultados);
            ReportDataSource rds2 = new ReportDataSource("dsPercentiles", dtPercentiles);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            rvReporteRaven.LocalReport.DataSources.Clear();
            rvReporteRaven.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Raven.rptReporteRaven.rdlc";
            rvReporteRaven.LocalReport.DataSources.Add(rds);
            rvReporteRaven.LocalReport.DataSources.Add(rds2);
            rvReporteRaven.LocalReport.DataSources.Add(rds_candidato);

            rvReporteRaven.LocalReport.SetParameters(Parametros);

            rvReporteRaven.RefreshReport();

        }


        private void frmReporteRaven_Load(object sender, EventArgs e)
        {

            dtResultados.Columns.Add("Serie");
            dtResultados.Columns.Add("Suma");
            dtResultados.Columns.Add("Discrepancia");
            dtResultados.Columns.Add("Resultado");

            dtPercentiles.Columns.Add("PuntajeTotal");
            dtPercentiles.Columns.Add("Percentiles");
            dtPercentiles.Columns.Add("Rangos");
            dtPercentiles.Columns.Add("Diagnostico");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            CargarReporte();


        }




    }
}
