﻿namespace QuickHumCliente.Vistas.Pruebas.Dominos
{
    partial class frmDominos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDominos));
            this.label3 = new System.Windows.Forms.Label();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCancelarAyuda = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.gbOpciones = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.picOpcion6 = new System.Windows.Forms.PictureBox();
            this.picOpcion5 = new System.Windows.Forms.PictureBox();
            this.picOpcion0 = new System.Windows.Forms.PictureBox();
            this.picOpcion4 = new System.Windows.Forms.PictureBox();
            this.picOpcion1 = new System.Windows.Forms.PictureBox();
            this.picOpcion3 = new System.Windows.Forms.PictureBox();
            this.picOpcion2 = new System.Windows.Forms.PictureBox();
            this.gbRespuesta = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.picResInferior = new System.Windows.Forms.PictureBox();
            this.picResSuperior = new System.Windows.Forms.PictureBox();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.picPregunta = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.panelAyuda.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel9.SuspendLayout();
            this.gbOpciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion2)).BeginInit();
            this.gbRespuesta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picResInferior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResSuperior)).BeginInit();
            this.gbPreguntas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPregunta)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(667, 36);
            this.label3.TabIndex = 65;
            this.label3.Text = "PRUEBA DOMINOS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.Color.White;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreguntas.Location = new System.Drawing.Point(12, 11);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.Size = new System.Drawing.Size(89, 26);
            this.txtPreguntas.TabIndex = 98;
            this.txtPreguntas.Text = "";
            // 
            // panelAyuda
            // 
            this.panelAyuda.Controls.Add(this.panel3);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.MinimumSize = new System.Drawing.Size(674, 632);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(674, 632);
            this.panelAyuda.TabIndex = 99;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.btnCancelarAyuda);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.btnIniciarAyuda);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(53, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(570, 501);
            this.panel3.TabIndex = 76;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.btnCerrarAyuda);
            this.panel5.Location = new System.Drawing.Point(534, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(36, 36);
            this.panel5.TabIndex = 74;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.Transparent;
            this.btnCerrarAyuda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCerrarAyuda.BackgroundImage")));
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(-1, -1);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 77;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(21, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(530, 245);
            this.panel1.TabIndex = 75;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(311, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 21);
            this.label8.TabIndex = 1;
            this.label8.Text = "B)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "A)";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::QuickHumCliente.Properties.RecursosPruebas.ejemplo2;
            this.pictureBox2.Location = new System.Drawing.Point(274, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(250, 200);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::QuickHumCliente.Properties.RecursosPruebas.ejemplo1;
            this.pictureBox1.Location = new System.Drawing.Point(7, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnCancelarAyuda
            // 
            this.btnCancelarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCancelarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarAyuda.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancelarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarAyuda.Location = new System.Drawing.Point(144, 454);
            this.btnCancelarAyuda.Name = "btnCancelarAyuda";
            this.btnCancelarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnCancelarAyuda.TabIndex = 69;
            this.btnCancelarAyuda.Text = "Cancelar";
            this.btnCancelarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarAyuda.UseVisualStyleBackColor = false;
            this.btnCancelarAyuda.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 74;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.i33x33;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(325, 454);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 421);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(427, 18);
            this.label6.TabIndex = 68;
            this.label6.Text = "Como ha visto, es muy sencillo ahora continúe con los siguientes 48.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(390, 36);
            this.label4.TabIndex = 74;
            this.label4.Text = "EJEMPLO:\r\nExamine este grupo de fichas y piense cual iria a continuaciòn.\r\n";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(16, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(537, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Calibri", 12F);
            this.label1.Location = new System.Drawing.Point(18, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(533, 93);
            this.label1.TabIndex = 73;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Controls.Add(this.gbOpciones);
            this.panel6.Controls.Add(this.gbRespuesta);
            this.panel6.Controls.Add(this.gbPreguntas);
            this.panel6.Controls.Add(this.btnCancelar);
            this.panel6.Controls.Add(this.btnSiguiente);
            this.panel6.Controls.Add(this.btnAnterior);
            this.panel6.Controls.Add(this.btnAyuda);
            this.panel6.Location = new System.Drawing.Point(-10, 81);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(655, 563);
            this.panel6.TabIndex = 106;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel9.Controls.Add(this.lblNumPreg);
            this.panel9.Location = new System.Drawing.Point(0, 477);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(655, 25);
            this.panel9.TabIndex = 102;
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNumPreg.Location = new System.Drawing.Point(260, 4);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(115, 17);
            this.lblNumPreg.TabIndex = 69;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // gbOpciones
            // 
            this.gbOpciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gbOpciones.Controls.Add(this.label10);
            this.gbOpciones.Controls.Add(this.picOpcion6);
            this.gbOpciones.Controls.Add(this.picOpcion5);
            this.gbOpciones.Controls.Add(this.picOpcion0);
            this.gbOpciones.Controls.Add(this.picOpcion4);
            this.gbOpciones.Controls.Add(this.picOpcion1);
            this.gbOpciones.Controls.Add(this.picOpcion3);
            this.gbOpciones.Controls.Add(this.picOpcion2);
            this.gbOpciones.Location = new System.Drawing.Point(11, 344);
            this.gbOpciones.Name = "gbOpciones";
            this.gbOpciones.Size = new System.Drawing.Size(632, 127);
            this.gbOpciones.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(630, 27);
            this.label10.TabIndex = 67;
            this.label10.Text = "OPCIONES";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picOpcion6
            // 
            this.picOpcion6.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion6.Image = global::QuickHumCliente.Properties.RecursosPruebas.d6;
            this.picOpcion6.Location = new System.Drawing.Point(539, 35);
            this.picOpcion6.Name = "picOpcion6";
            this.picOpcion6.Size = new System.Drawing.Size(82, 82);
            this.picOpcion6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion6.TabIndex = 0;
            this.picOpcion6.TabStop = false;
            this.picOpcion6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion6_MouseDown);
            // 
            // picOpcion5
            // 
            this.picOpcion5.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion5.Image = global::QuickHumCliente.Properties.RecursosPruebas.d5;
            this.picOpcion5.Location = new System.Drawing.Point(450, 35);
            this.picOpcion5.Name = "picOpcion5";
            this.picOpcion5.Size = new System.Drawing.Size(82, 82);
            this.picOpcion5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion5.TabIndex = 0;
            this.picOpcion5.TabStop = false;
            this.picOpcion5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion5_MouseDown);
            // 
            // picOpcion0
            // 
            this.picOpcion0.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion0.Image = global::QuickHumCliente.Properties.RecursosPruebas.d0;
            this.picOpcion0.Location = new System.Drawing.Point(9, 35);
            this.picOpcion0.Name = "picOpcion0";
            this.picOpcion0.Size = new System.Drawing.Size(82, 82);
            this.picOpcion0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion0.TabIndex = 0;
            this.picOpcion0.TabStop = false;
            this.picOpcion0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion0_MouseDown);
            // 
            // picOpcion4
            // 
            this.picOpcion4.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion4.Image = global::QuickHumCliente.Properties.RecursosPruebas.d4;
            this.picOpcion4.Location = new System.Drawing.Point(362, 35);
            this.picOpcion4.Name = "picOpcion4";
            this.picOpcion4.Size = new System.Drawing.Size(82, 82);
            this.picOpcion4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion4.TabIndex = 0;
            this.picOpcion4.TabStop = false;
            this.picOpcion4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion4_MouseDown);
            // 
            // picOpcion1
            // 
            this.picOpcion1.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion1.Image = global::QuickHumCliente.Properties.RecursosPruebas.d1;
            this.picOpcion1.Location = new System.Drawing.Point(97, 35);
            this.picOpcion1.Name = "picOpcion1";
            this.picOpcion1.Size = new System.Drawing.Size(82, 82);
            this.picOpcion1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion1.TabIndex = 0;
            this.picOpcion1.TabStop = false;
            this.picOpcion1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion1_MouseDown);
            // 
            // picOpcion3
            // 
            this.picOpcion3.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion3.Image = global::QuickHumCliente.Properties.RecursosPruebas.d3;
            this.picOpcion3.Location = new System.Drawing.Point(274, 35);
            this.picOpcion3.Name = "picOpcion3";
            this.picOpcion3.Size = new System.Drawing.Size(82, 82);
            this.picOpcion3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion3.TabIndex = 0;
            this.picOpcion3.TabStop = false;
            this.picOpcion3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion3_MouseDown);
            // 
            // picOpcion2
            // 
            this.picOpcion2.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picOpcion2.Image = global::QuickHumCliente.Properties.RecursosPruebas.d2;
            this.picOpcion2.Location = new System.Drawing.Point(186, 35);
            this.picOpcion2.Name = "picOpcion2";
            this.picOpcion2.Size = new System.Drawing.Size(82, 82);
            this.picOpcion2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picOpcion2.TabIndex = 0;
            this.picOpcion2.TabStop = false;
            this.picOpcion2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpcion2_MouseDown);
            // 
            // gbRespuesta
            // 
            this.gbRespuesta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gbRespuesta.Controls.Add(this.label9);
            this.gbRespuesta.Controls.Add(this.picResInferior);
            this.gbRespuesta.Controls.Add(this.picResSuperior);
            this.gbRespuesta.Location = new System.Drawing.Point(520, 14);
            this.gbRespuesta.Name = "gbRespuesta";
            this.gbRespuesta.Size = new System.Drawing.Size(123, 313);
            this.gbRespuesta.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 27);
            this.label9.TabIndex = 66;
            this.label9.Text = "RESPUESTA";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picResInferior
            // 
            this.picResInferior.Image = global::QuickHumCliente.Properties.RecursosPruebas.Dpregunta;
            this.picResInferior.Location = new System.Drawing.Point(19, 161);
            this.picResInferior.Name = "picResInferior";
            this.picResInferior.Size = new System.Drawing.Size(82, 82);
            this.picResInferior.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picResInferior.TabIndex = 0;
            this.picResInferior.TabStop = false;
            this.picResInferior.DragDrop += new System.Windows.Forms.DragEventHandler(this.picResInferior_DragDrop);
            this.picResInferior.DragEnter += new System.Windows.Forms.DragEventHandler(this.picResInferior_DragEnter);
            // 
            // picResSuperior
            // 
            this.picResSuperior.Image = global::QuickHumCliente.Properties.RecursosPruebas.Dpregunta;
            this.picResSuperior.Location = new System.Drawing.Point(19, 74);
            this.picResSuperior.Name = "picResSuperior";
            this.picResSuperior.Size = new System.Drawing.Size(82, 82);
            this.picResSuperior.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picResSuperior.TabIndex = 0;
            this.picResSuperior.TabStop = false;
            this.picResSuperior.DragDrop += new System.Windows.Forms.DragEventHandler(this.picResSuperior_DragDrop);
            this.picResSuperior.DragEnter += new System.Windows.Forms.DragEventHandler(this.picResSuperior_DragEnter);
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gbPreguntas.Controls.Add(this.txtPreguntas);
            this.gbPreguntas.Controls.Add(this.picPregunta);
            this.gbPreguntas.Location = new System.Drawing.Point(11, 14);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(500, 313);
            this.gbPreguntas.TabIndex = 0;
            // 
            // picPregunta
            // 
            this.picPregunta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picPregunta.Location = new System.Drawing.Point(107, 11);
            this.picPregunta.Name = "picPregunta";
            this.picPregunta.Size = new System.Drawing.Size(382, 292);
            this.picPregunta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPregunta.TabIndex = 66;
            this.picPregunta.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton1;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(525, 517);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 95;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(343, 517);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 94;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(206, 517);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 97;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(16, 517);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 96;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.BackgroundImage")));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Location = new System.Drawing.Point(597, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 105;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.lbltime);
            this.panel2.Location = new System.Drawing.Point(391, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(241, 37);
            this.panel2.TabIndex = 100;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(43, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltime.Location = new System.Drawing.Point(162, 8);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // frmDominos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(684, 634);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 650);
            this.Name = "frmDominos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prueba Dominos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDominos_FormClosing);
            this.Load += new System.EventHandler(this.frmDominos_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.gbOpciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpcion2)).EndInit();
            this.gbRespuesta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picResInferior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResSuperior)).EndInit();
            this.gbPreguntas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPregunta)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picPregunta;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancelarAyuda;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.PictureBox picResInferior;
        private System.Windows.Forms.PictureBox picResSuperior;
        private System.Windows.Forms.PictureBox picOpcion5;
        private System.Windows.Forms.PictureBox picOpcion4;
        private System.Windows.Forms.PictureBox picOpcion3;
        private System.Windows.Forms.PictureBox picOpcion2;
        private System.Windows.Forms.PictureBox picOpcion1;
        private System.Windows.Forms.PictureBox picOpcion0;
        private System.Windows.Forms.PictureBox picOpcion6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel gbOpciones;
        private System.Windows.Forms.Panel gbRespuesta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblNumPreg;
    }
}