﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuickHumCliente.ServicioQuickHum;

using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;


namespace QuickHumCliente.Vistas.Pruebas.Dominos
{
    public partial class frmReporteDominos : Form
    {
        readonly QuickHumClient cliente = Globales.cliente;

        readonly DataTable dtResultados = new DataTable();
        DataTable dtCandidato = new DataTable();
        private readonly Expediente _expediente = null;
        public frmReporteDominos(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void frmReporteDominos_Load(object sender, EventArgs e)
        {
            dtResultados.Columns.Add("Factor");
            dtResultados.Columns.Add("Percentil");
            dtResultados.Columns.Add("SumaPuntos");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            CargarReporte();
        }

        private void CargarReporte()
        {
            dto_resultados_dominos Resultados = cliente.ListaDominosResultados(_expediente.Id);

            DataRow row = dtResultados.NewRow();
            row["Factor"] = Resultados.Factor.ToString();
            row["Percentil"] = Convert.ToInt32(Resultados.Percentil);
            row["SumaPuntos"] = Convert.ToInt32(Resultados.SumaPuntos);
            dtResultados.Rows.Add(row);

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);


            ReportDataSource rds = new ReportDataSource("DataSetdominos", dtResultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            reportViewer1.LocalReport.DataSources.Clear();
            //rptPruebaMoss.LocalReport.ReportPath = "rptPruebaMoss.rdlc";
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Dominos.rptPruebaDominos.rdlc";

            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            //reportViewer1 .LocalReport.SetParameters(Parametros);            


            this.reportViewer1.RefreshReport();
        }
    }
}
