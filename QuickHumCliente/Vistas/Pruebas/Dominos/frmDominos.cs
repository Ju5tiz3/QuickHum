﻿using System;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Dominos
{
    public partial class frmDominos : Form
    {
        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;
        dto_dominos_respuestas_candidato respuestas = new dto_dominos_respuestas_candidato();
        dto_dominos_preguntas[] ListaPreguntas;
        dto_dominos_respuestas_candidato[] RespuestasCandidato;


        Bitmap[] PreguntaImagen = new Bitmap[48];


        int x = 0;
        int Recorrido = 0;

        public frmDominos(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        int Numero_Prueba;

        private void frmDominos_Load(object sender, EventArgs e)
        {
            picResSuperior.AllowDrop = true;
            picResInferior.AllowDrop = true;


            btnSiguiente.Name = "Siguiente";
            DateTime hora;

            this.panelAyuda.Location = new Point(1, 1);
            // this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:10:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

            //Cargamos las imagenes para utilizarlas como preguntas
            CargarPreguntas();



        }

        private void MostrarPregunta(int i)
        {
            this.txtPreguntas.Text = " Pregunta ) " + ListaPreguntas[i].numero_pregunta;

            //Se muestra la imagen de preguntas
            picPregunta.Image = PreguntaImagen[i];


            if (Recorrido > i)
            {

                //Si el candidato ha retrocedido se captura su respuesta anterior
                RespuestaSuperior = Convert.ToInt32(RespuestasCandidato[i].Respuesta_Superior);
                RespuestaInferior = Convert.ToInt32(RespuestasCandidato[i].Respuesta_Inferior);

                /*Reconocemos la respuesta superior que fue dada por el candidato
                para poder asignar la imagen que corresponde */
                switch (RespuestasCandidato[x].Respuesta_Superior)
                {
                    case 0:
                        picResSuperior.Image = Properties.RecursosPruebas.d0;
                        break;

                    case 1:
                        picResSuperior.Image = Properties.RecursosPruebas.d1;
                        break;

                    case 2:
                        picResSuperior.Image = Properties.RecursosPruebas.d2;
                        break;


                    case 3:
                        picResSuperior.Image = Properties.RecursosPruebas.d3;
                        break;

                    case 4:
                        picResSuperior.Image = Properties.RecursosPruebas.d4;
                        break;

                    case 5:
                        picResSuperior.Image = Properties.RecursosPruebas.d5;
                        break;

                    case 6:
                        picResSuperior.Image = Properties.RecursosPruebas.d6;
                        break;

                }

                /*Reconocemos la respuesta inferior que fue dada por el candidato
             para poder asignar la imagen que corresponde */
                switch (RespuestasCandidato[x].Respuesta_Inferior)
                {
                    case 0:
                        picResInferior.Image = Properties.RecursosPruebas.d0;
                        break;

                    case 1:
                        picResInferior.Image = Properties.RecursosPruebas.d1;
                        break;

                    case 2:
                        picResInferior.Image = Properties.RecursosPruebas.d2;
                        break;


                    case 3:
                        picResInferior.Image = Properties.RecursosPruebas.d3;
                        break;

                    case 4:
                        picResInferior.Image = Properties.RecursosPruebas.d4;
                        break;

                    case 5:
                        picResInferior.Image = Properties.RecursosPruebas.d5;
                        break;

                    case 6:
                        picResInferior.Image = Properties.RecursosPruebas.d6;
                        break;
                }

            }



        }

        private void CargarPreguntas()
        {
            //Cargamos el listado con el numeor de pregunta y las respuestas respectivas
            ListaPreguntas = Cliente.ListaDominosPreguntas();

            //Definimos las imagenes que seran consideradas pregunts
            PreguntaImagen[0] = Properties.RecursosPruebas.pregunta1;
            PreguntaImagen[1] = Properties.RecursosPruebas.pregunta2;
            PreguntaImagen[2] = Properties.RecursosPruebas.pregunta3;
            PreguntaImagen[3] = Properties.RecursosPruebas.pregunta4;
            PreguntaImagen[4] = Properties.RecursosPruebas.pregunta5;
            PreguntaImagen[5] = Properties.RecursosPruebas.pregunta6;
            PreguntaImagen[6] = Properties.RecursosPruebas.pregunta7;
            PreguntaImagen[7] = Properties.RecursosPruebas.pregunta8;
            PreguntaImagen[8] = Properties.RecursosPruebas.pregunta9;
            PreguntaImagen[9] = Properties.RecursosPruebas.pregunta10;
            PreguntaImagen[10] = Properties.RecursosPruebas.pregunta11;
            PreguntaImagen[11] = Properties.RecursosPruebas.pregunta12;
            PreguntaImagen[12] = Properties.RecursosPruebas.pregunta13;
            PreguntaImagen[13] = Properties.RecursosPruebas.pregunta14;
            PreguntaImagen[14] = Properties.RecursosPruebas.pregunta15;
            PreguntaImagen[15] = Properties.RecursosPruebas.pregunta16;
            PreguntaImagen[16] = Properties.RecursosPruebas.pregunta17;
            PreguntaImagen[17] = Properties.RecursosPruebas.pregunta18;
            PreguntaImagen[18] = Properties.RecursosPruebas.pregunta19;
            PreguntaImagen[19] = Properties.RecursosPruebas.pregunta20;
            PreguntaImagen[20] = Properties.RecursosPruebas.pregunta21;
            PreguntaImagen[21] = Properties.RecursosPruebas.pregunta22;
            PreguntaImagen[22] = Properties.RecursosPruebas.pregunta23;
            PreguntaImagen[23] = Properties.RecursosPruebas.pregunta24;
            PreguntaImagen[24] = Properties.RecursosPruebas.pregunta25;
            PreguntaImagen[25] = Properties.RecursosPruebas.pregunta26;
            PreguntaImagen[26] = Properties.RecursosPruebas.pregunta27;
            PreguntaImagen[27] = Properties.RecursosPruebas.pregunta28;
            PreguntaImagen[28] = Properties.RecursosPruebas.pregunta29;
            PreguntaImagen[29] = Properties.RecursosPruebas.pregunta30;
            PreguntaImagen[30] = Properties.RecursosPruebas.pregunta31;
            PreguntaImagen[31] = Properties.RecursosPruebas.pregunta32;
            PreguntaImagen[32] = Properties.RecursosPruebas.pregunta33;
            PreguntaImagen[33] = Properties.RecursosPruebas.pregunta34;
            PreguntaImagen[34] = Properties.RecursosPruebas.pregunta35;
            PreguntaImagen[35] = Properties.RecursosPruebas.pregunta36;
            PreguntaImagen[36] = Properties.RecursosPruebas.pregunta37;
            PreguntaImagen[37] = Properties.RecursosPruebas.pregunta38;
            PreguntaImagen[38] = Properties.RecursosPruebas.pregunta39;
            PreguntaImagen[39] = Properties.RecursosPruebas.pregunta40;
            PreguntaImagen[40] = Properties.RecursosPruebas.pregunta41;
            PreguntaImagen[41] = Properties.RecursosPruebas.pregunta42;
            PreguntaImagen[42] = Properties.RecursosPruebas.pregunta43;
            PreguntaImagen[43] = Properties.RecursosPruebas.pregunta44;
            PreguntaImagen[44] = Properties.RecursosPruebas.pregunta45;
            PreguntaImagen[45] = Properties.RecursosPruebas.pregunta46;
            PreguntaImagen[46] = Properties.RecursosPruebas.pregunta47;
            PreguntaImagen[47] = Properties.RecursosPruebas.pregunta48;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {

            if (btnSiguiente.Name == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    return;
                }

                if (x == ListaPreguntas.ToArray().Count() - 1)
                {
                    this.btnSiguiente.Text = "Finalizar";
                    btnSiguiente.Name = "Finalizar";
                }

                MostrarPregunta(x);

                LimpiarControles();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Name == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {
                    btnSiguiente.Name = "Siguiente";
                    LimpiarControles();
                    btnSiguiente.Enabled = false;
                }

                MostrarPregunta(x);

            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Close();

                }

            }

            if (x == ListaPreguntas.ToArray().Count() - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + ListaPreguntas.ToArray().Count().ToString();

            btnAnterior.Enabled = true;

        }

        private void FinalizarPrueba()
        {

            //Validamos si hay una respuesta por parte del candidato antes de que se acabara el tiempo
            if ((RespuestaSuperior != -1 && RespuestaInferior != -1) && (btnSiguiente.Name == "Siguiente"))
            {
                GuardarRespuestas();
            }
            respuestas.Numero_prueba = Numero_Prueba;
            Cliente.FinalizaPruebaDominos(respuestas);


        }

        private void LimpiarControles()
        {
            RespuestaSuperior = -1;
            RespuestaInferior = -1;
            picResSuperior.Image = Properties.RecursosPruebas.Dpregunta;
            picResInferior.Image = Properties.RecursosPruebas.Dpregunta;

        }

        private void GuardarRespuestas()
        {

            respuestas.Id_pregunta = ListaPreguntas[x - 1].id_pregunta;
            respuestas.Respuesta_Superior = RespuestaSuperior;
            respuestas.Respuesta_Inferior = RespuestaInferior;
            respuestas.Numero_prueba = Numero_Prueba;

            if (ListaPreguntas[x - 1].respuesta_superior == RespuestaSuperior && ListaPreguntas[x - 1].respuesta_inferior == RespuestaInferior)
            {
                respuestas.Punto = 1;
            }
            else
            {
                respuestas.Punto = 0;
            }


            Cliente.InsertaRespuestaDominos(respuestas);

        }

        private void ActualizarRespuestas()
        {

            respuestas.id_respuestas_candidato = RespuestasCandidato[x - 1].id_respuestas_candidato;
            respuestas.Id_pregunta = ListaPreguntas[x].id_pregunta;
            respuestas.Respuesta_Superior = RespuestaSuperior;
            respuestas.Respuesta_Inferior = RespuestaInferior;
            respuestas.Numero_prueba = Numero_Prueba;

            if (ListaPreguntas[x - 1].respuesta_superior == RespuestaSuperior && ListaPreguntas[x - 1].respuesta_inferior == RespuestaInferior)
            {
                respuestas.Punto = 1;
            }
            else
            {
                respuestas.Punto = 0;
            }

            Cliente.ActualizaRespuestaDominos(respuestas);


        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {

            RespuestasCandidato = Cliente.ListaPreguntasContestadasDominos(Numero_Prueba);
            Recorrido = RespuestasCandidato.ToArray().Count();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + ListaPreguntas.ToArray().Count();


            x--;

            MostrarPregunta(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }

            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Name = "Siguiente";
                this.btnSiguiente.Text = "Siguiente";
            }

            btnSiguiente.Name = "Actualizar";
            btnSiguiente.Enabled = true;


        }


        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }

            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);


        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            //    this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
        }


        DOMINOS model_Dominos;

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {

            try
            {
                model_Dominos = Cliente.NuevoDominos(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            Numero_Prueba = model_Dominos.IdPrueba;
            //Mostramos las preguntas iniciales.
            MostrarPregunta(0);

            this.lblNumPreg.Text = "Pregunta: 1 de 48";
            this.panelAyuda.Hide();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
        }


        #region Eventos Arrastrar y Soltar en Picturbox

        int RespuestaSuperior = -1;
        int RespuestaInferior = -1;
        int PicSeleccionado = -1;

        private void picOpcion0_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 0;
            picOpcion0.DoDragDrop(picOpcion0.Image, DragDropEffects.Copy);


        }
        private void picOpcion1_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 1;
            picOpcion1.DoDragDrop(picOpcion1.Image, DragDropEffects.Copy);

        }

        private void picOpcion2_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 2;
            picOpcion2.DoDragDrop(picOpcion2.Image, DragDropEffects.Copy);

        }

        private void picOpcion3_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 3;
            picOpcion3.DoDragDrop(picOpcion3.Image, DragDropEffects.Copy);

        }

        private void picOpcion4_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 4;
            picOpcion4.DoDragDrop(picOpcion4.Image, DragDropEffects.Copy);

        }

        private void picOpcion5_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 5;
            picOpcion5.DoDragDrop(picOpcion5.Image, DragDropEffects.Copy);

        }

        private void picOpcion6_MouseDown(object sender, MouseEventArgs e)
        {
            PicSeleccionado = 6;
            picOpcion6.DoDragDrop(picOpcion6.Image, DragDropEffects.Copy);

        }

        private void picResSuperior_DragEnter(object sender, DragEventArgs e)
        {

            e.Effect = DragDropEffects.Copy;

        }

        private void picResSuperior_DragDrop(object sender, DragEventArgs e)
        {

            RespuestaSuperior = PicSeleccionado;
            picResSuperior.Image = (Image)e.Data.GetData(DataFormats.Bitmap);

            ValidarRespuestas();

        }

        private void picResInferior_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;

        }

        private void picResInferior_DragDrop(object sender, DragEventArgs e)
        {

            RespuestaInferior = PicSeleccionado;
            picResInferior.Image = (Image)e.Data.GetData(DataFormats.Bitmap);

            ValidarRespuestas();

        }

        #endregion

        private void ValidarRespuestas()
        {
            if (RespuestaSuperior != -1 && RespuestaInferior != -1)
            {
                btnSiguiente.Enabled = true;
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }


        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela una prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Dominos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if(prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.DOMINOS, Numero_Prueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmDominos_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
