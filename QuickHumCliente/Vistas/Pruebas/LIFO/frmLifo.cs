﻿using System;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Lifo
{
    public partial class frmLifo : Form
    {
        QuickHumClient cliente = Globales.cliente;
        private bool prueba_iniciada;
        dto_lifo_respuestas_candidato Respuestas = new dto_lifo_respuestas_candidato();
        dto_lifo_preguntas[] ListaPreguntas;
        dto_lifo_respuestas_candidato[] RespuestasCandidato;

        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;

        int Recorrido = 0;
        int x;
        int NumeroPrueba;

        public frmLifo(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }

        private void frmLifo_Load(object sender, EventArgs e)
        {
            btnSiguiente.Name = "Siguiente";

            DateTime hora;

            //Listamos las preguntas y las respuestas de la prueba
            ListaPreguntas = cliente.ListaLifoPreguntasRespuestas();

            //     CargarPreguntasRespuestas();

            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }



        private void MostrarPreguntas(int i)
        {

            this.txtPreguntas.Text = i + 1 + " ) " + ListaPreguntas[i].Pregunta;

            rtbA.Name = ListaPreguntas[i].Respuestas[0].IdRespuesta.ToString();
            rtbB.Name = ListaPreguntas[i].Respuestas[1].IdRespuesta.ToString();
            rtbC.Name = ListaPreguntas[i].Respuestas[2].IdRespuesta.ToString();
            rtbD.Name = ListaPreguntas[i].Respuestas[3].IdRespuesta.ToString();

            rtbA.Text = ListaPreguntas[i].Respuestas[0].Respuestas.ToString();
            rtbB.Text = ListaPreguntas[i].Respuestas[1].Respuestas.ToString();
            rtbC.Text = ListaPreguntas[i].Respuestas[2].Respuestas.ToString();
            rtbD.Text = ListaPreguntas[i].Respuestas[3].Respuestas.ToString();


            //Verificamos si el usuario ah retrocedido preguntas
            if (Recorrido / 4 > i)
            {

                txtA.Name = RespuestasCandidato[(x * 4)].IdRespuesta.ToString();
                txtB.Name = RespuestasCandidato[(x * 4) + 1].IdRespuesta.ToString();
                txtC.Name = RespuestasCandidato[(x * 4) + 2].IdRespuesta.ToString();
                txtD.Name = RespuestasCandidato[(x * 4) + 3].IdRespuesta.ToString();

                txtA.Text = RespuestasCandidato[(x * 4)].puntos.ToString();
                txtB.Text = RespuestasCandidato[(x * 4) + 1].puntos.ToString();
                txtC.Text = RespuestasCandidato[(x * 4) + 2].puntos.ToString();
                txtD.Text = RespuestasCandidato[(x * 4) + 3].puntos.ToString();

            }

        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {

            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {

            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }


        ServicioQuickHum.LIFO model_Lifo;
        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {

            model_Lifo = new ServicioQuickHum.LIFO();

            try
            {
                model_Lifo = cliente.NuevoLifo(_expediente.Numero, _escolaridad_prueba);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            NumeroPrueba = model_Lifo.IdPrueba;


            //Mostramos preguntas iniciales
            MostrarPreguntas(0);

            this.lblNumPreg.Text = "Pregunta: 1 de 18";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.txtA.Focus();
            this.tiempo.Enabled = true;

        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {



            if (btnSiguiente.Name == "Siguiente")
            {
                x++;
                guardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    // desctivarCajas(false);
                    return;
                }


                LimpiarCampos();
                txtA.Focus();
                btnSiguiente.Enabled = false;

                MostrarPreguntas(x);

            }
            else if (btnSiguiente.Name == "Actualizar")
            {
                x++;

                ActualizarRespuestas();

                if (Recorrido / 4 <= x)
                {
                    btnSiguiente.Name = "Siguiente";
                    LimpiarCampos();
                    btnSiguiente.Enabled = false;
                }

                MostrarPreguntas(x);
            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    guardarRespuestas();
                    Respuestas.Numero_prueba = NumeroPrueba;
                    cliente.FinalizaPruebaLifo(Respuestas);

                    this.Close();
                }

            }

            if (x == ListaPreguntas.ToArray().Count() - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
                btnSiguiente.Name = "Finalizar";
            }


            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + ListaPreguntas.ToList().Count();

            btnAnterior.Enabled = true;
            //btnSiguiente.Enabled = false;

        }

        private void guardarRespuestas()
        {

            ArrayList Puntos = new ArrayList();

            Puntos.Add(Convert.ToInt32(txtA.Text));
            Puntos.Add(Convert.ToInt32(txtB.Text));
            Puntos.Add(Convert.ToInt32(txtC.Text));
            Puntos.Add(Convert.ToInt32(txtD.Text));

            Respuestas.Numero_prueba = NumeroPrueba;

            for (int i = 0; i < 4; i++)
            {
                Respuestas.IdRespuesta = Convert.ToInt32(ListaPreguntas[x - 1].Respuestas[i].IdRespuesta);
                Respuestas.puntos = Convert.ToInt32(Puntos[i]);

                cliente.InsertaRespuestaLifo(Respuestas);
            }

        }

        private void ActualizarRespuestas()
        {

            ArrayList Puntos = new ArrayList();

            Puntos.Add(Convert.ToInt32(txtA.Text));
            Puntos.Add(Convert.ToInt32(txtB.Text));
            Puntos.Add(Convert.ToInt32(txtC.Text));
            Puntos.Add(Convert.ToInt32(txtD.Text));

            Respuestas.Numero_prueba = NumeroPrueba;

            for (int i = 0; i < 4; i++)
            {

                Respuestas.id_respuestas_candidato = RespuestasCandidato[((x - 1) * 4) + i].id_respuestas_candidato;
                Respuestas.IdRespuesta = Convert.ToInt32(RespuestasCandidato[((x - 1) * 4) + i].IdRespuesta);
                Respuestas.puntos = Convert.ToInt32(Puntos[i]);

                cliente.ActualizaRespuestaLifo(Respuestas);
            }

        }

        private void FinalizarPrueba()
        {
            //Validamos si el candidato ha contestado todo
            if (this.txtA.Text.Length != 0 && this.txtB.Text.Length != 0 && this.txtC.Text.Length != 0 && this.txtD.Text.Length != 0 && btnSiguiente.Name == "Siguiente")
            {
                guardarRespuestas();
            }


            Respuestas.Numero_prueba = NumeroPrueba;
            cliente.FinalizaPruebaLifo(Respuestas);


        }

        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }


            // bloquear las teclas 0 y del 5 al 9
            if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D5) || (ex.KeyChar == (char)Keys.D6) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
            {
                ex.Handled = true;
                return;
            }



        }

        private void LimpiarCampos()
        {
            txtA.Clear();
            txtB.Clear();
            txtC.Clear();
            txtD.Clear();
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            RespuestasCandidato = cliente.ListaPreguntasContestadasLifo(NumeroPrueba);
            Recorrido = RespuestasCandidato.ToArray().Count();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + ListaPreguntas.ToArray().Count();


            x--;

            MostrarPreguntas(x);


            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }

            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Name = "Siguiente";
                this.btnSiguiente.Text = "Siguiente";
            }

            btnSiguiente.Name = "Actualizar";
            btnSiguiente.Enabled = true;

        }


        #region Se valida que solo se digiten numeros

        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);

        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);

        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);

        }

        private void txtD_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);

        }

        #endregion


        #region Se valida que todos los valores sean correctos

        private void txtA_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtB.Focus();
        }

        private void txtB_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtC.Focus();
        }

        private void txtC_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtD.Focus();
        }

        private void txtD_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
        }

        private void validarCamposLlenos()
        {
            int[] datos;
            int i = 0;


            if (this.txtA.Text.Length == 0 || this.txtB.Text.Length == 0 || this.txtC.Text.Length == 0 || this.txtD.Text.Length == 0)
            {
                this.btnSiguiente.Enabled = false;
                return;
            }
            else
            {
                this.btnSiguiente.Enabled = true;
                this.btnSiguiente.Focus();
            }


            // validar campos con el mismo valor

            datos = new int[4];
            foreach (Control text in this.gbPreguntas.Controls)
            {
                if (text is TextBox)
                {
                    datos[i] = Convert.ToInt32(text.Text);
                    i++;
                }
            }
            Array.Sort(datos);
            for (i = 1; i < 4; i++)
            {
                if (datos[i] == datos[i - 1])
                {
                    this.lblMsj.Text = "El número: " + datos[i] + " está repetido. \nRecuerde que no se pueden repetir los números y solo puede usar del 1 al 4.";
                    this.btnSiguiente.Enabled = false;
                    return;
                }
                else
                {
                    this.lblMsj.Text = "";
                }
            }
            // fin de validación de campos con el mismo valor.  

        }

        #endregion



        private void frmLifo_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.panel1.Enabled = true;
        }

        private void btnCloseVentana_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void txtA_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtA_KeyUp_1(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtB.Focus();
        }

        private void txtB_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtC_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtD_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtB_KeyUp_1(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtC.Focus();
        }

        private void txtC_KeyUp_1(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
            txtD.Focus();
        }

        private void txtD_KeyUp_1(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Lifo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if(prueba_iniciada)
                cliente.CambiarEstadoPrueba(Test.LIFO, NumeroPrueba, PruebaEstado.CANCELADA);
            this.Close();

        }

    }
}
