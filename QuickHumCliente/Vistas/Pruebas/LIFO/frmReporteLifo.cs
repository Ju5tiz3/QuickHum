﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Lifo
{
    public partial class frmReporteLifo : Form
    {

        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        dto_lifo_resultados[] ListaResultados;

        DataTable dtResultados = new DataTable();
        DataTable dtCandidato = new DataTable();

        public frmReporteLifo(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void frmReporteLifo_Load(object sender, EventArgs e)
        {

            dtResultados.Columns.Add("Factor");
            dtResultados.Columns.Add("Suma");
            dtResultados.Columns.Add("Condicion");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            CargarReporte();

        }

        private void CargarReporte()
        {
            dtResultados.Clear();

            ListaResultados = Cliente.ListaLifoResultados(this._expediente.Id);

            for (int i = 0; i < ListaResultados.ToArray().Count(); i++)
            {

                DataRow row = dtResultados.NewRow();

                row["Factor"] = ListaResultados[i].Factor;
                row["Suma"] = ListaResultados[i].Suma;
                row["Condicion"] = ListaResultados[i].Condicion;

                dtResultados.Rows.Add(row);

            }

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportDataSource rds = new ReportDataSource("dsLifo", dtResultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            rptReporteLifo.LocalReport.DataSources.Clear();
            rptReporteLifo.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Lifo.rptPruebaLifo.rdlc";
            rptReporteLifo.LocalReport.DataSources.Add(rds);
            rptReporteLifo.LocalReport.DataSources.Add(rds_candidato);

            this.rptReporteLifo.RefreshReport();
        }

    }
}
