﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.CPI
{
    public partial class frmReporteCPI : Form
    {
        public frmReporteCPI(Expediente expediente)
        {
            InitializeComponent();
            this._expediente = expediente;
        }

        private readonly QuickHumClient Cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        dto_cpi_interpretacion_grupal[] ListaInterpretacionGrupal;
        dto_cpi_interpretacion_factor[] ListaInterpretacionFactor;
        dto_resultados_cpi[] ListaResultados;


        readonly DataTable dtInterpretacionGrupal = new DataTable();
        readonly DataTable dtValorFactores = new DataTable();
        readonly DataTable dtResultados = new DataTable();
        readonly DataTable dtResultadoInter = new DataTable();
        DataTable dtCandidato = new DataTable();

        ReportParameter Parametros = new ReportParameter();

   
        DataRow[] FiltroInterpretacion;

        private void frmReporteCPI_Load(object sender, EventArgs e)
        {

            // Definimos las estructuras de los Datatable
            dtValorFactores.Columns.Add("Factor");
            dtValorFactores.Columns.Add("Suma");
            dtValorFactores.Columns.Add("Total");
            dtValorFactores.Columns.Add("Nivel");
            dtValorFactores.Columns.Add("Interpretacion");

            dtInterpretacionGrupal.Columns.Add("IdInterpretacion");
            dtInterpretacionGrupal.Columns.Add("Grupo");
            dtInterpretacionGrupal.Columns.Add("Nivel");
            dtInterpretacionGrupal.Columns.Add("Interpretacion");

            dtResultados.Columns.Add("IdFactor");
            dtResultados.Columns.Add("Factor");
            dtResultados.Columns.Add("Grupo");
            dtResultados.Columns.Add("Resultado");

            dtResultadoInter.Columns.Add("IdInterpretacion");
            dtResultadoInter.Columns.Add("Grupo");
            dtResultadoInter.Columns.Add("Nivel");
            dtResultadoInter.Columns.Add("Interpretacion");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            ListaInterpretacionGrupal = Cliente.ListaCPIInterpretacionGrupal();
            ListaInterpretacionFactor = Cliente.ListaCPIInterpretacionFactor();


            for (int i = 0; i < ListaInterpretacionGrupal.ToArray().Count(); i++)
            {
                DataRow row = dtInterpretacionGrupal.NewRow();

                row["IdInterpretacion"] = ListaInterpretacionGrupal[i].IdInterpretacion;
                row["Grupo"] = ListaInterpretacionGrupal[i].grupo;
                row["Nivel"] = ListaInterpretacionGrupal[i].nivel;
                row["Interpretacion"] = ListaInterpretacionGrupal[i].Interpretacion;
                
                dtInterpretacionGrupal.Rows.Add(row);

            }

            GenerarResultado();
             
        }

        private void GenerarResultado()
        {
            DataRow[] Filtro;
            dtValorFactores.Clear();
            dtResultados.Clear();
            dtResultadoInter.Clear();

            string ParmGrupal = "";

            ListaResultados = Cliente.ListaCPIResultados(this._expediente.Id);

            //Convertimos la lista a DataTable para el DataSource
            for (int i = 0; i < ListaResultados.ToArray().Count(); i++)
            {

                if (ListaResultados[i].IdFactor != 0)
                {

                    DataRow row = dtValorFactores.NewRow();

                    row["Factor"] = ListaResultados[i].Factor;
                    row["Suma"] = ListaResultados[i].SumaPuntos;
                    row["Total"] = ListaResultados[i].Resultado;


                    //Validamos la interpretacion que le corresponde a 
                    for (int j = 0; j < ListaInterpretacionFactor.ToArray().Count(); j++)
                    {
                        if (ListaResultados[i].IdFactor == ListaInterpretacionFactor[j].IdFactor && ListaResultados[i].Resultado >= ListaInterpretacionFactor[j].ValorMinimo && ListaResultados[i].Resultado <= ListaInterpretacionFactor[j].ValorMaximo)
                        {
                            row["Nivel"] = ListaInterpretacionFactor[j].Nivel;
                            row["Interpretacion"] = ListaInterpretacionFactor[j].InterpretacionFactor;

                            dtValorFactores.Rows.Add(row);
                        }
                    }
                }
            }



            for (int i = 0; i < ListaResultados.ToArray().Count(); i++)
            {

                DataRow row = dtResultados.NewRow();

                row["IdFactor"] = ListaResultados[i].IdFactor;
                row["Factor"] = ListaResultados[i].Factor;
                row["Grupo"] = ListaResultados[i].Grupo;
                row["Resultado"] = Convert.ToInt32(ListaResultados[i].Resultado);

                dtResultados.Rows.Add(row);
            }

            //Declaramos un contador que nos dira si hay interpretaciones para mostrar
            int ContInterpretacioens = 0;
            try
            {
                //Validamos las interpretaciones grupales que se mostraran.
                if ((Filtro = dtResultados.Select("Resultado > 50 and Grupo = 'I'")).Length == 6)
                {

                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'I' and Nivel = 'alto'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;


                }
                if ((Filtro = dtResultados.Select("Resultado < 50 and Grupo = 'I'")).Length == 6)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'I' and Nivel = 'bajo'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;

                }
                if ((Filtro = dtResultados.Select("Resultado > 50 and Grupo = 'II'")).Length == 6)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'II' and Nivel = 'alto'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;

                }
                if ((Filtro = dtResultados.Select("Resultado < 50 and Grupo = 'II'")).Length == 6)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'II' and Nivel = 'bajo'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;
                }
                if ((Filtro = dtResultados.Select("Resultado > 50 and Grupo = 'III'")).Length == 3)
                {

                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'III' and Nivel = 'alto'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;

                }
                if ((Filtro = dtResultados.Select("Resultado < 50 and Grupo = 'III'")).Length == 3)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'III' and Nivel = 'bajo'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;
                }
                if ((Filtro = dtResultados.Select("Resultado > 50 and Grupo = 'IV'")).Length == 3)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'IV' and Nivel = 'alto'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;
                }
                if ((Filtro = dtResultados.Select("Resultado < 50 and Grupo = 'IV'")).Length == 3)
                {
                    FiltroInterpretacion = dtInterpretacionGrupal.Select("Grupo = 'IV' and Nivel = 'bajo'");
                    dtResultadoInter.Rows.Add(FiltroInterpretacion[0].ItemArray);
                    ContInterpretacioens = ContInterpretacioens + 1;
                }
            }

            catch (Exception)
            
            {
                
            }

            if (ContInterpretacioens == 0)
            {

                ParmGrupal = "true";

            }

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            dtCandidato.Rows.Add(row_candidato);

            Parametros = new ReportParameter("ValInterpretaciones", ParmGrupal);
            ReportDataSource rds = new ReportDataSource("dsFactores", dtValorFactores);
            ReportDataSource rds2 = new ReportDataSource("dsInterpretaciones", dtResultadoInter);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            rptReporteCPI.LocalReport.DataSources.Clear();
            rptReporteCPI.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.CPI.rptPruebaCPI.rdlc";
            rptReporteCPI.LocalReport.DataSources.Add(rds);
            rptReporteCPI.LocalReport.DataSources.Add(rds2);
            rptReporteCPI.LocalReport.DataSources.Add(rds_candidato);

            rptReporteCPI.LocalReport.SetParameters(Parametros);


            this.rptReporteCPI.RefreshReport();
        }
    }
}
