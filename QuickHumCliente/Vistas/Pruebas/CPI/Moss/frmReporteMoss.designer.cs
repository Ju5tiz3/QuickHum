﻿namespace QuickHumCliente.Vistas.Pruebas.Moss
{
    partial class frmReporteMoss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReporteMoss));
            this.dtMossBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rptPruebaMoss = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dtMossBindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtMossBindingSource
            // 
            this.dtMossBindingSource.DataMember = "dtMoss";
            // 
            // rptPruebaMoss
            // 
            this.rptPruebaMoss.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.dtMossBindingSource;
            this.rptPruebaMoss.LocalReport.DataSources.Add(reportDataSource1);
            this.rptPruebaMoss.LocalReport.ReportEmbeddedResource = "capaPresentacion.rptPruebaMoss.rdlc";
            this.rptPruebaMoss.Location = new System.Drawing.Point(0, 0);
            this.rptPruebaMoss.Name = "rptPruebaMoss";
            this.rptPruebaMoss.Size = new System.Drawing.Size(994, 565);
            this.rptPruebaMoss.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rptPruebaMoss);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(994, 565);
            this.panel3.TabIndex = 6;
            // 
            // frmReporteMoss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 565);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReporteMoss";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmReporteMoss";
            this.Load += new System.EventHandler(this.frmReporteMoss_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtMossBindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptPruebaMoss;
        private System.Windows.Forms.BindingSource dtMossBindingSource;
        //  private dsReportes dsReportes;
        private System.Windows.Forms.Panel panel3;
    }
}