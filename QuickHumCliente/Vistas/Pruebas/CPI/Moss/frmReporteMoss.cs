﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Reporting.WinForms;
using System.Windows.Forms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Moss
{


    public partial class frmReporteMoss : Form
    {
        private System.Windows.Forms.BindingSource Dt_BindingSource = new System.Windows.Forms.BindingSource();

        private Expediente _expediente = null;
        DataTable dtResultados = new DataTable();
        DataTable dtSumatoria = new DataTable();
        DataTable dtCandidato = new DataTable();
        QuickHumClient Cliente = Globales.cliente;

        public frmReporteMoss(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }
        private void frmReporteMoss_Load(object sender, EventArgs e)
        {

            dtSumatoria.Columns.Add("Factor");
            dtSumatoria.Columns.Add("Valor");

            dtResultados.Columns.Add("Factor");
            dtResultados.Columns.Add("Valor");
            dtResultados.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            CargarReporte();
        }
        private void CargarReporte()
        {
            dtResultados.Clear();
            dtSumatoria.Clear();
            dto_moss_resultados[] Resultados;
            try
            {

                Resultados = Cliente.ListaResultadosMoss(_expediente.Id);

                for (int i = 0; i < Resultados.ToList().Count; i++)
                {
                    DataRow row = dtSumatoria.NewRow();

                    row["Factor"] = Convert.ToString(Resultados[i].Factor);
                    row["Valor"] = Convert.ToInt32(Resultados[i].Suma);

                    dtSumatoria.Rows.Add(row);
                }


                for (int i = 0; i < dtSumatoria.Rows.Count; i++)
                {
                    DataRow row = dtResultados.NewRow();

                    row["Factor"] = dtSumatoria.Rows[i][0].ToString();

                    if (dtSumatoria.Rows[i][0].ToString().ToLower() == "habilidad de supervision")
                    {
                        row["Valor"] = Convert.ToInt32((100.0 / 6.0) * Convert.ToInt32(dtSumatoria.Rows[i][1]));
                    }
                    else if (dtSumatoria.Rows[i][0].ToString().ToLower() == "capacidad de decisión en las relaciones humanas.")
                    {
                        row["Valor"] = Convert.ToInt32((100.0 / 5.0) * Convert.ToInt32(dtSumatoria.Rows[i][1]));
                    }
                    else if (dtSumatoria.Rows[i][0].ToString().ToLower() == "capacidad de evaluación de problemas interpersonales.")
                    {
                        row["Valor"] = Convert.ToInt32((100.0 / 8.0) * Convert.ToInt32(dtSumatoria.Rows[i][1]));
                    }
                    else if (dtSumatoria.Rows[i][0].ToString().ToLower() == "habilidad para establecer relaciones interpersonales.")
                    {
                        row["Valor"] = Convert.ToInt32((100.0 / 5.0) * Convert.ToInt32(dtSumatoria.Rows[i][1]));
                    }
                    else if (dtSumatoria.Rows[i][0].ToString().ToLower() == "sentido comun y tacto en las relaciones interpersonales.")
                    {
                        row["Valor"] = Convert.ToInt32((100.0 / 6.0) * Convert.ToInt32(dtSumatoria.Rows[i][1]));
                    }


                    dtResultados.Rows.Add(row);

                }

                // Construimos el del candidato
                DataRow row_candidato = dtCandidato.NewRow();
                row_candidato["Dui"] = this._expediente.Candidato.Dui;
                row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
                row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
                row_candidato["Edad"] = this._expediente.Candidato.Edad;
                row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
                row_candidato["Fecha"] = this._expediente.Fecha;
                dtCandidato.Rows.Add(row_candidato);

                ReportParameter Parametros = new ReportParameter();
                Parametros = new ReportParameter("DUI", this._expediente.Candidato.Dui);

                ReportDataSource rds = new ReportDataSource("DataSetmoss", dtResultados);
                ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
                rptPruebaMoss.LocalReport.DataSources.Clear();
                //rptPruebaMoss.LocalReport.ReportPath = "rptPruebaMoss.rdlc";
                rptPruebaMoss.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Moss.rptPruebaMoss.rdlc";
                rptPruebaMoss.LocalReport.DataSources.Add(rds);
                rptPruebaMoss.LocalReport.DataSources.Add(rds_candidato);
                rptPruebaMoss.LocalReport.SetParameters(Parametros);
                rptPruebaMoss.RefreshReport();
                
            }
            catch (Exception)
            {
                MessageBox.Show("Candidato no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }





    }
}