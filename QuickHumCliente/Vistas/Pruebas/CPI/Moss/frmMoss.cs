﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Moss
{
    public partial class frmMoss : Form
    {

        //Variable que funcionara como parametro
        int Numero_prueba;
        DataTable dtPreguntas = new DataTable();
        DataTable dtRespuestas = new DataTable();
        DataTable dtResCandidato = new DataTable();
        int x = 0;
        int RespuestaCorrecta = 0;
        int Recorrido = 0;
        int punto = 0;
        DataRow[] row;
        MOSS modelo_MOSS;

        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;
        dto_moss_respusetas_candidato Respuestas = new dto_moss_respusetas_candidato();

        public frmMoss(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad = escolaridad;
        }

        private void frmMoss_Load(object sender, EventArgs e)
        {

            //Se le da la estructura al DataTable de Respuestas del candidato.
            dtResCandidato.Columns.Add("id_respuesta_candidato");
            dtResCandidato.Columns.Add("id_respuesta");
            dtResCandidato.Columns.Add("punto");

            DateTime hora;

            CargarPreguntasRespuestas();

            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private void CargarPreguntasRespuestas()
        {
            dto_moss_preguntas[] ListaPreguntas = Cliente.ListaMossPreguntas();
            //ReferenciaServicio.dto_moss_respuestas[] ListaOpciones = Cliente.ListaRespuestasMoss();        


            //Asignamos la estructura de los DataTable dtPreguntas y dtRespuestas
            dtPreguntas.Columns.Add("ID");
            dtPreguntas.Columns.Add("Descripcion");

            dtRespuestas.Columns.Add("id_respuesta");
            dtRespuestas.Columns.Add("Descripcion");
            dtRespuestas.Columns.Add("id_pregunta");
            dtRespuestas.Columns.Add("Respuesta_correcta");


            for (int i = 0; i < ListaPreguntas.ToList().Count; i++)
            {
                DataRow row = dtPreguntas.NewRow();

                row["ID"] = Convert.ToInt32(ListaPreguntas[i].IdPregunta);
                row["Descripcion"] = Convert.ToString(ListaPreguntas[i].Descripcion);

                for (int j = 0; j < 4; j++)
                {
                    DataRow row2 = dtRespuestas.NewRow();

                    row2["id_respuesta"] = Convert.ToInt32(ListaPreguntas[i].Respuestas[j].IdRespuesta);
                    row2["Descripcion"] = Convert.ToString(ListaPreguntas[i].Respuestas[j].Descripcion);
                    row2["id_pregunta"] = Convert.ToInt32(ListaPreguntas[i].IdPregunta);
                    row2["Respuesta_correcta"] = Convert.ToString(ListaPreguntas[i].Respuestas[j].RespuestaCorrecta);

                    dtRespuestas.Rows.Add(row2);
                }

                dtPreguntas.Rows.Add(row);
            }



            //Cargamos en pantalla las primeras preguntas con sus respectivas opciones de respuestas.
            txtPreguntas.Text = "1 " + ") " + dtPreguntas.Rows[0][1].ToString();

            rbtOpcionA.Name = dtRespuestas.Rows[0]["id_respuesta"].ToString();
            rbtOpcionB.Name = dtRespuestas.Rows[1]["id_respuesta"].ToString();
            rbtOpcionC.Name = dtRespuestas.Rows[2]["id_respuesta"].ToString();
            rbtOpcionD.Name = dtRespuestas.Rows[3]["id_respuesta"].ToString();

            rbtOpcionA.Text = "A) " + dtRespuestas.Rows[0]["descripcion"].ToString();
            rbtOpcionB.Text = "B) " + dtRespuestas.Rows[1]["descripcion"].ToString();
            rbtOpcionC.Text = "C) " + dtRespuestas.Rows[2]["descripcion"].ToString();
            rbtOpcionD.Text = "D) " + dtRespuestas.Rows[3]["descripcion"].ToString();

            for (int i = 0; i < 4; i++)
            {
                if (dtRespuestas.Rows[i]["Respuesta_correcta"].ToString() == "si")
                {
                    RespuestaCorrecta = Convert.ToInt32(dtRespuestas.Rows[i]["id_respuesta"]);
                }
            }

        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                modelo_MOSS = Cliente.NuevoMoss(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            Numero_prueba = modelo_MOSS.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 30";
            this.panelAyuda.Hide();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void FinalizarPrueba()
        {
            if ((rbtOpcionA.Checked == true || rbtOpcionB.Checked == true || rbtOpcionC.Checked == true || rbtOpcionD.Checked == true) && (btnSiguiente.Text == "Siguiente"))
            {
                guardarRespuestas();
            }

            Respuestas.NumeroPrueba = Numero_prueba;
            Cliente.FinalizaPruebaMoss(Respuestas);
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            //Limpiamos las respuestas anteriores.
            dtResCandidato.Clear();

            //Listamos las re`spuestas del candidato.
            dto_moss_respusetas_candidato[] ListaRespuestas = Cliente.ListaPreguntasContestadasMoss(Numero_prueba);


            for (int i = 0; i < ListaRespuestas.ToList().Count; i++)
            {
                DataRow row = dtResCandidato.NewRow();

                row["id_respuesta_candidato"] = Convert.ToInt32(ListaRespuestas[i].IdRespuestaTabla);
                row["id_respuesta"] = Convert.ToString(ListaRespuestas[i].IdRespuesta);
                row["punto"] = Convert.ToString(ListaRespuestas[i].Punto);

                dtResCandidato.Rows.Add(row);
            }

            Recorrido = dtResCandidato.Rows.Count;

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtPreguntas.Rows.Count;
            x--;

            mostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";
                desctivarCajas(true);
            }

            btnSiguiente.Text = "Actualizar";
            btnSiguiente.Enabled = true;
        }

        private void mostrarPreguntas(int i)
        {
            this.txtPreguntas.Text = i + 1 + " ) " + dtPreguntas.Rows[i][1].ToString();
            // se buscan las respuestas correspondientes a la pregunta en el datatable que ya se tiene en memoria
            row = dtRespuestas.Select("id_pregunta = '" + dtPreguntas.Rows[i][0].ToString() + "'");

            if (row.Length > 0)
            {
                rbtOpcionA.Name = row[0]["id_respuesta"].ToString();
                rbtOpcionB.Name = row[1]["id_respuesta"].ToString();
                rbtOpcionC.Name = row[2]["id_respuesta"].ToString();
                rbtOpcionD.Name = row[3]["id_respuesta"].ToString();

                rbtOpcionA.Text = "A) " + row[0]["descripcion"].ToString();
                rbtOpcionB.Text = "B) " + row[1]["descripcion"].ToString();
                rbtOpcionC.Text = "C) " + row[2]["descripcion"].ToString();
                rbtOpcionD.Text = "D) " + row[3]["descripcion"].ToString();

                for (int j = 0; j < 4; j++)
                {
                    if (row[j]["Respuesta_correcta"].ToString() == "si")
                    {
                        RespuestaCorrecta = Convert.ToInt32(row[j]["id_respuesta"]);
                    }
                }

                if (Recorrido > i)
                {
                    if (rbtOpcionA.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionA.Checked = true;
                    }
                    else if (rbtOpcionB.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionB.Checked = true;
                    }
                    else if (rbtOpcionC.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionC.Checked = true;
                    }
                    else if (rbtOpcionD.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionD.Checked = true;
                    }
                }

            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Text == "Siguiente")
            {
                x++;

                guardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    desctivarCajas(false);
                    return;
                }


                mostrarPreguntas(x);

                limpiarCampos();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Text == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {

                    btnSiguiente.Text = "Siguiente";
                    limpiarCampos();
                    btnSiguiente.Enabled = false;
                }

                mostrarPreguntas(x);

            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult DiagResul = XtraMessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (DiagResul == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Close();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;

            if (x == dtPreguntas.Rows.Count - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
            }


            btnAnterior.Enabled = true;

        }

        private void limpiarCampos()
        {
            rbtOpcionA.Checked = false;
            rbtOpcionB.Checked = false;
            rbtOpcionC.Checked = false;
            rbtOpcionD.Checked = false;

        }

        private void desctivarCajas(bool valor)
        {
            pnlOpciones.Enabled = valor;
        }

        private void guardarRespuestas()
        {
            Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
            Respuestas.Punto = Convert.ToByte(punto);
            Respuestas.NumeroPrueba = Convert.ToInt32(Numero_prueba);

            Cliente.InsertaRespuestaMoss(Respuestas);

        }

        private void ActualizarRespuestas()
        {

            Respuestas.IdRespuestaTabla = Convert.ToInt32(dtResCandidato.Rows[x - 1]["id_respuesta_candidato"]);
            Respuestas.IdRespuesta = Convert.ToByte(RecorrerRadios());
            Respuestas.Punto = Convert.ToByte(punto);
            Respuestas.NumeroPrueba = Convert.ToInt32(Numero_prueba);

            Cliente.ActualizaRespuestaMoss(Respuestas);

        }

        private int RecorrerRadios()
        {
            int radioChecado = 0;
            if (rbtOpcionA.Checked == true)
            {
                if (rbtOpcionA.Name == RespuestaCorrecta.ToString())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }
                radioChecado = Convert.ToInt32(rbtOpcionA.Name);


            }
            else if (rbtOpcionB.Checked == true)
            {

                if (rbtOpcionB.Name == RespuestaCorrecta.ToString())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionB.Name);
            }
            else if (rbtOpcionC.Checked == true)
            {

                if (rbtOpcionC.Name == RespuestaCorrecta.ToString())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionC.Name);
            }
            else if (rbtOpcionD.Checked == true)
            {

                if (rbtOpcionD.Name == RespuestaCorrecta.ToString())
                {
                    punto = 1;
                }
                else
                {
                    punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionD.Name);
            }

            return radioChecado;
        }

        #region Validar si hay un RadioButton seleccionado

        private void rbtOpcionA_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionB_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionC_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionD_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        #endregion


        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela el proceso de la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.MOSS, Numero_prueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmMoss_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
