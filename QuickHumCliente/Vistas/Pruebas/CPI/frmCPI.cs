﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.CPI
{
    public partial class frmCPI : Form
    {

        int NumeroPrueba;
        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada;


        ServicioQuickHum.CPI modelo_CPI;
        dto_cpi_respuestas_candidato Respuestas = new dto_cpi_respuestas_candidato();
        dto_cpi_preguntas[] ListaPreguntasRespuestas;
        dto_cpi_respuestas_candidato[] RespuestasCandidato;
        int x = 0;
        int Recorrido = 0;
        int RespuestaSeleccionada = 0;

        public frmCPI(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }


        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }


        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            this.pbxLoading.Visible = true;
            ////this.iniciarPrueba();
            ////como el procesos para cargar las 485 preguntas es lento
            ////se ha creado un backgroundWorker
            ////y mostrar una barra de carga mientras estas cargan
            if (this.backgroundWorker1.IsBusy != true)
            {
                this.backgroundWorker1.RunWorkerAsync();
            }
            //iniciarPrueba();
        }

        private void iniciarPrueba()
        {
            modelo_CPI = new ServicioQuickHum.CPI();
            try
            {
                modelo_CPI = Cliente.NuevoCPI(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            NumeroPrueba = modelo_CPI.IdPrueba;


            ListaPreguntasRespuestas = Cliente.ListaCPIPreguntasRespuestas();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {

        }

        private void frmCPI_Load(object sender, EventArgs e)
        {
            btnSiguiente.Name = "Siguiente";
            DateTime hora;



            this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }


        private void MostrarPreguntas(int i)
        {

            txtPreguntas.Text = i + 1 + " ) " + ListaPreguntasRespuestas[i].Descripcion;

            rbtOpcionA.Name = ListaPreguntasRespuestas[i].Respuestas[0].IdRespuesta.ToString();
            rbtOpcionB.Name = ListaPreguntasRespuestas[i].Respuestas[1].IdRespuesta.ToString();


            //Verificamos si el usuario ah retrocedido preguntas
            if (Recorrido > i)
            {

                if (rbtOpcionA.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                {
                    rbtOpcionA.Checked = true;
                    RespuestaSeleccionada = Convert.ToInt32(RespuestasCandidato[i].IdRespuestaTabla);
                }
                else if (rbtOpcionB.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                {
                    rbtOpcionB.Checked = true;
                    RespuestaSeleccionada = Convert.ToInt32(RespuestasCandidato[i].IdRespuestaTabla);
                }

            }

        }

        private void FinalizarPrueba()
        {

            //Validamos si hay una respuesta por parte del candidato antes de que se acabara el tiempo
            if ((rbtOpcionA.Checked == true || rbtOpcionB.Checked == true) && (btnSiguiente.Name == "Siguiente"))
            {
                GuardarRespuestas();
            }

            Respuestas.NumeroPrueba = NumeroPrueba;
            Cliente.FinalizaPruebaCPI(Respuestas);


        }



        #region Validar si hay un RadioButton Seleccionado


        private void rbtOpcionA_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }

        private void rbtOpcionB_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
            btnSiguiente.Focus();
        }



        #endregion

        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Name == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;

                    return;
                }

                MostrarPreguntas(x);

                LimpiarControles();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Name == "Actualizar")
            {
                x++;
                ActualizarRespuestas();
                if (Recorrido <= x)
                {
                    btnSiguiente.Name = "Siguiente";
                    LimpiarControles();
                    btnSiguiente.Enabled = false;
                }

                MostrarPreguntas(x);

            }
            else if (btnSiguiente.Name == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    GuardarRespuestas();
                    FinalizarPrueba();
                    this.Close();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + ListaPreguntasRespuestas.ToArray().Count().ToString();

            btnAnterior.Enabled = true;

            if (x == ListaPreguntasRespuestas.ToArray().Count() - 1)
            {
                this.btnSiguiente.Name = "Finalizar";
                this.btnSiguiente.Text = "Finalizar";
            }

        }


        private void GuardarRespuestas()
        {

            Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
            Respuestas.Punto = Convert.ToByte(1);
            Respuestas.NumeroPrueba = Convert.ToInt32(NumeroPrueba);

            Cliente.InsertaRespuestaCPI(Respuestas);

        }


        private void ActualizarRespuestas()
        {


            Respuestas.IdRespuestaTabla = RespuestaSeleccionada;
            Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
            Respuestas.Punto = Convert.ToByte(1);
            Respuestas.NumeroPrueba = Convert.ToInt32(NumeroPrueba);

            Cliente.ActualizaRespuestaCPI(Respuestas);

        }

        private int RecorrerRadios()
        {
            int radioChecado = 0;

            if (rbtOpcionA.Checked == true)
            {
                radioChecado = Convert.ToInt32(rbtOpcionA.Name);
            }
            else if (rbtOpcionB.Checked == true)
            {
                radioChecado = Convert.ToInt32(rbtOpcionB.Name);
            }

            return radioChecado;
        }


        private void LimpiarControles()
        {
            rbtOpcionA.Checked = false;
            rbtOpcionB.Checked = false;

        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            RespuestasCandidato = Cliente.ListaPreguntasContestadasCPI(NumeroPrueba);
            Recorrido = RespuestasCandidato.ToArray().Count();

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + ListaPreguntasRespuestas.ToArray().Count();
            x--;

            MostrarPreguntas(x);

            btnSiguiente.Name = "Actualizar";

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Name = "Siguiente";
                this.btnSiguiente.Text = "Siguiente";
            }


            btnSiguiente.Enabled = true;
        }


        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            this.iniciarPrueba();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                MessageBox.Show("Proceso cancelado");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error.Message);
            }
            else
            {
                //MessageBox.Show("Done");
            }

            //Mostramos las preguntas iniciales.
            MostrarPreguntas(0);
            this.lblNumPreg.Text = "Pregunta: 1 de 480";
            this.panelAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
            this.pbxLoading.Visible = false;
        }

        private void btnCancelarAyuda_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.CPI, NumeroPrueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmCPI_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }


    }
}
