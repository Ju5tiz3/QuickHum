﻿namespace QuickHumCliente.Vistas.Pruebas.CPI
{
    partial class frmReporteCPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptReporteCPI = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptReporteCPI);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(909, 483);
            this.panel2.TabIndex = 1;
            // 
            // rptReporteCPI
            // 
            this.rptReporteCPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptReporteCPI.Location = new System.Drawing.Point(0, 0);
            this.rptReporteCPI.Name = "rptReporteCPI";
            this.rptReporteCPI.Size = new System.Drawing.Size(909, 483);
            this.rptReporteCPI.TabIndex = 0;
            // 
            // frmReporteCPI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 483);
            this.Controls.Add(this.panel2);
            this.Name = "frmReporteCPI";
            this.Text = "frmReporteCPI";
            this.Load += new System.EventHandler(this.frmReporteCPI_Load);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer rptReporteCPI;
    }
}