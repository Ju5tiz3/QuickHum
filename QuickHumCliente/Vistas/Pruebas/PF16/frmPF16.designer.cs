﻿namespace QuickHumCliente.Vistas.Pruebas.PF16
{
    partial class frmPF16
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPF16));
            this.labNumAsignacion = new System.Windows.Forms.Label();
            this.labFecha = new System.Windows.Forms.Label();
            this.labCurso = new System.Windows.Forms.Label();
            this.labSexo = new System.Windows.Forms.Label();
            this.labEdad = new System.Windows.Forms.Label();
            this.labStado = new System.Windows.Forms.Label();
            this.labCentro = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Time = new System.Windows.Forms.Timer(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.Panel();
            this.rbtB = new System.Windows.Forms.RadioButton();
            this.rbtC = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtA = new System.Windows.Forms.RadioButton();
            this.btnAtras = new System.Windows.Forms.Button();
            this.labNum = new System.Windows.Forms.Label();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.labPregunta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labMT = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labM = new System.Windows.Forms.Label();
            this.labS = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // labNumAsignacion
            // 
            this.labNumAsignacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labNumAsignacion.AutoSize = true;
            this.labNumAsignacion.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.labNumAsignacion.Location = new System.Drawing.Point(592, 169);
            this.labNumAsignacion.Name = "labNumAsignacion";
            this.labNumAsignacion.Size = new System.Drawing.Size(71, 22);
            this.labNumAsignacion.TabIndex = 46;
            this.labNumAsignacion.Text = "Sexo: ¿?";
            this.labNumAsignacion.Visible = false;
            // 
            // labFecha
            // 
            this.labFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labFecha.AutoSize = true;
            this.labFecha.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labFecha.Location = new System.Drawing.Point(425, 123);
            this.labFecha.Name = "labFecha";
            this.labFecha.Size = new System.Drawing.Size(74, 18);
            this.labFecha.TabIndex = 45;
            this.labFecha.Text = "15-01-2015";
            // 
            // labCurso
            // 
            this.labCurso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labCurso.AutoSize = true;
            this.labCurso.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labCurso.Location = new System.Drawing.Point(483, 90);
            this.labCurso.Name = "labCurso";
            this.labCurso.Size = new System.Drawing.Size(107, 18);
            this.labCurso.TabIndex = 44;
            this.labCurso.Text = "Puesto resuesta";
            // 
            // labSexo
            // 
            this.labSexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labSexo.AutoSize = true;
            this.labSexo.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labSexo.Location = new System.Drawing.Point(425, 157);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(71, 18);
            this.labSexo.TabIndex = 43;
            this.labSexo.Text = "Masculino";
            // 
            // labEdad
            // 
            this.labEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labEdad.AutoSize = true;
            this.labEdad.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labEdad.Location = new System.Drawing.Point(425, 55);
            this.labEdad.Name = "labEdad";
            this.labEdad.Size = new System.Drawing.Size(22, 18);
            this.labEdad.TabIndex = 42;
            this.labEdad.Text = "23";
            // 
            // labStado
            // 
            this.labStado.AutoSize = true;
            this.labStado.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labStado.Location = new System.Drawing.Point(128, 140);
            this.labStado.Name = "labStado";
            this.labStado.Size = new System.Drawing.Size(52, 18);
            this.labStado.TabIndex = 41;
            this.labStado.Text = "Casado";
            // 
            // labCentro
            // 
            this.labCentro.AutoSize = true;
            this.labCentro.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labCentro.Location = new System.Drawing.Point(88, 97);
            this.labCentro.Name = "labCentro";
            this.labCentro.Size = new System.Drawing.Size(133, 18);
            this.labCentro.TabIndex = 40;
            this.labCentro.Text = "Regional San Miguel";
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labName.Location = new System.Drawing.Point(177, 54);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(116, 18);
            this.labName.TabIndex = 39;
            this.labName.Text = "Carlos Hernandez";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(360, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 22);
            this.label13.TabIndex = 38;
            this.label13.Text = "Fecha:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(15, 137);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 22);
            this.label12.TabIndex = 37;
            this.label12.Text = "Estado Civil:";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(360, 87);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 22);
            this.label11.TabIndex = 36;
            this.label11.Text = "Curso Puesto:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(15, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 22);
            this.label10.TabIndex = 35;
            this.label10.Text = "Centro:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(360, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 22);
            this.label9.TabIndex = 34;
            this.label9.Text = "Sexo:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(360, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 22);
            this.label8.TabIndex = 33;
            this.label8.Text = "Edad:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(15, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 22);
            this.label7.TabIndex = 32;
            this.label7.Text = "Nombre y Apellido:";
            // 
            // Time
            // 
            this.Time.Tick += new System.EventHandler(this.Time_Tick_1);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(883, 36);
            this.label14.TabIndex = 62;
            this.label14.Text = "PRUEBA PF16";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.labNumAsignacion);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.labFecha);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.labCurso);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.labSexo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.labEdad);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.labStado);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.labCentro);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.labName);
            this.groupBox3.Location = new System.Drawing.Point(94, 451);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(694, 211);
            this.groupBox3.TabIndex = 63;
            this.groupBox3.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(694, 28);
            this.label15.TabIndex = 63;
            this.label15.Text = "Datos Generales";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.btnFinalizar);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnAtras);
            this.groupBox1.Controls.Add(this.labNum);
            this.groupBox1.Controls.Add(this.btnSiguiente);
            this.groupBox1.Controls.Add(this.labPregunta);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(94, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(694, 342);
            this.groupBox1.TabIndex = 64;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnFinalizar.FlatAppearance.BorderSize = 0;
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnFinalizar.ForeColor = System.Drawing.Color.White;
            this.btnFinalizar.Image = global::QuickHumCliente.Properties.RecursosPruebas.timerStop;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(512, 293);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(164, 33);
            this.btnFinalizar.TabIndex = 25;
            this.btnFinalizar.Text = "Finalizar Prueba";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.rbtB);
            this.groupBox2.Controls.Add(this.rbtC);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.rbtA);
            this.groupBox2.Location = new System.Drawing.Point(10, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(666, 121);
            this.groupBox2.TabIndex = 69;
            // 
            // rbtB
            // 
            this.rbtB.AutoSize = true;
            this.rbtB.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.rbtB.Location = new System.Drawing.Point(46, 53);
            this.rbtB.Name = "rbtB";
            this.rbtB.Size = new System.Drawing.Size(14, 13);
            this.rbtB.TabIndex = 17;
            this.rbtB.UseVisualStyleBackColor = true;
            // 
            // rbtC
            // 
            this.rbtC.AutoSize = true;
            this.rbtC.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.rbtC.Location = new System.Drawing.Point(46, 82);
            this.rbtC.Name = "rbtC";
            this.rbtC.Size = new System.Drawing.Size(14, 13);
            this.rbtC.TabIndex = 16;
            this.rbtC.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(16, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 22);
            this.label4.TabIndex = 15;
            this.label4.Text = "C";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(16, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 22);
            this.label3.TabIndex = 14;
            this.label3.Text = "B";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(16, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 22);
            this.label2.TabIndex = 13;
            this.label2.Text = "A";
            // 
            // rbtA
            // 
            this.rbtA.AutoSize = true;
            this.rbtA.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.rbtA.Location = new System.Drawing.Point(46, 26);
            this.rbtA.Name = "rbtA";
            this.rbtA.Size = new System.Drawing.Size(14, 13);
            this.rbtA.TabIndex = 12;
            this.rbtA.UseVisualStyleBackColor = true;
            // 
            // btnAtras
            // 
            this.btnAtras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAtras.FlatAppearance.BorderSize = 0;
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtras.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAtras.ForeColor = System.Drawing.Color.White;
            this.btnAtras.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAtras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtras.Location = new System.Drawing.Point(10, 293);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(113, 33);
            this.btnAtras.TabIndex = 24;
            this.btnAtras.Text = "Atras";
            this.btnAtras.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtras.UseVisualStyleBackColor = false;
            this.btnAtras.Visible = false;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // labNum
            // 
            this.labNum.AutoSize = true;
            this.labNum.BackColor = System.Drawing.Color.Transparent;
            this.labNum.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.labNum.Location = new System.Drawing.Point(99, 41);
            this.labNum.Name = "labNum";
            this.labNum.Size = new System.Drawing.Size(53, 22);
            this.labNum.TabIndex = 68;
            this.labNum.Text = "Some";
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.Location = new System.Drawing.Point(131, 293);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(113, 33);
            this.btnSiguiente.TabIndex = 17;
            this.btnSiguiente.Text = "Iniciar";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnNex_Click);
            // 
            // labPregunta
            // 
            this.labPregunta.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labPregunta.BackColor = System.Drawing.Color.Transparent;
            this.labPregunta.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labPregunta.Location = new System.Drawing.Point(13, 89);
            this.labPregunta.Name = "labPregunta";
            this.labPregunta.Size = new System.Drawing.Size(663, 51);
            this.labPregunta.TabIndex = 67;
            this.labPregunta.Text = "Una de las preguntas del test que sera aplicada a un aspirante";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(15, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 22);
            this.label1.TabIndex = 66;
            this.label1.Text = "Pregunta:";
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(694, 28);
            this.label16.TabIndex = 64;
            this.label16.Text = "Responda cada una de las preguntas";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.btnCloseWindow.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Location = new System.Drawing.Point(846, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 77;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo21;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.labMT);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labM);
            this.panel2.Controls.Add(this.labS);
            this.panel2.Location = new System.Drawing.Point(534, 41);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 37);
            this.panel2.TabIndex = 65;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(287, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 18);
            this.label17.TabIndex = 24;
            this.label17.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(68, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 19);
            this.label5.TabIndex = 22;
            this.label5.Text = "Tiempo Transcurrido:";
            // 
            // labMT
            // 
            this.labMT.AutoSize = true;
            this.labMT.BackColor = System.Drawing.Color.Transparent;
            this.labMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMT.ForeColor = System.Drawing.Color.White;
            this.labMT.Location = new System.Drawing.Point(222, 10);
            this.labMT.Name = "labMT";
            this.labMT.Size = new System.Drawing.Size(26, 18);
            this.labMT.TabIndex = 21;
            this.labMT.Text = "00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(248, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 18);
            this.label6.TabIndex = 23;
            this.label6.Text = ":";
            // 
            // labM
            // 
            this.labM.AutoSize = true;
            this.labM.BackColor = System.Drawing.Color.Transparent;
            this.labM.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labM.ForeColor = System.Drawing.Color.White;
            this.labM.Location = new System.Drawing.Point(300, 10);
            this.labM.Name = "labM";
            this.labM.Size = new System.Drawing.Size(26, 18);
            this.labM.TabIndex = 19;
            this.labM.Text = "00";
            // 
            // labS
            // 
            this.labS.AutoSize = true;
            this.labS.BackColor = System.Drawing.Color.Transparent;
            this.labS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labS.ForeColor = System.Drawing.Color.White;
            this.labS.Location = new System.Drawing.Point(261, 10);
            this.labS.Name = "labS";
            this.labS.Size = new System.Drawing.Size(26, 18);
            this.labS.TabIndex = 20;
            this.labS.Text = "00";
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel3);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(883, 418);
            this.panelAyuda.TabIndex = 108;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Location = new System.Drawing.Point(143, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(597, 219);
            this.panel3.TabIndex = 74;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Location = new System.Drawing.Point(560, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(36, 36);
            this.panel7.TabIndex = 80;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(243, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 33);
            this.button1.TabIndex = 18;
            this.button1.Text = "Iniciar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(1, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(36, 36);
            this.panel6.TabIndex = 79;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label18.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(34, 0);
            this.label18.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(529, 36);
            this.label18.TabIndex = 61;
            this.label18.Text = "INDICACIONES";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.Font = new System.Drawing.Font("Calibri", 11F);
            this.label19.Location = new System.Drawing.Point(14, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(575, 80);
            this.label19.TabIndex = 0;
            this.label19.Text = "Por favor, responda con franqueza a las siguientes cuestiones.";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmPF16
            // 
            this.AcceptButton = this.btnSiguiente;
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 454);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPF16";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QuickHum - Prueba 16PF";
            this.Load += new System.EventHandler(this.xfrm16PF_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelAyuda.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labFecha;
        private System.Windows.Forms.Label labCurso;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label labEdad;
        private System.Windows.Forms.Label labStado;
        private System.Windows.Forms.Label labCentro;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMT;
        private System.Windows.Forms.Label labS;
        private System.Windows.Forms.Label labM;
        private System.Windows.Forms.Timer Time;
        private System.Windows.Forms.Label labNumAsignacion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel groupBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel groupBox2;
        private System.Windows.Forms.RadioButton rbtB;
        private System.Windows.Forms.RadioButton rbtC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtA;
        private System.Windows.Forms.Label labNum;
        private System.Windows.Forms.Label labPregunta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;

    }
}