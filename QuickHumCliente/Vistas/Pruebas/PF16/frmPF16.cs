﻿using System;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.PF16
{
    public partial class frmPF16 : DevExpress.XtraEditors.XtraForm
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad_prueba = null;

        string[] pregunta = new string[187];
        int[] IdRespuesta = new int[187];
        int[] IdPunto = new int[187];
        int OpcionA, OpcionB, OpcionC, mil, seg, min;
        int IdRespuestaSeleccionada = 0;
        int z = 0;
        int NumeroRespuesta = 1;
        int NumeroAsignacion;
        
        ServicioQuickHum.PF16 modelo_PF16;

        public frmPF16(Expediente expediente, EscolaridadPrueba escolaridad_prueba)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad_prueba = escolaridad_prueba;
        }
        /// <summary>
        /// Crea la Prueba Con Sus Preguntas y Respuestas
        /// </summary>
        public void CrearPrueba() {


            //Candidato candidato = cliente.CargarCandidato("76690331-3");
            //labName.Text = candidato.Nombre + " " + candidato.Apellido;
            //labSexo.Text = Convert.ToString((CandidatoGenero)candidato.Genero);
            //labEdad.Text = Convert.ToString(candidato.Edad);
            //labFecha.Text = DateTime.Now.ToString("yyyy-MM-dd");
            //labCentro.Text = expediente.Ubicacion.Direccion;
            //labStado.Text = "Tampoco se cual es";

            try
            {
                modelo_PF16 = new ServicioQuickHum.PF16();
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            modelo_PF16 = cliente.NuevoPF16(_expediente.Numero, _escolaridad_prueba);

            for (int i = 0; i < modelo_PF16.Pregunta.Length; i++)
            {  
                pregunta[i] = modelo_PF16.Pregunta[i].Pregunta;

            }

            rbtA.Text = modelo_PF16.Pregunta[z].Respuesta[0].OpcionRespuesta;
            rbtB.Text = modelo_PF16.Pregunta[z].Respuesta[1].OpcionRespuesta;
            rbtC.Text = modelo_PF16.Pregunta[z].Respuesta[2].OpcionRespuesta;

             OpcionA = modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta;
             OpcionB = modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta;
             OpcionC = modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta;

            labPregunta.Text = pregunta[0];
            labNum.Text = NumeroRespuesta++.ToString();
            NumeroAsignacion = modelo_PF16.IdPrueba;
        }

        /// <summary>
        /// Funcion que contiene el arreglo de las preguntas con sus respectivas respuestas
        /// </summary>
        /// <param name="z"></param>
        public void ListarPregunta(int z) 
        {
            NumeroRespuesta = z + 1;
            labPregunta.Text = pregunta[z];
            labNum.Text = NumeroRespuesta.ToString();

            rbtA.Text = modelo_PF16.Pregunta[z].Respuesta[0].OpcionRespuesta;
            rbtB.Text = modelo_PF16.Pregunta[z].Respuesta[1].OpcionRespuesta;
            rbtC.Text = modelo_PF16.Pregunta[z].Respuesta[2].OpcionRespuesta;

            OpcionA = modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta;
            OpcionB = modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta;
            OpcionC = modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta;
        }

        /// <summary>
        /// Inicia la Prueba...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNex_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Iniciar")
            {
                CrearPrueba();
                btnSiguiente.Text = "Siguiente";

                btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
                btnSiguiente.BackColor = Color.FromArgb(27, 183, 212);
                btnSiguiente.ImageAlign = ContentAlignment.MiddleRight;
                btnSiguiente.TextAlign = ContentAlignment.MiddleLeft;
                btnAtras.Visible = true;
                
                this.labS.Show(); this.labM.Show(); this.labMT.Show(); this.label5.Show(); this.label6.Show(); Time.Start();
                this.rbtA.Show(); this.rbtB.Show(); this.rbtC.Show(); label2.Show(); label3.Show(); label4.Show();
                return;
            }

            if (btnSiguiente.Text == "Siguiente")
            {
                Time.Start();
                if (rbtA.Checked) { IdRespuestaSeleccionada = OpcionA; }

                if (rbtB.Checked) { IdRespuestaSeleccionada = OpcionB; }

                if (rbtC.Checked) { IdRespuestaSeleccionada = OpcionC; }

                if (rbtA.Checked == false && rbtC.Checked == false && rbtB.Checked == false)
                { MessageBox.Show("Debes Seleccionar una Respuesta", "16PF", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }

                //dto_PF16Punto[] modelo_punto = cliente.ListarPuntos();
                dto_PF16Punto actualizar_respuesta = new dto_PF16Punto();

                if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta || IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta || IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta) 
                {
                    
                    rbtA.Checked = false; rbtB.Checked = false; rbtC.Checked = false;
                    
                    actualizar_respuesta.IdPunto = IdPunto[z];
                    actualizar_respuesta.IdRespuesta = IdRespuestaSeleccionada;
                    if (cliente.ActualizarRespuesta(actualizar_respuesta))
                    { }
                    z++;
                    if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta) { rbtA.Checked = true; }
                    if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta) { rbtB.Checked = true; }
                    if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta) { rbtC.Checked = true; }
                    //z++;
                    ListarPregunta(z);
                    
                    btnAtras.Enabled = true;

                    return;
                }

                dto_PF16Punto insert_respuestas = new dto_PF16Punto();
                
                insert_respuestas.Fecha = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                insert_respuestas.IdRespuesta = IdRespuestaSeleccionada;
                insert_respuestas.NumeroPrueba = NumeroAsignacion;
                if (cliente.InsertarRespuestaPF16(insert_respuestas))
                {
                    z++;
                }
                rbtA.Checked = false; rbtB.Checked = false; rbtC.Checked = false;

                if (Convert.ToInt32(labNum.Text) > 1) { btnAtras.Enabled = true; }

                if (Convert.ToInt32(labNum.Text) == 187)
                {

                    ServicioQuickHum.PF16 finalizar_prueba = new ServicioQuickHum.PF16();
                    finalizar_prueba.IdPrueba = modelo_PF16.IdPrueba;
                    if(cliente.FinalizarPruebaPF16(finalizar_prueba))
                    {}


                    btnSiguiente.Enabled = false;
                    Time.Stop();
                    labName.Text = ""; labNum.Text = ""; labPregunta.Text = ""; rbtA.Visible = false; rbtC.Visible = false; rbtB.Visible = false; label2.Hide(); label3.Hide(); label4.Hide();
  
                    MessageBox.Show("Test Finalizado","16PF",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                    return;
                }
                ListarPregunta(z);
            }
        }

        /// <summary>
        /// Inicializa el Temporizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Time_Tick_1(object sender, EventArgs e)
        {
            Time.Interval = 10;

            mil = Convert.ToInt16(labM.Text); mil += 1; labM.Text = mil.ToString();

            if (mil == 60)
            {
                seg = Convert.ToInt16(labS.Text); seg += 1; labS.Text = seg.ToString(); labM.Text = "00";

                if (seg == 60) { min = Convert.ToInt16(labMT.Text); min += 1; labMT.Text = min.ToString(); labS.Text = "00"; }
            }
            if (min == 40)
            {
                Time.Stop();
                MessageBox.Show("Se ha terminado el tiempo estimado para realizar esta prueba", "TIEMPO AGOTADO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                // btnUpload_Click(sender, e);s
                //btnUpload.Enabled = true; btnUpload.PerformClick();
                this.Close();
            }
        }

        /// <summary>
        /// Carga Controles al Cargar el Formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xfrm16PF_Load(object sender, EventArgs e)
        {
            rbtA.Hide(); label2.Hide();
            rbtB.Hide(); label3.Hide();
            rbtC.Hide(); label4.Hide();
        }

        /// <summary>
        /// Actualiza las respuestas que el candidato ha contestado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAtras_Click(object sender, EventArgs e)
        {
            if (z == 0) { btnAtras.Enabled = false; return; }
            z--;

            dto_PF16Punto[] modelo_punto = cliente.ListarPuntos(NumeroAsignacion);

            for (int x = 0; x < modelo_punto.Length; ++x)
            {
                IdRespuesta[x] = modelo_punto[x].IdRespuesta;
                IdPunto[x] = modelo_punto[x].IdPunto;
            }
           
            labNum.Text = z.ToString();
            if (z == 0)
            {
                labNum.Text = "1";
                labPregunta.Text = pregunta[0];
                if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta) { rbtA.Checked = true; }
                if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta) { rbtB.Checked = true; }
                if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta) { rbtC.Checked = true; }
                ListarPregunta(z);
                btnAtras.Enabled = false; return;
            }

            if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[0].IdRespuesta) { rbtA.Checked = true; }
            if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[1].IdRespuesta) { rbtB.Checked = true; }
            if (IdRespuesta[z] == modelo_PF16.Pregunta[z].Respuesta[2].IdRespuesta) { rbtC.Checked = true; }
            ListarPregunta(z);
        }

        /// <summary>
        /// Finaliza la Prueba sin Importar las Respuestas que ha contestado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            ServicioQuickHum.PF16  finalizar_prueba = new ServicioQuickHum.PF16 ();
            finalizar_prueba.IdPrueba = modelo_PF16.IdPrueba;
            if (cliente.FinalizarPruebaPF16(finalizar_prueba))
            { }


            btnSiguiente.Enabled = false;
            Time.Stop();
            labName.Text = ""; labNum.Text = ""; labPregunta.Text = ""; rbtA.Visible = false; rbtC.Visible = false; rbtB.Visible = false; label2.Hide(); label3.Hide(); label4.Hide();

            MessageBox.Show("Test Finalizado", "16PF", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.btnNex_Click(sender, e);
        }

    }
}



