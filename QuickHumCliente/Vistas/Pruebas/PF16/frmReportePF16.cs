﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Reporting.WinForms;


using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.PF16
{
    public partial class frmReportePF16 : DevExpress.XtraEditors.XtraForm
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;

        string[] FactoresGeneral = new string[] { 
            "SIZOTIMIA - AFECTOTIMIA","INTELIGENCIA ALTA - BAJA",
            "POCA, MUCHA FUERZA DEL EGO","SUMISION - DOMINANCIA",
            "DISURGENCIA - SURGENCIA","POCA - MUCHA FUERZA DEL SUPEREGO",
            "TRECTIA - PARMIA","HARRIA - PREMSIA","ALAXIA - PROTENSION",
            "PRAXERNIA -AUTIA","SENCILLEZ - ASTUCIA",
            "ADECUACION IMPERTURBABLE - TENDENCIA A CULPABILIDAD","CONSERVADURISMO - RADICALISMO",
            "ADHESION AL GRUPO - AUTOSUFICIENCIA","BAJA INTEGRACION - MUCHO CONTROL AUTIMAGEN",
            "POCA TENSION ENERGETICA - MUCHA TENSION ENERGETICA"};
        string[] FactorSegundoOrden = new string[]{
            "AJUSTE - ANSIEDAD","INTROVERSEION - EXTRAVERSION",
            "POCA - MUCHA SOCIALIZACION CONTROLADA","DEPENDENCIA - INDEPENDENCIA"};
        
        DataTable data = new DataTable();
        DataTable dtCandidato = new DataTable();

        int[] PuntosPerfilGeneral = new int[16];
        string[] Factor = new string[16];

        int  totalPositivo, totalNegativo, SumaPositivos, SumaNegativos, suma,
        FactorQ1Positivo, FactorQ2Positivo, FactorQ3Positivo, FactorQ4Positivo,
        FactorQ1Negativo, FactorQ2Negativo, FactorQ3Negativo, FactorQ4Negativo, total = 0;
        int NumeroAsignacion;
        byte Edad, genero;

        int[] PuntosFinal = new int[17];
        double[] PuntosFactorSecundario = new double[5];
        
        public frmReportePF16(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;  
        }

        /// <summary>
        /// Convierten los puntos para el Segundo Factor
        /// </summary>
        public void FactoresSegundoOrden()
        {

            dto_PF16Punto[] modelo_punto = cliente.ListarPuntos(NumeroAsignacion);
            for (int x = 0; x < modelo_punto.Length; ++x)
            {

                //******************************************************************************************** MOTIVACION DISTORCIONAL ********************************************************************************************
                if (modelo_punto[x].IdRespuesta == 10) { total += 1; } if (modelo_punto[x].IdRespuesta == 25) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 73) { total += 1; } if (modelo_punto[x].IdRespuesta == 84) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 111) { total += 1; } if (modelo_punto[x].IdRespuesta == 199) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 223) { total += 1; } if (modelo_punto[x].IdRespuesta == 274) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 283) { total += 1; } if (modelo_punto[x].IdRespuesta == 252) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 336) { total += 1; } if (modelo_punto[x].IdRespuesta == 339) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 453) { total += 1; } if (modelo_punto[x].IdRespuesta == 523) { total += 1; }
                if (modelo_punto[x].IdRespuesta == 510) { total += 1; }
            }
            if (genero == 0)
            {
                //******************************************************************************* ANCIEDAD *************************************************************************************************************************

                if (Factor[0].ToString().Trim() == "A")
                { suma = PuntosFinal[0] * 1; totalPositivo += suma; }
                if (Factor[7].ToString().Trim() == "I")
                { suma = PuntosFinal[7] * 1; totalPositivo += suma; }
                if (Factor[8].ToString().Trim() == "L")
                { suma = PuntosFinal[8] * 2; totalPositivo += suma; }
                if (Factor[11].ToString().Trim() == "O")
                { suma = PuntosFinal[11] * 3; totalPositivo += suma; }
                if (Factor[15].ToString().Trim() == "Q4")
                { suma = PuntosFinal[15] * 3; totalPositivo += suma; }

                if (Factor[2].ToString().Trim() == "C")
                { suma = PuntosFinal[2] * 3; totalNegativo += suma; }
                if (Factor[6].ToString().Trim() == "H")
                { suma = PuntosFinal[6] * 2; totalNegativo += suma; }
                if (Factor[9].ToString().Trim() == "M")
                { suma = PuntosFinal[9] * 1; totalNegativo += suma; }
                if (Factor[14].ToString().Trim() == "Q3")
                { suma = PuntosFinal[14] * 1; totalNegativo += suma; }

                SumaPositivos = totalPositivo + 46;

                FactorQ1Positivo = SumaPositivos; FactorQ1Negativo = totalNegativo;

                totalPositivo = 0; totalNegativo = 0;

                //***************************************************************************** EXTRAVERSION ***************************************************************************************************************************

                if (Factor[0].ToString().Trim() == "A")
                { suma = PuntosFinal[0] * 4; totalPositivo += suma; }
                if (Factor[3].ToString().Trim() == "E")
                { suma = PuntosFinal[3] * 1; totalPositivo += suma; }
                if (Factor[4].ToString().Trim() == "F")
                { suma = PuntosFinal[4] * 3; totalPositivo += suma; }
                if (Factor[5].ToString().Trim() == "G")
                { suma = PuntosFinal[5] * 2; totalPositivo += suma; }
                if (Factor[6].ToString().Trim() == "H")
                { suma = PuntosFinal[6] * 3; totalPositivo += suma; }
                if (Factor[7].ToString().Trim() == "I")
                { suma = PuntosFinal[7] * 1; totalPositivo += suma; }
                if (Factor[8].ToString().Trim() == "L")
                { suma = PuntosFinal[8] * 1; totalPositivo += suma; }
                if (Factor[14].ToString().Trim() == "Q3")
                { suma = PuntosFinal[14] * 1; totalPositivo += suma; }
                if (Factor[15].ToString().Trim() == "Q4")
                { suma = PuntosFinal[15] * 1; totalPositivo += suma; }

                if (Factor[1].ToString().Trim() == "B")
                { suma = PuntosFinal[1] * 1; totalNegativo += suma; }
                if (Factor[2].ToString().Trim() == "C")
                { suma = PuntosFinal[2] * 1; totalNegativo += suma; }
                if (Factor[9].ToString().Trim() == "M")
                { suma = PuntosFinal[9] * 1; totalNegativo += suma; }
                if (Factor[13].ToString().Trim() == "Q2")
                { suma = PuntosFinal[13] * 4; totalNegativo += suma; }

                SumaPositivos = totalPositivo + 2; SumaNegativos = totalNegativo;

                FactorQ2Positivo = SumaPositivos; FactorQ2Negativo = SumaNegativos;

                totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

                //******************************************************************************* SOCIALIZACION CONTROLADA *************************************************************************************************************************

                if (Factor[0].ToString().Trim() == "A")
                { suma = PuntosFinal[0] * 1; totalPositivo += suma; }
                if (Factor[1].ToString().Trim() == "B")
                { suma = PuntosFinal[1] * 3; totalPositivo += suma; }
                if (Factor[5].ToString().Trim() == "G")
                { suma = PuntosFinal[5] * 4; totalPositivo += suma; }
                if (Factor[7].ToString().Trim() == "I")
                { suma = PuntosFinal[7] * 1; totalPositivo += suma; }
                if (Factor[10].ToString().Trim() == "N")
                { suma = PuntosFinal[10] * 4; totalPositivo += suma; }
                if (Factor[14].ToString().Trim() == "Q3")
                { suma = PuntosFinal[14] * 3; totalPositivo += suma; }

                if (Factor[2].ToString().Trim() == "C")
                { suma = PuntosFinal[2] * 1; totalNegativo += suma; }
                if (Factor[3].ToString().Trim() == "E")
                { suma = PuntosFinal[3] * 2; totalNegativo += suma; }
                if (Factor[4].ToString().Trim() == "F")
                { suma = PuntosFinal[4] * 3; totalNegativo += suma; }
                if (Factor[6].ToString().Trim() == "H")
                { suma = PuntosFinal[6] * 1; totalNegativo += suma; }
                if (Factor[9].ToString().Trim() == "M")
                { suma = PuntosFinal[9] * 1; totalNegativo += suma; }
                if (Factor[15].ToString().Trim() == "Q4")
                { suma = PuntosFinal[15] * 1; totalNegativo += suma; }

                SumaPositivos = totalPositivo + 23; SumaNegativos = totalNegativo;

                FactorQ3Positivo = SumaPositivos; FactorQ3Negativo = totalNegativo;

                totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

                //******************************************************************************* INDEPENDENCIA *************************************************************************************************************************

                if (Factor[2].ToString().Trim() == "B")
                { suma = PuntosFinal[2] * 5; totalPositivo += suma; }
                if (Factor[3].ToString().Trim() == "E")
                { suma = PuntosFinal[3] * 3; totalPositivo += suma; }
                if (Factor[8].ToString().Trim() == "L")
                { suma = PuntosFinal[8] * 3; totalPositivo += suma; }
                if (Factor[9].ToString().Trim() == "M")
                { suma = PuntosFinal[9] * 1; totalPositivo += suma; }
                if (Factor[10].ToString().Trim() == "N")
                { suma = PuntosFinal[10] * 1; totalPositivo += suma; }
                if (Factor[12].ToString().Trim() == "Q1")
                { suma = PuntosFinal[12] * 4; totalPositivo += suma; }
                if (Factor[13].ToString().Trim() == "Q2")
                { suma = PuntosFinal[13] * 1; totalPositivo += suma; }

                if (PuntosPerfilGeneral[11].ToString().Trim() == "O")
                { suma = PuntosFinal[11] * 1; totalNegativo += suma; }

                SumaPositivos = totalPositivo; SumaNegativos = totalNegativo + 46;

                FactorQ4Positivo = SumaNegativos; FactorQ4Negativo = SumaNegativos;

                totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

                //*************************************************************** Puntos Para la Grafica de Perfil Secundario (Hombres) **********************************************************************************

                PuntosFactorSecundario[0] = FactorQ1Positivo - FactorQ1Negativo; PuntosFactorSecundario[0] = PuntosFactorSecundario[0] / 10;
                PuntosFactorSecundario[1] = FactorQ2Positivo - FactorQ2Negativo; PuntosFactorSecundario[1] = PuntosFactorSecundario[1] / 10;
                PuntosFactorSecundario[2] = FactorQ3Positivo - FactorQ3Negativo; PuntosFactorSecundario[2] = PuntosFactorSecundario[2] / 10;
                PuntosFactorSecundario[3] = FactorQ4Positivo - FactorQ4Negativo; PuntosFactorSecundario[3] = PuntosFactorSecundario[3] / 10;
                // MessageBox.Show("Q1 " + puntos[0] + " " + "Q2 " + puntos[1] + " " + "Q3 " + puntos[2] + " " + "Q4 " + puntos[3] + " ");
                if (PuntosFactorSecundario[0] >= 10) { PuntosFactorSecundario[0] = 10; } if (PuntosFactorSecundario[0] <= 1) { PuntosFactorSecundario[0] = 1; }
                if (PuntosFactorSecundario[1] >= 10) { PuntosFactorSecundario[1] = 10; } if (PuntosFactorSecundario[1] <= 1) { PuntosFactorSecundario[1] = 1; }
                if (PuntosFactorSecundario[2] >= 10) { PuntosFactorSecundario[2] = 10; } if (PuntosFactorSecundario[2] <= 1) { PuntosFactorSecundario[2] = 1; }
                if (PuntosFactorSecundario[3] >= 10) { PuntosFactorSecundario[3] = 10; } if (PuntosFactorSecundario[3] <= 1) { PuntosFactorSecundario[3] = 1; }

                // MessageBox.Show("Q1 " + puntos[0] + " " + "Q2 " + puntos[1] + " " + "Q3 " + puntos[2] + " " + "Q4 " + puntos[3] + " ");

                PuntosFactorSecundario[0] = Math.Round(PuntosFactorSecundario[0], MidpointRounding.AwayFromZero);
                PuntosFactorSecundario[1] = Math.Round(PuntosFactorSecundario[1], MidpointRounding.AwayFromZero);
                PuntosFactorSecundario[2] = Math.Round(PuntosFactorSecundario[2], MidpointRounding.AwayFromZero);
                PuntosFactorSecundario[3] = Math.Round(PuntosFactorSecundario[3], MidpointRounding.AwayFromZero);
                // MessageBox.Show(PuntosFactorSecundario[0].ToString() + " " + PuntosFactorSecundario[1].ToString() + " " + PuntosFactorSecundario[2].ToString() + " " + PuntosFactorSecundario[3].ToString());     
                return;
                //********************************************************************************************************************************************************************************************************
            }

            //********************************************************************************************************************************************************************************************************

            if (genero == 1)
            {
                //******************************************************************************* ANCIEDAD *************************************************************************************************************************

                if (PuntosPerfilGeneral[5].ToString().Trim() == "G")
                { suma = PuntosFinal[5] * 1; totalPositivo += suma; }
                if (PuntosPerfilGeneral[8].ToString().Trim() == "L")
                { suma = PuntosFinal[8] * 2; totalPositivo += suma; }
                if (PuntosPerfilGeneral[11].ToString().Trim() == "O")
                { suma = PuntosFinal[11] * 3; totalPositivo += suma; }
                if (PuntosPerfilGeneral[15].ToString().Trim() == "Q4")
                { suma = PuntosFinal[15] * 3; totalPositivo += suma; }

                if (PuntosPerfilGeneral[2].ToString().Trim() == "C")
                { suma = PuntosFinal[2] * 3; totalNegativo += suma; }
                if (PuntosPerfilGeneral[6].ToString().Trim() == "H")
                { suma = PuntosFinal[6] * 2; totalNegativo += suma; }
                if (PuntosPerfilGeneral[9].ToString().Trim() == "M")
                { suma = PuntosFinal[9] * 1; totalNegativo += suma; }
                if (PuntosPerfilGeneral[14].ToString().Trim() == "Q3")
                { suma = PuntosFinal[14] * 1; totalNegativo += suma; }

                SumaPositivos = totalPositivo + 51;

                FactorQ1Positivo = SumaPositivos; FactorQ1Negativo = totalNegativo;

                totalPositivo = 0; totalNegativo = 0;
            }
            //***************************************************************************** EXTRAVERSION ***************************************************************************************************************************

            if (PuntosPerfilGeneral[0].ToString().Trim() == "A")
            { suma = PuntosFinal[0] * 5; totalPositivo += suma; }
            if (PuntosPerfilGeneral[3].ToString().Trim() == "E")
            { suma = PuntosFinal[3] * 3; totalPositivo += suma; }
            if (PuntosPerfilGeneral[4].ToString().Trim() == "F")
            { suma = PuntosFinal[4] * 3; totalPositivo += suma; }
            if (PuntosPerfilGeneral[5].ToString().Trim() == "G")
            { suma = PuntosFinal[5] * 1; totalPositivo += suma; }
            if (PuntosPerfilGeneral[6].ToString().Trim() == "H")
            { suma = PuntosFinal[6] * 3; totalPositivo += suma; }
            if (PuntosPerfilGeneral[8].ToString().Trim() == "L")
            { suma = PuntosFinal[8] * 5; totalPositivo += suma; }
            if (PuntosPerfilGeneral[13].ToString().Trim() == "Q2")
            { suma = PuntosFinal[13] * 1; totalPositivo += suma; }
            if (PuntosPerfilGeneral[14].ToString().Trim() == "Q3")
            { suma = PuntosFinal[14] * 1; totalPositivo += suma; }
            if (PuntosPerfilGeneral[15].ToString().Trim() == "Q4")
            { suma = PuntosFinal[15] * 1; totalPositivo += suma; }

            if (PuntosPerfilGeneral[2].ToString().Trim() == "C")
            { suma = PuntosFinal[2] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[7].ToString().Trim() == "I")
            { suma = PuntosFinal[7] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[9].ToString().Trim() == "M")
            { suma = PuntosFinal[9] * 1; totalNegativo += suma; }

            SumaPositivos = totalPositivo; SumaNegativos = totalNegativo + 63;

            FactorQ2Positivo = SumaPositivos; FactorQ2Negativo = SumaNegativos;

            totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

            //******************************************************************************* SOCIALIZACION CONTROLADA *************************************************************************************************************************

            if (PuntosPerfilGeneral[0].ToString().Trim() == "A")
            { suma = PuntosFinal[0] * 1; totalPositivo += suma; }
            if (PuntosPerfilGeneral[1].ToString().Trim() == "B")
            { suma = PuntosFinal[1] * 2; totalPositivo += suma; }
            if (PuntosPerfilGeneral[5].ToString().Trim() == "G")
            { suma = PuntosFinal[5] * 4; totalPositivo += suma; }
            if (PuntosPerfilGeneral[8].ToString().Trim() == "L")
            { suma = PuntosFinal[8] * 1; totalPositivo += suma; }
            if (PuntosPerfilGeneral[10].ToString().Trim() == "N")
            { suma = PuntosFinal[10] * 3; totalPositivo += suma; }
            if (PuntosPerfilGeneral[14].ToString().Trim() == "Q3")
            { suma = PuntosFinal[14] * 3; totalPositivo += suma; }

            if (PuntosPerfilGeneral[2].ToString().Trim() == "C")
            { suma = PuntosFinal[2] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[3].ToString().Trim() == "E")
            { suma = PuntosFinal[3] * 2; totalNegativo += suma; }
            if (PuntosPerfilGeneral[4].ToString().Trim() == "F")
            { suma = PuntosFinal[4] * 2; totalNegativo += suma; }
            if (PuntosPerfilGeneral[6].ToString().Trim() == "H")
            { suma = PuntosFinal[6] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[9].ToString().Trim() == "M")
            { suma = PuntosFinal[9] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[12].ToString().Trim() == "Q1")
            { suma = PuntosFinal[12] * 1; totalNegativo += suma; }
            if (PuntosPerfilGeneral[15].ToString().Trim() == "Q4")
            { suma = PuntosFinal[15] * 1; totalNegativo += suma; }

            SumaPositivos = totalPositivo + 22; SumaNegativos = totalNegativo;

            FactorQ3Positivo = SumaPositivos; FactorQ3Negativo = SumaNegativos;

            totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

            //******************************************************************************* INDEPENDENCIA *************************************************************************************************************************

            if (PuntosPerfilGeneral[1].ToString().Trim() == "B")
            { suma = PuntosFinal[1] * 6; totalPositivo += suma; }
            if (Factor[3].ToString().Trim() == "E")
            { suma = PuntosFinal[3] * 2; totalPositivo += suma; }
            if (Factor[5].ToString().Trim() == "G")
            { suma = PuntosFinal[5] * 1; totalPositivo += suma; }
            if (Factor[7].ToString().Trim() == "I")
            { suma = PuntosFinal[7] * 1; totalPositivo += suma; }
            if (Factor[8].ToString().Trim() == "L")
            { suma = PuntosFinal[8] * 1; totalPositivo += suma; }
            if (Factor[9].ToString().Trim() == "M")
            { suma = PuntosFinal[9] * 3; totalPositivo += suma; }
            if (Factor[12].ToString().Trim() == "Q1")
            { suma = PuntosFinal[12] * 4; totalPositivo += suma; }
            if (Factor[14].ToString().Trim() == "Q3")
            { suma = PuntosFinal[14] * 1; totalPositivo += suma; }

            if (Factor[0].ToString().Trim() == "A")
            { suma = PuntosFinal[0] * 2; totalNegativo += suma; }
            if (Factor[10].ToString().Trim() == "N")
            { suma = PuntosFinal[10] * 1; totalNegativo += suma; }
            if (Factor[13].ToString().Trim() == "Q2")
            { suma = PuntosFinal[13] * 1; totalNegativo += suma; }

            SumaPositivos = totalPositivo; SumaNegativos = totalNegativo + 31;

            FactorQ4Positivo = SumaPositivos; FactorQ4Negativo = SumaNegativos;

            totalPositivo = 0; totalNegativo = 0; SumaPositivos = 0; SumaNegativos = 0;

            //*************************************************************** Puntos Para la Grafica de Perfil Secundario (Mujeres) **********************************************************************************

            PuntosFactorSecundario[0] = FactorQ1Positivo - FactorQ1Negativo; PuntosFactorSecundario[0] = PuntosFactorSecundario[0] / 10;
            PuntosFactorSecundario[1] = FactorQ1Positivo - FactorQ1Negativo; PuntosFactorSecundario[1] = PuntosFactorSecundario[1] / 10;
            PuntosFactorSecundario[2] = FactorQ1Positivo - FactorQ1Negativo; PuntosFactorSecundario[2] = PuntosFactorSecundario[2] / 10;
            PuntosFactorSecundario[3] = FactorQ1Positivo - FactorQ1Negativo; PuntosFactorSecundario[3] = PuntosFactorSecundario[3] / 10;
            // MessageBox.Show("Q1 " + puntos[0] + " " + "Q2 " + puntos[1] + " " + "Q3 " + puntos[2] + " " + "Q4 " + puntos[3] + " ");
            if (PuntosFactorSecundario[0] >= 10) { PuntosFactorSecundario[0] = 10; } if (PuntosFactorSecundario[0] <= 1) { PuntosFactorSecundario[0] = 1; }
            if (PuntosFactorSecundario[1] >= 10) { PuntosFactorSecundario[1] = 10; } if (PuntosFactorSecundario[1] <= 1) { PuntosFactorSecundario[1] = 1; }
            if (PuntosFactorSecundario[2] >= 10) { PuntosFactorSecundario[2] = 10; } if (PuntosFactorSecundario[2] <= 1) { PuntosFactorSecundario[2] = 1; }
            if (PuntosFactorSecundario[3] >= 10) { PuntosFactorSecundario[3] = 10; } if (PuntosFactorSecundario[3] <= 1) { PuntosFactorSecundario[3] = 1; }

            //MessageBox.Show("Q1 " + puntos[0] + " " + "Q2 " + puntos[1] + " " + "Q3 " + puntos[2] + " " + "Q4 " + puntos[3] + " ");

            PuntosFactorSecundario[0] = Math.Round(PuntosFactorSecundario[0], MidpointRounding.AwayFromZero);
            PuntosFactorSecundario[1] = Math.Round(PuntosFactorSecundario[1], MidpointRounding.AwayFromZero);
            PuntosFactorSecundario[2] = Math.Round(PuntosFactorSecundario[2], MidpointRounding.AwayFromZero);
            PuntosFactorSecundario[3] = Math.Round(PuntosFactorSecundario[3], MidpointRounding.AwayFromZero);
            //MessageBox.Show(PuntosFactorSecundario[0].ToString() + " " + PuntosFactorSecundario[1].ToString() + " " + PuntosFactorSecundario[2].ToString() + " " + PuntosFactorSecundario[3].ToString());     

            //********************************************************************************************************************************************************************************************************
        }

        /// <summary>
        /// Listar los Puntos de la Tabla Puntos Generos
        /// </summary>
        public void CargarPuntos()
        {
            dto_PF16Punto[] puntos_sumas = cliente.PuntosRetornadosPF16(NumeroAsignacion);
            dto_CargarPuntosGeneroPF16[] puntos_genero = cliente.CargarPuntosGeneroPF16();

            for (int x = 0; x < puntos_sumas.Length; ++x)
            {
                PuntosPerfilGeneral[x] = puntos_sumas[x].Puntos;
                Factor[x] = puntos_sumas[x].Factor;
                //MessageBox.Show( "Numero de Fator : "+ x +" Factor: " + puntos_sumas[x].Factor +"  " + " Puntos: "+puntos_sumas[x].Puntos);
                for (int i = 0; i < puntos_genero.Length; i++)
                {
                    if (puntos_genero[i].Genero == genero &&
                        puntos_genero[i].Factor.Trim() == puntos_sumas[x].Factor.Trim() &&
                        puntos_sumas[x].Puntos >= puntos_genero[i].Rango_Punto1 &&
                        puntos_sumas[x].Puntos <= puntos_genero[i].Rango_Punto2 &&
                        Edad >= puntos_genero[i].Rango_Edad1 &&
                        Edad <= puntos_genero[i].Rango_Edad2)
                    {
                        PuntosFinal[x] = puntos_genero[i].Punto;
                        //MessageBox.Show(" Factor: " + puntos_sumas[x].Factor + "  " +"Puntos Convertidos: " + PuntosFinal[x] + " " + x);
                    }
                }
            }
        }

        /// <summary>
        /// Descripcion de los puntos generados segun el rango de calificacion
        /// </summary>
        public void Resultado()
        {
            data.Columns.Add("Descripcion1"); data.Columns.Add("Descripcion2"); data.Columns.Add("Descripcion3");
            data.Columns.Add("Descripcion4"); data.Columns.Add("Descripcion5"); data.Columns.Add("Descripcion6");
            data.Columns.Add("Descripcion7"); data.Columns.Add("Descripcion8"); data.Columns.Add("Descripcion9");
            data.Columns.Add("Descripcion10"); data.Columns.Add("Descripcion11"); data.Columns.Add("Descripcion12");
            data.Columns.Add("Descripcion13"); data.Columns.Add("Descripcion14"); data.Columns.Add("Descripcion15");
            data.Columns.Add("Descripcion16"); data.Columns.Add("Puntos");

            DataRow rows = data.NewRow();

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[0] <= 3)
            {
                rows["Descripcion1"] = "Persona fría, impersonal, distante, rígida, centrada en las cosas,  crítica, escéptica, bajo nivel de empatía, inflexible, desconfiada, cerrada, hostil, egoísta, impersonal. Afecto algo aplanado. Poco afectuoso. " +
                " Suele ser reservada, cauta en sus implicaciones y contactos; le suele gustar el trabajo en solitario, y a menudo éste es de tipo mecánico, intelectual y artístico, no busca ni necesita del contacto interpersonal. " +
                " Puede sentirse poco confortable en situaciones en las que hay mucha relación personal. " +
                " Óptimo para el manejo de computadores, área contable, programador. ";
            }
            else if (PuntosFinal[0] >= 8)
            {
                rows["Descripcion1"] = "Persona afable, cálida, generosa, atenta a los demás, simpática, colaboradora, reposada, adaptable,  abierta, amable, tratable,    emocionalmente expresiva, de buen genio,   y gusta de los trabajos en equipo." +
               " Suele tener más interés en las personas y preferir ocupaciones en las que intervengan personas aunque no es necesariamente social. Se siente confortable en situaciones que demanden cercanía personal. Poco temeroso de las críticas y capaz de recordar los nombres de personas. " +
               " Puede representar una dependencia extrema de personas y de relaciones íntimas.";
            }
            else
            {
                rows["Descripcion1"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[1] <= 3)
            {
                rows["Descripcion2"] = "Persona de pensamiento concreto, lenta para captar y analizar, no transfiere información, le cuesta trabajo concluir, falta de criterio, se da por vencido fácilmente, demorado para aprender, burda. Ha de presentar bajo rendimiento. " +
                " Juicio deficiente, moral baja, e inconstante. " +
                " Puede indicar mala comprensión,  incorrecta interpretación o falta de atención.";
            }
            else if (PuntosFinal[1] >= 8)
            {
                rows["Descripcion2"] = "Persona de pensamiento abstracto, inteligente,  tiende a ser rápida en la comprensión y aprendizaje de las ideas, ágil para reconocer la solución de un problema, creativa  y gusta de sacar conclusiones. " +
                " Juicio superior, moral alta, y perseverante.";
            }
            else
            {
                 rows["Descripcion2"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[2] <= 3)
            {
                rows["Descripcion3"] = "Persona reactiva y emocionalmente cambiable, se frustra fácilmente, negativa, se victimiza, evade responsabilidades, con resentimiento hacia a los demás, incapaz de luchar, inconformista. " +
                " Cuando las condiciones no son satisfactorias es voluble, plástica, neurótica fatigada, displicente, de emoción y turbación fácil, activa cuando se encuentra insatisfecha, presenta síntomas neuróticos; fobias alteraciones del sueño, quejas psicosomáticas y tiende a darse por vencida. " +
                " Experimenta una cierta falta de control sobre su vida, tiene más altibajos de humor que la mayoría,  admite que sus necesidades emocionales no están satisfechas y que se siente como si no pudiera controlar el que pequeñas cosas le salgan mal. ";
            }
            else if (PuntosFinal[2] >= 8)
            {
                rows["Descripcion3"] = "Persona emocionalmente estable, adaptada, madura, con alto nivel de tolerancia a la frustración, capaz de controlar emociones, afronta la realidad, capacidad de mantener una sólida moral de grupo, alta energía, ecuánime. Ante una gran pérdida se repone fácilmente, más prospera, no se desanima, tranquila, evita conflictos, firme, de fuerza interior, con capacidad humana, auto responsable, culmina proyectos, constante en sus intereses. " +
                " Suele ir controlando con equilibrio y de modo adaptativo los sucesos y emociones, sin embargo puede indicar que no es nada dado a informar, y tampoco a experimentar sentimientos problemáticos. " +
                " En raras ocasiones se encuentra con un problema que no pueda afrontar y se recobra fácilmente de los contratiempos. ";
            }
            else
            {
                rows["Descripcion3"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[3] <= 3)
            {
                rows["Descripcion4"] = "Persona deferente, cooperativa, que evita los conflictos, manejable, humilde, convencional, conformista, apacible, sumisa, insegura,  dócil, acomodaticia, retraída,  gusta ser supervisada y mandada, considerada y cuidadosa con los demás, es efectivo amenazarle con la autoridad, constantemente se justifica, es orientada al cliente, servicial, no apto para formar equipo. Necesita relacionarse con otro para el manejo de la autoridad, normas y valores. " +
                " Tendencia a acomodarse a los deseos de los demás, a evitar el conflicto asintiendo a los deseos de los otros y pospone sus sentimientos y deseos. En el extremo puede alienar a quien desea una postura esforzada y participativa. " +
                " Suele ser más cooperativa que asertiva y reconoce que deja pasar el tema si alguien hace algo que le molesta o estorba.";
            }
            else if (PuntosFinal[3] >= 8)
            {
                rows["Descripcion4"] = "Persona dominante, asertiva, competitiva, seria, solemne, rebelde, poco convencional, obstinada, de ideas posesivas, fuerte, puede manejar la afectividad; sirve cuando  hay que transformar o imponer una orden con ideas de dominancia, son competitivas, le gusta que la alaben; adecuada para emergencias o situaciones críticas. Agresiva en la búsqueda de metas, dogmática, segura de sí misma, de mentalidad independiente, austera, autorreguladora, puede hacer caso omiso de la autoridad. " +
                " Tendencia a ejercer la voluntad de uno mismo frente a la de los demás, suelen mostrarse esforzadas en manifestar sus deseos y opiniones y en conseguir lo, que quieren. En el extremo puede llegar a alienar a aquellos que no desean ser subyugados. " +
                " Se siente confortable impartiendo instrucciones a la gente y reconoce que expresa claramente si considera erróneo el razonamiento de otra persona.";
            }
            else
            {
                rows["Descripcion4"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[4] <= 3)
            {
                rows["Descripcion5"] = "Persona seria, reprimida, terca,  cuidadosa, ligada a los hábitos,  sobria, prudente, rígida,  taciturna, calmada, analítica, introspectiva, terca, predispuesta a ver peligros, precavida, lenta pero segura para tomar decisiones, reflexiva, adecuadas para situaciones donde hay alto error de accidentalidad, obstinada,  deprimida,  pesimista, presumida, estiradamente correcta digna de confianza, cautelosa, se apega a sus valores personales. " +
                " Tiende a tomar la vida más seriamente; es más callada o apagada, cuidadosa y menos dada a la diversión; tiende a inhibir su espontaneidad , a veces hasta el punto de parecer reprimida y " +
                " Circunspecta. " +
                " Prefiere estar ocupada en una tarea tranquila.";
            }
            else if (PuntosFinal[4] >= 8)
            {
                rows["Descripcion5"] = "Persona animosa, espontánea, activa, entusiasta, impulsiva, jovial, franca, expresiva, descuidada, confiada a la buena ventura, charlatán, ambiciosa y rápida. Frecuentemente se le acoge como líder electa, de reacción inmediata, no analiza, actúa, rápida para hablar, demasiado confiada, de actividad imprevisible o cambiante. " +
                " En situaciones extremas puede reflejar un aspecto caprichoso considerado como inmaduro y poco fiable. " +
                " Le gusta estar en medio de mucha actividad y excitación, estar a la moda y emplear una tarde en una fiesta con sus amigos. " +
                " Óptima para ser digitadora, secretaria, operativa.";
            }
            else
            {
                rows["Descripcion5"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[5] <= 3)
            {
                rows["Descripcion6"] = "Persona inconformista, muy suya, indulgente, despreocupada, acepta pocas obligaciones, suele ser inestable en sus propósitos, libre, voluble, frívola, indulgente consigo misma,  sus acciones son casuales y faltas de atención a los compromisos del grupo y a las exigencias culturales, se tolera fallar, no siente la necesidad de cumplirse a sí misma, siente placer por lo que hace pero cuando la  motivación baja cambia de actividad y hace lo que quiere, se guía por sus y impulsos no tiene normas. Podría sembrar a su alrededor el desorden y el desaliño. " +
                " Suele esquivar las reglas y puede tener dificultades para ajustarse a normas estrictas, baja fuerza del súper yo y poco confiable. " +
                " Su comportamiento inconformista parece implicar la necesidad de autonomía, la necesidad de dejar lo serio y la necesidad de flexibilidad. " +
                " Dice que la mayoría de las normas se han hecho para no cumplirlas cuando haya buenas razones para ello, y que ser libre para hacer lo que se desee es más importante que tener buenos modales y respetar las normas.";
            }
            else if (PuntosFinal[5] >= 8)
            {
                rows["Descripcion6"] = "Persona atenta a las normas, cumplidora,  formal, disciplinada, estable, de carácter exigente, responsable, organizada, moralista, dominada por el sentido del deber,  perseverante, religiosa, de estamentos militares, y recursiva. " +
                " Seguidora de las reglas, los principios y los buenos modales, puede ser considerada  sobria, inflexible o rígida consigo misma." +
                " Antes de tomar una decisión siempre piensa cuidadosamente en lo que es correcto y justo, y le da más valor y respeto a las normas y buenas maneras que a ser libre de hacer lo que se desee. " +
                " Posee mucha fuerza del súper yo, necesita de poca supervisión, es constante en la tarea, y de mayor control interno.";
            }
            else
            {
                rows["Descripcion6"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[6] <= 3)
            {
                rows["Descripcion7"] = "Persona tímida, temerosa, cohibida, reprimida, falta de confianza en sí misma, baja autoeficacia, alejada, cautelosa, retraída, al margen de la actividad social, puede presentar sentimientos de inferioridad, lenta y torpe para hablar. " +
                " Tiende a ser socialmente cauta y vergonzosa; le resulta difícil hablar frente a un grupo de gente y puede representar una falta de autoestima. Se intimida fácilmente, tiende a la introversión, posee alta concentración, sus  áreas de interés son reducidas, siente miedo a avanzar, encerrada en sí misma, sensible, y sujeta a reglas. " +
                "Óptima para ser: auxiliar contable, sistemas, visado.";
            }
            else if (PuntosFinal[6] >= 8)
            {
                rows["Descripcion7"] = "Persona atrevida, segura en lo social,  emprendedora, espontánea, sociable, abierta, lanzada  y dispersa,   dispuesta a intentar nuevas cosas, de numerosas respuestas emocionales, capaz de manejar clientes. Tiende a la extroversión. Soporta situaciones sociales de presión y abrumadoras. " +
                " Suele iniciar los contactos sociales y no es tímida cuando se encuentra en un ambiente nuevo y rodeado de gente. Tiene habilidad para relacionarse con el sexo opuesto, no valora adecuadamente las situaciones peligrosas, es confiada, conversadora, se impacta menos por los problemas. " +
                " Óptima para ser: vendedor,  líder, gerente, relaciones públicas.";
            }
            else
            {
                rows["Descripcion7"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************


            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[7] <= 3)
            {
                rows["Descripcion8"] = "Persona objetiva, nada sentimental, autoritaria, sensibilidad dura, confiada en sí misma, realista, práctica, independiente, responsable, emocionalmente madura,  escéptica de relaciones culturales subjetivas, inamovible, mantiene al grupo trabajando sobre bases realistas y acertadas. Evaluativa, insensible, no se transa por nada, actúa basado en evidencias prácticas y lógicas. " +
                " Suele tener un enfoque muy utilitario, muestra menos sentimiento y atiende más a lo operativo de las cosas del trabajo.";
            }
            else if (PuntosFinal[7] >= 8)
            {
                rows["Descripcion8"] = "Persona sensible, esteta, sentimental, demandante de atención y afecto. Suele dejarse afectar por los sentimientos, idealista, soñadora, impaciente, dependiente, poco práctica, insegura,  le disgustan las profesiones y acciones rudas. Suele frenar la acción del grupo y turbar su moral con actividades inútiles e idealistas. " +
                " Tiende a basar sus juicios en gustos personales y valores estéticos. " +
                " Se apoya en la empatía y en la sensibilidad a la hora de hacer consideraciones y suele ser más refinada en sus intereses y gustos.";
            }
            else
            {
                rows["Descripcion8"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[8] <= 3)
            {
                rows["Descripcion9"] = "Persona confiada, sin sospechas,  adaptable al cambio, no suele presentar tendencia a los celos o envidia, animada, poco competitiva, buena colaboradora de grupo, perdonadora, no rencorosa, cree en la gente, tranquila en las relaciones interpersonales, prefiere dejar que los demás sean como quieran, tiende a ver lo bueno de todo, persona vivible, crea un mejor ambiente, no conflictiva. " +
                " Suele esperar un tratamiento justo y leal, y buenas intenciones de los demás.";
            }
            else if (PuntosFinal[8] >= 8)
            {
                rows["Descripcion9"] = "Persona, celosa, desconfiada,  suspicaz, escéptica, precavida, vigilante, acusadora, generadora de culpa, está pendiente de caerle al otro, rencorosa, generadora de conflictos, señal de peligro en todas partes, actúa con premeditación. " +
                " Pésimo para el grupo ya que daña el clima. Antijefe, reacio al cambio, tiránico  y centrado en si mismo. " +
                " Incapaz de relajar su vigilancia en situaciones en que  le sería apropiado hacerlo y está siempre a la guardia (solo se tolera alto para escoltas). " +
                " Óptimo para ser: vigilante.";
            }
            else
            {
                rows["Descripcion9"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[9] <= 3)
            {
                rows["Descripcion10"] = "Persona práctica, con los pies en la tierra,  realista, cuidadosa, convencional, ansiosa por hacer las cosas correctamente, preocupada por los detalles, capaz de serenidad en las situaciones de emergencia, aunque a veces es poco imaginativa, trabaja en el aquí y el ahora centrada en lo que tiene que hacer, control total de su trabajo, cuidadosa, interesada o preocupada, pero estable.` " +
                " Motivada por resultados inmediatos, evita metas lejanas, confía en juicios prácticos. " +
                " Óptima para ser: secretaria.";
            }
            else if (PuntosFinal[9] >= 8)
            {
                rows["Descripcion10"] = "Persona abstraída, imaginativa, idealista, centrada en sus necesidades íntimas, poco convencional, despreocupada de lo cotidiano, bohemia, motivada por sí misma, creadora, preocupada por lo esencial, despreocupada de personas particulares, y de la realidad física. Sus intereses se dirigen hacia su intimidad, su individualidad la empuja a verse excluida de las actividades del grupo. Generalmente entusiasmada, pero con ocasionales cambios histéricos de abandono. " +
                " Ideal para cargos de planeación, creativos y asesores, no compagina con lo comercial, no se le puede dar  nada concreto.";
            }
            else
            {
                rows["Descripcion10"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[10] <= 3)
            {
                rows["Descripcion11"] = "Persona abierta, genuina, llana, natural, sencilla, sentimental, poco sofisticada, se satisface fácilmente, espontánea, poco refinada, torpe, burda en sus expresiones, ordinaria, de mente imprecisa, poco juiciosa, gregaria, se implica calurosa y afectivamente, poco hábil para analizar motivos, se conforma con lo que encuentra. Hace y dice las cosas sin pensar en sus consecuencias, socialmente desinhibida, ingenua, no brillante, autentica. ";
            }
            else if (PuntosFinal[10] >= 8)
            {
                rows["Descripcion11"] = "Persona privada, astuta,  perspicaz,  mundana,  calculadora, discreta, no se abre, atenta al manejo de lo social, capacidad diplomática, cuidadosa de no hacer daño, maneja su imagen, piensa lo que dice, respetuosa del otro, de mente exacta, ambiciosa, se guarda sus problemas, difícil para hablar de temas personales. Su enfoque es intelectual y poco sentimental, se aproxima a las situaciones cínicamente, planea todos sus actos, inteligente socialmente. Capaz de ponerse en el lugar del otro en lo social, empatía  social. " +
                " Óptima para: el área financiera,  el contacto con el público, y empresa de servicios.";
            }
            else
            {
                rows["Descripcion11"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[11] <= 3)
            {
                rows["Descripcion12"] = "Persona segura, despreocupada, satisfecha, apacible, flexible, de ánimo invariable, alta autoestima y autoeficacia, madura, poco ansiosa, puede mostrarse insensible cuando el grupo no va desacuerdo con ella lo que puede provocar antipatías y recelos, sin temores, entregada a la acción espontánea, alegre, serena,  y animada,  tranquila, no amenazarle, sin preocupaciones, soporta la presión, no sufre por sus errores,  baja culpabilidad, confía en lo que sabe y puede controlar, actúa con propiedad. " +
                " Puede generar bajo rendimiento laboral.";
            }
            else if (PuntosFinal[11] >= 8)
            {
                rows["Descripcion12"] = "Persona aprensiva, insegura, autocrítica y preocupada, se autocastiga, puede cumplir pero sufriendo o llorando. Hace todo por castigo, por miedo, por culpa, hipocondríacas, fóbicas, trabajan bien pero son depresivas, llenas de presagios e ideas largamente gestadas, con tendencia infantil a la ansiedad. En los grupos no se siente aceptada ni con libertad para actuar, dificultad para cambiar, es productiva pero a un costo alto, se preocupa por los cambios en su estado de ánimo y se responsabiliza por las cosas que suceden. " +
                " Factor clínico. " +
                " Drogadictos, alcohólicos.";
            }
            else
            {
                rows["Descripcion12"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[12] <= 3)
            {
                rows["Descripcion13"] = "Persona tradicional apegada a lo familiar, conservadora, moderada, no imaginativa, conformista, satisfecha de si y de todo,  respetuosa de las ideas establecidas, tolerante de los defectos tradicionales, precavida, puntillosa con las nuevas ideas, tiende a posponer u oponerse a los cambios, prefiere lo predictible, confía en lo que le han enseñado a creer y acepta lo conocido y verdadero a pesar de sus inconsistencias, tiende a ser conservadora en religión y política y a despreocuparse de las ideas analíticas e intelectuales.";
            }
            else if (PuntosFinal[12] >= 8)
            {
                rows["Descripcion13"] = "Persona abierta al cambio, experimental, liberal, analítica, radical,  crítica, experimental,  cuestionadora de las instituciones, acepta nuevas ideas, rebelde sin causa, no actúa, complicada en la empresa pero no será revolucionaria. Se interesa por cuestiones intelectuales y duda de los principios fundamentales, es escéptica, suele estar informada, poco inclinada a moralizar y más a preguntarse por la vida en general y a ser más tolerante con las molestias y el cambio, coherente con sus actos, puede crear problemas con sus jefes, pero tiene actitudes de innovación, y acepta modificación en sus valores. ";
            }
            else
            {
                rows["Descripcion13"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[13] <= 3)
            {
                rows["Descripcion14"] = "Persona seguidora, buena compañera,  se integra en el grupo con facilidad,  dependiente, prefiere trabajar y tomar decisiones con los demás, indecisas, gusta y depende de la aprobación social, puede ser negativa pues es influenciable. Tiende a seguir las directrices del grupo, incluso mostrando falta de decisiones personales porque necesita del apoyo del mismo, pero puede ser líder porque consulta a su equipo de trabajo. Puede no resultar muy efectiva en las situaciones en las que no es posible tener ayuda o cuando los demás solo ofrecen una dirección o consejos muy pobres.";
            }
            else if (PuntosFinal[13] >= 8)
            {
                rows["Descripcion14"] = "Persona, individualista, autosuficiente, llena de recursos,  prefiere tomar sus propias decisiones, prefiere trabajar sola, se aísla, buena para la creatividad, temperamentalmente independiente. Está acostumbrada a seguir su propio camino, no le disgusta la gente, simplemente no necesita de su asentimiento y apoyo,  no tiene en consideración la opinión del grupo, aunque no es necesariamente dominante en su relación con los demás. No delega mucho, es  recursiva para utilizar en forma práctica y eficiente lo que tiene, el grupo no la presiona fácilmente y es introvertida. Puede tener dificultades para trabajar con otras personas, le cuesta pedir ayuda cuando la necesita, y puede pasar por alto los aspectos interpersonales y las consecuencias de sus acciones.";
            }
            else
            {
                rows["Descripcion14"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[14] <= 3)
            {
                rows["Descripcion15"] = "Persona flexible y tolerante con el desorden y faltas, auto conflictiva,  perezosa, poco manejo de lo social, no se preocupa por aceptar o ceñirse a las exigencias sociales. No es excesivamente considerada, cuidadosa o esmerada. Puede sentirse desajustada, no tiene conciencia de la imagen, desaliñada, poco controlada, de carácter disparejo, no respetuosa con otros, explosiva, despreocupada de protocolos. Fuertemente inestable, bajo control y baja autoimagen, no le interesa verse bien cuando está sola. Sin metas claras, sigue impulsos de momento, sus necesidades son sus normas de conducta.";
            }
            else if (PuntosFinal[14] >= 8)
            {
                rows["Descripcion15"] = "Persona perfeccionista, organizada, disciplinada, preocupada por la expresión de sus emociones y control de su conducta, socialmente adaptada, llevada por su propia imagen, cuidadosa, abierta a lo social, tiene en cuenta la reputación social, controla la expresión de ansiedad, laboralmente apetecida, buen predictor de desempeño. Ciñe su actuación a metas personales, persiste en alcanzarlas, cuidadosa en sus relaciones. Le gusta verse bien cuando está sola, autocontrol alto. Considerada con otros, a veces obstinada y  evidencia el respeto por sí misma.";
            }
            else
            {
                rows["Descripcion15"] = "La puntuacion obtenida en este factor representa un valor promedio";
            }
            //****************************************************************************************************************************************************************************************************************************************

            //****************************************************************************************************************************************************************************************************************************************
            if (PuntosFinal[15] <= 3)
            {
                rows["Descripcion16"] = "Persona relajada, plácida,  tranquila, paciente, sosegada, calmada,  satisfecha,  no se frustra con rapidez, y le resulta fácil ser paciente con la gente. En algunas situaciones su estado de mucha satisfacción puede llevarlo al bajo rendimiento, en el sentido que no tiene motivación para intentar algo.";
            }
            else if (PuntosFinal[15] >= 8)
            {
                rows["Descripcion16"] = "Persona tensa, enérgica, impaciente, intranquila, excitable, irritable, incapaz de mantenerse inactiva, bajo nivel de cohesión grupal, suele manifestar una energía incansable y mostrarse intranquila cuando tiene que esperar. Dentro del grupo tiene una pobre visión del grado de cohesión, del orden y el mando. Puede bloquear el desempeño con sus miedos situacionales y fobias.";
            }
            else
            {
                rows["Descripcion16"] = "La puntuacion obtenida en este factor representa un valor promedio";

            }
            //****************************************************************************************************************************************************************************************************************************************

            rows["Puntos"] = total;
            data.Rows.Add(rows);

        }

        private void frmReporteExpediente_Load(object sender, EventArgs e)
        {

            NumeroAsignacion = _expediente.Id;
            Edad = _expediente.Candidato.Edad;
            genero = (byte)_expediente.Candidato.Genero;
            CargarPuntos();
            FactoresSegundoOrden();
            

            data.Columns.Add("Factores");
            data.Columns.Add("PerfilGeneral");

            data.Columns.Add("Factor");
            data.Columns.Add("SegundoOrden");
            data.Columns.Add("DT");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");
            //MessageBox.Show(PuntosFinal[10].ToString());;
            //MessageBox.Show(modelo_punto[0].Factor + " "+ modelo_punto[0].Puntos);

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);


            for (int z = 0; z < 4; ++z)
            {
                DataRow row = data.NewRow();
                row["Factor"] = FactorSegundoOrden[z];
                row["SegundoOrden"] = PuntosFactorSecundario[z];
                row["DT"] = total;
                data.Rows.Add(row);
            }
            for (int x = 0; x < 16; x++)
            {
                DataRow row = data.NewRow();
                row["Factores"] = FactoresGeneral[x];
                row["PerfilGeneral"] = PuntosFinal[x];
                data.Rows.Add(row);
            }
            ReportDataSource reporte = new ReportDataSource("dtPF16", data);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            rvExpediente.LocalReport.DataSources.Clear();

            rvExpediente.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.PF16.ReportePF16.rdlc";
            rvExpediente.LocalReport.DataSources.Add(reporte);
            rvExpediente.LocalReport.DataSources.Add(rds_candidato);
            this.rvExpediente.RefreshReport();
        }
    }
}