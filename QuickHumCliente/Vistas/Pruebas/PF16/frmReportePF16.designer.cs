﻿namespace QuickHumCliente.Vistas.Pruebas.PF16
{
    partial class frmReportePF16
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rvExpediente = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rvExpediente
            // 
            this.rvExpediente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvExpediente.Location = new System.Drawing.Point(0, 0);
            this.rvExpediente.Name = "rvExpediente";
            this.rvExpediente.Size = new System.Drawing.Size(998, 495);
            this.rvExpediente.TabIndex = 0;
            // 
            // frmReportePF16
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 495);
            this.Controls.Add(this.rvExpediente);
            this.Name = "frmReportePF16";
            this.Text = "Reporte Expediente";
            this.Load += new System.EventHandler(this.frmReporteExpediente_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvExpediente;
    }
}