﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Cleaver
{
    public partial class frmCleaverreporte : Form
    {
        public frmCleaverreporte(Expediente expediente)
        {
            InitializeComponent();
            this._expediente = expediente;
        }

        private readonly QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        readonly DataTable dtresultado = new DataTable();
        readonly DataTable dtgrafica = new DataTable();
        readonly DataTable dtinterpretacion = new DataTable();
        DataTable dtCandidato = new DataTable();

        int[] suma = new int[6];
        int td = 0, percentilt = 0, contgrafica = 0, percentilM = 0, percentilL = 0;
        string orden;
        string expresion;
        int interdt = 0, interdl = 0, interdm = 0, interit = 0, interil = 0, interim = 0, intercl = 0, intercm = 0, interct = 0, interst = 0, intersl = 0, intersm = 0;
        string perdt, perdl, perdm, perit, peril, perim, percl, percm, perct, persm, perst, persl, rdt, rdm, rdl, rit, rim, ril, rst, rsm, rsl, rct, rcm, rcl;
        DataRow[] filtro;
        DataRow[] filtros;
        private void frmCleaverreporte_Load(object sender, EventArgs e)
        {
            dto_cleaver_grafica[] grafica = cliente.ListarGraficaCleaver();
            dto_cleaver_interpretacion[] inter = cliente.ListaCleaverinterpretacion();
            dtgrafica.Columns.Add("idgrafica");
            dtgrafica.Columns.Add("idfactor");
            dtgrafica.Columns.Add("punto");
            dtgrafica.Columns.Add("grafico");
            dtgrafica.Columns.Add("resultado");
            dtgrafica.Columns.Add("idinterpretacion");
         
            dtinterpretacion.Columns.Add("idinterpretacion");
            dtinterpretacion.Columns.Add("interpretacion");
            dtinterpretacion.Columns.Add("rango");
            dtinterpretacion.Columns.Add("combinacion");

            dtresultado.Columns.Add("factor");
            dtresultado.Columns.Add("M");
            dtresultado.Columns.Add("L");
            dtresultado.Columns.Add("orden");
            dtresultado.Columns.Add("puntosT");
            dtresultado.Columns.Add("percentilT");
            dtresultado.Columns.Add("percentilM");
            dtresultado.Columns.Add("percentilL");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            for(int d=0; d <grafica.ToList().Count;d++)
            {
                DataRow fila = dtgrafica.NewRow();
                fila["idgrafica"]=Convert.ToInt32(grafica[d].Idgrafica);
                fila["idfactor"] = Convert.ToInt32(grafica[d].Idfactor);
                fila["resultado"] = Convert.ToString(grafica[d].Resultado);
                fila["punto"] = Convert.ToInt32(grafica[d].Punto);
                fila["grafico"] = Convert.ToInt32(grafica[d].Grafico);
                fila["idinterpretacion"] = Convert.ToInt32(grafica[d].Idinterpretacion);
                dtgrafica.Rows.Add(fila);
            }
           // contgrafica = dtgrafica.Rows.Count;

            for (int h = 0; h < inter.ToList().Count;h++)
            {
                DataRow row = dtinterpretacion.NewRow();
                row["idinterpretacion"] = Convert.ToInt32(inter[h].Idinterpretacion);
                row["interpretacion"] = Convert.ToString(inter[h].Interpretacion);
                row["rango"] = Convert.ToString(inter[h].Rango);
                row["combinacion"] = Convert.ToString(inter[h].Combinacion);
                dtinterpretacion.Rows.Add(row);
            }
            contgrafica = dtinterpretacion.Rows.Count;

            GenerarReporte();
        }

        private void GenerarReporte()
        {
            dtresultado.Clear();
            dto_cleaver_resultados[] reporte = cliente.ListaCleaverResultados(this._expediente.Id);
            for (int i = 0; i < reporte.ToList().Count; i++)
            {
                DataRow row = dtresultado.NewRow();

                switch (reporte[i].Factor)
                {
                    case "D":
                        td = reporte[i].SumaM - reporte[i].SumaL;
                        orden = "A";

                        expresion = "idfactor= 1 and resultado='T' and punto= '" + td + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilt = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interdt = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interdt + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perdt = (filtros[0]["interpretacion"].ToString());
                        rdt = filtros[0]["rango"].ToString();


                        expresion = "idfactor= 1 and resultado='M' and punto= '" + reporte[i].SumaM + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilM = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interdm = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interdm + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perdm = (filtros[0]["interpretacion"].ToString());
                        rdm = filtros[0]["rango"].ToString();


                        expresion = "idfactor= 1 and resultado='L' and punto= '" + reporte[i].SumaL + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilL = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interdl = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interdl + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perdl = (filtros[0]["interpretacion"].ToString());
                        rdl = filtros[0]["rango"].ToString();

                        break;
                    case "I":
                        td = reporte[i].SumaM - reporte[i].SumaL;
                        orden = "B";
                        expresion = "idfactor= 2 and resultado='T' and punto= '" + td + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilt = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interit = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interit + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perit = (filtros[0]["interpretacion"].ToString());
                        rit = filtros[0]["rango"].ToString();


                        expresion = "idfactor= 2 and resultado='M' and punto= '" + reporte[i].SumaM + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilM = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interim = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interim + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perim = (filtros[0]["interpretacion"].ToString());
                        rim = filtros[0]["rango"].ToString();

                        expresion = "idfactor= 2 and resultado='L' and punto= '" + reporte[i].SumaL + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilL = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interil = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interil + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        peril = (filtros[0]["interpretacion"].ToString());
                        ril = filtros[0]["rango"].ToString();
                        break;
                    case "S":
                        td = reporte[i].SumaM - reporte[i].SumaL;
                        orden = "C";
                        expresion = "idfactor= 3 and resultado='T' and punto= '" + td + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilt = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interst = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interst + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perst = (filtros[0]["interpretacion"].ToString());
                        rst = filtros[0]["rango"].ToString();

                        expresion = "idfactor= 3 and resultado='M' and punto= '" + reporte[i].SumaM + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilM = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        intersm = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + intersm + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        persm = (filtros[0]["interpretacion"].ToString());
                        rsm = filtros[0]["rango"].ToString();

                        expresion = "idfactor= 3 and resultado='L' and punto= '" + reporte[i].SumaL + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilL = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        intersl = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + intersl + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        persl = (filtros[0]["interpretacion"].ToString());
                        rsl = filtros[0]["rango"].ToString();
                        break;
                    case "C":
                        td = reporte[i].SumaM - reporte[i].SumaL;
                        orden = "D";
                        expresion = "idfactor= 4 and resultado='T' and punto= '" + td + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilt = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interct = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + interct + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        perct = (filtros[0]["interpretacion"].ToString());
                        rct = filtros[0]["rango"].ToString();

                        expresion = "idfactor= 4 and resultado='M' and punto= '" + reporte[i].SumaM + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilM = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        intercm = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + intercm + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        percm = (filtros[0]["interpretacion"].ToString());
                        rcm = filtros[0]["rango"].ToString();

                        expresion = "idfactor= 4 and resultado='L' and punto= '" + reporte[i].SumaL + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilL = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        intercl = Convert.ToInt32(filtro[0]["idinterpretacion"].ToString());

                        expresion = " idinterpretacion= '" + intercl + "'";
                        filtros = dtinterpretacion.Select(expresion);
                        percl = (filtros[0]["interpretacion"].ToString());
                        rcl = filtros[0]["rango"].ToString();
                        break;
                }
                row["factor"] = Convert.ToString(reporte[i].Factor);
                row["M"] = Convert.ToInt32(reporte[i].SumaM);
                row["L"] = Convert.ToInt32(reporte[i].SumaL);
                row["orden"] = orden;
                row["puntosT"] = td;
                row["percentilT"] = percentilt;
                row["percentilM"] = percentilM;
                row["percentilL"] = percentilL;
                // row["percentil"] = percentil;
                dtresultado.Rows.Add(row);
            }

            //Combinaciones para grafica T
            DataTable dtt = new DataTable();
            dtt.Columns.Add("combinacion");
            DataRow filas = dtt.NewRow();
            if (rdt == "Alto" && rst == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Empuje: " + dtinterpretacion.Rows[8]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rit == "Alto" && rdt == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Buena voluntad: " + dtinterpretacion.Rows[9]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rit == "Alto" && rct == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Confianza en si mismo: " + dtinterpretacion.Rows[10]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rst == "Alto" && rit == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Reflexion o concentracion: " + dtinterpretacion.Rows[11]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rct == "Alto" && rdt == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Adaptabilidad: " + dtinterpretacion.Rows[12]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rct == "Alto" && rst == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Sensibilidad: " + dtinterpretacion.Rows[13]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rct == "Alto" && rit == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Perfeccionista: " + dtinterpretacion.Rows[14]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rst == "Alto" && rct == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Persistente: " + dtinterpretacion.Rows[15]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rst == "Alto" && rdt == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Paciente: " + dtinterpretacion.Rows[16]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rit == "Alto" && rst == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Habilidad de contacto: " + dtinterpretacion.Rows[17]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rdt == "Alto" && rct == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Individualista: " + dtinterpretacion.Rows[18]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rdt == "Alto" && rit == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Creativo: " + dtinterpretacion.Rows[19]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rct == "Alto" && rdt == "Alto")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Bandera roja alta: " + dtinterpretacion.Rows[20]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            if (rct == "Bajo" && rdt == "Bajo")
            { filas = dtt.NewRow(); filas["combinacion"] = Convert.ToString("Bandera roja baja: " + dtinterpretacion.Rows[21]["interpretacion"].ToString()); dtt.Rows.Add(filas); }

            ////////////////////////////Combinaciones para grafica M /////////////////////////////////////////
            DataTable dtm = new DataTable();
            dtm.Columns.Add("combinacion");
            DataRow fila = dtm.NewRow();
            if (rdm == "Alto" && rsm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Empuje: " + dtinterpretacion.Rows[8]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rim == "Alto" && rdm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Buena voluntad: " + dtinterpretacion.Rows[9]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rim == "Alto" && rcm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Confianza en si mismo: " + dtinterpretacion.Rows[10]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rsm == "Alto" && rim == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Reflexion o concentracion: " + dtinterpretacion.Rows[11]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rcm == "Alto" && rdm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Adaptabilidad: " + dtinterpretacion.Rows[12]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rcm == "Alto" && rsm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Sensibilidad: " + dtinterpretacion.Rows[13]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rcm == "Alto" && rim == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Perfeccionista: " + dtinterpretacion.Rows[14]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rsm == "Alto" && rcm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Persistente: " + dtinterpretacion.Rows[15]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rsm == "Alto" && rdm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Paciente: " + dtinterpretacion.Rows[16]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rim == "Alto" && rsm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Habilidad de contacto: " + dtinterpretacion.Rows[17]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rdm == "Alto" && rcm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Individualista: " + dtinterpretacion.Rows[18]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rdm == "Alto" && rim == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Creativo: " + dtinterpretacion.Rows[19]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rcm == "Alto" && rdm == "Alto")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Bandera roja alta: " + dtinterpretacion.Rows[20]["interpretacion"].ToString()); dtm.Rows.Add(fila); }

            if (rcm == "Bajo" && rdm == "Bajo")
            { fila = dtm.NewRow(); fila["combinacion"] = Convert.ToString("Bandera roja baja: " + dtinterpretacion.Rows[21]["interpretacion"].ToString()); dtm.Rows.Add(fila); }


            //////////////////////// grafica para L ///////////////////////////////////////////////
            DataTable dtl = new DataTable();
            dtl.Columns.Add("combinacion");
            DataRow rows = dtl.NewRow();
            if (rdl == "Alto" && rsl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Empuje: " + dtinterpretacion.Rows[8]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (ril == "Alto" && rdl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Buena voluntad: " + dtinterpretacion.Rows[9]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (ril == "Alto" && rcl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Confianza en si mismo: " + dtinterpretacion.Rows[10]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rsl == "Alto" && ril == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Reflexion o concentracion: " + dtinterpretacion.Rows[11]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rcl == "Alto" && rdl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Adaptabilidad: " + dtinterpretacion.Rows[12]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rcl == "Alto" && rsl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Sensibilidad: " + dtinterpretacion.Rows[13]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rcl == "Alto" && ril == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Perfeccionista: " + dtinterpretacion.Rows[14]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rsl == "Alto" && rcl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Persistente: " + dtinterpretacion.Rows[15]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rsl == "Alto" && rdl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Paciente: " + dtinterpretacion.Rows[16]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (ril == "Alto" && rsl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Habilidad de contacto: " + dtinterpretacion.Rows[17]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rdl == "Alto" && rcl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Individualista: " + dtinterpretacion.Rows[18]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rdl == "Alto" && ril == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Creativo: " + dtinterpretacion.Rows[19]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rcl == "Alto" && rdl == "Alto")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Bandera roja alta: " + dtinterpretacion.Rows[20]["interpretacion"].ToString()); dtl.Rows.Add(rows); }

            if (rcl == "Bajo" && rdl == "Bajo")
            { rows = dtl.NewRow(); rows["combinacion"] = Convert.ToString("Bandera roja baja: " + dtinterpretacion.Rows[21]["interpretacion"].ToString()); dtl.Rows.Add(rows); }


            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportParameter[] parametro = new ReportParameter[12];
            parametro[0] = new ReportParameter("DT", rdt + ": " + perdt);
            parametro[1] = new ReportParameter("DM", rdm + ": " + perdm);
            parametro[2] = new ReportParameter("DL", rdl + ": " + perdl);
            parametro[3] = new ReportParameter("IT", rit + ": " + perit);
            parametro[4] = new ReportParameter("IM", rim + ": " + perim);
            parametro[5] = new ReportParameter("IL", ril + ": " + peril);
            parametro[6] = new ReportParameter("ST", rst + ": " + perst);
            parametro[7] = new ReportParameter("SM", rsm + ": " + persm);
            parametro[8] = new ReportParameter("SL", rsl + ": " + persl);
            parametro[9] = new ReportParameter("CT", rct + ": " + perct);
            parametro[10] = new ReportParameter("CM", rcm + ": " + percm);
            parametro[11] = new ReportParameter("CL", rcl + ": " + percl);


            ReportDataSource rds = new ReportDataSource("DataSetcleaver", dtresultado);
            ReportDataSource rds2 = new ReportDataSource("DataSetT", dtt);
            ReportDataSource rds3 = new ReportDataSource("DataSetM", dtm);
            ReportDataSource rds4 = new ReportDataSource("DataSetL", dtl);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Cleaver.reporteCleaver.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds2);
            reportViewer1.LocalReport.DataSources.Add(rds3);
            reportViewer1.LocalReport.DataSources.Add(rds4);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.LocalReport.SetParameters(parametro);
            reportViewer1.RefreshReport();
        }
    }
}
