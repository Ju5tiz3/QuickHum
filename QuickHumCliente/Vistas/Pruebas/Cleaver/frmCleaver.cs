﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Cleaver
{
    public partial class frmCleaver : Form
    {
        public frmCleaver(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        private readonly QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        private readonly EscolaridadPrueba _escolaridad = null;
        readonly dto_cleaver_respuestas_candidato respcandidato = new dto_cleaver_respuestas_candidato();
        CLEAVER cleaver = new CLEAVER();
        readonly DataTable dtpreguntas = new DataTable();
        readonly DataTable dtrespuestas = new DataTable();
        readonly DataTable dtactualizar = new DataTable();
        DataRow[] row, row1, row2, row3;
        int x = 0; int idM = 0; int idL = 0; int puntoMmas = 0, puntoLmas = 0, puntoMmenos = 0, puntoLmenos = 0, recorrido = 0, numero_prueba;
        private void frmCleaver_Load(object sender, EventArgs e)
        {
           
            dtactualizar.Columns.Add("id_respuesta_candidato");
            dtactualizar.Columns.Add("id_respuesta");
            dtactualizar.Columns.Add("M");
            dtactualizar.Columns.Add("L");
            dtactualizar.Columns.Add("numero_prueba");

            //this.Size = new Size(538, 525);
            this.panelAyuda.Location = new Point(1, 1);
            DecripcionLabels();
            listardatos();
        }

        private void listardatos()
        {
            dto_cleaver_preguntas[] preguntas = cliente.ListadoCleaverPreguntas();
            
            dtpreguntas.Columns.Add("idpregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("identificador");

            dtrespuestas.Columns.Add("idrespuesta");
            dtrespuestas.Columns.Add("respuesta");
            dtrespuestas.Columns.Add("id_pregunta");

            for (int i=0; i < preguntas.ToList().Count; i++)
            {
                DataRow fila = dtpreguntas.NewRow();

                fila["idpregunta"] = Convert.ToInt32(preguntas[i].Idpregunta);
                fila["pregunta"] = Convert.ToString(preguntas[i].Pregunta);
                fila["identificador"] = Convert.ToInt32(preguntas[i].Identificador);

                for(int j = 0; j < 2 ; j++)
                {
                    DataRow row = dtrespuestas.NewRow();

                    row["idrespuesta"] = Convert.ToInt32(preguntas[i].Respuesta[j].Idrespuesta);
                    row["respuesta"] = Convert.ToString(preguntas[i].Respuesta[j].Respuesta);
                    row["id_pregunta"] = Convert.ToInt32(preguntas[i].Respuesta[j].Idpregunta);
                    dtrespuestas.Rows.Add(row);
                }
                dtpreguntas.Rows.Add(fila);
            }
            lblpregunta.Text = "Pregunta 1";

            lblA.Text="- " + dtpreguntas.Rows[0]["pregunta"].ToString();
            lblB.Text = "- " + dtpreguntas.Rows[1]["pregunta"].ToString();
            lblC.Text = "- " + dtpreguntas.Rows[2]["pregunta"].ToString();
            lblD.Text = "- " + dtpreguntas.Rows[3]["pregunta"].ToString();


        }

        private void MostrarPreguntas(int i)
        {
            lblpregunta.Text = "Pregunta " + (i+1)+"." ;
            row = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(4 * i)][0].ToString() + " ' ");
            row1 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(4 * i) + 1][0].ToString() + " ' ");
            row2 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(4 * i) + 2][0].ToString() + " ' ");
            row3 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(4 * i) + 3][0].ToString() + " ' ");


            if (row.Length > 0)
            {
                //lblIdPregunta.Text = dtpreguntas.Rows[(4 * x)-4]["id_pregunta"].ToString();
                lblA.Text =  dtpreguntas.Rows[(4 * x)]["pregunta"].ToString();
                lblB.Text =  dtpreguntas.Rows[(4 * x) + 1]["pregunta"].ToString();
                lblC.Text =  dtpreguntas.Rows[(4 * x) + 2]["pregunta"].ToString();
                lblD.Text =  dtpreguntas.Rows[(4 * x) + 3]["pregunta"].ToString();
            }

        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                DecripcionLabels();
                GuardarRespuestas();
                MostrarPreguntas(x);
                lblpregunta.Text = "Pregunta " + (x+1) ;
                limpiarcampos();
                btnSiguiente.Enabled = false;
            }

            else if (btnSiguiente.Text=="Siguiente.")
            {
                x++;
                DecripcionLabels();
                ActualizarRespuestas();
                MostrarPreguntas(x);
                lblpregunta.Text = "Pregunta " + (x+1);
                if(recorrido>(x *2))
                {
                    CheckarRadiosadelante();
                }
                if (recorrido <=(x *2))
                {
                    this.btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    limpiarcampos();
                }
                
            }

            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Cleaver", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                {
                    x++;
                    GuardarRespuestas();
                    respcandidato.Numeroprueba = numero_prueba;
                    cliente.FinalizaCleaverPrueba(respcandidato);
                    this.Close();
                }
            }


            if (x == Convert.ToInt32(dtpreguntas.Rows.Count - 72) - 1)
            {
                btnSiguiente.Text = "Finalizar";
            }
            btnAnterior.Enabled = true;

        }
        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_cleaver_respuestas_candidato[] listarespcandidato = cliente.ListaCleaverRespuestasCandidatos(numero_prueba);
            dtactualizar.Clear();
            for(int w=0;w < listarespcandidato.ToList().Count;w++)
            {
                DataRow fila = dtactualizar.NewRow();
                fila["id_respuesta_candidato"] = Convert.ToInt32(listarespcandidato[w].Idrespuestascandidato);
                fila["id_respuesta"] = Convert.ToInt32(listarespcandidato[w].Idrespuesta);
                fila["M"] = Convert.ToInt32(listarespcandidato[w].M);
                fila["L"] = Convert.ToInt32(listarespcandidato[w].L);
                fila["numero_prueba"] = Convert.ToInt32(listarespcandidato[w].Numeroprueba);

                dtactualizar.Rows.Add(fila);
            }
            recorrido = dtactualizar.Rows.Count;
            btnSiguiente.Text = "Siguiente.";
            btnSiguiente.Enabled = true;
          
            x--;
            DecripcionLabels();
           
            CheckarRadiosadelante();
            MostrarPreguntas(x);
            lblpregunta.Text = "Pregunta " + (x+1);
            if(x==0)
            { btnAnterior.Enabled = false; }

            if(btnSiguiente.Text=="Finalizar")
            { btnSiguiente.Text = "Siguiente"; }
        }
        private void Evaluar()
        {
            if (rbtAmas.Checked == true)
            {
                idM = Convert.ToInt32(dtrespuestas.Rows[(8 * x) - 8]["idrespuesta"].ToString());
                puntoMmas = 1; puntoLmas = 0;
            }
            else if (rbtAmenos.Checked == true)
            {
                idL = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 1]["idrespuesta"].ToString());
                puntoMmenos = 0; puntoLmenos = 1;
            }
            if (rbtBmas.Checked == true)
            {
                idM = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 2]["idrespuesta"].ToString());
                puntoMmas = 1; puntoLmas = 0;
            }
            else if (rbtBmenos.Checked == true)
            {
                idL = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 3]["idrespuesta"].ToString());
                puntoMmenos = 0; puntoLmenos = 1;
            }
            if (rbtCmas.Checked == true)
            {
                idM = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 4]["idrespuesta"].ToString());
                puntoMmas = 1; puntoLmas = 0;
            }
            else if (rbtCmenos.Checked == true)
            {
                idL = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 5]["idrespuesta"].ToString());
                puntoMmenos = 0; puntoLmenos = 1;
            }
            if (rbtDmas.Checked == true)
            {
                idM = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 6]["idrespuesta"].ToString());
                puntoMmas = 1; puntoLmas = 0;
            }
            else if (rbtDmenos.Checked == true)
            {
                idL = Convert.ToInt32(dtrespuestas.Rows[((8 * x) - 8) + 7]["idrespuesta"].ToString());
                puntoMmenos = 0; puntoLmenos = 1;
            }
        }
        private void CheckarRadiosadelante()
        {
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x)]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtAmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 1]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtAmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 2]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtBmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 3]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtBmenos.Checked = true;
            }
            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 4]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtCmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 5]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtCmenos.Checked = true;
            }

            if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 6]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[x * 2]["id_respuesta"].ToString()))
            {
                rbtDmas.Checked = true;
            }
            else if (Convert.ToInt32(dtrespuestas.Rows[(8 * x) + 7]["idrespuesta"].ToString()) == Convert.ToInt32(dtactualizar.Rows[(x * 2) + 1]["id_respuesta"].ToString()))
            {
                rbtDmenos.Checked = true;
            }
        }
        private void GuardarRespuestas()
        {
            Evaluar();

            respcandidato.Idrespuesta = idM;
            respcandidato.M = puntoMmas;
            respcandidato.L = puntoLmas;
            respcandidato.Numeroprueba = numero_prueba;
            cliente.InsertaCleaverRespuesta(respcandidato);

            respcandidato.Idrespuesta = idL;
            respcandidato.M = puntoMmenos;
            respcandidato.L = puntoLmenos;
            respcandidato.Numeroprueba = numero_prueba;
            cliente.InsertaCleaverRespuesta(respcandidato);
        }

        private void ActualizarRespuestas()
        {
            Evaluar();

            respcandidato.Idrespuestascandidato = Convert.ToInt32(dtactualizar.Rows[(x * 2) - 2]["id_respuesta_candidato"].ToString());
            respcandidato.Idrespuesta = idM;
            respcandidato.M = puntoMmas;
            respcandidato.L = puntoLmas;
            cliente.ActualizaCleaverRespuesta(respcandidato);

            respcandidato.Idrespuestascandidato = Convert.ToInt32(dtactualizar.Rows[(x * 2) - 1]["id_respuesta_candidato"].ToString());
            respcandidato.Idrespuesta = idL;
            respcandidato.M = puntoMmenos;
            respcandidato.L = puntoLmenos;
            cliente.ActualizaCleaverRespuesta(respcandidato);
        }

        private void limpiarcampos()
        {
            foreach (Control radio in this.groupBox2.Controls)
            {
                if(radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }

            foreach (Control radio in this.groupBox3.Controls)
            {
                if (radio is RadioButton)
                {
                    ((RadioButton)radio).Checked = false;
                }
            }
        }

        private bool ValidacionRAdioButtons()
        {
            if (rbtAmas.Checked == false && rbtBmas.Checked == false && rbtCmas.Checked == false && rbtDmas.Checked == false)
            {
                if (rbtAmenos.Checked == false && rbtBmenos.Checked == false && rbtCmenos.Checked == false && rbtDmenos.Checked == false)
                {
                    MessageBox.Show("Debe ingresar un campo de la columna (-) ");
                    return false;
                }
                return true;
            }

            return true;
        }

        #region Validar RadioButtons
        private void rbtAmas_Click(object sender, EventArgs e)
        {
            if (rbtAmas.Checked == true)
            {
                rbtAmenos.Checked = false;
            }

            if ((rbtAmas.Checked == true && rbtBmenos.Checked == true) || (rbtAmas.Checked == true && rbtCmenos.Checked == true) || (rbtAmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;

            }

        }

        private void rbtAmenos_Click(object sender, EventArgs e)
        {
            if (rbtAmenos.Checked == true)
            {
                rbtAmas.Checked = false;
            }

            if ((rbtAmenos.Checked == true && rbtBmas.Checked == true) || (rbtAmenos.Checked == true && rbtCmas.Checked == true) || (rbtAmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtBmas_Click(object sender, EventArgs e)
        {
            if (rbtBmas.Checked == true)
            {
                rbtBmenos.Checked = false;
            }
            if ((rbtBmas.Checked == true && rbtAmenos.Checked == true) || (rbtBmas.Checked == true && rbtCmenos.Checked == true) || (rbtBmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtBmenos_Click(object sender, EventArgs e)
        {
            if (rbtBmenos.Checked == true)
            {
                rbtBmas.Checked = false;
            }

            if ((rbtBmenos.Checked == true && rbtAmas.Checked == true) || (rbtBmenos.Checked == true && rbtCmas.Checked == true) || (rbtBmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtCmas_Click(object sender, EventArgs e)
        {
            if (rbtCmas.Checked == true)
            {
                rbtCmenos.Checked = false;
            }

            if ((rbtCmas.Checked == true && rbtAmenos.Checked == true) || (rbtCmas.Checked == true && rbtBmenos.Checked == true) || (rbtCmas.Checked == true && rbtDmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtCmenos_Click(object sender, EventArgs e)
        {
            if (rbtCmenos.Checked == true)
            {
                rbtCmas.Checked = false;
            }

            if ((rbtCmenos.Checked == true && rbtAmas.Checked == true) || (rbtCmenos.Checked == true && rbtBmas.Checked == true) || (rbtCmenos.Checked == true && rbtDmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtDmas_Click(object sender, EventArgs e)
        {
            if (rbtDmas.Checked == true)
            {
                rbtDmenos.Checked = false;
            }


            if ((rbtDmas.Checked == true && rbtAmenos.Checked == true) || (rbtDmas.Checked == true && rbtBmenos.Checked == true) || (rbtDmas.Checked == true && rbtCmenos.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }

            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rbtDmenos_Click(object sender, EventArgs e)
        {
            if (rbtDmenos.Checked == true)
            {
                rbtDmas.Checked = false;
            }


            if ((rbtDmenos.Checked == true && rbtAmas.Checked == true) || (rbtDmenos.Checked == true && rbtBmas.Checked == true) || (rbtDmenos.Checked == true && rbtCmas.Checked == true))
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }
        #endregion

        #region Descripcion Labels
        private void DecripcionLabels()
        {
            if (x == 0)
            {
                toolTip1.SetToolTip(lblA, "capaz de llevar a otro a creer o hacer algo");
                toolTip2.SetToolTip(lblB, "aquel que en su trato es amable ");
                toolTip3.SetToolTip(lblC, "que demuestra sencillez y docilidad  ");
                toolTip4.SetToolTip(lblD, "especial, único, distinto a los demás");
            }
           else if (x==1)
            {
                toolTip1.SetToolTip(lblA, "aquel que ataca u ofende a los demás");
                toolTip2.SetToolTip(lblB, "el que es alegre, entusiasta");
                toolTip3.SetToolTip(lblC, "el que busca lo agradable con el menor esfuerzo ");
                toolTip4.SetToolTip(lblD, "aquel que es miedoso, cobarde ");
            }
           else if (x == 2)
            {
                toolTip1.SetToolTip(lblA, "aquel que es grato y atrayente ");
                toolTip2.SetToolTip(lblB, "que tiene miedo al castigo divino ");
                toolTip3.SetToolTip(lblC, "aquel que es constante para alcanzar sus objetivos");
                toolTip4.SetToolTip(lblD, "aquel que con sus cualidades capta la atención");
           }
            else if (x == 3)
            {
                toolTip1.SetToolTip(lblA, "precavido, que actúa previendo posibles dificultades");
                toolTip2.SetToolTip(lblB, "resuelto y decidido");
                toolTip3.SetToolTip(lblC, "que consigue que una idea o hecho se acepten como verdad");
                toolTip4.SetToolTip(lblD, "aquel que es accesible, cordial y/o crédulo");
            }
            else if (x == 4)
            {
                toolTip1.SetToolTip(lblA, "aquel que fácilmente se amolda a los requerimientos");
                toolTip2.SetToolTip(lblB, "que actúa de manera arriesgada, con decisión y arrojo");
                toolTip3.SetToolTip(lblC, "que es fiel a personas o ideales");
                toolTip4.SetToolTip(lblD, "que cautiva la atención, seductor");
            }
            else if (x == 5)
            {
                toolTip1.SetToolTip(lblA, "disponible, preparado y con ánimo favorable para participar");
                toolTip2.SetToolTip(lblB, "que anhela poseer, o realizar una cierta actividad");
                toolTip3.SetToolTip(lblC, "aquel con quien es fácil llegar a un acuerdo");
                toolTip4.SetToolTip(lblD, "el que se siente motivado");
            }
            else if (x == 6)
            {
                toolTip1.SetToolTip(lblA, "con convencimiento interno y tenacidad para alcanzar un objetivo");
                toolTip2.SetToolTip(lblB, "que es tolerante y acepta ideas nuevas y distintas a las suyas");
                toolTip3.SetToolTip(lblC, "el que busca ser agradable y dar satisfacción a otros");
                toolTip4.SetToolTip(lblD, "que muestra energía, motivación y deseos para hacer algo");
            }
            else if (x == 7)
            {
                toolTip1.SetToolTip(lblA, "satisfecho de si mismo, crédulo, ingenuo");
                toolTip2.SetToolTip(lblB, "el que trata de mantener relaciones agradables con los demás");
                toolTip3.SetToolTip(lblC, "que respeta y sobrelleva aquello que no le es familiar");
                toolTip4.SetToolTip(lblD, "que sostiene sus ideas o actitudes con determinación");
            }
            else if (x == 8)
            {
                toolTip1.SetToolTip(lblA, "aquella persona que se muestra equilibrada y serena");
                toolTip2.SetToolTip(lblB, "que es exacto y conciso");
                toolTip3.SetToolTip(lblC, "que es irritable, inquieto, impresionable");
                toolTip4.SetToolTip(lblD, "alegre, festivo, divertido");
            }
            else if (x == 9)
            {
                toolTip1.SetToolTip(lblA, "que sigue un método de manera continua");
                toolTip2.SetToolTip(lblB, "que da lo que tiene");
                toolTip3.SetToolTip(lblC, "que muestra energía, motivación y deseos de hacer algo");
                toolTip4.SetToolTip(lblD, "que es constante y tenaz en una actividad que se propone");
            }
            else if (x == 10)
            {
                toolTip1.SetToolTip(lblA, "que tiene la disposición y características para rivalizar ");
                toolTip2.SetToolTip(lblB, "que se muestra contento y regocijado. Gozoso");
                toolTip3.SetToolTip(lblC, "que es atento y toma en cuenta a los demás");
                toolTip4.SetToolTip(lblD, "que busca el acuerdo, la convivencia, lo equilibrado");
            }
            else if (x == 11)
            {
                toolTip1.SetToolTip(lblA, "notable, digno de respeto y aprecio");
                toolTip2.SetToolTip(lblB, "que se inclina hacer el bien, amable");
                toolTip3.SetToolTip(lblC, "que se conforma con la situación en la que vive");
                toolTip4.SetToolTip(lblD, "que es comprometido con sus convicciones");
            }
            else if (x == 12)
            {
                toolTip1.SetToolTip(lblA, "que se apega a lo establecido");
                toolTip2.SetToolTip(lblB, "que toma en cuenta hasta el más mínimo detalle, susceptible");
                toolTip3.SetToolTip(lblC, "alguien de quien es muy difícil obtener el aprecio o aceptación");
                toolTip4.SetToolTip(lblD, "travieso, inquieto");
            }
            else if (x == 13)
            {
                toolTip1.SetToolTip(lblA, "que toma en cuenta y acata la reglas");
                toolTip2.SetToolTip(lblB, "que propone e inicia proyectos");
                toolTip3.SetToolTip(lblC, "que ve las cosas de manera favorable");
                toolTip4.SetToolTip(lblD, "dispuesto a complacer y ayudar");
            }
            else if (x == 14)
            {
                toolTip1.SetToolTip(lblA, "que enfrenta el peligro");
                toolTip2.SetToolTip(lblB, "que hace surgir ideas creativas");
                toolTip3.SetToolTip(lblC, "que es manejable, dócil, obediente");
                toolTip4.SetToolTip(lblD, "que se muestra poco abierto con las personas");
            }
            else if (x == 15)
            {
                toolTip1.SetToolTip(lblA, "que se acomoda o ajusta a circunstancias y condiciones");
                toolTip2.SetToolTip(lblB, "el que discute sus razonamientos para demostrar algo");
                toolTip3.SetToolTip(lblC, "el que no tenga preferencia o interés por algo");
                toolTip4.SetToolTip(lblD, "que en su manera de ser es agradable y atractivo, con carisma");
            }
            else if (x == 16)
            {
                toolTip1.SetToolTip(lblA, "que gusta de establecer relaciones afectuosas");
                toolTip2.SetToolTip(lblB, "que tiene la capacidad para esperar con tranquilidad");
                toolTip3.SetToolTip(lblC, "que se siente seguro y satisfecho con su manera de ser y actuar");
                toolTip4.SetToolTip(lblD, "que evita los excesos al expresarse verbalmente");
            }
            else if (x == 17)
            {
                toolTip1.SetToolTip(lblA, "que se resigna a las circunstancias");
                toolTip2.SetToolTip(lblB, "que inspira un sentimiento de seguridad");
                toolTip3.SetToolTip(lblC, "tranquilo, que está en paz, que evita peleas");
                toolTip4.SetToolTip(lblD, "que muestra lo que es cierto, real autentico");
            }
            else if (x == 18)
            {
                toolTip1.SetToolTip(lblA, "que gusta de tener experiencias novedosas y arriesgadas");
                toolTip2.SetToolTip(lblB, "que percibe fácilmente sentimientos, ideas y hechos");
                toolTip3.SetToolTip(lblC, "que es respetuoso y amable con su trato");
                toolTip4.SetToolTip(lblD, "el que en su conducta se mantiene alejado de los extremos");
            }
            else if (x == 19)
            {
                toolTip1.SetToolTip(lblA, "que es tolerante respecto a errores, que perdona fácilmente");
                toolTip2.SetToolTip(lblB, "el que gusta de lo bello");
                toolTip3.SetToolTip(lblC, "que tiene energía, fuerza y vitalidad");
                toolTip4.SetToolTip(lblD, "que busca la convivencia con sus semejantes");
            }
            else if (x == 20)
            {
                toolTip1.SetToolTip(lblA, "que habla mucho");
                toolTip2.SetToolTip(lblB, "que se domina y frena a si mismo");
                toolTip3.SetToolTip(lblC, "apegado a las costumbres, evita cambios e innovaciones");
                toolTip4.SetToolTip(lblD, "que es tajante, terminante y definitivo");
            }
            else if (x == 21)
            {
                toolTip1.SetToolTip(lblA, "que se siente restringido, intimidado para actuar, avergonzado");
                toolTip2.SetToolTip(lblB, "el que cuida los más pequeños detalles, conciso");
                toolTip3.SetToolTip(lblC, "el que dice lo que piensa abiertamente, comunicativo");
                toolTip4.SetToolTip(lblD, "atento, cortés, respetuoso, cooperativo");
            }
            else if (x == 22)
            {
                toolTip1.SetToolTip(lblA, "que tiene tacto y delicadeza en su trato");
                toolTip2.SetToolTip(lblB, "que se atreve a correr riesgos");
                toolTip3.SetToolTip(lblC, "distinguido, muy fino y delicado");
                toolTip4.SetToolTip(lblD, "que se siente complacido con lo que ha logrado");
            }
            else if (x == 23)
            {
                toolTip1.SetToolTip(lblA, "que está en constante actividad");
                toolTip2.SetToolTip(lblB, "que es aceptado y querido en un grupo");
                toolTip3.SetToolTip(lblC, "respetuoso, amable, considerado");
                toolTip4.SetToolTip(lblD, "que vive de acuerdo a costumbres e ideas religiosas");
            }
        }
    
        #endregion

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                cleaver = cliente.NuevoCleaver(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            numero_prueba = cleaver.IdPrueba;

            panelAyuda.Hide();
            btnCancelarAyuda.Hide();
            btnIniciarAyuda.Hide();
            btnAyuda.Show();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            gbPreguntas.Enabled = true;
            panelAyuda.Show();
            btnCerrarAyuda.Show();
            btnCancelarAyuda.Hide();
            btnIniciarAyuda.Hide();
        }

        private void btnCancelarAyuda_Click(object sender, EventArgs e)
        {
            panelAyuda.Hide();
            gbPreguntas.Enabled = true;
        }

        private void frmCleaver_FormClosing(object sender, FormClosingEventArgs e)
        {
            respcandidato.Numeroprueba = numero_prueba;
            cliente.FinalizaCleaverPrueba(respcandidato);
        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Cleaver", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Cleaver", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            this.Close();
        }
    }
}
