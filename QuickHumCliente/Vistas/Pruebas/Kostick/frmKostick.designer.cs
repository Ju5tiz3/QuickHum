﻿namespace QuickHumCliente.Vistas.Pruebas.Kostick
{
    partial class frmKostick
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmKostick));
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.pnlOpciones = new System.Windows.Forms.Panel();
            this.rbtOpcionA = new System.Windows.Forms.RadioButton();
            this.rbtOpcionB = new System.Windows.Forms.RadioButton();
            this.lblMsj = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtEjemp1 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnlOpciones.SuspendLayout();
            this.panelAyuda.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(689, 36);
            this.label3.TabIndex = 64;
            this.label3.Text = "PRUEBA KOSTICK";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(429, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 37);
            this.panel1.TabIndex = 74;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(50, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lbltime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbltime.Location = new System.Drawing.Point(170, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // pnlOpciones
            // 
            this.pnlOpciones.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlOpciones.Controls.Add(this.rbtOpcionA);
            this.pnlOpciones.Controls.Add(this.rbtOpcionB);
            this.pnlOpciones.Location = new System.Drawing.Point(3, 3);
            this.pnlOpciones.Name = "pnlOpciones";
            this.pnlOpciones.Size = new System.Drawing.Size(621, 174);
            this.pnlOpciones.TabIndex = 73;
            // 
            // rbtOpcionA
            // 
            this.rbtOpcionA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionA.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionA.Location = new System.Drawing.Point(21, 16);
            this.rbtOpcionA.Name = "rbtOpcionA";
            this.rbtOpcionA.Size = new System.Drawing.Size(580, 51);
            this.rbtOpcionA.TabIndex = 71;
            this.rbtOpcionA.Text = "Opcion A";
            this.rbtOpcionA.UseVisualStyleBackColor = false;
            this.rbtOpcionA.CheckedChanged += new System.EventHandler(this.rbtOpcionA_CheckedChanged);
            // 
            // rbtOpcionB
            // 
            this.rbtOpcionB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionB.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionB.Location = new System.Drawing.Point(21, 88);
            this.rbtOpcionB.Name = "rbtOpcionB";
            this.rbtOpcionB.Size = new System.Drawing.Size(580, 51);
            this.rbtOpcionB.TabIndex = 71;
            this.rbtOpcionB.Text = "Opcion B";
            this.rbtOpcionB.UseVisualStyleBackColor = false;
            this.rbtOpcionB.CheckedChanged += new System.EventHandler(this.rbtOpcionB_CheckedChanged);
            // 
            // lblMsj
            // 
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(4, 180);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(621, 72);
            this.lblMsj.TabIndex = 10;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel3);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(689, 552);
            this.panelAyuda.TabIndex = 76;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.btnCerrarAyuda);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.btnIniciarAyuda);
            this.panel3.Location = new System.Drawing.Point(46, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(597, 474);
            this.panel3.TabIndex = 73;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCerrarAyuda.BackgroundImage")));
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(559, 1);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 81;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Location = new System.Drawing.Point(560, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(36, 36);
            this.panel7.TabIndex = 80;
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(1, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(36, 36);
            this.panel6.TabIndex = 79;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.rbtEjemp1);
            this.panel2.Controls.Add(this.radioButton1);
            this.panel2.Location = new System.Drawing.Point(18, 203);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(563, 144);
            this.panel2.TabIndex = 72;
            // 
            // rbtEjemp1
            // 
            this.rbtEjemp1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtEjemp1.Checked = true;
            this.rbtEjemp1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtEjemp1.Location = new System.Drawing.Point(21, 14);
            this.rbtEjemp1.Name = "rbtEjemp1";
            this.rbtEjemp1.Size = new System.Drawing.Size(524, 51);
            this.rbtEjemp1.TabIndex = 71;
            this.rbtEjemp1.TabStop = true;
            this.rbtEjemp1.Text = "A) Lo limpiaría.";
            this.rbtEjemp1.UseVisualStyleBackColor = false;
            // 
            // radioButton1
            // 
            this.radioButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(21, 74);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(524, 51);
            this.radioButton1.TabIndex = 71;
            this.radioButton1.Text = "B) Le diría al licenciado que si lo manda limpiar.";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(36, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(427, 18);
            this.label6.TabIndex = 68;
            this.label6.Text = "Como ha visto, es muy sencillo ahora continúe con los siguientes 90.";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(34, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(529, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Calibri", 11F);
            this.label1.Location = new System.Drawing.Point(18, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(575, 75);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(461, 54);
            this.label4.TabIndex = 62;
            this.label4.Text = "EJEMPLO:\r\nMe encuentro sentado frente a un vidrio que esta sucio, a mi no me gust" +
    "a\r\nesta situación lo que haría es:";
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(248, 412);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(221, 360);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 81;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(14, 360);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 80;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNumPreg.Location = new System.Drawing.Point(270, 4);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(115, 17);
            this.lblNumPreg.TabIndex = 78;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(533, 360);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 79;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(341, 360);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 77;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.gbPreguntas);
            this.panel4.Controls.Add(this.btnAyuda);
            this.panel4.Controls.Add(this.btnAnterior);
            this.panel4.Controls.Add(this.btnSiguiente);
            this.panel4.Controls.Add(this.btnCancelar);
            this.panel4.Location = new System.Drawing.Point(13, 96);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(655, 423);
            this.panel4.TabIndex = 82;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel9.Controls.Add(this.lblNumPreg);
            this.panel9.Location = new System.Drawing.Point(1, 317);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(654, 25);
            this.panel9.TabIndex = 82;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.pnlOpciones);
            this.gbPreguntas.Controls.Add(this.lblMsj);
            this.gbPreguntas.Location = new System.Drawing.Point(14, 50);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(629, 267);
            this.gbPreguntas.TabIndex = 0;
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCloseWindow.BackgroundImage")));
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Location = new System.Drawing.Point(652, 0);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 81;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // frmKostick
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(689, 588);
            this.ControlBox = false;
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(697, 596);
            this.Name = "frmKostick";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prueba Kostick";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmKostick_FormClosing);
            this.Load += new System.EventHandler(this.frmKostick_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlOpciones.ResumeLayout(false);
            this.panelAyuda.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel pnlOpciones;
        private System.Windows.Forms.RadioButton rbtOpcionA;
        private System.Windows.Forms.RadioButton rbtOpcionB;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtEjemp1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnCloseWindow;

    }
}