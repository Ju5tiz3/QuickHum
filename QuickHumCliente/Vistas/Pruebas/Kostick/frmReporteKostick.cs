﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Kostick
{
    public partial class frmReporteKostick : Form
    {
        QuickHumClient Cliente = Globales.cliente;

        DataTable dtFactores = new DataTable();
        DataTable dtSubFactores = new DataTable();
        DataTable dtInterpretaciones = new DataTable();
        DataTable dtCandidato = new DataTable();
        DataRow[] RowFiltro;

        private Expediente _expediente = null;

        public frmReporteKostick(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }


        private void frmReporteKostick_Load(object sender, EventArgs e)
        {
            //Asignamos la estructura necesaria a los DataTable       
            dtFactores.Columns.Add("Factor");
            dtFactores.Columns.Add("Suma");

            dtSubFactores.Columns.Add("IdFactor");
            dtSubFactores.Columns.Add("IdSubFactor");
            dtSubFactores.Columns.Add("SubFactor");
            dtSubFactores.Columns.Add("Suma");
            dtSubFactores.Columns.Add("InterpretacionPositiva");
            dtSubFactores.Columns.Add("InterpretacionNegativa");

            dtInterpretaciones.Columns.Add("IdInterpretacion");
            dtInterpretaciones.Columns.Add("IdSubFactor");
            dtInterpretaciones.Columns.Add("InterpretacionPositiva");
            dtInterpretaciones.Columns.Add("InterpretacionNegativa");
            dtInterpretaciones.Columns.Add("PuntMaximo");
            dtInterpretaciones.Columns.Add("PuntMinimo");
            dtInterpretaciones.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Evaluador"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);
        }


        private void btnMostrar_Click(object sender, EventArgs e)
        {
            //Variable que contendra los parametros de envio
            ReportParameter[] Parametros = new ReportParameter[20];

            //Limpiamos los DataTable por si se desea recargar el reporte
            dtFactores.Clear();
            dtSubFactores.Clear();
            dtInterpretaciones.Clear();

            //Vectores necesarias para la crear el DataTable dtFactores
            int[] SumaFactor = new int[7];
            string[] NombreFactor = new string[7];
            double[] NumeroFactores = new double[7];


            dto_kostick_resultados[] resultados = Cliente.ListaKosticResultados(_expediente.Id);
            dto_kostick_interpretacion[] Interpretacion = Cliente.ListaKostickInterpretacion();

            dataGridView1.DataSource = resultados;
            dataGridView2.DataSource = Interpretacion;

            //Cargamos el DataTable de Interpretaciones
            for (int i = 0; i < Interpretacion.ToList().Count; i++)
            {
                DataRow row = dtInterpretaciones.NewRow();
                row["IdInterpretacion"] = Convert.ToInt32(Interpretacion[i].IdInterpretacion);
                row["IdSubFactor"] = Convert.ToInt32(Interpretacion[i].IdSubFactor);
                row["InterpretacionPositiva"] = Convert.ToString(Interpretacion[i].InterpretacionPositiva);
                row["InterpretacionNegativa"] = Convert.ToString(Interpretacion[i].InterpretacionNegativa);
                row["PuntMaximo"] = Convert.ToInt32(Interpretacion[i].PuntajeMaximo);
                row["PuntMinimo"] = Convert.ToInt32(Interpretacion[i].PuntajeMinimo);
                row["Rango"] = Convert.ToString(Interpretacion[i].Rango);

                dtInterpretaciones.Rows.Add(row);

            }

            //Sumamos el valor de los factores
            for (int i = 0; i < resultados.ToList().Count; i++)
            {

                switch (resultados[i].IdFactor)
                {
                    case 1:
                        NombreFactor[0] = resultados[i].Factor;
                        SumaFactor[0] = SumaFactor[0] + resultados[i].Suma;
                        NumeroFactores[0] = 3.0;
                        break;

                    case 2:
                        NombreFactor[1] = resultados[i].Factor;
                        SumaFactor[1] = SumaFactor[1] + resultados[i].Suma;
                        NumeroFactores[1] = 3.0;
                        break;

                    case 3:
                        NombreFactor[2] = resultados[i].Factor;
                        SumaFactor[2] = SumaFactor[2] + resultados[i].Suma;
                        NumeroFactores[2] = 2.0;
                        break;

                    case 4:
                        NombreFactor[3] = resultados[i].Factor;
                        SumaFactor[3] = SumaFactor[3] + resultados[i].Suma;
                        NumeroFactores[3] = 4.0;
                        break;

                    case 5:
                        NombreFactor[4] = resultados[i].Factor;
                        SumaFactor[4] = SumaFactor[4] + resultados[i].Suma;
                        NumeroFactores[4] = 3.0;
                        break;

                    case 6:
                        NombreFactor[5] = resultados[i].Factor;
                        SumaFactor[5] = SumaFactor[5] + resultados[i].Suma;
                        NumeroFactores[5] = 3.0;
                        break;

                    case 7:
                        NombreFactor[6] = resultados[i].Factor;
                        SumaFactor[6] = SumaFactor[6] + resultados[i].Suma;
                        NumeroFactores[6] = 2.0;
                        break;
                }
            }

            //Creamos el DataTable de Factores
            for (int i = 0; i < 7; i++)
            {
                DataRow row = dtFactores.NewRow();

                row["Factor"] = NombreFactor[i];
                row["Suma"] = Math.Round((SumaFactor[i] / NumeroFactores[i]), 1);

                dtFactores.Rows.Add(row);
            }

            dataGridView2.DataSource = dtFactores;

            //Filtramos las interpretaciones de cada SubFactor
            for (int i = 0; i < resultados.ToList().Count; i++)
            {
                DataRow Row = dtSubFactores.NewRow();
                Row["IdFactor"] = Convert.ToInt32(resultados[i].IdFactor);
                Row["IdSubFactor"] = Convert.ToInt32(resultados[i].IdSubFactor);
                Row["SubFactor"] = Convert.ToString(resultados[i].SubFactor);
                Row["Suma"] = Convert.ToInt32(resultados[i].Suma);



                RowFiltro = dtInterpretaciones.Select("IdSubFactor = '" + Convert.ToInt32(resultados[i].IdSubFactor) + "' AND PuntMaximo >= " + Convert.ToInt32(resultados[i].Suma) + " AND PuntMinimo <= " + Convert.ToInt32(resultados[i].Suma));

                if (RowFiltro.Length > 0)
                {
                    Row["InterpretacionPositiva"] = RowFiltro[0]["InterpretacionPositiva"].ToString();
                    Row["InterpretacionNegativa"] = RowFiltro[0]["InterpretacionNegativa"].ToString();

                    //Determinamos que rango posee cada uno de los SubFactores.
                    switch (Convert.ToInt32(resultados[i].IdSubFactor))
                    {

                        case 1:
                            Parametros[0] = new ReportParameter("SubFactor1", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 2:
                            Parametros[1] = new ReportParameter("SubFactor2", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 3:
                            Parametros[2] = new ReportParameter("SubFactor3", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 4:
                            Parametros[3] = new ReportParameter("SubFactor4", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 5:
                            Parametros[4] = new ReportParameter("SubFactor5", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 6:
                            Parametros[5] = new ReportParameter("SubFactor6", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 7:
                            Parametros[6] = new ReportParameter("SubFactor7", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 8:
                            Parametros[7] = new ReportParameter("SubFactor8", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 9:
                            Parametros[8] = new ReportParameter("SubFactor9", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 10:
                            Parametros[9] = new ReportParameter("SubFactor10", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 11:
                            Parametros[10] = new ReportParameter("SubFactor11", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 12:
                            Parametros[11] = new ReportParameter("SubFactor12", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 13:
                            Parametros[12] = new ReportParameter("SubFactor13", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 14:
                            Parametros[13] = new ReportParameter("SubFactor14", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 15:
                            Parametros[14] = new ReportParameter("SubFactor15", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 16:
                            Parametros[15] = new ReportParameter("SubFactor16", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 17:
                            Parametros[16] = new ReportParameter("SubFactor17", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 18:
                            Parametros[17] = new ReportParameter("SubFactor18", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 19:
                            Parametros[18] = new ReportParameter("SubFactor19", RowFiltro[0]["Rango"].ToString());
                            break;

                        case 20:
                            Parametros[19] = new ReportParameter("SubFactor20", RowFiltro[0]["Rango"].ToString());
                            break;
                    }

                }

                dtSubFactores.Rows.Add(Row);
            }

            //Limpiamos el Source del reporte
            rptPruebaKostick.LocalReport.DataSources.Clear();
            rptPruebaKostick.Reset();

            //Validamos que tipo de reporte ha seleccionado el usuario
            if (cboTipoReporte.SelectedIndex == 0)
            {

                ReportDataSource rds = new ReportDataSource("dsFactores", dtFactores);
                ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

                rptPruebaKostick.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Kostick.rptPrubaKostickResumen.rdlc";
                rptPruebaKostick.LocalReport.DataSources.Add(rds);
                rptPruebaKostick.LocalReport.DataSources.Add(rds_candidato);

            }
            else if (cboTipoReporte.SelectedIndex == 1)
            {
                ReportDataSource rds1 = new ReportDataSource("dsFactores", dtFactores);
                ReportDataSource rds2 = new ReportDataSource("dsSubFactores", dtSubFactores);
                ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

                rptPruebaKostick.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Kostick.rptPrubaKostick.rdlc";
                rptPruebaKostick.LocalReport.DataSources.Add(rds1);
                rptPruebaKostick.LocalReport.DataSources.Add(rds2);

                rptPruebaKostick.LocalReport.SetParameters(Parametros);
                rptPruebaKostick.LocalReport.DataSources.Add(rds_candidato);

            }
            else
            {

                MessageBox.Show("Debe de seleccionar un tipo de reporte", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }


            rptPruebaKostick.RefreshReport();


        }

        private void cboTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnMostrar.Enabled = true;
        }
    }
}