﻿using System;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Kostick
{
    public partial class frmKostick : Form
    {

        int NumeroAsignacion;
        DataTable dtRespuestas = new DataTable();
        DataTable dtResCandidato = new DataTable();
        int x = 0;
        int Recorrido = 0;
        DataRow[] row;
        kostick modelo_KOSTICK;

        private readonly QuickHumClient Cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        private readonly EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada;

        dto_kostick_respuestas_candidato Respuestas = new dto_kostick_respuestas_candidato();

        public frmKostick(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad = escolaridad;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnIniciarAyuda.Hide();
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {

            modelo_KOSTICK = new kostick();

            try
            {
                modelo_KOSTICK = Cliente.NuevoKostick(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            NumeroAsignacion = modelo_KOSTICK.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 90";
            this.panelAyuda.Hide();this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {

        }

        private void frmKostick_Load(object sender, EventArgs e)
        {

            //Se le da la estructura al DataTable de Respuestas del candidato.
            dtResCandidato.Columns.Add("id_respuesta_candidato");
            dtResCandidato.Columns.Add("id_respuesta");
            dtResCandidato.Columns.Add("punto");

            DateTime hora;

            CargarPreguntasRespuestas();

            //this.panelAyuda.Location = new Point(1, 1);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);

        }


        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }


        private void FinalizarPrueba()
        {
            //Validamos si hay una respuesta por parte del candidato antes de que se acabara el tiempo
            if ((rbtOpcionA.Checked == true || rbtOpcionB.Checked == true) && (btnSiguiente.Text == "Siguiente"))
            {
                guardarRespuestas();
            }

            Respuestas.NumeroPrueba = NumeroAsignacion;
            Cliente.FinalizaKostickPrueba(Respuestas);

        }

        private void CargarPreguntasRespuestas()
        {

            dto_kostick_respuestas[] ListaOpciones = Cliente.ListaKostickRespuestas();



            dtRespuestas.Columns.Add("id_respuesta");
            dtRespuestas.Columns.Add("numero_pregunta");
            dtRespuestas.Columns.Add("Descripcion");
            dtRespuestas.Columns.Add("id_subfactor");


            for (int i = 0; i < ListaOpciones.ToList().Count; i++)
            {
                DataRow row = dtRespuestas.NewRow();

                row["id_respuesta"] = Convert.ToInt32(ListaOpciones[i].IdRespuesta);
                row["numero_pregunta"] = Convert.ToInt32(ListaOpciones[i].NumeroPregunta);
                row["Descripcion"] = Convert.ToString(ListaOpciones[i].Respuesta);
                row["id_subfactor"] = Convert.ToInt32(ListaOpciones[i].IdSubFactor);


                dtRespuestas.Rows.Add(row);
            }



            rbtOpcionA.Name = dtRespuestas.Rows[0]["id_respuesta"].ToString();
            rbtOpcionB.Name = dtRespuestas.Rows[1]["id_respuesta"].ToString();


            rbtOpcionA.Text = "A) " + dtRespuestas.Rows[0]["descripcion"].ToString();
            rbtOpcionB.Text = "B) " + dtRespuestas.Rows[1]["descripcion"].ToString();



        }

        #region Validamos si hay un RadioButton seleccionado


        private void rbtOpcionA_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionB_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        #endregion


        private void btnSiguiente_Click(object sender, EventArgs e)
        {


            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                guardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    desctivarCajas(false);
                    return;
                }



                mostrarPreguntas(x);

                limpiarCampos();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Text == "Actualizar")
            {
                x++;
                ActualizarRespuestas();

                if (Recorrido <= x)
                {

                    btnSiguiente.Text = "Siguiente";
                    limpiarCampos();
                    btnSiguiente.Enabled = false;
                }

                mostrarPreguntas(x);

            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Close();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + (dtRespuestas.Rows.Count / 2);

            if (x == (dtRespuestas.Rows.Count / 2) - 1)
            {
                this.btnSiguiente.Text = "Finalizar";
            }

            btnAnterior.Enabled = true;

        }

        private void mostrarPreguntas(int i)
        {

            // se buscan las respuestas correspondientes a la pregunta en el datatable que ya se tiene en memoria
            row = dtRespuestas.Select("numero_pregunta = '" + (i + 1) + "'");

            if (row.Length > 0)
            {

                rbtOpcionA.Name = row[0]["id_respuesta"].ToString();
                rbtOpcionB.Name = row[1]["id_respuesta"].ToString();


                rbtOpcionA.Text = "A) " + row[0]["descripcion"].ToString();
                rbtOpcionB.Text = "B) " + row[1]["descripcion"].ToString();



                if (Recorrido > i)
                {
                    if (rbtOpcionA.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionA.Checked = true;
                    }
                    else if (rbtOpcionB.Name == dtResCandidato.Rows[i]["id_respuesta"].ToString())
                    {
                        rbtOpcionB.Checked = true;
                    }

                }

            }
        }

        private void limpiarCampos()
        {
            rbtOpcionA.Checked = false;
            rbtOpcionB.Checked = false;


        }

        private void guardarRespuestas()
        {

            Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
            Respuestas.Punto = 1;
            Respuestas.NumeroPrueba = Convert.ToByte(NumeroAsignacion);


            Cliente.InsertaKostickRespuesta(Respuestas);


        }

        private void ActualizarRespuestas()
        {

            Respuestas.IdRespuestaCandidato = Convert.ToInt32(dtResCandidato.Rows[x - 1]["id_respuesta_candidato"]);
            Respuestas.IdRespuesta = Convert.ToByte(RecorrerRadios());
            Respuestas.Punto = 1;
            Respuestas.NumeroPrueba = Convert.ToByte(NumeroAsignacion);

            Cliente.ActualizaKostickRespuesta(Respuestas);

        }

        private int RecorrerRadios()
        {
            int radioChecado = 0;
            if (rbtOpcionA.Checked == true)
            {
                radioChecado = Convert.ToInt32(rbtOpcionA.Name);

            }
            else if (rbtOpcionB.Checked == true)
            {

                radioChecado = Convert.ToInt32(rbtOpcionB.Name);
            }


            return radioChecado;
        }

        private void desctivarCajas(bool valor)
        {
            pnlOpciones.Enabled = valor;
        }


        private void btnAnterior_Click(object sender, EventArgs e)
        {

            //Limpiamos las respuestas anteriores.
            dtResCandidato.Clear();

            //Listamos las re`spuestas del candidato.
            dto_kostick_respuestas_candidato[] ListaRespuestas = Cliente.ListaKostickPreguntasContestadas(NumeroAsignacion);


            for (int i = 0; i < ListaRespuestas.ToList().Count; i++)
            {
                DataRow row = dtResCandidato.NewRow();

                row["id_respuesta_candidato"] = Convert.ToInt32(ListaRespuestas[i].IdRespuestaCandidato);
                row["id_respuesta"] = Convert.ToString(ListaRespuestas[i].IdRespuesta);
                row["punto"] = Convert.ToString(ListaRespuestas[i].Punto);

                dtResCandidato.Rows.Add(row);
            }

            Recorrido = dtResCandidato.Rows.Count;

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + (dtRespuestas.Rows.Count / 2);
            x--;

            mostrarPreguntas(x);
            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";
                desctivarCajas(true);
            }

            btnSiguiente.Text = "Actualizar";
            btnSiguiente.Enabled = true;

        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancelar prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Kostick", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                Cliente.CambiarEstadoPrueba(Test.KOSTICK, NumeroAsignacion, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmKostick_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}
