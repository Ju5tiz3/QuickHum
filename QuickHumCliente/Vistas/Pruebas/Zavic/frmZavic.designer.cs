﻿namespace QuickHumCliente.Vistas.Pruebas.Zavic
{
    partial class frmZavic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmZavic));
            this.txtA = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.txtC = new System.Windows.Forms.TextBox();
            this.txtD = new System.Windows.Forms.TextBox();
            this.lblIdPregunta = new System.Windows.Forms.Label();
            this.rtbD = new System.Windows.Forms.RichTextBox();
            this.rtbA = new System.Windows.Forms.RichTextBox();
            this.rtbB = new System.Windows.Forms.RichTextBox();
            this.rtbC = new System.Windows.Forms.RichTextBox();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.lblMsj = new System.Windows.Forms.Label();
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCloseAyuda = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancelarAyuda = new System.Windows.Forms.Button();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnCerrarVentanaPrueba = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.panelAyuda.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtA
            // 
            this.txtA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtA.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(178)))), ((int)(((byte)(158)))));
            this.txtA.Location = new System.Drawing.Point(546, 78);
            this.txtA.MaxLength = 1;
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(36, 24);
            this.txtA.TabIndex = 2;
            this.txtA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtA.TextChanged += new System.EventHandler(this.txtA_TextChanged);
            this.txtA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_KeyPress);
            this.txtA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtA_KeyUp);
            // 
            // txtB
            // 
            this.txtB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(178)))), ((int)(((byte)(158)))));
            this.txtB.Location = new System.Drawing.Point(546, 133);
            this.txtB.MaxLength = 1;
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(36, 24);
            this.txtB.TabIndex = 7;
            this.txtB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtB.TextChanged += new System.EventHandler(this.txtB_TextChanged);
            this.txtB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_KeyPress);
            this.txtB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtB_KeyUp);
            // 
            // txtC
            // 
            this.txtC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtC.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(178)))), ((int)(((byte)(158)))));
            this.txtC.Location = new System.Drawing.Point(546, 181);
            this.txtC.MaxLength = 1;
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(36, 24);
            this.txtC.TabIndex = 8;
            this.txtC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtC.TextChanged += new System.EventHandler(this.txtC_TextChanged);
            this.txtC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtC_KeyPress);
            this.txtC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtC_KeyUp);
            // 
            // txtD
            // 
            this.txtD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtD.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(178)))), ((int)(((byte)(158)))));
            this.txtD.Location = new System.Drawing.Point(546, 231);
            this.txtD.MaxLength = 1;
            this.txtD.Name = "txtD";
            this.txtD.Size = new System.Drawing.Size(36, 24);
            this.txtD.TabIndex = 9;
            this.txtD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtD_KeyPress);
            this.txtD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtD_KeyUp);
            // 
            // lblIdPregunta
            // 
            this.lblIdPregunta.AutoSize = true;
            this.lblIdPregunta.Location = new System.Drawing.Point(517, 295);
            this.lblIdPregunta.Name = "lblIdPregunta";
            this.lblIdPregunta.Size = new System.Drawing.Size(88, 13);
            this.lblIdPregunta.TabIndex = 67;
            this.lblIdPregunta.Text = "Pregunta N de M";
            this.lblIdPregunta.Visible = false;
            // 
            // rtbD
            // 
            this.rtbD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbD.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbD.Font = new System.Drawing.Font("Calibri", 12F);
            this.rtbD.Location = new System.Drawing.Point(13, 220);
            this.rtbD.Name = "rtbD";
            this.rtbD.ReadOnly = true;
            this.rtbD.Size = new System.Drawing.Size(520, 50);
            this.rtbD.TabIndex = 16;
            this.rtbD.Text = "";
            // 
            // rtbA
            // 
            this.rtbA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbA.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbA.Font = new System.Drawing.Font("Calibri", 12F);
            this.rtbA.Location = new System.Drawing.Point(13, 67);
            this.rtbA.Name = "rtbA";
            this.rtbA.ReadOnly = true;
            this.rtbA.Size = new System.Drawing.Size(520, 50);
            this.rtbA.TabIndex = 15;
            this.rtbA.Text = "";
            // 
            // rtbB
            // 
            this.rtbB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbB.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbB.Font = new System.Drawing.Font("Calibri", 12F);
            this.rtbB.Location = new System.Drawing.Point(13, 118);
            this.rtbB.Name = "rtbB";
            this.rtbB.ReadOnly = true;
            this.rtbB.Size = new System.Drawing.Size(520, 50);
            this.rtbB.TabIndex = 14;
            this.rtbB.Text = "";
            // 
            // rtbC
            // 
            this.rtbC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbC.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbC.Font = new System.Drawing.Font("Calibri", 12F);
            this.rtbC.Location = new System.Drawing.Point(13, 169);
            this.rtbC.Name = "rtbC";
            this.rtbC.ReadOnly = true;
            this.rtbC.Size = new System.Drawing.Size(520, 50);
            this.rtbC.TabIndex = 13;
            this.rtbC.Text = "";
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.Color.White;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtPreguntas.Location = new System.Drawing.Point(23, 14);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.Size = new System.Drawing.Size(582, 40);
            this.txtPreguntas.TabIndex = 12;
            this.txtPreguntas.Text = "";
            // 
            // lblMsj
            // 
            this.lblMsj.AutoSize = true;
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(10, 295);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(135, 18);
            this.lblMsj.TabIndex = 10;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNumPreg.Location = new System.Drawing.Point(251, 4);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(118, 19);
            this.lblNumPreg.TabIndex = 11;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel2);
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(759, 648);
            this.panelAyuda.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnCancelarAyuda);
            this.panel2.Controls.Add(this.btnIniciarAyuda);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.textBox4);
            this.panel2.Controls.Add(this.textBox3);
            this.panel2.Location = new System.Drawing.Point(57, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(654, 572);
            this.panel2.TabIndex = 71;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.btnCloseAyuda);
            this.panel4.Location = new System.Drawing.Point(618, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 71;
            // 
            // btnCloseAyuda
            // 
            this.btnCloseAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseAyuda.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCloseAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseAyuda.FlatAppearance.BorderSize = 0;
            this.btnCloseAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseAyuda.Location = new System.Drawing.Point(-3, 0);
            this.btnCloseAyuda.Name = "btnCloseAyuda";
            this.btnCloseAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCloseAyuda.TabIndex = 68;
            this.btnCloseAyuda.UseVisualStyleBackColor = false;
            this.btnCloseAyuda.Visible = false;
            this.btnCloseAyuda.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(0, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(36, 36);
            this.panel3.TabIndex = 71;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 11F);
            this.richTextBox1.Location = new System.Drawing.Point(9, 40);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(632, 218);
            this.richTextBox1.TabIndex = 62;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(91, 488);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(427, 18);
            this.label6.TabIndex = 68;
            this.label6.Text = "Como ha visto, es muy sencillo ahora continúe con los siguientes 20.";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(22, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(604, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancelarAyuda
            // 
            this.btnCancelarAyuda.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancelarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCancelarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarAyuda.Location = new System.Drawing.Point(164, 523);
            this.btnCancelarAyuda.Name = "btnCancelarAyuda";
            this.btnCancelarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnCancelarAyuda.TabIndex = 69;
            this.btnCancelarAyuda.Text = "Cancelar";
            this.btnCancelarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarAyuda.UseVisualStyleBackColor = false;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(349, 523);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(461, 54);
            this.label4.TabIndex = 62;
            this.label4.Text = "EJEMPLO:\r\nMe encuentro sentado frente a un vidrio que esta sucio, a mi no me gust" +
    "a\r\nesta situación lo que haría es:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(19, 335);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(300, 126);
            this.label5.TabIndex = 63;
            this.label5.Text = "A) Lo limpiaría.\r\n\r\nB) Le diría al licenciado que si lo manda limpiar.\r\n\r\nC) Le d" +
    "iría a la señora del aseo que lo haga.\r\n\r\nD) Me guardaría la pena y me quedaría " +
    "callado. ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(364, 438);
            this.textBox1.MaxLength = 1;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(48, 20);
            this.textBox1.TabIndex = 67;
            this.textBox1.Text = "1";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(364, 406);
            this.textBox2.MaxLength = 1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(48, 20);
            this.textBox2.TabIndex = 66;
            this.textBox2.Text = "3";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(364, 374);
            this.textBox4.MaxLength = 1;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(48, 20);
            this.textBox4.TabIndex = 65;
            this.textBox4.Text = "2";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(364, 343);
            this.textBox3.MaxLength = 1;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(48, 20);
            this.textBox3.TabIndex = 64;
            this.textBox3.Text = "4";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(759, 36);
            this.label3.TabIndex = 62;
            this.label3.Text = "PRUEBA ZAVIC";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.btnSiguiente);
            this.panel5.Controls.Add(this.btnCancelar);
            this.panel5.Controls.Add(this.btnAyuda);
            this.panel5.Controls.Add(this.btnAnterior);
            this.panel5.Location = new System.Drawing.Point(1, 379);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(606, 62);
            this.panel5.TabIndex = 69;
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(308, 13);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(121, 35);
            this.btnSiguiente.TabIndex = 0;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(482, 14);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(9, 13);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 63;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(181, 13);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(121, 35);
            this.btnAnterior.TabIndex = 67;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel6.Controls.Add(this.lblNumPreg);
            this.panel6.Location = new System.Drawing.Point(0, 354);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(608, 25);
            this.panel6.TabIndex = 70;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.BackColor = System.Drawing.Color.Transparent;
            this.gbPreguntas.Controls.Add(this.panel8);
            this.gbPreguntas.Controls.Add(this.lblIdPregunta);
            this.gbPreguntas.Controls.Add(this.lblMsj);
            this.gbPreguntas.Controls.Add(this.rtbD);
            this.gbPreguntas.Controls.Add(this.txtA);
            this.gbPreguntas.Controls.Add(this.rtbC);
            this.gbPreguntas.Controls.Add(this.txtD);
            this.gbPreguntas.Controls.Add(this.txtC);
            this.gbPreguntas.Controls.Add(this.rtbA);
            this.gbPreguntas.Controls.Add(this.txtB);
            this.gbPreguntas.Controls.Add(this.rtbB);
            this.gbPreguntas.Controls.Add(this.pictureBox2);
            this.gbPreguntas.Controls.Add(this.pictureBox3);
            this.gbPreguntas.Controls.Add(this.pictureBox4);
            this.gbPreguntas.Controls.Add(this.pictureBox5);
            this.gbPreguntas.Location = new System.Drawing.Point(0, 0);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(607, 357);
            this.gbPreguntas.TabIndex = 71;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtPreguntas);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(607, 61);
            this.panel8.TabIndex = 68;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::QuickHumCliente.Properties.RecursosPruebas.textboxCuadro;
            this.pictureBox2.Location = new System.Drawing.Point(539, 66);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.TabIndex = 73;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::QuickHumCliente.Properties.RecursosPruebas.textboxCuadro;
            this.pictureBox3.Location = new System.Drawing.Point(539, 118);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.TabIndex = 73;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::QuickHumCliente.Properties.RecursosPruebas.textboxCuadro;
            this.pictureBox4.Location = new System.Drawing.Point(539, 170);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.TabIndex = 73;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::QuickHumCliente.Properties.RecursosPruebas.textboxCuadro;
            this.pictureBox5.Location = new System.Drawing.Point(539, 222);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.TabIndex = 73;
            this.pictureBox5.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.gbPreguntas);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Location = new System.Drawing.Point(74, 77);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(607, 441);
            this.panel7.TabIndex = 72;
            // 
            // btnCerrarVentanaPrueba
            // 
            this.btnCerrarVentanaPrueba.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarVentanaPrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarVentanaPrueba.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCerrarVentanaPrueba.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarVentanaPrueba.FlatAppearance.BorderSize = 0;
            this.btnCerrarVentanaPrueba.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarVentanaPrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarVentanaPrueba.Location = new System.Drawing.Point(725, 1);
            this.btnCerrarVentanaPrueba.Name = "btnCerrarVentanaPrueba";
            this.btnCerrarVentanaPrueba.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarVentanaPrueba.TabIndex = 68;
            this.btnCerrarVentanaPrueba.UseVisualStyleBackColor = false;
            this.btnCerrarVentanaPrueba.Click += new System.EventHandler(this.btnCerrarVentanaPrueba_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.tiempo1;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(517, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 37);
            this.panel1.TabIndex = 66;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(43, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltime.Location = new System.Drawing.Point(161, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // frmZavic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(759, 684);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.btnCerrarVentanaPrueba);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(698, 692);
            this.Name = "frmZavic";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRUEBA ZABIC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmZavic_FormClosing);
            this.Load += new System.EventHandler(this.frmTest_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.TextBox txtD;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelarAyuda;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox rtbC;
        private System.Windows.Forms.RichTextBox rtbD;
        private System.Windows.Forms.RichTextBox rtbA;
        private System.Windows.Forms.RichTextBox rtbB;
        private System.Windows.Forms.Label lblIdPregunta;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnCerrarVentanaPrueba;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCloseAyuda;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}