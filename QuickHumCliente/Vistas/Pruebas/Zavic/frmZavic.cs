﻿using System;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Zavic
{
    public partial class frmZavic : Form
    {
        private readonly QuickHumClient cliente = Globales.cliente;
        private readonly Expediente _expediente = null;
        private readonly EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;


        readonly dto_zavic_respuestas_candidato respuestas = new dto_zavic_respuestas_candidato();

        readonly DataTable dtPreguntas = new DataTable();
        readonly DataTable dtRespuestas = new DataTable();
        readonly DataTable dtrespactualizar = new DataTable();

        int recorrido = 0;
        int x = 0;
        DataRow[] row;
        ZAVIC modelo_ZAVIC;
        int NumeroAsignacion;

        public frmZavic(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            _expediente = expediente;
            _escolaridad = escolaridad;
        }

        private void frmTest_Load(object sender, EventArgs e)
        {
            dto_zavic_preguntas[] listadototal = cliente.ListadoZavicPreguntas();

            dtrespactualizar.Columns.Add("id_respuesta_candidato");
            dtrespactualizar.Columns.Add("numero_prueba");
            dtrespactualizar.Columns.Add("id_respuesta");
            dtrespactualizar.Columns.Add("valor_respuesta");

            dtPreguntas.Columns.Add("pregunta_id");
            dtPreguntas.Columns.Add("pregunta");

            dtRespuestas.Columns.Add("correspondencia");
            dtRespuestas.Columns.Add("idpregunta"); ;
            dtRespuestas.Columns.Add("idrespuesta");
            dtRespuestas.Columns.Add("respuesta");

            for (int i = 0; i < listadototal.ToList().Count; i++)
            {
                DataRow fila = dtPreguntas.NewRow();
                fila["pregunta_id"] = Convert.ToInt32(listadototal[i].PreguntaId);
                fila["pregunta"] = Convert.ToString(listadototal[i].pregunta);

                for (int j = 0; j < 4; j++)
                {
                    DataRow resp = dtRespuestas.NewRow();
                    resp["correspondencia"] = Convert.ToString(listadototal[i].Respuestas[j].Correspondencia);
                    resp["idpregunta"] = Convert.ToInt32(listadototal[i].Respuestas[j].IdPregunta);
                    resp["idrespuesta"] = Convert.ToInt32(listadototal[i].Respuestas[j].IdRespuesta);
                    resp["respuesta"] = Convert.ToString(listadototal[i].Respuestas[j].Respuesta);
                    dtRespuestas.Rows.Add(resp);
                }
                dtPreguntas.Rows.Add(fila);
            }

            this.lblMsj.Text = "";
            DateTime hora;
            this.txtPreguntas.Text = "1) " + dtPreguntas.Rows[0][1].ToString();

            this.lblIdPregunta.Text = dtPreguntas.Rows[0]["pregunta_id"].ToString();
            this.rtbA.Text = "A) " + dtRespuestas.Rows[0]["respuesta"].ToString();
            this.rtbB.Text = "B) " + dtRespuestas.Rows[1]["respuesta"].ToString();
            this.rtbC.Text = "C) " + dtRespuestas.Rows[2]["respuesta"].ToString();
            this.rtbD.Text = "D) " + dtRespuestas.Rows[3]["respuesta"].ToString();

            this.txtA.Name = dtRespuestas.Rows[0]["idrespuesta"].ToString();
            this.txtB.Name = dtRespuestas.Rows[1]["idrespuesta"].ToString();
            this.txtC.Name = dtRespuestas.Rows[2]["idrespuesta"].ToString();
            this.txtD.Name = dtRespuestas.Rows[3]["idrespuesta"].ToString();

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void mostrarPreguntas(int i)
        {
            this.txtPreguntas.Text = i + 1 + ") " + dtPreguntas.Rows[i][1].ToString();
            // se buscan las respuestas correspondientes a la pregunta en el datatable que ya se tiene en memoria
            row = dtRespuestas.Select("idpregunta = '" + dtPreguntas.Rows[i][0].ToString() + "'");

            if (row.Length > 0)
            {
                this.lblIdPregunta.Text = row[0]["idpregunta"].ToString();
                this.txtA.Name = row[0]["idrespuesta"].ToString();
                this.txtB.Name = row[1]["idrespuesta"].ToString();
                this.txtC.Name = row[2]["idrespuesta"].ToString();
                this.txtD.Name = row[3]["idrespuesta"].ToString();
                this.rtbA.Text = "A) " + row[0]["respuesta"].ToString();
                this.rtbB.Text = "B) " + row[1]["respuesta"].ToString();
                this.rtbC.Text = "C) " + row[2]["respuesta"].ToString();
                this.rtbD.Text = "D) " + row[3]["respuesta"].ToString();

                if (recorrido - (3 * i) > i)
                {
                    if (i == 0)
                    {
                        txtA.Text = dtrespactualizar.Rows[0]["valor_respuesta"].ToString();
                        txtB.Text = dtrespactualizar.Rows[1]["valor_respuesta"].ToString();
                        txtC.Text = dtrespactualizar.Rows[2]["valor_respuesta"].ToString();
                        txtD.Text = dtrespactualizar.Rows[3]["valor_respuesta"].ToString();
                    }
                    else
                    {
                        txtA.Text = dtrespactualizar.Rows[i + 3 * i]["valor_respuesta"].ToString();
                        txtB.Text = dtrespactualizar.Rows[(i + 3 * i) + 1]["valor_respuesta"].ToString();
                        txtC.Text = dtrespactualizar.Rows[(i + 3 * i) + 2]["valor_respuesta"].ToString();
                        txtD.Text = dtrespactualizar.Rows[(i + 3 * i) + 3]["valor_respuesta"].ToString();
                    }
                }
            }
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_zavic_respuestas_candidato[] listarespuestascandidato = cliente.ListaZavicPreguntasContestadas(NumeroAsignacion);

            dtrespactualizar.Clear();
            for (int w = 0; w < listarespuestascandidato.ToList().Count; w++)
            {
                DataRow filas = dtrespactualizar.NewRow();

                filas["id_respuesta_candidato"] = Convert.ToInt32(listarespuestascandidato[w].IdRespuestaTabla);
                filas["numero_prueba"] = Convert.ToInt32(listarespuestascandidato[w].NumeroPrueba);
                filas["id_respuesta"] = Convert.ToInt32(listarespuestascandidato[w].IdRespuesta);
                filas["valor_respuesta"] = Convert.ToInt32(listarespuestascandidato[w].ValorRespuseta);
                dtrespactualizar.Rows.Add(filas);

            }
            recorrido = dtrespactualizar.Rows.Count;
            btnSiguiente.Text = "Siguiente.";
            btnSiguiente.Enabled = true;
            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + dtPreguntas.Rows.Count;
            x--;
            mostrarPreguntas(x);
            if (x == 0)
                btnAnterior.Enabled = false;
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";
                desctivarCajas(true);
            }
        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {

                if (this.txtA.Text.Trim() == "" || this.txtB.Text.Trim() == "" || this.txtC.Text.Trim() == "" || this.txtD.Text.Trim() == "")
                {
                    this.lblMsj.Text = "Es necesario que todos los campos estén llenos.";
                    return;
                }
                else
                { x++; guardarRespuestas(); }

                this.btnSiguiente.Enabled = false;
                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                    desctivarCajas(false);
                    return;
                }
                mostrarPreguntas(x);
                this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;
                limpiarCampos();
            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                x++;
                ActualizarRespuestas();
                this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + dtPreguntas.Rows.Count;
                mostrarPreguntas(x);
                if (recorrido - (3 * x) <= x)
                {
                    this.btnSiguiente.Enabled = false;
                    btnSiguiente.Text = "Siguiente";
                    limpiarCampos();
                }
            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba Zavic", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Dispose();
                }
            }

            if (x == dtPreguntas.Rows.Count - 1)
                this.btnSiguiente.Text = "Finalizar";
            btnAnterior.Enabled = true;
        }

        private void FinalizarPrueba()
        {
            if (btnSiguiente.Text == "Siguiente" || btnSiguiente.Text == "Finalizar")
            {
                if (this.txtA.Text.Length == 0 || this.txtB.Text.Length == 0 || this.txtC.Text.Length == 0 || this.txtD.Text.Length == 0)
                {
                }
                else
                {
                    x++;
                    guardarRespuestas();
                }
            }
            respuestas.NumeroPrueba = NumeroAsignacion;
            cliente.FinalizaZavicPrueba(respuestas);
            this.Dispose();
        }
        private void desctivarCajas(bool valor)
        {
            this.txtA.Enabled = valor;
            this.txtB.Enabled = valor;
            this.txtC.Enabled = valor;
            this.txtD.Enabled = valor;
        }
        private void guardarRespuestas()
        {
            respuestas.IdRespuesta = Convert.ToInt32(txtA.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtA.Text);
            respuestas.NumeroPrueba = (NumeroAsignacion);
            cliente.InsertaZavicRespuesta(respuestas);

            respuestas.IdRespuesta = Convert.ToInt32(txtB.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtB.Text);
            respuestas.NumeroPrueba = (NumeroAsignacion);
            cliente.InsertaZavicRespuesta(respuestas);

            respuestas.IdRespuesta = Convert.ToInt32(txtC.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtC.Text);
            respuestas.NumeroPrueba = (NumeroAsignacion);
            cliente.InsertaZavicRespuesta(respuestas);

            respuestas.IdRespuesta = Convert.ToInt32(txtD.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtD.Text);
            respuestas.NumeroPrueba = (NumeroAsignacion);
            cliente.InsertaZavicRespuesta(respuestas);
        }

        private void ActualizarRespuestas()
        {
            respuestas.IdRespuestaTabla = Convert.ToInt32(dtrespactualizar.Rows[((4 * x) - 4)]["id_respuesta_candidato"]);
            respuestas.IdRespuesta = Convert.ToInt32(txtA.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtA.Text);
            cliente.ActualizaZavicRespuesta(respuestas);

            respuestas.IdRespuestaTabla = Convert.ToInt32(dtrespactualizar.Rows[((4 * x) - 4) + 1]["id_respuesta_candidato"]);
            respuestas.IdRespuesta = Convert.ToInt32(txtB.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtB.Text);
            cliente.ActualizaZavicRespuesta(respuestas);

            respuestas.IdRespuestaTabla = Convert.ToInt32(dtrespactualizar.Rows[((4 * x) - 4) + 2]["id_respuesta_candidato"]);
            respuestas.IdRespuesta = Convert.ToInt32(txtC.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtC.Text);
            cliente.ActualizaZavicRespuesta(respuestas);

            respuestas.IdRespuestaTabla = Convert.ToInt32(dtrespactualizar.Rows[((4 * x) - 4) + 3]["id_respuesta_candidato"]);
            respuestas.IdRespuesta = Convert.ToInt32(txtD.Name);
            respuestas.ValorRespuseta = Convert.ToInt32(txtD.Text);
            cliente.ActualizaZavicRespuesta(respuestas);
        }

        private void limpiarCampos()
        {
            foreach (Control text in this.gbPreguntas.Controls)
            {
                if (text is TextBox)
                {
                    text.Text = "";
                }
            }
            this.txtA.Focus();
        }

        private void validarCamposLlenos()
        {
            if (this.txtA.Text.Length == 0 || this.txtB.Text.Length == 0 || this.txtC.Text.Length == 0 || this.txtD.Text.Length == 0)
            {
                this.btnSiguiente.Enabled = false;
                return;
            }
            else
            {
                this.btnSiguiente.Enabled = true;
                this.btnSiguiente.Focus();
            }

            // validar campos con el mismo valor
            int[] datos;
            int i = 0;
            datos = new int[4];
            foreach (Control text in this.gbPreguntas.Controls)
            {
                if (text is TextBox)
                {
                    datos[i] = Convert.ToInt32(text.Text);
                    i++;
                }
            }
            Array.Sort(datos);
            for (i = 1; i < 4; i++)
            {
                if (datos[i] == datos[i - 1])
                {
                    this.lblMsj.Text = "El número: " + datos[i] + " está repetido. \nRecuerde que no se pueden repetir los números y solo puede usar del 1 al 4.";
                    this.btnSiguiente.Enabled = false;
                    return;
                }
                else
                {
                    this.lblMsj.Text = "";
                }
            }
            // fin de validación de campos con el mismo valor.          
        }

        // función para permitir solo numeros
        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }
            // bloquear las teclas 0, y del 5 al 9
            if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D5) || (ex.KeyChar == (char)Keys.D6) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ex.Handled = true;
                return;
            }
        }

        #region "Eventos de validación Keypress"
        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtD_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }
        #endregion
        #region "Verificar que todos los campos estén llenos con el evento KeyUp"
        private void txtA_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
        }

        private void txtB_KeyUp(object sender, KeyEventArgs e)
        {
            if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control) + Convert.ToInt32(Keys.D5))
            {
                this.lblMsj.Text = "El valor que desea ingresar no es válido.";
            }
            validarCamposLlenos();
        }

        private void txtC_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
        }

        private void txtD_KeyUp(object sender, KeyEventArgs e)
        {
            validarCamposLlenos();
        }
        #endregion

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            modelo_ZAVIC = new ZAVIC();
            try
            {
                modelo_ZAVIC = cliente.NuevoZavic(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            prueba_iniciada = true;
            NumeroAsignacion = modelo_ZAVIC.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 20";
            this.panelAyuda.Hide();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.txtA.Focus();
            this.tiempo.Enabled = true;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCloseAyuda.Show();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;
        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
                FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void txtA_TextChanged(object sender, EventArgs e)
        {
            this.txtB.Focus();
        }

        private void txtB_TextChanged(object sender, EventArgs e)
        {
            this.txtC.Focus();
        }

        private void txtC_TextChanged(object sender, EventArgs e)
        {
            this.txtD.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Se ejecuta cuando se presiona el boton de cerrar de la ventana
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCerrarVentanaPrueba_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela el proceso de la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Zavic", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if(prueba_iniciada)
                cliente.CambiarEstadoPrueba(Test.MOSS, NumeroAsignacion, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void frmZavic_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
    }
}