﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Zavic
{
    public partial class frmZavicreporte : Form
    {
        QuickHumClient cliente = Globales.cliente;
        string EcoAlto, Ecobajo, polalto, polbajo, relialto, relibajo, socalto, socbajo, coralto, corbajo, inalto, inbajo, legalto, legbajo, moralto, morbajo;
        string economico, politico, religioso, social, corrupto, indiferencia, legal, moral;
        DataTable dtresultados = new DataTable();
        DataTable dtinteres = new DataTable();
        DataTable dtCandidato = new DataTable();
        private Expediente _expediente = null;

        public frmZavicreporte(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }
        int NumeroAsignacion;
        private void frmZavicreporte_Load(object sender, EventArgs e)
        {
            dto_bfq_preguntas[] listar = cliente.ListadoBFQPreguntas();
            dtresultados.Columns.Add("Correspondencia");
            dtresultados.Columns.Add("Suma");
            dtresultados.Columns.Add("ValoresInteres");

            dtinteres.Columns.Add("Correspondencia");
            dtinteres.Columns.Add("Suma");
            dtinteres.Columns.Add("ValoresInteres");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            Interpretaciones();
            CargarReporte();
            // this.reportViewer1.RefreshReport();
        }

        private void CargarReporte()
        {
            dtresultados.Clear();

            dto_zavic_resultados[] reporte = cliente.ListaZavicResultados(_expediente.Id);
            for (int i = 0; i < reporte.ToList().Count; i++)
            {


                DataRow row = dtresultados.NewRow();
                switch (Convert.ToString(reporte[i].Correspondencia).Trim())
                {
                    case "Economico":
                        if (reporte[i].Suma > 24)
                        { economico = EcoAlto; }
                        else
                        { economico = Ecobajo; }
                        break;

                    case "Politico":
                        if (reporte[i].Suma > 24)
                        { politico = polalto; }
                        else
                        { politico = polbajo; }
                        break;

                    case "Religioso":
                        if (reporte[i].Suma > 24)
                        { religioso = relialto; }
                        else
                        { religioso = relibajo; }
                        break;

                    case "Social":
                        if (reporte[i].Suma > 24)
                        { social = socalto; }
                        else
                        { social = socbajo; }
                        break;

                    case "Corrupto":
                        if (reporte[i].Suma > 24)
                        { corrupto = coralto; }
                        else
                        { corrupto = corbajo; }
                        break;

                    case "Indiferencia":
                        if (reporte[i].Suma > 24)
                        { indiferencia = inalto; }
                        else
                        { indiferencia = inbajo; }
                        break;

                    case "Legalidad":
                        if (reporte[i].Suma > 24)
                        { legal = legalto; }
                        else
                        { legal = legbajo; }
                        break;

                    case "Moral":
                        if (reporte[i].Suma > 24)
                        { moral = moralto; }
                        else
                        { moral = morbajo; }
                        break;

                }

                row["Correspondencia"] = Convert.ToString(reporte[i].Correspondencia);
                row["Suma"] = Convert.ToInt32(reporte[i].Suma);
                row["ValoresInteres"] = Convert.ToString(reporte[i].ValoresInteres);


                dtresultados.Rows.Add(row);
            }

            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);

            ReportParameter[] parametro = new ReportParameter[9];
            parametro[0] = new ReportParameter("DUI", this._expediente.Candidato.Dui);
            parametro[1] = new ReportParameter("Moral", moral);
            parametro[2] = new ReportParameter("Legalidad", legal);
            parametro[3] = new ReportParameter("Indiferencia", indiferencia);
            parametro[4] = new ReportParameter("Corrupcion", corrupto);
            parametro[5] = new ReportParameter("Economico", economico);
            parametro[6] = new ReportParameter("Politico", politico);
            parametro[7] = new ReportParameter("Social", social);
            parametro[8] = new ReportParameter("Religioso", religioso);




            ReportDataSource rds = new ReportDataSource("DataSetZavic", dtresultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Zavic.reportezavic.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            reportViewer1.LocalReport.SetParameters(parametro);
            reportViewer1.RefreshReport();
        }


        private void Interpretaciones()
        {
            moralto = "ALTO MORAL – SEGUIR LAS NORMAS DE UN BUEN COMPORTAMIENTO: Un puesto que requiere un fuerte valor moral que se someta a las reglas que establece la familia, sociedad o algún organismo, para realizar el bien y no el mal.";
            morbajo = "BAJO MORAL: Un puesto que requiere un bajo valor moral, es aquella persona que no seguirá los lineamientos o normas que se esté llevando a cabo, ya sea en un ambiente familiar o social, seguro de sus propias convicciones sin saber si está actuando bien o mal.";
            legalto = "ALTO LEGALIDAD: – DESEO DE HONESTIDAD: Un puesto que requiere un alto valor legal, será aquella persona que será honesta y honrada, acopiándose a las políticas que regirá la empresa, será un empleado que será “fiel” a las ordenes que dictamine el jefe inmediato.";
            legbajo = "BAJO LEGALIDAD: Un puesto que requiere un bajo valor legal, es aquella persona que se mostrará con doble intención, sacando provecho de las situaciones para sus propios fines, ya sea personales o laborales, no será tan recta ni se acoplará a las reglas que estipule la empresa y seguirá sus propias convicciones y actuara por lo general solo en sus tomas de decisiones.";
            inalto = "ALTO INDIFERENCIA: – DESEO DE INCUMPLIMIENTO: Un puesto que requiere un valor alto de indiferencia, será una persona que denota un bajo rendimiento productivo, talvez por la desmotivación o por el estado de animo que aqueje a su comportamiento, no tomará las cosas muy en serio.";
            inbajo = "BAJO INDIFERENCIA: Un puesto que requiere un bajo valor de indiferencia, será una persona con bastante entusiasmo en hacer las cosas, siempre buscará la motivación del logro, y buscara los éxitos y los retos, tendrá una producción alta y trabajará sobre los detalles.";
            coralto = "ALTO CORRUPTO: – DESEO DE COHECHO: Un puesto que requiere un alto valor de corrupto, será una persona que le agrada “seducir, incomodar, fastidiar, alterar un escrito”, etc. es ir en búsqueda del cohecho y corromper las reglas y/o normas que rige ya sea la sociedad, la familia o las empresas, es ir en contra de las buenas costumbres y no acatar costumbres y no acatar los valores morales.";
            corbajo = "BAJO CORRUPTO: Un puesto que requiere un bajo valor de corrupción, será una persona que buscará el éxito a través de sus propios logros sin corromper a terceros, y será una persona que se acopiará a los lineamientos que le regirán en su comportamiento, una persona que se rige por los valores morales y legalidad.";
            EcoAlto = "ALTO ECONOMICO – DESEO DE RIQUEZA: Un puesto que requiere un fuerte valor económico, necesita una persona que esté interesada en ser juzgada por su habilidad para lograr utilidades, técnicas de reducción de costos, o habilidad para resolver problemas prácticos y un fuerte enfoque respecto al retorno sobre la inversión o n la efectividad de costos.";
            Ecobajo = "BAJO ECONOMICO: Un puesto que requiere un valor económico bajo, necesita un individuo que prefiera servir por encima de cualquier utilidad y prefiera no hacer caso de las consideraciones materiales.";
            polalto = "ALTO POLÍTICO: – BUSCANDO PODER SOBRE OTROS: Un puesto que requiere un alto valor político, tiende a demandar gente ambiciosa que quiera llegar lejos en una organización.  Es importante buscar la promoción en este tipo de posiciones.  En compañías que están orientadas a la utilidad, los mandos intermedios tienen la característica  de lucha para estar a la cabeza por el deseo de ganar.   Sin embargo, en organizaciones mas democráticas o participativas, este valor es de menor importancia.";
            polbajo = "BAJO POLÍTICO:. Personas que demandan el trabajo en equipo y la cooperación. Asistencias de Staff o coordinadores en una organización democrática, puede provocar la ira de los líderes si ellos manifiestan comportamientos ambiciosos en sus roles.";
            socalto = "ALTO SOCIAL: – PREOCUPACIÓN POR LAS PERSONAS: Un puesto que requiera un alto valor social está enfocado principalmente en el genuino interés por las personas.  Típicamente los directores de recursos humanos, gerentes de relaciones públicas, de comunicaciones o médicos, etc. deben manifestar este tipo de valores.  El énfasis está en la preocupación real por ayudar a las personas, lo cual significa también que será un buen miembro para conformar equipos de trabajo";
            socbajo = "BAJO SOCIAL: Los puestos que requieren un valor social bajo, sugieren enfoques poco sentimentales en tareas difíciles.  Las decisiones para obtener éxito son tomadas sin considerar el impacto que causen en las personas.  Un ejecutivo que tiene un gran número de trabajadores debe estar principalmente preocupado en la supervivencia del negocio y puede carecer de importancia el crear desempleo o afectar a sus trabajadores.";
            relialto = "ALTO RELIGIOSO: – REGULATORIO Y RESPETO A LA AUTORIDAD:  Un puesto que requiere un alto valor regulatorio demanda disciplina, estructura y orden, un amplio sentido de reglas y consideraciones morales, tales como violaciones a la ley o el cumplimiento de contratos, pueden ser características importantes para este puesto.";
            relibajo = "BAJO RELIGIOSO: Un puesto que requiere un bajo valor regulatorio demanda un alto grado de libertad para operar de manera indisciplinada.  En el mundo moderno, estos puestos cada vez menos, pero algunos puestos de ventas por ejemplo, entretenimiento y artísticos, permiten acciones que no requieren de disciplina para lograr el éxito.";
        }
    }
}
