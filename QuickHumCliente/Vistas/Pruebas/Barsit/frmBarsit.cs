﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Barsit
{
    public partial class frmBarsit : Form
    {
        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;

        dto_barsit_respuestas_candidato Respuestas = new dto_barsit_respuestas_candidato();
        dto_barsit_preguntas[] ListaPreguntas;
        dto_barsit_respuestas_candidato[] RespuestasCandidato;
        //Variable que funcionara como parametro
        int Numero_prueba;
        DataRow[] row;
        BARSIT Model_BARSIT;

        int RespuestaCorrecta = 0;
        int Recorrido = 0;
        int x = 0;
        int TipoPregunta = 0;
        int Punto = 0;

        public frmBarsit(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }


        private void frmPruebaBarsit_Load(object sender, EventArgs e)
        {            
            DateTime hora;

            //Cargamos preguntas y respuestas            
            ListaPreguntas = Cliente.ListarBarsitPreguntas();
            mostrarPreguntas(Recorrido);

          //  CargarPreguntasRespuestas();
            this.panelAyuda.Location = new Point(1, 1);
            pnlLogica.Location = new Point(13, 107);
            this.lblMsj.Text = "";

            hora = Convert.ToDateTime("0:20:00");
            conthoras = hora.Hour;
            contminutos = hora.Minute;
            contsegundos = hora.Second;
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }
        
        private void mostrarPreguntas(int i)
        {    
               //Determinamos si la pregunta es de seleccion = 1 O de Logica = 2
                TipoPregunta = ListaPreguntas[i].Tipo;
                ValidarTipoPregunta();

            //Mostramos las preguntas y las opciones de respuesta
                this.txtPreguntas.Text = i + 1 + " ) " + ListaPreguntas[i].Pregunta;
                if (TipoPregunta == 1)
                {
                    rbtOpcionA.Name = ListaPreguntas[i].Respuestas[0].IdRespuesta.ToString();
                    rbtOpcionB.Name = ListaPreguntas[i].Respuestas[1].IdRespuesta.ToString();
                    rbtOpcionC.Name = ListaPreguntas[i].Respuestas[2].IdRespuesta.ToString();
                    rbtOpcionD.Name = ListaPreguntas[i].Respuestas[3].IdRespuesta.ToString();
                    rbtOpcionE.Name = ListaPreguntas[i].Respuestas[4].IdRespuesta.ToString();

                    rbtOpcionA.Text = "A) " + ListaPreguntas[i].Respuestas[0].Respuesta;
                    rbtOpcionB.Text = "B) " + ListaPreguntas[i].Respuestas[1].Respuesta;
                    rbtOpcionC.Text = "C) " + ListaPreguntas[i].Respuestas[2].Respuesta;
                    rbtOpcionD.Text = "D) " + ListaPreguntas[i].Respuestas[3].Respuesta;
                    rbtOpcionE.Text = "E) " + ListaPreguntas[i].Respuestas[4].Respuesta;

                    for (int j = 0; j < 5; j++)
                    {
                        if (ListaPreguntas[i].Respuestas[j].RespuestaCorrecta == "si")
                        {
                            RespuestaCorrecta = Convert.ToInt32(ListaPreguntas[i].Respuestas[j].IdRespuesta);
                        }
                    }

                }           

            //Verificamos si el usuario ah retrocedido preguntas
                if (Recorrido > i)
                {
                    if (TipoPregunta == 1)
                    {

                        if (rbtOpcionA.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                        {
                            rbtOpcionA.Checked = true;
                        }
                        else if (rbtOpcionB.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                        {
                            rbtOpcionB.Checked = true;
                        }
                        else if (rbtOpcionC.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                        {
                            rbtOpcionC.Checked = true;
                        }
                        else if (rbtOpcionD.Name == RespuestasCandidato[i].IdRespuesta.ToString())
                        {
                            rbtOpcionD.Checked = true;
                        }

                    }
                    else
                    {
                        //Dividimos las respuestas anteriormente almacenadas.
                        string[] Numeros = RespuestasCandidato[i].Respuesta.Split('-');

                        rtbResA.Text = Numeros[0];
                        rtbResB.Text = Numeros[1];
                    }
                }
        }
        
        private void btnSiguiente_Click(object sender, EventArgs e)
        {         

            if (btnSiguiente.Text == "Siguiente")
            {
                x++;
                GuardarRespuestas();
                this.btnSiguiente.Enabled = false;

                if (this.btnSiguiente.Text == "Finalizar")
                {
                    this.btnSiguiente.Enabled = false;
                   
                    return;
                }

                if (x == ListaPreguntas .ToArray().Count() - 1)
                {
                    this.btnSiguiente.Text = "Finalizar";
                }

                mostrarPreguntas(x);

                LimpiarControles();
                btnSiguiente.Enabled = false;
            }
            else if (btnSiguiente.Text == "Actualizar")
            {
                x++;  
                ActualizarRespuestas();
                if (Recorrido <= x)
                {
                    btnSiguiente.Text = "Siguiente";
                    LimpiarControles();
                    btnSiguiente.Enabled = false;
                }

                mostrarPreguntas(x);

            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult DiagResul = MessageBox.Show("¿Esta seguro que desea terminar la prueba?", "Prueba Moss", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (DiagResul == DialogResult.Yes)
                {
                    FinalizarPrueba();
                    this.Close();
                }

            }

            this.lblNumPreg.Text = "Pregunta: " + (x + 1) + " de " + ListaPreguntas.ToArray().Count().ToString();

            btnAnterior.Enabled = true;

        }
        
        private void ActualizarRespuestas()
        {

            Respuestas.IdRespuestaTabla = Convert.ToInt32 (RespuestasCandidato[x -1].IdRespuestaTabla);

            //Validamos que tipo de respuesta se actualizara.
            if (TipoPregunta == 1)
            {
                Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
                Respuestas.Punto = Convert.ToByte(Punto);
                Respuestas.Respuesta = "";

            }
            else
            {
                Respuestas.IdRespuesta = RespuestaCorrecta;
                Respuestas.Punto = Convert.ToByte(ValidarRespuestaLogica());
                Respuestas.Respuesta = rtbResA.Text.Trim() + "-" + rtbResB.Text.Trim();
            }    
                                        
         
            Cliente.ActualizaRespuestaBarsit(Respuestas);

        }
        
        private void LimpiarControles()
        {
            rbtOpcionA.Checked = false;
            rbtOpcionB.Checked = false;
            rbtOpcionC.Checked = false;
            rbtOpcionD.Checked = false;
            rbtOpcionE.Checked = false;

            rtbResA.Clear();
            rtbResB.Clear();

        }

        private void GuardarRespuestas()
        {
            Respuestas.NumeroPrueba = Numero_prueba;

            //Validamos que tipo de respuesta se insertara.
            if (TipoPregunta == 1)
            {
                Respuestas.IdRespuesta = Convert.ToInt32(RecorrerRadios());
                Respuestas.Punto = Convert.ToByte(Punto);               
                Respuestas.Respuesta = "";

            }
            else
            {
                Respuestas.IdRespuesta = RespuestaCorrecta;
                Respuestas.Punto = Convert.ToByte(ValidarRespuestaLogica());
                Respuestas.Respuesta = rtbResA.Text.Trim() + "-" + rtbResB.Text.Trim(); 
            }           

            Cliente.InsertaRespuestaBarsit(Respuestas);   
        }

        #region Validamos las respuestas correctas tanto en las preguntas de seleccion como logicas

        private int ValidarRespuestaLogica()
        {
            string RespuestaCandidato = rtbResA.Text.Trim() + "-" + rtbResB.Text.Trim();

            if (RespuestaCandidato == ListaPreguntas[x - 1].Respuestas[0].Respuesta.ToString())
            {
                Punto = 1;
            }
            else
            {
                Punto = 0;
            }

            return Punto;
        }

        private int RecorrerRadios()
        {
            int radioChecado = 0;
            if (rbtOpcionA.Checked == true)
            {
                if (rbtOpcionA.Name == RespuestaCorrecta.ToString())
                {
                    Punto = 1;
                }
                else
                {
                    Punto = 0;
                }
                radioChecado = Convert.ToInt32(rbtOpcionA.Name);


            }
            else if (rbtOpcionB.Checked == true)
            {

                if (rbtOpcionB.Name == RespuestaCorrecta.ToString())
                {
                    Punto = 1;
                }
                else
                {
                    Punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionB.Name);
            }
            else if (rbtOpcionC.Checked == true)
            {

                if (rbtOpcionC.Name == RespuestaCorrecta.ToString())
                {
                    Punto = 1;
                }
                else
                {
                    Punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionC.Name);
            }
            else if (rbtOpcionD.Checked == true)
            {

                if (rbtOpcionD.Name == RespuestaCorrecta.ToString())
                {
                    Punto = 1;
                }
                else
                {
                    Punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionD.Name);
            }
            else if (rbtOpcionE.Checked == true)
            {

                if (rbtOpcionE.Name == RespuestaCorrecta.ToString())
                {
                    Punto = 1;
                }
                else
                {
                    Punto = 0;
                }

                radioChecado = Convert.ToInt32(rbtOpcionD.Name);
            }


            return radioChecado;
        }

        #endregion
       
        private void ValidarTipoPregunta()
        {
            if (TipoPregunta == 1)
            {
                pnlLogica.Visible = false;
                pnlSeleccion.Visible = true;
            }
            else
            {
                pnlLogica.Visible = true;
                pnlSeleccion.Visible = false; 
            }
            
        }
        
        private int conthoras = 0;
        private int contminutos = 0;
        private int contsegundos = 0;

        private void tiempo_Tick(object sender, EventArgs e)
        {
            contsegundos = contsegundos - 1;
            if ((contsegundos == -1))
            {
                contminutos = contminutos - 1;
                contsegundos = 59;
            }
            if ((contminutos == -1) && (conthoras > 0))
            {
                conthoras = conthoras - 1;
                contminutos = 59;
            }
            if (conthoras == 0 && contminutos == 0 && contsegundos == 0)
            {
                tiempo.Enabled = false;
                MessageBox.Show("Su tiempo se ha terminado", "Advertencia");
               FinalizarPrueba();
                this.Close();
            }
            lbltime.Text = Convert.ToString(conthoras) + " : " + Convert.ToString(contminutos) + " : " + Convert.ToString(contsegundos);
        }

        private void FinalizarPrueba()
        {
            //Validamos si el usuario a respondido una ultioma respuesta antes de que se acabara el tiempo
            if (((rbtOpcionA.Checked == true || rbtOpcionB.Checked == true || rbtOpcionC.Checked == true || rbtOpcionD.Checked == true || rbtOpcionE.Checked == true) || (rtbResA .Text .Trim().Length > 0 && rtbResB .Text .Trim().Length > 0))  && (btnSiguiente .Text == "Siguiente"))
            {
                GuardarRespuestas();
            }

            Respuestas.NumeroPrueba = Numero_prueba;
            Cliente.FinalizaPruebaBarsit(Respuestas);
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            this.gbPreguntas.Enabled = false;
            this.panelAyuda.Show();
            this.btnCerrarAyuda.Show();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {

            Model_BARSIT = new BARSIT();
            try
            {
                Model_BARSIT = Cliente.NuevoBarsit(this._expediente.Numero, this._escolaridad);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al momento de crear la prueba: " + ex.Message, "Creacion prueba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }
            Numero_prueba = Model_BARSIT.IdPrueba;
            this.lblNumPreg.Text = "Pregunta: 1 de 60";
            this.panelAyuda.Hide();
            this.btnCancelarAyuda.Hide();
            this.btnIniciarAyuda.Hide();
            this.btnAyuda.Show();
            this.tiempo.Enabled = true;
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
           
        }        
    

        #region Validar Solo numeros


        private void rtbResA_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void rtbResB_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }

        }

        #endregion

        #region Validar si hay un RadioButton Seleccionado
       

        private void rbtOpcionA_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionB_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionC_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionD_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }

        private void rbtOpcionE_CheckedChanged(object sender, EventArgs e)
        {
            btnSiguiente.Enabled = true;
        }


        #endregion

        #region Validamos si en las respuestas de complementar se introducen ambas respuestas

        private void rtbResA_TextChanged(object sender, EventArgs e)
        {
            if (rtbResA.Text.Trim().Length > 0 && rtbResB.Text.Trim().Length > 0)
            {
                btnSiguiente.Enabled = true;
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        private void rtbResB_TextChanged(object sender, EventArgs e)
        {
            if (rtbResA.Text.Trim().Length > 0 && rtbResB.Text.Trim().Length > 0)
            {
                btnSiguiente.Enabled = true;
            }
            else
            {
                btnSiguiente.Enabled = false;
            }
        }

        #endregion     

        private void btnAnterior_Click(object sender, EventArgs e)
        {            
            RespuestasCandidato = Cliente.ListarPreguntasContestadasBarsit(Numero_prueba);
            Recorrido = RespuestasCandidato.ToArray().Count();            

            this.lblNumPreg.Text = "Pregunta: " + (x) + " de " + ListaPreguntas.ToArray().Count();
            x--;

            mostrarPreguntas(x);

            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (this.btnSiguiente.Text == "Finalizar")
            {
                this.btnSiguiente.Text = "Siguiente";               
            }

            btnSiguiente.Text = "Actualizar";
            btnSiguiente.Enabled = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        /// <summary>
        /// Cancela la prueba
        /// </summary>
        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba Terman", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            Cliente.CambiarEstadoPrueba(Test.BARSIT, Numero_prueba, PruebaEstado.CANCELADA);
            this.Close();
        }

        private void frmBarsit_FormClosing(object sender, FormClosingEventArgs e)
        {
            tiempo.Enabled = false;
        }
     

    }
}
