﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Barsit
{
    public partial class frmReporteBarsit : Form
    {
        QuickHumClient Cliente = Globales.cliente;
        dto_barsit_resultados Resultados;
        private Expediente _expediente = null;

        DataTable dtCandidato = new DataTable();
        DataTable dtResultados = new DataTable();

        public frmReporteBarsit(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

       
        private void frmReporteBarsit_Load(object sender, EventArgs e)
        {

            dtResultados.Columns.Add("TotalPuntos");
            dtResultados.Columns.Add("PN");
            dtResultados.Columns.Add("Rango");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");
            CargarReporte();
          
        }

        private void CargarReporte()
        {
            Resultados = Cliente.ListaBarsitResultados(_expediente.Id);

            DataRow row = dtResultados.NewRow();
            row["TotalPuntos"] = Convert.ToInt32(Resultados.TotalPuntos);
            row["PN"] = Convert.ToInt32(Resultados.PuntuacionNormalizada);
            row["Rango"] = Resultados.Rango.ToString();

            dtResultados.Rows.Add(row);

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);


            ReportDataSource rds = new ReportDataSource("DataSet1", dtResultados);
            ReportDataSource rds_candidato = new ReportDataSource("dsCandidatos", dtCandidato);
            reportViewer1.LocalReport.DataSources.Clear();
            //rptPruebaMoss.LocalReport.ReportPath = "rptPruebaMoss.rdlc";
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Barsit.rptPruebaBarsit.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(rds_candidato);
            //   reportViewer1 .LocalReport.SetParameters(Parametros);
            this.reportViewer1.RefreshReport();
        }

     
       
    }
}
