﻿namespace QuickHumCliente.Vistas.Pruebas.Barsit
{
    partial class frmBarsit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBarsit));
            this.label3 = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.btnCancelarAyuda = new System.Windows.Forms.Button();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tiempo = new System.Windows.Forms.Timer(this.components);
            this.lblNumPreg = new System.Windows.Forms.Label();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.lblMsj = new System.Windows.Forms.Label();
            this.pnlSeleccion = new System.Windows.Forms.Panel();
            this.rbtOpcionC = new System.Windows.Forms.RadioButton();
            this.rbtOpcionE = new System.Windows.Forms.RadioButton();
            this.rbtOpcionD = new System.Windows.Forms.RadioButton();
            this.rbtOpcionA = new System.Windows.Forms.RadioButton();
            this.rbtOpcionB = new System.Windows.Forms.RadioButton();
            this.pnlLogica = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rtbResB = new System.Windows.Forms.RichTextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rtbResA = new System.Windows.Forms.RichTextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.panelAyuda.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.pnlSeleccion.SuspendLayout();
            this.pnlLogica.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(617, 36);
            this.label3.TabIndex = 65;
            this.label3.Text = "PRUEBA BARSIT";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel2);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(617, 539);
            this.panelAyuda.TabIndex = 77;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.btnCancelarAyuda);
            this.panel2.Controls.Add(this.btnIniciarAyuda);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(14, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(588, 487);
            this.panel2.TabIndex = 72;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(1, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(36, 36);
            this.panel3.TabIndex = 75;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.btnCerrarAyuda);
            this.panel4.Location = new System.Drawing.Point(552, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 74;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = global::QuickHumCliente.Properties.Resources.close;
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(-2, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 76;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(26, 80);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(534, 293);
            this.tabControl1.TabIndex = 71;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.radioButton7);
            this.tabPage1.Controls.Add(this.radioButton8);
            this.tabPage1.Controls.Add(this.radioButton1);
            this.tabPage1.Controls.Add(this.radioButton5);
            this.tabPage1.Controls.Add(this.radioButton6);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(526, 267);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ejemplo 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton7.Location = new System.Drawing.Point(33, 142);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(248, 29);
            this.radioButton7.TabIndex = 72;
            this.radioButton7.Text = "Casa";
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton8.Location = new System.Drawing.Point(33, 212);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(248, 29);
            this.radioButton8.TabIndex = 73;
            this.radioButton8.Text = "Animal";
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // radioButton1
            // 
            this.radioButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(33, 177);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(248, 29);
            this.radioButton1.TabIndex = 74;
            this.radioButton1.Text = "Fruto";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(33, 71);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(248, 29);
            this.radioButton5.TabIndex = 75;
            this.radioButton5.Text = "Libro";
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // radioButton6
            // 
            this.radioButton6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(33, 106);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(248, 29);
            this.radioButton6.TabIndex = 76;
            this.radioButton6.Text = "Piedra";
            this.radioButton6.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F);
            this.label4.Location = new System.Drawing.Point(4, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(304, 38);
            this.label4.TabIndex = 62;
            this.label4.Text = "EJEMPLO:\r\nQue palabra nos dice lo que es una manzana.\r\n";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(526, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ejemplo 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel10.BackgroundImage")));
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel10.Controls.Add(this.richTextBox3);
            this.panel10.Location = new System.Drawing.Point(264, 121);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(89, 80);
            this.panel10.TabIndex = 78;
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.Color.White;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(15, 21);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(59, 39);
            this.richTextBox3.TabIndex = 77;
            this.richTextBox3.Text = "";
            this.richTextBox3.TextChanged += new System.EventHandler(this.rtbResB_TextChanged);
            this.richTextBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbResB_KeyPress);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 12F);
            this.richTextBox1.Location = new System.Drawing.Point(6, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(514, 70);
            this.richTextBox1.TabIndex = 78;
            this.richTextBox1.Text = "EJEMPLO:\nEscriba los dos números que faltan en esta serie:\n2          4          " +
    "6          8          10          ?          14          ?          18          " +
    "20";
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel8.BackgroundImage")));
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel8.Controls.Add(this.richTextBox2);
            this.panel8.Location = new System.Drawing.Point(165, 121);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(89, 80);
            this.panel8.TabIndex = 78;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.White;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(16, 22);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(60, 39);
            this.richTextBox2.TabIndex = 77;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.rtbResA_TextChanged);
            this.richTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbResA_KeyPress);
            // 
            // btnCancelarAyuda
            // 
            this.btnCancelarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCancelarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarAyuda.Location = new System.Drawing.Point(159, 431);
            this.btnCancelarAyuda.Name = "btnCancelarAyuda";
            this.btnCancelarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnCancelarAyuda.TabIndex = 69;
            this.btnCancelarAyuda.Text = "Cancelar";
            this.btnCancelarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarAyuda.UseVisualStyleBackColor = false;
            this.btnCancelarAyuda.Visible = false;
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(319, 431);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 386);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(427, 18);
            this.label6.TabIndex = 68;
            this.label6.Text = "Como ha visto, es muy sencillo ahora continúe con los siguientes 60.";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Calibri", 11F);
            this.label1.Location = new System.Drawing.Point(23, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "A continuación se presentan cuestiones parecidas a estas.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(21, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(601, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tiempo
            // 
            this.tiempo.Interval = 1000;
            this.tiempo.Tick += new System.EventHandler(this.tiempo_Tick);
            // 
            // lblNumPreg
            // 
            this.lblNumPreg.AutoSize = true;
            this.lblNumPreg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNumPreg.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNumPreg.Location = new System.Drawing.Point(215, 4);
            this.lblNumPreg.Name = "lblNumPreg";
            this.lblNumPreg.Size = new System.Drawing.Size(115, 17);
            this.lblNumPreg.TabIndex = 84;
            this.lblNumPreg.Text = "Pregunta N de M";
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.Color.White;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreguntas.Location = new System.Drawing.Point(25, 11);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.Size = new System.Drawing.Size(493, 69);
            this.txtPreguntas.TabIndex = 0;
            this.txtPreguntas.Text = "";
            // 
            // lblMsj
            // 
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(25, 299);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(493, 74);
            this.lblMsj.TabIndex = 10;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // pnlSeleccion
            // 
            this.pnlSeleccion.Controls.Add(this.rbtOpcionC);
            this.pnlSeleccion.Controls.Add(this.rbtOpcionE);
            this.pnlSeleccion.Controls.Add(this.rbtOpcionD);
            this.pnlSeleccion.Controls.Add(this.rbtOpcionA);
            this.pnlSeleccion.Controls.Add(this.rbtOpcionB);
            this.pnlSeleccion.Location = new System.Drawing.Point(23, 85);
            this.pnlSeleccion.Name = "pnlSeleccion";
            this.pnlSeleccion.Size = new System.Drawing.Size(493, 210);
            this.pnlSeleccion.TabIndex = 73;
            // 
            // rbtOpcionC
            // 
            this.rbtOpcionC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionC.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionC.Location = new System.Drawing.Point(53, 86);
            this.rbtOpcionC.Name = "rbtOpcionC";
            this.rbtOpcionC.Size = new System.Drawing.Size(248, 29);
            this.rbtOpcionC.TabIndex = 71;
            this.rbtOpcionC.Text = "Opcion C";
            this.rbtOpcionC.UseVisualStyleBackColor = false;
            this.rbtOpcionC.CheckedChanged += new System.EventHandler(this.rbtOpcionC_CheckedChanged);
            // 
            // rbtOpcionE
            // 
            this.rbtOpcionE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionE.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionE.Location = new System.Drawing.Point(53, 156);
            this.rbtOpcionE.Name = "rbtOpcionE";
            this.rbtOpcionE.Size = new System.Drawing.Size(248, 29);
            this.rbtOpcionE.TabIndex = 71;
            this.rbtOpcionE.Text = "Opcion E";
            this.rbtOpcionE.UseVisualStyleBackColor = false;
            this.rbtOpcionE.CheckedChanged += new System.EventHandler(this.rbtOpcionE_CheckedChanged);
            // 
            // rbtOpcionD
            // 
            this.rbtOpcionD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionD.Location = new System.Drawing.Point(53, 121);
            this.rbtOpcionD.Name = "rbtOpcionD";
            this.rbtOpcionD.Size = new System.Drawing.Size(248, 29);
            this.rbtOpcionD.TabIndex = 71;
            this.rbtOpcionD.Text = "Opcion D";
            this.rbtOpcionD.UseVisualStyleBackColor = false;
            this.rbtOpcionD.CheckedChanged += new System.EventHandler(this.rbtOpcionD_CheckedChanged);
            // 
            // rbtOpcionA
            // 
            this.rbtOpcionA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionA.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionA.Location = new System.Drawing.Point(53, 15);
            this.rbtOpcionA.Name = "rbtOpcionA";
            this.rbtOpcionA.Size = new System.Drawing.Size(248, 29);
            this.rbtOpcionA.TabIndex = 71;
            this.rbtOpcionA.Text = "Opcion A";
            this.rbtOpcionA.UseVisualStyleBackColor = false;
            this.rbtOpcionA.CheckedChanged += new System.EventHandler(this.rbtOpcionA_CheckedChanged);
            // 
            // rbtOpcionB
            // 
            this.rbtOpcionB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rbtOpcionB.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtOpcionB.Location = new System.Drawing.Point(53, 50);
            this.rbtOpcionB.Name = "rbtOpcionB";
            this.rbtOpcionB.Size = new System.Drawing.Size(248, 29);
            this.rbtOpcionB.TabIndex = 71;
            this.rbtOpcionB.Text = "Opcion B";
            this.rbtOpcionB.UseVisualStyleBackColor = false;
            this.rbtOpcionB.CheckedChanged += new System.EventHandler(this.rbtOpcionB_CheckedChanged);
            // 
            // pnlLogica
            // 
            this.pnlLogica.Controls.Add(this.panel7);
            this.pnlLogica.Controls.Add(this.panel6);
            this.pnlLogica.Location = new System.Drawing.Point(24, 84);
            this.pnlLogica.Name = "pnlLogica";
            this.pnlLogica.Size = new System.Drawing.Size(493, 210);
            this.pnlLogica.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel7.BackgroundImage")));
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel7.Controls.Add(this.rtbResB);
            this.panel7.Location = new System.Drawing.Point(258, 59);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(87, 75);
            this.panel7.TabIndex = 0;
            // 
            // rtbResB
            // 
            this.rtbResB.BackColor = System.Drawing.Color.White;
            this.rtbResB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbResB.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbResB.Location = new System.Drawing.Point(16, 20);
            this.rtbResB.MaxLength = 3;
            this.rtbResB.Name = "rtbResB";
            this.rtbResB.Size = new System.Drawing.Size(62, 39);
            this.rtbResB.TabIndex = 77;
            this.rtbResB.Text = "";
            this.rtbResB.TextChanged += new System.EventHandler(this.rtbResB_TextChanged);
            this.rtbResB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbResB_KeyPress);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel6.BackgroundImage")));
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel6.Controls.Add(this.rtbResA);
            this.panel6.Location = new System.Drawing.Point(142, 59);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(86, 75);
            this.panel6.TabIndex = 0;
            // 
            // rtbResA
            // 
            this.rtbResA.BackColor = System.Drawing.Color.White;
            this.rtbResA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbResA.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbResA.Location = new System.Drawing.Point(17, 20);
            this.rtbResA.MaxLength = 3;
            this.rtbResA.Name = "rtbResA";
            this.rtbResA.Size = new System.Drawing.Size(61, 39);
            this.rtbResA.TabIndex = 0;
            this.rtbResA.Text = "";
            this.rtbResA.TextChanged += new System.EventHandler(this.rtbResA_TextChanged);
            this.rtbResA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbResA_KeyPress);
            // 
            // panel5
            // 
            this.panel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.btnAnterior);
            this.panel5.Controls.Add(this.btnSiguiente);
            this.panel5.Controls.Add(this.btnCancelar);
            this.panel5.Controls.Add(this.btnAyuda);
            this.panel5.Controls.Add(this.gbPreguntas);
            this.panel5.Location = new System.Drawing.Point(33, 85);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(548, 499);
            this.panel5.TabIndex = 88;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.panel9.Controls.Add(this.lblNumPreg);
            this.panel9.Location = new System.Drawing.Point(1, 387);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(547, 25);
            this.panel9.TabIndex = 88;
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.pnlSeleccion);
            this.gbPreguntas.Controls.Add(this.pnlLogica);
            this.gbPreguntas.Controls.Add(this.txtPreguntas);
            this.gbPreguntas.Controls.Add(this.lblMsj);
            this.gbPreguntas.Location = new System.Drawing.Point(3, 4);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(543, 389);
            this.gbPreguntas.TabIndex = 4;
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(164, 436);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 0;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(280, 436);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 5;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(422, 436);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(13, 436);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 1;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCloseWindow.BackgroundImage = global::QuickHumCliente.Properties.Resources.close;
            this.btnCloseWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCloseWindow.FlatAppearance.BorderSize = 0;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCloseWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseWindow.Location = new System.Drawing.Point(580, 1);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(35, 35);
            this.btnCloseWindow.TabIndex = 76;
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = global::QuickHumCliente.Properties.Resources.tiempo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(368, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 37);
            this.panel1.TabIndex = 75;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(47, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 19);
            this.label7.TabIndex = 65;
            this.label7.Text = "Tiempo restante:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lbltime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltime.Location = new System.Drawing.Point(166, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(50, 19);
            this.lbltime.TabIndex = 64;
            this.lbltime.Text = "label9";
            // 
            // frmBarsit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.btnCloseWindow);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel5);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(633, 613);
            this.Name = "frmBarsit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prueba Barsit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBarsit_FormClosing);
            this.Load += new System.EventHandler(this.frmPruebaBarsit_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.pnlSeleccion.ResumeLayout(false);
            this.pnlLogica.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.gbPreguntas.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.Button btnCancelarAyuda;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tiempo;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Label lblNumPreg;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel pnlSeleccion;
        private System.Windows.Forms.RadioButton rbtOpcionA;
        private System.Windows.Forms.RadioButton rbtOpcionB;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Panel pnlLogica;
        private System.Windows.Forms.RadioButton rbtOpcionC;
        private System.Windows.Forms.RadioButton rbtOpcionD;
        private System.Windows.Forms.RadioButton rbtOpcionE;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RichTextBox rtbResB;
        private System.Windows.Forms.RichTextBox rtbResA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}