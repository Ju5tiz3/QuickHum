﻿namespace QuickHumCliente.Vistas.Pruebas.Bfq
{
    partial class frmBFQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBFQ));
            this.label3 = new System.Windows.Forms.Label();
            this.panelAyuda = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnIniciarAyuda = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txtF = new System.Windows.Forms.TextBox();
            this.rtbF = new System.Windows.Forms.RichTextBox();
            this.txtE = new System.Windows.Forms.TextBox();
            this.txtC = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.txtD = new System.Windows.Forms.TextBox();
            this.rtbE = new System.Windows.Forms.RichTextBox();
            this.txtPreguntas = new System.Windows.Forms.RichTextBox();
            this.rtbD = new System.Windows.Forms.RichTextBox();
            this.rtbA = new System.Windows.Forms.RichTextBox();
            this.rtbB = new System.Windows.Forms.RichTextBox();
            this.rtbC = new System.Windows.Forms.RichTextBox();
            this.txtA = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.gbPreguntas = new System.Windows.Forms.Panel();
            this.lblMsj = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelAyuda.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.gbPreguntas.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(662, 36);
            this.label3.TabIndex = 63;
            this.label3.Text = "PRUEBA BFQ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelAyuda
            // 
            this.panelAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.panelAyuda.Controls.Add(this.panel1);
            this.panelAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.panelAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelAyuda.Location = new System.Drawing.Point(0, 36);
            this.panelAyuda.Name = "panelAyuda";
            this.panelAyuda.Size = new System.Drawing.Size(662, 607);
            this.panelAyuda.TabIndex = 64;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.richTextBox2);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.btnIniciarAyuda);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Location = new System.Drawing.Point(31, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(597, 553);
            this.panel1.TabIndex = 72;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(24, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(291, 72);
            this.label4.TabIndex = 62;
            this.label4.Text = "EJEMPLO:\r\n1- Me gusta pasear por el parque de la ciudad.\r\n\r\n2- La familia es el m" +
    "ovil de todos mis actor.\r\n";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.right_corner;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.btnCerrarAyuda);
            this.panel4.Location = new System.Drawing.Point(560, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 36);
            this.panel4.TabIndex = 73;
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.btnCerrarAyuda.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.btnCerrarAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCerrarAyuda.FlatAppearance.BorderSize = 0;
            this.btnCerrarAyuda.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.btnCerrarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarAyuda.Location = new System.Drawing.Point(-3, 0);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(35, 35);
            this.btnCerrarAyuda.TabIndex = 68;
            this.btnCerrarAyuda.UseVisualStyleBackColor = false;
            this.btnCerrarAyuda.Visible = false;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click_1);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.left_corner1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(36, 36);
            this.panel3.TabIndex = 72;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.White;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox2.Font = new System.Drawing.Font("Calibri", 11F);
            this.richTextBox2.Location = new System.Drawing.Point(21, 304);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(561, 175);
            this.richTextBox2.TabIndex = 71;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 11F);
            this.richTextBox1.Location = new System.Drawing.Point(9, 45);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(579, 172);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // btnIniciarAyuda
            // 
            this.btnIniciarAyuda.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnIniciarAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnIniciarAyuda.FlatAppearance.BorderSize = 0;
            this.btnIniciarAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciarAyuda.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnIniciarAyuda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIniciarAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.timePlay;
            this.btnIniciarAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIniciarAyuda.Location = new System.Drawing.Point(240, 507);
            this.btnIniciarAyuda.Name = "btnIniciarAyuda";
            this.btnIniciarAyuda.Size = new System.Drawing.Size(118, 33);
            this.btnIniciarAyuda.TabIndex = 11;
            this.btnIniciarAyuda.Text = "Iniciar";
            this.btnIniciarAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIniciarAyuda.UseVisualStyleBackColor = false;
            this.btnIniciarAyuda.Click += new System.EventHandler(this.btnIniciarAyuda_Click);
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(371, 258);
            this.textBox3.MaxLength = 1;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(48, 31);
            this.textBox3.TabIndex = 64;
            this.textBox3.Text = "5";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(71)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(545, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "INDICACIONES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(371, 223);
            this.textBox4.MaxLength = 1;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(48, 31);
            this.textBox4.TabIndex = 65;
            this.textBox4.Text = "2";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtF
            // 
            this.txtF.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtF.Location = new System.Drawing.Point(454, 326);
            this.txtF.MaxLength = 1;
            this.txtF.Name = "txtF";
            this.txtF.Size = new System.Drawing.Size(48, 32);
            this.txtF.TabIndex = 74;
            this.txtF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF.TextChanged += new System.EventHandler(this.txtF_TextChanged);
            this.txtF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtF_KeyPress);
            this.txtF.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtF_KeyUp);
            // 
            // rtbF
            // 
            this.rtbF.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbF.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbF.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbF.Location = new System.Drawing.Point(13, 317);
            this.rtbF.Name = "rtbF";
            this.rtbF.ReadOnly = true;
            this.rtbF.Size = new System.Drawing.Size(421, 41);
            this.rtbF.TabIndex = 73;
            this.rtbF.Text = "";
            // 
            // txtE
            // 
            this.txtE.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtE.Location = new System.Drawing.Point(454, 273);
            this.txtE.MaxLength = 1;
            this.txtE.Name = "txtE";
            this.txtE.Size = new System.Drawing.Size(48, 32);
            this.txtE.TabIndex = 72;
            this.txtE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtE.TextChanged += new System.EventHandler(this.txtE_TextChanged);
            this.txtE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtE_KeyPress);
            this.txtE.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtE_KeyUp);
            // 
            // txtC
            // 
            this.txtC.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtC.Location = new System.Drawing.Point(454, 175);
            this.txtC.MaxLength = 1;
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(48, 32);
            this.txtC.TabIndex = 71;
            this.txtC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtC.TextChanged += new System.EventHandler(this.txtC_TextChanged);
            this.txtC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtC_KeyPress);
            this.txtC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtC_KeyUp);
            // 
            // txtB
            // 
            this.txtB.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtB.Location = new System.Drawing.Point(454, 126);
            this.txtB.MaxLength = 1;
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(48, 32);
            this.txtB.TabIndex = 70;
            this.txtB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtB.TextChanged += new System.EventHandler(this.txtB_TextChanged);
            this.txtB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_KeyPress);
            this.txtB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtB_KeyUp);
            // 
            // txtD
            // 
            this.txtD.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtD.Location = new System.Drawing.Point(454, 224);
            this.txtD.MaxLength = 1;
            this.txtD.Name = "txtD";
            this.txtD.Size = new System.Drawing.Size(48, 32);
            this.txtD.TabIndex = 69;
            this.txtD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtD.TextChanged += new System.EventHandler(this.txtD_TextChanged);
            this.txtD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtD_KeyPress);
            this.txtD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtD_KeyUp);
            // 
            // rtbE
            // 
            this.rtbE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbE.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbE.Location = new System.Drawing.Point(13, 268);
            this.rtbE.Name = "rtbE";
            this.rtbE.ReadOnly = true;
            this.rtbE.Size = new System.Drawing.Size(421, 41);
            this.rtbE.TabIndex = 68;
            this.rtbE.Text = "";
            // 
            // txtPreguntas
            // 
            this.txtPreguntas.BackColor = System.Drawing.Color.White;
            this.txtPreguntas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreguntas.Font = new System.Drawing.Font("Calibri", 11F);
            this.txtPreguntas.Location = new System.Drawing.Point(3, 3);
            this.txtPreguntas.Name = "txtPreguntas";
            this.txtPreguntas.ReadOnly = true;
            this.txtPreguntas.Size = new System.Drawing.Size(569, 63);
            this.txtPreguntas.TabIndex = 12;
            this.txtPreguntas.Text = "";
            // 
            // rtbD
            // 
            this.rtbD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbD.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbD.Location = new System.Drawing.Point(13, 219);
            this.rtbD.Name = "rtbD";
            this.rtbD.ReadOnly = true;
            this.rtbD.Size = new System.Drawing.Size(421, 41);
            this.rtbD.TabIndex = 16;
            this.rtbD.Text = "";
            // 
            // rtbA
            // 
            this.rtbA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbA.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbA.Location = new System.Drawing.Point(13, 72);
            this.rtbA.Name = "rtbA";
            this.rtbA.ReadOnly = true;
            this.rtbA.Size = new System.Drawing.Size(421, 41);
            this.rtbA.TabIndex = 15;
            this.rtbA.Text = "";
            // 
            // rtbB
            // 
            this.rtbB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbB.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbB.Location = new System.Drawing.Point(13, 121);
            this.rtbB.Name = "rtbB";
            this.rtbB.ReadOnly = true;
            this.rtbB.Size = new System.Drawing.Size(421, 41);
            this.rtbB.TabIndex = 14;
            this.rtbB.Text = "";
            // 
            // rtbC
            // 
            this.rtbC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtbC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbC.Font = new System.Drawing.Font("Calibri", 11F);
            this.rtbC.Location = new System.Drawing.Point(13, 170);
            this.rtbC.Name = "rtbC";
            this.rtbC.ReadOnly = true;
            this.rtbC.Size = new System.Drawing.Size(421, 41);
            this.rtbC.TabIndex = 13;
            this.rtbC.Text = "";
            // 
            // txtA
            // 
            this.txtA.Font = new System.Drawing.Font("Calibri", 15F);
            this.txtA.Location = new System.Drawing.Point(454, 77);
            this.txtA.MaxLength = 1;
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(48, 32);
            this.txtA.TabIndex = 2;
            this.txtA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtA.TextChanged += new System.EventHandler(this.txtA_TextChanged);
            this.txtA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_KeyPress);
            this.txtA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtA_KeyUp);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.gbPreguntas);
            this.panel2.Location = new System.Drawing.Point(47, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 469);
            this.panel2.TabIndex = 72;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnAyuda);
            this.panel5.Controls.Add(this.btnSiguiente);
            this.panel5.Controls.Add(this.btnCancelar);
            this.panel5.Controls.Add(this.btnAnterior);
            this.panel5.Location = new System.Drawing.Point(3, 406);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(575, 60);
            this.panel5.TabIndex = 73;
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(173)))), ((int)(((byte)(176)))));
            this.btnAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAyuda.FlatAppearance.BorderSize = 0;
            this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyuda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAyuda.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAyuda.Image = global::QuickHumCliente.Properties.RecursosPruebas.helpLight;
            this.btnAyuda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyuda.Location = new System.Drawing.Point(13, 13);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(110, 35);
            this.btnAyuda.TabIndex = 70;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAyuda.UseVisualStyleBackColor = false;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiguiente.Image = global::QuickHumCliente.Properties.RecursosPruebas.nextBtn;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(292, 13);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(110, 35);
            this.btnSiguiente.TabIndex = 68;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelar.Image = global::QuickHumCliente.Properties.RecursosPruebas.cancelButton;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(454, 13);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 35);
            this.btnCancelar.TabIndex = 69;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(183)))), ((int)(((byte)(212)))));
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.Enabled = false;
            this.btnAnterior.FlatAppearance.BorderSize = 0;
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.btnAnterior.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAnterior.Image = global::QuickHumCliente.Properties.RecursosPruebas.previousBtn;
            this.btnAnterior.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnterior.Location = new System.Drawing.Point(173, 13);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(110, 35);
            this.btnAnterior.TabIndex = 71;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // gbPreguntas
            // 
            this.gbPreguntas.Controls.Add(this.lblMsj);
            this.gbPreguntas.Controls.Add(this.txtF);
            this.gbPreguntas.Controls.Add(this.rtbA);
            this.gbPreguntas.Controls.Add(this.rtbF);
            this.gbPreguntas.Controls.Add(this.txtA);
            this.gbPreguntas.Controls.Add(this.txtE);
            this.gbPreguntas.Controls.Add(this.rtbC);
            this.gbPreguntas.Controls.Add(this.txtC);
            this.gbPreguntas.Controls.Add(this.rtbB);
            this.gbPreguntas.Controls.Add(this.txtB);
            this.gbPreguntas.Controls.Add(this.rtbD);
            this.gbPreguntas.Controls.Add(this.txtD);
            this.gbPreguntas.Controls.Add(this.txtPreguntas);
            this.gbPreguntas.Controls.Add(this.rtbE);
            this.gbPreguntas.Location = new System.Drawing.Point(3, 3);
            this.gbPreguntas.Name = "gbPreguntas";
            this.gbPreguntas.Size = new System.Drawing.Size(575, 397);
            this.gbPreguntas.TabIndex = 0;
            // 
            // lblMsj
            // 
            this.lblMsj.AutoSize = true;
            this.lblMsj.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblMsj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(68)))), ((int)(((byte)(58)))));
            this.lblMsj.Location = new System.Drawing.Point(10, 361);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(135, 18);
            this.lblMsj.TabIndex = 74;
            this.lblMsj.Text = "Mostrar Msj de Error";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.button1.BackgroundImage = global::QuickHumCliente.Properties.RecursosPruebas.close1;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(96)))), ((int)(((byte)(109)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(624, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 35);
            this.button1.TabIndex = 73;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmBFQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(662, 643);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelAyuda);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.MinimumSize = new System.Drawing.Size(639, 659);
            this.Name = "frmBFQ";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRUEBA BFQ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBFQ_FormClosing);
            this.Load += new System.EventHandler(this.frmBFQ_Load);
            this.panelAyuda.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.gbPreguntas.ResumeLayout(false);
            this.gbPreguntas.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelAyuda;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnIniciarAyuda;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtbE;
        private System.Windows.Forms.RichTextBox rtbD;
        private System.Windows.Forms.RichTextBox rtbA;
        private System.Windows.Forms.RichTextBox rtbB;
        private System.Windows.Forms.RichTextBox rtbC;
        private System.Windows.Forms.RichTextBox txtPreguntas;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.TextBox txtF;
        private System.Windows.Forms.RichTextBox rtbF;
        private System.Windows.Forms.TextBox txtE;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtD;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel gbPreguntas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblMsj;
    }
}