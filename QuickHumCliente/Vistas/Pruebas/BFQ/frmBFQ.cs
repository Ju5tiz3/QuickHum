﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Bfq
{
    public partial class frmBFQ : Form
    {
        private QuickHumClient cliente = Globales.cliente;
        private Expediente _expediente = null;
        private EscolaridadPrueba _escolaridad = null;
        private bool prueba_iniciada = false;

        dto_bfq_respuestas_candidato respcandidato = new dto_bfq_respuestas_candidato();
        int ida = 0, idb = 0, idc = 0, idd = 0, ide = 0, idf = 0;
        DataTable dtpreguntas = new DataTable();
        DataTable dtrespuestas = new DataTable();
        DataTable dtrespcandidato = new DataTable();
        int recorrido = 0;
        DataRow[] row, row1, row2, row3, row4, row6;
        int x = 0;
        int NumeroAsignacion;

        public frmBFQ(Expediente expediente, EscolaridadPrueba escolaridad)
        {
            InitializeComponent();
            if (expediente == null || escolaridad == null)
            {
                XtraMessageBox.Show("Los argumentos pasados a la prueba son nulos");
                this.Close();
                return;
            }
            this._expediente = expediente;
            this._escolaridad = escolaridad;
        }

        private void frmBFQ_Load(object sender, EventArgs e)
        {
            dtrespcandidato.Columns.Add("idrespuestacndidato");
            dtrespcandidato.Columns.Add("idrespuesta");
            dtrespcandidato.Columns.Add("numeroprueba");
            dtrespcandidato.Columns.Add("puntaje");

            this.Size = new Size(679, 545);
            this.panelAyuda.Location = new Point(1, 1);

            listardatos();
            lblMsj.Text = "";

        }

        private void listardatos()
        {
            dto_bfq_preguntas[] Listarpreguntas = cliente.ListadoBFQPreguntas();

            dtpreguntas.Columns.Add("idpregunta");
            dtpreguntas.Columns.Add("pregunta");
            dtpreguntas.Columns.Add("idfactor");

            dtrespuestas.Columns.Add("idrespuesta");
            dtrespuestas.Columns.Add("respuesta");
            dtrespuestas.Columns.Add("id_pregunta");

            for (int i = 0; i < Listarpreguntas.ToList().Count; i++)
            {
                DataRow fila = dtpreguntas.NewRow();

                fila["idpregunta"] = Convert.ToInt32(Listarpreguntas[i].Idpregunta);
                fila["pregunta"] = Convert.ToString(Listarpreguntas[i].Pregunta);
                fila["idfactor"] = Convert.ToInt32(Listarpreguntas[i].Idpregunta);

                for (int j = 0; j < 5; j++)
                {
                    DataRow row = dtrespuestas.NewRow();

                    row["idrespuesta"] = Convert.ToInt32(Listarpreguntas[i].Respuesta[j].Idrespuesta);
                    row["respuesta"] = Convert.ToString(Listarpreguntas[i].Respuesta[j].Respuesta);
                    row["id_pregunta"] = Convert.ToInt32(Listarpreguntas[i].Respuesta[j].Idpregunta);
                    dtrespuestas.Rows.Add(row);
                }
                dtpreguntas.Rows.Add(fila);
            }
            txtPreguntas.Text = "Pregunta de la " + 1 + " a la " + 6;

            //lblIdPregunta.Text = dtpreguntas.Rows[0]["idpregunta"].ToString();

            rtbA.Text = "- " + dtpreguntas.Rows[0]["pregunta"].ToString();
            rtbB.Text = "- " + dtpreguntas.Rows[1]["pregunta"].ToString();
            rtbC.Text = "- " + dtpreguntas.Rows[2]["pregunta"].ToString();
            rtbD.Text = "- " + dtpreguntas.Rows[3]["pregunta"].ToString();
            rtbE.Text = "- " + dtpreguntas.Rows[4]["pregunta"].ToString();
            rtbF.Text = "- " + dtpreguntas.Rows[5]["pregunta"].ToString();

            txtA.Name = dtrespuestas.Rows[0]["idrespuesta"].ToString();
            txtB.Name = dtrespuestas.Rows[1]["idrespuesta"].ToString();
            txtC.Name = dtrespuestas.Rows[2]["idrespuesta"].ToString();
            txtD.Name = dtrespuestas.Rows[3]["idrespuesta"].ToString();
            txtE.Name = dtrespuestas.Rows[4]["idrespuesta"].ToString();
            txtF.Name = dtrespuestas.Rows[5]["idrespuesta"].ToString();
        }

        private void Evaluar()
        {
            if (Convert.ToInt32(txtA.Text) == 5)
            {
                ida = Convert.ToInt32(dtrespuestas.Rows[(x * 30) - 30]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtA.Text) == 4)
            {
                ida = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 1]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtA.Text) == 3)
            {
                ida = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 2]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtA.Text) == 2)
            {
                ida = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 3]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtA.Text) == 1)
            {
                ida = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 4]["idrespuesta"].ToString());
            }

            if (Convert.ToInt32(txtB.Text) == 5)
            {
                idb = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 5]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtB.Text) == 4)
            {
                idb = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 6]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtB.Text) == 3)
            {
                idb = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 7]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtB.Text) == 2)
            {
                idb = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 8]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtB.Text) == 1)
            {
                idb = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 9]["idrespuesta"].ToString());
            }
            if (Convert.ToInt32(txtC.Text) == 5)
            {
                idc = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 10]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtC.Text) == 4)
            {
                idc = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 11]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtC.Text) == 3)
            {
                idc = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 12]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtC.Text) == 2)
            {
                idc = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 13]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtC.Text) == 1)
            {
                idc = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 14]["idrespuesta"].ToString());
            }
            if (Convert.ToInt32(txtD.Text) == 5)
            {
                idd = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 15]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtD.Text) == 4)
            {
                idd = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 16]["idrespuesta"].ToString());

            }
            else if (Convert.ToInt32(txtD.Text) == 3)
            {
                idd = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 17]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtD.Text) == 2)
            {
                idd = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 18]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtD.Text) == 1)
            {
                idd = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 19]["idrespuesta"].ToString());
            }
            if (Convert.ToInt32(txtE.Text) == 5)
            {
                ide = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 20]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtE.Text) == 4)
            {
                ide = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 21]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtE.Text) == 3)
            {
                ide = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 22]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtE.Text) == 2)
            {
                ide = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 23]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtE.Text) == 1)
            {
                ide = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 24]["idrespuesta"].ToString());
            }
            if (Convert.ToInt32(txtF.Text) == 5)
            {
                idf = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 25]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtF.Text) == 4)
            {
                idf = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 26]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtF.Text) == 3)
            {
                idf = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 27]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtF.Text) == 2)
            {
                idf = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 28]["idrespuesta"].ToString());
            }
            else if (Convert.ToInt32(txtF.Text) == 1)
            {
                idf = Convert.ToInt32(dtrespuestas.Rows[((x * 30) - 30) + 29]["idrespuesta"].ToString());
            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (btnSiguiente.Text == "Siguiente")
            {
                if (this.txtA.Text.Trim() == "" || this.txtB.Text.Trim() == "" || this.txtC.Text.Trim() == "" || this.txtD.Text.Trim() == "" || this.txtE.Text.Trim() == "" || this.txtF.Text.Trim() == "")
                {
                    this.lblMsj.Text = "Es necesario que todos los campos estén llenos.";
                    return;
                }
                else
                {
                    x++;
                    lblMsj.Text = "";
                    GuardarRespuestas();
                }
                MostrarPregunta(x);
                limpiarCampos();
            }

            else if (btnSiguiente.Text == "Siguiente.")
            {
                if (this.txtA.Text.Trim() == "" || this.txtB.Text.Trim() == "" || this.txtC.Text.Trim() == "" || this.txtD.Text.Trim() == "" || this.txtE.Text.Trim() == "" || this.txtF.Text.Trim() == "")
                {
                    this.lblMsj.Text = "Es necesario que todos los campos estén llenos.";

                    return;
                }
                else
                {
                    x++;
                    lblMsj.Text = "";
                    ActualizarRespuestas();
                }

                if (recorrido > (x * 6))
                {
                    llenartextbox();
                }

                if (recorrido - (5 * x) <= x)
                {
                    btnSiguiente.Enabled = false; ;
                    btnSiguiente.Text = "Siguiente";
                    limpiarCampos();
                }

                MostrarPregunta(x);

            }
            else if (btnSiguiente.Text == "Finalizar")
            {
                DialogResult dr = MessageBox.Show("¿Esta seguro que quiere terminar la prueba? ", "Prueba BFQ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                {
                    if (this.txtA.Text.Trim() == "" || this.txtB.Text.Trim() == "" || this.txtC.Text.Trim() == "" || this.txtD.Text.Trim() == "" || this.txtE.Text.Trim() == "" || this.txtF.Text.Trim() == "")
                    {
                        this.lblMsj.Text = "Es necesario que todos los campos estén llenos.";

                        return;
                    }
                    else
                    {
                        x++;
                        GuardarRespuestas();
                        respcandidato.numeroprueba = NumeroAsignacion;
                        cliente.FinalizaBFQPrueba(respcandidato);

                    }
                    this.Dispose();
                }
            }
            if (x == Convert.ToInt32(dtpreguntas.Rows.Count - 110) - 1)
            {
                btnSiguiente.Text = "Finalizar";
            }

            btnAnterior.Enabled = true;
        }

        private void GuardarRespuestas()
        {
            Evaluar();
            respcandidato.Idrespuesta = Convert.ToInt32(ida);
            respcandidato.Puntaje = Convert.ToInt32(txtA.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);


            respcandidato.Idrespuesta = Convert.ToInt32(idb);
            respcandidato.Puntaje = Convert.ToInt32(txtB.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);


            respcandidato.Idrespuesta = Convert.ToInt32(idc);
            respcandidato.Puntaje = Convert.ToInt32(txtC.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);



            respcandidato.Idrespuesta = Convert.ToInt32(idd);
            respcandidato.Puntaje = Convert.ToInt32(txtD.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);


            respcandidato.Idrespuesta = Convert.ToInt32(ide);
            respcandidato.Puntaje = Convert.ToInt32(txtE.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);


            respcandidato.Idrespuesta = Convert.ToInt32(idf);
            respcandidato.Puntaje = Convert.ToInt32(txtF.Text);
            respcandidato.numeroprueba = Convert.ToInt32(NumeroAsignacion);
            cliente.InsertaBFQRespuesta(respcandidato);

        }

        private void ActualizarRespuestas()
        {
            Evaluar();
            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 6]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = ida;
            respcandidato.Puntaje = Convert.ToInt32(txtA.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 5]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = idb;
            respcandidato.Puntaje = Convert.ToInt32(txtB.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 4]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = idc;
            respcandidato.Puntaje = Convert.ToInt32(txtC.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 3]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = idd;
            respcandidato.Puntaje = Convert.ToInt32(txtD.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 2]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = ide;
            respcandidato.Puntaje = Convert.ToInt32(txtE.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);

            respcandidato.Idrespuestacandidato = Convert.ToInt32(dtrespcandidato.Rows[(x * 6) - 1]["idrespuestacndidato"].ToString());
            respcandidato.Idrespuesta = idf;
            respcandidato.Puntaje = Convert.ToInt32(txtF.Text);
            cliente.ActualizaBFQRespuesta(respcandidato);
        }

        private void MostrarPregunta(int i)
        {

            txtPreguntas.Text = "Pregunta de la " + (((i + 1) * 6) - 5) + " a la " + ((i + 1) * 6);
            row = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i)][0].ToString() + " ' ");
            row1 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i) + 1][0].ToString() + " ' ");
            row2 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i) + 2][0].ToString() + " ' ");
            row3 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i) + 3][0].ToString() + " ' ");
            row4 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i) + 4][0].ToString() + " ' ");
            row6 = dtrespuestas.Select("id_pregunta = '" + dtpreguntas.Rows[(6 * i) + 5][0].ToString() + " ' ");

            if (row.Length > 0)
            {
                rtbA.Text = "- " + dtpreguntas.Rows[6 * x]["pregunta"].ToString();
                rtbB.Text = "- " + dtpreguntas.Rows[(6 * x) + 1]["pregunta"].ToString();
                rtbC.Text = "- " + dtpreguntas.Rows[(6 * x) + 2]["pregunta"].ToString();
                rtbD.Text = "- " + dtpreguntas.Rows[(6 * x) + 3]["pregunta"].ToString();
                rtbE.Text = "- " + dtpreguntas.Rows[(6 * x) + 4]["pregunta"].ToString();
                rtbF.Text = "- " + dtpreguntas.Rows[(6 * x) + 5]["pregunta"].ToString();

            }
        }

        private void llenartextbox()
        {
            txtA.Text = dtrespcandidato.Rows[6 * x]["puntaje"].ToString();
            txtB.Text = dtrespcandidato.Rows[(6 * x) + 1]["puntaje"].ToString();
            txtC.Text = dtrespcandidato.Rows[(6 * x) + 2]["puntaje"].ToString();
            txtD.Text = dtrespcandidato.Rows[(6 * x) + 3]["puntaje"].ToString();
            txtE.Text = dtrespcandidato.Rows[(6 * x) + 4]["puntaje"].ToString();
            txtF.Text = dtrespcandidato.Rows[(6 * x) + 5]["puntaje"].ToString();
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            dto_bfq_respuestas_candidato[] lista = cliente.ListaBFQRespuestasCandidatos(NumeroAsignacion);
            dtrespcandidato.Clear();
            for (int w = 0; w < lista.ToList().Count; w++)
            {
                DataRow fila = dtrespcandidato.NewRow();
                fila["idrespuestacndidato"] = Convert.ToInt32(lista[w].Idrespuestacandidato);
                fila["idrespuesta"] = Convert.ToInt32(lista[w].Idrespuesta);
                fila["numeroprueba"] = Convert.ToInt32(lista[w].numeroprueba);
                fila["puntaje"] = Convert.ToInt32(lista[w].Puntaje);
                dtrespcandidato.Rows.Add(fila);

            }
            recorrido = dtrespcandidato.Rows.Count;
            btnSiguiente.Text = "Siguiente.";
            btnSiguiente.Enabled = true;

            txtPreguntas.Text = "Pregunta de la " + (((x + 1) * 6) - 5) + " a la " + ((x + 1) * 6);
            x--;
            MostrarPregunta(x);

            if (recorrido > (x * 6))
            {
                llenartextbox();
            }
            if (x == 0)
            {
                btnAnterior.Enabled = false;
            }
            if (btnSiguiente.Text == "Finalizar")
            {
                btnSiguiente.Text = "Siguiente";
            }
        }

        private void limpiarCampos()
        {
            foreach (Control text in this.gbPreguntas.Controls)
            {
                if (text is TextBox)
                {
                    text.Text = "";
                }
            }
            txtA.Focus();
        }

        private void ValidarCamposLlenos()
        {
            if (txtA.Text.Length == 0 || txtB.Text.Length == 0 || txtC.Text.Length == 0 || txtD.Text.Length == 0 || txtE.Text.Length == 0 || txtF.Text.Length == 0)
            {
                btnSiguiente.Enabled = false;
                return;
            }
            else
            {
                btnSiguiente.Enabled = true;
                btnSiguiente.Focus();
            }
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            gbPreguntas.Enabled = true;
            panelAyuda.Show();
            btnCerrarAyuda.Show();
            btnIniciarAyuda.Hide();
        }

        private void btnIniciarAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                BFQ modelo_BFQ = new BFQ();
                modelo_BFQ = cliente.NuevoBFQ(this._expediente.Numero, this._escolaridad);
                this.prueba_iniciada = true;
                NumeroAsignacion = modelo_BFQ.IdPrueba;
                panelAyuda.Hide();
                btnIniciarAyuda.Hide();
                btnAyuda.Show();
                txtA.Focus();
            }
            catch
            {
                MessageBox.Show("Ya existe una prueba para este expediente");
            }
        }

        public void numeros(object sender, KeyPressEventArgs ex)
        {
            if (char.IsDigit(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else if (char.IsControl(ex.KeyChar))
            {
                ex.Handled = false;
            }
            else
            {
                ex.Handled = true;
            }
            // bloquear las teclas 0, y del 5 al 9
            if ((ex.KeyChar == (char)Keys.D0) || (ex.KeyChar == (char)Keys.D6) || (ex.KeyChar == (char)Keys.D7) || (ex.KeyChar == (char)Keys.D8) || (ex.KeyChar == (char)Keys.D9))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ex.Handled = true;
                return;
            }
        }

        #region "Eventos de los Textbox"
        private void txtA_TextChanged(object sender, EventArgs e)
        {
            txtB.Focus();
        }

        private void txtB_TextChanged(object sender, EventArgs e)
        {
            txtC.Focus();
        }

        private void txtC_TextChanged(object sender, EventArgs e)
        {
            txtD.Focus();
        }

        private void txtD_TextChanged(object sender, EventArgs e)
        {
            txtE.Focus();
        }

        private void txtE_TextChanged(object sender, EventArgs e)
        {
            txtF.Focus();
        }

        private void txtF_TextChanged(object sender, EventArgs e)
        {
            btnSiguiente.Focus();
        }

        private void txtA_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtB_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtC_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtD_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtE_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtF_KeyUp(object sender, KeyEventArgs e)
        {
            ValidarCamposLlenos();
        }

        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtD_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtE_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }

        private void txtF_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(sender, e);
        }
        #endregion

        private void btnCerrarAyuda_Click_1(object sender, EventArgs e)
        {
            this.panelAyuda.Hide();
            this.gbPreguntas.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarPrueba();
        }

        private void CancelarPrueba()
        {
            if (XtraMessageBox.Show("¿Esta seguro que desea cancelar la prueba?", "Prueba BFQ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (prueba_iniciada)
                cliente.CambiarEstadoPrueba(Test.BFQ, NumeroAsignacion, PruebaEstado.CANCELADA);
            this.Dispose();
        }

        /// <summary>
        /// cancelar ALT + F4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBFQ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4 && e.Alt)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void frmBFQ_FormClosing(object sender, FormClosingEventArgs e)
        {
  
        }
    }
}