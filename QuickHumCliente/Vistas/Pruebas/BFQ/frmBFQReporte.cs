﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas.Pruebas.Bfq
{
    public partial class frmBFQReporte : Form
    {
        QuickHumClient cliente = Globales.cliente;
        DataTable dt = new DataTable();
        DataTable dtCandidato = new DataTable();
        DataTable dtgrafica = new DataTable();
        DataTable dtrealista = new DataTable();
        DataTable dtinvestigador = new DataTable();
        DataTable dtcreativo = new DataTable();
        DataTable dtsocial = new DataTable();
        DataTable dtdirigente = new DataTable();
        DataTable dtinter = new DataTable();
        DataTable dtconvencional = new DataTable();
        string intere, interdi, interdo, intera, intercp, interco, intert, interes, interpe, interee, interce, interci, interam, interac, interae, interd;
        string re, rdi, rdo, ra, rcp, rco, rt, res, rpe, ree, rce, rci, ram, rac, rae, rd, expresion;
        string subfactor, factores;
        int[] suma = new int[11];
        int[] factor = new int[6];
        int totalfactor = 0, totalsubfactor = 0, orden = 0, percentil = 0, contgrafica = 0, percentilD = 0, count = 0, interpretacion = 0;
        DataRow[] filtro;
        private Expediente _expediente = null;


        public frmBFQReporte(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void frmBFQReporte_Load(object sender, EventArgs e)
        {
            dto_bfq_grafica[] grafica = cliente.ListarGraficaBFQ();
            dto_bfq_interpretacion[] inter = cliente.ListaBFQinterpretacion();

            dtinter.Columns.Add("idinterpretacion");
            dtinter.Columns.Add("interpretacion");
            dtinter.Columns.Add("rango");
            dtinter.Columns.Add("factor");

            dtgrafica.Columns.Add("id_grafica");
            dtgrafica.Columns.Add("idfactor");
            dtgrafica.Columns.Add("punto");
            dtgrafica.Columns.Add("grafico");
            dtgrafica.Columns.Add("iddescripcion");
            dtgrafica.Columns.Add("factores");
            dtgrafica.Columns.Add("puntofactor");
            dtgrafica.Columns.Add("graficofactor");
            dtgrafica.Columns.Add("iddesdimension");

            dtCandidato.Columns.Add("Dui");
            dtCandidato.Columns.Add("Candidato");
            dtCandidato.Columns.Add("Genero");
            dtCandidato.Columns.Add("Edad");
            dtCandidato.Columns.Add("Evaluador");
            dtCandidato.Columns.Add("Fecha");

            for (int g = 0; g < inter.ToList().Count; g++)
            {
                DataRow row = dtinter.NewRow();
                row["idinterpretacion"] = Convert.ToInt32(inter[g].Iddescripcion);
                row["interpretacion"] = Convert.ToString(inter[g].Interpretacion);
                row["rango"] = Convert.ToString(inter[g].Rango);
                row["factor"] = Convert.ToString(inter[g].Factor);
                dtinter.Rows.Add(row);
            }
            count = dtinter.Rows.Count;
            for (int j = 0; j < grafica.ToList().Count; j++)
            {
                DataRow rows = dtgrafica.NewRow();
                rows["id_grafica"] = Convert.ToInt32(grafica[j].IdGrafica);
                rows["idfactor"] = Convert.ToInt32(grafica[j].IdFactor);
                rows["punto"] = Convert.ToInt32(grafica[j].Punto);
                rows["grafico"] = Convert.ToInt32(grafica[j].Grafico);
                rows["iddescripcion"] = Convert.ToInt32(grafica[j].Iddescripcion);
                rows["factores"] = Convert.ToString(grafica[j].factores);
                rows["puntofactor"] = Convert.ToInt32(grafica[j].Puntofactor);
                rows["graficofactor"] = Convert.ToInt32(grafica[j].Graficofactor);
                rows["iddesdimension"] = Convert.ToInt32(grafica[j].Iddescdimension);
                dtgrafica.Rows.Add(rows);

            }
            contgrafica = dtgrafica.Rows.Count;

            dt.Columns.Add("Idfactor");
            dt.Columns.Add("Subfactor");
            dt.Columns.Add("Factor");
            dt.Columns.Add("Suma");
            dt.Columns.Add("Total");
            dt.Columns.Add("Orden");
            dt.Columns.Add("Percentil");
            dt.Columns.Add("PercentilD");
            dt.Columns.Add("Serie");

            // COnstruimos el del candidato
            DataRow row_candidato = dtCandidato.NewRow();
            row_candidato["Dui"] = this._expediente.Candidato.Dui;
            row_candidato["Candidato"] = this._expediente.Candidato.Nombre + " " + this._expediente.Candidato.Apellido;
            row_candidato["Genero"] = this._expediente.Candidato.Genero == CandidatoGenero.MASCULINO ? "MASCULINO" : "FEMENINO";
            row_candidato["Edad"] = this._expediente.Candidato.Edad;
            row_candidato["Evaluador"] = Globales.InformacionEvaluador.Nombre + " " + Globales.InformacionEvaluador.Apellido;
            row_candidato["Fecha"] = this._expediente.Fecha;
            dtCandidato.Rows.Add(row_candidato);


            LlenarTablas();
        }
        private void LlenarTablas()
        {
            dtrealista.Columns.Add("Idfactor");
            dtrealista.Columns.Add("Subfactor");
            dtrealista.Columns.Add("Factor");
            dtrealista.Columns.Add("Percentil");
            dtrealista.Columns.Add("PercentilD");

            dtinvestigador.Columns.Add("Idfactor");
            dtinvestigador.Columns.Add("Subfactor");
            dtinvestigador.Columns.Add("Factor");
            dtinvestigador.Columns.Add("Percentil");
            dtinvestigador.Columns.Add("PercentilD");

            dtcreativo.Columns.Add("Idfactor");
            dtcreativo.Columns.Add("Subfactor");
            dtcreativo.Columns.Add("Factor");
            dtcreativo.Columns.Add("Percentil");
            dtcreativo.Columns.Add("PercentilD");

            dtsocial.Columns.Add("Idfactor");
            dtsocial.Columns.Add("Subfactor");
            dtsocial.Columns.Add("Factor");
            dtsocial.Columns.Add("Percentil");
            dtsocial.Columns.Add("PercentilD");

            dtdirigente.Columns.Add("Idfactor");
            dtdirigente.Columns.Add("Subfactor");
            dtdirigente.Columns.Add("Factor");
            dtdirigente.Columns.Add("Percentil");
            dtdirigente.Columns.Add("PercentilD");

            dtconvencional.Columns.Add("Idfactor");
            dtconvencional.Columns.Add("Subfactor");
            dtconvencional.Columns.Add("Factor");
            dtconvencional.Columns.Add("Percentil");
            dtconvencional.Columns.Add("PercentilD");

            DataRow FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "A"; FILAS["Subfactor"] = "Di"; FILAS["Factor"] = "E";
            FILAS["Percentil"] = 50; FILAS["PercentilD"] = 45; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "B"; FILAS["Subfactor"] = "Do"; FILAS["Factor"] = "i";
            FILAS["Percentil"] = 40; FILAS["PercentilD"] = 45; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "C"; FILAS["Subfactor"] = "Cp"; FILAS["Factor"] = "A";
            FILAS["Percentil"] = 50; FILAS["PercentilD"] = 46; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "D"; FILAS["Subfactor"] = "Co"; FILAS["Factor"] = "i";
            FILAS["Percentil"] = 45; FILAS["PercentilD"] = 46; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "E"; FILAS["Subfactor"] = "Es"; FILAS["Factor"] = "T";
            FILAS["Percentil"] = 62; FILAS["PercentilD"] = 63; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "F"; FILAS["Subfactor"] = "Pe"; FILAS["Factor"] = "i";
            FILAS["Percentil"] = 59; FILAS["PercentilD"] = 63; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "G"; FILAS["Subfactor"] = "Ce"; FILAS["Factor"] = "EE";
            FILAS["Percentil"] = 54; FILAS["PercentilD"] = 46; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "H"; FILAS["Subfactor"] = "Ci"; FILAS["Factor"] = "i";
            FILAS["Percentil"] = 40; FILAS["PercentilD"] = 46; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "I"; FILAS["Subfactor"] = "Ac"; FILAS["Factor"] = "AM";
            FILAS["Percentil"] = 44; FILAS["PercentilD"] = 36; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "J"; FILAS["Subfactor"] = "Ae"; FILAS["Factor"] = "i";
            FILAS["Percentil"] = 32; FILAS["PercentilD"] = 36; dtrealista.Rows.Add(FILAS);
            FILAS = dtrealista.NewRow();
            FILAS["Idfactor"] = "K"; FILAS["Subfactor"] = "D"; FILAS["Factor"] = "D";
            FILAS["Percentil"] = 48; FILAS["PercentilD"] = 48; dtrealista.Rows.Add(FILAS);
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            DataRow row = dtinvestigador.NewRow();
            row["Idfactor"] = "A"; row["Subfactor"] = "Di"; row["Factor"] = "E";
            row["Percentil"] = 50; row["PercentilD"] = 54; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "B"; row["Subfactor"] = "Do"; row["Factor"] = "i";
            row["Percentil"] = 56; row["PercentilD"] = 54; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "C"; row["Subfactor"] = "Cp"; row["Factor"] = "A";
            row["Percentil"] = 50; row["PercentilD"] = 46; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "D"; row["Subfactor"] = "Co"; row["Factor"] = "i";
            row["Percentil"] = 44; row["PercentilD"] = 46; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "E"; row["Subfactor"] = "Es"; row["Factor"] = "T";
            row["Percentil"] = 72; row["PercentilD"] = 66; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "F"; row["Subfactor"] = "Pe"; row["Factor"] = "i";
            row["Percentil"] = 53; row["PercentilD"] = 66; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "G"; row["Subfactor"] = "Ce"; row["Factor"] = "EE";
            row["Percentil"] = 33; row["PercentilD"] = 30; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "H"; row["Subfactor"] = "Ci"; row["Factor"] = "i";
            row["Percentil"] = 32; row["PercentilD"] = 30; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "I"; row["Subfactor"] = "Ac"; row["Factor"] = "AM";
            row["Percentil"] = 71; row["PercentilD"] = 65; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "J"; row["Subfactor"] = "Ae"; row["Factor"] = "i";
            row["Percentil"] = 53; row["PercentilD"] = 65; dtinvestigador.Rows.Add(row);
            row = dtinvestigador.NewRow();
            row["Idfactor"] = "K"; row["Subfactor"] = "D"; row["Factor"] = "D";
            row["Percentil"] = 44; row["PercentilD"] = 44; dtinvestigador.Rows.Add(row);
            ///////////////////////////////////////////////////////////////////////////////////////////////
            DataRow rows = dtcreativo.NewRow();
            rows["Idfactor"] = "A"; rows["Subfactor"] = "Di"; rows["Factor"] = "E";
            rows["Percentil"] = 56; rows["PercentilD"] = 54; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "B"; rows["Subfactor"] = "Do"; rows["Factor"] = "i";
            rows["Percentil"] = 52; rows["PercentilD"] = 54; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "C"; rows["Subfactor"] = "Cp"; rows["Factor"] = "A";
            rows["Percentil"] = 54; rows["PercentilD"] = 53; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "D"; rows["Subfactor"] = "Co"; rows["Factor"] = "i";
            rows["Percentil"] = 52; rows["PercentilD"] = 53; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "E"; rows["Subfactor"] = "Es"; rows["Factor"] = "T";
            rows["Percentil"] = 25; rows["PercentilD"] = 27; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "F"; rows["Subfactor"] = "Pe"; rows["Factor"] = "i";
            rows["Percentil"] = 41; rows["PercentilD"] = 27; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "G"; rows["Subfactor"] = "Ce"; rows["Factor"] = "EE";
            rows["Percentil"] = 39; rows["PercentilD"] = 39; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "H"; rows["Subfactor"] = "Ci"; rows["Factor"] = "i";
            rows["Percentil"] = 43; rows["PercentilD"] = 39; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "I"; rows["Subfactor"] = "Ac"; rows["Factor"] = "AM";
            rows["Percentil"] = 61; rows["PercentilD"] = 62; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "J"; rows["Subfactor"] = "Ae"; rows["Factor"] = "i";
            rows["Percentil"] = 60; rows["PercentilD"] = 62; dtcreativo.Rows.Add(rows);
            rows = dtcreativo.NewRow();
            rows["Idfactor"] = "K"; rows["Subfactor"] = "D"; rows["Factor"] = "D";
            rows["Percentil"] = 30; rows["PercentilD"] = 30; dtcreativo.Rows.Add(rows);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            DataRow fila = dtsocial.NewRow();
            fila["Idfactor"] = "A"; fila["Subfactor"] = "Di"; fila["Factor"] = "E";
            fila["Percentil"] = 58; fila["PercentilD"] = 53; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "B"; fila["Subfactor"] = "Do"; fila["Factor"] = "i";
            fila["Percentil"] = 45; fila["PercentilD"] = 53; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "C"; fila["Subfactor"] = "Cp"; fila["Factor"] = "A";
            fila["Percentil"] = 66; fila["PercentilD"] = 70; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "D"; fila["Subfactor"] = "Co"; fila["Factor"] = "i";
            fila["Percentil"] = 68; fila["PercentilD"] = 70; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "E"; fila["Subfactor"] = "Es"; fila["Factor"] = "T";
            fila["Percentil"] = 48; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "F"; fila["Subfactor"] = "Pe"; fila["Factor"] = "i";
            fila["Percentil"] = 50; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "G"; fila["Subfactor"] = "Ce"; fila["Factor"] = "EE";
            fila["Percentil"] = 49; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "H"; fila["Subfactor"] = "Ci"; fila["Factor"] = "i";
            fila["Percentil"] = 46; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "I"; fila["Subfactor"] = "Ac"; fila["Factor"] = "AM";
            fila["Percentil"] = 50; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "J"; fila["Subfactor"] = "Ae"; fila["Factor"] = "i";
            fila["Percentil"] = 45; fila["PercentilD"] = 47; dtsocial.Rows.Add(fila);
            fila = dtsocial.NewRow();
            fila["Idfactor"] = "K"; fila["Subfactor"] = "D"; fila["Factor"] = "D";
            fila["Percentil"] = 54; fila["PercentilD"] = 54; dtsocial.Rows.Add(fila);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            DataRow filas = dtdirigente.NewRow();
            filas["Idfactor"] = "A"; filas["Subfactor"] = "Di"; filas["Factor"] = "E";
            filas["Percentil"] = 58; filas["PercentilD"] = 64; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "B"; filas["Subfactor"] = "Do"; filas["Factor"] = "i";
            filas["Percentil"] = 68; filas["PercentilD"] = 64; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "C"; filas["Subfactor"] = "Cp"; filas["Factor"] = "A";
            filas["Percentil"] = 52; filas["PercentilD"] = 45; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "D"; filas["Subfactor"] = "Co"; filas["Factor"] = "i";
            filas["Percentil"] = 42; filas["PercentilD"] = 45; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "E"; filas["Subfactor"] = "Es"; filas["Factor"] = "T";
            filas["Percentil"] = 52; filas["PercentilD"] = 58; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "F"; filas["Subfactor"] = "Pe"; filas["Factor"] = "i";
            filas["Percentil"] = 63; filas["PercentilD"] = 58; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "G"; filas["Subfactor"] = "Ce"; filas["Factor"] = "EE";
            filas["Percentil"] = 48; filas["PercentilD"] = 47; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "H"; filas["Subfactor"] = "Ci"; filas["Factor"] = "i";
            filas["Percentil"] = 48; filas["PercentilD"] = 47; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "I"; filas["Subfactor"] = "Ac"; filas["Factor"] = "AM";
            filas["Percentil"] = 55; filas["PercentilD"] = 61; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "J"; filas["Subfactor"] = "Ae"; filas["Factor"] = "i";
            filas["Percentil"] = 66; filas["PercentilD"] = 61; dtdirigente.Rows.Add(filas);
            filas = dtdirigente.NewRow();
            filas["Idfactor"] = "K"; filas["Subfactor"] = "D"; filas["Factor"] = "D";
            filas["Percentil"] = 51; filas["PercentilD"] = 51; dtdirigente.Rows.Add(filas);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            DataRow filass = dtconvencional.NewRow();
            filass["Idfactor"] = "A"; filass["Subfactor"] = "Di"; filass["Factor"] = "E";
            filass["Percentil"] = 59; filass["PercentilD"] = 54; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "B"; filass["Subfactor"] = "Do"; filass["Factor"] = "i";
            filass["Percentil"] = 48; filass["PercentilD"] = 54; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "C"; filass["Subfactor"] = "Cp"; filass["Factor"] = "A";
            filass["Percentil"] = 45; filass["PercentilD"] = 50; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "D"; filass["Subfactor"] = "Co"; filass["Factor"] = "i";
            filass["Percentil"] = 54; filass["PercentilD"] = 50; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "E"; filass["Subfactor"] = "Es"; filass["Factor"] = "T";
            filass["Percentil"] = 62; filass["PercentilD"] = 62; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "F"; filass["Subfactor"] = "Pe"; filass["Factor"] = "i";
            filass["Percentil"] = 56; filass["PercentilD"] = 62; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "G"; filass["Subfactor"] = "Ce"; filass["Factor"] = "EE";
            filass["Percentil"] = 46; filass["PercentilD"] = 50; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "H"; filass["Subfactor"] = "Ci"; filass["Factor"] = "i";
            filass["Percentil"] = 54; filass["PercentilD"] = 50; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "I"; filass["Subfactor"] = "Ac"; filass["Factor"] = "AM";
            filass["Percentil"] = 42; filass["PercentilD"] = 37; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "J"; filass["Subfactor"] = "Ae"; filass["Factor"] = "i";
            filass["Percentil"] = 37; filass["PercentilD"] = 37; dtconvencional.Rows.Add(filass);
            filass = dtconvencional.NewRow();
            filass["Idfactor"] = "K"; filass["Subfactor"] = "D"; filass["Factor"] = "D";
            filass["Percentil"] = 47; filass["PercentilD"] = 47; dtconvencional.Rows.Add(filass);

        }



        private void button1_Click(object sender, EventArgs e)
        {
            dt.Clear();
            dto_bfq_resultados[] resultados = cliente.ListaBFQResultados(_expediente.Id);
            suma[0] = (resultados[0].Suma + 36) - resultados[1].Suma;
            suma[1] = (resultados[2].Suma + 36) - resultados[3].Suma;
            suma[2] = (resultados[4].Suma + 36) - resultados[5].Suma;
            suma[3] = (resultados[6].Suma + 36) - resultados[7].Suma;
            suma[4] = (resultados[8].Suma + 36) - resultados[9].Suma;
            suma[5] = (resultados[10].Suma + 36) - resultados[11].Suma;
            suma[6] = (resultados[12].Suma + 36) - resultados[13].Suma;
            suma[7] = (resultados[14].Suma + 36) - resultados[15].Suma;
            suma[8] = (resultados[16].Suma + 36) - resultados[17].Suma;
            suma[9] = (resultados[18].Suma + 36) - resultados[19].Suma;
            suma[10] = resultados[20].Suma;

            factor[0] = suma[0] + suma[1];
            factor[1] = suma[2] + suma[3];
            factor[2] = suma[4] + suma[5];
            factor[3] = suma[6] + suma[7];
            factor[4] = suma[8] + suma[9];
            factor[5] = suma[10];
            for (int i = 0; i < resultados.ToList().Count; i++)
            {
                DataRow FILAS = dt.NewRow();

                switch (resultados[i].IdFactor)
                {
                    case 1:
                        subfactor = "Di";
                        totalfactor = factor[0];
                        totalsubfactor = suma[0];
                        factores = "E";
                        orden = 0;


                        expresion = "idfactor= '1'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interdi = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rdi = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'E' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddesdimension"].ToString());

                        expresion = "idinterpretacion='" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        intere = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        re = Convert.ToString(filtro[0]["rango"].ToString());

                        break;
                    case 3:
                        subfactor = "Do";
                        totalfactor = factor[0];
                        totalsubfactor = suma[1];
                        factores = "i";
                        orden = 1;

                        expresion = "idfactor = '3'  and punto = '" + totalsubfactor + "' ";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interdo = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rdo = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'E' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());

                        break;
                    case 5:
                        subfactor = "Cp";
                        totalfactor = factor[1];
                        totalsubfactor = suma[2];
                        factores = "A";
                        orden = 11;

                        expresion = "idfactor= '5'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        intercp = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rcp = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'A' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddesdimension"].ToString());

                        expresion = "idinterpretacion='" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        intera = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        ra = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 7:
                        subfactor = "Co";
                        totalfactor = factor[1];
                        totalsubfactor = suma[3];
                        factores = "i";
                        orden = 111;

                        expresion = "idfactor= '7'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interco = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rco = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'A' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());

                        break;
                    case 9:
                        subfactor = "Es";
                        totalfactor = factor[2];
                        totalsubfactor = suma[4];
                        factores = "T";
                        orden = 1111;

                        expresion = "idfactor= '9'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interes = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        res = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'T' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddesdimension"].ToString());

                        expresion = "idinterpretacion='" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        intert = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rt = Convert.ToString(filtro[0]["rango"].ToString());

                        break;
                    case 11:
                        subfactor = "Pe";
                        totalfactor = factor[2];
                        totalsubfactor = suma[5];
                        factores = "i";
                        orden = 11111;

                        expresion = "idfactor= '11'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interpe = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rpe = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'T' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        break;
                    case 13:
                        subfactor = "Ce";
                        totalfactor = factor[3];
                        totalsubfactor = suma[6];
                        factores = "EE";
                        orden = 111111;

                        expresion = "idfactor= '13'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interce = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rce = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'EE' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddesdimension"].ToString());

                        expresion = "idinterpretacion='" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interee = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        ree = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 15:
                        subfactor = "Ci";
                        totalfactor = factor[3];
                        totalsubfactor = suma[7];
                        factores = "i";
                        orden = 1111111;

                        expresion = "idfactor= '15'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interci = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rci = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'EE' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        break;
                    case 17:
                        subfactor = "Ac";
                        totalfactor = factor[4];
                        totalsubfactor = suma[8];
                        factores = "AM";
                        orden = 11111111;

                        expresion = "idfactor= '17'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interac = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rac = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'AM' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddesdimension"].ToString());

                        expresion = "idinterpretacion='" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interam = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        ram = Convert.ToString(filtro[0]["rango"].ToString());
                        break;
                    case 19:
                        subfactor = "Ae";
                        totalfactor = factor[4];
                        totalsubfactor = suma[9];
                        factores = "i";
                        orden = 1111111111;

                        expresion = "idfactor= '19'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interae = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rae = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'AM' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());

                        break;
                    case 21:
                        subfactor = "D";
                        totalfactor = factor[5];
                        totalsubfactor = suma[10];
                        factores = "D";

                        expresion = "idfactor= '21'  and punto= '" + totalsubfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentil = Convert.ToInt32(filtro[0]["grafico"].ToString());
                        interpretacion = Convert.ToInt32(filtro[0]["iddescripcion"].ToString());

                        expresion = "idinterpretacion= '" + interpretacion + "'";
                        filtro = dtinter.Select(expresion);
                        interd = Convert.ToString(filtro[0]["interpretacion"].ToString());
                        rd = Convert.ToString(filtro[0]["rango"].ToString());

                        expresion = "factores = 'D' and puntofactor= '" + totalfactor + "'";
                        filtro = dtgrafica.Select(expresion);
                        percentilD = Convert.ToInt32(filtro[0]["graficofactor"].ToString());
                        break;
                }

                if (resultados[i].IdFactor == 1 || resultados[i].IdFactor == 3 || resultados[i].IdFactor == 5 || resultados[i].IdFactor == 7 || resultados[i].IdFactor == 9 || resultados[i].IdFactor == 11 || resultados[i].IdFactor == 13 || resultados[i].IdFactor == 15 || resultados[i].IdFactor == 17 || resultados[i].IdFactor == 19 || resultados[i].IdFactor == 21)
                {
                    FILAS["Idfactor"] = Convert.ToInt32(resultados[i].IdFactor);
                    FILAS["Subfactor"] = subfactor;
                    FILAS["Factor"] = factores;
                    FILAS["Suma"] = totalsubfactor;
                    FILAS["Total"] = totalfactor;
                    FILAS["Orden"] = orden;
                    FILAS["Percentil"] = percentil;
                    FILAS["PercentilD"] = percentilD;
                    FILAS["Serie"] = "Candidato";
                    dt.Rows.Add(FILAS);
                }
            }
            DataTable dtccreativo = new DataTable();
            dtccreativo = dt.Copy();
            DataTable dtcrealista = new DataTable();
            dtcrealista = dt.Copy();
            DataTable dtcinvestigador = new DataTable();
            dtcinvestigador = dt.Copy();
            DataTable dtcsocial = new DataTable();
            dtcsocial = dt.Copy();
            DataTable dtcdirigente = new DataTable();
            dtcdirigente = dt.Copy();
            DataTable dtcconvencional = new DataTable();
            dtcconvencional = dt.Copy();

            for (int i = 0; i < dtcreativo.Rows.Count; i++)
            {

                DataRow row = dtccreativo.NewRow();

                row["Idfactor"] = dtcreativo.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtcreativo.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtcreativo.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtcreativo.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtcreativo.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Creativo";

                dtccreativo.Rows.Add(row);

            }

            for (int i = 0; i < dtrealista.Rows.Count; i++)
            {

                DataRow row = dtcrealista.NewRow();

                row["Idfactor"] = dtrealista.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtrealista.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtrealista.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtrealista.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtrealista.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Realista";

                dtcrealista.Rows.Add(row);

            }

            for (int i = 0; i < dtinvestigador.Rows.Count; i++)
            {

                DataRow row = dtcinvestigador.NewRow();

                row["Idfactor"] = dtinvestigador.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtinvestigador.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtinvestigador.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtinvestigador.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtinvestigador.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Investigador";

                dtcinvestigador.Rows.Add(row);

            }

            for (int i = 0; i < dtsocial.Rows.Count; i++)
            {

                DataRow row = dtcsocial.NewRow();

                row["Idfactor"] = dtsocial.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtsocial.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtsocial.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtsocial.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtsocial.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Social";

                dtcsocial.Rows.Add(row);

            }

            for (int i = 0; i < dtdirigente.Rows.Count; i++)
            {

                DataRow row = dtcdirigente.NewRow();

                row["Idfactor"] = dtdirigente.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtdirigente.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtdirigente.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtdirigente.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtdirigente.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Dirigente";

                dtcdirigente.Rows.Add(row);

            }

            for (int i = 0; i < dtconvencional.Rows.Count; i++)
            {

                DataRow row = dtcconvencional.NewRow();

                row["Idfactor"] = dtconvencional.Rows[i]["Idfactor"].ToString();
                row["Subfactor"] = dtconvencional.Rows[i]["Subfactor"].ToString();
                row["Factor"] = dtconvencional.Rows[i]["Factor"].ToString();
                row["Suma"] = 0;
                row["Total"] = 0;
                row["Orden"] = 0;
                row["Percentil"] = Convert.ToInt32(dtconvencional.Rows[i]["Percentil"].ToString());
                row["PercentilD"] = Convert.ToInt32(dtconvencional.Rows[i]["PercentilD"].ToString());
                row["Serie"] = "Convencional";

                dtcconvencional.Rows.Add(row);

            }

            ReportParameter[] parametros = new ReportParameter[17];
            parametros[0] = new ReportParameter("Perfil", cbbperfil.Text);
            parametros[1] = new ReportParameter("DIINTER", rdi + ": " + interdi);
            parametros[2] = new ReportParameter("DOINTER", rdo + ": " + interdo);
            parametros[3] = new ReportParameter("CPINTER", rcp + ": " + intercp);
            parametros[4] = new ReportParameter("COINTER", rco + ": " + interco);
            parametros[5] = new ReportParameter("ESINTER", res + ": " + interes);
            parametros[6] = new ReportParameter("PEINTER", rpe + ": " + interpe);
            parametros[7] = new ReportParameter("CEINTER", rce + ": " + interce);
            parametros[8] = new ReportParameter("CIINTER", rci + ": " + interci);
            parametros[9] = new ReportParameter("ACINTER", rac + ": " + interac);
            parametros[10] = new ReportParameter("AEINTER", rae + ": " + interae);
            parametros[11] = new ReportParameter("EINTER", re + ": " + intere);
            parametros[12] = new ReportParameter("AINTER", ra + ": " + intera);
            parametros[13] = new ReportParameter("TINTER", rt + ": " + intert);
            parametros[14] = new ReportParameter("EEINTER", ree + ": " + interee);
            parametros[15] = new ReportParameter("AMINTER", ram + ": " + interam);
            parametros[16] = new ReportParameter("DINTER", rd + ": " + interd);

            ReportDataSource rds = new ReportDataSource("DataSetbfq", dt);
            ReportDataSource realista = new ReportDataSource("DataSetbfqr", dtcrealista);
            ReportDataSource investigador = new ReportDataSource("DataSetbfqi", dtcinvestigador);
            ReportDataSource creativo = new ReportDataSource("DataSetbfqc", dtccreativo);
            ReportDataSource social = new ReportDataSource("DataSetbfqs", dtcsocial);
            ReportDataSource dirigente = new ReportDataSource("DataSetbfqd", dtcdirigente);
            ReportDataSource convencional = new ReportDataSource("DataSetbfqco", dtcconvencional);
            ReportDataSource rdsCan = new ReportDataSource("dsCandidato", dtCandidato);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.Pruebas.Bfq.reporteBFQ.rdlc";
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.DataSources.Add(realista);
            reportViewer1.LocalReport.DataSources.Add(investigador);
            reportViewer1.LocalReport.DataSources.Add(creativo);
            reportViewer1.LocalReport.DataSources.Add(social);
            reportViewer1.LocalReport.DataSources.Add(dirigente);
            reportViewer1.LocalReport.DataSources.Add(convencional);
            reportViewer1.LocalReport.DataSources.Add(rdsCan);

            reportViewer1.LocalReport.SetParameters(parametros);

            reportViewer1.RefreshReport();
        }
    }
}
