﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Reporting.WinForms;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmReporteExpediente : DevExpress.XtraEditors.XtraForm
    {
        private QuickHumClient Cliente = Globales.cliente;
        private Expediente _expediente = null;

        DataTable data = new DataTable();

        public frmReporteExpediente(Expediente expediente)
        {
            InitializeComponent();
            _expediente = expediente;
        }

        private void frmReporteExpediente_Load(object sender, EventArgs e)
        {
            data.Columns.Add("NombreApellido");
            data.Columns.Add("Edad");
            data.Columns.Add("Dui");
            data.Columns.Add("Genero");
            data.Columns.Add("EstadoCivil");
            data.Columns.Add("Telefono");
            data.Columns.Add("DireccionSucursal");
            data.Columns.Add("NumeroExpediente");

            DataRow row = data.NewRow();
            row["NombreApellido"] = _expediente.Candidato.Nombre + " " + _expediente.Candidato.Apellido;
            row["Edad"] = _expediente.Candidato.Edad;
            row["Dui"] = _expediente.Candidato.Dui;
            row["Genero"] = _expediente.Candidato.Genero;
            row["EstadoCivil"] = _expediente.Candidato.EstadoCivil;
            row["Telefono"] = _expediente.Candidato.Telefono;
            row["DireccionSucursal"] = _expediente.Ubicacion.Departamento + " " + _expediente.Ubicacion.Municipio + " " + _expediente.Ubicacion.Direccion;
            row["NumeroExpediente"] = _expediente.Numero;
            data.Rows.Add(row);
            data.Columns.Add("Pruebas");
            for (int x = 0; x < _expediente.PruebasAsignadas.Length; ++x)
            {
                DataRow rows = data.NewRow();
                rows["Pruebas"] = _expediente.PruebasAsignadas[x].TipoPrueba;
                data.Rows.Add(rows);
            }
            
            ReportDataSource reporte = new ReportDataSource("dtExpediente", data);
            rvExpediente.LocalReport.DataSources.Clear();
            
            rvExpediente.LocalReport.ReportEmbeddedResource = "QuickHumCliente.Vistas.ReporteExpediente.rdlc";
            rvExpediente.LocalReport.DataSources.Add(reporte);
            this.rvExpediente.RefreshReport();
        }
    }
}