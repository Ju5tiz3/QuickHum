﻿namespace QuickHumCliente.Vistas
{
    partial class frmConfirmarSeleccionPrueba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentedorTituloPestana = new DevExpress.XtraEditors.PanelControl();
            this.CabezeraPrueba = new DevExpress.XtraEditors.LabelControl();
            this.lblNombrePrueba = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDescripcionPrueba = new System.Windows.Forms.Label();
            this.lblTiempo = new System.Windows.Forms.Label();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lookEscolaridades = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).BeginInit();
            this.ContentedorTituloPestana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookEscolaridades.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentedorTituloPestana
            // 
            this.ContentedorTituloPestana.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ContentedorTituloPestana.Appearance.Options.UseBackColor = true;
            this.ContentedorTituloPestana.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ContentedorTituloPestana.Controls.Add(this.CabezeraPrueba);
            this.ContentedorTituloPestana.Dock = System.Windows.Forms.DockStyle.Top;
            this.ContentedorTituloPestana.Location = new System.Drawing.Point(0, 0);
            this.ContentedorTituloPestana.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContentedorTituloPestana.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContentedorTituloPestana.Name = "ContentedorTituloPestana";
            this.ContentedorTituloPestana.Size = new System.Drawing.Size(501, 48);
            this.ContentedorTituloPestana.TabIndex = 4;
            // 
            // CabezeraPrueba
            // 
            this.CabezeraPrueba.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CabezeraPrueba.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.CabezeraPrueba.Dock = System.Windows.Forms.DockStyle.Top;
            this.CabezeraPrueba.Location = new System.Drawing.Point(0, 0);
            this.CabezeraPrueba.Name = "CabezeraPrueba";
            this.CabezeraPrueba.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.CabezeraPrueba.Size = new System.Drawing.Size(171, 36);
            this.CabezeraPrueba.TabIndex = 1;
            this.CabezeraPrueba.Text = "Asignacion de Prueba";
            // 
            // lblNombrePrueba
            // 
            this.lblNombrePrueba.AutoSize = true;
            this.lblNombrePrueba.Font = new System.Drawing.Font("Open Sans Condensed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrePrueba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.lblNombrePrueba.Location = new System.Drawing.Point(6, 64);
            this.lblNombrePrueba.Name = "lblNombrePrueba";
            this.lblNombrePrueba.Size = new System.Drawing.Size(106, 37);
            this.lblNombrePrueba.TabIndex = 5;
            this.lblNombrePrueba.Text = "LUSCHER";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(12, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(477, 2);
            this.label2.TabIndex = 6;
            // 
            // lblDescripcionPrueba
            // 
            this.lblDescripcionPrueba.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcionPrueba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.lblDescripcionPrueba.Location = new System.Drawing.Point(12, 236);
            this.lblDescripcionPrueba.Name = "lblDescripcionPrueba";
            this.lblDescripcionPrueba.Size = new System.Drawing.Size(477, 144);
            this.lblDescripcionPrueba.TabIndex = 7;
            this.lblDescripcionPrueba.Text = "Descripcion prueba";
            // 
            // lblTiempo
            // 
            this.lblTiempo.AutoSize = true;
            this.lblTiempo.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTiempo.Location = new System.Drawing.Point(99, 128);
            this.lblTiempo.Name = "lblTiempo";
            this.lblTiempo.Size = new System.Drawing.Size(87, 18);
            this.lblTiempo.TabIndex = 9;
            this.lblTiempo.Text = "20 MINUTOS";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(394, 394);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnAceptar.Size = new System.Drawing.Size(95, 35);
            this.btnAceptar.TabIndex = 10;
            this.btnAceptar.Text = "Asignar Prueba";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(293, 394);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnCancelar.Size = new System.Drawing.Size(95, 35);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.label5.Location = new System.Drawing.Point(12, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tiempo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.label6.Location = new System.Drawing.Point(10, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Escolaridad";
            // 
            // lookEscolaridades
            // 
            this.lookEscolaridades.Location = new System.Drawing.Point(102, 160);
            this.lookEscolaridades.Name = "lookEscolaridades";
            this.lookEscolaridades.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(190)))), ((int)(((byte)(190)))));
            this.lookEscolaridades.Properties.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookEscolaridades.Properties.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lookEscolaridades.Properties.Appearance.Options.UseBorderColor = true;
            this.lookEscolaridades.Properties.Appearance.Options.UseFont = true;
            this.lookEscolaridades.Properties.Appearance.Options.UseForeColor = true;
            this.lookEscolaridades.Properties.AutoHeight = false;
            this.lookEscolaridades.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lookEscolaridades.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookEscolaridades.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Escolaridad", "Escolaridad")});
            this.lookEscolaridades.Properties.DropDownItemHeight = 28;
            this.lookEscolaridades.Size = new System.Drawing.Size(383, 28);
            this.lookEscolaridades.TabIndex = 15;
            this.lookEscolaridades.EditValueChanged += new System.EventHandler(this.lookEscolaridades_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.label1.Location = new System.Drawing.Point(205, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Descripción";
            // 
            // frmConfirmarSeleccionPrueba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 434);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lookEscolaridades);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lblTiempo);
            this.Controls.Add(this.lblDescripcionPrueba);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblNombrePrueba);
            this.Controls.Add(this.ContentedorTituloPestana);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfirmarSeleccionPrueba";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Asignar Pruebas";
            this.Load += new System.EventHandler(this.frmConfirmarSeleccionPrueba_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentedorTituloPestana)).EndInit();
            this.ContentedorTituloPestana.ResumeLayout(false);
            this.ContentedorTituloPestana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookEscolaridades.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ContentedorTituloPestana;
        private DevExpress.XtraEditors.LabelControl CabezeraPrueba;
        private System.Windows.Forms.Label lblNombrePrueba;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDescripcionPrueba;
        private System.Windows.Forms.Label lblTiempo;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit lookEscolaridades;
        private System.Windows.Forms.Label label1;
    }
}