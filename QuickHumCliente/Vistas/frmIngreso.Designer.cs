﻿namespace QuickHumCliente.Vistas
{
    partial class frmIngreso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnConfiguracion = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.cmbSeleccionLogin = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LoginContenedor = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAcceder = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfiguracion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSeleccionLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginContenedor)).BeginInit();
            this.LoginContenedor.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnConfiguracion);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(464, 48);
            this.panelControl1.TabIndex = 0;
            // 
            // btnConfiguracion
            // 
            this.btnConfiguracion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfiguracion.EditValue = global::QuickHumCliente.Properties.Resources.settings11;
            this.btnConfiguracion.Location = new System.Drawing.Point(422, 10);
            this.btnConfiguracion.Name = "btnConfiguracion";
            this.btnConfiguracion.Properties.AllowFocused = false;
            this.btnConfiguracion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnConfiguracion.Properties.Appearance.Options.UseBackColor = true;
            this.btnConfiguracion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnConfiguracion.Size = new System.Drawing.Size(32, 32);
            this.btnConfiguracion.TabIndex = 1;
            this.btnConfiguracion.Click += new System.EventHandler(this.btnConfiguracion_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Open Sans Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(464, 48);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Inicio de sesion";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.cmbSeleccionLogin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 216);
            this.panelControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(464, 40);
            this.panelControl2.TabIndex = 1;
            // 
            // cmbSeleccionLogin
            // 
            this.cmbSeleccionLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSeleccionLogin.EditValue = "CANDIDATO";
            this.cmbSeleccionLogin.Location = new System.Drawing.Point(0, 0);
            this.cmbSeleccionLogin.Name = "cmbSeleccionLogin";
            this.cmbSeleccionLogin.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.cmbSeleccionLogin.Properties.Appearance.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSeleccionLogin.Properties.Appearance.ForeColor = System.Drawing.Color.Turquoise;
            this.cmbSeleccionLogin.Properties.Appearance.Options.UseBackColor = true;
            this.cmbSeleccionLogin.Properties.Appearance.Options.UseFont = true;
            this.cmbSeleccionLogin.Properties.Appearance.Options.UseForeColor = true;
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.Turquoise;
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSeleccionLogin.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.cmbSeleccionLogin.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.cmbSeleccionLogin.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.cmbSeleccionLogin.Properties.AutoHeight = false;
            this.cmbSeleccionLogin.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbSeleccionLogin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSeleccionLogin.Properties.DropDownItemHeight = 32;
            this.cmbSeleccionLogin.Properties.HighlightedItemStyle = DevExpress.XtraEditors.HighlightStyle.Standard;
            this.cmbSeleccionLogin.Properties.Items.AddRange(new object[] {
            "CANDIDATO",
            "EVALUADOR"});
            this.cmbSeleccionLogin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbSeleccionLogin.Size = new System.Drawing.Size(464, 40);
            this.cmbSeleccionLogin.TabIndex = 0;
            this.cmbSeleccionLogin.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // LoginContenedor
            // 
            this.LoginContenedor.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.LoginContenedor.Appearance.Options.UseBackColor = true;
            this.LoginContenedor.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LoginContenedor.Controls.Add(this.tableLayoutPanel1);
            this.LoginContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoginContenedor.Location = new System.Drawing.Point(0, 48);
            this.LoginContenedor.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LoginContenedor.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LoginContenedor.Name = "LoginContenedor";
            this.LoginContenedor.Size = new System.Drawing.Size(464, 168);
            this.LoginContenedor.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel1.Controls.Add(this.btnAcceder, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 126);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(464, 42);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnAcceder
            // 
            this.btnAcceder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcceder.Location = new System.Drawing.Point(360, 5);
            this.btnAcceder.Margin = new System.Windows.Forms.Padding(5, 5, 10, 5);
            this.btnAcceder.Name = "btnAcceder";
            this.btnAcceder.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnAcceder.Size = new System.Drawing.Size(94, 32);
            this.btnAcceder.TabIndex = 0;
            this.btnAcceder.Text = "Acceder";
            this.btnAcceder.Click += new System.EventHandler(this.Acceder);
            // 
            // frmIngreso
            // 
            this.AcceptButton = this.btnAcceder;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 256);
            this.Controls.Add(this.LoginContenedor);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmIngreso";
            this.Text = "Acceso";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnConfiguracion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbSeleccionLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginContenedor)).EndInit();
            this.LoginContenedor.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl LoginContenedor;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSeleccionLogin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnAcceder;
        private DevExpress.XtraEditors.PictureEdit btnConfiguracion;
    }
}