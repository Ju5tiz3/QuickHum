﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using System.ServiceModel;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Vistas
{
    public partial class frmIngreso : DevExpress.XtraEditors.XtraForm
    {

        private Form _pantalla_actual = null;

        public frmIngreso()
        {
            InitializeComponent();
            CargarPantallaCandidato();
        }

        /// <summary>
        /// Carga los controles necesarios en el formulario para poder acceder al panel de administracion de los evaluadores.
        /// </summary>
        private void CargarPantallaPsicologo()
        {
            if (_pantalla_actual != null)
            {
                this.LoginContenedor.Controls.Remove(_pantalla_actual);
                _pantalla_actual.Close();
            }
            frmIngresoEvaluador pantalla_evaluador = new frmIngresoEvaluador();
            pantalla_evaluador.Name = "PantallaEvaluador";
            _pantalla_actual = pantalla_evaluador;
            pantalla_evaluador.TopLevel = false;
            pantalla_evaluador.Show();
            pantalla_evaluador.Dock = DockStyle.Fill;
            LoginContenedor.Controls.Add(pantalla_evaluador);
        }

        /// <summary>
        /// Carga los controles necesarios en el formulario de login para poder acceder a las formas del candidato.
        /// </summary>
        private void CargarPantallaCandidato()
        {
            if (_pantalla_actual != null)
            {
                this.LoginContenedor.Controls.Remove(_pantalla_actual);
                _pantalla_actual.Close();
            }
            frmIngresoCandidato pantalla_candidato = new frmIngresoCandidato();
            pantalla_candidato.Name = "PantallaCandidato";
            _pantalla_actual = pantalla_candidato;
            pantalla_candidato.TopLevel = false;
            pantalla_candidato.Show();
            pantalla_candidato.Dock = DockStyle.Fill;
            LoginContenedor.Controls.Add(pantalla_candidato);
        }

        /// <summary>
        /// Maneja que controles son mostrados deacuerdo al tipo de login que se realizara.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch( ((ComboBoxEdit)sender).SelectedIndex )
            {
                case 0:
                    CargarPantallaCandidato();
                    break;
                case 1:
                    CargarPantallaPsicologo();
                    break;
            }
        }

        /// <summary>
        /// Ejecuta la funcion correspondiente para iniciar sesion dependiendo del tipo de usuario que intenta ingresar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Acceder(object sender, EventArgs e)
        {
            try
            {
                Globales.InicializarServicio();
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                XtraMessageBox.Show(
                    "No se puedo alcanzar el extremo remoto. Favor verifique la dirección IP del servicio y que el servicio este en ejecución.",
                    "Error Servicio Web", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Globales.cliente.Abort();
                return;
            }
            catch (TimeoutException timeProblem)
            {
                XtraMessageBox.Show("La operacion del servicio tomo mas tiempo del esperado: " + timeProblem.Message,
                    "Error Servicio Web", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Globales.cliente.Abort();
                return;
            }
            catch (CommunicationException commProblem)
            {
                XtraMessageBox.Show("Ocurrio un problema de comunicacion " + commProblem.Message, "Error Servicio Web",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Globales.cliente.Abort();
                return;
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema de comunicacion " + ex.Message, "Error Servicio Web", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Globales.cliente.Abort();
                return;
            }
            BOLAutenticacion control_autenticacion = new BOLAutenticacion();
            switch ( cmbSeleccionLogin.SelectedIndex )
            {
                case 0:
                    IniciarSesionCandidato();
                    break;
                case 1:
                    IniciarSesionEvaluador();
                    break;
            }
        }

        /// <summary>
        /// Inicia sesion como un evaluador
        /// </summary>
        private void IniciarSesionEvaluador()
        {
            frmIngresoEvaluador formulario_login = (frmIngresoEvaluador)LoginContenedor.Controls["PantallaEvaluador"];
            BOLAutenticacion control_autenticacion = new BOLAutenticacion();
            try
            {
                control_autenticacion.IniciarSesion(formulario_login.Usuario, formulario_login.Password);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un error al iniciar sesion: " + ex.Message, "Error inicio de sesion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un error al iniciar sesion: " + ex.Message, "Error inicio de sesion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.Hide();
            frmAdministracion formulario_adnministracion = new frmAdministracion(formulario_login.Usuario);
            formulario_adnministracion.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// Inicia sesion como candidato y muestra la interfaz para la ejecucion de las pruebas
        /// </summary>
        private void IniciarSesionCandidato(){
            frmIngresoCandidato formulario_login = (frmIngresoCandidato) LoginContenedor.Controls["PantallaCandidato"];
            BOLAutenticacion control_autenticacion = new BOLAutenticacion();
            try
            {
                if (control_autenticacion.IniciarSesionCandidato(formulario_login.Dui, formulario_login.Password) ==
                    false)
                {
                    XtraMessageBox.Show("La contraseña es incorrecta.", "Autenticacion", MessageBoxButtons.OK,
                        MessageBoxIcon.Stop);
                    return;
                }
                BOLCandidato bol_candidato = new BOLCandidato();
                Globales.Candidato = bol_candidato.CargarCandidato(formulario_login.Dui);
            }
            catch (FaultException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al iniciar la sesion del candidato: " + ex.Message,
                    "Problema ingreso candidato", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show("Ocurrio un problema al iniciar la sesion del candidato: " + ex.Message,
                    "Problema ingreso candidato", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.Hide();
            frmPruebasAsignadas pruebas_asignadas = new frmPruebasAsignadas();
            if (pruebas_asignadas.IsDisposed)
            {
                this.Close();
                return;}
            pruebas_asignadas.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// Muestra la ventana de configuraciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfiguracion_Click(object sender, EventArgs e)
        {
            frmConfiguracion frm_configuracion = new frmConfiguracion();
            frm_configuracion.ShowDialog();
        }
    }}