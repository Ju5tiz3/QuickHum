﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    class DAOUsuario
    {
        /// <summary>
        /// Invoca la funcion en el servicio para actualizar la contraseña de un usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool ActualizarPassword(Usuario usuario, string password)
        {
            return Globales.cliente.ActualizarPassword(usuario, password);
        }

        /// <summary>
        /// Invoca la funcion en el servicio para recuperar la informacion de un usuario
        /// </summary>
        /// <param name="codigo_usuario"></param>
        /// <returns></returns>
        public InformacionUsuario CargarInformacionUsuario(int codigo_usuario)
        {
            return Globales.cliente.CargarInformacionUsuario(codigo_usuario);
        }
    }
}
