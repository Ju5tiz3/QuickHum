﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraReports.Parameters;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    class DAOReportes
    {
        /// <summary>
        /// Devuelve un arreglo de objetos que describe la prueba y la cantidad de pruebas realizadas de esta por genero.
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static PruebasRealizadasGenero[] ObtenerPruebasRealizadasPorGenero(int usuario_id, short sucursal_id, DateTime fecha_inicio, DateTime fecha_final)
        {
            return Globales.cliente.ObtenerPruebasRealizadasPorGenero(usuario_id, sucursal_id, fecha_inicio, fecha_final);
        }

        /// <summary>
        /// Devuelve 
        /// </summary>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_fin"></param>
        /// <returns></returns>
        public static PruebasRealizadasSucursal[] ObtenerPruebasRealizadasPorSucursal(short sucursal_id, DateTime fecha_inicio, DateTime fecha_fin)
        {
            return Globales.cliente.ObtenerPruebasRealizadasPorSucursal(sucursal_id, fecha_inicio, fecha_fin);
        }

        /// <summary>
        /// Retorna un objeto con las estadisticas de los expedientes filtrados por los parametros
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicial"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static EstadisticasExpedientes ObtenerEstadisticasExpedientes
            (int usuario_id, short sucursal_id,DateTime fecha_inicial, DateTime fecha_final)
        {
            return Globales.cliente.ObtenerEstadisticasExpedientes(usuario_id, sucursal_id, fecha_inicial, fecha_final);
        }
    }
}
