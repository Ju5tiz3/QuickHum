﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;


namespace QuickHumCliente.Motor.Datos
{
    class DAOCandidato
    {
        /// <summary>
        /// Accede a la funcion de registro de candidatos del servicio web
        /// </summary>
        /// <param name="candidato"></param>
        /// <returns></returns>
        public bool RegistrarCandidato(ServicioQuickHum.Candidato candidato)
        {
            Globales.cliente.RegistrarCandidato(candidato);
            return true;
        }

        /// <summary>
        /// Accede a la funcion de actualizar del servicio web
        /// </summary>
        /// <param name="candidato"></param>
        /// <returns></returns>
        public bool ActualizarCandidato(ServicioQuickHum.Candidato candidato)
        {
            return Globales.cliente.ActualizarCandidato(candidato);
        }

        /// <summary>
        /// Accede a la funcion de cargar candidato en el servicio web
        /// </summary>
        /// <param name="dui"></param>
        /// <returns></returns>
        public ServicioQuickHum.Candidato CargarCandidato(string dui)
        {
            ServicioQuickHum.Candidato candidato = Globales.cliente.CargarCandidato(dui);
            return candidato;
        }

        /// <summary>
        /// Accede a la funcion de eliminar un candidato en el servicio web.
        /// </summary>
        /// <param name="dui"></param>
        /// <returns></returns>
        public bool EliminarCandidato(string dui)
        {
            bool resultado = Globales.cliente.EliminarCandidato(dui);
            return resultado;
        }
    }
}
