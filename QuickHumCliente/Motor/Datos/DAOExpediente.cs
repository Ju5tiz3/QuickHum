﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    class DAOExpediente
    {
        /// <summary>
        /// Devuelve una lista de expedientes de un usuario en especifico
        /// </summary>
        /// <param name="dui_candidato"></param>
        /// <returns></returns>
        public ServicioQuickHum.Expediente[] ListarExpedientes(string dui_candidato)
        {
            ServicioQuickHum.Expediente[] expedientes = Globales.cliente.ListarExpedientesCandidato(dui_candidato);
            return expedientes;
        }

        /// <summary>
        /// Guarda un nuevo expediente para el candidato
        /// </summary>
        /// <param name="expediente"></param>
        /// <returns></returns>
        public bool NuevoExpediente(Expediente expediente)
        {
            return Globales.cliente.RegistrarExpediente(expediente);
        }

        /// <summary>
        /// Carga el expediente especificado
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <returns></returns>
        public Expediente CargarExpediente(int numero_expediente)
        {
            return Globales.cliente.CargarExpediente(numero_expediente);
        }

        /// <summary>
        /// Actualiza las anotaciones de un expediente en especifico
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <param name="anotaciones"></param>
        public bool ActualizarAnotacionesExpediente(int numero_expediente, string anotaciones)
        {
            return Globales.cliente.GuardarAnotacionExpediente(numero_expediente, anotaciones);
        }

        /// <summary>
        /// Cambia el estado de un expediente especifico
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        public bool CambiarEstadoExpediente(int numero_expediente, Expediente.EstadoExpediente estado)
        {
            return Globales.cliente.CambiarEstadoExpediente(numero_expediente, estado);
        }
    
    }
}
