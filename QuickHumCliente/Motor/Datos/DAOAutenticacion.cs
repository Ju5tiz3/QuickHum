﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    internal class DAOAutenticacion
    {
        /// <summary>
        /// Inicia una sesion autenticada que retorna un token de autenticacion
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Usuario IniciarSesion(string nombre, string password)
        {
             return Globales.cliente.IniciarSesion(nombre, password);
        }

        /// <summary>
        /// Autentifica el candidato
        /// </summary>
        /// <param name="dui"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IniciarSesionCandidato(string dui, string password)
        {
            return Globales.cliente.IniciarSesionCandidato(dui, password);
        }
        
        /// <summary>
        /// Carga un usuario del sistema
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public ServicioQuickHum.Usuario CargarUsuario(string nombre)
        {
            ServicioQuickHum.Usuario usuario = Globales.cliente.CargarUsuario(nombre);
            return usuario;
        }

        /// <summary>
        /// Registra un acceso temporal mediante el servicio
        /// </summary>
        /// <param name="candidato"></param>
        /// <param name="duracion"></param>
        /// <returns></returns>
        public string RegistrarAccesoTemporal(Candidato candidato, double duracion)
        {
            return Globales.cliente.RegistrarAccesoTemporal(candidato, duracion);
        }

        /// <summary>
        /// Elimina los accesos cuya fecha ya expiro
        /// </summary>
        /// <returns></returns>
        public bool DepurarAccesosTemporales()
        {
            return Globales.cliente.DepurarAccesos();
        }
    }
}
