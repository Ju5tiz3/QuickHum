﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    class DAOEscolaridad
    {
        /// <summary>
        /// Devuelve una lista de las escolariades
        /// </summary>
        /// <returns></returns>
        public Escolaridad[] ListarEscolaridades()
        {
            return Globales.cliente.ListaEscolaridades();
        }

        /// <summary>
        /// Registra una nueva escolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        /// <returns></returns>
        public bool NuevaEscolaridad(Escolaridad escolaridad)
        {
            return Globales.cliente.RegistrarEscolaridad(escolaridad);
        }

        /// <summary>
        /// Actualiza una escolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        /// <returns></returns>
        public bool ActualizarEscolaridad(Escolaridad escolaridad)
        {
            return Globales.cliente.ActualizarEscolaridad(escolaridad);
        }

        /// <summary>
        /// Elimina una escolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        /// <returns></returns>
        public bool EliminarEscolaridad(Escolaridad escolaridad)
        {
            return Globales.cliente.EliminarEscolaridad(escolaridad);
        }
    }
}
