﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    class DAOOcupacion
    {
        /// <summary>
        /// Enlista las ocupaciones
        /// </summary>
        /// <returns></returns>
        public ServicioQuickHum.Ocupacion[] ListarOcupaciones()
        {
            ServicioQuickHum.Ocupacion[] ocupaciones = Globales.cliente.ListaOcupaciones();
            return ocupaciones;
        }

        /// <summary>
        /// Registra una nueva ocupacion
        /// </summary>
        /// <param name="ocupacion"></param>
        public bool NuevaOcupacion(Ocupacion ocupacion)
        {
            return Globales.cliente.RegistrarOcupacion(ocupacion);
        }

        /// <summary>
        /// Actualiza una ocupacion especificada
        /// </summary>
        /// <param name="ocupacion"></param>
        /// <returns></returns>
        public bool ActualizarOcupacion(Ocupacion ocupacion)
        {
            return Globales.cliente.ActualizarOcupacion(ocupacion);
        }

        /// <summary>
        /// Elimina una ocupacion especificada
        /// </summary>
        /// <param name="ocupacion"></param>
        /// <returns></returns>
        public bool EliminarOcupacion(Ocupacion ocupacion)
        {
            return Globales.cliente.EliminarOcupacion(ocupacion);
        }
    }
}
