﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Negocio;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Datos
{
    internal class DAOPrueba
    {
        /// <summary>
        /// Devuelve la lista de las pruebas creadas.
        /// </summary>
        /// <returns></returns>
        public ServicioQuickHum.Prueba[] ListaPruebas()
        {
            return Globales.cliente.ListaPruebas();
        }

        /// <summary>
        /// Devuelve el estado de una prueba
        /// </summary>
        /// <param name="test"></param>
        /// <param name="numero_prueba"></param>
        /// <returns></returns>
        public PruebaEstado ObtenerEstadoPrueba(Test test, int expediente_id)
        {
            return expediente_id == -1 ? PruebaEstado.INACTIVA : Globales.cliente.ObtenerEstadoPrueba(test, expediente_id);
        }

        /// <summary>
        /// Devuelve el numero de una prueba tipo X en el expediente N
        /// </summary>
        /// <param name="test"></param>
        /// <param name="numero_expediente"></param>
        /// <returns></returns>
        public int ObtenerNumeroPrueba(Test test, int numero_expediente)
        {
            return Globales.cliente.ObtenerNumeroPrueba(test, numero_expediente);
        }

        /// <summary>
        /// Devuelve las escolaridades permitidas a un tipo de prueba
        /// </summary>
        /// <param name="test"></param>
        /// <returns></returns>
        public EscolaridadPrueba[] ObtenerEscolaridadesPrueba(Test test)
        {
            return Globales.cliente.ObtenerEscolaridadPrueba(test);
        }

        ///// <summary>// Crea una nueva prueba IPV
        ///// </summary>
        ///// <param name="id_expediente"></param>
        ///// <returns></returns>
        //public IPV CrearIPV(int id_expediente, EscolaridadPrueba escolaridad_prueba)
        //{
        //    return Globales.cliente.NuevoIPV(id_expediente, escolaridad_prueba);
        //}

    }
}
