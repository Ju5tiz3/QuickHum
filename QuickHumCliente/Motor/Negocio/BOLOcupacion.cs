﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLOcupacion
    {
        /// <summary>
        /// Devuelve una lista de todas las ocupaciones
        /// </summary>
        /// <returns></returns>
        public ServicioQuickHum.Ocupacion[] ListarOcupaciones()
        {
            DAOOcupacion dao_ocupacion = new Datos.DAOOcupacion();
            return dao_ocupacion.ListarOcupaciones();
        }

        /// <summary>
        /// Registra una nueva ocupacion
        /// </summary>
        /// <param name="ocupacion"></param>
        /// <returns></returns>
        public bool NuevaOcupacion(Ocupacion ocupacion)
        {
            ValidarOcupacion(ocupacion);
            DAOOcupacion dao_ocupacion = new DAOOcupacion();
            return dao_ocupacion.NuevaOcupacion(ocupacion);
        }

        /// <summary>
        /// Actualiza una ocupacion
        /// </summary>
        /// <param name="ocupacion"></param>
        /// <returns></returns>
        public bool ActualizarOcupacion(Ocupacion ocupacion, string nuevo_nombre, string nueva_descripcion)
        {
            ocupacion.Nombre = nuevo_nombre;
            ocupacion.Descripcion = nueva_descripcion;
            ValidarOcupacion(ocupacion);
            if(ocupacion.Id == -1)
                throw new ArgumentException("El id de la ocupacion es nulo");
            DAOOcupacion dao_ocupacion = new DAOOcupacion();
            return dao_ocupacion.ActualizarOcupacion(ocupacion);
        }

        /// <summary>
        /// Elimina una ocupacion
        /// </summary>
        /// <param name="ocupacion"></param>
        /// <returns></returns>
        public bool EliminarOcupacion(Ocupacion ocupacion)
        {
            ValidarOcupacion(ocupacion);
            DAOOcupacion dao_ocupacion = new DAOOcupacion();
            return dao_ocupacion.EliminarOcupacion(ocupacion);
        }

        /// <summary>
        /// Valida el objeto ocupacion utulizado para las diferentes funciones de esta clase
        /// </summary>
        /// <param name="ocupacion"></param>
        private void ValidarOcupacion(Ocupacion ocupacion)
        {
            if(ocupacion == null)
                throw new ArgumentException("La ocupacion es nula");
            if(ocupacion.Nombre == null || ocupacion.Descripcion == null)
                throw  new ArgumentException("Los valores de la ocupacion son nulos");
            if(ocupacion.Nombre == string.Empty || ocupacion.Descripcion == string.Empty)
                throw  new ArgumentException("Los valores de la ocupacion se encuentran vacios");

            // Removemos espacios en blancos consecutivos

            ocupacion.Nombre = ocupacion.Nombre.Trim();
            ocupacion.Descripcion = ocupacion.Descripcion.Trim();
        }
    }
}
