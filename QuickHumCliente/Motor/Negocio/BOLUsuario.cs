﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLUsuario
    {
        /// <summary>
        /// Actualiza el password de un usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool ActualizarPassword(Usuario usuario, string passwordA, string passwordB)
        {
            if (passwordA != passwordB)
                throw new ArgumentException("Las contraseñas no coinciden.");
            DAOUsuario dao_usuario = new DAOUsuario();
            return dao_usuario.ActualizarPassword(usuario, passwordA);
        }
        
        /// <summary>
        /// Recupera informacion de un usuario
        /// </summary>
        /// <param name="codigo_usuario"></param>
        /// <returns></returns>
        public InformacionUsuario CargarInformacionUsuario(int codigo_usuario)
        {
            if (codigo_usuario < 1)
                throw new ArgumentException("El usuario suministrado no es valido.");
            DAOUsuario dao_usuario = new DAOUsuario();
            return dao_usuario.CargarInformacionUsuario(codigo_usuario);
        }
    }
}
