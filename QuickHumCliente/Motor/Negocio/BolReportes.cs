﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLReportes
    {
        /// <summary>
        /// Devuelve un arreglo de objetos que describe la prueba y la cantidad de pruebas realizadas de esta por genero.
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static PruebasRealizadasGenero[] ObtenerPruebasRealizadasGenero(int usuario_id, short sucursal_id, DateTime fecha_inicio, DateTime fecha_final)
        {
            DAOReportes dao_reporte = new DAOReportes();
            return DAOReportes.ObtenerPruebasRealizadasPorGenero(usuario_id, sucursal_id, fecha_inicio, fecha_final);
        }

        /// <summary>
        /// Devuelve 
        /// </summary>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_fin"></param>
        /// <returns></returns>
        public static PruebasRealizadasSucursal[] ObtenerPruebasRealizadasSucursal(short sucursal_id, DateTime fecha_inicio, DateTime fecha_fin)
        {
            DAOReportes dao_reporte = new DAOReportes();
            return DAOReportes.ObtenerPruebasRealizadasPorSucursal(sucursal_id, fecha_inicio, fecha_fin);
        }

        /// <summary>
        /// Devuelve el total de pruebas realizadas por X usuario en Z sucursal entre un intervalo de tiempo.
        /// La tabla construida es de un solo registro de 6 columnas donde cada par representa una categoria
        /// y cada elemento del par el total de un genero de esa categoria
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static DataTable GenerarPruebasRealizadasPorCategoriaYGenero(int usuario_id = -1, short sucursal_id = -1,
            DateTime fecha_inicio = new DateTime(), DateTime fecha_final = new DateTime())
        {
            PruebasRealizadasGenero[] pruebas_realizadas = ObtenerPruebasRealizadasGenero(usuario_id, sucursal_id,
                fecha_inicio, fecha_final);
            DataTable dt_resumen_pruebas_categoria = new DataTable("ResumenPruebaCategoria");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALCOMPORTAMIENTOHOMBRES");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALCOMPORTAMIENTOMUJERES");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALINTELIGENCIAHOMBRES");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALINTELIGENCIAMUJERES");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALPERSONALIDADHOMBRES");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALPERSONALIDADMUJERES");

            PruebasRealizadasGenero[] enumeracion_comportamiento =
                pruebas_realizadas.Where(
                    l_prueba =>
                        l_prueba.prueba == "ALLPORT" || l_prueba.prueba == "CLEAVER" || l_prueba.prueba == "KOSTICK"
                        || l_prueba.prueba == "LIFO" || l_prueba.prueba == "MOSS" || l_prueba.prueba == "ZAVIC")
                    .ToArray();

            PruebasRealizadasGenero[] enumeracion_inteligencia =
                pruebas_realizadas.Where(
                    l_prueba =>
                        l_prueba.prueba == "BARSIT" || l_prueba.prueba == "BETAIII" || l_prueba.prueba == "DOMINO"
                        || l_prueba.prueba == "INTEL" || l_prueba.prueba == "RAVEN" || l_prueba.prueba == "TERMAN" ||
                        l_prueba.prueba == "WONDERLIC").ToArray();
            PruebasRealizadasGenero[] enumeracion_personalidad =
                pruebas_realizadas.Where(
                    l_prueba =>
                        l_prueba.prueba == "PF16" || l_prueba.prueba == "BFQ" || l_prueba.prueba == "LUSCHER" ||
                        l_prueba.prueba == "CPI"
                        || l_prueba.prueba == "IPV" || l_prueba.prueba == "MMPIA" || l_prueba.prueba == "MMPIA2" ||
                        l_prueba.prueba == "PIPG").ToArray();

            dt_resumen_pruebas_categoria.Rows.Add(
                enumeracion_comportamiento.Sum(l_comportamiento => l_comportamiento.masculino),
                enumeracion_comportamiento.Sum(l_comportamiento => l_comportamiento.femenino),
                enumeracion_inteligencia.Sum(l_inteligencia => l_inteligencia.masculino),
                enumeracion_inteligencia.Sum(l_inteligencia => l_inteligencia.femenino),
                enumeracion_personalidad.Sum(l_personalidad => l_personalidad.masculino),
                enumeracion_personalidad.Sum(l_personalidad => l_personalidad.femenino)
                );
            return dt_resumen_pruebas_categoria;
        }

        /// <summary>
        /// Devuelveuna tabla que contiene las pruebas realizadas agrupadas por generos.
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static DataTable GenerarPruebasRealizadasPorGenero(int usuario_id = -1, short sucursal_id = -1,
            DateTime fecha_inicio = new DateTime(), DateTime fecha_final = new DateTime())
        {
            PruebasRealizadasGenero[] pruebas_realizadas = ObtenerPruebasRealizadasGenero(usuario_id, sucursal_id,
                fecha_inicio, fecha_final);
            DataTable dt_resumen_pruebas = new DataTable("ResumenPrueba");
            dt_resumen_pruebas.Columns.Add("PRUEBA");
            dt_resumen_pruebas.Columns.Add("TOTALHOMBRES");
            dt_resumen_pruebas.Columns.Add("TOTALMUJERES");
            foreach (PruebasRealizadasGenero prueba in pruebas_realizadas)
            {
                dt_resumen_pruebas.Rows.Add(prueba.prueba, prueba.masculino, prueba.femenino);
            }
            return dt_resumen_pruebas;
        }

        /// <summary>
        /// Devuelve la cantidad de tests realizadas por prueba agrupados por sucursal
        /// </summary>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_inicio"></param>
        /// <param name="fecha_fin"></param>
        /// <returns></returns>
        public static DataTable GenerarPruebasRealizadasSucursales(short sucursal_id = -1, DateTime fecha_inicio = new DateTime(), DateTime fecha_fin = new DateTime())
        {
            PruebasRealizadasSucursal[] pruebas_sucursales = ObtenerPruebasRealizadasSucursal(sucursal_id, fecha_inicio,
                fecha_fin);
            DataTable dt_resumen_pruebas_categoria = new DataTable("ResumenPruebaCategoriaGlobal");
            dt_resumen_pruebas_categoria.Columns.Add("OFICINA");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALCOMPORTAMIENTO");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALINTELIGENCIA");
            dt_resumen_pruebas_categoria.Columns.Add("TOTALPERSONALIDAD");
            foreach (PruebasRealizadasSucursal prueba in pruebas_sucursales)
            {                
                dt_resumen_pruebas_categoria.Rows.Add(
                    prueba.OFICINA, prueba.ALLPORT + prueba.CLEAVER + prueba.KOSTICK + prueba.LIFO + prueba.MOSS + prueba.ZAVIC,
                    prueba.BARSIT + prueba.BETAIII + prueba.DOMINO + prueba.INTEL + prueba.RAVEN + prueba.TERMAN + prueba.WONDERLIC,
                    prueba.PF16 + prueba.BFQ + prueba.LUSCHER + prueba.CPI + prueba.IPV + prueba.MMPIA + prueba.MMPIA2 + prueba.PIPG);
            }
            return dt_resumen_pruebas_categoria;
        }

        /// <summary>
        /// Devuelve las estadisticas de los expedientes filtrados por los parametros
        /// </summary>
        /// <param name="usuario_id"></param>
        /// <param name="sucursal_id"></param>
        /// <param name="fecha_incial"></param>
        /// <param name="fecha_final"></param>
        /// <returns></returns>
        public static EstadisticasExpedientes ObtenerEstadisticasExpedientes
            (int usuario_id = -1, short sucursal_id = 1, DateTime fecha_incial = new DateTime(), DateTime fecha_final = new DateTime())
        {
            return DAOReportes.ObtenerEstadisticasExpedientes(usuario_id, sucursal_id, fecha_incial, fecha_final);
        }
    }
}