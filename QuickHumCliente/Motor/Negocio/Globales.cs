﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    public class Globales
    {
        public static QuickHumClient cliente;
        public static Oficina Oficina { get; set; }
        public static Usuario Evaluador { get; set; }
        public static Candidato Candidato { get; set; }
        public static InformacionUsuario InformacionEvaluador { get; set; }

        public static Dictionary<string, Test> NombrePruebas = new Dictionary<string, Test>()
        {
            {Properties.Settings.Default.ALLPORT, Test.ALLPORT},
            {Properties.Settings.Default.BARSIT, Test.BARSIT},
            {Properties.Settings.Default.BETAIII, Test.BETAIII},
            {Properties.Settings.Default.BFQ, Test.BFQ},
            {Properties.Settings.Default.CLEAVER, Test.CLEAVER},
            {Properties.Settings.Default.CPI, Test.CPI},
            {Properties.Settings.Default.DOMINOS, Test.DOMINOS},
            {Properties.Settings.Default.HUMANSOFT, Test.HUMANSOFT},
            {Properties.Settings.Default.INTELSOFT, Test.INTELSOFT},
            {Properties.Settings.Default.IPV, Test.IPV},
            {Properties.Settings.Default.KOSTICK, Test.KOSTICK},
            {Properties.Settings.Default.LIFO, Test.LIFO},
            {Properties.Settings.Default.LUSCHER, Test.LUSCHER},
            {Properties.Settings.Default.MMPI2, Test.MMPI2},
            {Properties.Settings.Default.MMPIA, Test.MMPIA},
            {Properties.Settings.Default.MOSS, Test.MOSS},
            {Properties.Settings.Default.PF16, Test.PF16},
            {Properties.Settings.Default.PIPG, Test.PIPG},
            {Properties.Settings.Default.RAVEN, Test.RAVEN},
            {Properties.Settings.Default.TERMAN, Test.TERMAN},
            {Properties.Settings.Default.WONDERLIC, Test.WONDERLIC},
            {Properties.Settings.Default.ZAVIC, Test.ZAVIC},
        };


        // Inicializa el servicio Web
        public static void InicializarServicio()
        {
            Globales.cliente = new QuickHumClient();
            cliente.Endpoint.Address = new EndpointAddress(new Uri(Properties.Settings.Default.DireccionServicio));
            cliente.Open();
        }

        /// <summary>
        /// Finaliza el servicio web
        /// </summary>
        public static void FinalizarServicio()
        {
            if (cliente == null)
                return;
            cliente.Close();
        }
    }
}
