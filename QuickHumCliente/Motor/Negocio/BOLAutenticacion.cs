﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuickHumCliente.Motor.Datos;
using System.ServiceModel;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLAutenticacion
    {
        /// <summary>
        /// Invoca la autenticacion en la capa de datos
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IniciarSesion(string nombre, string password)
        {
            DAOAutenticacion autenticacion = new DAOAutenticacion();
            DAOUsuario dao_usuario = new DAOUsuario();
            Globales.Evaluador = autenticacion.IniciarSesion(nombre, password);
            Globales.InformacionEvaluador = dao_usuario.CargarInformacionUsuario(Globales.Evaluador.Id);
            Globales.Oficina = Globales.Evaluador.Oficina;
            return true;
        }

        /// <summary>
        /// Inicia la sesion de un candidato
        /// </summary>
        /// <param name="dui"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IniciarSesionCandidato(string dui, string password)
        {
            ValidarDui(dui);
            DAOAutenticacion dao_autenticacion = new DAOAutenticacion();
            return dao_autenticacion.IniciarSesionCandidato(dui, password);
        }

        /// <summary>
        /// Registra un acceso temporal para el ingreso del candidato
        /// </summary>
        /// <param name="candidato"></param>
        /// <param name="duracion"></param>
        /// <returns></returns>
        public string  RegistrarAccesoTemporal(Candidato candidato, string duracion)
        {
            if (candidato == null)
                throw new ArgumentException("El candidato es nulo");
            if (candidato.Dui == string.Empty)
                throw new ArgumentException("El dui del candidato esta vacio");
            double duracion_acceso = double.Parse(duracion);
            DAOAutenticacion dao_autenticacion = new DAOAutenticacion();
            return dao_autenticacion.RegistrarAccesoTemporal(candidato, duracion_acceso);
        }

        /// <summary>
        /// Elimina los accesos que ya caducaron
        /// </summary>
        /// <returns></returns>
        public bool DepurarAccesosTemporales()
        {
            DAOAutenticacion dao_autenticacion = new DAOAutenticacion();
            return dao_autenticacion.DepurarAccesosTemporales();
        }

        /// <summary>
        /// Validacion de dui
        /// </summary>
        /// <param name="dui"></param>
        /// <returns></returns>
        private bool ValidarDui(string dui)
        {
            if (String.IsNullOrEmpty(dui))
                throw new ArgumentException("El dui es nulo o vacio");
            if(dui.Length != 9)
                throw new ArgumentException("EL dui es invalido");
            return true;}
    }
}
