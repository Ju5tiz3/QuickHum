﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLEscolaridad
    {
        /// <summary>
        /// Retorna la lista de escolaridades
        /// </summary>
        /// <returns></returns>
        public Escolaridad[] ListarEscolaridades()
        {
            DAOEscolaridad dao_escolaridad = new DAOEscolaridad();
            return dao_escolaridad.ListarEscolaridades();
        }

        /// <summary>
        /// Registra una nueva escolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        /// <returns></returns>
        public bool NuevaEscolaridad(Escolaridad escolaridad)
        {
            ValidarEscolaridad(escolaridad);
            DAOEscolaridad dao_escolaridad = new DAOEscolaridad();
            return dao_escolaridad.NuevaEscolaridad(escolaridad);
        }

        /// <summary>
        /// Actualiza una escolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        /// <returns></returns>
        public bool ActualizarEscolaridad(Escolaridad escolaridad)
        {
            ValidarEscolaridad(escolaridad);
            DAOEscolaridad dao_escolaridad = new DAOEscolaridad();
            return dao_escolaridad.ActualizarEscolaridad(escolaridad);
        }

        /// <summary>
        /// Eliminar una escolaridad especificada
        /// </summary>
        /// <param name="escolaridad"></param>
        public bool EliminarEscolaridad(Escolaridad escolaridad)
        {
            ValidarEscolaridad(escolaridad);
            DAOEscolaridad dao_escolaridad = new DAOEscolaridad();
            return dao_escolaridad.EliminarEscolaridad(escolaridad);
        }


        /// <summary>
        /// Valida el objeto escolaridad pasado a las diferentes funciones de BOLEscolaridad
        /// </summary>
        /// <param name="escolaridad"></param>
        private void ValidarEscolaridad(Escolaridad escolaridad)
        {
            if (escolaridad == null)
            {
                throw new ArgumentException("El objeto escolaridad es nulo.");
            }
            if (escolaridad.Nombre == string.Empty)
            {
                throw new ArgumentException("La escolaridad enviada esta vacia.");
            }
        }


    }
}
