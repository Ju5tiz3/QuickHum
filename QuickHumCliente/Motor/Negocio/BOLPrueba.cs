﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraPrinting.Native;
using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLPrueba
    {
        /// <summary>
        /// Devuelve una lista de pruebas
        /// </summary>
        /// <returns></returns>
        public ServicioQuickHum.Prueba[] ListaPruebas()
        {
            DAOPrueba dao_prueba = new DAOPrueba();
            return dao_prueba.ListaPruebas();
        }

        /// <summary>
        /// Devuelve la enumeracion prueba correspondiente al nombre de la prueba
        /// </summary>
        /// <param name="nombre_prueba"></param>
        /// <returns></returns>
        public Test ObtenerEnumeracionPrueba(string nombre_prueba)
        {
            if (Globales.NombrePruebas.ContainsKey(nombre_prueba) == false)
                throw new ArgumentException("La prueba solicitada no existe");
            return Globales.NombrePruebas[nombre_prueba];
        }

        /// <summary>
        /// Obtiene el estado de una prueba especifica
        /// </summary>
        /// <param name="nombre_test"></param>
        /// <param name="expediente_id"></param>
        /// <returns></returns>
        public PruebaEstado ObtenerEstadoPrueba(string nombre_test, int expediente_id = -1)
        {
            if (Globales.NombrePruebas.ContainsKey(nombre_test) == false)
                throw new ArgumentException("La prueba solicitada no existe");
            Test test = Globales.NombrePruebas[nombre_test];
            DAOPrueba dao_prueba = new DAOPrueba();
            return dao_prueba.ObtenerEstadoPrueba(test, expediente_id);
        }

        /// <summary>
        /// Devuelve el numero de una prueba X en un expediente N
        /// </summary>
        /// <param name="nombre_test"></param>
        /// <param name="numero_expediente"></param>
        /// <returns></returns>
        public int ObtenerNumeroPrueba(string nombre_test, int numero_expediente)
        {
            if (Globales.NombrePruebas.ContainsKey(nombre_test) == false )
                throw new ArgumentException("La prueba solicitada no existe");
            Test test = Globales.NombrePruebas[nombre_test];
            DAOPrueba dao_prueba = new DAOPrueba();
            int numero = dao_prueba.ObtenerNumeroPrueba(test, numero_expediente);
            if (numero == 0)
                throw new FaultException("No se encontro la prueba esepcificada en el expediente");
            return numero;
        }

        /// <summary>
        /// Devuelve las escolaridades de un tipo de prueba
        /// </summary>
        /// <param name="prueba"></param>
        /// <returns></returns>
        public EscolaridadPrueba[] ObtenerEscolaridadesPrueba(Prueba prueba)
        {
            if (Globales.NombrePruebas.ContainsKey(prueba.Nombre) == false)
                throw new ArgumentException("El nombre de la prueba no coincide con las existentes.");
            DAOPrueba dao_prueba = new DAOPrueba();
            return dao_prueba.ObtenerEscolaridadesPrueba(Globales.NombrePruebas[prueba.Nombre]);
        }
    }
}
