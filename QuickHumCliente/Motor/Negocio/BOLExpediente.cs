﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuickHumCliente.Motor.Datos;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente.Motor.Negocio
{
    class BOLExpediente
    {
        public ServicioQuickHum.Expediente[] ListarExpedientes(string dui_candidato)
        {
            DAOExpediente dao_expediente = new DAOExpediente();
            return dao_expediente.ListarExpedientes(dui_candidato);
        }

        /// <summary>
        /// Crea un nuevo expediente
        /// </summary>
        /// <param name="expediente"></param>
        /// <returns></returns>
        public bool NuevoExpediente(Expediente expediente)
        {
            DAOExpediente dao_expediente = new DAOExpediente();
            return dao_expediente.NuevoExpediente(expediente);
        }

        /// <summary>
        /// Devuelve el expediente especificado
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <returns></returns>
        public Expediente CargarExpediente(int numero_expediente)
        {
            if (numero_expediente < 1)
                throw new ArgumentException("El numero de expediente no es valido");
            DAOExpediente dao_expediente = new DAOExpediente();
            return dao_expediente.CargarExpediente(numero_expediente);
        }

        /// <summary>
        /// Actualiza las anotaciones de un expediente
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <param name="anotaciones"></param>
        public bool ActualizarAnotacionesExpediente(int numero_expediente, string anotaciones)
        {
            anotaciones = anotaciones.Trim();
            DAOExpediente dao_expediente = new DAOExpediente();
            return dao_expediente.ActualizarAnotacionesExpediente(numero_expediente, anotaciones);
        }

        /// <summary>
        /// Cambia el estado de un expediente
        /// </summary>
        /// <param name="numero_expediente"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        public bool CambiarEstadoExpediente(int numero_expediente, Expediente.EstadoExpediente estado)
        {
            if(numero_expediente == 0)
                throw new ArgumentException("No se ha definido el numero del expediente.");
            DAOExpediente dao_expediente = new DAOExpediente();
            return dao_expediente.CambiarEstadoExpediente(numero_expediente, estado);
        }
    }
}
