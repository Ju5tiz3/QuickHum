﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuickHumCliente.Motor.Datos;

namespace QuickHumCliente.Motor.Negocio
{
    /// <summary>
    /// Clase que controla las operaciones relacionadas con el candidato
    /// </summary>
    class BOLCandidato
    {
        /// <summary>
        /// Registra un nuevo candidato
        /// </summary>
        /// <param name="candidato"></param>
        /// <returns></returns>
        public bool RegistrarCandidato(ServicioQuickHum.Candidato candidato)
        {
            DAOCandidato dao_candidato = new DAOCandidato();
            return dao_candidato.RegistrarCandidato(candidato);
        }

        /// <summary>
        /// Actualiza un candidato
        /// </summary>
        /// <param name="candidato"></param>
        /// <returns></returns>
        public bool ActualizarCandidato(ServicioQuickHum.Candidato candidato)
        {
            DAOCandidato dao_candidato = new DAOCandidato();
            return dao_candidato.ActualizarCandidato(candidato);
        }

        /// <summary>
        /// Carga un candidato especificado mediante su dui
        /// </summary>
        /// <param name="dui"></param>
        /// <returns></returns>
        public ServicioQuickHum.Candidato CargarCandidato(string dui)
        {
            DAOCandidato dao_candidato = new DAOCandidato();
            return dao_candidato.CargarCandidato(dui);
        }

        /// <summary>
        /// Elimina un candidato mediante su dui
        /// </summary>
        /// <param name="dui"></param>
        /// <returns></returns>
        public bool EliminarCandidato(string dui)
        {
            DAOCandidato dao_candidato = new DAOCandidato();
            return dao_candidato.EliminarCandidato(dui);
        }
    }
}
