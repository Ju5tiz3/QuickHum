﻿using System;
using System.Windows.Forms;
using QuickHumCliente.ServicioQuickHum;

namespace QuickHumCliente
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Vistas.frmIngreso());
            Motor.Negocio.Globales.FinalizarServicio();
        }
    }
}
