﻿namespace QuickHumCliente.ServicioQuickHum
{
    public partial class Expediente
    {
        public Expediente()
        {
            this.Candidato = new Candidato();
            this.Estado = EstadoExpediente.ABIERTO;
            this.Evaluador = new Usuario();
            this.Ocupacion = new Ocupacion();
            this.PruebasAsignadas = null;
            this.Ubicacion = new Oficina();
            this.Anotaciones = string.Empty;
        }
    }
}