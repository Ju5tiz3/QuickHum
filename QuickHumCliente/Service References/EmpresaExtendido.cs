﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickHumCliente.ServicioQuickHum
{
    /// <summary>
    /// Extiende la clase dto empresa
    /// </summary>
    public partial class Oficina
    {
        public override string ToString()
        {
            return this.Nombre;
        }
    }
}
