﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickHumCliente.ServicioQuickHum
{
    /// <summary>
    /// Extiende la clase dto Ocupacion
    /// </summary>
    public partial class Ocupacion
    {
        public override string ToString()
        {
            return this.Nombre;
        }
    }
}
