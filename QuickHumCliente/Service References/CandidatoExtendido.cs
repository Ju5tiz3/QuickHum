﻿namespace QuickHumCliente.ServicioQuickHum
{
    public partial class Candidato
    {
        public Candidato()
        {
            this.Escolaridad = new Escolaridad();
            this.Genero = CandidatoGenero.MASCULINO;
            this.EstadoCivil = "SOLTERO";
        }
    }
}