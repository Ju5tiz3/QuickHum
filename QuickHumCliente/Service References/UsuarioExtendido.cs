﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickHumCliente.ServicioQuickHum
{
    /// <summary>
    /// Extiende la clase dto usuario
    /// </summary>
    public partial class Usuario
    {
        public Usuario()
        {
            this.Oficina = new Oficina();
            this.Rol = new Rol();
        }

        public override string ToString()
        {
            return this.Nombre;
        }
    }
}
